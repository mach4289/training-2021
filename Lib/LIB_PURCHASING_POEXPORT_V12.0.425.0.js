///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "scriptType": "COMMON",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_POEdition_V12.0.425.0",
    "Lib_P2P_CompanyCodesValue_V12.0.425.0",
    "Lib_Purchasing_Vendor_V12.0.425.0",
    "Lib_Purchasing_ShipTo_V12.0.425.0",
    "Lib_P2P_Export_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var PORPTMapping = {
            header: {
                "OrderDate__": function (info) {
                    return Sys.Helpers.Date.ToUTCDate(new Date());
                },
                "RevisionDateTime__": "RevisionDateTime__",
                "VendorNumber__": "VendorNumber__",
                "VendorName__": "VendorName__",
                "VendorAddress__": "VendorAddress__",
                "ShipToCompany__": "ShipToCompany__",
                "ShipToContact__": "ShipToContact__",
                "ShipToPhone__": "ShipToPhone__",
                "ShipToEmail__": "ShipToEmail__",
                "ShipToAddress__": "ShipToAddress__",
                "OrderNumber__": function (info) {
                    return info.poNumber;
                },
                "Currency__": "Currency__",
                "TotalNetAmount__": "TotalNetAmount__",
                "TotalTaxAmount__": "TotalTaxAmount__",
                "TotalAmountIncludingVAT__": "TotalAmountIncludingVAT__",
                "PaymentAmount__": "PaymentAmount__",
                "BuyerComment__": "BuyerComment__",
                "PaymentTermCode__": "PaymentTermCode__",
                "PaymentTermDescription__": function (info) {
                    if (Lib.ERP.IsSAP()) {
                        return Sys.Helpers.Promise.Create(function (resolve, reject) {
                            var config = Variable.GetValueAsString("SAPConfiguration");
                            var language = info.language || "%SAPCONNECTIONLANGUAGE%";
                            var filter = "ZTERM = '" + Data.GetValue("PaymentTermCode__") + "' AND SPRAS = '%ISO-" + language + "%'";
                            Sys.GenericAPI.SAPQuery(config, "T052U", filter, ["TEXT1"], function (r, error) {
                                resolve(!error && r && r.length > 0 ? r[0].TEXT1 : "");
                            }, 1);
                        });
                    }
                    return Data.GetValue("PaymentTermDescription__");
                },
                "PaymentMethodCode__": "PaymentMethodCode__",
                "PaymentMethodDescription__": "PaymentMethodDescription__",
                "ValidityStart__": "ValidityStart__",
                "ValidityEnd__": "ValidityEnd__",
                "DisplayTaxCode__": function (info) {
                    return !!Sys.Parameters.GetInstance("PAC").GetParameter("DisplayTaxCode");
                },
                "DisplayUnitOfMeasure__": function (info) {
                    return !!Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
                }
                /* Also available before S137:
                ,"logo": function() {
                    return "%AccountLogo%";
                }*/
            },
            logo: {
                "template": function (info) {
                    return "%images%\\PO_logo.png";
                },
                "suffix": function () {
                    return Sys.Helpers.String.NormalizeFileSuffix(Sys.Helpers.String.RemoveIllegalCharactersFromFilename(Data.GetValue("CompanyCode__")));
                },
                "default": function () {
                    return "%AccountLogo%";
                }
            },
            companyInfo: {
                "FormattedAddress__": function (info) {
                    return info.formattedBlockAddress;
                },
                "CompanyName__": "CompanyName__",
                "Sub__": "Sub__",
                "Street__": "Street__",
                "PostOfficeBox__": "PostOfficeBox__",
                "City__": "City__",
                "PostalCode__": "PostalCode__",
                "Region__": "Region__",
                "Country__": "Country__",
                "PhoneNumber__": "PhoneNumber__",
                "FaxNumber__": "FaxNumber__",
                "VATNumber__": "VATNumber__",
                "ContactEmail__": "ContactEmail__",
                "SIRET__": "SIRET__",
                "DecimalCount": function (info) {
                    return 2;
                },
                "FormatDate": function (info) {
                    return (Sys.Helpers.Date.dateFormatLocales[info.culture] || Sys.Helpers.Date.dateFormatLocales["default"])
                        .replace(/\bmm\b/, "MM")
                        .replace(/\bm\b/, "M");
                },
                "SeparatorThousand": function (info) {
                    return encodeURIComponent(Lib.Purchasing.delimitersThousandsLocales[info.culture] || Lib.Purchasing.delimitersThousandsLocales["default"]);
                },
                "SeparatorDecimal": function (info) {
                    return Lib.Purchasing.delimitersDecimalLocales[info.culture] || Lib.Purchasing.delimitersDecimalLocales["default"];
                },
                "SeparatorInfos": function (info) {
                    return encodeURIComponent(" - ");
                },
                "SupplierAddressRightWindow": function (info) {
                    return "0";
                },
                "MultiShipTo": function (info) {
                    return Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
                }
            },
            tables: {
                "LineItems__": {
                    table: "LineItems__",
                    fields: {
                        "LineItemNumber__": "LineItemNumber__",
                        "ItemNumber__": "ItemNumber__",
                        "ItemDescription__": "ItemDescription__",
                        "ItemQuantity__": function (info) {
                            var qty = 0;
                            if (info.item.GetValue("ItemType__") !== Lib.P2P.ItemType.AMOUNT_BASED) {
                                qty = info.item.GetValue("ItemQuantity__");
                            }
                            return qty;
                        },
                        "ItemRequestedDeliveryDate__": "ItemRequestedDeliveryDate__",
                        "ItemUnitPrice__": function (info) {
                            var pu = 0;
                            if (info.item.GetValue("ItemType__") !== Lib.P2P.ItemType.AMOUNT_BASED) {
                                pu = info.item.GetValue("ItemUnitPrice__");
                            }
                            return pu;
                        },
                        "ItemNetAmount__": "ItemNetAmount__",
                        "ItemTaxCode__": "ItemTaxCode__",
                        "ItemTaxRate__": "ItemTaxRate__",
                        "ItemTaxAmount__": "ItemTaxAmount__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemShipToAddress__": "ItemShipToAddress__",
                        "ItemShipToCompany__": "ItemShipToCompany__",
                        "ItemType__": "ItemType__",
                        "ItemStartDate__": "ItemStartDate__",
                        "ItemEndDate__": "ItemEndDate__"
                    }
                },
                "POWorkflow__": {
                    table: "POWorkflow__",
                    fields: {
                        "WRKFRole__": "WRKFRole__",
                        "WRKFUserName__": "WRKFUserName__"
                    }
                },
                "TaxSummary__": {
                    table: "TaxSummary__",
                    fields: {
                        "TaxCode__": "TaxCode__",
                        "TaxDescription__": "TaxDescription__",
                        "TaxRate__": "TaxRate__",
                        "NetAmount__": "NetAmount__",
                        "TaxAmount__": "TaxAmount__"
                    }
                },
                "AdditionalFees__": {
                    table: "AdditionalFees__",
                    fields: {
                        "ItemDescription__": "AdditionalFeeDescription__",
                        "ItemUnitPrice__": function (info) {
                            var ret = info.item.GetValue("Price__");
                            if (!ret) {
                                ret = 0;
                            }
                            return ret;
                        },
                        "ItemNetAmount__": function (info) {
                            var ret = info.item.GetValue("Price__");
                            if (!ret) {
                                ret = 0;
                            }
                            return ret;
                        },
                        "ItemTaxCode__": "ItemTaxCode__",
                        "ItemTaxRate__": "ItemTaxRate__",
                        "ItemTaxAmount__": "ItemTaxAmount__"
                    }
                }
            }
        };
        Purchasing.POExport = {
            GetPOTemplateInfos: function () {
                // Base infos
                var poInfos = {
                    escapedCompanyCode: Sys.Helpers.String.NormalizeFileSuffix(Sys.Helpers.String.RemoveIllegalCharactersFromFilename(Data.GetValue("CompanyCode__"))),
                    template: null,
                    poNumber: Sys.ScriptInfo.IsClient() ? Sys.Helpers.Globals.Controls.OrderNumber__.GetValue() : Data.GetValue("OrderNumber__"),
                    fileFormat: null,
                    language: null,
                    culture: null,
                    termsConditions: null
                };
                // Overloads with user exit
                var templateFromUserExit = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.GetPOTemplateName");
                if (templateFromUserExit != null) {
                    if (Sys.Helpers.IsString(templateFromUserExit)) {
                        poInfos.template = templateFromUserExit;
                    }
                    else {
                        poInfos.template = templateFromUserExit.template;
                        poInfos.language = templateFromUserExit.language;
                        poInfos.culture = templateFromUserExit.culture;
                    }
                }
                if (poInfos.template == null) {
                    poInfos.template = Sys.Parameters.GetInstance("PAC").GetParameter("POTemplateName");
                    Log.Info("Template name comes from parameters");
                }
                Log.Info("Template name is: '" + poInfos.template + "'");
                var termsConditionsFromUserExit = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.GetPOTermsConditionsName");
                if (termsConditionsFromUserExit != null) {
                    if (Sys.Helpers.IsString(termsConditionsFromUserExit)) {
                        poInfos.termsConditions = "%Misc%\\" + termsConditionsFromUserExit;
                    }
                }
                if (poInfos.termsConditions == null && Sys.Parameters.GetInstance("PAC").GetParameter("POTermsConditionsTemplateName")) {
                    poInfos.termsConditions = "%Misc%\\" + Sys.Parameters.GetInstance("PAC").GetParameter("POTermsConditionsTemplateName");
                    Log.Info("Terms conditions name comes from parameters");
                }
                Log.Info("Terms conditions name is: '" + poInfos.termsConditions + "'");
                poInfos.fileFormat = poInfos.template.substr(poInfos.template.length - 3).toLowerCase() === "rpt" ? "RPT" : "DOCX";
                return poInfos;
            },
            CreatePOJsonString: function (poTemplateInfos, callback) {
                var cc = Data.GetValue("CompanyCode__");
                Lib.P2P.CompanyCodesValue.QueryValues(cc).Then(function (CCValues) {
                    if (Object.keys(CCValues).length > 0) {
                        var companyInfos = CCValues;
                        var shipToAddress = JSON.parse(Variable.GetValueAsString("ShipToAddress") || null);
                        var vendorAddress = JSON.parse(Variable.GetValueAsString("VendorAddress") || null);
                        var sameCountry = shipToAddress && vendorAddress && shipToAddress.ToCountry == vendorAddress.ToCountry && shipToAddress.ToCountry == companyInfos.Country__;
                        var CompanyAddress_1 = {
                            "ToName": "ToRemove",
                            "ToSub": companyInfos.Sub__,
                            "ToMail": companyInfos.Street__,
                            "ToPostal": companyInfos.PostalCode__,
                            "ToCountry": companyInfos.Country__,
                            "ToCountryCode": companyInfos.Country__,
                            "ToState": companyInfos.Region__,
                            "ToCity": companyInfos.City__,
                            "ToPOBox": companyInfos.PostOfficeBox__,
                            "ForceCountry": sameCountry ? "false" : "true"
                        };
                        var fromCountry = "";
                        if (sameCountry) {
                            fromCountry = companyInfos.Country__;
                        }
                        else {
                            fromCountry = "US";
                        }
                        var options_1 = {
                            "isVariablesAddress": true,
                            "address": CompanyAddress_1,
                            "countryCode": fromCountry // Get country code from contract ModProvider
                        };
                        var promises = [];
                        promises.push(Sys.Helpers.Promise.Create(function (resolve, reject) {
                            var vendor = null;
                            if (!poTemplateInfos.culture || !poTemplateInfos.language) {
                                if (Sys.ScriptInfo.IsServer()) {
                                    vendor = Lib.Purchasing.Vendor.GetVendorContact();
                                    if (vendor) {
                                        Log.Info("Using language and culture of vendor contact");
                                        resolve({
                                            culture: poTemplateInfos.culture || vendor.GetVars().GetValue_String("Culture", 0),
                                            language: poTemplateInfos.language || vendor.GetVars().GetValue_String("Language", 0)
                                        });
                                    }
                                    else {
                                        resolve({
                                            culture: poTemplateInfos.culture,
                                            language: poTemplateInfos.language
                                        });
                                    }
                                }
                                else {
                                    Lib.Purchasing.Vendor.GetVendorInfo(function (vendorInfos) {
                                        resolve({
                                            culture: poTemplateInfos.culture || vendorInfos.culture,
                                            language: poTemplateInfos.language || vendorInfos.language
                                        });
                                    });
                                }
                            }
                            else {
                                resolve({
                                    culture: poTemplateInfos.culture,
                                    language: poTemplateInfos.language
                                });
                            }
                        }));
                        promises.push(Sys.Helpers.Promise.Create(function (resolve, reject) {
                            Sys.GenericAPI.CheckPostalAddress(options_1, function (address) {
                                Log.Info("CheckPostalAddress input:  " + JSON.stringify(CompanyAddress_1));
                                Log.Info("CheckPostalAddress output: " + JSON.stringify(address));
                                if (!Sys.Helpers.IsEmpty(address.LastErrorMessage)) {
                                    Log.Error("Error while processing CheckPostalAddress during PO Json string creation.");
                                    var message = Language.Translate("InvalidCompanyAddress", false, Data.GetValue("CompanyCode__"), address.LastErrorMessage);
                                    Lib.CommonDialog.NextAlert.Define("_Purchase order attaching error", message, { isError: true });
                                    reject(message);
                                }
                                else {
                                    resolve({ formattedAddress: address.FormattedBlockAddress.replace(/^[^\r\n]+(\r|\n)+/, "") });
                                }
                            });
                        }));
                        // those promise are conditionals and we don't care of the return
                        if (shipToAddress) {
                            shipToAddress.ForceCountry = CompanyAddress_1.ForceCountry;
                            promises.push(Lib.Purchasing.ShipTo.FillPostalAddress(shipToAddress));
                        }
                        if (vendorAddress) {
                            vendorAddress.ForceCountry = CompanyAddress_1.ForceCountry;
                            promises.push(Lib.Purchasing.Vendor.FillPostalAddress(vendorAddress));
                        }
                        Sys.Helpers.Synchronizer.All(promises)
                            .Then(function (res) {
                            var POData = {};
                            // Language and culture
                            var culture = res[0].culture;
                            var language = res[0].language;
                            if (!culture || !language) {
                                Log.Info("Using language and culture of current user (Buyer)");
                                culture = (Sys.ScriptInfo.IsClient() ? Sys.Helpers.Globals.User.culture : Lib.P2P.GetValidatorOrOwner().GetValue("Culture"));
                                language = (Sys.ScriptInfo.IsClient() ? Sys.Helpers.Globals.User.language : Lib.P2P.GetValidatorOrOwner().GetValue("Language"));
                            }
                            if (res[1].formattedAddress) {
                                POData = Purchasing.POExport.CreatePODataForRPT(poTemplateInfos.poNumber, language, culture, res[1].formattedAddress);
                            }
                            var buyer = (Sys.ScriptInfo.IsClient() ? Sys.Helpers.Globals.User : Lib.P2P.GetValidatorOrOwner());
                            // Add language data and return
                            var poTemplateName = poTemplateInfos.template.substr(0, poTemplateInfos.template.lastIndexOf('.')) || poTemplateInfos.template;
                            Log.Time("GetTemplateContent");
                            Purchasing.POExport.GetTemplateContent(buyer, poTemplateName + ".json", language)
                                .Then(function (languageTemplate) {
                                Log.TimeEnd("GetTemplateContent");
                                // Add language
                                Sys.Helpers.Extend(true, POData, JSON.parse(languageTemplate.content));
                                // Final User callback
                                var updatedPOData = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.CustomizePOData", POData);
                                if (updatedPOData) {
                                    POData = updatedPOData;
                                }
                                callback(JSON.stringify(POData));
                            })
                                .Catch(function (e) {
                                Log.TimeEnd("GetTemplateContent");
                                Log.Error("Failed to GetTemplateContent: " + e);
                                callback(JSON.stringify(POData));
                            });
                        })
                            .Catch(function (error) {
                            var errorMessage = error.toString();
                            Log.Error(errorMessage);
                            callback(JSON.stringify({ error: errorMessage }));
                        });
                    }
                });
            },
            CreatePODataForRPT: function (poNumber, language, culture, formattedBlockAddress) {
                // Takes into account an user exit.
                var extraFields = Sys.Helpers.TryCallFunction("Lib.PO.Customization.Common.GetExtraPORPTFields");
                if (extraFields) {
                    Sys.Helpers.Extend(true, PORPTMapping, extraFields);
                }
                var info = {
                    language: language,
                    culture: culture,
                    poNumber: poNumber,
                    formattedBlockAddress: formattedBlockAddress
                };
                var POData = {
                    header: {},
                    logo: {},
                    companyInfo: {},
                    tables: {}
                };
                function FillValues(element, defaultGetValue) {
                    var poData = POData[element];
                    Sys.Helpers.Object.ForEach(PORPTMapping[element], function (data, field) {
                        var dataValue;
                        if (Sys.Helpers.IsFunction(data)) {
                            dataValue = data(info);
                        }
                        else if (Sys.Helpers.IsFunction(defaultGetValue)) {
                            dataValue = defaultGetValue(data);
                        }
                        else {
                            dataValue = Data.GetValue(data);
                        }
                        // except for Date type where we always set as a string with "YYYY-MM-DD" format
                        if (Sys.Helpers.Promise.IsPromise(dataValue)) {
                            dataValue.Then(function (value) { poData[field] = value instanceof Date ? Sys.Helpers.Date.ToUTCDate(value) : value; });
                        }
                        else {
                            poData[field] = dataValue instanceof Date ? Sys.Helpers.Date.ToUTCDate(dataValue) : dataValue;
                        }
                    });
                }
                if (PORPTMapping.header) {
                    FillValues("header");
                    if (Lib.Purchasing.POEdition.ChangesManager.HasChangeInForm() && Data.GetValue("RevisionDateTime__") == null) {
                        POData.header["RevisionDateTime__"] = Sys.Helpers.Date.ToUTCDate(new Date());
                    }
                }
                if (PORPTMapping.logo) {
                    FillValues("logo");
                }
                if (PORPTMapping.companyInfo) {
                    var cc = Data.GetValue("CompanyCode__");
                    var companyInfos_1 = Lib.P2P.CompanyCodesValue.GetValues(cc);
                    if (Object.keys(companyInfos_1).length > 0) {
                        FillValues("companyInfo", function (data) { return companyInfos_1[data]; });
                    }
                    else {
                        Log.Info("Error: No record found for company code '" + cc + "'");
                    }
                }
                if (PORPTMapping.tables) {
                    for (var key in PORPTMapping.tables) {
                        if (key) {
                            var tableMapping = PORPTMapping.tables[key];
                            var tableName = tableMapping.table || key;
                            if (tableMapping.fields) {
                                POData.tables[tableName] = Lib.P2P.Export.SerializeTable(tableName, tableMapping, info);
                            }
                            else {
                                Log.Info("Error: table '" + key + "' is missing 'fields' attribute");
                            }
                        }
                    }
                }
                if (POData.tables["LineItems__"].length > 0 && Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    var lineItems_1 = POData.tables["LineItems__"];
                    var isFirstLineAddressEmpty = Sys.Helpers.IsEmpty(lineItems_1[0].ItemShipToCompany__) && Sys.Helpers.IsEmpty(lineItems_1[0].ItemShipToAddress__);
                    var isAllSameAddress = Sys.Helpers.Array.Every(lineItems_1, function (item) {
                        return item.ItemShipToCompany__ === lineItems_1[0].ItemShipToCompany__ &&
                            item.ItemShipToAddress__ === lineItems_1[0].ItemShipToAddress__;
                    });
                    POData.companyInfo["MultiShipTo"] = !isAllSameAddress;
                    if (isAllSameAddress && !isFirstLineAddressEmpty) {
                        POData.header["ShipToCompany__"] = lineItems_1[0].ItemShipToCompany__;
                        POData.header["ShipToAddress__"] = lineItems_1[0].ItemShipToAddress__;
                    }
                }
                if (POData.tables["AdditionalFees__"].length > 0) {
                    POData.tables["LineItems__"].push({});
                    var additionnalFeesSummary = {};
                    var rowToRemove = [];
                    for (var i = 0; i < POData.tables["AdditionalFees__"].length; i++) {
                        var row = POData.tables["AdditionalFees__"][i];
                        if (additionnalFeesSummary[row.ItemDescription__ + row.ItemTaxCode__]) {
                            var additionnalFee = additionnalFeesSummary[row.ItemDescription__ + row.ItemTaxCode__];
                            rowToRemove.push(additionnalFee.Index);
                            additionnalFee.Index = i;
                            additionnalFee.NetAmount = new Sys.Decimal(additionnalFee.NetAmount || 0).add(row.ItemNetAmount__).toNumber();
                            row.ItemNetAmount__ = additionnalFee.NetAmount;
                            row.ItemUnitPrice__ = additionnalFee.NetAmount;
                        }
                        else {
                            additionnalFeesSummary[row.ItemDescription__ + row.ItemTaxCode__] = {
                                Index: i,
                                NetAmount: row.ItemNetAmount__
                            };
                        }
                    }
                    rowToRemove = rowToRemove.reverse();
                    for (var _i = 0, rowToRemove_1 = rowToRemove; _i < rowToRemove_1.length; _i++) {
                        var index = rowToRemove_1[_i];
                        POData.tables["AdditionalFees__"].splice(index, 1);
                    }
                    POData.tables["LineItems__"] = POData.tables["LineItems__"].concat(POData.tables["AdditionalFees__"]);
                }
                POData.tables["AdditionalFees__"] = [{ AdditionalFeeDescription__: "", Price__: 0 }];
                return POData;
            },
            GetTemplateContent: function (user, jsonFileName, culture) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (Sys.ScriptInfo.IsClient()) {
                        user.GetTemplateContent(function (templateContent) { resolve(templateContent); }, jsonFileName, culture);
                    }
                    else {
                        var templateContent = user.GetTemplateContent({
                            templateName: jsonFileName,
                            language: culture
                        });
                        resolve(templateContent);
                    }
                });
            }
        };
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
