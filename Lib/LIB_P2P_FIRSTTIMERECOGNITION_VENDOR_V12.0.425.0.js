///#GLOBALS Lib
/* LIB_DEFINITION{
  "name": "Lib_P2P_FirstTimeRecognition_Vendor",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "P2P library",
  "require": [
    "Sys/Sys_Helpers_IBAN",
    "Lib_P2P_V12.0.425.0",
    "[Lib_P2P_Customization_FTRVendor]"
  ]
}*/
/**
 * @namespace Lib.P2P.FirstTimeRecognition_Vendor
 */
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        var FirstTimeRecognition_Vendor;
        (function (FirstTimeRecognition_Vendor) {
            // #region debug
            // Enable/disable debug mode
            var g_debug = false;
            // Enable/disable this very verbose mode for areas construction
            var g_debugAreas = false;
            function traceDebug(txt) {
                if (g_debug) {
                    Log.Info(txt);
                }
            }
            function traceDebugAreas(txt) {
                if (g_debugAreas) {
                    Log.Info(txt);
                }
            }
            /**
             * Handle the list of supported and available culture
             * @class CultureManager
             * @memberof Lib.P2P.FirstTimeRecognition_Vendor
             */
            var CultureManager = /** @class */ (function () {
                function CultureManager() {
                    /**
                     * The list of allowed ISO code for this document
                     * @member Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#m_availableCultures
                     * @private
                     * @type {string[]}
                     */
                    this.m_availableCultures = [];
                    /**
                     * The list of supported cultures
                     * @member Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#m_availableCultures
                     * @private
                     * @type {Lib.P2P.FirstTimeRecognition_Vendor.CultureInfo}
                     */
                    this.m_culturesInfo = {};
                    /**
                     * The current active culture for the document
                     * @member Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#activeCulture
                     * @public
                     * @type {Lib.P2P.FirstTimeRecognition_Vendor.CultureInfo}
                     */
                    this.activeCulture = null;
                    // Init manager
                    this.Init();
                }
                /**
                 * Initialize the culture manager. Calls the Lib.P2P.Customization.FTRVendor.GetCultureInfos user exit.
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#Init
                 * @public
                 */
                CultureManager.prototype.Init = function () {
                    this.m_availableCultures = [];
                    this.m_culturesInfo = {};
                    this.activeCulture = null;
                    var defaultCulture = this.GetDefaultSupportedCultures();
                    var supportedCultures = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.FTRVendor.GetCultureInfos", defaultCulture) || defaultCulture;
                    for (var culture in supportedCultures) {
                        if (supportedCultures.hasOwnProperty(culture)) {
                            var cultureInfo = supportedCultures[culture];
                            this.m_culturesInfo[cultureInfo.name] = cultureInfo;
                        }
                    }
                    this.SetAvailableCultures();
                };
                /**
                 * List the default supported cultures
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetDefaultSupportedCultures
                 * @public
                 * @returns {Lib.P2P.FirstTimeRecognition_Vendor.CultureInfo[]}
                 */
                CultureManager.prototype.GetDefaultSupportedCultures = function () {
                    return [
                        {
                            name: "en-US",
                            faxOrPhoneRegExp: /[0-9+\-()/ .]{9,}[0-9]/mg,
                            postalCodeRegExp: /[a-zA-Z]{2}(\.)?[ ]+[0-9]{5}(([ ]?-[ ]?[0-9]{4})|-)?/mg,
                            defaultCurrency: "USD",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Total Net Amount", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Grand Total"],
                            totalKeywordsSecondTry: ["Amount", "Total"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]o", "INVOICE [#]", "CREDIT MEMO", "Credit memo"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "de-DE",
                            VATRegExp: /((DE)?[ ]?\d{3}[ ]?\d{3}[ ]?\d{3})|((ATU)[ ]?\d{8})/mg,
                            faxOrPhoneRegExp: /((\+?(49))|0)[0-9\-\(\)\/ \.]{2,}[0-9]/mg,
                            postalCodeRegExp: /\b(DE[- ])?\d{2}[ ]?\d{3}\b/mg,
                            defaultCurrency: "EUR"
                        },
                        {
                            name: "fr-BE",
                            VATRegExp: /(BE)?[\. \-]?[01]?\d{3}[\. ]?\d{3}[\. ]?\d{3}/mg,
                            faxOrPhoneRegExp: /((\+?(32))|0)[0-9\-()/ .]{9,}[0-9]/mg,
                            postalCodeRegExp: /[0-9]{4}/mg,
                            defaultCurrency: "EUR"
                        },
                        {
                            name: "fr-FR",
                            VATRegExp: /((FR)[ ]?(([0-9A-Z]?[ ]?){2}([0-9]{3}[ ]?){2})[0-9]{3})|((BE)0?[0-9]{3}[ .]?[0-9]{3}[ .]?[0-9]{3})/mg,
                            faxOrPhoneRegExp: /((\+?(33))|0)[0-9\-()/ .]{9,}[0-9]/mg,
                            postalCodeRegExp: /([ ]|$|^)([Ff]-)?[0-9]{2}[ ]?[0-9]{3}[ ]?/mg,
                            defaultCurrency: "EUR"
                        },
                        {
                            name: "en-GB",
                            VATRegExp: /(GB)?[ ]?(([0-9]{3}[ ]?[0-9]{4}[ ]?[0-9]{2}[ ]?([0-9]{3})?)|((GD)[ ]?[0-4][0-9]{2})|((HA)[ ]?[5-9][0-9]{2}))/mg,
                            faxOrPhoneRegExp: /((\+?(44))|0)[0-9\-()/ .]{9,}[0-9]/mg,
                            postalCodeRegExp: /[a-zA-Z]{1,2}[0-9]{1,2}[a-zA-Z]?[ ]?[0-9][a-zA-Z]{2}/mg,
                            defaultCurrency: "GBP",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-AU",
                            VATRegExp: /\b([0-9]{2}[ -]?[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{3}|[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{2})\b/mg,
                            faxOrPhoneRegExp: /((\+?(61))|0)[0-9\-()/ .]{8,}[0-9]\b/mg,
                            postalCodeRegExp: /\b(08[0-9]{2}|[1-8][0-9]{3})\b/mg,
                            defaultCurrency: "AUD",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-NZ",
                            VATRegExp: /\b[0-9]{2,3}[ -]?[0-9]{3}[ -]?[0-9]{3}\b/mg,
                            faxOrPhoneRegExp: /((\+?(64))|0)[0-9\-()/ .]{7,}[0-9]\b/mg,
                            postalCodeRegExp: /\b[0-9]{4}\b/mg,
                            defaultCurrency: "NZD",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-SG",
                            VATRegExp: /\b((T|M|MR)[-]?)?[0-9]{2}[-]?[0-9]{5}?[/]?[0-9]{2}[-]?[A-Z0-9]\b/mg,
                            faxOrPhoneRegExp: /(\+?65)[0-9\-()/ .]{7,}[0-9]\b/mg,
                            postalCodeRegExp: /\b[0-9]{6}\b/mg,
                            defaultCurrency: "SGD",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-TH",
                            VATRegExp: /\b[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{4}[ -]?[0-9]{3}\b/mg,
                            faxOrPhoneRegExp: /((\+?(66))|0)[0-9\-()/ .]{7,}[0-9]\b/mg,
                            postalCodeRegExp: /\b[0-9]{5}\b/mg,
                            defaultCurrency: "THB",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-PH",
                            VATRegExp: /\b[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{3}[ -]?[0-9]{3}\b/mg,
                            faxOrPhoneRegExp: /((\+?(63))|0)[0-9\-()/ .]{8,}[0-9]\b/mg,
                            postalCodeRegExp: /\b[0-9]{4}\b/mg,
                            defaultCurrency: "PHP",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-ID",
                            VATRegExp: /\b0[0-9][.]?[0-9]{3}[.]?[0-9]{3}[.]?[0-9][-][0-9][.]?[0-9]{3}[.]?00[0-9]\b/mg,
                            faxOrPhoneRegExp: /((\+?(62))|0)[0-9\-()/ .]{5,}[0-9]\b/mg,
                            postalCodeRegExp: /\b[0-9]{5}\b/mg,
                            defaultCurrency: "IDR",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "en-MY",
                            VATRegExp: /[0-9]{12}/mg,
                            faxOrPhoneRegExp: /(\+?60)[0-9\-()/ .]{7,}[0-9]\b/mg,
                            postalCodeRegExp: /[0-9]{6}/mg,
                            defaultCurrency: "MYR",
                            // Used in APv1 engine only
                            totalKeywordsFirstTry: ["TOTAL AMOUNT DUE", "Invoice Total", "Total amount", "AMOUNT DUE", "Total Due", "Gross Value", "Amount to pay", "Total amt", "Balance Due", "Total Value", "Grand Total"],
                            totalKeywordsSecondTry: ["Total", "Amount"],
                            invoiceNumberKeywordsFirstTry: ["Invoice Number", "Invoice [N]°", "Invoice [N]o", "INVOICE [#]", "TAX INVOICE", "Tax Invoice Number", "Inv Number", "Inv [N]o", "CREDIT MEMO", "Credit memo", "CREDIT NOTE", "Credit Note", "Credit note"],
                            invoiceNumberKeywordsSecondTry: ["INVOICE", "Number"],
                            invoiceNumberRegExp: /[A-Z0-9][0-9A-Z./-]*[0-9][0-9A-Z./-]*/mg,
                            invoicePORegExp: /4[59][0-9]{8}/mg
                        },
                        {
                            name: "es-ES",
                            VATRegExp: /(ES)[ ]?(([A-Z]\d{7}[A-Z0-9])|(\d{8}[A-Z]))/mg,
                            faxOrPhoneRegExp: /(T|F)?(\(?(\+?(34))|0)\)?[ ]?([0-9][ ]?){9}/mg,
                            postalCodeRegExp: /[0-9]{2}[ ]?[0-9]{3}/mg,
                            defaultCurrency: "EUR"
                        },
                        {
                            name: "it-IT",
                            VATRegExp: /[0-9]{2}[ ]?[0-9]{3}[ ]?[0-9]{3}[ ]?[0-9]{3}/mg,
                            faxOrPhoneRegExp: /((\+?(39))|0)[0-9\-\(\)/ \.]{9,}[0-9]/mg,
                            postalCodeRegExp: /\b([a-zA-Z]{2}[- ]?)?\d{2}[ ]?\d{3}\b/mg,
                            defaultCurrency: "EUR"
                        },
                        {
                            name: "jp-JP",
                            VATRegExp: /[1-9]\d{12}/mg,
                            faxOrPhoneRegExp: /^(\+81|0)((\d[ ]?-[ ]?\d{4})|(\d{2}[ ]?\-[ ]?\d{3}))[ ]?-[ ]?\d{4}$/mg,
                            postalCodeRegExp: /[0-9]{3}[ ]?\-[ ]?[0-9]{4}/mg,
                            defaultCurrency: "JPY"
                        },
                        {
                            name: "ko-KR",
                            VATRegExp: /((\d{3})[ ]?-[ ]?(\d{2})[ ]?-[ ]?(\d{5}))/mg,
                            faxOrPhoneRegExp: /^(\+82|0)\d{2}[ ]?\-[ ]?(\d{3}|\d{4})[ ]?\-[ ]?\d{4}$/mg,
                            postalCodeRegExp: /([0-2]{1})\d{4}/mg,
                            defaultCurrency: "KRW"
                        },
                        {
                            name: "nl-NL",
                            VATRegExp: /(NL)?[ ]?([0-9][ ]?){9}[ ]?B[ ]?([0-9][ ]?){2}/mg,
                            faxOrPhoneRegExp: /(T|F)?[ ]?((\+?(31))|0)\)?[ ]?[(]?0?[)]?[ ]?[ ]?([0-9]([ ]|[-])?){9}/mg,
                            postalCodeRegExp: /[0-9]{4}[ ]?[ ]?[a-zA-Z]{2}/mg,
                            defaultCurrency: "EUR"
                        }
                    ];
                };
                /**
                 * Give the current document culture or null if no culture set
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetCurrentDocumentCulture
                 * @public
                 * @returns {string} the culture use for the document
                 */
                CultureManager.prototype.GetCurrentDocumentCulture = function () {
                    if (this.activeCulture) {
                        return this.activeCulture.name;
                    }
                    return null;
                };
                /**
                 * Give the current document culture or null if no culture set
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#SetCurrentCulture
                 * @public
                 * @param {string} culture the culture to activate
                 */
                CultureManager.prototype.SetCurrentCulture = function (culture) {
                    this.activeCulture = this.m_culturesInfo[culture];
                };
                /**
                 * Give the current document culture or null if no culture set
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#SetCurrentDocumentCulture
                 * @public
                 * @param {string} culture the culture of the current document, if not available the first available on will be chosen.
                 */
                CultureManager.prototype.SetCurrentDocumentCulture = function (culture) {
                    if (this.m_availableCultures.indexOf(culture) !== -1) {
                        this.SetCurrentCulture(culture);
                    }
                    else {
                        this.SetCurrentCulture(this.m_availableCultures[0]);
                        Log.Warn("Chosen document culture is not among the available ones");
                    }
                };
                /**
                 * Set the list of available culture for the cyrrent document.
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#SetAvailableCultures
                 * @public
                 * @param {string|string[]} ac the culture or the list of culture available for the current document.
                 */
                CultureManager.prototype.SetAvailableCultures = function (ac) {
                    this.m_availableCultures = [];
                    if (Object.prototype.toString.call(ac) === "[object Array]" && ac.length > 0) {
                        this.m_availableCultures = ac;
                    }
                    else if (typeof ac === "string") {
                        this.m_availableCultures[0] = ac;
                    }
                    else {
                        this.m_availableCultures[0] = "en-US";
                    }
                    if (this.m_availableCultures.indexOf(this.GetCurrentDocumentCulture()) === -1) {
                        this.SetCurrentCulture(this.m_availableCultures[0]);
                    }
                };
                /**
                 * Allow to access to any property of the active CultureInfo
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetProperty
                 * @public
                 * @param {string} propertyName The name of the property to access on the activeCultureInfo (case-sensitive)
                 * @returns {string|number|boolean} The value of the property or null if the property is missing
                 */
                CultureManager.prototype.GetProperty = function (propertyName) {
                    if (this.activeCulture && propertyName) {
                        return this.activeCulture[propertyName] || null;
                    }
                    return null;
                };
                /**
                 * Returns the name of the active CultureInfo
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetCulture
                 * @public
                 * @returns {string} cuture name
                 */
                CultureManager.prototype.GetCulture = function () {
                    return this.GetProperty("name");
                };
                /**
                 * Returns the country of the active CultureInfo
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetCountry
                 * @public
                 * @returns {string} cuture country
                 */
                CultureManager.prototype.GetCountry = function () {
                    var culture = this.GetCulture();
                    if (culture) {
                        var country = culture.slice(culture.indexOf("-") + 1);
                        return country.toUpperCase();
                    }
                    return null;
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetVATRegExp
                 * @public
                 * @returns {RegExp}
                 */
                CultureManager.prototype.GetVATRegExp = function () {
                    return this.GetProperty("VATRegExp");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetFaxOrPhoneRegExp
                 * @public
                 * @returns {RegExp}
                 */
                CultureManager.prototype.GetFaxOrPhoneRegExp = function () {
                    return this.GetProperty("faxOrPhoneRegExp");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetPostalCodeRegExp
                 * @public
                 * @returns {RegExp}
                 */
                CultureManager.prototype.GetPostalCodeRegExp = function () {
                    return this.GetProperty("postalCodeRegExp");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetTotalKeywordsFirstTry
                 * @public
                 * @returns {string[]}
                 */
                CultureManager.prototype.GetTotalKeywordsFirstTry = function () {
                    return this.GetProperty("totalKeywordsFirstTry");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetTotalKeywordsSecondTry
                 * @public
                 * @returns {string[]}
                 */
                CultureManager.prototype.GetTotalKeywordsSecondTry = function () {
                    return this.GetProperty("totalKeywordsSecondTry");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetInvoiceNumberKeywordsFirstTry
                 * @public
                 * @returns {string[]}
                 */
                CultureManager.prototype.GetInvoiceNumberKeywordsFirstTry = function () {
                    return this.GetProperty("invoiceNumberKeywordsFirstTry");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetInvoiceNumberKeywordsSecondTry
                 * @public
                 * @returns {string[]}
                 */
                CultureManager.prototype.GetInvoiceNumberKeywordsSecondTry = function () {
                    return this.GetProperty("invoiceNumberKeywordsSecondTry");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetInvoiceNumberRegExp
                 * @public
                 * @returns {RegExp}
                 */
                CultureManager.prototype.GetInvoiceNumberRegExp = function () {
                    return this.GetProperty("invoiceNumberRegExp");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetInvoicePORegExp
                 * @public
                 * @returns {RegExp}
                 */
                CultureManager.prototype.GetInvoicePORegExp = function () {
                    return this.GetProperty("invoicePORegExp");
                };
                /**
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#GetDefaultCurrency
                 * @public
                 * @returns {string}
                 */
                CultureManager.prototype.GetDefaultCurrency = function () {
                    var extDefaultCurrency = Variable.GetValueAsString("DefaultCurrency");
                    return extDefaultCurrency ? extDefaultCurrency : this.GetProperty("defaultCurrency");
                };
                /**
                 * Normalize a phone or fax number
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#NormalizeTelInternal
                 * @private
                 * @param {string} sTel the phone number
                 * @param {string} ext the international prefix
                 * @param {number} len the length of the phone number
                 * @returns {string} the normalized phone number
                 */
                CultureManager.NormalizeTelInternal = function (sTel, ext, len) {
                    if (sTel.indexOf("+") === 0 || sTel.indexOf("0") === 0) {
                        sTel = sTel.substr(1);
                    }
                    if (sTel.indexOf(ext) === 0 && sTel.length > len) {
                        sTel = sTel.substr(ext.length);
                    }
                    if (sTel.indexOf("0") === 0) {
                        sTel = sTel.substr(1);
                    }
                    if (sTel.length !== len) {
                        return null;
                    }
                    sTel = "+" + ext + " " + sTel;
                    return sTel;
                };
                /**
                 * Normalize a phone or fax number in the current document culture
                 * @method Lib.P2P.FirstTimeRecognition_Vendor.CultureManager#NormalizeTel
                 * @public
                 * @param {string} sTel the phone number
                 * @returns {string} the normalized phone number
                 */
                CultureManager.prototype.NormalizeTel = function (sTel) {
                    var currentCulture = this.GetCurrentDocumentCulture();
                    if (currentCulture === "fr-FR") {
                        return CultureManager.NormalizeTelInternal(sTel, "33", 9);
                    }
                    else if (currentCulture === "en-GB") {
                        return CultureManager.NormalizeTelInternal(sTel, "44", 10);
                    }
                    else if (currentCulture === "en-US") {
                        return CultureManager.NormalizeTelInternal(sTel, "1", 10);
                    }
                    return sTel;
                };
                return CultureManager;
            }());
            // Define search options
            var optionsDefaultValues = {
                searchIBAN: true,
                searchVAT: true,
                searchPhoneOrFax: true,
                searchAddress: true,
                ownPhone: "",
                ownFax: "",
                ownVAT: ""
            };
            var options = Sys.Helpers.Clone(optionsDefaultValues);
            var lookupResults = {};
            var countriesCheckForIBAN = {};
            function Init() {
                options = Sys.Helpers.Clone(optionsDefaultValues);
                // Define search options
                if (Sys.Parameters.GetInstance("AP").GetParameter("FTRVendorOnIdentifiersOnly") === "1") {
                    options.searchAddress = false;
                }
                var companyCode = Sys.Parameters.GetInstance("AP").GetParameter("CompanyCode");
                if (companyCode) {
                    var filter = "CompanyCode__=" + companyCode;
                    var dbresult = Lib.P2P.FirstTimeRecognition_Vendor.queryDbFirstRow("PhoneNumber__,FaxNumber__,VATNumber__", "PurchasingCompanycodes__", filter);
                    if (dbresult) {
                        options.ownPhone = dbresult.GetValue_String("PhoneNumber__", 0);
                        options.ownFax = dbresult.GetValue_String("FaxNumber__", 0);
                        options.ownVAT = dbresult.GetValue_String("VATNumber__", 0);
                    }
                }
                lookupResults = {};
                countriesCheckForIBAN = {};
            }
            FirstTimeRecognition_Vendor.Init = Init;
            // #region recognition engine
            function EnableDebug(enable) {
                g_debug = enable;
            }
            FirstTimeRecognition_Vendor.EnableDebug = EnableDebug;
            function GetLookupResults() {
                return lookupResults;
            }
            FirstTimeRecognition_Vendor.GetLookupResults = GetLookupResults;
            FirstTimeRecognition_Vendor.cultureManager = new CultureManager();
            function GetOptions() {
                return options;
            }
            FirstTimeRecognition_Vendor.GetOptions = GetOptions;
            // #endregion recognition engine
            // #region preview interaction
            /**
            * Select the best Area with the most matching words
            * @method GetBestMatchedArea
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {MatchingInfo[]} Array of matching information
            * @returns {MatchingInfo} the best selected area
            */
            function GetBestMatchedArea(areas) {
                var bestMatchIndex = -1;
                var maxMatchingWords = 0;
                for (var i = 0; i < areas.length; i++) {
                    var currentBlock = areas[i];
                    if (currentBlock.matchingWords > maxMatchingWords) {
                        maxMatchingWords = currentBlock.matchingWords;
                        bestMatchIndex = i;
                    }
                }
                return bestMatchIndex !== -1 ? areas[bestMatchIndex] : null;
            }
            FirstTimeRecognition_Vendor.GetBestMatchedArea = GetBestMatchedArea;
            // #endregion preview interaction
            // #region text helpers
            /**
            * Returns The top right word area from a given rectabgle
            * @method findTopRightWordInRect
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {number} iPage The current page number
            * @param {number} __iResX The initial X position
            * @param {number} __iResY The initial Y position
            * @param {number} __iResW the rectangle width
            * @returns {Area} The most top right word from the given position
            */
            function findTopRightWordInRect(iPage, __iResX, __iResY, __iResW) {
                var __areaCurrentWord = Document.GetWord(iPage, __iResX + __iResW, __iResY);
                var __iBreak = 0;
                while (__areaCurrentWord.x > __iResX + __iResW && __iBreak < 3) {
                    var __iCurX = __areaCurrentWord.x + __areaCurrentWord.width;
                    var tmpAreaPrevWord = Document.GetWord(iPage, __iCurX, __iResY);
                    var __areaPrevWord = tmpAreaPrevWord.GetPreviousWord();
                    if (__areaPrevWord.x === 0 || __areaPrevWord.x + __areaPrevWord.w < __iCurX) {
                        // next line
                        break;
                    }
                    __areaCurrentWord = __areaPrevWord;
                    __iBreak++;
                }
                return __areaCurrentWord;
            }
            FirstTimeRecognition_Vendor.findTopRightWordInRect = findTopRightWordInRect;
            /**
            * Expand the area to the right
            * @method enlargeToTheRight
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {number} iPage The current page number
            * @param {number} __iResX The initial X position
            * @param {number} __iResW The initial width
            * @param {number} __iResH The initial height
            * @param {number} maxX the maximum allowed to expand the area
            * @returns {Rect} The new area enclosing words on the right next to the initial area
            */
            function enlargeToTheRight(iPage, __iResX, __iResY, __iResW, __iResH, maxX) {
                var __areaCurrentWord = findTopRightWordInRect(iPage, __iResX, __iResY, __iResW);
                if (__areaCurrentWord.x !== 0) {
                    while (1) {
                        var __iOldX = __areaCurrentWord.x + __areaCurrentWord.width;
                        var tmpAreaNextWord = Document.GetWord(iPage, __iOldX, __iResY);
                        var __areaNextWord = tmpAreaNextWord.GetNextWord();
                        var __iNewX = __areaNextWord.x;
                        if (__iNewX !== 0 && __iNewX > __iOldX && __iNewX - __iOldX < maxX) {
                            traceDebugAreas("enlarged to right: __iNewX =" + __iNewX + "__iOldX = " + __iOldX);
                            __iResW = __iNewX + __areaNextWord.width - __iResX;
                            var __areaNewCurrentWord = Document.GetWord(iPage, __iResX + __iResW, __iResY);
                            if (__areaNewCurrentWord.x === __areaCurrentWord.x) {
                                traceDebugAreas("Could not advance to next word to the right. Stop here.");
                                break;
                            }
                            __areaCurrentWord = __areaNewCurrentWord;
                        }
                        else {
                            // Ensure the whole word lies within our rectangle
                            if (__iResX + __iResW < __iOldX) {
                                traceDebugAreas("current word not included into current rect. Forcing it.");
                                __iResW = __iOldX - __iResX;
                            }
                            break;
                        }
                    }
                    traceDebugAreas("1-resX = " + __iResX + " resW = " + __iResW);
                }
                return { x: __iResX, y: __iResY, width: __iResW, height: __iResH };
            }
            FirstTimeRecognition_Vendor.enlargeToTheRight = enlargeToTheRight;
            /**
            * Returns The top left word area from a given position
            * @method findTopLeftWordInRect
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {number} iPage The current page number
            * @param {number} __iResX The initial X position
            * @param {number} __iResY The initial Y position
            * @returns {Area} The most top left word from the given position
            */
            function findTopLeftWordInRect(iPage, __iResX, __iResY) {
                var __areaCurrentWord = Document.GetWord(iPage, __iResX, __iResY);
                var __iBreak = 0;
                while (__areaCurrentWord.x > __iResX && __iBreak < 3) {
                    var tmpAreaNextWord = Document.GetWord(iPage, __areaCurrentWord.x, __iResY);
                    var __areaNextWord = tmpAreaNextWord.GetNextWord();
                    if (__areaNextWord.x === 0 ||
                        (__areaNextWord.x < __areaCurrentWord.x ||
                            __areaNextWord.y > __areaCurrentWord.y + __areaCurrentWord.height)) {
                        // Next line
                        break;
                    }
                    __areaCurrentWord = __areaNextWord;
                    __iBreak++;
                }
                return __areaCurrentWord;
            }
            FirstTimeRecognition_Vendor.findTopLeftWordInRect = findTopLeftWordInRect;
            /**
            * Expand the area to the left
            * @method enlargeToTheLeft
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {number} iPage The current page number
            * @param {number} __iResX The initial X position
            * @param {number} __iResW The initial width
            * @param {number} __iResH The initial height
            * @param {number} maxX the maximum allowed to expand the area
            * @returns {Rect} The new area enclosing words on the left next to the initial area
            */
            function enlargeToTheLeft(iPage, __iResX, __iResY, __iResW, __iResH, maxX) {
                var __areaCurrentWord = findTopLeftWordInRect(iPage, __iResX, __iResY);
                if (__areaCurrentWord.x !== 0) {
                    traceDebugAreas("Before Loop 2");
                    while (1) {
                        var __areaPrevWord = __areaCurrentWord.GetPreviousWord();
                        var __iNewX = __areaPrevWord.x + __areaPrevWord.width;
                        if (__iNewX !== 0 && __iNewX < __iResX && __iResX - __iNewX < maxX) {
                            traceDebugAreas("enlarged to left: __iNewX = " + __iNewX + " iResX = " + __iResX);
                            __iResW += __iResX - __areaPrevWord.x;
                            __iResX = __areaPrevWord.x;
                            __areaCurrentWord = __areaPrevWord;
                        }
                        else {
                            break;
                        }
                    }
                }
                return { x: __iResX, y: __iResY, width: __iResW, height: __iResH };
            }
            FirstTimeRecognition_Vendor.enlargeToTheLeft = enlargeToTheLeft;
            /**
            * Enlarge the area to enclose around words in the resulting area
            * @method getTextAroundWord
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {number} iPage The current page number
            * @param {number} __iResX The initial X position
            * @param {number} __iResW The initial width
            * @param {number} __iResH The initial height
            * @returns {Area} The new area enclosing words next to the initial area
            */
            function getTextAroundWord(iPage, __iResX, __iResY, __iResW, __iResH) {
                // Max Block address lines
                var maxLines = 7;
                // Tolerances when enlarging block address between two words
                // maxX ~= 0.75cm
                var maxX = Document.GetPageResolutionX(iPage) * 0.75 / 2.54;
                // maxY ~= 0.5cm
                var maxY = Document.GetPageResolutionY(iPage) * 0.5 / 2.54;
                traceDebugAreas("__maxX = " + maxX + "__maxY = " + maxY);
                var nbLines = 0;
                while (nbLines < maxLines) {
                    var enlargeRight = enlargeToTheRight(iPage, __iResX, __iResY, __iResW, __iResH, maxX);
                    var enlargeLeft = enlargeToTheLeft(iPage, enlargeRight.x, enlargeRight.y, enlargeRight.width, enlargeRight.height, maxX);
                    __iResX = enlargeLeft.x;
                    __iResY = enlargeLeft.y;
                    __iResW = enlargeLeft.width;
                    __iResH = enlargeLeft.height;
                    traceDebugAreas("2-resX = " + __iResX + "  resW = " + __iResW + "  __iResY = " + __iResY + "  __iResH = " + __iResH);
                    // enlrge to the top: look for the nearest word on the left, middle and right of the top of the block address (this will solve text alignement problem)
                    var prevLine = void 0;
                    var zoneBlockAdressToLeft = Document.GetWord(iPage, __iResX, __iResY);
                    var zoneBlockAdressToMiddle = Document.GetWord(iPage, __iResX + (__iResW / 2), __iResY);
                    var zoneBlockAdressToRight = Document.GetWord(iPage, __iResX + __iResW, __iResY);
                    var prevLineLeft = zoneBlockAdressToLeft.GetWordAbove();
                    var prevLineMiddle = zoneBlockAdressToMiddle.GetWordAbove();
                    var prevLineRight = zoneBlockAdressToRight.GetWordAbove();
                    var iYLeft = prevLineLeft.y + prevLineLeft.height;
                    var iYMiddle = prevLineMiddle.y + prevLineMiddle.height;
                    var iYRight = prevLineRight.y + prevLineRight.height;
                    if (iYLeft >= __iResY) {
                        iYLeft = 0;
                    }
                    if (iYMiddle >= __iResY) {
                        iYMiddle = 0;
                    }
                    if (iYRight >= __iResY) {
                        iYRight = 0;
                    }
                    // Take the closest
                    if (iYLeft > iYMiddle) {
                        if (iYLeft > iYRight) {
                            prevLine = prevLineLeft;
                        }
                        else {
                            prevLine = prevLineRight;
                        }
                    }
                    else if (iYMiddle > iYRight) {
                        prevLine = prevLineMiddle;
                    }
                    else {
                        prevLine = prevLineRight;
                    }
                    var __iNewY = prevLine.y + prevLine.height;
                    if (__iNewY !== 0 && __iNewY < __iResY && __iResY - __iNewY < maxY) {
                        __iResH += __iResY - prevLine.y;
                        __iResY = prevLine.y;
                        traceDebugAreas("enlarged to top, newResH = " + __iResH);
                        nbLines++;
                    }
                    else {
                        traceDebugAreas("Stopped enlarging to top, __iNewY = " + __iNewY + " __iResY = " + __iResY);
                        break;
                    }
                }
                traceDebugAreas("final area: __iResX = " + __iResX + " __iResY = " + __iResY + " __iResW = " + __iResW + " __iResW = " + __iResW);
                var zoneBlockAdress = Document.GetArea(iPage, __iResX, __iResY, __iResW, __iResH);
                return zoneBlockAdress;
            }
            FirstTimeRecognition_Vendor.getTextAroundWord = getTextAroundWord;
            /**
            * Filter a string from a string of forbidden characters
            * @method spanExcluding
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {string} sString The input string to clean
            * @param {string} sSpan Forbidden characters
            * @returns {string} The input string filtered excluding all characters whithin the sSpan string
            */
            function spanExcluding(sString, sSpan) {
                var sRes = "";
                for (var i = 0; i < sString.length; i++) {
                    if (sSpan.indexOf(sString[i]) === -1) {
                        sRes += sString[i];
                    }
                }
                return sRes;
            }
            FirstTimeRecognition_Vendor.spanExcluding = spanExcluding;
            /**
            * Filter a string from a string of allowed characters
            * @method spanIncluding
            * @memberof Lib.P2P.FirstTimeRecognition_Vendor
            * @param {string} sString The input string to clean
            * @param {string} sSpan Allowed characters
            * @returns {string} The input string filtered with only allowed characters from sSpan string
            */
            function spanIncluding(sString, sSpan) {
                var sRes = "";
                for (var i = 0; i < sString.length; i++) {
                    if (sSpan.indexOf(sString[i]) !== -1) {
                        sRes += sString[i];
                    }
                }
                return sRes;
            }
            FirstTimeRecognition_Vendor.spanIncluding = spanIncluding;
            /**
             * @namespace Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
             */
            FirstTimeRecognition_Vendor.QueryHelper = {
                /**
                 * Return FTS Query for specified words list
                 * @method DoFTSQuery
                 * @param {string[]} arrayWordToFind words list
                 * @param {string} sTableName Table to look into
                 * @param {string[]} arrayObjMatchingInfo block in the document used to build words list
                 * @param {string[]} validateFunc Check if the record is valid regarding to the text found
                 *		Parameters -> Current record to evaluate, block array
                 * @param {boolean} useMultipleRecords If true, return every matching record. Otherwise return first match
                 * @param {string} companyCode If specified a filter on companyCode__ is added to the request
                 * @returns {xVars|xVars[]} a record vars or a list of record vars depending on useMultipleRecords parameter
                 */
                DoFTSQuery: function (arrayWordToFind, sTableName, arrayObjMatchingInfo, validateFunc, useMultipleRecords, companyCode) {
                    return this.BuildAndExecuteQuery(arrayWordToFind, sTableName, null, arrayObjMatchingInfo, validateFunc, useMultipleRecords, companyCode);
                },
                /**
                 * Execute a Query to find all matching records
                 * @method DoQuery
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string[]} ArrayWordToFind words list
                 * @param {string} sTableName Table to look into
                 * @param {string[]} columnsToSearch Column or columns to look into
                 * @param {string[]} arrayObjMatchingInfo block in the document used to build words list
                 * @param {callback} validateFunc Check if the record is valid regarding to the text found
                 * 	Parameters -> Current record to evaluate, block array
                 * @param {boolean} useMultipleRecords If true, return every matching record. Otherwise return first match
                 * @param {string} companyCode If specified a filter on companyCode__ is added to the request
                 * @returns {xVars|xVars[]} a record vars or a list of record vars depending on useMultipleRecords parameter
                 */
                DoQuery: function (arrayWordToFind, sTableName, columnsToSearch, arrayObjMatchingInfo, validateFunc, useMultipleRecords, companyCode) {
                    return this.BuildAndExecuteQuery(arrayWordToFind, sTableName, columnsToSearch, arrayObjMatchingInfo, validateFunc, useMultipleRecords, companyCode);
                },
                /**
                 * Build and execute a FTS query or a simple Query to find all matching records
                 * @method BuildAndExecuteQuery
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string[]} ArrayWordToFind words list
                 * @param {string} sTableName Table to look into
                 * @param {string[]} columnsToSearch Column or columns to look into
                 * @param {string[]} arrayObjMatchingInfo block in the document used to build words list
                 * @param {callback} validateFunc Check if the record is valid regarding to the text found
                 * 	Parameters -> Current record to evaluate, block array
                 * @param {boolean} useMultipleRecords If true, return every matching record. Otherwise return first match
                 * @param {string} companyCode If specified a filter on companyCode__ is added to the request
                 * @returns {xVars|xVars[]} a record vars or a list of record vars depending on useMultipleRecords parameter
                 */
                BuildAndExecuteQuery: function (arrayWordToFind, sTableName, columnsToSearch, arrayObjMatchingInfo, validateFunc, useMultipleRecords, companyCode) {
                    // Validate parameters
                    if (!arrayWordToFind || arrayWordToFind.length === 0) {
                        return null;
                    }
                    var filter;
                    if (!columnsToSearch) {
                        var sWordsToSearch = this.EscapeWordToFind(arrayWordToFind);
                        if (sWordsToSearch === "") {
                            Log.Warn("No word to search");
                            return null;
                        }
                        filter = this.BuildFTSFilter(sWordsToSearch, companyCode);
                    }
                    else {
                        filter = this.BuildFilter(columnsToSearch, arrayWordToFind, companyCode);
                    }
                    return this.ExecuteQuery(sTableName, filter, arrayObjMatchingInfo, useMultipleRecords, validateFunc);
                },
                /**
                 * Execute a FTS query or a simple Query to find all matching records
                 * @method ExecuteQuery
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string} sTableName Table to look into
                 * @param {string} sFilter Filter to use
                 * @param {string[]} arrayObjMatchingInfo block in the document used to build words list
                 * @param {boolean} useMultipleRecords If true, return every matching record. Otherwise return first match
                 * @param {callback} validateFunc Check if the record is valid regarding to the text found
                 * @returns {xVars|xVars[]} a record vars or a list of record vars depending on useMultipleRecords parameter
                 */
                ExecuteQuery: function (sTableName, sFilter, arrayObjMatchingInfo, useMultipleRecords, validateFunc) {
                    Query.Reset();
                    Query.SetAttributesList("*");
                    Query.SetSpecificTable(sTableName);
                    Query.SetFilter(sFilter);
                    if (Query.MoveFirst()) {
                        var queryResult = Query.MoveNextRecord();
                        var recordList = [];
                        while (queryResult) {
                            var record = this.ValidateCurrentRecord(queryResult, arrayObjMatchingInfo, validateFunc);
                            if (record) {
                                if (!useMultipleRecords) {
                                    return record;
                                }
                                recordList.push(record);
                            }
                            queryResult = Query.MoveNextRecord();
                        }
                        if (recordList.length > 0) {
                            return recordList;
                        }
                    }
                    return null;
                },
                /**
                 * @method ValidateCurrentRecord
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {xRecord} queryResult the record to validate
                 * @param {string[]} arrayObjMatchingInfo block in the document used to build words list
                 * @param {callback} [validateFunc] Check if the record is valid regarding to the text found
                 * @returns {xVars} a record vars if validated
                 */
                ValidateCurrentRecord: function (queryResult, arrayObjMatchingInfo, validateFunc) {
                    var result = null;
                    var record = queryResult.GetVars();
                    if (record) {
                        // Validate this matching record
                        if (!Sys.Helpers.IsFunction(validateFunc) || validateFunc(record, arrayObjMatchingInfo)) {
                            result = record;
                        }
                        else {
                            traceDebug("Discarding invalid value");
                        }
                    }
                    return result;
                },
                /**
                 * @method EscapeWordToFind
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string[]} arrayWordToFind
                 * @returns {string} an escaped string
                 */
                EscapeWordToFind: function (arrayWordToFind) {
                    var sWordsToSearch = "";
                    for (var i = 0; i < arrayWordToFind.length; i++) {
                        if (arrayWordToFind[i].length >= 2 && sWordsToSearch.indexOf("\"" + arrayWordToFind[i] + "\"") === -1) {
                            var wordEscaped = arrayWordToFind[i].replace("(", "\\(");
                            wordEscaped = wordEscaped.replace(")", "\\)");
                            wordEscaped = wordEscaped.replace("*", "\\*");
                            if (sWordsToSearch[0] === '"') {
                                sWordsToSearch += ", ";
                            }
                            sWordsToSearch += "\"" + wordEscaped + "\"";
                        }
                        else {
                            traceDebug("Discarding duplicate or small value:" + arrayWordToFind[i]);
                        }
                    }
                    return sWordsToSearch;
                },
                /**
                 * Construct the filter for the Query
                 * @method BuildFilter
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string|string[]} columnsToSearch Column or columns to look into
                 * @param {string[]} ArrayWordToFind List the keyword to find
                 * @param {string|string[]} companyCode Name of the CompanyCode to filter on
                 * @return {string} the filter to use in the query
                 */
                BuildFilter: function (columnsToSearch, arrayWordToFind, companyCode) {
                    var filter = "";
                    var checkOptions = Boolean(arrayWordToFind && columnsToSearch);
                    if (checkOptions) {
                        checkOptions = arrayWordToFind.length > 0 && columnsToSearch.length > 0 && arrayWordToFind instanceof Array;
                    }
                    if (checkOptions && companyCode) {
                        checkOptions = !(companyCode instanceof Array) && companyCode.length > 0;
                    }
                    if (checkOptions) {
                        var maxWord = arrayWordToFind.length;
                        var columns = [];
                        if (columnsToSearch instanceof Array) {
                            columns = columnsToSearch;
                        }
                        else {
                            columns.push(columnsToSearch);
                        }
                        var maxColumn = columns.length;
                        for (var iWord = 0; iWord < maxWord; iWord++) {
                            for (var iColumn = 0; iColumn < maxColumn; iColumn++) {
                                if (columns[iColumn].length > 0 && arrayWordToFind[iWord].length > 0) {
                                    filter += "(" + columns[iColumn] + "=" + arrayWordToFind[iWord] + ")";
                                }
                                else {
                                    filter = "invalid filter";
                                    break;
                                }
                            }
                        }
                        if (filter && filter !== "invalid filter") {
                            if (maxWord > 1 || maxColumn > 1) {
                                filter = "(|" + filter + ")";
                            }
                            if (companyCode && companyCode.length > 0) {
                                filter = filter.AddCompanyCodeFilter(companyCode);
                            }
                        }
                    }
                    else {
                        filter = "invalid filter";
                        Log.Warn("QueryHelper.BuildFilter function call with wrong arguments");
                    }
                    if (filter === "invalid filter") {
                        Log.Warn("QueryHelper.BuildFilter function call with wrong arguments");
                    }
                    traceDebug("Query Filter = " + filter);
                    return filter;
                },
                /**
                 * Construct the filter for the FTS Query
                 * @method BuildFilter
                 * @memberof Lib.P2P.FirstTimeRecognition_Vendor.QueryHelper
                 * @param {string} sWordsToSearch words to look for
                 * @param {string} companyCode Name of the CompanyCode to filter on
                 * @return {string} the filter to use in the query
                 */
                BuildFTSFilter: function (sWordsToSearch, companyCode) {
                    var sFTSFilter = "$Content$%=CONTAINS('ISABOUT(";
                    sFTSFilter += sWordsToSearch;
                    sFTSFilter += ")')";
                    if (companyCode && companyCode.length > 0) {
                        sFTSFilter = sFTSFilter.AddCompanyCodeFilter(companyCode);
                    }
                    traceDebug("Query Filter = " + sFTSFilter);
                    return sFTSFilter;
                }
            };
            function queryDbFirstRow(columns, tableName, filter) {
                var item;
                Query.Reset();
                Query.SetAttributesList(columns);
                Query.SetSpecificTable(tableName);
                Query.SetFilter(filter);
                if (Query.MoveFirst()) {
                    item = Query.MoveNextRecord();
                    if (item) {
                        return item.GetVars();
                    }
                }
                return null;
            }
            FirstTimeRecognition_Vendor.queryDbFirstRow = queryDbFirstRow;
            // #endregion fts and query helpers
            // #region lookup functions
            /* Helper for vendorLookupFTSIBAN
                *	Look for some IBAN numbers
                *	and perform a Full Text Search Query in the AP - Vendor__ table
                */
            function vendorLookupFTSIBAN(companyCode, invoiceDocument) {
                var result = { dbresult: null, areas: [] };
                var countryCode = FirstTimeRecognition_Vendor.cultureManager.GetCountry();
                if (countriesCheckForIBAN[countryCode]) {
                    // check already done without any vendor found
                    return result;
                }
                countriesCheckForIBAN[countryCode] = true;
                var IBANRegExp = Sys.Helpers.Iban.GetValidationRegexp(countryCode, "img");
                if (!IBANRegExp) {
                    return result;
                }
                function getVendorInfosFromVendorNumber(vendorNumber) {
                    if (vendorNumber) {
                        var filter = "Number__=" + vendorNumber;
                        var dbresult = Lib.P2P.FirstTimeRecognition_Vendor.queryDbFirstRow("*", "AP - Vendors__", filter.AddCompanyCodeFilter(companyCode));
                        if (dbresult) {
                            result.dbresult = dbresult;
                        }
                    }
                }
                var documentWordsList = [];
                for (var iPage = 0; iPage < Document.GetPageCount(); iPage++) {
                    var sPageText = Document.GetArea(iPage).toString();
                    var arrayIBAN = IBANRegExp.exec(sPageText);
                    while (arrayIBAN) {
                        traceDebug("Found IBAN:" + arrayIBAN[0]);
                        var sWord = spanIncluding(arrayIBAN[0], "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                        documentWordsList.push(sWord);
                        var objMatchingInfo = {
                            zone: Document.SearchString(arrayIBAN[0], iPage, true, false)[0],
                            matchingWords: 1,
                            value: sWord
                        };
                        result.areas.push(objMatchingInfo);
                        arrayIBAN = IBANRegExp.exec(sPageText);
                    }
                }
                if (result.areas.length === 0) {
                    return result;
                }
                if (invoiceDocument && Sys.Helpers.IsFunction(invoiceDocument.GetFirstVendorNumberFromIBANS)) {
                    invoiceDocument.GetFirstVendorNumberFromIBANS({ companyCode: companyCode, ibans: documentWordsList }, getVendorInfosFromVendorNumber);
                }
                else {
                    var queryResult = FirstTimeRecognition_Vendor.QueryHelper.DoQuery(documentWordsList, "AP - Bank details__", "IBAN__", result.areas, null, false, companyCode);
                    if (queryResult) {
                        var vendorNumber = queryResult.GetValue_String("VendorNumber__", 0);
                        getVendorInfosFromVendorNumber(vendorNumber);
                    }
                }
                return result;
            }
            FirstTimeRecognition_Vendor.vendorLookupFTSIBAN = vendorLookupFTSIBAN;
            /* Helper for vendorLookupFTSVAT
                *	Look for some VAT numbers
                *	and perform a Full Text Search Query in the AP - Vendor__ table
                */
            function vendorLookupFTSVAT(companyCode) {
                var VATCodeRegExp = FirstTimeRecognition_Vendor.cultureManager.GetProperty("VATRegExp");
                var documentWordsList = [];
                var result = { dbresult: null, areas: [] };
                if (!VATCodeRegExp) {
                    return result;
                }
                var ownNormalizedVAT = spanIncluding(options.ownVAT, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                for (var iPage = 0; iPage < Document.GetPageCount(); iPage++) {
                    var sPageText = Document.GetArea(iPage).toString();
                    var arrayVATCode = VATCodeRegExp.exec(sPageText);
                    while (arrayVATCode) {
                        traceDebug("Found VATCode:" + arrayVATCode[0]);
                        var sWord = spanIncluding(arrayVATCode[0], "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                        if (sWord !== ownNormalizedVAT) {
                            documentWordsList.push(sWord);
                            var objMatchingInfo = {
                                zone: Document.SearchString(arrayVATCode[0], iPage, true, false)[0],
                                columns: ["VATNumber__"],
                                value: sWord
                            };
                            result.areas.push(objMatchingInfo);
                        }
                        arrayVATCode = VATCodeRegExp.exec(sPageText);
                    }
                }
                var queryResult = FirstTimeRecognition_Vendor.QueryHelper.DoQuery(documentWordsList, "AP - Vendors__", "VATNumber__", result.areas, validateVendorRecordForVATPhoneOrFax, false, companyCode);
                if (queryResult) {
                    result.dbresult = queryResult;
                }
                return result;
            }
            FirstTimeRecognition_Vendor.vendorLookupFTSVAT = vendorLookupFTSVAT;
            /* Helper for vendorLookupFTSPhoneOrFax
                *	Look for some Telephone and Fax
                *	and perform a Full Text Search Query in the AP - Vendor__ table
                */
            function vendorLookupFTSPhoneOrFax(companyCode) {
                var faxOrTelRegExp = FirstTimeRecognition_Vendor.cultureManager.GetProperty("faxOrPhoneRegExp");
                var documentWordsList = [];
                var result = {
                    dbresult: null,
                    areas: []
                };
                if (!faxOrTelRegExp) {
                    return result;
                }
                var ownNormalizedPhone = FirstTimeRecognition_Vendor.cultureManager.NormalizeTel(spanIncluding(options.ownPhone, "+0123456789"));
                var ownNormalizedFax = FirstTimeRecognition_Vendor.cultureManager.NormalizeTel(spanIncluding(options.ownFax, "+0123456789"));
                for (var iPage = 0; iPage < Document.GetPageCount(); iPage++) {
                    // If nothing found, look for Tel or Fax
                    var sPageText = Document.GetArea(iPage).toString();
                    var arrayTelOrFax = faxOrTelRegExp.exec(sPageText);
                    while (arrayTelOrFax) {
                        traceDebug("Found Tel or Fax:" + arrayTelOrFax[0]);
                        var sWord = FirstTimeRecognition_Vendor.cultureManager.NormalizeTel(spanIncluding(arrayTelOrFax[0], "+0123456789"));
                        if (sWord && (sWord !== ownNormalizedPhone) && (sWord !== ownNormalizedFax)) {
                            documentWordsList.push(sWord);
                            var objMatchingInfo = {
                                zone: Document.SearchString(arrayTelOrFax[0], iPage, true, false)[0],
                                columns: ["PhoneNumber__", "FaxNumber__"],
                                value: sWord
                            };
                            result.areas.push(objMatchingInfo);
                        }
                        arrayTelOrFax = faxOrTelRegExp.exec(sPageText);
                    }
                }
                var queryResult = FirstTimeRecognition_Vendor.QueryHelper.DoQuery(documentWordsList, "AP - Vendors__", ["PhoneNumber__", "FaxNumber__"], result.areas, validateVendorRecordForVATPhoneOrFax, false, companyCode);
                if (queryResult) {
                    result.dbresult = queryResult;
                }
                return result;
            }
            FirstTimeRecognition_Vendor.vendorLookupFTSPhoneOrFax = vendorLookupFTSPhoneOrFax;
            /* Helper for vendorLookupFTSPostal
                *	Look for some Postal Codes using a regular expression,
                *	and enlarge the zone around the Postal Code to extract a Block Address
                *	These Block Addresses will be used to perform a Full Text Search Query in the AP - Vendor__ table
                */
            function vendorLookupFTSPostal(companyCode) {
                var result = { dbresult: null, areas: [] };
                if (!FirstTimeRecognition_Vendor.cultureManager.GetProperty("postalCodeRegExp")) {
                    return result;
                }
                var addressObject = lookupBlockAddressInDocument();
                result.areas = addressObject.blockAddressArray;
                var queryResult = FirstTimeRecognition_Vendor.QueryHelper.DoFTSQuery(addressObject.documentWordsList, "AP - Vendors__", result.areas, Lib.P2P.FirstTimeRecognition_Vendor.validateVendorRecordForPostalAddress, false, companyCode);
                if (queryResult) {
                    result.dbresult = queryResult;
                }
                return result;
            }
            FirstTimeRecognition_Vendor.vendorLookupFTSPostal = vendorLookupFTSPostal;
            function lookupBlockAddressInDocument() {
                var postalCodeRegExp = FirstTimeRecognition_Vendor.cultureManager.GetProperty("postalCodeRegExp");
                var sPostalCodeArray;
                // FTS Query will contain only words bigger than this Length
                var minWordLength = 2;
                // Limit the count of blockaddresses to look for
                var maxBlockAddressCount = 5;
                // Our Words-to-look-for list
                var documentWordsList = [];
                var blockAddressArray = [];
                for (var iPage = 0; iPage < Document.GetPageCount() && blockAddressArray.length < maxBlockAddressCount; iPage++) {
                    var sPageText = Document.GetArea(iPage).toString();
                    traceDebugAreas("Current page " + iPage);
                    sPostalCodeArray = postalCodeRegExp.exec(sPageText);
                    while (sPostalCodeArray && blockAddressArray.length < maxBlockAddressCount) {
                        // Lookup this text in the document
                        traceDebugAreas("Current Postal Code " + sPostalCodeArray[0]);
                        var zonePostalCode = Document.SearchString(sPostalCodeArray[0], iPage, true, false);
                        var iPostalCode = 0;
                        while (zonePostalCode && iPostalCode < zonePostalCode.length && blockAddressArray.length < maxBlockAddressCount) {
                            var zoneBlockAdress = zonePostalCode[iPostalCode++];
                            var __iResX = zoneBlockAdress.x;
                            var __iResY = zoneBlockAdress.y;
                            var __iResW = zoneBlockAdress.width;
                            var __iResH = zoneBlockAdress.height;
                            zoneBlockAdress = getTextAroundWord(iPage, __iResX, __iResY, __iResW, __iResH);
                            __iResX = zoneBlockAdress.x;
                            __iResY = zoneBlockAdress.y;
                            __iResW = zoneBlockAdress.width;
                            __iResH = zoneBlockAdress.height;
                            traceDebugAreas("Analysing address: " + zoneBlockAdress.toString());
                            var sBlockAddress = "" + zoneBlockAdress.toString();
                            var isBlockComputed = false;
                            // Check if there is a single line
                            if (sBlockAddress.search(/\r/g) === -1 && sBlockAddress.search(/\n/g) === -1) {
                                traceDebugAreas("not enough lines for Postal Code " + sBlockAddress);
                                var sOriginalAreaText = Document.GetArea(iPage, __iResX, __iResY, __iResW, __iResH).toString();
                                if (sOriginalAreaText === sBlockAddress) {
                                    // Could not find more than the Postal Code. Force to take the previous word on the left, as far as it is
                                    traceDebugAreas("Forcing to enlarge on the left");
                                    var tmpAreaPrevWord = Document.GetWord(iPage, __iResX, __iResY);
                                    var __areaPrevWord = tmpAreaPrevWord.GetPreviousWord();
                                    var __iNewX = __areaPrevWord.x + __areaPrevWord.w;
                                    if (__iNewX !== 0 && __iNewX < __iResX) {
                                        traceDebugAreas("Forced to enlarge on the left");
                                        __iResW += __iResX - __areaPrevWord.x;
                                        __iResX = __areaPrevWord.x;
                                        __iResY = zoneBlockAdress.y;
                                        __iResH = zoneBlockAdress.height;
                                        zoneBlockAdress = getTextAroundWord(iPage, __iResX, __iResY, __iResW, __iResH);
                                        __iResX = zoneBlockAdress.x;
                                        __iResY = zoneBlockAdress.y;
                                        __iResW = zoneBlockAdress.width;
                                        __iResH = zoneBlockAdress.height;
                                        sBlockAddress = "" + zoneBlockAdress.toString();
                                        var withoutCR = sBlockAddress.split("\\n");
                                        isBlockComputed = withoutCR.length > 1;
                                    }
                                }
                                else {
                                    __iResX = zoneBlockAdress.x;
                                }
                                if (!isBlockComputed) {
                                    // Enlarge to the top, until the first word
                                    traceDebugAreas("Forcing to enlarge on the top");
                                    var prevLineToLeft = Document.GetWord(iPage, __iResX, __iResY);
                                    var areaPrevLine = prevLineToLeft.GetWordAbove();
                                    var __iNewY = areaPrevLine.y + areaPrevLine.height;
                                    traceDebugAreas("DEBUG iX " + __iResX + " iY " + __iResY);
                                    traceDebugAreas("Prev Line X " + areaPrevLine.x + " Y " + areaPrevLine.y + " W " + areaPrevLine.width + " H " + areaPrevLine.height);
                                    traceDebugAreas("WORD ABOVE " + areaPrevLine.toString());
                                    traceDebugAreas(" TEST " + __iNewY + " ResY " + __iResY);
                                    if (__iNewY !== 0 && __iNewY < __iResY) {
                                        traceDebugAreas("Forced to enlarged on the top");
                                        __iResH += __iResY - areaPrevLine.y;
                                        __iResY = areaPrevLine.y;
                                        zoneBlockAdress = getTextAroundWord(iPage, __iResX, __iResY, __iResW, __iResH);
                                    }
                                }
                            }
                            sBlockAddress = "" + zoneBlockAdress.toString();
                            traceDebugAreas("Step 2 Analysing address: " + zoneBlockAdress.toString());
                            // Change CRLF to spaces
                            sBlockAddress = sBlockAddress.replace(/$/g, " ");
                            sBlockAddress = sBlockAddress.replace(/\r/g, " ");
                            sBlockAddress = sBlockAddress.replace(/\n/g, " ");
                            sBlockAddress = sBlockAddress.replace(/\t/g, " ");
                            sBlockAddress = sBlockAddress.replace(",", " ");
                            sBlockAddress = spanExcluding(sBlockAddress, "/\\&<>'\".:~{}");
                            // And split the block address in Words
                            var words = sBlockAddress.split(" ");
                            var wordsList = [];
                            for (var iWord = 0; iWord < words.length; iWord++) {
                                var currentWord = words[iWord];
                                if (currentWord.length >= minWordLength) {
                                    // Check if already inserted in our word-to-look list
                                    var bIsNew = documentWordsList.indexOf(currentWord) === -1;
                                    if (bIsNew) {
                                        documentWordsList.push(currentWord);
                                        wordsList.push(currentWord);
                                    }
                                    else {
                                        var bIsNewInBlock = wordsList.indexOf(currentWord) === -1;
                                        if (bIsNewInBlock) {
                                            wordsList.push(currentWord);
                                        }
                                    }
                                }
                            }
                            sBlockAddress = wordsList.join(" ");
                            var objMatchingInfo = {
                                zone: zoneBlockAdress,
                                columns: ["PostalCode__", "Street__", "Region__", "City__", "Name__"],
                                lookupText: [
                                    sBlockAddress,
                                    sBlockAddress,
                                    sBlockAddress,
                                    sBlockAddress,
                                    sBlockAddress
                                ]
                            };
                            blockAddressArray.push(objMatchingInfo);
                        }
                        sPostalCodeArray = postalCodeRegExp.exec(sPageText);
                    }
                    traceDebugAreas("sPostalCodeArray Finished");
                }
                return { documentWordsList: documentWordsList, blockAddressArray: blockAddressArray };
            }
            FirstTimeRecognition_Vendor.lookupBlockAddressInDocument = lookupBlockAddressInDocument;
            // #endregion lookup functions
            /* Helper for validateVendorRecordForVATPhoneOrFax
                * Check that the data have been found in the correct columns
                * arrayObjMatchingInfo contains the zones we're looking for.
                * It must be an array of objects containing:
                *	- zone: the zone where the text was found
                *	- columns: array of column names which should match
                *	- value: value of the field in the document which will be compared to database value
                */
            function validateVendorRecordForVATPhoneOrFax(record, arrayObjMatchingInfo) {
                traceDebug("ValidateRecordForVATPhoneOrFax - msn : '" + record.GetValue_String("msn", 0) + "'");
                // For each block containing text
                for (var i = 0; i < arrayObjMatchingInfo.length; i++) {
                    var currentBlock = arrayObjMatchingInfo[i];
                    traceDebug("Evaluating block : '" + currentBlock.value + "'");
                    // For each Column possible for that query
                    for (var j = 0; j < currentBlock.columns.length; j++) {
                        // Verify that the requested text has been found in one of these columns
                        var sColName = currentBlock.columns[j];
                        var sColValue = record.GetValue_String(sColName, 0).toLowerCase();
                        if (sColValue === currentBlock.value.toLowerCase()) {
                            traceDebug("Found matching Word - Doc : '" + currentBlock.value + "' / DB : '" + sColValue + "' in '" + sColName + "'");
                            currentBlock.matchingWords = 1;
                            return true;
                        }
                    }
                }
                return false;
            }
            FirstTimeRecognition_Vendor.validateVendorRecordForVATPhoneOrFax = validateVendorRecordForVATPhoneOrFax;
            /* Helper for validateVendorRecordForPostalAddress
                *	Check that the data have been found in the correct columns
                *	arrayObjMatchingInfo contains the zones we're looking for.
                *	It must be an array of objects containing:
                *	- zone: the zone where the text was found
                *	- columns: array of column names which should match
                *	- lookupText: array of text corresponding to the columns
                */
            function validateVendorRecordForPostalAddress(record, arrayObjMatchingInfo) {
                var bValidRecord = false;
                var minRequiredMatchingWords = 2;
                traceDebug("ValidateRecordForPostalAddress - msn : '" + record.GetValue_String("msn", 0) + "'");
                // For each block containing text
                for (var i = 0; i < arrayObjMatchingInfo.length; i++) {
                    var matchingWords = 0;
                    var currentBlock = arrayObjMatchingInfo[i];
                    traceDebug("Evaluating block : '" + currentBlock.lookupText[0] + "'");
                    // For each Column possible for that query
                    for (var j = 0; j < currentBlock.columns.length; j++) {
                        // Verify that the requested text has been found in one of these columns
                        var sColName = currentBlock.columns[j];
                        var sColValue = record.GetValue_String(sColName, 0).toLowerCase();
                        var arrayLookupText = currentBlock.lookupText[j].split(" ");
                        for (var k = 0; k < arrayLookupText.length; k++) {
                            var sCandidate = arrayLookupText[k];
                            if (sCandidate.length > 0 && sColValue.indexOf(sCandidate.toLowerCase()) !== -1) {
                                traceDebug("Found matching Word - Doc : '" + sCandidate + "' / DB : '" + sColValue + "' in '" + sColName + "'");
                                matchingWords++;
                            }
                        }
                    }
                    currentBlock.matchingWords = matchingWords;
                    bValidRecord = bValidRecord || matchingWords >= minRequiredMatchingWords;
                    traceDebug("Block score : " + matchingWords + " (Min:" + minRequiredMatchingWords + ")");
                }
                return bValidRecord;
            }
            FirstTimeRecognition_Vendor.validateVendorRecordForPostalAddress = validateVendorRecordForPostalAddress;
            // #endregion validation functions
            function SearchVendorNumber(sCompanyCode, sVendorNumber, sVendorName, funcFillVendor) {
                var filter = "";
                if (sVendorNumber) {
                    filter = "Number__=" + sVendorNumber;
                }
                else if (sVendorName) {
                    filter = "Name__=" + sVendorName;
                }
                var dbresult = Lib.P2P.FirstTimeRecognition_Vendor.queryDbFirstRow("*", "AP - Vendors__", filter.AddCompanyCodeFilter(sCompanyCode));
                if (dbresult) {
                    traceDebug("FirstTimeReco vendor number '" + sVendorNumber + "' already defined");
                    funcFillVendor(dbresult);
                    // Keep pre-filled results
                    return true;
                }
                return false;
            }
            FirstTimeRecognition_Vendor.SearchVendorNumber = SearchVendorNumber;
            /**
             * @callback fillVendorCallback
             * @memberof Lib.P2P.FirstTimeRecognition_Vendor
             * @param {xVars} recordVars the vendor record values
             * @param {string} [lookupValue] the lookup value that permits to retrieve the vendor
             * @param {string} [desc] the desc of the method that permits to retrieve the vendor
             */
            /**
             * @callback highlightVendorCallback
             * @memberof Lib.P2P.FirstTimeRecognition_Vendor
             * @param {Lib.P2P.FirstTimeRecognition_Vendor.MatchingInfo} matchingInfo the matching info
             * @param {string} [lookupValue] the lookup value that permits to retrieve the vendor
             * @param {string} [desc] the desc of the method that permits to retrieve the vendor
             */
            /**
             * @callback searchFunctionCallback
             * @memberof Lib.P2P.FirstTimeRecognition_Vendor
             * @param {string} sCompanyCode company code to search the vendor for
             * @param {string} sVendorNumber current vendor number if any
             * @param {string} sVendorName current vandor name if any
             * @param {Lib.P2P.FirstTimeRecognition_Vendor.fillVendorCallback} funcFillVendor function use to fill the form with the retrieved vendor information
             */
            /**
             * start the research of the vendor based on document information
             * @memberof Lib.P2P.FirstTimeRecognition_Vendor
             * @param {string|string[]} availableCultures culture otr list of cultures available for the current document
             * @param {string} sCompanyCode company code to search the vendor for
             * @param {string} sVendorNumber current vendor number if any
             * @param {string} sVendorName current vandor name if any
             * @param {Lib.P2P.FirstTimeRecognition_Vendor.fillVendorCallback} funcFillVendor function use to fill the form with the retrieved vendor information
             * @param {Lib.P2P.FirstTimeRecognition_Vendor.highlightVendorCallback} funcHighlightVendor function use to highlight information on the document
             * @param {Lib.P2P.FirstTimeRecognition_Vendor.searchFunctionCallback} searchFunction function to use when the vendor has been already found by teaching or autolearning
             */
            function Recognize(availableCultures, sCompanyCode, sVendorNumber, sVendorName, funcFillVendor, funcHighlightVendor, searchFunction, invoiceDocument) {
                var found = false;
                Init();
                FirstTimeRecognition_Vendor.cultureManager = new CultureManager();
                FirstTimeRecognition_Vendor.cultureManager.SetAvailableCultures(availableCultures);
                if (typeof availableCultures === "string") {
                    availableCultures = [availableCultures];
                }
                // Vendor already defined by teaching / Learning
                if (searchFunction && (sVendorNumber || sVendorName)) {
                    searchFunction(sCompanyCode, sVendorNumber, sVendorName, funcFillVendor);
                    return true;
                }
                // Recognition by VAT (each available culture), then phone fax (each available culture), then address bloc (each available culture)
                var arrayMatchFunctions = [];
                if (options.searchIBAN) {
                    arrayMatchFunctions.push({ "func": Lib.P2P.FirstTimeRecognition_Vendor.vendorLookupFTSIBAN, "desc": "by IBAN" });
                }
                if (options.searchVAT) {
                    arrayMatchFunctions.push({ "func": Lib.P2P.FirstTimeRecognition_Vendor.vendorLookupFTSVAT, "desc": "by VAT" });
                }
                if (options.searchPhoneOrFax) {
                    arrayMatchFunctions.push({ "func": Lib.P2P.FirstTimeRecognition_Vendor.vendorLookupFTSPhoneOrFax, "desc": "by Phone number or fax" });
                }
                if (options.searchAddress) {
                    arrayMatchFunctions.push({ "func": Lib.P2P.FirstTimeRecognition_Vendor.vendorLookupFTSPostal, "desc": "by address bloc" });
                }
                var customArrayMatchFunctions = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.FTRVendor.GetVendorFTRMethods", arrayMatchFunctions, FirstTimeRecognition_Vendor.cultureManager) || arrayMatchFunctions;
                for (var testFuncIdx = 0; testFuncIdx < customArrayMatchFunctions.length && !found; testFuncIdx++) {
                    for (var cultureIdx = 0; cultureIdx < availableCultures.length; cultureIdx++) {
                        FirstTimeRecognition_Vendor.cultureManager.SetCurrentDocumentCulture(availableCultures[cultureIdx]);
                        var lookupResult = customArrayMatchFunctions[testFuncIdx].func(sCompanyCode, invoiceDocument);
                        if (lookupResult) {
                            var desc = customArrayMatchFunctions[testFuncIdx].desc;
                            if (lookupResult.dbresult) {
                                found = true;
                                var area = Lib.P2P.FirstTimeRecognition_Vendor.GetBestMatchedArea(lookupResult.areas);
                                var lookupValue = null;
                                if (area) {
                                    lookupResults[desc] = area;
                                    lookupValue = area.value;
                                    if (funcHighlightVendor) {
                                        funcHighlightVendor(area);
                                    }
                                }
                                funcFillVendor(lookupResult.dbresult, lookupValue, desc);
                                Log.Info("Found vendor number: '" + lookupResult.dbresult.GetValue_String("Number__", 0) + "' " + desc + " with culture : '" + availableCultures[cultureIdx]);
                                break;
                            }
                            else if (lookupResult.areas && lookupResult.areas.length > 0 && !lookupResults[desc]) {
                                lookupResults[desc] = lookupResult.areas[0];
                            }
                        }
                    }
                }
                if (!found) {
                    Log.Warn("No vendor found.");
                }
                return found;
            }
            FirstTimeRecognition_Vendor.Recognize = Recognize;
        })(FirstTimeRecognition_Vendor = P2P.FirstTimeRecognition_Vendor || (P2P.FirstTimeRecognition_Vendor = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
