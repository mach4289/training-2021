/* eslint no-empty-function: "off", no-unused-vars: "off" */
/**
 * @file Lib.AP.Customization.VendorRegistration_HTMLScripts library
 * @namespace Lib.AP.Customization.VendorRegistration_HTMLScripts
 */

// eslint-disable-next-line no-redeclare
var Lib = Lib || {};
Lib.AP = Lib.AP || {};
Lib.AP.Customization = Lib.AP.Customization || {};

(function (parentLib)
{
	/**
	 * @lends Lib.AP.Customization.VendorRegistration_HTMLScripts
	 */
	parentLib.VendorRegistration_HTMLScripts =
	{
		/**
		* Function called in Vendor registration when the Next button is clicked by the user (Vendor profile), to add additionnal checks on step validation
		* Default process checks are done after calling this function.
		*
		* @memberof Lib.AP.Customization.VendorRegistration_HTMLScripts
		* @returns {boolean} If the current step is valid and the user can go to the next step.
		* @example
		* <pre><code>
		* ValidCurrentStep: function()
		* {
		*	//We require a value for this field
		*	const value = Data.GetValue("CompanyStructure__");
		*	const isValid = !Controls.CompanyStructure__.IsVisible() || (value !== null && value !== "");
		*	if(!isValid){
		*		Data.SetError("CompanyStructure__", "This field is required");
		*	}
		*	return isValid;
		* }
		* </code></pre>
		*/
		ValidCurrentStep: function()
		{
		}
	};
})(Lib.AP.Customization);
