///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CM_Workflow_Server",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Purchasing Catalog server library used to manage document workflow",
  "require": [
    "Lib_P2P_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_CM_Server_V12.0.425.0",
    "Lib_Purchasing_CM_Workflow_V12.0.425.0",
    "Sys/Sys_Helpers",
    "Sys/Sys_OnDemand_Users",
    "Sys/Sys_Helpers_Controls",
    "Sys/Sys_WorkflowController",
    "Sys/Sys_EmailNotification"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            var Workflow;
            (function (Workflow) {
                var vendorLogin = Lib.P2P.GetValidatorOrOwner().GetValue("AccountId") + "$" + Data.GetValue("RequesterLogin__");
                var isInternalUpdateRequest = Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest();
                function Forward() {
                    var idx = Workflow.controller.GetContributorIndex();
                    var step = Workflow.controller.GetContributorAt(idx);
                    Log.Info("Forwarding message to " + step.login + "(" + idx + ")");
                    Process.SetRight(step.login, "read");
                    Process.Forward(step.login);
                }
                function UpdateParentProcess(action, data) {
                    var transport = Process.GetUpdatableTransportAsProcessAdmin(Variable.GetValueAsString("CMRUIDEX"));
                    var externalVars = transport.GetExternalVars();
                    if (data) {
                        var resumeWithActionData = JSON.stringify(data);
                        externalVars.AddValue_String("resumeWithActionData", resumeWithActionData, true);
                    }
                    externalVars.AddValue_String(Lib.Purchasing.CM.extractedDataVariable, Variable.GetValueAsString(Lib.Purchasing.CM.extractedDataVariable), true);
                    transport.ResumeWithAction(action);
                }
                Workflow.UpdateParentProcess = UpdateParentProcess;
                function SendEmailNotification(login, template, backupUserAsCC) {
                    var options = {
                        userId: login,
                        template: template,
                        fromName: "Esker Catalog Management",
                        backupUserAsCC: !!backupUserAsCC,
                        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
                    };
                    //let doSendNotif = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnSendEmailNotification", options);
                    //if (doSendNotif !== false)
                    {
                        Sys.EmailNotification.SendEmailNotification(options);
                    }
                }
                function DoImport(comment) {
                    var nbreSteps = Workflow.controller.GetNbContributors();
                    var bOK = Lib.Purchasing.CM.ProcessedData(Lib.Purchasing.CM.ProcessLines);
                    bOK = bOK && Lib.Purchasing.CM.GetExternalData().ProcessingError.length === 0;
                    if (bOK) {
                        var approvedDate = new Date();
                        // Does not forward to anyone, and lets the document be approved and go to the success state.
                        Workflow.controller.EndWorkflow({
                            action: "approved",
                            date: approvedDate,
                            comment: comment
                        });
                        Data.SetValue("ValidationDateTime__", approvedDate);
                        Data.SetValue("Status__", "Approved");
                        Data.SetValue("State", 100);
                        Data.SetValue("Comments__", "");
                        if (!isInternalUpdateRequest) {
                            UpdateParentProcess("CSVApproved");
                            var step = Workflow.controller.GetContributorAt(Workflow.controller.GetContributorIndex());
                            SendEmailNotification(nbreSteps > 1 ? step.login : Data.GetValue("OwnerID"), "CM-CatalogManager_ImportSuccess.htm", true);
                            SendEmailNotification(vendorLogin, "CM-Vendor_ImportSuccess.htm", true);
                        }
                    }
                    else {
                        /* State 60 does not work Requested action is deleted.
                        if(Data.GetValue("NTries") <= 4)
                        {
                            Log.Error("Error while processing, retry (" + Data.GetValue("NTries") + ")");
                            Data.SetValue("State", 60);
                            Process.PreventApproval();
                        }
                        else
                        {
                            */
                        Variable.SetValueAsString("CSVProcessingError", "true");
                        Log.Error("Error while processing, no more retry");
                        if (!isInternalUpdateRequest) {
                            // Notify the buyer that the import has failed
                            var step = Workflow.controller.GetContributorAt(Workflow.controller.GetContributorIndex());
                            SendEmailNotification(nbreSteps > 1 ? step.login : Data.GetValue("OwnerID"), "CM-CatalogManager_ImportFail.htm", true);
                        }
                        Process.PreventApproval();
                        //}
                    }
                }
                var serverParameters = {
                    actions: {
                        submit: {
                            OnDone: function (index) {
                                var nbSteps = Workflow.controller.GetNbContributors();
                                var comment;
                                if (isInternalUpdateRequest) {
                                    var currentContributor = Workflow.controller.GetContributorAt(index);
                                    comment = Lib.P2P.AddOnBehalfOf(currentContributor, Data.GetValue("Comments__"));
                                    Data.SetValue("SubmissionDateTime__", new Date());
                                    Data.SetValue("Status__", "ToApprove");
                                }
                                else {
                                    comment = Language.Translate("_Catalog import request submitted by the vendor");
                                }
                                // In case there are no approvers in the workflow, approve the catalog import request and import the catalog.
                                // Otherwise, forward the request to the first validator.
                                if (index < nbSteps - 1) {
                                    Workflow.controller.NextContributor({
                                        action: "submitted",
                                        date: Data.GetValue("SubmissionDateTime__"),
                                        comment: comment
                                    });
                                    var step = Workflow.controller.GetContributorAt(Workflow.controller.GetContributorIndex());
                                    if (isInternalUpdateRequest) {
                                        SendEmailNotification(step.login, "CM-ApproveRequestInternal.htm", true);
                                    }
                                    else {
                                        SendEmailNotification(step.login, "CM-ApproveRequest.htm", true);
                                    }
                                    Forward();
                                }
                                else {
                                    if (isInternalUpdateRequest || Variable.GetValueAsString("MissingWorkflowRuleError") !== "true") {
                                        DoImport(comment);
                                    }
                                }
                            }
                        },
                        submitted: {},
                        approve: {
                            OnDone: function (index) {
                                var nbSteps = Workflow.controller.GetNbContributors();
                                var currentContributor = Workflow.controller.GetContributorAt(index);
                                var comment = Lib.P2P.AddOnBehalfOf(currentContributor, Data.GetValue("Comments__"));
                                if (index < nbSteps - 1) {
                                    Workflow.controller.NextContributor({
                                        action: "approved",
                                        date: new Date(),
                                        comment: comment
                                    });
                                    var step = Workflow.controller.GetContributorAt(Workflow.controller.GetContributorIndex());
                                    if (isInternalUpdateRequest) {
                                        SendEmailNotification(step.login, "CM-ApproveRequestInternal.htm", true);
                                    }
                                    else {
                                        SendEmailNotification(step.login, "CM-ApproveRequest.htm", true);
                                    }
                                    Forward();
                                }
                                else {
                                    DoImport(comment);
                                }
                            }
                        },
                        approved: {},
                        reject: {
                            OnDone: function () {
                                var rejectedDate = new Date();
                                var idx = Workflow.controller.GetContributorIndex();
                                var currentContributor = Workflow.controller.GetContributorAt(idx);
                                var comment = Lib.P2P.AddOnBehalfOf(currentContributor, Data.GetValue("Comments__"));
                                // Terminates the workflow and sets the message in the rejected state.
                                Workflow.controller.EndWorkflow({
                                    action: "rejected",
                                    date: rejectedDate,
                                    comment: comment
                                });
                                if (!isInternalUpdateRequest) {
                                    UpdateParentProcess("CSVRejected", { rejectionReason: Data.GetValue("Comments__") });
                                    // Notify the vendor that his import request has been rejected
                                    SendEmailNotification(vendorLogin, "CM-Vendor_ImportReject.htm", true);
                                }
                                Data.SetValue("Reason__", Data.GetValue("Comments__"));
                                Data.SetValue("ValidationDateTime__", rejectedDate);
                                Data.SetValue("Status__", "Rejected");
                                Data.SetValue("Comments__", "");
                                Data.SetValue("State", 400);
                                Process.LeaveForm();
                            }
                        },
                        rejected: {}
                    }
                };
                Sys.Helpers.Extend(true, Lib.Purchasing.CM.Workflow.parameters, serverParameters);
            })(Workflow = CM.Workflow || (CM.Workflow = {}));
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
