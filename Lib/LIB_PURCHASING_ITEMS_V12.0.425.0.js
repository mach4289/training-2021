///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_Items",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers_String",
    "Sys/Sys_Helpers_Array",
    "Sys/Sys_Helpers_Object",
    "Sys/Sys_Helpers_Data",
    "Sys/Sys_Helpers_Date",
    "Sys/Sys_Helpers_Controls",
    "Sys/Sys_Helpers_Promise",
    "[Sys/Sys_Helpers_Database]",
    "Lib_CommonDialog_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_TechnicalData"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var Items;
        (function (Items) {
            Items.AddLib = Lib.AddLib;
            Items.ExtendLib = Lib.ExtendLib;
            // Members declaration and implementation
            var g = Sys.Helpers.Globals;
            Items.MAXRECORDS = 100;
            function InitDBFieldsMap(dbInfo) {
                return dbInfo.fieldsMap = dbInfo.fields.reduce(function (ret, field) {
                    if (Sys.Helpers.IsPlainObject(field) && Sys.Helpers.IsString(field.name)) {
                        if (!field.foreign) {
                            ret[field.name] = field.type || "string";
                        }
                    }
                    else if (Sys.Helpers.IsString(field)) {
                        ret[field] = "string";
                    }
                    return ret;
                }, {});
            }
            Items.PRItemsDBInfo = {
                "docType": "PR",
                "table": "PAC - PR - Items__",
                "lineKey": ["PRNumber__", "LineNumber__"],
                "fields": [
                    "CompanyCode__",
                    "PRNumber__",
                    { "name": "LineNumber__", "type": "int" },
                    "Status__",
                    "ItemType__",
                    "CatalogReference__",
                    "Description__",
                    "CostCenterName__",
                    "CostCenterId__",
                    "ProjectCode__",
                    "ProjectCodeDescription__",
                    "WBSElement__",
                    "WBSElementID__",
                    "InternalOrder__",
                    { "name": "Quantity__", "type": "double" },
                    { "name": "QuantityTakenFromStock__", "type": "double" },
                    { "name": "CanceledQuantity__", "type": "double" },
                    { "name": "OrderedQuantity__", "type": "double" },
                    { "name": "ReceivedQuantity__", "type": "double" },
                    "Currency__",
                    { "name": "ExchangeRate__", "type": "double" },
                    "TaxCode__",
                    { "name": "TaxRate__", "type": "double" },
                    { "name": "UnitPrice__", "type": "double" },
                    { "name": "NetAmount__", "type": "double" },
                    { "name": "AmountTakenFromStock__", "type": "double" },
                    { "name": "CanceledAmount__", "type": "double" },
                    { "name": "ItemOrderedAmount__", "type": "double" },
                    { "name": "ItemReceivedAmount__", "type": "double" },
                    "CostType__",
                    "GLAccount__",
                    "Group__",
                    "VendorName__",
                    "VendorNumber__",
                    { "name": "StartDate__", "type": "date" },
                    { "name": "EndDate__", "type": "date" },
                    { "name": "RequestedDeliveryDate__", "type": "date" },
                    { "name": "RequestedDeliveryDateInPast__", "type": "bool" },
                    "SupplyTypeName__",
                    "SupplyTypeId__",
                    "SupplyTypeFullPath__",
                    "PurchasingGroup__",
                    "PurchasingOrganization__",
                    "DeliveryAddressID__",
                    "ShipToAddress__",
                    "ShipToCompany__",
                    "ShipToContact__",
                    "ShipToEmail__",
                    "ShipToPhone__",
                    { "name": "CompletelyOrdered__", "type": "bool" },
                    "RequesterDN__",
                    "BuyerDN__",
                    "BuyerLogin__",
                    "RecipientDN__",
                    "PRRUIDEX__",
                    "BudgetID__",
                    "ItemInCxml__",
                    "Reason__",
                    "ItemUnit__",
                    "ItemUnitDescription__",
                    { "name": "NoGoodsReceipt__", "type": "bool" },
                    { "name": "Locked__", "type": "bool" },
                    "LeadTime__",
                    { "name": "ShipToAddress", "TechnicalData": true },
                    { "name": "PRSubmissionDateTime__", "type": "date" },
                    "FreeDimension1__",
                    "FreeDimension1ID__",
                    { "name": "IsReplenishmentItem__", "type": "bool" },
                    "WarehouseID__",
                    "WarehouseName__"
                ]
            };
            Items.PRItemsSynchronizeConfig = {
                "tableKey": "PRNumber__",
                "ParentRuidKey": "PRRUIDEX__",
                "lineKey": "LineNumber__",
                "dbInfo": Items.PRItemsDBInfo,
                "formTable": "LineItems__",
                "computedData": null,
                "mappings": {
                    "common": {
                        "PRSubmissionDateTime__": "PRSubmissionDateTime__",
                        "CompanyCode__": "CompanyCode__",
                        "PRNumber__": "RequisitionNumber__",
                        "PurchasingGroup__": "PurchasingGroup__",
                        "PurchasingOrganization__": "PurchasingOrganization__",
                        "DeliveryAddressID__": "DeliveryAddressID__",
                        "ShipToAddress__": "ShipToAddress__",
                        "ShipToCompany__": "ShipToCompany__",
                        "ShipToContact__": "ShipToContact__",
                        "ShipToEmail__": "ShipToEmail__",
                        "ShipToPhone__": "ShipToPhone__",
                        "RequesterDN__": { "name": "RequesterDN", "computed": true },
                        "PRRUIDEX__": "RUIDEX",
                        "Reason__": "Reason__",
                        "TechnicalData__": "TechnicalData__"
                    },
                    "byLine": {
                        "LineNumber__": "LineItemNumber__",
                        "Currency__": "ItemCurrency__",
                        "ExchangeRate__": { "name": "ExchangeRate", "computed": true },
                        "ItemType__": "ItemType__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "CostCenterId__": { "name": "ItemCostCenterId__", "availableOnWorkflowRule": true },
                        "ProjectCode__": { "name": "ProjectCode__", "availableOnWorkflowRule": true },
                        "ProjectCodeDescription__": { "name": "ProjectCodeDescription__" },
                        "WBSElement__": { "name": "WBSElement__", "availableOnWorkflowRule": true },
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": { "name": "InternalOrder__", "availableOnWorkflowRule": true },
                        "Quantity__": "ItemQuantity__",
                        "QuantityTakenFromStock__": "QuantityTakenFromStock__",
                        "CanceledQuantity__": "CanceledQuantity__",
                        "OrderedQuantity__": "ItemOrderedQuantity__",
                        "ReceivedQuantity__": "ItemDeliveredQuantity__",
                        "Status__": "ItemStatus__",
                        "CompletelyOrdered__": { "name": "CompletelyOrdered", "computed": true },
                        "TaxCode__": { "name": "ItemTaxCode__", "availableOnWorkflowRule": true },
                        "TaxRate__": "ItemTaxRate__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "NetAmount__": "ItemNetAmount__",
                        "AmountTakenFromStock__": "AmountTakenFromStock__",
                        "CanceledAmount__": "CanceledAmount__",
                        "ItemOrderedAmount__": "ItemOrderedAmount__",
                        "ItemReceivedAmount__": "ItemReceivedAmount__",
                        "CostType__": { "name": "CostType__", "availableOnWorkflowRule": true },
                        "GLAccount__": { "name": "ItemGLAccount__", "availableOnWorkflowRule": true },
                        "Group__": { "name": "ItemGroup__", "availableOnWorkflowRule": true },
                        "VendorName__": "VendorName__",
                        "VendorNumber__": "VendorNumber__",
                        "BuyerDN__": { "name": "BuyerDN", "computed": true },
                        "BuyerLogin__": "BuyerLogin__",
                        "RecipientDN__": { "name": "RecipientDN", "computed": true },
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeId__": "SupplyTypeID__",
                        "SupplyTypeFullPath__": "SupplyTypeFullPath__",
                        "StartDate__": "ItemStartDate__",
                        "EndDate__": "ItemEndDate__",
                        "RequestedDeliveryDate__": "ItemRequestedDeliveryDate__",
                        "RequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
                        "BudgetID__": "BudgetID__",
                        "ItemInCxml__": "ItemInCxml__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "Locked__": "Locked__",
                        "LeadTime__": "LeadTime__",
                        "FreeDimension1__": { "name": "FreeDimension1__", "availableOnWorkflowRule": true },
                        "FreeDimension1ID__": { "name": "FreeDimension1ID__", "availableOnWorkflowRule": true },
                        "IsReplenishmentItem__": { "name": "IsReplenishmentItem__", "availableOnWorkflowRule": true },
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    delete Items.PRItemsSynchronizeConfig.mappings.common.ShipToCompany__;
                    delete Items.PRItemsSynchronizeConfig.mappings.common.DeliveryAddressID__;
                    delete Items.PRItemsSynchronizeConfig.mappings.common.ShipToAddress__;
                    Items.PRItemsSynchronizeConfig.mappings.byLine["ShipToCompany__"] = "ItemShipToCompany__";
                    Items.PRItemsSynchronizeConfig.mappings.byLine["DeliveryAddressID__"] = "ItemDeliveryAddressID__";
                    Items.PRItemsSynchronizeConfig.mappings.byLine["ShipToAddress__"] = "ItemShipToAddress__";
                }
            });
            Items.PRItemsBrowseDisplayedColumns = [
                { id: "PRNumber__", label: "_PRNumber", type: "STR", width: 100 },
                { id: "LineNumber__", label: "_LineNumber", type: "STR", width: 80 },
                { id: "Description__", label: "_ItemDescription", type: "STR", width: 260 },
                { id: "RequestedDeliveryDate__", label: "_ItemRequestedDeliveryDate", type: "DATE", width: 80 },
                { id: "Quantity__", label: "_ItemRequestedQuantity", type: "INTEGER", width: 50 },
                { id: "OrderedQuantity__", label: "_Ordered quantity", type: "INTEGER", width: 50 },
                { id: "UnitPrice__", label: "_Item unit price", type: "DECIMAL", width: 70 },
                { id: "NetAmount__", label: "_Item net amount", type: "DECIMAL", width: 70 },
                { id: "ItemOrderedAmount__", label: "_ItemOrderedAmount", type: "INTEGER", width: 50 }
            ];
            Items.PRItemsToPRICanceler = {
                "dbInfo": Items.PRItemsDBInfo,
                "formTable": "LineItems__",
                "errorMessages": {},
                "mappings": {
                    "common": {},
                    "byLine": {
                        "PRRUIDEX__": "PRRUIDEX__",
                        "PRNumber__": "PRNumber__",
                        "LineNumber__": "PRLineNumber__",
                        "Description__": "Description__",
                        "Quantity__": "RequestedQuantity__",
                        "UnitPrice__": "UnitPrice__",
                        "NetAmount__": "NetAmount__",
                        "RequesterDN__": "RequesterDN__",
                        "RequestedDeliveryDate__": "RequestedDeliveryDate__",
                        "ItemType__": "ItemType__"
                    }
                }
            };
            Items.POItemsDBInfo = {
                "docType": "PO",
                "table": "PAC - PO - Items__",
                "lineKey": ["PONumber__", "LineNumber__"],
                "fields": [
                    "PONumber__",
                    { "name": "LineNumber__", "type": "int" },
                    "PRNumber__",
                    { "name": "PRLineNumber__", "type": "int" },
                    "Status__",
                    "CompanyCode__",
                    "CatalogReference__",
                    "Description__",
                    "CostCenterName__",
                    "CostCenterId__",
                    "ProjectCode__",
                    "WBSElement__",
                    "WBSElementID__",
                    "InternalOrder__",
                    "ProjectCodeDescription__",
                    { "name": "OrderedQuantity__", "type": "double" },
                    { "name": "ReceivedQuantity__", "type": "double" },
                    "Currency__",
                    "TaxCode__",
                    { "name": "TaxRate__", "type": "double" },
                    { "name": "UnitPrice__", "type": "double" },
                    { "name": "NetAmount__", "type": "double" },
                    { "name": "ItemReceivedAmount__", "type": "double" },
                    "CostType__",
                    "GLAccount__",
                    "VendorName__",
                    "VendorNumber__",
                    { "name": "StartDate__", "type": "date" },
                    { "name": "EndDate__", "type": "date" },
                    { "name": "RequestedDeliveryDate__", "type": "date" },
                    { "name": "RequestedDeliveryDateInPast__", "type": "bool" },
                    { "name": "DeliveryDate__", "type": "date" },
                    "SupplyTypeName__",
                    "SupplyTypeId__",
                    { "name": "CompletelyDelivered__", "type": "bool" },
                    "RequesterDN__",
                    "BuyerDN__",
                    "RecipientDN__",
                    "RecipientLogin__",
                    "PRRUIDEX__",
                    "PORUIDEX__",
                    "Group__",
                    "BudgetID__",
                    { "name": "ExchangeRate__", "type": "double" },
                    "ItemUnit__",
                    "ItemUnitDescription__",
                    "ItemType__",
                    { "name": "NoGoodsReceipt__", "type": "bool" },
                    { "name": "ItemRequestedAmount__", "type": "double" },
                    { "name": "PRSubmissionDateTime__", "type": "date" },
                    "FreeDimension1__",
                    "FreeDimension1ID__",
                    "ShipToCompany__",
                    "DeliveryAddressID__",
                    "ShipToAddress__",
                    "IsReplenishmentItem__",
                    "WarehouseID__",
                    "WarehouseName__"
                ]
            };
            Items.POItemsSynchronizeConfig = {
                "tableKey": "PONumber__",
                "lineKey": "LineNumber__",
                "ParentRuidKey": "PORUIDEX__",
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineItems__",
                "computedData": null,
                "mappings": {
                    "common": {
                        "PONumber__": "OrderNumber__",
                        "CompanyCode__": "CompanyCode__",
                        "Currency__": "Currency__",
                        "DeliveryAddressID__": "DeliveryAddressID__",
                        "ShipToAddress__": "ShipToAddress__",
                        "ShipToCompany__": "ShipToCompany__",
                        "VendorName__": "VendorName__",
                        "VendorNumber__": "VendorNumber__",
                        "PORUIDEX__": "RUIDEX"
                    },
                    "byLine": {
                        /*PAC PO Items (table) field name: PO items (form) field name */
                        "PRSubmissionDateTime__": "PRSubmissionDateTime__",
                        "PRRUIDEX__": "PRRUIDEX__",
                        "PRNumber__": "PRNumber__",
                        "PRLineNumber__": "PRLineNumber__",
                        "RequesterDN__": "RequesterDN__",
                        "BuyerDN__": "BuyerDN__",
                        "RecipientDN__": "RecipientDN__",
                        "RecipientLogin__": { "name": "RecipientLogin", "computed": true },
                        "LineNumber__": "LineItemNumber__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "StartDate__": "ItemStartDate__",
                        "EndDate__": "ItemEndDate__",
                        "RequestedDeliveryDate__": "ItemRequestedDeliveryDate__",
                        "RequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
                        "CostCenterId__": "ItemCostCenterId__",
                        "ProjectCode__": "ProjectCode__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "OrderedQuantity__": "ItemQuantity__",
                        "ReceivedQuantity__": "ItemTotalDeliveredQuantity__",
                        "DeliveryDate__": "ItemDeliveryDate__",
                        "Status__": { "name": "Status", "computed": true },
                        "CompletelyDelivered__": "ItemDeliveryComplete__",
                        "TaxCode__": "ItemTaxCode__",
                        "TaxRate__": "ItemTaxRate__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "NetAmount__": "ItemNetAmount__",
                        "ItemReceivedAmount__": "ItemReceivedAmount__",
                        "CostType__": "CostType__",
                        "GLAccount__": "ItemGLAccount__",
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeId__": "SupplyTypeId__",
                        "Group__": "ItemGroup__",
                        "BudgetID__": "BudgetID__",
                        "ExchangeRate__": "ItemExchangeRate__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "ItemType__": "ItemType__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "ItemRequestedAmount__": "ItemRequestedAmount__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__",
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    delete Items.POItemsSynchronizeConfig.mappings.common.ShipToCompany__;
                    delete Items.POItemsSynchronizeConfig.mappings.common.DeliveryAddressID__;
                    delete Items.POItemsSynchronizeConfig.mappings.common.ShipToAddress__;
                    Items.POItemsSynchronizeConfig.mappings.byLine["ShipToCompany__"] = "ItemShipToCompany__";
                    Items.POItemsSynchronizeConfig.mappings.byLine["DeliveryAddressID__"] = "ItemDeliveryAddressID__";
                    Items.POItemsSynchronizeConfig.mappings.byLine["ShipToAddress__"] = "ItemShipToAddress__";
                }
            });
            Items.PRItemsToPO = {
                "dbInfo": Items.PRItemsDBInfo,
                "formTable": "LineItems__",
                "formLineNumber": "LineItemNumber__",
                "errorMessages": {
                    "CompanyCode__": "_Only items from the same company can be selected.",
                    "Currency__": "_Only items with the same currency can be selected."
                    // PurchasingGroup__ and PurchasingOrganization__ are defined by company code
                },
                "mappings": {
                    "common": {
                        "CompanyCode__": "CompanyCode__",
                        "Currency__": "Currency__",
                        "VendorName__": { "name": "VendorName__", "sectionName": "Vendor" },
                        "VendorNumber__": { "name": "VendorNumber__", "sectionName": "Vendor", "ignoreMultipleValuesAtItemLevel": true },
                        "VendorEmail__": { "name": "VendorEmail__", "sectionName": "Vendor" },
                        "VendorVatNumber__": { "name": "VendorVatNumber__", "sectionName": "Vendor" },
                        "VendorAddress__": { "name": "VendorAddress__", "sectionName": "Vendor" },
                        "PurchasingGroup__": "PurchasingGroup__",
                        "PurchasingOrganization__": "PurchasingOrganization__",
                        "DeliveryAddressID__": { "name": "DeliveryAddressID__", "sectionName": "ShipTo" },
                        "ShipToAddress__": { "name": "ShipToAddress__", "sectionName": "ShipTo" },
                        "ShipToCompany__": { "name": "ShipToCompany__", "sectionName": "ShipTo" },
                        "ShipToContact__": { "name": "ShipToContact__", "sectionName": "ShipTo" },
                        "ShipToEmail__": { "name": "ShipToEmail__", "sectionName": "ShipTo" },
                        "ShipToPhone__": { "name": "ShipToPhone__", "sectionName": "ShipTo" },
                        "ShipToAddress": { "name": "ShipToAddress", "TechnicalData": true, "sectionName": "ShipTo" }
                    },
                    "byLine": {
                        /*PAC PR Items (table) field name: PO items (form) field name */
                        "PRSubmissionDateTime__": "PRSubmissionDateTime__",
                        "CompanyCode__": "ItemCompanyCode__",
                        "Currency__": "ItemCurrency__",
                        "PRNumber__": "PRNumber__",
                        "LineNumber__": "PRLineNumber__",
                        "ExchangeRate__": "ItemExchangeRate__",
                        "ItemType__": "ItemType__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "CostCenterId__": "ItemCostCenterId__",
                        "ProjectCode__": "ProjectCode__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "TaxCode__": "ItemTaxCode__",
                        "TaxRate__": "ItemTaxRate__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "CostType__": "CostType__",
                        "GLAccount__": "ItemGLAccount__",
                        "Group__": "ItemGroup__",
                        "StartDate__": "ItemStartDate__",
                        "EndDate__": "ItemEndDate__",
                        "RequestedDeliveryDate__": "ItemRequestedDeliveryDate__",
                        "RequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeFullPath__": "SupplyTypeFullPath__",
                        "SupplyTypeId__": "SupplyTypeId__",
                        "RequesterDN__": "RequesterDN__",
                        "BuyerDN__": "BuyerDN__",
                        "RecipientDN__": "RecipientDN__",
                        "PRRUIDEX__": "PRRUIDEX__",
                        "Status__": "Status__",
                        "BudgetID__": "BudgetID__",
                        "VendorNumber__": "RequestedVendor__",
                        "ItemInCxml__": "ItemInCxml__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "Locked__": "Locked__",
                        "LeadTime__": "LeadTime__",
                        "NetAmount__": "ItemRequestedAmount__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__",
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    delete Items.PRItemsToPO.mappings.common.ShipToCompany__;
                    delete Items.PRItemsToPO.mappings.common.DeliveryAddressID__;
                    delete Items.PRItemsToPO.mappings.common.ShipToAddress__;
                    Items.PRItemsToPO.mappings.byLine["ShipToCompany__"] = "ItemShipToCompany__";
                    Items.PRItemsToPO.mappings.byLine["DeliveryAddressID__"] = "ItemDeliveryAddressID__";
                    Items.PRItemsToPO.mappings.byLine["ShipToAddress__"] = "ItemShipToAddress__";
                }
            });
            Items.GRItemsDBInfo = {
                "docType": "GR",
                "table": "P2P - Goods receipt - Items__",
                "fields": [
                    "OrderNumber__",
                    { "name": "LineNumber__", "type": "int" },
                    "GRNumber__",
                    "Status__",
                    "RequisitionNumber__",
                    "CompanyCode__",
                    "DeliveryNote__",
                    { "name": "Quantity__", "type": "double" },
                    { "name": "Amount__", "type": "double" },
                    { "name": "InvoicedQuantity__", "type": "double" },
                    { "name": "InvoicedAmount__", "type": "double" },
                    { "name": "DeliveryDate__", "type": "date" },
                    { "name": "DeliveryCompleted__", "type": "bool" },
                    { "name": "ExchangeRate__", "type": "double" },
                    "BudgetID__",
                    "ItemUnit__",
                    "ItemType__",
                    "ItemUnitDescription__",
                    "CostType__",
                    "ProjectCode__",
                    "ProjectCodeDescription__",
                    "WBSElement__",
                    "WBSElementID__",
                    "InternalOrder__",
                    "FreeDimension1__",
                    "FreeDimension1ID__",
                    "IsReplenishmentItem__"
                ]
            };
            Items.GRItemsSynchronizeConfig = {
                "tableKey": "GRNumber__",
                "lineKey": "LineNumber__",
                "dbInfo": Items.GRItemsDBInfo,
                "formTable": "LineItems__",
                "computedData": null,
                "mappings": {
                    "common": {
                        "CompanyCode__": "CompanyCode__",
                        "DeliveryDate__": "DeliveryDate__",
                        "GRNumber__": "GRNumber__",
                        "DeliveryNote__": "DeliveryNote__"
                    },
                    "byLine": {
                        "RequisitionNumber__": "RequisitionNumber__",
                        "OrderNumber__": "OrderNumber__",
                        "LineNumber__": "LineNumber__",
                        "Quantity__": "ReceivedQuantity__",
                        "Amount__": "NetAmount__",
                        "InvoicedAmount__": { "name": "InvoicedAmount", "computed": true },
                        "InvoicedQuantity__": { "name": "InvoicedQuantity", "computed": true },
                        "DeliveryCompleted__": "DeliveryCompleted__",
                        "Status__": { "name": "Status", "computed": true },
                        "BudgetID__": "BudgetID__",
                        "ExchangeRate__": "ExchangeRate__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "ItemType__": "ItemType__",
                        "CostType__": "CostType__",
                        "ProjectCode__": "ProjectCode__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__"
                    }
                }
            };
            Items.POItemsToEditPORequest = {
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineItems__",
                "formLineNumber": "LineNumber__",
                "errorMessages": {},
                "mappings": {
                    "common": {
                        "PONumber__": "OrderNumber__",
                        "CompanyCode__": "CompanyCode__",
                        "Currency__": "Currency__",
                        "PORUIDEX__": "RUIDEX",
                        "VendorName__": "VendorName__",
                        "VendorNumber__": "VendorNumber__",
                        "ShipToCompany__": "ShipToCompany__"
                    },
                    "byLine": {
                        /*PAC PO Items (table) field name: PO items (form) field name */
                        "PRSubmissionDateTime__": "PRSubmissionDateTime__",
                        "PRRUIDEX__": "PRRUIDEX__",
                        "PRNumber__": "PRNumber__",
                        "PRLineNumber__": "PRLineNumber__",
                        "RequesterDN__": "RequesterDN__",
                        "BuyerDN__": "BuyerDN__",
                        "RecipientDN__": "RecipientDN__",
                        "RecipientLogin__": { "name": "RecipientLogin", "computed": true },
                        "LineNumber__": "LineItemNumber__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "RequestedDeliveryDate__": "ItemRequestedDeliveryDate__",
                        "RequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
                        "CostCenterId__": "ItemCostCenterId__",
                        "ProjectCode__": "ProjectCode__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "OrderedQuantity__": "ItemQuantity__",
                        "ReceivedQuantity__": "ItemTotalDeliveredQuantity__",
                        "DeliveryDate__": "ItemDeliveryDate__",
                        "Status__": { "name": "Status", "computed": true },
                        "CompletelyDelivered__": "ItemDeliveryComplete__",
                        "TaxCode__": "ItemTaxCode__",
                        "TaxRate__": "ItemTaxRate__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "NetAmount__": "ItemNetAmount__",
                        "ItemReceivedAmount__": "ItemReceivedAmount__",
                        "CostType__": "CostType__",
                        "GLAccount__": "ItemGLAccount__",
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeId__": "SupplyTypeId__",
                        "Group__": "ItemGroup__",
                        "BudgetID__": "BudgetID__",
                        "ExchangeRate__": "ItemExchangeRate__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "ItemType__": "ItemType__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "ItemRequestedAmount__": "ItemRequestedAmount__",
                        "Currency__": "ItemCurrency__"
                    }
                }
            };
            Items.POItemsToSES = {
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineServices__",
                "errorMessages": {},
                "mappings": {
                    "common": {},
                    "byLine": {
                        /*PAC PO Items (table) field name: SES items (form) field name */
                        "Description__": "Description__",
                        "ItemUnit__": "UnitOfMeasure__",
                        "UnitPrice__": "UnitPrice__"
                    }
                }
            };
            Items.POItemsToASN = {
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineItems__",
                "errorMessages": {},
                "mappings": {
                    "common": {
                        "VendorName__": "VendorName__"
                    },
                    "byLine": {
                        /*PAC PO Items (table) field name: ASN items (form) field name */
                        "PONumber__": "ItemPONumber__",
                        "Description__": "ItemDescription__",
                        "OrderedQuantity__": "ItemPOLineQuantity__",
                        "ItemUnit__": "ItemUOM__"
                    }
                }
            };
            Items.POItemsToGR = {
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineItems__",
                "formLineNumber": "LineNumber__",
                "errorMessages": {},
                "mappings": {
                    "common": {
                        "CompanyCode__": "CompanyCode__",
                        "PONumber__": "OrderNumber__"
                    },
                    "byLine": {
                        "LineNumber__": "LineNumber__",
                        "CatalogReference__": "Number__",
                        "Description__": "Description__",
                        "OrderedQuantity__": "OrderedQuantity__",
                        "UnitPrice__": "UnitPrice__",
                        "NetAmount__": "ItemOrderedAmount__",
                        "CostCenterId__": "CostCenterId__",
                        "CostCenterName__": "CostCenter__",
                        "ProjectCode__": "ProjectCode__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "Group__": "Group__",
                        "RequestedDeliveryDate__": "RequestedDeliveryDate__",
                        "ExchangeRate__": "ExchangeRate__",
                        "PRNumber__": "RequisitionNumber__",
                        "PONumber__": "OrderNumber__",
                        "RequesterDN__": "RequesterDN__",
                        "RecipientDN__": "RecipientDN__",
                        "BudgetID__": "BudgetID__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemType__": "ItemType__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "CostType__": "CostType__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__",
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    Items.POItemsToGR.mappings.byLine["ShipToCompany__"] = "ItemShipToCompany__";
                    Items.POItemsToGR.mappings.byLine["DeliveryAddressID__"] = "ItemDeliveryAddressID__";
                    Items.POItemsToGR.mappings.byLine["ShipToAddress__"] = "ItemShipToAddress__";
                }
            });
            Items.POItemsToPR = {
                "dbInfo": Items.POItemsDBInfo,
                "formTable": "LineItems__",
                "formLineNumber": "LineNumber__",
                "mappings": {
                    "common": {
                        "CompanyCode__": "CompanyCode__"
                    },
                    "byLine": {
                        "LineNumber__": "LineNumber__",
                        "ItemType__": "ItemType__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "OrderedQuantity__": "ItemQuantity__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "NetAmount__": "ItemNetAmount__",
                        "Currency__": "ItemCurrency__",
                        "ExchangeRate__": "ItemExchangeRate__",
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeId__": "SupplyTypeID__",
                        "VendorName__": "VendorName__",
                        "VendorNumber__": "VendorNumber__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "CostCenterId__": "ItemCostCenterId__",
                        "ProjectCode__": "ProjectCode__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "TaxCode__": "ItemTaxCode__",
                        "TaxRate__": "ItemTaxRate__",
                        "CostType__": "CostType__",
                        "GLAccount__": "ItemGLAccount__",
                        "Group__": "ItemGroup__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__",
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            Sys.Parameters.GetInstance("PAC").OnLoad(function () {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    Items.POItemsToPR.mappings.byLine["ShipToCompany__"] = "ItemShipToCompany__";
                    Items.POItemsToPR.mappings.byLine["DeliveryAddressID__"] = "ItemDeliveryAddressID__";
                    Items.POItemsToPR.mappings.byLine["ShipToAddress__"] = "ItemShipToAddress__";
                }
            });
            Items.PRItemsToPR = {
                "dbInfo": Items.PRItemsDBInfo,
                "formTable": "LineItems__",
                "formLineNumber": "LineNumber__",
                "mappings": {
                    "common": {
                        "CompanyCode__": "CompanyCode__",
                        "Reason__": "Reason__"
                    },
                    "byLine": {
                        "LineNumber__": "LineNumber__",
                        "ItemType__": "ItemType__",
                        "CatalogReference__": "ItemNumber__",
                        "Description__": "ItemDescription__",
                        "Quantity__": "ItemQuantity__",
                        "UnitPrice__": "ItemUnitPrice__",
                        "NetAmount__": "ItemNetAmount__",
                        "Currency__": "ItemCurrency__",
                        "ExchangeRate__": "ItemExchangeRate__",
                        "SupplyTypeName__": "SupplyTypeName__",
                        "SupplyTypeId__": "SupplyTypeID__",
                        "VendorName__": "VendorName__",
                        "VendorNumber__": "VendorNumber__",
                        "CostCenterName__": "ItemCostCenterName__",
                        "CostCenterId__": "ItemCostCenterId__",
                        "ProjectCode__": "ProjectCode__",
                        "ProjectCodeDescription__": "ProjectCodeDescription__",
                        "WBSElement__": "WBSElement__",
                        "WBSElementID__": "WBSElementID__",
                        "InternalOrder__": "InternalOrder__",
                        "TaxCode__": "ItemTaxCode__",
                        "TaxRate__": "ItemTaxRate__",
                        "CostType__": "CostType__",
                        "GLAccount__": "ItemGLAccount__",
                        "Group__": "ItemGroup__",
                        "ItemUnit__": "ItemUnit__",
                        "ItemUnitDescription__": "ItemUnitDescription__",
                        "NoGoodsReceipt__": "NoGoodsReceipt__",
                        "ShipToCompany__": "ItemShipToCompany__",
                        "DeliveryAddressID__": "ItemDeliveryAddressID__",
                        "ShipToAddress__": "ItemShipToAddress__",
                        "FreeDimension1__": "FreeDimension1__",
                        "FreeDimension1ID__": "FreeDimension1ID__",
                        "IsReplenishmentItem__": "IsReplenishmentItem__",
                        "WarehouseID__": "WarehouseID__",
                        "WarehouseName__": "WarehouseName__"
                    }
                }
            };
            /**
             * Compare 2 objects
             *
             * If both are plain objects, compare their stringified version
             * Otherwise do standard `!=` comparison
             */
            function isDifferent(item1, item2) {
                if (Sys.Helpers.IsPlainObject(item1) || Sys.Helpers.IsPlainObject(item2)) {
                    return JSON.stringify(item1) != JSON.stringify(item2);
                }
                else {
                    return item1 != item2;
                }
            }
            /**
             * Fill the document items form according to the selected database items.
             * @param {array} dbItems selected DB items used to fill form items.
             * @param {object} config declaring some information used to fill data (table, fields mapping, ...).
             * @param {object} options
             * @throws {Error} any error
             */
            function FillFormItems(dbItems, config, options) {
                options = options || {};
                var table = Data.GetTable(config.formTable);
                var commonValues = {};
                var offset;
                // only use when updating items
                var itemsByLineKey;
                if (options.updateItems) {
                    offset = 0;
                    itemsByLineKey = {};
                    Sys.Helpers.Data.ForEachTableItem(table, function (item /*, i*/) {
                        var lineKey = config.dbInfo.lineKey.map(function (field) {
                            return item.GetValue(config.mappings.byLine[field]);
                        }).join("|");
                        itemsByLineKey[lineKey] = item;
                    });
                }
                else {
                    if (options.resetItems) {
                        table.SetItemCount(0);
                        offset = 0;
                    }
                    else {
                        offset = table.GetItemCount();
                        // If only one item is returned, we check if it's an empty line
                        if (offset === 1 && Lib.Purchasing.IsLineItemEmpty(table.GetItem(0))) {
                            offset = 0;
                        }
                    }
                    table.SetItemCount(offset + dbItems.length);
                }
                // use map in order to keep uniqueness
                var fieldsInError = {};
                dbItems.forEach(function (dbItem, index) {
                    var item;
                    if (options.updateItems) {
                        var lineKey = config.dbInfo.lineKey.map(function (field) {
                            return dbItem.GetValue(field);
                        }).join("|");
                        item = itemsByLineKey[lineKey];
                        if (!item) {
                            Log.Info("Item not present for line key: " + lineKey);
                            return;
                        }
                    }
                    else {
                        item = table.GetItem(offset + index);
                        if (config.formLineNumber) {
                            item.SetValue(config.formLineNumber, (offset + 1 + index) * 10);
                        }
                    }
                    // Set values on each line
                    config.dbInfo.fields.forEach(function (field) {
                        var fieldName = Sys.Helpers.IsString(field) ? field : field.name;
                        var fieldValue = null;
                        if (field.foreign) {
                            if (options.foreignData) {
                                fieldValue = options.foreignData[dbItem.GetValue(config.dbInfo.localKey)][field.name];
                            }
                        }
                        else if (field.TechnicalData) {
                            var technicalData = dbItem.GetValue("TechnicalData__");
                            if (technicalData) {
                                fieldValue = JSON.parse(technicalData)[fieldName];
                            }
                        }
                        else {
                            fieldValue = dbItem.GetValue(fieldName);
                        }
                        // normalize multiline string values
                        if (Sys.Helpers.IsString(fieldValue)) {
                            fieldValue = fieldValue.replace(/\r\n/g, "\n");
                        }
                        if (fieldName in config.mappings.byLine) {
                            item.SetValue(config.mappings.byLine[fieldName], fieldValue);
                        }
                        if (config.mappings.common[fieldName]) {
                            if (commonValues[fieldName]) {
                                if (!commonValues[fieldName].hasMultipleValues && isDifferent(commonValues[fieldName].value, fieldValue)) {
                                    commonValues[fieldName].hasMultipleValues = true;
                                }
                            }
                            else if (!options.doNotFillCommonFields) {
                                commonValues[fieldName] = { "value": fieldValue }; // Save first value found
                            }
                            // If value at line level is different from first found => set field in error
                            // By default, don't ignore having different multiple values at line item level. Excepted if 'ignoreMultipleValuesAtItemLevel' is set to 'true' on mapping settings.
                            if (fieldName in config.mappings.byLine && commonValues[fieldName] && commonValues[fieldName].hasMultipleValues && commonValues[fieldName].value != fieldValue && !config.mappings.common[fieldName].ignoreMultipleValuesAtItemLevel) {
                                item.SetError(config.mappings.byLine[fieldName], config.errorMessages[fieldName]);
                            }
                        }
                    });
                    if (!options.updateItems && Sys.Helpers.IsFunction(options.fillItem)) {
                        var lineStillRelevant = options.fillItem(dbItem, item, options);
                        if (!lineStillRelevant) {
                            offset -= 1;
                            table.SetItemCount(offset + dbItems.length);
                        }
                    }
                });
                // Set header data
                if (!options.doNotFillCommonFields) {
                    // Check presence of multiple values, clean fields when needed
                    var sectionsToClean_1 = {};
                    Sys.Helpers.Object.ForEach(commonValues, function (commonValue, key) {
                        var fieldName = config.mappings.common[key].name || config.mappings.common[key];
                        if (!commonValue.hasMultipleValues) // unique value found
                         {
                            if (config.mappings.common[key].TechnicalData) {
                                Sys.TechnicalData.SetValue(fieldName, commonValue.value);
                            }
                            else {
                                Data.SetValue(fieldName, commonValue.value);
                            }
                        }
                        else if (config.mappings.common[key].sectionName) // multiple values found, all fields of the same section will be cleared
                         {
                            Log.Warn("Multiple values found for attribute '" + key + "', section '" + config.mappings.common[key].sectionName + "' will be cleared");
                            sectionsToClean_1[config.mappings.common[key].sectionName] = true;
                        }
                        else // multiple values found, set first found value on header
                         {
                            Log.Warn("Multiple values found for attribute '" + fieldName + "', setting first found value");
                            Data.SetValue(fieldName, commonValue.value);
                            fieldsInError[fieldName] = true;
                        }
                    });
                    // Clean sections
                    Sys.Helpers.Object.ForEach(sectionsToClean_1, function (value, key) {
                        Sys.Helpers.Object.ForEach(config.mappings.common, function (value2 /*, key2*/) {
                            if (value2.name && value2.sectionName == key) {
                                if (value2.TechnicalData) {
                                    Sys.TechnicalData.SetValue(value2.name, "");
                                }
                                else {
                                    Data.SetValue(value2.name, "");
                                }
                            }
                        });
                    });
                }
                // transform this map to array
                fieldsInError = Object.keys(fieldsInError);
                return fieldsInError;
            }
            Items.FillFormItems = FillFormItems;
            function IsLineItemsInError() {
                var error = false;
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item /*, i*/) {
                    for (var itemName in Items.PRItemsToPO.errorMessages) {
                        if (item.GetError(Items.PRItemsToPO.mappings.byLine[itemName])) {
                            error = true;
                            break;
                        }
                    }
                    return error;
                });
                return error;
            }
            Items.IsLineItemsInError = IsLineItemsInError;
            function ComputePRAmounts(items) {
                var netAmountLocal = new Sys.Decimal(0);
                var count = items.length;
                var i, net, totalNetForeign = new Sys.Decimal(0), rowItem;
                var isMonoCurrency = true;
                var hasForeignCurrency = false;
                var previousCurrency;
                for (i = 0; i < count; i++) {
                    rowItem = items[i];
                    net = rowItem.GetValue("ItemNetAmount__");
                    if (!Sys.Helpers.IsEmpty(net)) {
                        var itemCurrency = rowItem.GetValue("ItemCurrency__");
                        isMonoCurrency = (i == 0) || (isMonoCurrency && (previousCurrency == itemCurrency));
                        if (itemCurrency && itemCurrency != Variable.GetValueAsString("companyCurrency")) {
                            if (isMonoCurrency) {
                                totalNetForeign = totalNetForeign.add(net);
                            }
                            net = new Sys.Decimal(net).mul(rowItem.GetValue("ItemExchangeRate__") || Data.GetValue("ExchangeRate__")).toNumber();
                            hasForeignCurrency = true;
                        }
                        netAmountLocal = netAmountLocal.add(net);
                        previousCurrency = itemCurrency;
                    }
                }
                var totalNetAmount = isMonoCurrency && hasForeignCurrency ? totalNetForeign : netAmountLocal;
                var currency = isMonoCurrency && hasForeignCurrency ? previousCurrency : Variable.GetValueAsString("companyCurrency");
                return {
                    "netAmountLocal": netAmountLocal.toNumber(),
                    "isMonoCurrency": isMonoCurrency,
                    "hasForeignCurrency": hasForeignCurrency,
                    "TotalNetAmount": totalNetAmount.toNumber(),
                    "Currency": currency
                };
            }
            Items.ComputePRAmounts = ComputePRAmounts;
            function ComputeTotalAmount() {
                // Used only in PO form
                var currency = Data.GetValue("Currency__");
                var totNet = new Sys.Decimal(0), locTotNet = new Sys.Decimal(0), net, rate;
                var openOrderLocalCurrency = new Sys.Decimal(0);
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item /*, i*/) {
                    if (currency === item.GetValue("ItemCurrency__")) {
                        net = item.GetValue("ItemNetAmount__");
                        rate = item.GetValue("ItemExchangeRate__") || Data.GetValue("ExchangeRate__");
                        if (!Sys.Helpers.IsEmpty(net)) {
                            totNet = totNet.add(net);
                            locTotNet = locTotNet.add(new Sys.Decimal(net).mul(rate));
                        }
                    }
                    openOrderLocalCurrency = openOrderLocalCurrency.add(new Sys.Decimal(item.GetValue("ItemUndeliveredQuantity__") || 0).mul(item.GetValue("ItemUnitPrice__") || 0).mul(rate || 0));
                });
                if (!rate) {
                    // When there is no item, but we have Additional Fees, rate is undefined
                    // and the Additional Fees loop crashes and nothing more is done (order button is still clickable)
                    // So set a default value
                    rate = 1;
                }
                Sys.Helpers.Data.ForEachTableItem("AdditionalFees__", function (item /*, i*/) {
                    net = item.GetValue("Price__");
                    if (!Sys.Helpers.IsEmpty(net)) {
                        totNet = totNet.add(net);
                        locTotNet = locTotNet.add(new Sys.Decimal(net).mul(rate));
                    }
                });
                // Missing header fields
                Data.SetValue("TotalNetAmount__", totNet.toNumber());
                Data.SetValue("TotalNetLocalCurrency__", locTotNet.toNumber());
                Data.SetValue("OpenOrderLocalCurrency__", openOrderLocalCurrency.toNumber());
            }
            Items.ComputeTotalAmount = ComputeTotalAmount;
            /**
             * Load data from foreign table
             * @param {array} dbItems selected DB items used to fill form items.
             * @param {object} config declaring some information used to fill data (table, fields mapping, ...).
             * @returns {promise}
             */
            function LoadForeignData(dbItems, config) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (config.foreignTable) {
                        Log.Info(">> LoadForeignData");
                        // Get all involved ForeignData
                        var foreignKey_1 = [];
                        dbItems.forEach(function (dbItem /*, index*/) {
                            foreignKey_1.push(dbItem.GetValue(config.foreignKeyValue));
                        });
                        // select items from all foreignData
                        var foreignFilter_1 = "(|" + Sys.Helpers.Array.Map(foreignKey_1, function (v) {
                            return "(" + config.foreignKey + "=" + v + ")";
                        }).join("") + ")";
                        Log.Info("Fetching information from Purchase requisitions: " + foreignFilter_1);
                        // Build list of fields to retrieve from foreign table. Add foreignKey
                        var foreignFields_1 = Sys.Helpers.Array.Map(Sys.Helpers.Array.Filter(config.fields, function (field) {
                            return field.foreign;
                        }), function (field) {
                            return field.name;
                        });
                        foreignFields_1.push(config.foreignKey);
                        Sys.GenericAPI.Query(config.foreignTable, foreignFilter_1, foreignFields_1, function (queryResult, error) {
                            if (error) {
                                reject("LoadForeignData: error querying " + config.foreignTable + " with filter: " + foreignFilter_1 + ".Details: " + error);
                            }
                            else if (queryResult.length === 0) {
                                reject("LoadForeignData: cannot find any " + config.foreignTable + " with filter: " + foreignFilter_1);
                            }
                            else {
                                var foreignData_1 = {}; // Map of RUIDEX to Map of foreign fields
                                var _loop_1 = function (idx) {
                                    var dbDoc = queryResult[idx];
                                    foreignData_1[dbDoc[config.foreignKey]] = {};
                                    foreignFields_1.forEach(function (field) {
                                        var fieldValue = dbDoc[field];
                                        // normalize multiline string values
                                        if (Sys.Helpers.IsString(fieldValue)) {
                                            fieldValue = fieldValue.replace(/\r\n/g, "\n");
                                        }
                                        foreignData_1[dbDoc[config.foreignKey]][field] = fieldValue;
                                    });
                                };
                                for (var idx in queryResult) {
                                    _loop_1(idx);
                                }
                                Log.Info("<< foreignData: " + JSON.stringify(foreignData_1));
                                resolve(foreignData_1);
                            }
                        });
                    }
                    else {
                        resolve({});
                    }
                });
            }
            Items.LoadForeignData = LoadForeignData;
            /**
             * Retrieves items for document. Returns a map a item array by itemKey.
             * @param {object} itemsDBInfo information about items in database
             * @param {string} filter allowing to select items
             * @param {string} itemKey item field used to feed the key map
             * @returns {Sys.Helpers.IPromise} returns items in a promise
             */
            function GetItemsForDocument(itemsDBInfo, filter, itemKey) {
                !itemsDBInfo.fieldsMap && InitDBFieldsMap(itemsDBInfo);
                Log.Info("Retrieve all items with filter " + filter);
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    Sys.GenericAPI.Query(itemsDBInfo.table, filter, ["*"], function (dbItems, error) {
                        if (error) {
                            reject("GetItemsForDocument: cannot get items. Details: " + error);
                        }
                        else {
                            var items_1 = {};
                            dbItems.forEach(function (dbItem) {
                                var itemKeyValue = dbItem.GetValue(itemKey);
                                if (!(itemKeyValue in items_1)) {
                                    items_1[itemKeyValue] = [];
                                }
                                items_1[itemKeyValue].push(dbItem);
                            });
                            Log.Info(dbItems.length + " items retrieved");
                            resolve(items_1);
                        }
                    }, "", null, {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult,
                        fieldToTypeMap: itemsDBInfo.fieldsMap,
                        bigQuery: true,
                        queryOptions: "ReturnNullAttributes=1"
                    });
                });
            }
            Items.GetItemsForDocument = GetItemsForDocument;
            /**
             * Fill the LineItemNumber field of line items (by steps of 10)
             */
            function NumberLineItems() {
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item, i) {
                    var lineItemNumber = parseInt(item.GetValue("LineItemNumber__"), 10);
                    var currentLineNumber = (i + 1) * 10;
                    if (lineItemNumber !== currentLineNumber) {
                        item.SetValue("LineItemNumber__", currentLineNumber);
                    }
                });
            }
            Items.NumberLineItems = NumberLineItems;
            // initialization
            InitDBFieldsMap(Items.PRItemsDBInfo);
            InitDBFieldsMap(Items.POItemsDBInfo);
            InitDBFieldsMap(Items.GRItemsDBInfo);
            var SyncFieldManipulator = /** @class */ (function () {
                function SyncFieldManipulator(dbField, byLine, config) {
                    this.dbField = dbField;
                    this.byLine = byLine;
                    this.config = config;
                    this.formField = config.mappings[byLine ? "byLine" : "common"][dbField];
                    this.dbType = config.dbInfo.fieldsMap[dbField] || "string";
                }
                SyncFieldManipulator.prototype.GetFormValue = function (data) {
                    if (this.formField.computed === true) {
                        var formFieldName = this.formField.name || this.dbField;
                        if (this.byLine) {
                            if (this.dbField !== this.config.lineKey) {
                                var formLineKey = GetSyncFieldManipulator(this.config.lineKey, true, this.config);
                                var formLineValue = formLineKey.GetFormValue(data);
                                return formLineValue && this.config.computedData.byLine[formLineValue] ? this.config.computedData.byLine[formLineValue][formFieldName] : undefined;
                            }
                        }
                        else {
                            return this.config.computedData.common[formFieldName];
                        }
                    }
                    return data.GetValue(this.formField.name || this.formField);
                };
                SyncFieldManipulator.prototype.SetDBValue = function (record, value) {
                    Sys.Helpers.Database.SetRecordValue(record, this.dbField, value, this.dbType);
                };
                SyncFieldManipulator.prototype.GetDBValue = function (record) {
                    return Sys.Helpers.Database.GetRecordValue(record, this.dbField, this.dbType);
                };
                return SyncFieldManipulator;
            }());
            var syncFieldManipulatorsCache = {};
            function GetSyncFieldManipulator(dbField, byLine, config) {
                var manipulator = syncFieldManipulatorsCache[dbField];
                if (!manipulator) {
                    manipulator = syncFieldManipulatorsCache[dbField] = new SyncFieldManipulator(dbField, byLine, config);
                }
                return manipulator;
            }
            function FillRecord(formLine, dbLine, config) {
                if (config.mappings.common) {
                    Object.keys(config.mappings.common).forEach(function (dbField) {
                        var formField = GetSyncFieldManipulator(dbField, false, config);
                        var formValue = formField.GetFormValue(Data);
                        if (Sys.Helpers.IsDefined(formValue)) {
                            formField.SetDBValue(dbLine, formValue);
                        }
                    });
                }
                if (config.mappings.byLine) {
                    Object.keys(config.mappings.byLine).forEach(function (dbField) {
                        var formField = GetSyncFieldManipulator(dbField, true, config);
                        var formValue = formField.GetFormValue(formLine);
                        if (Sys.Helpers.IsDefined(formValue)) {
                            formField.SetDBValue(dbLine, formValue);
                        }
                    });
                }
            }
            /**
             * Synchronizes the items based on the parent document's items so that there are the same number of items and the items data match :
             *   - deletes items that are no longer in the parent document
             *   - inserts new items that were not present in the items table
             *   - updates items that are different
             * @param {object} config The configuration allowing to access to the form & DB table, the fields mapping, etc.
             * see PRItemsSynchronizeConfig or POItemsSynchronizeConfig
             * @throws {Error} any error
             */
            function Synchronize(config, impactedItemKeys) {
                if (Sys.ScriptInfo.IsServer()) {
                    !config.dbInfo.fieldsMap && InitDBFieldsMap(config.dbInfo);
                    syncFieldManipulatorsCache = {}; // reset cache
                    var formTableKey = GetSyncFieldManipulator(config.tableKey, false, config);
                    var filter = config.tableKey + "=" + formTableKey.GetFormValue(Data);
                    var formLineKey_1 = GetSyncFieldManipulator(config.lineKey, true, config);
                    var dbTable_1 = [];
                    Log.Info("Requesting items from " + config.dbInfo.table + " with filter " + filter);
                    g.Query.Reset();
                    g.Query.SetSpecificTable(config.dbInfo.table);
                    g.Query.SetFilter(filter);
                    if (g.Query.MoveFirst()) {
                        var record = g.Query.MoveNextRecord();
                        while (record) {
                            dbTable_1.push({
                                "record": record,
                                "key": formLineKey_1.GetDBValue(record)
                            });
                            record = g.Query.MoveNextRecord();
                        }
                    }
                    else if (g.Query.GetLastError()) {
                        throw new Error("Synchronize: cannot get items. Details: " + g.Query.GetLastErrorMessage());
                    }
                    var title_1;
                    var message_1;
                    var behaviorName_1 = null;
                    Sys.Helpers.Data.ForEachTableItem(config.formTable, function (formLine) {
                        var key = formLineKey_1.GetFormValue(formLine);
                        var dbLine = Sys.Helpers.Array.Remove(dbTable_1, function (dbLine) { return dbLine.key === key; });
                        // only update impacted line items
                        if (!impactedItemKeys || Sys.Helpers.Array.IndexOf(impactedItemKeys, key) >= 0) {
                            if (!dbLine) {
                                Log.Info("Creating new record in " + config.dbInfo.table + " with key " + key);
                                dbLine = { record: Process.CreateTableRecord(config.dbInfo.table), key: key };
                            }
                            else {
                                // Check the sequence is duplicated
                                if (Sys.Helpers.IsUndefined(config.ParentRuidKey) == false && dbLine.record.GetVars().GetValue(config.ParentRuidKey, 0) != Data.GetValue("RUIDEX")) {
                                    title_1 = "_DuplicateSequenceTitle" + config.dbInfo.docType;
                                    message_1 = "_DuplicateSequenceMessage" + config.dbInfo.docType;
                                    behaviorName_1 = "ClearSequence";
                                    Lib.CommonDialog.NextAlert.Define(title_1, message_1, { isError: true, behaviorName: behaviorName_1 });
                                    return true; // stop the loop
                                }
                                Log.Info("Updating record in " + config.dbInfo.table + " with key " + key);
                            }
                            FillRecord(formLine, dbLine.record, config);
                            dbLine.record.Commit();
                            if (dbLine.record.GetLastError()) {
                                throw new Error("Synchronize: commit item failed. Details: " + dbLine.record.GetLastErrorMessage());
                            }
                        }
                    });
                    if (typeof message_1 == "string") {
                        return false;
                    }
                    // delete remaining dbTable lines
                    dbTable_1.forEach(function (dbLine) {
                        Log.Info("Deleting record from " + config.dbInfo.table + " with key " + dbLine.key);
                        dbLine.record.Delete();
                        if (dbLine.record.GetLastError()) {
                            throw new Error("Synchronize: cannot delete item from table. Details: " + dbLine.record.GetLastErrorMessage());
                        }
                    });
                    return true;
                }
                throw "Not implemented on client side";
            }
            Items.Synchronize = Synchronize;
            function GetUpdatableTransport(ruidEx, resumeWithActionData) {
                var transport = Process.GetUpdatableTransport(ruidEx);
                var externalVars = transport.GetExternalVars();
                externalVars.AddValue_String("resumeWithActionData", resumeWithActionData, true);
                return transport;
            }
            /**
             * Resume the document identified by its ProcessName and RuidEx with an action.
             * We try first to resume synchronously and asynchronously if the previous attempt fails.
             * @param {string} processName The name of document process. (ex. Purchase Order)
             * @param {string} ruidEx The unique long ID of document.
             * @param {string} action The executed action once document is resumed.
             * @throws {Error} any error
             */
            function ResumeDocumentToSynchronizeItems(processName, ruidEx, action, resumeWithActionData) {
                if (Sys.ScriptInfo.IsServer()) {
                    Log.Time("ResumeDocumentToSynchronizeItems");
                    Log.Info("Resume document " + processName + " with ID: " + ruidEx + " and action: " + action);
                    // Try to resume synchronously
                    var transport = GetUpdatableTransport(ruidEx, resumeWithActionData);
                    var ok = transport.ResumeWithAction(action);
                    // If the sync attempt fails, we try an asynchronous one
                    if (!ok) {
                        Log.Info("Try a ResumeWithActionAsync because the synchronous attempt failed, error: " + transport.GetLastErrorMessage());
                        // Here we need to recreate a new updatable transport. Some updated data may be lost...
                        transport = GetUpdatableTransport(ruidEx, resumeWithActionData);
                        ok = transport.ResumeWithActionAsync(action);
                    }
                    Log.TimeEnd("ResumeDocumentToSynchronizeItems");
                    if (!ok) {
                        throw new Error("ResumeDocumentToSynchronizeItems: cannot resume document. Details: " + transport.GetLastErrorMessage());
                    }
                }
                else {
                    throw "Not implemented on client side";
                }
            }
            Items.ResumeDocumentToSynchronizeItems = ResumeDocumentToSynchronizeItems;
            /**
             * Synchronous version used for server based on the common version.
             * Retrieves items for document. Returns a map a item array by itemKey.
             * @param {object} itemsDBInfo information about items in database
             * @param {string} filter allowing to select items
             * @param {string} itemKey item field used to feed the key map
             * @returns {object} items for document.
             * @throws {Error} any error
             */
            function GetItemsForDocumentSync(itemsDBInfo, filter, itemKey) {
                var retItems = null;
                var lastError = null;
                Lib.Purchasing.Items.GetItemsForDocument(itemsDBInfo, filter, itemKey)
                    .Then(function (items) { retItems = items; })
                    .Catch(function (err) { lastError = err; });
                if (lastError) {
                    throw new Error(lastError);
                }
                return retItems;
            }
            Items.GetItemsForDocumentSync = GetItemsForDocumentSync;
            /**
             * Converts a TableRow control to a database record (query result object) in order to use the same APIs .
             * @param {object} tableRow a row of the specified Table control
             * @returns {object} a database record object
             */
            function ConvertTableRowToDBRecord(tableRow) {
                if (Sys.ScriptInfo.IsClient()) {
                    return {
                        GetValue: function (fieldName) {
                            if (fieldName in tableRow) {
                                return tableRow[fieldName].GetValue();
                            }
                            return null;
                        }
                    };
                }
                Log.Error("Not implemented on server side");
            }
            Items.ConvertTableRowToDBRecord = ConvertTableRowToDBRecord;
            function FillPRFromAncestor(dbItems, ancestorItemsToPR) {
                try {
                    Lib.Purchasing.Items.FillFormItems(dbItems, ancestorItemsToPR);
                }
                catch (e) {
                    Log.Error("FillPRFromAncestor error: " + e);
                }
            }
            Items.FillPRFromAncestor = FillPRFromAncestor;
            function PrepareFillPRFromAncestor(srcRuid, originalProcess) {
                return Sys.Helpers.Promise.Create(function (resolve, reject) {
                    if (srcRuid) {
                        var ItemsDBInfo = originalProcess === "PO" ? Lib.Purchasing.Items.POItemsDBInfo : Lib.Purchasing.Items.PRItemsDBInfo;
                        // Lib.Purchasing.AncestorItemsToPR = originalProcess === "PO" ? Lib.Purchasing.Items.POItemsToPR : Lib.Purchasing.Items.PRItemsToPR;
                        var filter_1 = originalProcess + "RUIDEX__=" + srcRuid;
                        Sys.GenericAPI.Query(ItemsDBInfo.table, filter_1, ["*"], function (dbItems, error) {
                            if (error) {
                                reject("PrepareFillPRFromAncestor: error querying ancestor items with filter. Details: " + error);
                            }
                            else if (dbItems.length === 0) {
                                reject("PrepareFillPRFromAncestor: cannot find any ancestor items with filter: " + filter_1);
                            }
                            else {
                                resolve(dbItems);
                            }
                        }, "", Items.MAXRECORDS, {
                            recordBuilder: Sys.GenericAPI.BuildQueryResult,
                            fieldToTypeMap: ItemsDBInfo.fieldsMap,
                            bigQuery: true,
                            queryOptions: "ReturnNullAttributes=1"
                        });
                    }
                    else {
                        reject("Ancestor RUID " + srcRuid + " was not found.");
                    }
                });
            }
            Items.PrepareFillPRFromAncestor = PrepareFillPRFromAncestor;
            function QueryUNSPSCSupplyType(unspsc) {
                if (unspsc) {
                    var allUNSPSC = ["(UNSPSC__=" + unspsc + ")"];
                    for (var i = unspsc.length - 2; i > 1; i -= 2) {
                        allUNSPSC.push("(UNSPSC__=" + Sys.Helpers.String.PadRight(unspsc.slice(0, i), "0", unspsc.length) + ")");
                    }
                    var companyCode = Data.GetValue("CompanyCode__") || Data.GetValue("ItemCompanyCode__");
                    var filter = "(&(|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(CompanyCode__!=*))(|" + allUNSPSC.join("") + "))";
                    var options = {
                        table: "P2P - Mapping UNSPSC to SupplyType__",
                        filter: filter,
                        sortOrder: "UNSPSC__ DESC",
                        attributes: ["SupplyTypeId__"],
                        maxRecords: 1
                    };
                    return Sys.GenericAPI.PromisedQuery(options);
                }
                return Sys.Helpers.Promise.Create(function (resolve /*, reject*/) {
                    resolve();
                });
            }
            Items.QueryUNSPSCSupplyType = QueryUNSPSCSupplyType;
            function IsQuantityBasedItem(item) {
                return item ? item.GetValue("ItemType__") !== Lib.P2P.ItemType.AMOUNT_BASED : true;
            }
            Items.IsQuantityBasedItem = IsQuantityBasedItem;
            function IsAmountBasedItem(item) {
                return item ? item.GetValue("ItemType__") === Lib.P2P.ItemType.AMOUNT_BASED : false;
            }
            Items.IsAmountBasedItem = IsAmountBasedItem;
            function IsServiceBasedItem(item) {
                return item ? item.GetValue("ItemType__") === Lib.P2P.ItemType.SERVICE_BASED : false;
            }
            Items.IsServiceBasedItem = IsServiceBasedItem;
            function GetLineItemsTypes() {
                var types = {};
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                    var itemType = item.GetValue("ItemType__");
                    if (itemType) {
                        types[itemType] = true;
                    }
                });
                return Object.keys(types);
            }
            Items.GetLineItemsTypes = GetLineItemsTypes;
            function ShouldDisplayColumn(itemTypes, visibility, status) {
                var visible = false;
                if (!Sys.Helpers.IsFunction(visibility.VisibleCondition) || visibility.VisibleCondition(status)) {
                    visible = !!Sys.Helpers.Array.Find(itemTypes, function (type) {
                        return visibility[type];
                    });
                }
                return visible;
            }
            Items.ShouldDisplayColumn = ShouldDisplayColumn;
            function UpdateItemTypeDependencies(visibilities, status) {
                var types = Lib.Purchasing.Items.GetLineItemsTypes();
                Sys.Helpers.Object.ForEach(visibilities, function (visibility, columnName) {
                    var visible = ShouldDisplayColumn(types, visibility, status);
                    var ctrl = g.Controls.LineItems__[columnName];
                    if (visible != ctrl.IsVisible()) {
                        ctrl.Hide(!visible);
                        if (visible && visibility) {
                            // !!! all cells in this column become visible => hide no visible cells
                            Sys.Helpers.Controls.ForEachTableRow(g.Controls.LineItems__, function (row) {
                                row[columnName].Hide(!visibility[row["ItemType__"].GetValue()] && !visibility.ShowAllColumnCellsWhenOneCellIsVisible);
                            });
                        }
                    }
                });
            }
            Items.UpdateItemTypeDependencies = UpdateItemTypeDependencies;
        })(Items = Purchasing.Items || (Purchasing.Items = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
