///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_P2P_CompanyCodesValue",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "P2P library",
  "require": [
    "Lib_P2P_V12.0.425.0",
    "[Sys/Sys_GenericAPI_Client]",
    "[Sys/Sys_GenericAPI_Server]",
    "Sys/Sys_Decimal",
    "Sys/Sys_Helpers"
  ]
}*/
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        var CompanyCodesValue;
        (function (CompanyCodesValue) {
            var cachedValues = {};
            var ccvaluesPromiseCache = {};
            var Currencies = /** @class */ (function () {
                function Currencies(companyCode) {
                    this.companyCode = companyCode;
                    this.conversion = {};
                    this.currenciesPromiseCache = {};
                }
                Currencies.prototype.GetRate = function (currency) {
                    if (this.conversion[currency]) {
                        return this.conversion[currency];
                    }
                    return null;
                };
                Currencies.prototype.QueryRate = function (currency) {
                    var _this = this;
                    return this.currenciesPromiseCache[currency] || (this.currenciesPromiseCache[currency] = Sys.Helpers.Promise.Create(function (resolve) {
                        if (Object.keys(_this.conversion).length > 0) {
                            resolve(_this.GetRate(currency));
                        }
                        else {
                            Sys.GenericAPI.Query("P2P - Exchange rate__", "CompanyCode__=" + _this.companyCode, ["CompanyCode__", "CurrencyFrom__", "Rate__", "RatioFrom__", "RatioTo__"], function (result, error) {
                                if (!error) {
                                    var j = void 0, r = void 0;
                                    for (j = 0; j < result.length; j++) {
                                        r = result[j];
                                        _this.conversion[r.CurrencyFrom__] = new Sys.Decimal(r.RatioTo__).div(r.RatioFrom__).mul(r.Rate__).toNumber();
                                    }
                                }
                                resolve(_this.GetRate(currency));
                            }, "", 100);
                        }
                    }));
                };
                return Currencies;
            }());
            CompanyCodesValue.Currencies = Currencies;
            function QueryValues(companyCode, queryCurrencies) {
                /** return cached promise :
                * if the promise for the company code specified exit and :
                *	if the promise cached has queryCurrencies as true
                *   OR
                *	if queryCurrencies as parameter is false (and of course queryCurrencies from the promise cached is false also)
                */
                if (!ccvaluesPromiseCache[companyCode] || (queryCurrencies && !ccvaluesPromiseCache[companyCode].queryCurrencies)) {
                    if (!ccvaluesPromiseCache[companyCode]) {
                        ccvaluesPromiseCache[companyCode] = {};
                    }
                    ccvaluesPromiseCache[companyCode].queryCurrencies = queryCurrencies;
                    ccvaluesPromiseCache[companyCode].promise = Sys.Helpers.Promise.Create(function (resolve) {
                        // when we want the currencies, return cache value only if the currencies have been loaded
                        if (cachedValues[companyCode] && (!queryCurrencies || (cachedValues[companyCode] && cachedValues[companyCode].currencies && Object.keys(cachedValues[companyCode].currencies.conversion).length > 0))) {
                            resolve(cachedValues[companyCode]);
                        }
                        else if (companyCode) {
                            Sys.GenericAPI.Query("PurchasingCompanycodes__", "CompanyCode__=" + companyCode, ["CompanyCode__", "CompanyName__", "PurchasingOrganization__", "PurchasingGroup__", "DeliveryAddressID__", "Currency__", "ERP__", "Sub__", "Street__", "PostOfficeBox__", "City__", "PostalCode__", "Region__", "Country__", "PhoneNumber__", "FaxNumber__", "VATNumber__", "ContactEmail__", "SIRET__", "DefaultConfiguration__", "DefaultExpenseReportTypeID__", "DefaultExpenseReportType__"], function (result, error) {
                                if (!error && result[0]) {
                                    var r = result[0];
                                    cachedValues[companyCode] = {
                                        CompanyCode__: r.CompanyCode__,
                                        CompanyName__: r.CompanyName__,
                                        PurchasingOrganization__: r.PurchasingOrganization__,
                                        PurchasingGroup__: r.PurchasingGroup__,
                                        DeliveryAddressID__: r.DeliveryAddressID__,
                                        Currency__: r.Currency__,
                                        ERP__: r.ERP__,
                                        Sub__: r.Sub__,
                                        Street__: r.Street__,
                                        PostOfficeBox__: r.PostOfficeBox__,
                                        City__: r.City__,
                                        PostalCode__: r.PostalCode__,
                                        Region__: r.Region__,
                                        Country__: r.Country__,
                                        PhoneNumber__: r.PhoneNumber__,
                                        FaxNumber__: r.FaxNumber__,
                                        VATNumber__: r.VATNumber__,
                                        ContactEmail__: r.ContactEmail__,
                                        SIRET__: r.SIRET__,
                                        DefaultConfiguration__: r.DefaultConfiguration__ || "Default",
                                        DefaultExpenseReportTypeID__: r.DefaultExpenseReportTypeID__,
                                        DefaultExpenseReportType__: r.DefaultExpenseReportType__,
                                        currencies: new Currencies(companyCode)
                                    };
                                    if (!r.DefaultConfiguration__) {
                                        Log.Warn("The configuration is not defined in company code '" + companyCode + "' => default to configuration 'Default'.");
                                    }
                                    if (queryCurrencies) {
                                        cachedValues[companyCode].currencies.QueryRate().Then(function ( /*rate*/) {
                                            resolve(cachedValues[companyCode]);
                                        });
                                    }
                                    else {
                                        resolve(cachedValues[companyCode]);
                                    }
                                }
                                else {
                                    cachedValues[companyCode] = {};
                                    resolve(cachedValues[companyCode]);
                                }
                            }, "", 100);
                        }
                        else {
                            cachedValues[companyCode] = {};
                            resolve(cachedValues[companyCode]);
                        }
                    });
                }
                return ccvaluesPromiseCache[companyCode].promise;
            }
            CompanyCodesValue.QueryValues = QueryValues;
            function GetValues(companyCode) {
                if (cachedValues[companyCode]) {
                    return cachedValues[companyCode];
                }
                return {};
            }
            CompanyCodesValue.GetValues = GetValues;
        })(CompanyCodesValue = P2P.CompanyCodesValue || (P2P.CompanyCodesValue = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
