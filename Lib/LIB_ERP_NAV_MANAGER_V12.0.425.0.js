/* eslint-disable class-methods-use-this */
/* LIB_DEFINITION{
  "name": "LIB_ERP_NAV_Manager",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "ERP Manager for Navision - system library",
  "require": [
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Object",
    "Lib_ERP_Manager_V12.0.425.0",
    "Lib_ERP_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var ERP;
    (function (ERP) {
        var NAV;
        (function (NAV) {
            var Manager = /** @class */ (function (_super) {
                __extends(Manager, _super);
                function Manager() {
                    return _super.call(this, "NAV") || this;
                }
                return Manager;
            }(Lib.ERP.Manager.Instance));
            NAV.Manager = Manager;
            Lib.ERP.NAV.Manager.prototype.documentFactories = {};
        })(NAV = ERP.NAV || (ERP.NAV = {}));
    })(ERP = Lib.ERP || (Lib.ERP = {}));
})(Lib || (Lib = {}));
Lib.ERP.Manager.factories.NAV = function () {
    return new Lib.ERP.NAV.Manager();
};
