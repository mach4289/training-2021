///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "LIB_AP_SAP",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "AP Library",
  "require": [
    "Lib_AP_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var SAP;
        (function (SAP) {
            // #endregion
            // #region classes
            var SAPTaxFromNetBapi = /** @class */ (function () {
                function SAPTaxFromNetBapi() {
                    this.lastError = "";
                }
                return SAPTaxFromNetBapi;
            }());
            SAP.SAPTaxFromNetBapi = SAPTaxFromNetBapi;
            var SAPTaxAccount = /** @class */ (function () {
                function SAPTaxAccount(tax, taxCode) {
                    // Class providing members for Tax Account management.
                    if (tax) {
                        Lib.AP.SAP.AddGetValue(tax);
                        // Transaction key
                        this.Acct_Key = tax.GetValue("KTOSL");
                        // Condition type
                        this.Cond_Key = tax.GetValue("KSCHL");
                        // General ledger account
                        this.Gl_Account = tax.GetValue("HKONT");
                        // Tax amount in document currency
                        this.Tax_Amount = tax.GetValue("WMWST");
                        // Tax code
                        this.Tax_Code = taxCode;
                        // Tax rate
                        this.Tax_Rate = tax.GetValue("MSATZ");
                        // Jurisdiction for tax calculation - tax jurisdiction code
                        this.Taxjurcode = tax.GetValue("TXJCD");
                        // Jurisdiction for tax calculation - tax jurisdiction code
                        this.Taxjurcode_Deep = tax.GetValue("TXJCD_DEEP");
                        // Tax jurisdiction code level
                        this.Taxjurcode_Level = tax.GetValue("TXJLV");
                    }
                    else {
                        this.Acct_Key = null;
                        this.Cond_Key = null;
                        this.Gl_Account = null;
                        this.Tax_Amount = null;
                        this.Tax_Code = null;
                        this.Tax_Rate = null;
                        this.Taxjurcode = null;
                        this.Taxjurcode_Deep = null;
                        this.Taxjurcode_Level = null;
                    }
                }
                return SAPTaxAccount;
            }());
            SAP.SAPTaxAccount = SAPTaxAccount;
            // #endregion
            function buildOrFilter(queryResult, fields) {
                if (typeof fields === "string") {
                    fields = [fields];
                }
                var filter = [];
                for (var i in queryResult) {
                    if (Object.prototype.hasOwnProperty.call(queryResult, i)) {
                        var element = queryResult[i];
                        var subfilter = [];
                        for (var f in fields) {
                            if (Object.prototype.hasOwnProperty.call(fields, f)) {
                                var field = fields[f];
                                subfilter.push(field + " = '" + Sys.Helpers.String.SAP.Trim(element[field]) + "'");
                            }
                        }
                        var subresult = subfilter.join(" AND ");
                        if (subfilter.length > 1) {
                            subresult = "( " + subresult + " )";
                        }
                        filter.push(subresult);
                    }
                }
                var result = filter.join("\nOR ");
                if (filter.length > 1) {
                    result = "( " + result + " )";
                }
                return result;
            }
            function IsMultiAccountAssignment(acctAssCat) {
                return acctAssCat === "K1" || acctAssCat === "K2" || acctAssCat === "U";
            }
            SAP.IsMultiAccountAssignment = IsMultiAccountAssignment;
            function AddAccountAssignmentLine(invoiceDocItem, accountAssignmentFromPO, poItemData, line, callback) {
                var isMultipleAccountAssignment = poItemData.AcctAssCat === "K" && (poItemData.Distribution === "1" || poItemData.Distribution === "2");
                var isUnknownAccountAssignment = poItemData.AcctAssCat === "U";
                if (isMultipleAccountAssignment || isUnknownAccountAssignment) {
                    var accountingLine = {
                        // Update data with form values
                        INVOICE_DOC_ITEM: invoiceDocItem,
                        ITEM_AMOUNT: Math.abs(line.GetValue("Amount__")),
                        GL_ACCOUNT: Sys.Helpers.String.SAP.NormalizeID(line.GetValue("GLAccount__"), 10),
                        COSTCENTER: Sys.Helpers.String.SAP.NormalizeID(line.GetValue("CostCenter__"), 10),
                        TAX_CODE: line.GetValue("TaxCode__"),
                        QUANTITY: line.GetValue("Quantity__"),
                        TAXJURCODE: line.GetValue("TaxJurisdiction__"),
                        ORDERID: Sys.Helpers.String.SAP.NormalizeID(line.GetValue("InternalOrder__"), 12),
                        WBS_ELEM: line.GetValue("WBSElementID__") || line.GetValue("WBSElement__")
                    };
                    if (accountAssignmentFromPO) {
                        accountingLine.SERIAL_NO = accountAssignmentFromPO.SERIAL_NO;
                        accountingLine.NETWORK = Sys.Helpers.String.SAP.NormalizeID(accountAssignmentFromPO.NETWORK, 12);
                        accountingLine.PROFIT_CTR = Sys.Helpers.String.SAP.NormalizeID(accountAssignmentFromPO.PROFIT_CTR, 10);
                        accountingLine.FUNC_AREA = accountAssignmentFromPO.FUNC_AREA;
                        accountingLine.CO_AREA = accountAssignmentFromPO.CO_AREA;
                    }
                    else {
                        accountingLine.XUNPL = "X";
                    }
                    if (poItemData.IsServiceItem() || poItemData.IsLimitItem()) {
                        delete accountingLine.QUANTITY;
                        delete accountingLine.PO_PR_QNT;
                    }
                    else {
                        accountingLine.PO_UNIT = poItemData.Unit;
                        accountingLine.PO_PR_UOM = poItemData.Orderpr_Un;
                    }
                    callback(accountingLine);
                }
            }
            SAP.AddAccountAssignmentLine = AddAccountAssignmentLine;
            function AddGetValue(obj) {
                if (obj && typeof obj.GetValue === "undefined") {
                    // Client side script - redefine GetValue
                    obj.GetValue = function (attr) {
                        return obj[attr];
                    };
                }
            }
            SAP.AddGetValue = AddGetValue;
            function GetNewSAPTaxAccount(tax, taxcode) {
                return new SAPTaxAccount(tax, taxcode);
            }
            SAP.GetNewSAPTaxAccount = GetNewSAPTaxAccount;
            function GetDocumentType() {
                var documentType;
                if (Lib.AP.InvoiceType.isConsignmentInvoice()) {
                    documentType = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeFIConsignmentStock");
                }
                else if (Lib.AP.InvoiceType.isPOInvoice()) {
                    documentType = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeMMInvoice");
                    if (parseFloat(Data.GetValue("InvoiceAmount__")) < 0) {
                        documentType = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeMMCreditNote");
                    }
                }
                else {
                    documentType = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeFIInvoice");
                    if (parseFloat(Data.GetValue("InvoiceAmount__")) < 0) {
                        documentType = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeFICreditNote");
                    }
                }
                return documentType;
            }
            SAP.GetDocumentType = GetDocumentType;
            function GetMMInvoiceDocumentTypeFilter() {
                var documentType1 = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeMMInvoice");
                var documentType2 = Sys.Parameters.GetInstance("AP").GetParameter("SAPDocumentTypeMMCreditNote");
                if (documentType1 === documentType2) {
                    documentType2 = null;
                }
                var filter = "";
                if (documentType1 && documentType2) {
                    filter = "(BLART = '" + documentType1 + "' OR BLART = '" + documentType2 + "')";
                }
                else if (documentType1) {
                    filter = "BLART = '" + documentType1 + "'";
                }
                else if (documentType2) {
                    filter = "BLART = '" + documentType2 + "'";
                }
                return filter;
            }
            SAP.GetMMInvoiceDocumentTypeFilter = GetMMInvoiceDocumentTypeFilter;
            function GetSelectedBankDetailsType() {
                var bankDetails = Data.GetTable("BankDetails__");
                if (bankDetails) {
                    for (var i = 0; i < bankDetails.GetItemCount(); i++) {
                        if (bankDetails.GetItem(i).GetValue("BankDetails_Select__")) {
                            return bankDetails.GetItem(i).GetValue("BankDetails_Type__");
                        }
                    }
                }
                return null;
            }
            SAP.GetSelectedBankDetailsType = GetSelectedBankDetailsType;
            function escapeToSQL(filter) {
                if (filter && filter.length > 0) {
                    filter = filter.replace(/\+/g, "_");
                    filter = filter.replace(/\*/g, "%");
                    filter = filter.replace(/'/g, "''");
                }
                return filter;
            }
            SAP.escapeToSQL = escapeToSQL;
            function FilterResultsOnAWKEY(bkpfResult, rbkpResults) {
                for (var _i = 0, rbkpResults_1 = rbkpResults; _i < rbkpResults_1.length; _i++) {
                    var r = rbkpResults_1[_i];
                    var key = r.BELNR + r.GJAHR;
                    for (var i = 0; i < bkpfResult.length; i++) {
                        if (bkpfResult[i].AWKEY === key) {
                            bkpfResult.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            SAP.FilterResultsOnAWKEY = FilterResultsOnAWKEY;
            function AddDuplicateCandidates(result, duplicateCandidates) {
                if (!result) {
                    return duplicateCandidates;
                }
                var duplicateNumber, i, refDocNum, refDocCompCode, refDocFiscYear;
                for (i = 0; i < result.length; ++i) {
                    refDocNum = result[i].BELNR;
                    refDocCompCode = result[i].BUKRS;
                    refDocFiscYear = result[i].GJAHR;
                    duplicateNumber = refDocNum + "-" + refDocCompCode + "-" + refDocFiscYear;
                    if (duplicateCandidates.indexOf(duplicateNumber) < 0) {
                        duplicateCandidates.push(duplicateNumber);
                    }
                }
                return duplicateCandidates;
            }
            SAP.AddDuplicateCandidates = AddDuplicateCandidates;
            function GetDuplicateFIparkedInvoiceFilter(result, companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate) {
                // Compute filter for VBSEGK query
                var filter = "";
                if (result && result.length > 0) {
                    filter = "BUKRS = '" + companyCode + "'";
                    filter += "\nAND LIFNR = '" + normalizedVendorNumber + "'";
                    filter += "\nAND ZFBDT = '" + normalizedInvoiceDate + "'";
                    filter += "\nAND " + buildOrFilter(result, ["BELNR", "GJAHR"]);
                }
                return filter;
            }
            SAP.GetDuplicateFIparkedInvoiceFilter = GetDuplicateFIparkedInvoiceFilter;
            function AddDuplicateAmountFilter(filter, normalizedInvoiceAmount, bForMM) {
                // Complete filter for BSIP or RBKP query
                var bCreditMemo = false;
                var invAmount = normalizedInvoiceAmount;
                if (normalizedInvoiceAmount.indexOf("-") === 0) {
                    invAmount = normalizedInvoiceAmount.substr(1);
                    bCreditMemo = true;
                }
                if (filter) {
                    filter += "\nAND ";
                }
                if (bForMM) {
                    filter += "RMWWR = '" + invAmount + "'";
                    if (bCreditMemo) {
                        // Credit note or Subsequent credit
                        filter += "\nAND XRECH <> 'X'";
                    }
                }
                else {
                    filter += "WRBTR = '" + invAmount + "'";
                    filter += "\nAND SHKZG = '" + (bCreditMemo ? "S" : "H") + "'";
                }
                return filter;
            }
            SAP.AddDuplicateAmountFilter = AddDuplicateAmountFilter;
            function GetDuplicateMMInvoiceFilter(companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount) {
                // Compute filter for RBKP query
                var filter = "BUKRS = '" + companyCode + "'";
                filter += "\nAND RBSTAT <> '2'";
                filter += "\nAND LIFNR = '" + normalizedVendorNumber + "'";
                filter += "\nAND BLDAT = '" + normalizedInvoiceDate + "'";
                filter += "\nAND WAERS = '" + invoiceCurrency + "'";
                // Invoice not reversed
                filter += "\nAND STBLG = ''";
                if (!Lib.AP.InvoiceType.isPOInvoice()) {
                    // use inv # if it exists, otherwise use inv amount
                    if (normalizedInvoiceNumber) {
                        filter += "\nAND XBLNR = '" + normalizedInvoiceNumber + "'";
                    }
                    else {
                        filter = Lib.AP.SAP.AddDuplicateAmountFilter(filter, normalizedInvoiceAmount, true);
                    }
                }
                // built RBKP po filter
                // use inv # if it exists, otherwise use inv amount
                else if (normalizedInvoiceNumber && normalizedInvoiceAmount) {
                    filter += "\nAND XBLNR = '" + normalizedInvoiceNumber + "'";
                    filter = Lib.AP.SAP.AddDuplicateAmountFilter(filter, normalizedInvoiceAmount, true);
                }
                // When InvoiceReferenceNumber__ we want to remove from duplicate check referenced field
                var invoiceRef = Lib.AP.ParseInvoiceDocumentNumber(Data.GetValue("InvoiceReferenceNumber__"), true);
                if (invoiceRef && invoiceRef.documentNumber) {
                    filter += "\nAND REBZG <> '" + invoiceRef.documentNumber + "'";
                }
                return filter;
            }
            SAP.GetDuplicateMMInvoiceFilter = GetDuplicateMMInvoiceFilter;
            function GetDuplicateReversedMMInvoicesFilter(result, companyCode) {
                var fiFromMM = [];
                for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                    var r = result_1[_i];
                    if (r.AWKEY && r.AWKEY.length === 14) {
                        fiFromMM.push({
                            documentNumber: r.AWKEY.substr(0, 10),
                            fiscalYear: r.AWKEY.substr(10)
                        });
                    }
                }
                var filter = "";
                if (fiFromMM.length > 0) {
                    filter = "BUKRS = '" + companyCode + "' AND (\n";
                    var invFilters = [];
                    for (var _a = 0, fiFromMM_1 = fiFromMM; _a < fiFromMM_1.length; _a++) {
                        var inv = fiFromMM_1[_a];
                        invFilters.push(" ( BELNR = '" + inv.documentNumber + "' AND GJAHR = '" + inv.fiscalYear + "'\n AND STBLG <> '' ) ");
                    }
                    filter += invFilters.join("\nOR ");
                    filter += " ) ";
                }
                return filter;
            }
            SAP.GetDuplicateReversedMMInvoicesFilter = GetDuplicateReversedMMInvoicesFilter;
            function GetDuplicateFIInvoiceNotReversedFilter(result, companyCode, normalizedInvoiceNumber, normalizedInvoiceDate, invoiceCurrency) {
                // Compute filter for BKPF query
                var filter = "";
                if (result && result.length > 0) {
                    filter = "BUKRS = '" + companyCode + "'";
                    filter += "\nAND STBLG = ''";
                    if (normalizedInvoiceNumber) {
                        filter += "\nAND XBLNR = '" + normalizedInvoiceNumber + "'";
                    }
                    filter += "\nAND WAERS = '" + invoiceCurrency + "'";
                    filter += "\nAND BLDAT = '" + normalizedInvoiceDate + "'";
                    filter += "\nAND " + buildOrFilter(result, ["BELNR", "GJAHR"]);
                }
                return filter;
            }
            SAP.GetDuplicateFIInvoiceNotReversedFilter = GetDuplicateFIInvoiceNotReversedFilter;
            function GetDuplicateFIInvoiceFilter(companyCode, normalizedInvoiceNumber, normalizedVendorNumber, normalizedInvoiceDate, invoiceCurrency, normalizedInvoiceAmount) {
                // Compute filter for BSIP query
                var filter = "BUKRS = '" + companyCode + "'";
                if (!Lib.AP.InvoiceType.isPOInvoice()) {
                    // use inv # if it exists, otherwise use inv amount
                    if (normalizedInvoiceNumber) {
                        filter += "\nAND XBLNR = '" + normalizedInvoiceNumber + "'";
                    }
                    else {
                        filter = Lib.AP.SAP.AddDuplicateAmountFilter(filter, normalizedInvoiceAmount);
                    }
                }
                // built BSIP po filter
                // use inv # if it exists, otherwise use inv amount
                else if (normalizedInvoiceNumber && normalizedInvoiceAmount) {
                    filter += "\nAND XBLNR = '" + normalizedInvoiceNumber + "'";
                    filter = Lib.AP.SAP.AddDuplicateAmountFilter(filter, normalizedInvoiceAmount);
                }
                filter += "\nAND LIFNR = '" + normalizedVendorNumber + "'";
                filter += "\nAND BLDAT = '" + normalizedInvoiceDate + "'";
                filter += "\nAND WAERS = '" + invoiceCurrency + "'";
                return filter;
            }
            SAP.GetDuplicateFIInvoiceFilter = GetDuplicateFIInvoiceFilter;
            // This cache is used when generating the FI or MM simulation report
            // There is no need for the language to be a key for this cache since the simulation report is only visible by the AP user.
            SAP.glDescriptionsCache = {};
            function InitGLDescriptionCacheFromLines(companyCode) {
                Lib.AP.SAP.glDescriptionsCache[companyCode] = {};
                // For each line on form with a GLAccount__, store the GLDescription__ in cache
                var lineItems = Data.GetTable("LineItems__");
                for (var idx = 0; idx < lineItems.GetItemCount(); idx++) {
                    var lineItem = lineItems.GetItem(idx);
                    var glAcc = lineItem.GetValue("GLAccount__");
                    var glAccDesc = lineItem.GetValue("GLDescription__");
                    if (glAcc && glAccDesc) {
                        Lib.AP.SAP.glDescriptionsCache[companyCode][glAcc] = glAccDesc;
                    }
                }
            }
            SAP.InitGLDescriptionCacheFromLines = InitGLDescriptionCacheFromLines;
            function GetNewSAPTaxFromNetBapi() {
                return null;
            }
            SAP.GetNewSAPTaxFromNetBapi = GetNewSAPTaxFromNetBapi;
            function CheckVendorNumber(vendorNumber) {
                var res = {
                    isValid: true,
                    vendorNumber: vendorNumber
                };
                var headerVendorNumber = Data.GetValue("VendorNumber__");
                if (Lib.AP.InvoiceType.isConsignmentInvoice() && headerVendorNumber) {
                    if (typeof vendorNumber === "string" && vendorNumber) {
                        res.isValid = headerVendorNumber === vendorNumber;
                    }
                    else {
                        var lineItemsTable = Data.GetTable("LineItems__");
                        var nbItems = lineItemsTable.GetItemCount();
                        for (var i = 0; i < nbItems && res.isValid; i++) {
                            var item = lineItemsTable.GetItem(i);
                            var lineVendor = item.GetValue("VendorNumber__");
                            if (lineVendor) {
                                res.isValid = headerVendorNumber === lineVendor;
                            }
                        }
                    }
                }
                var customBehavior = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAPCheckVendorNumber", res);
                if (!customBehavior) {
                    if (!res.isValid) {
                        Data.SetWarning("VendorNumber__", "_Vendor don't match lines");
                    }
                    else {
                        Data.SetWarning("VendorNumber__", null);
                    }
                }
                return res.isValid;
            }
            SAP.CheckVendorNumber = CheckVendorNumber;
            function GetDefaultTaxAccDataFromItem(item) {
                return {
                    "AUFNR": Sys.Helpers.String.SAP.NormalizeID(item.GetValue("InternalOrder__"), 12),
                    "EBELP": Sys.Helpers.String.SAP.NormalizeID(item.GetValue("ItemNumber__"), 5),
                    "EBELN": Sys.Helpers.String.SAP.NormalizeID(item.GetValue("OrderNumber__"), 10),
                    "LIFNR": Sys.Helpers.String.SAP.NormalizeID(Data.GetValue("VendorNumber__"), 10),
                    "SAKNR": Sys.Helpers.String.SAP.NormalizeID(item.GetValue("GLAccount__"), 10),
                    "KOSTL": Sys.Helpers.String.SAP.NormalizeID(item.GetValue("CostCenter__"), 10)
                };
            }
            SAP.GetDefaultTaxAccDataFromItem = GetDefaultTaxAccDataFromItem;
        })(SAP = AP.SAP || (AP.SAP = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
