///#GLOBALS Lib Sys
/* LIB_DEFINITION
{
    "name": "Lib_Contract",
    "libraryType": "LIB",
    "scriptType": "COMMON",
    "comment": "P2P library",
    "require": [
        "Sys/Sys_Helpers",
        "Sys/Sys_Helpers_Date"
    ]
}
*/
var Lib;
(function (Lib) {
    var Contract;
    (function (Contract) {
        var g = Sys.Helpers.Globals;
        function ComputeEndDate(duration) {
            var startDate = Data.GetValue("StartDate__");
            var newEndDate = new Date(startDate.getTime());
            newEndDate.setMonth(startDate.getMonth() + duration);
            var diff = Sys.Helpers.Date.ComputeDeltaMonth(startDate, newEndDate);
            // If don't get the right number, set date to last day of previous month
            if (diff != duration) {
                newEndDate.setDate(0);
            }
            else {
                newEndDate.setDate(startDate.getDate() - 1);
            }
            Data.SetValue("EndDate__", newEndDate);
        }
        Contract.ComputeEndDate = ComputeEndDate;
        function IsInitialOwner() {
            if (Sys.ScriptInfo.IsClient()) {
                var contractOwnerLogin = Data.GetValue("OwnerLogin__");
                return !contractOwnerLogin
                    || g.User.loginId === contractOwnerLogin
                    || g.User.IsMemberOf(contractOwnerLogin)
                    || g.User.IsBackupUserOf(contractOwnerLogin);
            }
            return false;
        }
        Contract.IsInitialOwner = IsInitialOwner;
    })(Contract = Lib.Contract || (Lib.Contract = {}));
})(Lib || (Lib = {}));
