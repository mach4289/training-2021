/* LIB_DEFINITION{
  "name": "Lib_VendorRegistration_WorkflowDefinition",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "VendorRegistration library",
  "versionable": false,
  "require": [
    "Sys/Sys_VendorRegistration_WorkflowDefinition",
    "Lib_V12.0.425.0"
  ]
}*/
///#GLOBALS Lib Sys
// Common part
var Lib;
(function (Lib) {
    var VendorRegistration;
    (function (VendorRegistration) {
        var WorkflowDefinition;
        (function (WorkflowDefinition) {
            function AddJSONTo(definitions) {
                definitions.push(Sys.VendorRegistration.WorkflowDefinition.GetJSON(0));
            }
            WorkflowDefinition.AddJSONTo = AddJSONTo;
        })(WorkflowDefinition = VendorRegistration.WorkflowDefinition || (VendorRegistration.WorkflowDefinition = {}));
    })(VendorRegistration = Lib.VendorRegistration || (Lib.VendorRegistration = {}));
})(Lib || (Lib = {}));
