///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_P2P_UserProperties",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "P2P library",
  "require": [
    "Lib_P2P_V12.0.425.0",
    "[Sys/Sys_GenericAPI_Server]",
    "[Sys/Sys_GenericAPI_Client]"
  ]
}*/
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        var UserProperties;
        (function (UserProperties) {
            var cachedValues = {};
            var CUserProperties = /** @class */ (function () {
                function CUserProperties(dbRecord) {
                    this.CompanyCode__ = "";
                    this.UserNumber__ = "";
                    this.CostCenter__ = "";
                    this.CostCenter__Description__ = "";
                    this.DefaultWarehouse__ = "";
                    this.DefaultWarehouseName__ = "";
                    this.AllowedCompanyCodes__ = "";
                    this.allowedCompanyCodesString = null;
                    this.HasCompanyCard__ = false;
                    if (dbRecord) {
                        this.CompanyCode__ = dbRecord.CompanyCode__;
                        this.UserNumber__ = dbRecord.UserNumber__;
                        this.CostCenter__ = dbRecord.CostCenter__;
                        this.CostCenter__Description__ = dbRecord["CostCenter__.Description__"];
                        this.DefaultWarehouse__ = dbRecord.DefaultWarehouse__;
                        this.DefaultWarehouseName__ = dbRecord["DefaultWarehouse__.WarehouseName___"];
                        this.AllowedCompanyCodes__ = dbRecord.AllowedCompanyCodes__;
                        this.HasCompanyCard__ = dbRecord.HasCompanyCard__ == "1";
                    }
                }
                CUserProperties.prototype.GetAllowedCompanyCodes = function () {
                    if (!this.allowedCompanyCodesString) {
                        var list = this.AllowedCompanyCodes__.split(";");
                        list.push(this.CompanyCode__);
                        var uniqueCCList_1 = [];
                        Sys.Helpers.Array.ForEach(list, function (cc) {
                            if (!Sys.Helpers.IsEmpty(cc) && uniqueCCList_1.indexOf(cc) < 0) {
                                uniqueCCList_1.push(cc);
                            }
                        });
                        this.allowedCompanyCodesString = uniqueCCList_1.join("\n");
                    }
                    return this.allowedCompanyCodesString;
                };
                return CUserProperties;
            }());
            UserProperties.CUserProperties = CUserProperties;
            function QueryValues(userLogin) {
                return Sys.Helpers.Promise.Create(function (resolve) {
                    if (cachedValues[userLogin]) {
                        resolve(cachedValues[userLogin]);
                    }
                    else if (userLogin) {
                        var attributes = ["CompanyCode__",
                            "UserNumber__",
                            "CostCenter__",
                            "CostCenter__.Description__",
                            "DefaultWarehouse__",
                            "DefaultWarehouse__.WarehouseName__",
                            "AllowedCompanyCodes__",
                            "HasCompanyCard__"];
                        Sys.GenericAPI.Query("P2P - User properties__", "(" + "UserLogin__=" + userLogin + ")", attributes, function (result, error) {
                            if (!error) {
                                cachedValues[userLogin] = new CUserProperties(result[0]);
                                resolve(cachedValues[userLogin]);
                            }
                            else {
                                cachedValues[userLogin] = new CUserProperties();
                                resolve(cachedValues[userLogin]);
                            }
                        }, "", 100, "EnableJoin=1");
                    }
                    else {
                        cachedValues[userLogin] = new CUserProperties();
                        resolve(cachedValues[userLogin]);
                    }
                });
            }
            UserProperties.QueryValues = QueryValues;
            function GetValues(userLogin) {
                if (cachedValues[userLogin]) {
                    return cachedValues[userLogin];
                }
                return new CUserProperties();
            }
            UserProperties.GetValues = GetValues;
        })(UserProperties = P2P.UserProperties || (P2P.UserProperties = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
