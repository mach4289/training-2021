///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Contract_Workflow",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "P2P library",
  "require": [
    "Lib_Contract_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var Contract;
    (function (Contract) {
        var Workflow;
        (function (Workflow) {
            Workflow.Parameters = {
                actions: {
                    submission: {
                        image: "Contract_submit_grey.png"
                    },
                    submitted: {
                        image: "Contract_submit.png"
                    },
                    approval: {
                        image: "Contract_approve_or_reject_grey.png"
                    },
                    approved: {
                        image: "Contract_approval.png"
                    },
                    modifyContract: {
                        image: "Contract_back.png"
                    },
                    rejected: {
                        image: "Contract_reject.png"
                    },
                    sentBack: {
                        image: "Contract_back.png"
                    },
                    deleted: {
                        image: "Contract_cancel.png"
                    },
                    share: {
                        image: "Contract_share.png"
                    },
                    comment: {
                        image: "Contract_comments_grey.png"
                    },
                    commentDone: {
                        image: "Contract_comments.png"
                    }
                },
                roles: {
                    requester: {},
                    approver: {}
                },
                mappingTable: {
                    tableName: "ApproversList__",
                    columns: {
                        WRKFUserName__: {
                            data: "name"
                        },
                        WRKFRole__: {
                            data: "role",
                            translate: true
                        },
                        WRKFDate__: {
                            data: "date"
                        },
                        WRKFComment__: {
                            data: "comment"
                        },
                        WRKFAction__: {
                            data: "action"
                        },
                        WRKFIsGroup__: {
                            data: "isGroup"
                        },
                        WRKFRequestDateTime__: {
                            data: "startingDate"
                        },
                        WRKFActualApprover__: {
                            data: "actualApprover"
                        }
                    }
                },
                delayedData: {
                    isGroup: {
                        type: "isGroupInfo",
                        key: "login"
                    }
                },
                callbacks: {
                    OnError: function (msg) {
                        Log.Error(msg);
                    },
                    OnBuilding: function () {
                    },
                    OnBuilt: function () {
                    }
                }
            };
        })(Workflow = Contract.Workflow || (Contract.Workflow = {}));
    })(Contract = Lib.Contract || (Lib.Contract = {}));
})(Lib || (Lib = {}));
