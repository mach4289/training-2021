///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_P2P_Inventory_Server",
  "libraryType": "LIB",
  "scriptType": "Server",
  "comment": "P2P library",
  "require": [
    "Lib_P2P_Inventory_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        var Inventory;
        (function (Inventory) {
            var ldaputil = Sys.Helpers.LdapUtil;
            function CreateOrUpdateInventoryMovement(inventoryMovement) {
                var record = GetInventoryMovementRecord(inventoryMovement);
                var newRecord = record || Process.CreateTableRecord("P2P - Inventory Movements__");
                if (newRecord) {
                    var newVars = newRecord.GetVars();
                    newVars.AddValue_String("CompanyCode__", inventoryMovement.companyCode, true);
                    newVars.AddValue_String("LineNumber__", inventoryMovement.lineNumber, true);
                    newVars.AddValue_String("MovementOrigin__", inventoryMovement.movementOrigin, true);
                    newVars.AddValue_String("ItemNumber__", inventoryMovement.itemNumber, true);
                    newVars.AddValue_String("warehouseID__", inventoryMovement.warehouseID, true);
                    newVars.AddValue_String("MovementValue__", inventoryMovement.movementValue, true);
                    newVars.AddValue_Date("MovementDateTime__", new Date(newVars.GetValue_String("MovementDateTime__", 0) || Date.now()), true);
                    newVars.AddValue_String("MovementType__", inventoryMovement.movementType, true);
                    newVars.AddValue_String("UnitOfMeasure__", inventoryMovement.unitOfMeasure, true);
                    Log.Info("Creating stock movement of " + inventoryMovement.movementValue + " for item number: " + inventoryMovement.itemNumber + " for warehouse : " + inventoryMovement.warehouseID);
                    newRecord.Commit();
                    if (newRecord.GetLastError()) {
                        Log.Error("Failed to create Stock movement. Details: " + newRecord.GetLastErrorMessage());
                        return false;
                    }
                }
                else {
                    Log.Error("Failed to create Stock movement. Details: CreateTableRecord returns null.");
                    return false;
                }
                return true;
            }
            Inventory.CreateOrUpdateInventoryMovement = CreateOrUpdateInventoryMovement;
            function UpdateInventoryStock(inventories) {
                var bok = true;
                inventories.forEach(function (inventory) {
                    var record = Process.GetUpdatableTableRecord(inventory.ruidex);
                    var vars = record.GetVars();
                    vars.AddValue_String("CurrentStock__", inventory.stock, true);
                    record.Commit();
                    if (record.GetLastError()) {
                        Log.Error("Failed to update current stock for inventory: CompanyCode '" + inventory.companyCode + "', WarehouseID '" + inventory.warehouseID + "', ItemNumber '" + inventory.itemNumber + "'. Details: " + record.GetLastErrorMessage());
                        bok = false;
                    }
                });
                return bok;
            }
            Inventory.UpdateInventoryStock = UpdateInventoryStock;
            function DeleteInventoryMovements(grRuidEx) {
                return Lib.P2P.Inventory.InventoryMovement.QueryMovementsForRuidex(grRuidEx)
                    .Then(function (inventoryMovements) {
                    inventoryMovements.forEach(function (inventoryMovement) {
                        inventoryMovement.record.Delete();
                        if (inventoryMovement.record.GetLastError()) {
                            Log.Error("Cannot delete item from inventory movement table. Details: " + inventoryMovement.record.GetLastErrorMessage());
                        }
                    });
                    return Sys.Helpers.Promise.Resolve(inventoryMovements);
                })
                    .Catch(function (err) {
                    Log.Error("Error while deleting Inventory Movements table: " + JSON.stringify(err));
                    return Sys.Helpers.Promise.Reject();
                });
            }
            Inventory.DeleteInventoryMovements = DeleteInventoryMovements;
            function GetInventoryMovementRecord(inventoryMovement) {
                var filter = [];
                filter.push(ldaputil.FilterEqual("MovementOrigin__", inventoryMovement.movementOrigin));
                filter.push(ldaputil.FilterEqual("LineNumber__", inventoryMovement.lineNumber));
                var queryOptions = {
                    table: "P2P - Inventory movements__",
                    filter: ldaputil.FilterAnd.apply(ldaputil, filter).toString(),
                    attributes: ["*"],
                    additionalOptions: {
                        recordBuilder: Sys.GenericAPI.BuildQueryResult
                    }
                };
                var res = null;
                Sys.GenericAPI.PromisedQuery(queryOptions)
                    .Then(function (queryResult) {
                    res = queryResult[0].record;
                })
                    .Catch(function () {
                    res = null;
                });
                return res;
            }
        })(Inventory = P2P.Inventory || (P2P.Inventory = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
