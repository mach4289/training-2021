///#GLOBALS Lib Sys
/*global setTimeout */
/* LIB_DEFINITION{
  "name": "LIB_P2P",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "P2P library",
  "require": [
    "Lib_V12.0.425.0",
    "Lib_ERP_V12.0.425.0",
    "Lib_CommonDialog_V12.0.425.0",
    "Sys/Sys_Decimal",
    "Sys/Sys_WorkflowEngine",
    "Sys/Sys_WorkflowController",
    "Sys/Sys_OnDemand_Users",
    "Sys/Sys_Parameters",
    "[Sys/Sys_GenericAPI_Server]",
    "[Sys/Sys_GenericAPI_Client]",
    "[Lib_Version_V12.0.425.0]"
  ]
}*/
/**
 * Common P2P functions
 * @namespace Lib.P2P
 */
var Lib;
(function (Lib) {
    var P2P;
    (function (P2P) {
        P2P.AddLib = Lib.AddLib;
        P2P.ExtendLib = Lib.ExtendLib;
        // Workflow definition cache
        var wfDefinition, g = Sys.Helpers.Globals;
        /**
         * @exports P2P
         * @memberof Lib.P2P
         */
        /**
         * Line Types
         */
        P2P.LineType = {
            PO: "PO",
            GL: "GL",
            POGL: "POGL",
            CONSIGNMENT: "Consignment"
        };
        /**
         * Item types
         */
        P2P.ItemType = {
            AMOUNT_BASED: "AmountBased",
            QUANTITY_BASED: "QuantityBased",
            SERVICE_BASED: "ServiceBased"
        };
        function InitItemTypeControl(ctrl) {
            if (Sys.ScriptInfo.IsClient() && Sys.Parameters.GetInstance("P2P").GetParameter("EnableServiceBasedItem") === "1") {
                if (!ctrl) {
                    ctrl = g.Controls.LineItems__.ItemType__;
                }
                var options = ctrl.GetAvailableValues();
                options.push(P2P.ItemType.SERVICE_BASED + "=_ServiceBased");
                ctrl.SetAvailableValues(options);
            }
        }
        P2P.InitItemTypeControl = InitItemTypeControl;
        /**
         * Invoice Line Item Helper
        */
        P2P.InvoiceLineItem = {
            /**
             * Check if the line is a PO line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to PO
             */
            IsPOLineItem: function (item) {
                return item && item.GetValue("LineType__") === P2P.LineType.PO;
            },
            /**
             * Check if the line is a GL line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to GL
             */
            IsGLLineItem: function (item) {
                return item && item.GetValue("LineType__") === P2P.LineType.GL;
            },
            /**
             * Check if the line is a POGL line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to POGL
             */
            IsPOGLLineItem: function (item) {
                return item && item.GetValue("LineType__") === P2P.LineType.POGL;
            },
            /**
             * Check if the line is a UDC line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to GL and associated keyword is defined
             */
            IsUDCLineItem: function (item) {
                return item && this.IsGLLineItem(item) && item.GetValue("Keyword__");
            },
            /**
             * Check if the line is a PO or POGL line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to Consignment
             */
            IsPOLikeLineItem: function (item) {
                return item && (item.GetValue("LineType__") === P2P.LineType.PO || item.GetValue("LineType__") === P2P.LineType.POGL);
            },
            /**
             * Check if the line is a Consignment line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to Consignment
             */
            IsConsignmentLineItem: function (item) {
                return item && item.GetValue("LineType__") === P2P.LineType.CONSIGNMENT;
            },
            /**
             * Check if the line is a GL, POGL or Consignment line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to Consignment
             */
            IsGLLikeLineItem: function (item) {
                return item && (item.GetValue("LineType__") === P2P.LineType.GL || item.GetValue("LineType__") === P2P.LineType.POGL || item.GetValue("LineType__") === P2P.LineType.CONSIGNMENT);
            },
            /**
             * Checks if the item is amount based
             * @param {Data} item The line to test
             * @return boolean True if the ItemType of the line is equal to AmountBased
             */
            IsAmountBasedRow: function (row) {
                return row && row.ItemType__.GetValue() === P2P.ItemType.AMOUNT_BASED;
            },
            IsAmountBasedItem: function (item) {
                return item && item.GetValue("ItemType__") === P2P.ItemType.AMOUNT_BASED;
            },
            /**
             * Checks if the item is quantity based
             * @param item The line to test
             * @return boolean True if the ItemType of the line is equal to QuantityBased
             */
            IsQuantityBasedItem: function (item) {
                return item && item.ItemType__.GetValue() !== P2P.ItemType.AMOUNT_BASED;
            },
            /**
             * Determine if a line item is ready to post or not
             * @param item The data of the line to test
             * @return boolean True if the line is not correctly filled
             */
            IsPostable: function (item) {
                if (!P2P.InvoiceLineItem.IsGLLineItem(item) && item.GetValue("OrderNumber__")) {
                    // postable only if both are set
                    return item.GetValue("Amount__") !== null && (item.GetValue("Quantity__") !== null || Data.GetValue("SubsequentDocument__"));
                }
                return true;
            },
            /**
             * Determine if a line item can be removed
             * @param {Data} item The data of the line to test
             * @return boolean True if the line can be safely removed
             */
            IsRemovable: function (item) {
                if (!P2P.InvoiceLineItem.IsGLLineItem(item) && item.GetValue("OrderNumber__")) {
                    // removable only if both are empty
                    return item.GetValue("Amount__") === null && item.GetValue("Quantity__") === null;
                }
                return false;
            },
            /**
             * Commons check for all kind of line type
             * @param {Data} item The data of the line to test
             * @return boolean True if the line is empty
             */
            IsGenericLineEmpty: function (item) {
                return !item.GetValue("Description__") && !item.GetValue("Amount__") && !item.GetValue("TaxCode__");
            },
            /**
             * Check if a GL line item is empty
             * @param {Data} item The data of the line to test
             * @return boolean True if the line is empty
             */
            IsGLLineItemEmpty: function (item) {
                return P2P.InvoiceLineItem.IsGenericLineEmpty(item) && !item.GetValue("GLAccount__") && !item.GetValue("CostCenter__");
            },
            /**
             * Check if a PO line item is empty
             * @param {Data} item The data of the line to test
             * @return boolean True if the line is empty
             */
            IsPOLineItemEmpty: function (item) {
                return P2P.InvoiceLineItem.IsGenericLineEmpty(item) && !item.GetValue("OrderNumber__") && !item.GetValue("ItemNumber__") && !item.GetValue("Quantity__");
            },
            /**
             * Check if a PO line item is empty
             * @param {Data} item The data of the line to test
             * @return boolean True if the line is empty
             */
            IsConsignmentLineItemEmpty: function (item) {
                return P2P.InvoiceLineItem.IsGenericLineEmpty(item) && !item.GetValue("Quantity__");
            },
            /**
             * Check if the line is a GL line item
             * @param {Data} item The data of the line to test
             * @return boolean True if the LineType of the line is equal to GL
             */
            IsEmpty: function (item) {
                if (P2P.InvoiceLineItem.IsGLLineItem(item)) {
                    return P2P.InvoiceLineItem.IsGLLineItemEmpty(item);
                }
                else if (P2P.InvoiceLineItem.IsPOLineItem(item) || P2P.InvoiceLineItem.IsPOGLLineItem(item)) {
                    return P2P.InvoiceLineItem.IsPOLineItemEmpty(item);
                }
                // Non-PO Line item are managed like GL line item
                return P2P.InvoiceLineItem.IsGLLineItemEmpty(item);
            }
        };
        /*
         * COME FROM EskFSDK_String
         * Function: removeAccents
         * Purpose:  Remove accents, to lower case, keep char
         * Returns:  string
         * Inputs:   string
         */
        String.prototype.RemoveAccents = function () {
            // https://github.com/yui/yui3/blob/master/src/text/js/text-data-accentfold.js
            var AccentFold = {
                0: /[\u2070\u2080\u24EA\uFF10]/gi,
                1: /[\u00B9\u2081\u2460\uFF11]/gi,
                2: /[\u00B2\u2082\u2461\uFF12]/gi,
                3: /[\u00B3\u2083\u2462\uFF13]/gi,
                4: /[\u2074\u2084\u2463\uFF14]/gi,
                5: /[\u2075\u2085\u2464\uFF15]/gi,
                6: /[\u2076\u2086\u2465\uFF16]/gi,
                7: /[\u2077\u2087\u2466\uFF17]/gi,
                8: /[\u2078\u2088\u2467\uFF18]/gi,
                9: /[\u2079\u2089\u2468\uFF19]/gi,
                a: /[\u00AA\u00E0-\u00E5\u0101\u0103\u0105\u01CE\u01DF\u01E1\u01FB\u0201\u0203\u0227\u1D43\u1E01\u1E9A\u1EA1\u1EA3\u1EA5\u1EA7\u1EA9\u1EAB\u1EAD\u1EAF\u1EB1\u1EB3\u1EB5\u1EB7\u24D0\uFF41]/gi,
                b: /[\u1D47\u1E03\u1E05\u1E07\u24D1\uFF42]/gi,
                c: /[\u00E7\u0107\u0109\u010B\u010D\u1D9C\u1E09\u24D2\uFF43]/gi,
                d: /[\u010F\u1D48\u1E0B\u1E0D\u1E0F\u1E11\u1E13\u217E\u24D3\uFF44]/gi,
                e: /[\u00E8-\u00EB\u0113\u0115\u0117\u0119\u011B\u0205\u0207\u0229\u1D49\u1E15\u1E17\u1E19\u1E1B\u1E1D\u1EB9\u1EBB\u1EBD\u1EBF\u1EC1\u1EC3\u1EC5\u1EC7\u2091\u212F\u24D4\uFF45]/gi,
                f: /[\u1DA0\u1E1F\u24D5\uFF46]/gi,
                g: /[\u011D\u011F\u0121\u0123\u01E7\u01F5\u1D4D\u1E21\u210A\u24D6\uFF47]/gi,
                h: /[\u0125\u021F\u02B0\u1E23\u1E25\u1E27\u1E29\u1E2B\u1E96\u210E\u24D7\uFF48]/gi,
                i: /[\u00EC-\u00EF\u0129\u012B\u012D\u012F\u0133\u01D0\u0209\u020B\u1D62\u1E2D\u1E2F\u1EC9\u1ECB\u2071\u2139\u2170\u24D8\uFF49]/gi,
                j: /[\u0135\u01F0\u02B2\u24D9\u2C7C\uFF4A]/gi,
                k: /[\u0137\u01E9\u1D4F\u1E31\u1E33\u1E35\u24DA\uFF4B]/gi,
                l: /[\u013A\u013C\u013E\u0140\u01C9\u02E1\u1E37\u1E39\u1E3B\u1E3D\u2113\u217C\u24DB\uFF4C]/gi,
                m: /[\u1D50\u1E3F\u1E41\u1E43\u217F\u24DC\uFF4D]/gi,
                n: /[\u00F1\u0144\u0146\u0148\u01F9\u1E45\u1E47\u1E49\u1E4B\u207F\u24DD\uFF4E]/gi,
                o: /[\u00BA\u00F2-\u00F6\u014D\u014F\u0151\u01A1\u01D2\u01EB\u01ED\u020D\u020F\u022B\u022D\u022F\u0231\u1D52\u1E4D\u1E4F\u1E51\u1E53\u1ECD\u1ECF\u1ED1\u1ED3\u1ED5\u1ED7\u1ED9\u1EDB\u1EDD\u1EDF\u1EE1\u1EE3\u2092\u2134\u24DE\uFF4F]/gi,
                p: /[\u1D56\u1E55\u1E57\u24DF\uFF50]/gi,
                q: /[\u02A0\u24E0\uFF51]/gi,
                r: /[\u0155\u0157\u0159\u0211\u0213\u02B3\u1D63\u1E59\u1E5B\u1E5D\u1E5F\u24E1\uFF52]/gi,
                s: /[\u015B\u015D\u015F\u0161\u017F\u0219\u02E2\u1E61\u1E63\u1E65\u1E67\u1E69\u1E9B\u24E2\uFF53]/gi,
                t: /[\u0163\u0165\u021B\u1D57\u1E6B\u1E6D\u1E6F\u1E71\u1E97\u24E3\uFF54]/gi,
                u: /[\u00F9-\u00FC\u0169\u016B\u016D\u016F\u0171\u0173\u01B0\u01D4\u01D6\u01D8\u01DA\u01DC\u0215\u0217\u1D58\u1D64\u1E73\u1E75\u1E77\u1E79\u1E7B\u1EE5\u1EE7\u1EE9\u1EEB\u1EED\u1EEF\u1EF1\u24E4\uFF55]/gi,
                v: /[\u1D5B\u1D65\u1E7D\u1E7F\u2174\u24E5\uFF56]/gi,
                w: /[\u0175\u02B7\u1E81\u1E83\u1E85\u1E87\u1E89\u1E98\u24E6\uFF57]/gi,
                x: /[\u02E3\u1E8B\u1E8D\u2093\u2179\u24E7\uFF58]/gi,
                y: /[\u00FD\u00FF\u0177\u0233\u02B8\u1E8F\u1E99\u1EF3\u1EF5\u1EF7\u1EF9\u24E8\uFF59]/gi,
                z: /[\u017A\u017C\u017E\u1DBB\u1E91\u1E93\u1E95\u24E9\uFF5A]/gi
            };
            var text = this.toLowerCase();
            for (var letter in AccentFold) {
                if (Object.prototype.hasOwnProperty.call(AccentFold, letter)) {
                    var regex = AccentFold[letter];
                    text = text.replace(regex, letter);
                }
            }
            return text;
        };
        function GetPackageVersion(packageName) {
            return (Lib.Version && Lib.Version[packageName]) || 1;
        }
        P2P.GetPackageVersion = GetPackageVersion;
        function GetVersionedProcessName(packageName, processName) {
            var version = GetPackageVersion(packageName);
            return version >= 2 ? processName + " V" + version : processName;
        }
        P2P.GetVersionedProcessName = GetVersionedProcessName;
        function QueryTransportVersioned(packageName, processName, filter, attributes, asAdmin) {
            var versionedProcessName = GetVersionedProcessName(packageName, processName);
            var query = asAdmin ? Process.CreateQueryAsProcessAdmin() : Query;
            query.Reset();
            query.SetSpecificTable("CDNAME#" + versionedProcessName);
            query.SetFilter(filter);
            query.SetAttributesList(attributes);
            var transport = query.MoveFirst() ? query.MoveNext() : null;
            if (!transport && versionedProcessName !== processName) {
                query.SetSpecificTable("CDNAME#" + processName);
                transport = query.MoveFirst() ? query.MoveNext() : null;
            }
            return transport;
        }
        function GetPRProcessName() {
            return GetVersionedProcessName("PAC", "Purchase requisition");
        }
        P2P.GetPRProcessName = GetPRProcessName;
        function GetPOProcessName() {
            return GetVersionedProcessName("PAC", "Purchase order");
        }
        P2P.GetPOProcessName = GetPOProcessName;
        function GetGRProcessName() {
            return GetVersionedProcessName("PAC", "Goods receipt");
        }
        P2P.GetGRProcessName = GetGRProcessName;
        function QueryPurchasingTransport(processName, filter, attributes, asAdmin) {
            return QueryTransportVersioned("PAC", processName, filter, attributes, asAdmin);
        }
        P2P.QueryPurchasingTransport = QueryPurchasingTransport;
        function QueryPOFormData(orderNumber) {
            var transport = QueryTransportVersioned("PAC", "Purchase order", "OrderNumber__=" + orderNumber, "DataFile", false);
            return transport && transport.GetFormData();
        }
        P2P.QueryPOFormData = QueryPOFormData;
        function InitSAPConfiguration(erpName, parameterInstance) {
            if (parameterInstance) {
                Variable.SetValueAsString("SAPConfiguration", Sys.Parameters.GetInstance(parameterInstance).GetParameter("SAPConfiguration"));
            }
            else if (!Variable.GetValueAsString("SAPConfiguration")) {
                Variable.SetValueAsString("SAPConfiguration", Sys.Parameters.GetInstance("P2P_" + (erpName ? erpName : Lib.ERP.GetERPName())).GetParameter("Configuration"));
            }
        }
        P2P.InitSAPConfiguration = InitSAPConfiguration;
        function IsInvestmentEnabled() {
            var erp = Lib.ERP.GetERPName();
            if (erp) {
                return Sys.Parameters.GetInstance("P2P_" + erp).GetParameter("EnableInvestment", false);
            }
            return false;
        }
        P2P.IsInvestmentEnabled = IsInvestmentEnabled;
        function GetPOMatchingMode() {
            return Sys.Parameters.GetInstance("AP").GetParameter("VerificationPOMatchingMode");
        }
        P2P.GetPOMatchingMode = GetPOMatchingMode;
        function IsGRIVEnabledGlobally() {
            return Lib.P2P.GetPOMatchingMode() === "GR";
        }
        P2P.IsGRIVEnabledGlobally = IsGRIVEnabledGlobally;
        function IsGRIVEnabledByLine() {
            return Lib.P2P.GetPOMatchingMode() === "LineItem";
        }
        P2P.IsGRIVEnabledByLine = IsGRIVEnabledByLine;
        function IsGRIVEnabled() {
            return Lib.P2P.IsGRIVEnabledGlobally() || Lib.P2P.IsGRIVEnabledByLine();
        }
        P2P.IsGRIVEnabled = IsGRIVEnabled;
        function IsPR() {
            var processName = Lib.P2P.GetPRProcessName();
            return (Sys.ScriptInfo.IsClient() && Process.GetName() === processName) ||
                (Sys.ScriptInfo.IsServer() && Data.GetValue("ProcessId") === Process.GetProcessID(processName));
        }
        P2P.IsPR = IsPR;
        function IsPO() {
            var processName = Lib.P2P.GetPOProcessName();
            return (Sys.ScriptInfo.IsClient() && Process.GetName() === processName) ||
                (Sys.ScriptInfo.IsServer() && Data.GetValue("ProcessId") === Process.GetProcessID(processName));
        }
        P2P.IsPO = IsPO;
        /**
         * Build a LDAP filter to ensure that the company code equals a given value or is empty, but not null
         * @memberof Lib.P2P
         * @param {string} companyCode The value of the company code
         */
        function GetCompanyCodeFilter(companyCode) {
            return "|(CompanyCode__=" + companyCode + ")(CompanyCode__=)(!(CompanyCode__=*))";
        }
        P2P.GetCompanyCodeFilter = GetCompanyCodeFilter;
        /**
         * Build a LDAP filter to ensure that the vendor number equals a given value or is empty, but not null
         * @memberof Lib.P2P
         * @param {string} vendorNumber The value of the vendorNumber
         */
        function GetVendorNumberFilter(vendorNumber) {
            return "|(VendorNumber__=" + vendorNumber + ")(VendorNumber__=)(!(VendorNumber__=*))";
        }
        P2P.GetVendorNumberFilter = GetVendorNumberFilter;
        P2P.GetBudgetKeyColumns = Sys.Helpers.Object.ConstantGetter(function () {
            return Sys.Parameters.GetInstance("P2P").GetParameter("BudgetKeyColumns", "CompanyCode__;PeriodCode__;CostCenter__;Group__").split(";");
        });
        P2P.GetBudgetValidationKeyColumns = Sys.Helpers.Object.ConstantGetter(function () {
            return Sys.Parameters.GetInstance("P2P").GetParameter("BudgetValidationKeyColumns", "CompanyCode__;CostCenter__").split(";");
        });
        P2P.IsCrossSectionalBudgetLineDisabled = Sys.Helpers.Object.ConstantGetter(function () {
            return Sys.Parameters.GetInstance("P2P").GetParameter("DisableCrossSectionalBudgetLine", null, { traceLevel: "Warn" });
        });
        /**
         * Build a multiple fields mapping according to the line items grouped by CostCenter
         * @memberof Lib.P2P
         * @param {Lib.P2P.MappingOptions} options The list of options
         *  exchangeRate: exchange rate (default is 1)
         *  lineItems: table object of line items
         *  costCenterColumnName: name of the Item CostCenter column
         *  amountColumnName: {string} name of the Item Amount column
         *  baseFieldsMapping: {object} common fields to extend with the generated mapping
         *  keepEmpty: {boolean} indicate if you want to keep the empty cost center line or not (default is false)
         *  emptyCheckFunction: {function} optional to check if a line should looked at or not
         */
        function BuildFieldsMapping(options) {
            // copy values on new object to keep original object free from added properties
            var optionsEx = {
                lineItems: options.lineItems,
                groupingName: options.groupingName,
                exchangeRate: options.exchangeRate || 1,
                amountColumnName: options.amountColumnName,
                baseFieldsMapping: options.baseFieldsMapping,
                keepEmpty: options.keepEmpty || false,
                emptyCheckFunction: options.emptyCheckFunction,
                fieldsDefinition: {
                    keyFieldName: "DimensionValue__",
                    computationFieldName: "WorkflowAmount__"
                },
                lineItemFieldsForWorkflow: options.lineItemFieldsForWorkflow
            };
            optionsEx.keyColumnName = options.costCenterKeyName;
            optionsEx.tableColumnName = options.costCenterColumnName;
            var fieldsCC = Lib.P2P.BuildFieldsMappingGeneric(optionsEx);
            optionsEx.keyColumnName = options.glKeyName;
            optionsEx.tableColumnName = options.glAccountColumnName;
            var fieldsGL = Lib.P2P.BuildFieldsMappingGeneric(optionsEx);
            return fieldsCC.concat(fieldsGL);
        }
        P2P.BuildFieldsMapping = BuildFieldsMapping;
        /**
         * Contains the name of the key field and the name the of the amount field
         * @typedef {object} MappingFieldsDefinition
         * @memberof Lib.P2P
         * @property {string} keyFieldName The name of the field to returned
         * @property {string} computationFieldName The name of the field returned which contains the result of the computation
         */
        /**
         * @typedef {object} MappingOptions
         * @memberof Lib.P2P
         * @property {object[]} lineItems table object of line items
         * @property {string} keyColumnName name of the Item key column
         * @property {string} amountColumnName name of the Item Amount column
         * @property {Number} [exchangeRate] exchange rate (default is 1)
         * @property {object[]} baseFieldsMapping common fields to extend with the generated mapping
         * @property {Lib.P2P.MappingFieldsDefinition} fieldsDefinition The definition of the result
         * @property {boolean} [keepEmpty] indicate if you want to keep the empty key value line or not (default is false)
         * @property {callback} emptyCheckFunction function to check if a line should looked at or not,
         */
        /**
         * Create a mapping on the keyColumnName and create a sum based on the amountColumnName
         * @memberof Lib.P2P
         * @param {Lib.P2P.MappingOptions} options The list of options for the mapping generation
         */
        function BuildFieldsMappingGeneric(options) {
            var amountForKey = {}, lineValuesForKey = {}, uniqueKeys = [], fieldsMapping = [];
            var i, line, amount;
            if (options.lineItems) {
                for (i = 0; i < options.lineItems.GetItemCount(); i++) {
                    var keyValue = {};
                    line = options.lineItems.GetItem(i);
                    if (line && (typeof options.emptyCheckFunction !== "function" || !options.emptyCheckFunction(line))) {
                        keyValue.value = line.GetValue(options.keyColumnName) || "";
                        amount = new Sys.Decimal(line.GetValue(options.amountColumnName) || 0).mul(line.GetValue(options.exchangeRate) || (Sys.Helpers.IsNumeric(options.exchangeRate) ? options.exchangeRate : 1)).toNumber();
                        if (line.GetValue("CompanyCode__")) {
                            keyValue.companyCode = line.GetValue("CompanyCode__");
                        }
                        else {
                            keyValue.companyCode = Data.GetValue("CompanyCode__");
                        }
                        keyValue.hashkey = keyValue.value + keyValue.companyCode;
                        if (options.keepEmpty || keyValue.value) {
                            if (!(keyValue.hashkey in amountForKey)) {
                                amountForKey[keyValue.hashkey] = amount;
                                uniqueKeys.push(keyValue);
                            }
                            else {
                                amountForKey[keyValue.hashkey] = new Sys.Decimal(amountForKey[keyValue.hashkey]).add(amount).toNumber();
                            }
                        }
                        var extraLineItemFieldsForWorkflow = options.lineItemFieldsForWorkflow || {};
                        // Add line values but as array as we are grouped
                        for (var f in extraLineItemFieldsForWorkflow) {
                            // Check this is an flx field not any standard object property
                            if (f.lastIndexOf("__", f.length) === (f.length - 2)) {
                                var fieldName = f.substr(f.lastIndexOf("-", f.length) + 1);
                                if (!lineValuesForKey[keyValue.hashkey]) {
                                    lineValuesForKey[keyValue.hashkey] = {};
                                }
                                if (!lineValuesForKey[keyValue.hashkey][f]) {
                                    lineValuesForKey[keyValue.hashkey][f] = [];
                                }
                                lineValuesForKey[keyValue.hashkey][f].push(line.GetValue(fieldName));
                            }
                        }
                    }
                }
            }
            // Here we iterate on uniqueKeys array because the order of keys isn't garanteed in a js obect with numerical keys.
            Sys.Helpers.Array.ForEach(uniqueKeys, function (key) {
                var result = {
                    "values": {
                        CompanyCode__: null,
                        DimensionColumnInCT__: null,
                        DimensionValue__: null
                    }
                };
                Sys.Helpers.Extend(result.values, lineValuesForKey[key.hashkey]);
                if (key.companyCode) {
                    result.values.CompanyCode__ = key.companyCode;
                }
                else {
                    delete result.values.CompanyCode__;
                }
                result.values.DimensionColumnInCT__ = options.tableColumnName;
                result.values.DimensionValue__ = key.value;
                result.values[options.fieldsDefinition.computationFieldName] = amountForKey[key.hashkey];
                fieldsMapping.push(Sys.Helpers.Extend(true, result, options.baseFieldsMapping));
            });
            return fieldsMapping;
        }
        P2P.BuildFieldsMappingGeneric = BuildFieldsMappingGeneric;
        /**
         * Build a multiple fields mapping according to the line items grouped by PO number
         * @memberof Lib.P2P
         * @param {object} options The list of options
         *  lineItems: table object of line items
         *  poNumberColumnName: name of the Item CostCenter column
         *  baseFieldsMapping: {object} common fields to extend with the generated mapping
         *  emptyCheckFunction: {function} optional to check if a line should looked at or not
         */
        function BuildFieldsMappingByPONumber(options) {
            var poNumbers = [], fieldsMapping = [];
            var i, line;
            for (i = 0; i < options.lineItems.GetItemCount(); i++) {
                line = options.lineItems.GetItem(i);
                if (typeof options.emptyCheckFunction !== "function" || !options.emptyCheckFunction(line)) {
                    var poNumber = line.GetValue(options.poNumberColumnName) || "";
                    if (poNumbers.indexOf(poNumber) === -1) {
                        poNumbers.push(poNumber);
                    }
                }
            }
            // Here we iterate on poNumbers array because the order of po number isn't garanteed in a js obect with numerical keys.
            Sys.Helpers.Array.ForEach(poNumbers, function (poNumber) {
                fieldsMapping.push(Sys.Helpers.Extend(true, {
                    "values": {
                        "PONumber__": poNumber
                    }
                }, options.baseFieldsMapping));
            });
            return fieldsMapping;
        }
        P2P.BuildFieldsMappingByPONumber = BuildFieldsMappingByPONumber;
        /**
         * Build a multiple fields mapping according to the lines of a table
         * @memberof Lib.P2P
         * @param {object} options The list of options
         *  table {object} table object containing the data
         *  columns {Array} name of the columns to map on the field mapping
         *  baseFieldsMapping: {object} common fields to extend with the generated mapping
         *  emptyCheckFunction: {function} optional to check if a line should looked at or not
         */
        function BuildFieldsMappingFromLineItems(_options) {
            var options = {
                lineItems: _options.lineItems,
                columns: _options.columns,
                baseFieldsMapping: _options.baseFieldsMapping,
                emptyCheckFunction: _options.emptyCheckFunction
            };
            var fieldsMapping = [];
            var tableIndex, columnIndex, line;
            for (tableIndex = 0; tableIndex < options.lineItems.GetItemCount(); tableIndex++) {
                line = options.lineItems.GetItem(tableIndex);
                if (typeof options.emptyCheckFunction !== "function" || !options.emptyCheckFunction(line)) {
                    // Add specified columns to mapping object
                    var values = {};
                    for (columnIndex = 0; columnIndex < options.columns.length; columnIndex++) {
                        var columnName = void 0, valuePropertyName = void 0;
                        if (typeof options.columns[columnIndex] === "string") {
                            columnName = options.columns[columnIndex];
                            valuePropertyName = options.columns[columnIndex];
                        }
                        else {
                            columnName = options.columns[columnIndex][0];
                            valuePropertyName = options.columns[columnIndex][1];
                        }
                        if (typeof line.IsNullOrEmpty === "undefined" || !line.IsNullOrEmpty(columnName)) {
                            values[valuePropertyName] = line.GetValue(columnName);
                        }
                        else {
                            values[valuePropertyName] = null;
                        }
                    }
                    fieldsMapping.push(Sys.Helpers.Extend(true, {
                        "values": values
                    }, options.baseFieldsMapping));
                }
            }
            return fieldsMapping;
        }
        P2P.BuildFieldsMappingFromLineItems = BuildFieldsMappingFromLineItems;
        /**
         * Get the approval workflow using the workflow engine
         * @memberof Lib.P2P
         * @param {object} options The list of options
         *  fields: object passed to workflow engine
         *  allowNoCCOwner: tells if the absence of ccowner do not raises an error
         *  success: success callback. The parameter of the callback is an array of approvers:
         *	[
         *		{login,displayName,emailAddress},
         *		{login,displayName,emailAddress},
         *		...
         *	]
         * error: error callback. The parameter of the callback is a string containing an error message
         */
        function GetApprovalWorkflow(options, layoutHelper) {
            if (layoutHelper) {
                layoutHelper.DisableButtons(true, "GetApprovalWorkflow");
            }
            var getStepsOptions = {
                "debug": false,
                "definition": wfDefinition,
                "noRuleAppliedAction": options.noRuleAppliedAction,
                "fields": options.fields,
                "disableErrorIfNoRuleApplied": options.disableErrorIfNoRuleApplied,
                "nbRulesToApply": options.nbRulesToApply,
                success: function (approvers, ruleApplied, noMergedApprovers) {
                    Log.Info("Approval Workflow, rule applied: " + ruleApplied);
                    Lib.P2P.CompleteUsersInformations(approvers, ["displayname", "emailaddress"], function (users) {
                        if (layoutHelper) {
                            layoutHelper.DisableButtons(false, "GetApprovalWorkflow");
                        }
                        options.success(users, ruleApplied, noMergedApprovers);
                    });
                },
                error: function (errorMessage, ruleApplied) {
                    if (layoutHelper) {
                        layoutHelper.DisableButtons(false, "GetApprovalWorkflow");
                    }
                    options.error(errorMessage, ruleApplied);
                },
                "merger": null,
                forceMerger: !!options.forceMerger
            };
            if (options.merger) {
                getStepsOptions.merger = options.merger;
            }
            if (!wfDefinition) {
                Sys.WorkflowDefinition.Extend({
                    customs: options.allowNoCCOwner ? [
                        {
                            "stepTypes": {
                                "costCenterOwner": {
                                    "noResult": "stop",
                                    "allowEmptyCostCenter": true,
                                    "companyCodeIsOptional": options.companyCodeIsOptional
                                },
                                "dimensionManager": {
                                    "noResult": "stop",
                                    "allowEmptyDimension": true,
                                    "companyCodeIsOptional": options.companyCodeIsOptional
                                }
                            }
                        }
                    ] : [],
                    success: function (definition) {
                        Log.Info("WorkflowDefinition loaded");
                        wfDefinition = definition;
                        getStepsOptions.definition = wfDefinition;
                        Sys.WorkflowEngine.GetStepsResult(getStepsOptions);
                    },
                    error: function (errorMessage) {
                        Log.Error("failed to load WorkflowDefinition: " + errorMessage);
                        options.error("Failed to load workflow definition: " + errorMessage);
                    }
                });
            }
            else {
                Sys.WorkflowEngine.GetStepsResult(getStepsOptions);
            }
        }
        P2P.GetApprovalWorkflow = GetApprovalWorkflow;
        /*
         * users can be either a Users list (server side), or a single User (client side script)
         */
        function ResolveDemoLogin(login) {
            if (login && login.indexOf("%[reference:login:demosubstring]") !== -1) {
                // From sprint 171 variable SuffixAccount is available
                // If SuffixAccount is available, use it to resolve the login else do like before
                if (!Sys.Helpers.IsEmpty(Sys.Parameters.GetInstance("P2P").GetParameter("SuffixAccount"))) {
                    return login.replace("%[reference:login:demosubstring]", Sys.Parameters.GetInstance("P2P").GetParameter("SuffixAccount"));
                }
                var referenceValue = void 0;
                if (Sys.ScriptInfo.IsServer()) {
                    var ownerUser = g.Users.GetUser(g.Data.GetValue("OwnerId"));
                    if (ownerUser.GetValue("vendor") === "1") {
                        ownerUser = g.Users.GetUserAsProcessAdmin(Sys.Helpers.String.ExtractLoginFromDN(g.Data.GetValue("/process/OwnerId")));
                    }
                    referenceValue = ownerUser.GetValue("login");
                    // looking for the best real admin with the same profile
                    if (Sys.OnDemand.Users.IsServiceUser(referenceValue)) {
                        var realAdminUser = Sys.OnDemand.Users.TryGetBestRealUser(ownerUser);
                        referenceValue = realAdminUser.GetVars().GetValue_String("login", 0);
                    }
                }
                else {
                    referenceValue = g.User.loginId;
                    if (Sys.OnDemand.Users.IsServiceUser(referenceValue)) {
                        throw new Error("Unable to resolve demo login with service user on client side");
                    }
                }
                var firstDot = referenceValue.indexOf(".");
                var firstAt = referenceValue.indexOf("@");
                if (firstDot < firstAt) {
                    referenceValue = referenceValue.substr(firstDot + 1);
                }
                login = login.replace("%[reference:login:demosubstring]", referenceValue);
            }
            return login;
        }
        P2P.ResolveDemoLogin = ResolveDemoLogin;
        function CompleteUsersInformations(users, fields, onDoneCallback) {
            //Keep track of the original array of object
            var originalUsers = users;
            // Keep only login
            var usersLogin = Sys.Helpers.Array.Map(users, function (v) {
                return v.login;
            });
            // Find users
            Sys.OnDemand.Users.GetUsersFromLogins(usersLogin, fields, function (fullUsers) {
                onDoneCallback(Sys.Helpers.Array.Map(fullUsers, function (user, index) {
                    return {
                        login: user.login,
                        emailAddress: user.exists ? user.emailaddress : user.login,
                        displayName: user.exists ? user.displayname : user.login,
                        originalValues: originalUsers[index],
                        exists: user.exists
                    };
                }));
            });
        }
        P2P.CompleteUsersInformations = CompleteUsersInformations;
        /**
         * Compute unit price from amount and quantity. The value is returned as a String, so that 0 is set as ""
         * @memberof Lib.P2P
         * @param {(string|float)} amount
         * @param {(string|float)} quantity
         *
         */
        function ComputeUnitPrice(amount, quantity) {
            var unitPrice = parseFloat(amount) / parseFloat(quantity);
            var result = null;
            if (!isNaN(unitPrice) && unitPrice !== Infinity && unitPrice !== 0) {
                result = unitPrice;
            }
            return result;
        }
        P2P.ComputeUnitPrice = ComputeUnitPrice;
        /**
         * Warn user when no archive duration is defined (or default one (2 months)) and this feature is enabled according to his account/environment type.
         * Cases matrix:
         *	- feat. enabled(undefined)/disabled:	YES/NO
         *	- warning disabled:						NO
         *	- demo account:							NO
         *	- DEV env:								NO
         *	- QA env:								NO
         *	- PROD env:								YES
         *	- Unknown env:							YES
         * @memberof Lib.P2P
         * @param {string} paramInstance where the ArchiveDurationInMonths parameter is defined
         * @param {function} warnCallback in order to customize warning
         */
        function DisplayArchiveDurationWarning(paramInstance, warnCallback) {
            Sys.Parameters.GetInstance(paramInstance).IsReady(function () {
                var enabled = Sys.Parameters.GetInstance(paramInstance).GetParameter("ArchiveDurationWarning", null);
                // Is defined on the user parameter instance ?
                if (enabled === null) {
                    enabled = Sys.Parameters.GetInstance("P2P").GetParameter("ArchiveDurationWarning", true);
                }
                if (enabled) {
                    var archiveDuration = Sys.Parameters.GetInstance(paramInstance).GetParameter("ArchiveDurationInMonths") || parseFloat(Data.GetValue("ArchiveDuration")) || parseFloat(Data.GetValue("ArchiveDurationInMonths__")) || parseFloat(Process.GetDefaultArchiveDuration());
                    if (!Sys.Helpers.IsNumeric(archiveDuration) || parseFloat(archiveDuration) < 12) {
                        if (Sys.ScriptInfo.IsServer() || !g.User.isInDemoAccount) {
                            if (Sys.Helpers.IsFunction(warnCallback)) {
                                warnCallback();
                            }
                            Log.Warn("No archive duration defined.");
                        }
                    }
                }
            });
        }
        P2P.DisplayArchiveDurationWarning = DisplayArchiveDurationWarning;
        /**
         * Check the connected user or the specified user matches the login
         */
        function CurrentUserMatchesLogin(login, currentUser) {
            currentUser = currentUser || Sys.Helpers.Globals.User;
            if (!currentUser) {
                throw new Error("No current user specified to test the login");
            }
            var currentUserLogin = currentUser.loginId || currentUser.GetValue("Login");
            return !!login &&
                (currentUserLogin.toLowerCase() === login.toLowerCase() ||
                    currentUser.IsMemberOf(login) ||
                    currentUser.IsBackupUserOf(login));
        }
        P2P.CurrentUserMatchesLogin = CurrentUserMatchesLogin;
        /**
         * Tells if the current user is the owner of the document
         *
         * @memberof Lib.P2P
         * @returns is the current user the owner of the current document
         */
        function IsOwner() {
            return Sys.ScriptInfo.IsClient()
                && (!Data.GetValue("OwnerId")
                    || g.User.loginId === Data.GetValue("OwnerId")
                    || g.User.IsMemberOf(Data.GetValue("OwnerId")));
        }
        P2P.IsOwner = IsOwner;
        /**
         * Tells if the current user has the fullAdmin role
         * @memberof Lib.P2P
         * @returns A boolean that tells us if the current user is an Admin
         */
        function IsAdmin() {
            return Sys.ScriptInfo.IsClient() && (g.User.profileRole === "accountManagement");
        }
        P2P.IsAdmin = IsAdmin;
        /**
         * Tells if the current user has opened somebody else's document as Admin (he's the admin but not the owner of the document)
         * @memberof Lib.P2P
         * @returns A boolean that tells us if the current user opened somebody else's document as Admin (he's the admin but not the owner of the document)
         */
        function IsAdminNotOwner() {
            return Sys.ScriptInfo.IsClient() && !IsOwner() && !g.User.IsBackupUserOf(Data.GetValue("OwnerId")) && IsAdmin();
        }
        P2P.IsAdminNotOwner = IsAdminNotOwner;
        /**
         * Warn the admin that he has access to a document that doesn't belong to him, in read/write mode, because he's an admin
         * Cases matrix:
         *	- feat. enabled(undefined)/disabled:	YES/NO
         *	- warning disabled:						NO
         *	- demo account:							YES
         *	- DEV env:								YES
         *	- QA env:								YES
         *	- PROD env:								YES
         *	- Unknown env:							YES
         * @memberof Lib.P2P
         * @param {string} paramInstance where the AdminWarning parameter is defined
         * @param {function} warnCallback in order to customize warning
         * @param {additionalDisplayCondition} additionalDisplayCondition the condition to be met, other than the document state, to display the warning
         * @param {number[]} states the liste of document states the warning is displayed in
         */
        function DisplayAdminWarning(paramInstance, warnCallback, additionalDisplayCondition, states) {
            if (additionalDisplayCondition === void 0) { additionalDisplayCondition = true; }
            if (states === void 0) { states = [70, 90]; }
            Sys.Parameters.GetInstance(paramInstance).IsReady(function () {
                var enabled = Sys.Parameters.GetInstance(paramInstance).GetParameter("AdminWarning", null);
                // Is defined on the user parameter instance ?
                if (enabled === null) {
                    enabled = Sys.Parameters.GetInstance("P2P").GetParameter("AdminWarning", true);
                }
                if (enabled) {
                    var login_1 = Data.GetValue("OwnerID");
                    var isDocumentStateVerified = states.indexOf(+Data.GetValue("State")) > -1;
                    if (login_1 && Sys.ScriptInfo.IsClient() && IsAdminNotOwner() && isDocumentStateVerified && additionalDisplayCondition) {
                        Sys.OnDemand.Users.CacheByLogin.Get(login_1, Lib.P2P.attributesForUserCache).Then(function (result) {
                            var user = result[login_1];
                            if (!user.$error && Sys.Helpers.IsFunction(warnCallback)) {
                                warnCallback(user.displayname);
                            }
                        });
                    }
                }
            });
        }
        P2P.DisplayAdminWarning = DisplayAdminWarning;
        /**
         * Warn the admin that he has access to a document that doesn't belong to him, in read/write mode, because he's an admin Even if he is ooto
         * Cases matrix:
         *	- feat. enabled(undefined)/disabled:	YES/NO
         *	- warning disabled:						NO
         *	- demo account:							YES
         *	- DEV env:								YES
         *	- QA env:								YES
         *	- PROD env:								YES
         *	- Unknown env:							YES
         * @memberof Lib.P2P
         * @param {string} paramInstance where the AdminWarning parameter is defined
         * @param {function} warnCallback in order to customize warning
         * @param {additionalDisplayCondition} additionalDisplayCondition the condition to be met, other than the document state, to display the warning
         * @param {number[]} states the liste of document states the warning is displayed in
         */
        function DisplayAdminAndOOTOWarning(paramInstance, warnCallback, additionalDisplayCondition, states) {
            if (additionalDisplayCondition === void 0) { additionalDisplayCondition = true; }
            if (states === void 0) { states = [70, 90]; }
            Sys.Parameters.GetInstance(paramInstance).IsReady(function () {
                var enabled = Sys.Parameters.GetInstance(paramInstance).GetParameter("AdminWarning", null);
                // Is defined on the user parameter instance ?
                if (enabled === null) {
                    enabled = Sys.Parameters.GetInstance("P2P").GetParameter("AdminWarning", true);
                }
                if (enabled) {
                    var login_2 = Data.GetValue("OwnerID");
                    var isDocumentStateVerified = states.indexOf(+Data.GetValue("State")) > -1;
                    if (login_2 && Sys.ScriptInfo.IsClient() && IsAdmin() && isDocumentStateVerified && additionalDisplayCondition) {
                        Sys.OnDemand.Users.CacheByLogin.Get(login_2, Lib.P2P.attributesForUserCache).Then(function (result) {
                            var user = result[login_2];
                            if (!user.$error && Sys.Helpers.IsFunction(warnCallback)) {
                                warnCallback(user.displayname);
                            }
                        });
                    }
                }
            });
        }
        P2P.DisplayAdminAndOOTOWarning = DisplayAdminAndOOTOWarning;
        /**
         * Warn user if he see a document because he is the backup of the current owner of this document.
         * Cases matrix:
         *	- feat. enabled(undefined)/disabled:	YES/NO
         *	- warning disabled:						NO
         *	- demo account:							YES
         *	- DEV env:								YES
         *	- QA env:								YES
         *	- PROD env:								YES
         *	- Unknown env:							YES
         * @memberof Lib.P2P
         * @param {string} paramInstance where the BackupUserWarning parameter is defined
         * @param {function} warnCallback in order to customize warning
         */
        function DisplayBackupUserWarning(paramInstance, warnCallback, ootoUserCallback) {
            function query_CB() {
                //var that=this;
                var err = this.GetQueryError();
                if (err) {
                    return;
                }
                if (this.GetRecordsCount() > 0) {
                    var login_3 = Sys.Helpers.String.ExtractLoginFromDN(this.GetQueryValue("UserOwnerID", 0));
                    Sys.OnDemand.Users.CacheByLogin.Get(login_3, Lib.P2P.attributesForUserCache)
                        .Then(function (result) {
                        var user = result[login_3];
                        if (!user.$error && Sys.Helpers.IsFunction(warnCallback)) {
                            warnCallback(user.displayname);
                        }
                    });
                }
            }
            Sys.Parameters.GetInstance(paramInstance).IsReady(function () {
                var enabled = Sys.Parameters.GetInstance(paramInstance).GetParameter("BackupUserWarning", null);
                // Is defined on the user parameter instance ?
                if (enabled === null) {
                    enabled = Sys.Parameters.GetInstance("P2P").GetParameter("BackupUserWarning", true);
                }
                if (enabled) {
                    var login_4 = null;
                    if (ootoUserCallback) {
                        login_4 = ootoUserCallback();
                    }
                    else {
                        var ownerId = Data.GetValue("OwnerID");
                        if (Sys.ScriptInfo.IsClient() && Data.GetValue("State") === "70" && g.User.loginId !== ownerId && g.User.IsBackupUserOf(ownerId)) {
                            login_4 = Data.GetValue("OwnerID");
                        }
                    }
                    if (login_4) {
                        Sys.OnDemand.Users.CacheByLogin.Get(login_4, Lib.P2P.attributesForUserCache).Then(function (result) {
                            var user = result[login_4];
                            if (!user.$error && Sys.Helpers.IsFunction(warnCallback)) {
                                if (user.isgroup == "1") {
                                    var realUser = Sys.ScriptInfo.IsClient() ? g.User : Lib.P2P.GetValidatorOrOwner();
                                    var users = realUser.GetBackedUpUsers();
                                    var filter_1 = "(&(GROUPOWNERID=cn=" + login_4 + ",*)(UserOwnerID[=](";
                                    Sys.Helpers.Array.ForEach(users, function (key) {
                                        filter_1 += Sys.Helpers.String.EscapeValueForLdapFilterForINClause(key);
                                        filter_1 += ",";
                                    });
                                    filter_1 += ")))";
                                    Query.DBQuery(query_CB, "ODUSERGROUP", "UserOwnerID", filter_1, "", 1);
                                }
                                else {
                                    warnCallback(user.displayname);
                                }
                            }
                        });
                    }
                }
            });
        }
        P2P.DisplayBackupUserWarning = DisplayBackupUserWarning;
        /**
             * Warn user if he see a document because he is creating the document on behalf ofanother user.
             * Cases matrix:
             *	- feat. enabled(undefined)/disabled:	YES/NO
             *	- warning disabled:						NO
             *	- demo account:							YES
             *	- DEV env:								YES
             *	- QA env:								YES
             *	- PROD env:								YES
             *	- Unknown env:							YES
             * @memberof Lib.P2P
             * @param {string} paramInstance where the OnBehalfWarning parameter is defined
             * @param {function} onBehalfCallback in order to customize warning
             */
        function DisplayOnBehalfWarning(userLoginId, onBehalfCallback) {
            var enabled = Sys.Helpers.String.ToBoolean(Data.GetValue("CreatedOnBehalf"));
            if (enabled) {
                var nameToDisplay_1 = "";
                var isCreatedBy_1;
                var CreatorOwnerLogin_1 = Data.GetValue("CreatorOwnerID") ? Sys.Helpers.String.ExtractLoginFromDN(Data.GetValue("CreatorOwnerID")) : null;
                var OriginalOwnerLogin_1 = Data.GetValue("OriginalOwnerID") ? Sys.Helpers.String.ExtractLoginFromDN(Data.GetValue("OriginalOwnerID")) : null;
                var IsCreatedByYou = CreatorOwnerLogin_1 === userLoginId || CreatorOwnerLogin_1 === null;
                var IsCreatedForYou = OriginalOwnerLogin_1 === userLoginId || OriginalOwnerLogin_1 === null;
                if ((!IsCreatedByYou && IsCreatedForYou) || (IsCreatedByYou && !IsCreatedForYou)) {
                    if (!IsCreatedByYou && CreatorOwnerLogin_1 !== null) {
                        Sys.OnDemand.Users.CacheByLogin.Get(CreatorOwnerLogin_1, Lib.P2P.attributesForUserCache).Then(function (result) {
                            var user = result[CreatorOwnerLogin_1];
                            if (!user.$error) {
                                nameToDisplay_1 = user.displayname;
                                isCreatedBy_1 = true;
                                if (onBehalfCallback) {
                                    onBehalfCallback(nameToDisplay_1, isCreatedBy_1);
                                }
                            }
                        });
                    }
                    if (!IsCreatedForYou && OriginalOwnerLogin_1 !== null) {
                        Sys.OnDemand.Users.CacheByLogin.Get(OriginalOwnerLogin_1, Lib.P2P.attributesForUserCache).Then(function (result) {
                            var user = result[OriginalOwnerLogin_1];
                            if (!user.$error) {
                                nameToDisplay_1 = user.displayname;
                                isCreatedBy_1 = false;
                                if (onBehalfCallback) {
                                    onBehalfCallback(nameToDisplay_1, isCreatedBy_1);
                                }
                            }
                        });
                    }
                }
            }
        }
        P2P.DisplayOnBehalfWarning = DisplayOnBehalfWarning;
        var TopMessageWarningClass = /** @class */ (function () {
            function TopMessageWarningClass(panelControl, messageControl) {
                this.panelControl = null;
                this.messages = [];
                this.panelControl = panelControl;
                this.messageControl = messageControl;
                this.Display();
            }
            TopMessageWarningClass.prototype.Display = function () {
                if (this.messages.length > 0) {
                    if (this.messageControl.GetType() === "HTML") {
                        this.messageControl.SetCSS("div { font-size: 16px; color: white; font-weight: bold; text-align: center }");
                        this.messageControl.SetHTML("<div>" + Sys.Helpers.Array.Map(this.messages, function (v) {
                            return v.replace("\n", "<br/>");
                        }).join("<br/>") + "</div>");
                    }
                    else {
                        this.messageControl.SetLabel(this.messages.join(" \n "));
                    }
                    this.panelControl.Hide(false);
                }
                else {
                    this.panelControl.Hide(true);
                }
            };
            TopMessageWarningClass.prototype.Add = function (msg, position) {
                var i = this.Find(msg);
                if (i >= 0) {
                    return i;
                }
                if (Sys.Helpers.IsUndefined(position)) {
                    position = this.messages.length;
                }
                this.messages.splice(position, 0, msg);
                this.Display();
                return this.messages.length ? this.messages.length - 1 : 0;
            };
            TopMessageWarningClass.prototype.Remove = function (idx) {
                if (Sys.Helpers.IsString(idx)) {
                    idx = this.Find(idx);
                }
                if (idx >= 0 && idx < this.messages.length) {
                    this.messages.splice(idx, 1);
                    this.Display();
                }
            };
            TopMessageWarningClass.prototype.Find = function (msg) {
                for (var i = 0; i < this.messages.length; ++i) {
                    if (msg === this.messages[i]) {
                        return i;
                    }
                }
                return -1;
            };
            TopMessageWarningClass.prototype.Clear = function () {
                this.messages = [];
                this.panelControl.Hide(true);
            };
            return TopMessageWarningClass;
        }());
        P2P.TopMessageWarningClass = TopMessageWarningClass;
        /**
         * Object to manage the TopPaneWarning
         * @memberof Lib.P2P
         * @param {object} TopPaneWarning control object of the panel that contain the warning message
         * @param {object} TopMessageWarning__ control object of the field that contain the warning message
         */
        function TopMessageWarning(panelControl, messageControl) {
            return new TopMessageWarningClass(panelControl, messageControl);
        }
        P2P.TopMessageWarning = TopMessageWarning;
        var OnSynchronizerProgressClass = /** @class */ (function () {
            function OnSynchronizerProgressClass(_, sync) {
                this.progressPopup = (function () {
                    var currentDialog = null, dialogTitle = null, dialogMessage = null, synchronizer = null, 
                    // properties to create dialog
                    fillPopup = function (dialog) {
                        currentDialog = dialog;
                        var control = dialog.AddDescription("Message__");
                        control.SetText(dialogMessage);
                        dialog.HideDefaultButtons();
                        control = dialog.AddButton("Yes__", "_Continue");
                        control.SetSubmitStyle();
                        dialog.AddButton("Cancel__", "_Close");
                    }, commitPopup = function () {
                        onDialogAction("Yes");
                    }, handlePopup = function (dialog, tabId, event, control) {
                        if (event === "OnClick") {
                            if (control.GetName() === "Yes__") {
                                dialog.Commit();
                            }
                            else {
                                dialog.Cancel();
                            }
                            currentDialog = null;
                        }
                    }, cancelPopup = function () {
                        onDialogAction("Cancel");
                    }, 
                    // properties to display dialog
                    displaying = false, lastProgressTime = null, offsetFromLastProgressTime = 0, delayedDisplayTimeoutId = null, showDialog = function () {
                        delayedDisplayTimeoutId = null;
                        if (synchronizer.IsPending()) {
                            displaying = true;
                            Sys.Helpers.Globals.Popup.Dialog(dialogTitle, null, fillPopup, commitPopup, null, handlePopup, cancelPopup);
                        }
                    }, hideDialog = function () {
                        if (currentDialog) {
                            currentDialog.Commit();
                        }
                    }, 
                    // Action on dialog
                    onDialogAction = function (action) {
                        if (synchronizer.IsPending()) {
                            // Don't wait and quit form
                            if (action !== "Yes") {
                                synchronizer.Stop();
                                if (!g.ProcessInstance.Next("next")) {
                                    g.ProcessInstance.Quit("quit");
                                }
                            }
                            else if (waitScreenControl) {
                                setTimeout(function () {
                                    if (synchronizer.IsPending()) {
                                        waitScreenControl.Wait(true);
                                    }
                                });
                            }
                            displaying = false;
                            offsetFromLastProgressTime = new Date().getTime() - lastProgressTime;
                        }
                    }, 
                    // waitscreen
                    waitScreenControl = (function () {
                        for (var key in g.Controls) {
                            if (Object.prototype.hasOwnProperty.call(g.Controls, key) && key.endsWith("__")) {
                                return g.Controls[key];
                            }
                        }
                        return null;
                    })();
                    return {
                        Show: function (sync) {
                            if (Sys.ScriptInfo.IsClient()) {
                                if (!synchronizer) {
                                    synchronizer = sync;
                                    synchronizer.Synchronize(function () {
                                        hideDialog();
                                    });
                                }
                                dialogTitle = (synchronizer.userData && synchronizer.userData.dialogTitle) || "_Form awaiting external data loading";
                                dialogMessage = (synchronizer.userData && synchronizer.userData.dialogMessage) || "_Do you want to wait for the end of external data loading?";
                                lastProgressTime = new Date().getTime();
                                if (!displaying && delayedDisplayTimeoutId === null) {
                                    delayedDisplayTimeoutId = setTimeout(showDialog, offsetFromLastProgressTime, synchronizer);
                                }
                            }
                        }
                    };
                })();
                if (this.progressPopup) {
                    this.progressPopup.Show(sync);
                }
            }
            return OnSynchronizerProgressClass;
        }());
        P2P.OnSynchronizerProgressClass = OnSynchronizerProgressClass;
        function OnSynchronizerProgress(_, sync) {
            return new OnSynchronizerProgressClass(_, sync);
        }
        P2P.OnSynchronizerProgress = OnSynchronizerProgress;
        // isGroup can be xxx.GetValue(). Ex. Controls.workflow__.GetRow(0).ISGROUP__.GetValue()
        function GetP2PUserImage(isGroup) {
            var image = Sys.Helpers.String.ToBoolean(isGroup) ? "P2P_Workflow_group_user.png" : "P2P_Workflow_single_user.png";
            var url = Process.GetImageURL(image, true);
            return url ? url : image;
        }
        P2P.GetP2PUserImage = GetP2PUserImage;
        function SetBillingInfo(processingLabel) {
            if (processingLabel) {
                Log.Info("Set processing label: " + processingLabel);
                g.Data.SetValue("ProcessingLabel", processingLabel);
            }
            var ownerId = g.Data.GetValue("OwnerId");
            var lastSavedOwnerId = g.Data.GetValue("LastSavedOwnerId");
            // is group
            if (ownerId !== lastSavedOwnerId) {
                var info = {
                    userId: Sys.Helpers.String.ExtractLoginFromDN(lastSavedOwnerId)
                };
                Log.Info("Set billing info: " + info.userId);
                if (!Process.SetBillingInfo(info)) {
                    Log.Error("Error setting billing info: " + info.userId);
                    return false;
                }
            }
            return true;
        }
        P2P.SetBillingInfo = SetBillingInfo;
        function SetTablesToIndex(tables) {
            Variable.SetValueAsString("InternalDataTablesToIndex", tables.map(function (table) {
                return table + "," + Data.GetTable(table).GetLocalUniqueId();
            }).join(";"));
        }
        P2P.SetTablesToIndex = SetTablesToIndex;
        /**
         * Set of attributes to retrieve when query a user by login (in order to reduce the number of queries).
         * @memberof Lib.P2P
         * @constant
         */
        P2P.attributesForUserCache = [
            // general information
            "login", "displayname", "emailaddress", "isgroup",
            "phonenumber",
            // to format address
            "company", "street", "pobox", "zipcode", "city", "mailstate", "country", "mailsub", "pobox"
        ];
        var UserObject = /** @class */ (function () {
            function UserObject(loginfield) {
                this.user = null;
                this.loginField = loginfield;
            }
            UserObject.prototype.GetUser = function () {
                return this.user || (this.user = g.Users.GetUser(g.Data.GetValue(this.loginField)));
            };
            UserObject.prototype.Reset = function () {
                this.user = null;
            };
            return UserObject;
        }());
        var OwnerObject = new UserObject("OwnerId");
        P2P.GetOwner = function () {
            return OwnerObject.GetUser();
        };
        // Particular case on server. We need to reset cached OwnerObject when the owner changes
        if (Sys.ScriptInfo.IsServer()) {
            Process.ChangeOwner = Sys.Helpers.Wrap(Process.ChangeOwner, function (originalFn) {
                var params = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    params[_i - 1] = arguments[_i];
                }
                originalFn.apply(Process, params);
                OwnerObject.Reset();
            });
        }
        var ValidatorObject = new UserObject("LastSavedOwnerID");
        P2P.GetValidator = function () {
            return ValidatorObject.GetUser();
        };
        function GetValidatorOrOwner() {
            return Lib.P2P.GetValidator() || Lib.P2P.GetOwner();
        }
        P2P.GetValidatorOrOwner = GetValidatorOrOwner;
        function GetValidatorOrOwnerLogin() {
            return g.Data.GetValue("LastSavedOwnerID") || g.Data.GetValue("OwnerId");
        }
        P2P.GetValidatorOrOwnerLogin = GetValidatorOrOwnerLogin;
        function AddOnBehalfOf(currentContributor, comment) {
            if (Sys.ScriptInfo.IsClient()) {
                throw new Error("AddOnBehalfOf is not supported on client side");
            }
            var validator = Lib.P2P.GetValidator();
            if (!validator) {
                return comment;
            }
            var owner;
            if (currentContributor) {
                owner = g.Users.GetUser(currentContributor.login);
            }
            else {
                owner = Lib.P2P.GetOwner();
            }
            if (currentContributor && currentContributor.login !== validator.GetValue("Login")
                && validator.GetValue("Login") !== owner.GetValue("Login")
                && comment !== Language.Translate("_Automatically created")) {
                var prefix = "";
                if (validator.IsMemberOf(owner.GetValue("Login"))) {
                    prefix = Language.Translate("_Action performed by {0}", false, validator.GetValue("DisplayName"));
                }
                else {
                    prefix = Language.Translate("_On behalf of", false, validator.GetValue("DisplayName"), currentContributor ? currentContributor.name : owner.GetValue("DisplayName"));
                    if (owner.GetValue("IsGroup") === "1") {
                        var query = Process.CreateQuery();
                        query.SetSpecificTable("ODUSERGROUP");
                        var users = validator.GetBackedUpUsers();
                        var filter_2 = "(&(GROUPOWNERID=" + owner.GetValue("FullDN") + ")(UserOwnerID[=](";
                        Sys.Helpers.Array.ForEach(users, function (key) {
                            filter_2 += Sys.Helpers.String.EscapeValueForLdapFilterForINClause(key);
                            filter_2 += ",";
                        });
                        filter_2 += ")))";
                        query.SetFilter(filter_2);
                        query.SetAttributesList("UserOwnerID");
                        query.SetOptionEx("Limit=1");
                        var record = query.MoveFirst() ? query.MoveNextRecord() : null;
                        if (record) {
                            var onwerid = record.GetVars().GetValue_String("UserOwnerID", 0);
                            var realUser = g.Users.GetUser(onwerid);
                            if (realUser) {
                                prefix = Language.Translate("_On behalf of", false, validator.GetValue("DisplayName"), realUser.GetValue("DisplayName"));
                            }
                        }
                    }
                }
                if (comment) {
                    return prefix + "\n" + comment;
                }
                return prefix;
            }
            return comment;
        }
        P2P.AddOnBehalfOf = AddOnBehalfOf;
        function HighlightCurrentWorkflowStep(WKFController, workflowTableName, index) {
            var notMergeableRoles = ["_Role buyer"];
            function CheckPreviousRows(table, i) {
                var row = table.GetRow(i);
                var rowContributor = WKFController.GetContributorAt(row);
                var prevRow = i > 0 ? table.GetRow(--i) : null;
                var prevRowContributor = prevRow ? WKFController.GetContributorAt(prevRow) : null;
                var stillTheSame = rowContributor ? rowContributor.login == Sys.Helpers.Globals.User.loginId : false;
                var sameAsCurrentContributorRow = false;
                while (prevRow && stillTheSame && !sameAsCurrentContributorRow) {
                    if (WKFController.IsCurrentContributorAt(prevRow)) {
                        var currentContributor = prevRowContributor;
                        if (rowContributor
                            && currentContributor
                            && notMergeableRoles.indexOf(rowContributor.role) === -1
                            && rowContributor.login == g.User.loginId
                            && (rowContributor.login == currentContributor.login
                                || g.User.IsBackupUserOf(currentContributor.login)
                                || g.User.IsMemberOf(currentContributor.login))) {
                            sameAsCurrentContributorRow = true;
                        }
                        else {
                            stillTheSame = false;
                        }
                    }
                    else if (rowContributor
                        && prevRowContributor
                        && notMergeableRoles.indexOf(rowContributor.role) === -1
                        && rowContributor.login == g.User.loginId
                        && rowContributor.login == prevRowContributor.login) {
                        prevRow = i > 0 ? table.GetRow(--i) : null;
                        prevRowContributor = prevRow ? WKFController.GetContributorAt(prevRow) : null;
                    }
                    else {
                        stillTheSame = false;
                    }
                }
                return sameAsCurrentContributorRow;
            }
            function CheckRow(table, i) {
                var row = table.GetRow(i);
                if (WKFController.IsCurrentContributorAt(row) || CheckPreviousRows(table, i)) {
                    row.AddStyle("highlight");
                    // Add an image to the left of the user name
                    row.WRKFMarker__.SetImageURL("arrow.png", false);
                }
                else {
                    row.RemoveStyle("highlight");
                    // Remove previous image
                    row.WRKFMarker__.SetImageURL();
                }
            }
            if (Sys.ScriptInfo.IsClient()) {
                if (!WKFController.IsEnded()) {
                    var table = g.Controls[workflowTableName];
                    if (index) {
                        CheckRow(table, index);
                    }
                    else {
                        for (var i = 0; i < table.GetLineCount(true); i++) {
                            CheckRow(table, i);
                        }
                    }
                }
            }
        }
        P2P.HighlightCurrentWorkflowStep = HighlightCurrentWorkflowStep;
        /**
         * Add a company code filter to any LDAP filter
         * @memberof string
         * @param {string} companyCode The value of the company code
         */
        String.prototype.AddCompanyCodeFilter = function (companyCode) {
            // Don't add parenthesis if the current string already have them
            if ((this.indexOf("(") === 0 && this.lastIndexOf(")") === this.length - 1) || this == "") // Warning: !this ne fonctionne pas cf RD00011552
             {
                return "(&(" + Lib.P2P.GetCompanyCodeFilter(companyCode) + ")" + this + ")";
            }
            return "(&(" + Lib.P2P.GetCompanyCodeFilter(companyCode) + ")(" + this + "))";
        };
        /**
         * Add a vendor number filter to any LDAP filter
         * @memberof string
         * @param {string} vendorNumber The value of the vendor number
         */
        String.prototype.AddVendorNumberFilter = function (vendorNumber) {
            // Don't add parenthesis if the current string already have them
            if ((this.indexOf("(") === 0 && this.lastIndexOf(")") === this.length - 1) || this == "") // Warning: !this ne fonctionne pas cf RD00011552
             {
                return "(&(" + Lib.P2P.GetVendorNumberFilter(vendorNumber) + ")" + this + ")";
            }
            return "(&(" + Lib.P2P.GetVendorNumberFilter(vendorNumber) + ")(" + this + "))";
        };
        /**
         * Add a IsLocalPO filter to any LDAP filter
         * @memberof string
         */
        String.prototype.AddNotCreatedInERPFilter = function () {
            // Don't add parenthesis if the current string already have them
            if ((this.indexOf("(") === 0 && this.lastIndexOf(")") === this.length - 1) || this == "") // Warning: !this ne fonctionne pas cf RD00011552
             {
                return "(&(|(IsCreatedInERP__=false)(IsCreatedInERP__!=*))" + this + ")";
            }
            return "(&(|(IsCreatedInERP__=false)(IsCreatedInERP__!=*))(" + this + "))";
        };
        /**
        * Formats a number with a template.
        * In the template, the string "$seq$" is replaced by the given number with 5 figures, padded with zeroes
        * Exemple: FormatSequenceNumber("1234$seq$", 56) => "123400056"
        */
        function FormatSequenceNumber(sequenceNumber, formatTemplate, prefix) {
            return formatTemplate.replace("$seq$", Sys.Helpers.String.PadLeft(sequenceNumber, "0", 5))
                .replace("$prefix$", prefix)
                .replace("$YYYY$", new Date().getFullYear());
        }
        P2P.FormatSequenceNumber = FormatSequenceNumber;
        var glaccountToCostTypePromisedCache = {};
        function fillCostTypeFromGLAccount(item, glAccountField, costTypeField) {
            if (glAccountField === void 0) { glAccountField = "GLAccount__"; }
            if (costTypeField === void 0) { costTypeField = "CostType__"; }
            var filter = "(|(CompanyCode__=" + Data.GetValue("CompanyCode__") + ")(!(CompanyCode__=*))(CompanyCode__=))";
            var options = {
                table: "P2P - G/L account to Cost type__",
                filter: filter,
                attributes: ["FromGLAccount__", "ToGLAccount__", "CostType__"],
                maxRecords: "FULL_RESULT"
            };
            glaccountToCostTypePromisedCache[filter] = glaccountToCostTypePromisedCache[filter] || Sys.GenericAPI.PromisedQuery(options);
            return glaccountToCostTypePromisedCache[filter]
                .Then(function (results) {
                var find = false;
                if (results && results.length > 0) {
                    var glAccount = item.GetValue(glAccountField);
                    for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
                        var result = results_1[_i];
                        if (glAccount <= result.ToGLAccount__ && glAccount >= result.FromGLAccount__) {
                            item.SetValue(costTypeField, result.CostType__);
                            find = true;
                            break;
                        }
                    }
                }
                if (!find) {
                    item.SetValue(costTypeField, "OpEx");
                }
                return Sys.Helpers.Promise.Resolve(item);
            });
        }
        P2P.fillCostTypeFromGLAccount = fillCostTypeFromGLAccount;
        var FreeDimension;
        (function (FreeDimension) {
            function DefineOnSelectItem(FieldName, AttributName) {
                return function (item) {
                    var currentItem = this.GetItem();
                    if (item) {
                        currentItem.SetValue(FieldName, item.GetValue(AttributName));
                    }
                    else {
                        currentItem.SetValue(FieldName, "");
                        currentItem.SetError(FieldName, "The value could not be resolved");
                    }
                };
            }
            FreeDimension.DefineOnSelectItem = DefineOnSelectItem;
            function DefineOnUnknownOrEmptyValue(FieldName) {
                return function () {
                    var currentItem = this.GetItem();
                    currentItem.SetValue(FieldName, null);
                };
            }
            FreeDimension.DefineOnUnknownOrEmptyValue = DefineOnUnknownOrEmptyValue;
        })(FreeDimension = P2P.FreeDimension || (P2P.FreeDimension = {}));
    })(P2P = Lib.P2P || (Lib.P2P = {}));
})(Lib || (Lib = {}));
