/* eslint-disable max-depth */
/* eslint-disable dot-notation */
///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CM_Server",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Catalog Management library",
  "require": [
    "Lib_Purchasing_CM_V12.0.425.0",
    "Lib_Purchasing_CM_Workflow_V12.0.425.0",
    "Lib_Purchasing_Items_V12.0.425.0",
    "[Lib_P2P_Customization_Common]",
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_LdapUtil",
    "Sys/Sys_Helpers_CSVReader",
    "Sys/Sys_GenericAPI_Server",
    "Sys/Sys_Helpers_Object",
    "Sys/Sys_Helpers_TimeoutHelper"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            var g_CatalogTableName = "PurchasingOrderedItems__";
            var g_companyCode = Data.GetValue("CompanyCode__");
            var g_vendorNumber = Data.GetValue("VendorNumber__");
            var g_cmData = Lib.Purchasing.CM.GetExternalData();
            var g_linesToProcess;
            var g_vendorsExistence = {};
            var g_companyCodeExistence = {};
            var g_timeoutHelper = new Sys.Helpers.TimeoutHelper();
            var isInternalUpdateRequest = Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest();
            //Remove the part after the "&" of the vendor login
            function GetVendorShortLogin() {
                var vendorLogin = Data.GetValue("RequesterLogin__");
                if (vendorLogin && vendorLogin.indexOf("$") !== -1) {
                    vendorLogin = vendorLogin.substring(1 + vendorLogin.indexOf("$"));
                }
                return vendorLogin;
            }
            //Get all company code for the current vendor from AP - Vendors links__
            function GetVendorLinksInfos() {
                var vendorLinksInfos = {};
                var query = Process.CreateQueryAsProcessAdmin();
                query.Reset();
                query.SetSpecificTable("AP - Vendors links__");
                query.SetAttributesList("CompanyCode__,Number__,Number__.Name__,CompanyCode__.CompanyName__");
                query.SetFilter("ShortLoginPAC__=" + GetVendorShortLogin());
                query.SetSortOrder("CompanyCode__.CompanyName__ ASC");
                query.SetOptionEx("EnableJoin=1");
                query.MoveFirst();
                var firstRecord = true;
                var record = query.MoveNextRecord();
                while (record) {
                    var companyCode = record.GetVars().GetValue_String("CompanyCode__", 0);
                    var companyName = record.GetVars().GetValue_String("CompanyCode__.CompanyName__", 0);
                    var vendor = {
                        "CompanyName__": companyName ? companyName : companyCode,
                        "VendorNumber__": record.GetVars().GetValue_String("Number__", 0),
                        "VendorName__": record.GetVars().GetValue_String("Number__.Name__", 0)
                    };
                    if (!companyCode) {
                        vendorLinksInfos = {};
                        vendorLinksInfos["*"] = vendor;
                    }
                    vendorLinksInfos[companyCode] = vendor;
                    if (firstRecord) {
                        Data.SetValue("CompanyCode__", companyCode);
                        firstRecord = false;
                    }
                    Log.Info("Vendor Info retrieved for company code = " + companyCode + " is : " + JSON.stringify(vendor));
                    record = query.MoveNextRecord();
                }
                Data.SetValue("CompanyCode__", GetLastCompanySelected() || Data.GetValue("CompanyCode__"));
                return vendorLinksInfos;
            }
            //Get the last company description selected in order to set the same value for the current Catalog management process
            function GetLastCompanySelected() {
                var query = Process.CreateQuery();
                query.Reset();
                query.SetSpecificTable("CDNAME#Catalog management");
                query.SetAttributesList("RUIDEX,SubmitDateTime,CompanyCode__,Status__");
                query.SetSortOrder("SubmitDateTime DESC");
                query.SetFilter("!(Status__=Draft)");
                query.SetOptionEx("limit=1");
                query.MoveFirst();
                var record = query.MoveNextRecord();
                if (record) {
                    return record.GetVars().GetValue_String("CompanyCode__", 0);
                }
                return null;
            }
            /**
             * Only one CM can be sent into the WF
             */
            function IsCMSubmitable() {
                var query = Process.CreateQuery();
                query.Reset();
                query.SetSpecificTable("CDNAME#Catalog management");
                query.SetAttributesList("RUIDEX,SubmitDateTime,Status__");
                query.SetSortOrder("SubmitDateTime DESC");
                query.SetFilter("(&(Status__=ToApprove)(CompanyCode__=" + Data.GetValue("CompanyCode__") + "))");
                query.SetOptionEx("limit=1");
                query.MoveFirst();
                var record = query.MoveNextRecord();
                if (record) {
                    return false;
                }
                return true;
            }
            CM.IsCMSubmitable = IsCMSubmitable;
            function InitCompaniesAndVendorInfos() {
                var vendorLinksInfos = GetVendorLinksInfos();
                Variable.SetValueAsString("vendorLinksInfos", JSON.stringify(vendorLinksInfos));
                var companyCode = Data.GetValue("CompanyCode__");
                if (vendorLinksInfos[companyCode]) {
                    Data.SetValue("VendorNumber__", vendorLinksInfos[companyCode]["VendorNumber__"]);
                    Data.SetValue("VendorName__", vendorLinksInfos[companyCode]["VendorName__"]);
                    Log.Info("VendorNumber found : " + Data.GetValue("VendorNumber__"));
                    Log.Info("VendorName found : " + Data.GetValue("VendorName__"));
                }
                else if (vendorLinksInfos["*"]) {
                    Data.SetValue("VendorNumber__", vendorLinksInfos["*"]["VendorNumber__"]);
                    Data.SetValue("VendorName__", vendorLinksInfos["*"]["VendorName__"]);
                    Log.Info("VendorNumber found : " + Data.GetValue("VendorNumber__"));
                    Log.Info("VendorName found : " + Data.GetValue("VendorName__"));
                }
            }
            CM.InitCompaniesAndVendorInfos = InitCompaniesAndVendorInfos;
            function GetCSVReader() {
                Log.Info("Get the input CSV reader");
                // Read the first attachment "0" with the reader V2
                var csvReader = Sys.Helpers.CSVReader.CreateInstance(0, "V2");
                csvReader.ReturnSeparator = "\n";
                // read first line (Header line) and guess the separator
                csvReader.GuessSeparator();
                // Check key here
                csvReader.SupplierPartIDHeaderIdx = csvReader.GetHeaderIndex(Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierPartID);
                csvReader.SupplierAuxPartIDHeaderIdx = csvReader.GetHeaderIndex(Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierAuxPartID);
                if (csvReader.SupplierPartIDHeaderIdx < 0 || csvReader.SupplierAuxPartIDHeaderIdx < 0) {
                    csvReader.Error = {
                        CSVLine: csvReader.GetCurrentLineArray().toString(),
                        LineNumber: 1,
                        ErrorStatus: "_Missing key column SupplierPartID or SupplierAuxPartID"
                    };
                }
                if (isInternalUpdateRequest) {
                    csvReader.CompanyCodeIDHeaderIdx = csvReader.GetHeaderIndex(Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode);
                    csvReader.VendorNumberIDHeaderIdx = csvReader.GetHeaderIndex(Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.VendorNumber);
                    if (csvReader.CompanyCodeIDHeaderIdx < 0 || csvReader.VendorNumberIDHeaderIdx < 0) {
                        csvReader.Error = {
                            CSVLine: csvReader.GetCurrentLineArray().toString(),
                            LineNumber: 1,
                            ErrorStatus: "_Missing key column CompanyCode or VendorNumber"
                        };
                    }
                }
                return csvReader;
            }
            CM.GetCSVReader = GetCSVReader;
            function IsValideDate(d) {
                return d instanceof Date && !isNaN(d.getTime());
            }
            function PrepareImportJson(fromBuyer) {
                if (fromBuyer === void 0) { fromBuyer = false; }
                Log.Info("Build JSON from CSV");
                var csvReader = GetCSVReader();
                var line = csvReader.Error ? null : csvReader.GetNextLine();
                var lineCount = 0;
                var items = {};
                var companyCode;
                var vendorID;
                if (!fromBuyer) {
                    companyCode = Data.GetValue("CompanyCode__");
                    vendorID = Data.GetValue("VendorNumber__");
                }
                var ldapUtil = Sys.Helpers.LdapUtil;
                var itemsInError = [];
                if (csvReader.Error) {
                    itemsInError.push(csvReader.Error);
                }
                var linesFilterArray = [];
                while (line !== null) {
                    var lineObject = csvReader.GetCurrentLineObject();
                    if (lineObject) {
                        var keyArray = void 0;
                        if (fromBuyer) {
                            keyArray = [lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode], lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.VendorNumber], lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierPartID], lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierAuxPartID]];
                        }
                        else {
                            keyArray = [companyCode, vendorID, lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierPartID], lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierAuxPartID]];
                        }
                        var key = keyArray.join("-");
                        Log.Info("Processing CSV line: " + key);
                        var unitPrice = parseFloat(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UnitPrice]);
                        var leadTime = parseInt(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.LeadTime], 10);
                        var startDate = Sys.Helpers.Date.ISOSTringToDate(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.StartDate]);
                        var endDate = Sys.Helpers.Date.ISOSTringToDate(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.EndDate]);
                        if (items[key]) {
                            Log.Warn("Item " + key + " found twice in the CSV, skipping");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Duplicate line"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (Sys.Helpers.IsEmpty(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierPartID])) {
                            Log.Warn("Item " + key + " is missing key value Supplier part ID");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_SupplierPartID required"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (fromBuyer && Sys.Helpers.IsEmpty(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode])) {
                            Log.Warn("Item " + key + " is missing key value CompanyCode");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_CompanyCode required"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (fromBuyer && Sys.Helpers.IsEmpty(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.VendorNumber])) {
                            Log.Warn("Item " + key + " is missing key value VendorNumber");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_VendorNumber required"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (fromBuyer && !CompanyCodeExists(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode])) {
                            Log.Warn("Item " + key + " references a company code that doesn't exist");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_CompanyCode doesn't exist"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (fromBuyer && !VendorExists(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode], lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.VendorNumber])) {
                            Log.Warn("Item " + key + " references a vendor that doesn't exist");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Vendor doesn't exist"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UnitPrice] && (isNaN(unitPrice) || unitPrice <= 0)) {
                            Log.Warn("Item " + key + " has invalid UnitPrice");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Invalid unit price"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.LeadTime] && (isNaN(leadTime) || leadTime <= 0)) {
                            Log.Warn("Item " + key + " has invalid LeadTime");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Invalid LeadTime, required number"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.StartDate] && !IsValideDate(startDate)) {
                            Log.Warn("Item " + key + " has invalid StartDate");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Invalid StartDate, required YYYY-MM-DD format"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.EndDate] && !IsValideDate(endDate)) {
                            Log.Warn("Item " + key + " has invalid EndDate");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Invalid EndDate, required YYYY-MM-DD format"
                            };
                            itemsInError.push(csvError);
                        }
                        else if (lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UNSPSC] && lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UNSPSC].length != 8) {
                            Log.Warn("Item " + key + " has invalid UNSPSC");
                            var csvError = {
                                LineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString(),
                                ErrorStatus: "_Invalid UNSPSC, UNSPSC has a lenght of 8 number"
                            };
                            itemsInError.push(csvError);
                        }
                        else {
                            items[key] = {
                                ExtractedData: {
                                    Name: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.Name],
                                    Description: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.Description],
                                    SupplierPartID: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierPartID],
                                    SupplierAuxPartID: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.SupplierAuxPartID],
                                    ManufacturerName: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.ManufacturerName],
                                    ManufacturerID: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.ManufacturerID],
                                    UnitPrice: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UnitPrice] ? unitPrice : null,
                                    Currency: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.Currency],
                                    LeadTime: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.LeadTime] ? leadTime : null,
                                    UOM: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UOM],
                                    UNSPSC: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.UNSPSC],
                                    StartDate: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.StartDate] ? startDate : null,
                                    EndDate: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.EndDate] ? endDate : null,
                                    img: lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.img],
                                    deleted: Sys.Helpers.String.ToBoolean(lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.deleted])
                                },
                                CSVLineNumber: lineCount + 1,
                                CSVLine: csvReader.GetCurrentLineArray().toString()
                            };
                            if (fromBuyer) {
                                items[key].ExtractedData.CostType = lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CostType];
                                items[key].ExtractedData.CompanyCode = lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.CompanyCode];
                                items[key].ExtractedData.VendorNumber = lineObject[Lib.Purchasing.CM.EXTERACTED_DATA_TO_CSV_MAPPING.VendorNumber];
                            }
                            var SupplierPartIDFilter = ldapUtil.FilterEqual("ItemNumber__", items[key].ExtractedData.SupplierPartID);
                            var SupplierAuxPartIDFilter = ldapUtil.FilterEqual("ItemSupplierPartAuxID__", items[key].ExtractedData.SupplierAuxPartID);
                            var itemFilter = ldapUtil.FilterAnd(SupplierPartIDFilter, SupplierAuxPartIDFilter);
                            if (fromBuyer) {
                                for (var keyField in lineObject) {
                                    if (!(keyField in items[key].ExtractedData) && !(Lib.Purchasing.CM.CSV_TO_EXTERACTED_DATA_MAPPING[keyField] in items[key].ExtractedData) && !Sys.Helpers.IsEmpty(lineObject[keyField])) {
                                        items[key].ExtractedData[keyField] = lineObject[keyField];
                                    }
                                }
                                var ccFilter = ldapUtil.FilterEqual("ItemCompanyCode__", items[key].ExtractedData.CompanyCode);
                                var vendorFilter = ldapUtil.FilterEqual("VendorNumber__", items[key].ExtractedData.VendorNumber);
                                itemFilter = ldapUtil.FilterAnd(ccFilter, vendorFilter, SupplierPartIDFilter, SupplierAuxPartIDFilter);
                            }
                            linesFilterArray.push(itemFilter);
                        }
                        lineCount++;
                    }
                    line = csvReader.GetNextLine();
                }
                Log.Info(lineCount + " lines parsed");
                var ldapFilter = ldapUtil.FilterOr.apply(ldapUtil, linesFilterArray);
                if (!fromBuyer) {
                    var ccFilter = ldapUtil.FilterEqual("ItemCompanyCode__", companyCode);
                    var vendorFilter = ldapUtil.FilterEqual("VendorNumber__", vendorID);
                    ldapFilter = ldapUtil.FilterAnd(ccFilter, vendorFilter, ldapUtil.FilterOr.apply(ldapUtil, linesFilterArray));
                }
                return SetActionToPerform(items, ldapFilter)
                    .Then(function (catalogresult) {
                    var importJson = {
                        Lines: catalogresult,
                        ParsingError: itemsInError,
                        ProcessingError: []
                    };
                    return Sys.Helpers.Promise.Resolve(importJson);
                });
            }
            CM.PrepareImportJson = PrepareImportJson;
            function SetActionToPerform(items, ldapFilter) {
                var queryOptions = {
                    table: "PurchasingOrderedItems__",
                    filter: ldapFilter.toString(),
                    attributes: ["ItemNumber__", "ItemSupplierPartAuxID__"],
                    additionalOptions: {
                        asAdmin: true
                    }
                };
                return Sys.GenericAPI.PromisedQuery(queryOptions)
                    .Then(function (results) {
                    var _loop_1 = function (key) {
                        if (Object.prototype.hasOwnProperty.call(items, key)) {
                            var found = Sys.Helpers.Object.Find(results, function (data) {
                                if (data.ItemNumber__ && data.ItemNumber__ == items[key].ExtractedData.SupplierPartID
                                    && data.ItemSupplierPartAuxID__ == items[key].ExtractedData.SupplierAuxPartID) {
                                    return true;
                                }
                                return false;
                            });
                            if (items[key].ExtractedData.deleted) {
                                items[key].ActionToDo = "Deleted";
                            }
                            else if (found) {
                                items[key].ActionToDo = "Modified";
                            }
                            else {
                                items[key].ActionToDo = "Added";
                            }
                        }
                    };
                    for (var key in items) {
                        _loop_1(key);
                    }
                    return Sys.Helpers.Promise.Resolve(items);
                });
            }
            CM.SetActionToPerform = SetActionToPerform;
            function GetPreviousData(record) {
                var vars = record.GetVars();
                var catalogData = {};
                var size = vars.GetNbAttributes();
                for (var i = 0; i < size; ++i) {
                    var attrName = vars.GetAttribute(i);
                    if (attrName.endsWith("__")) {
                        catalogData[attrName] = vars.GetValue_String(attrName, 0);
                    }
                }
                return catalogData;
            }
            function GetRecord(lineId, item) {
                var query = Process.CreateQueryAsProcessAdmin();
                query.SetSpecificTable("CDNAME#" + g_CatalogTableName);
                var filter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("ItemCompanyCode__", isInternalUpdateRequest ? item.ExtractedData.CompanyCode : g_companyCode), Sys.Helpers.LdapUtil.FilterEqual("ItemNumber__", item.ExtractedData.SupplierPartID), Sys.Helpers.LdapUtil.FilterEqual("ItemSupplierPartAuxID__", item.ExtractedData.SupplierAuxPartID), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", isInternalUpdateRequest ? item.ExtractedData.VendorNumber : g_vendorNumber)).toString();
                query.SetFilter(filter);
                if (!query.MoveFirst()) {
                    g_cmData.ProcessingError.push({
                        LineId: lineId,
                        LineType: item.ActionToDo,
                        ErrorStatus: "Find : " + query.GetLastErrorMessage(),
                        Error: query.GetLastError()
                    });
                }
                return query.MoveNextRecord();
            }
            function ProcessLines() {
                var supplyTypesCache = {};
                Log.Info("Start ProcessLines");
                var _loop_2 = function (lineId) {
                    if (Object.prototype.hasOwnProperty.call(g_cmData.Lines, lineId) && Sys.Helpers.IsEmpty(g_cmData.Lines[lineId].ActionDone)) {
                        var item = g_cmData.Lines[lineId];
                        var extractedData_1 = item.ExtractedData;
                        var record = GetRecord(lineId, item);
                        if (record) {
                            item.PreviousData = GetPreviousData(record);
                        }
                        if (item.ActionToDo === "Deleted") {
                            if (record) {
                                var ret = record.Delete();
                                if (ret) {
                                    g_cmData.ProcessingError.push({
                                        LineId: lineId,
                                        LineType: "Deleted",
                                        ErrorStatus: "Delete : " + record.GetLastErrorMessage(),
                                        Error: ret
                                    });
                                }
                                else {
                                    item.WriteDateTime = new Date();
                                    item.ActionDone = "Deleted";
                                }
                            }
                            else {
                                item.WriteDateTime = new Date();
                                item.ActionDone = "Nothing";
                            }
                        }
                        else {
                            var actionToDo = void 0;
                            if (!record) {
                                actionToDo = "Added";
                                record = Process.CreateTableRecordAsProcessAdmin(g_CatalogTableName);
                            }
                            else {
                                actionToDo = "Modified";
                            }
                            var vars_1 = record.GetVars();
                            for (var attr in extractedData_1) {
                                if (Object.prototype.hasOwnProperty.call(extractedData_1, attr)) {
                                    if (Lib.Purchasing.CM.EXTRACTED_DATA_TO_CATALOG_MAPPING[attr]) {
                                        vars_1.AddValue_String(Lib.Purchasing.CM.EXTRACTED_DATA_TO_CATALOG_MAPPING[attr], extractedData_1[attr], true);
                                    }
                                    else if (isInternalUpdateRequest) {
                                        vars_1.AddValue_String(attr + "__", extractedData_1[attr], true);
                                    }
                                }
                            }
                            if (isInternalUpdateRequest) {
                                if (Sys.Parameters.GetInstance("PAC").GetParameter("DisplayItemType")) {
                                    vars_1.AddValue_String("ItemType__", extractedData_1.ItemType, true);
                                }
                            }
                            else {
                                vars_1.AddValue_String("ItemCompanyCode__", g_companyCode, true);
                                vars_1.AddValue_String("VendorNumber__", g_vendorNumber, true);
                            }
                            if (extractedData_1.UNSPSC) {
                                Lib.Purchasing.Items.QueryUNSPSCSupplyType(extractedData_1.UNSPSC)
                                    .Then(function (result) {
                                    if (!Object.prototype.hasOwnProperty.call(extractedData_1, "SupplyTypeID")) {
                                        var supplyTypeId_1 = result && result.length ? result[0].SupplyTypeId__ : "";
                                        var customSupplyTypeId = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetSupplyTypeIdFromUNSPSC", extractedData_1.UNSPSC, supplyTypeId_1);
                                        supplyTypeId_1 = customSupplyTypeId || supplyTypeId_1;
                                        vars_1.AddValue_String("SupplyTypeID__", supplyTypeId_1, true);
                                        // Fill Cost Type or Gl Account
                                        if (!extractedData_1.CostType || !extractedData_1.GLAccount) {
                                            if (Object.prototype.hasOwnProperty.call(supplyTypesCache, supplyTypeId_1)) {
                                                if (!extractedData_1.CostType && supplyTypesCache[supplyTypeId_1].DefaultCostType__) {
                                                    vars_1.AddValue_String("CostType__", supplyTypesCache[supplyTypeId_1].DefaultCostType__, true);
                                                }
                                                if (!extractedData_1.GLAccount && supplyTypesCache[supplyTypeId_1].DefaultGLAccount__) {
                                                    vars_1.AddValue_String("ItemGLAccount__", supplyTypesCache[supplyTypeId_1].DefaultGLAccount__, true);
                                                }
                                            }
                                            else {
                                                var options = {
                                                    table: "PurchasingSupply__",
                                                    filter: "(&(SupplyID__=" + supplyTypeId_1 + ")(|(CompanyCode__=" + g_companyCode + ")(!(CompanyCode__=*))(CompanyCode__=)))",
                                                    attributes: ["SupplyID__", "DefaultCostType__", "DefaultGLAccount__"],
                                                    maxRecords: 1
                                                };
                                                Sys.GenericAPI.PromisedQuery(options)
                                                    .Then(function (queryResult) {
                                                    if (queryResult.length > 0 && queryResult[0]) {
                                                        supplyTypesCache[supplyTypeId_1] = queryResult[0];
                                                        if (!extractedData_1.CostType && supplyTypesCache[supplyTypeId_1].DefaultCostType__) {
                                                            vars_1.AddValue_String("CostType__", supplyTypesCache[supplyTypeId_1].DefaultCostType__, true);
                                                        }
                                                        if (!extractedData_1.GLAccount && supplyTypesCache[supplyTypeId_1].DefaultGLAccount__) {
                                                            vars_1.AddValue_String("ItemGLAccount__", supplyTypesCache[supplyTypeId_1].DefaultGLAccount__, true);
                                                        }
                                                    }
                                                    else {
                                                        supplyTypesCache[supplyTypeId_1] = null;
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                            }
                            var ret = record.Commit();
                            if (ret) {
                                g_cmData.ProcessingError.push({
                                    LineId: lineId,
                                    LineType: item.ActionToDo,
                                    ErrorStatus: "Commit : " + record.GetLastErrorMessage(),
                                    Error: ret
                                });
                            }
                            else {
                                item.WriteDateTime = new Date();
                                item.ActionDone = actionToDo;
                            }
                        }
                        g_timeoutHelper.NotifyIteration();
                    }
                };
                for (var _i = 0, g_linesToProcess_1 = g_linesToProcess; _i < g_linesToProcess_1.length; _i++) {
                    var lineId = g_linesToProcess_1[_i];
                    _loop_2(lineId);
                }
                Log.Info("End ProcessLines");
            }
            CM.ProcessLines = ProcessLines;
            function RevertLines() {
                Log.Info("Start RevertLines");
                for (var _i = 0, g_linesToProcess_2 = g_linesToProcess; _i < g_linesToProcess_2.length; _i++) {
                    var lineId = g_linesToProcess_2[_i];
                    if (Object.prototype.hasOwnProperty.call(g_cmData.Lines, lineId)) {
                        var item = g_cmData.Lines[lineId];
                        var record = GetRecord(lineId, item);
                        if (item.ActionDone === "Added") {
                            if (record) {
                                var ret = record.Delete();
                                if (ret) {
                                    g_cmData.ProcessingError.push({
                                        LineId: lineId,
                                        LineType: "Deleted",
                                        ErrorStatus: "Delete : " + record.GetLastErrorMessage(),
                                        Error: ret
                                    });
                                }
                            }
                        }
                        else if (item.ActionDone === "Modified" || item.ActionDone === "Deleted") {
                            if (!record) // item.ActionDone === "Deleted"
                             {
                                record = Process.CreateTableRecordAsProcessAdmin(g_CatalogTableName);
                            }
                            var vars = record.GetVars();
                            for (var attr in item.PreviousData) {
                                if (Object.prototype.hasOwnProperty.call(item.PreviousData, attr)) {
                                    vars.AddValue_String(attr, item.PreviousData[attr], true);
                                }
                            }
                            var ret = record.Commit();
                            if (ret) {
                                g_cmData.ProcessingError.push({
                                    LineId: lineId,
                                    LineType: item.ActionToDo,
                                    ErrorStatus: "Commit : " + record.GetLastErrorMessage(),
                                    Error: ret
                                });
                            }
                        }
                        g_timeoutHelper.NotifyIteration();
                    }
                }
                Log.Info("End RevertLines");
            }
            CM.RevertLines = RevertLines;
            function CompanyCodeExists(companyCode) {
                var exists = false;
                var key = companyCode;
                if (Object.prototype.hasOwnProperty.call(g_companyCodeExistence, key)) {
                    exists = g_companyCodeExistence[key];
                }
                else {
                    var query = Process.CreateQueryAsProcessAdmin();
                    query.SetSpecificTable("PurchasingCompanycodes__");
                    var filter = Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode).toString();
                    query.SetFilter(filter);
                    if (query.MoveFirst() && query.MoveNext()) {
                        exists = true;
                    }
                    g_companyCodeExistence[key] = exists;
                }
                return exists;
            }
            function VendorExists(companyCode, vendorNumber) {
                var exists = false;
                var key = companyCode + "_" + vendorNumber;
                if (Object.prototype.hasOwnProperty.call(g_vendorsExistence, key)) {
                    exists = g_vendorsExistence[key];
                }
                else {
                    var query = Process.CreateQueryAsProcessAdmin();
                    query.SetSpecificTable("AP - Vendors__");
                    var filter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", companyCode), Sys.Helpers.LdapUtil.FilterEqual("Number__", vendorNumber)).toString();
                    query.SetFilter(filter);
                    if (query.MoveFirst() && query.MoveNext()) {
                        exists = true;
                    }
                    g_vendorsExistence[key] = exists;
                }
                return exists;
            }
            /**
             * Returns a critical section name to prevent concurrent access on catalog.
             * @param {string} companyCode companyCode we want to lock
             * @param {string} vendorNumber vendor number we want to lock
             * @returns {string} critical section name
             */
            function GetCriticalSection(companyCode, vendorNumber) {
                return "cataloglck" + companyCode + vendorNumber;
            }
            /**
             * If we could not take the lock, return false
             * @returns {boolean}
             */
            function ProcessedData(callBack) {
                if (isInternalUpdateRequest) {
                    var CC_VN_pairs = {};
                    for (var lineId in g_cmData.Lines) {
                        if (Object.prototype.hasOwnProperty.call(g_cmData.Lines, lineId)) {
                            var key = g_cmData.Lines[lineId].ExtractedData.CompanyCode + "_" + g_cmData.Lines[lineId].ExtractedData.VendorNumber;
                            if (Object.prototype.hasOwnProperty.call(CC_VN_pairs, key)) {
                                CC_VN_pairs[key][lineId] = g_cmData.Lines[lineId];
                            }
                            else {
                                CC_VN_pairs[key] = {};
                                CC_VN_pairs[key][lineId] = g_cmData.Lines[lineId];
                            }
                        }
                    }
                    //reset error before processing
                    g_cmData.ProcessingError = [];
                    for (var CC_VN in CC_VN_pairs) {
                        if (Object.prototype.hasOwnProperty.call(CC_VN_pairs, CC_VN)) {
                            g_linesToProcess = Object.keys(CC_VN_pairs[CC_VN]);
                            var tab = CC_VN.split("_");
                            var criticalSection = GetCriticalSection(tab[0], tab[1]);
                            // Protect concurrent access on impacted catalog
                            var lockOk = Process.PreventConcurrentAccess(criticalSection, callBack, null, 30);
                            if (!lockOk) {
                                return false;
                            }
                        }
                    }
                    Lib.Purchasing.CM.SetExternalData(g_cmData);
                }
                else {
                    //reset error before processing
                    g_cmData.ProcessingError = [];
                    g_linesToProcess = Object.keys(g_cmData.Lines);
                    var criticalSection = GetCriticalSection(g_companyCode, g_vendorNumber);
                    // Protect concurrent access on impacted catalog
                    var lockOk = Process.PreventConcurrentAccess(criticalSection, callBack, null, 30);
                    if (!lockOk) {
                        return false;
                    }
                    Lib.Purchasing.CM.SetExternalData(g_cmData);
                }
                return true;
            }
            CM.ProcessedData = ProcessedData;
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
