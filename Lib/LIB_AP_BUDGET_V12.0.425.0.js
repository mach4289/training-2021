/* LIB_DEFINITION{
  "name": "Lib_AP_Budget",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Budget library for Invoice",
  "require": [
    "Lib_Budget_V12.0.425.0",
    "[Lib_Budget_Updater_V12.0.425.0]",
    "Lib_P2P_V12.0.425.0",
    "Lib_CommonDialog_V12.0.425.0",
    "Sys/Sys_Decimal"
  ]
}*/
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var Budget;
        (function (Budget) {
            Lib.Budget.ExtendConfiguration({
                GetBudgetKeyColumns: Lib.P2P.GetBudgetKeyColumns,
                sourceTypes: {
                    // Configuration used for the new document in package V2
                    "30": {
                        CheckSourceType: function (document) {
                            if (Sys.ScriptInfo.IsClient()) {
                                return Process.GetName() === "Vendor invoice";
                            }
                            var processID = document ? document.GetUninheritedVars().GetValue_String("ProcessId", 0) : Data.GetValue("ProcessId");
                            return processID === Process.GetProcessID("Vendor invoice");
                        },
                        IsEmptyItem: function (item) {
                            return Lib.P2P.InvoiceLineItem.IsEmpty(item);
                        },
                        DeduceImpactAction: function (data, options) {
                            var postingDate = data.GetValue("ERPPostingDate__");
                            var state = parseInt(options.stateToDeduceImpactAction || Lib.Budget.GetBuiltinDocumentValue(options.document, "State"), 10);
                            var status = data.GetValue("InvoiceStatus__");
                            return postingDate && state <= 100 && status != "Rejected" ? "invoiced" : "none";
                        },
                        PrepareImpact: function (data, impactAction, item /*, i*/) {
                            var amountFactor = 1;
                            if (impactAction === "none") {
                                return new Lib.Budget.Impact();
                            }
                            else if (impactAction === "invoiceReversed") {
                                amountFactor = -1;
                            }
                            var previousBudgetID = item.GetValue("PreviousBudgetID__");
                            var invoiceAmount = new Sys.Decimal(item.GetValue("Amount__")).mul(data.GetValue("ExchangeRate__"));
                            if (Lib.P2P.InvoiceLineItem.IsGLLineItem(item)) {
                                return new Lib.Budget.Impact({
                                    InvoicedNonPO__: invoiceAmount.mul(amountFactor).toNumber()
                                });
                            }
                            var invoicedQuantity = new Sys.Decimal(item.GetValue("Quantity__"));
                            var expectedQuantity = new Sys.Decimal(item.GetValue("ExpectedQuantity__"));
                            var unitPrice = new Sys.Decimal(item.GetValue("UnitPrice__"));
                            var deliveredAmount = expectedQuantity.mul(unitPrice).mul(data.GetValue("ExchangeRate__"));
                            var expectedInvoicedAmount = invoicedQuantity.mul(unitPrice).mul(data.GetValue("ExchangeRate__"));
                            // The impact on the Received column can not exceed the previously received amount
                            // So no need to know that it's the last reception
                            // We need to know if there will be no more invoices to correct the Received column
                            // in case not all received items will be invoiced
                            // Since we don't have the information, we consider that all received items will be invoiced
                            // If needed, the invoice now contains a deliveryCompleted column
                            var previousBudgetImpact = invoicedQuantity.lessThan(expectedQuantity) ? expectedInvoicedAmount.mul(-1) : deliveredAmount.mul(-1);
                            var noGoodsReceiptVal = item.GetValue("NoGoodsReceipt__");
                            if (noGoodsReceiptVal === true || noGoodsReceiptVal === "1") {
                                return new Lib.Budget.MultiImpact([
                                    {
                                        budgetID: previousBudgetID,
                                        impact: new Lib.Budget.Impact({
                                            Ordered__: previousBudgetID ? previousBudgetImpact.mul(amountFactor).toNumber() : 0 // unallocate budget only if there was a budget
                                        })
                                    },
                                    {
                                        budgetID: "",
                                        impact: new Lib.Budget.Impact({
                                            InvoicedPO__: invoiceAmount.mul(amountFactor).toNumber()
                                        })
                                    }
                                ]);
                            }
                            return new Lib.Budget.MultiImpact([
                                {
                                    budgetID: previousBudgetID,
                                    impact: new Lib.Budget.Impact({
                                        Received__: previousBudgetID ? previousBudgetImpact.mul(amountFactor).toNumber() : 0 // unallocate budget only if there was a budget
                                    })
                                },
                                {
                                    budgetID: "",
                                    impact: new Lib.Budget.Impact({
                                        InvoicedPO__: invoiceAmount.mul(amountFactor).toNumber()
                                    })
                                }
                            ]);
                        },
                        formTable: "LineItems__",
                        mappings: {
                            common: {
                                "OperationID__": "InvoiceNumber__",
                                "CompanyCode__": "CompanyCode__"
                            },
                            byLine: {
                                "BudgetID__": "BudgetID__",
                                "PeriodCode__": getInvoiceImpactDate,
                                "CostCenter__": "CostCenter__",
                                "Group__": "Group__"
                            }
                        }
                    }
                }
            });
            var poDataCache = {};
            /**
            * Return the receipt or requested receipt date for a specific item
            */
            function getItemReceiptDate(invoiceItem) {
                // add a local cache for the purchase order data
                poDataCache = poDataCache || {};
                var orderNumber = invoiceItem.GetValue("OrderNumber__");
                if (!poDataCache[orderNumber]) {
                    poDataCache[orderNumber] = Lib.P2P.QueryPOFormData(orderNumber);
                }
                var poData = poDataCache[orderNumber];
                var receiptDate = null;
                if (poData) {
                    // Use the requested receipt date for unreceived item (on PO form)
                    // we find the most recent delivery date
                    var table = poData.GetTable("LineItems__");
                    for (var i = 0; i < table.GetItemCount(); i++) {
                        var line = table.GetItem(i);
                        var reqDeliveryDate = Sys.Helpers.Date.ISOSTringToDate(line.GetValue("ItemRequestedDeliveryDate__"));
                        if (reqDeliveryDate && (!receiptDate || Sys.Helpers.Date.CompareDate(receiptDate, reqDeliveryDate) > 0)) {
                            receiptDate = reqDeliveryDate;
                        }
                    }
                }
                return Sys.Helpers.Date.Date2DBDate(receiptDate);
            }
            /**
            * Return the date for the BudgetImpact object based on invoice line type
            **/
            function getInvoiceImpactDate(item) {
                var impactDate;
                var postingDate = Data.GetValue("PostingDate__");
                if (!postingDate) {
                    var extPostingDate = new Date(Variable.GetValueAsString("PostingDate__"));
                    if (extPostingDate) {
                        postingDate = extPostingDate;
                        Log.Warn("The posting date is empty and the budget is executed with a posting date posting date found in external variable");
                    }
                    else {
                        postingDate = new Date();
                        Log.Warn("The posting date is empty and the budget is executed with a posting date equal to today");
                    }
                }
                if (Lib.P2P.InvoiceLineItem.IsGLLineItem(item)) {
                    impactDate = Sys.Helpers.Date.Date2DBDate(postingDate);
                }
                else {
                    impactDate = getItemReceiptDate(item);
                    if (!impactDate) {
                        impactDate = Sys.Helpers.Date.Date2DBDate(postingDate);
                    }
                }
                return impactDate;
            }
            function FillItemBudgetID(budgets) {
                // for PO item, BudgetID__ is filled in customScript from "AP - Purchase order - Items__"
                // for Non PO item, BudgetID__ is known after GetBudgets during UpdateBudgets --> filling
                // Do nothing in recovery
                if (!(budgets.options && budgets.options.document)) {
                    var formTable = budgets.sourceTypeConfig.formTable;
                    Sys.Helpers.Data.ForEachTableItem(formTable, function (item, i) {
                        var budgetID = budgets.byItemIndex[i];
                        if (Sys.Helpers.IsString(budgetID)) {
                            item.SetValue("BudgetID__", budgetID);
                        }
                    });
                }
            }
            function UpdateBudgets(options) {
                var promise = Sys.Helpers.Promise.Create(function (resolve, reject) {
                    Lib.Budget.GetBudgets(options)
                        .Then(function (budgets) {
                        FillItemBudgetID(budgets);
                        Lib.Budget.UpdateBudgets(budgets)
                            .Then(resolve)
                            .Catch(function (error) {
                            if (error instanceof Lib.Budget.PreventConcurrentAccessError) {
                                Lib.CommonDialog.NextAlert.Define("_Budget update error", "_ValidationErrorBudgetLocked");
                            }
                            reject(error);
                        });
                    })
                        .Catch(reject);
                });
                promise.Catch(function (error) {
                    Log.Error("[UpdateBudgets] Unexpected error: " + error);
                });
                return Sys.Helpers.Promise.IsResolvedSync(promise);
            }
            function AsInvoiced(transport) {
                return UpdateBudgets({
                    impactAction: "invoiced",
                    checkRemainingBudgets: false,
                    document: transport
                });
            }
            Budget.AsInvoiced = AsInvoiced;
            function AsInvoiceReversed(transport) {
                return UpdateBudgets({
                    impactAction: "invoiceReversed",
                    checkRemainingBudgets: false,
                    document: transport
                });
            }
            Budget.AsInvoiceReversed = AsInvoiceReversed;
        })(Budget = AP.Budget || (AP.Budget = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
