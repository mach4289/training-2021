/* LIB_DEFINITION{
  "name": "Lib_AP_ArchivedInvoices",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "AP library",
  "require": [
    "Lib_AP_V12.0.425.0",
    "Lib_AP_WorkflowCtrl_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var ArchivedInvoices;
        (function (ArchivedInvoices) {
            function updateLastArchiveInformations(login) {
                Controls.LastArchiveEditionDate__.SetValue(new Date());
                Controls.LastArchiveEditor__.SetValue(login);
            }
            function SetEditingLayoutIfNeeded() {
                if (ProcessInstance.isEditing) {
                    Controls.VendorInformation.SetReadOnly(false);
                    Controls.HeaderDataPanel.SetReadOnly(false);
                    Controls.LineItems__.Amount__.SetReadOnly(false);
                    Controls.LineItems__.Quantity__.SetReadOnly(false);
                    Controls.UpdatePayment.Hide(true);
                    Controls.ApproversList__.HideTableRowDelete(true);
                    Controls.ApproversList__.HideTableRowAdd(true);
                }
                Controls.ReverseInvoice.Hide(!ProcessInstance.isEditing || Controls.InvoiceStatus__.GetValue() === Lib.AP.InvoiceStatus.Reversed);
            }
            ArchivedInvoices.SetEditingLayoutIfNeeded = SetEditingLayoutIfNeeded;
            function OnSave() {
                updateLastArchiveInformations(User.loginId);
                var editor = {
                    login: User.loginId,
                    emailAddress: User.emailAddress,
                    displayName: User.fullName
                };
                if (Data.GetActionName() !== "ReverseInvoice") {
                    Lib.AP.WorkflowCtrl.AddEditor(editor);
                }
                else {
                    Lib.AP.WorkflowCtrl.AddReverseInvoiceInformation(editor);
                }
            }
            ArchivedInvoices.OnSave = OnSave;
            /**
             * Update the status and save the archive
             */
            function ReverseInvoice() {
                Data.SetValue("InvoiceStatus__", Lib.AP.InvoiceStatus.Reversed);
                var vendorPortalRuidEx = Data.GetValue("PortalRuidEx__");
                var vendorInvoiceRuidEx = Data.GetValue("RuidEx");
                Process.CreateProcessInstance("Reverse Customer Invoice", null, { "CustomerInvoiceRuidEx": vendorPortalRuidEx, "VendorInvoiceRuidEx": vendorInvoiceRuidEx });
                Controls.SaveEditing_.Click();
            }
            ArchivedInvoices.ReverseInvoice = ReverseInvoice;
        })(ArchivedInvoices = AP.ArchivedInvoices || (AP.ArchivedInvoices = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
