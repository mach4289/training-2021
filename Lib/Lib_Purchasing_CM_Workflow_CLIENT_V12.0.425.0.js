///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CM_Workflow_Client",
  "libraryType": "LIB",
  "scriptType": "CLIENT",
  "comment": "Purchasing Catalog client library used to manage document workflow",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "Lib_Purchasing_CM_Workflow_V12.0.425.0",
    "Sys/Sys_Helpers",
    "Sys/Sys_OnDemand_Users",
    "Sys/Sys_Helpers_Controls",
    "Sys/Sys_WorkflowController"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            var Workflow;
            (function (Workflow) {
                function InitWorkflowLayout() {
                    Controls.WorkflowTable__.SetWidth("100%");
                    Controls.WorkflowTable__.SetExtendableColumn("WRKFComment__");
                    Controls.WorkflowTable__.SetRowToolsHidden(true);
                    if (!ProcessInstance.isDebugActive) {
                        Controls.WorkflowTable__.WRKFAction__.Hide();
                        Controls.WorkflowTable__.Workflow_index__.Hide();
                    }
                }
                Workflow.InitWorkflowLayout = InitWorkflowLayout;
                Workflow.RevertManager = (function () {
                    var Revert = (function () {
                        function HideWaitScreen(hide) {
                            // async call just after boot
                            setTimeout(function () {
                                Controls.CompanyCode__.Wait(!hide);
                            });
                        }
                        var cmReverter = {
                            creationErrorMessage: null,
                            resultNextAlertVariable: null,
                            table: null,
                            msnEx: null,
                            refreshStatusInterval: null,
                            queryPending: false,
                            ended: false,
                            syncToken: null
                        };
                        function CreateAndWaitForCMReverter(token) {
                            HideWaitScreen(false);
                            Process.CreateProcessInstance("Catalog Management Reverter", {
                                CMWRuidEx__: Data.GetValue("RuidEx"),
                                CMRuidEx__: Variable.GetValueAsString("CMRUIDEX"),
                                CompanyCode__: Data.GetValue("CompanyCode__"),
                                VendorNumber__: Data.GetValue("VendorNumber__")
                            }, {
                                Reverting: true
                            }, {
                                callback: function (data) {
                                    if (data.error) {
                                        cmReverter.creationErrorMessage = data.errorMessage;
                                        token.Use();
                                    }
                                    else {
                                        Log.Info("Canceler created with ruidEx: " + data.ruid);
                                        cmReverter.table = data.ruid.replace(/\.[^\.]+$/, "");
                                        cmReverter.msnEx = data.ruid.replace(/^[^\.]+\./, "");
                                        cmReverter.syncToken = token;
                                        // 2- Wait for the end of GR canceler (polling...)
                                        cmReverter.refreshStatusInterval = setInterval(RefreshCMReverterStatus, 2000);
                                    }
                                }
                            });
                        }
                        function RefreshCMReverterStatus() {
                            if (!cmReverter.queryPending) {
                                cmReverter.queryPending = true;
                                Query.DBQuery(OnRequestResult, cmReverter.table, "State|EXTERNAL_VARIABLE_CommonDialog_NextAlert%FORMATTED", "msnex=" + cmReverter.msnEx, "", 1);
                            }
                        }
                        function OnRequestResult() {
                            cmReverter.queryPending = false;
                            if (cmReverter.ended) {
                                return;
                            }
                            var err = this.GetQueryError();
                            if (err) {
                                Log.Error("Query Error: " + err);
                                return;
                            }
                            var recordsCount = this.GetRecordsCount();
                            if (recordsCount !== 1) {
                                return;
                            }
                            var state = this.GetQueryValue("State", 0);
                            if (state === 70 || state >= 100) {
                                cmReverter.ended = true;
                                clearInterval(cmReverter.refreshStatusInterval);
                                cmReverter.refreshStatusInterval = null;
                                cmReverter.resultNextAlertVariable = this.GetQueryValue("EXTERNAL_VARIABLE_CommonDialog_NextAlert%FORMATTED", 0);
                                cmReverter.syncToken.Use();
                                return;
                            }
                        }
                        function DisplayCMReverterResult() {
                            HideWaitScreen(true);
                            if (cmReverter.resultNextAlertVariable) {
                                Variable.SetValueAsString("CommonDialog_NextAlert", cmReverter.resultNextAlertVariable);
                            }
                            else {
                                var reason = "Unexpected error.";
                                if (cmReverter.creationErrorMessage) {
                                    Log.Error("CM reverter creation error. Details: " + cmReverter.creationErrorMessage);
                                    reason = cmReverter.creationErrorMessage;
                                }
                                Lib.CommonDialog.NextAlert.Define("_CM revert error", "_CM revert error message", {
                                    isError: true,
                                    behaviorName: "CMRevertFailure"
                                }, reason);
                            }
                            Lib.CommonDialog.NextAlert.Show({
                                "CMRevertSuccess": {
                                    OnOK: function () {
                                        ProcessInstance.Quit("quit");
                                    }
                                },
                                "CMRevertFailure": {
                                    OnOK: function () {
                                        ProcessInstance.Quit("quit");
                                    }
                                }
                            });
                        }
                        var syncCancel = Sys.Helpers.Synchronizer.Create(function () {
                            // defer call to avoid stacking popup
                            setTimeout(DisplayCMReverterResult);
                        }, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
                        syncCancel.userData = {
                            dialogTitle: "_Form awaiting CM cancel",
                            dialogMessage: "_Form awaiting CM cancel message"
                        };
                        syncCancel.Register(CreateAndWaitForCMReverter);
                        syncCancel.Start();
                    });
                    var revertMessage = "_This will revert all changes made to the catalog.";
                    Popup.Confirm(revertMessage, false, Revert, null, "_Revert changes");
                });
                function GetLastApprovedUpdateRequest() {
                    var companyCode = Data.GetValue("CompanyCode__") ? Data.GetValue("CompanyCode__") : "";
                    var vendorNumber = Data.GetValue("VendorNumber__") ? Data.GetValue("VendorNumber__") : "";
                    var queryOptions = {
                        table: "CDNAME#Catalog management workflow",
                        filter: "(&(Status__=Approved)(CompanyCode__=" + companyCode + ")(VendorNumber__=" + vendorNumber + "))",
                        attributes: ["RUIDEX", "CompletionDateTime", "Status__"],
                        sortOrder: "CompletionDateTime DESC",
                        maxRecords: 1
                    };
                    return Sys.GenericAPI.PromisedQuery(queryOptions)
                        .Then(function (results) {
                        return Sys.Helpers.Promise.Resolve(results.length > 0 ? results[0]["RUIDEX"] : null);
                    });
                }
                Workflow.GetLastApprovedUpdateRequest = GetLastApprovedUpdateRequest;
                var clientParameters = {
                    mappingTable: {
                        OnRefreshRow: function (index) {
                            var table = Controls[this.mappingTable.tableName];
                            var row = table.GetRow(index);
                            // Highlight the row and add a cursor in the first column if the process is not in a final state.
                            if (row.WRKFAction__.GetValue()) //test if the line is not empty
                             {
                                row.WRKFRole__.SetImageURL(this.actions[row.WRKFAction__.GetValue()].image, false);
                                Lib.P2P.HighlightCurrentWorkflowStep(Workflow.controller, this.mappingTable.tableName, index);
                            }
                            row.WRKFUserName__.SetImageURL(Lib.P2P.GetP2PUserImage(row.WRKFIsGroup__.GetValue()), true);
                        }
                    },
                    callbacks: {
                        OnError: function (msg) {
                            Log.Info(msg);
                        },
                        OnBuilding: function () {
                            // Called before the call to the OnBuild functions. Here you can:
                            // - Disable buttons
                            // - Display a waiting icon
                            // - etc.
                            ProcessInstance.SetSilentChange(true);
                        },
                        OnBuilt: function () {
                            // Called after the call to the OnBuild functions.
                            // Here you can do the opposite of the previous function.
                            ProcessInstance.SetSilentChange(false);
                        }
                    }
                };
                Sys.Helpers.Extend(true, Lib.Purchasing.CM.Workflow.parameters, clientParameters);
            })(Workflow = CM.Workflow || (CM.Workflow = {}));
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
