///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_CM_Client",
  "libraryType": "LIB",
  "scriptType": "Client",
  "comment": "Catalog Management library",
  "require": [
    "Lib_Purchasing_CM_V12.0.425.0",
    "Lib_Purchasing_CM_Workflow_V12.0.425.0"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var CM;
        (function (CM) {
            var isInternalUpdateRequest = Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest();
            var typeToControls = {
                "Added": {
                    label: "_PreviewAddedPane {0}",
                    table: "AddedItems__",
                    panel: "PreviewAddedPane"
                },
                "Modified": {
                    label: "_PreviewModifiedPane {0}",
                    table: "ModifiedItems__",
                    panel: "PreviewModifiedPane"
                },
                "Deleted": {
                    label: "_PreviewDeletedPane {0}",
                    table: "DeletedItems__",
                    panel: "PreviewDeletedPane"
                },
                "Nothing": {
                    label: "_PreviewDeletedPane {0}",
                    table: "DeletedItems__",
                    panel: "PreviewDeletedPane"
                }
            };
            var groupedExtractedData;
            function FillGroupedExternalData(actionToGroup) {
                groupedExtractedData = {
                    "Added": [],
                    "Modified": [],
                    "Deleted": []
                };
                var extractedData = Lib.Purchasing.CM.GetExternalData();
                var lines = extractedData.Lines || {};
                for (var key in lines) {
                    if (Object.prototype.hasOwnProperty.call(lines, key)) {
                        var action = lines[key][actionToGroup];
                        if (action === "Nothing") {
                            action = "Deleted";
                        }
                        groupedExtractedData[action].push(lines[key]);
                    }
                }
            }
            function FillAndDisplayPreview(previewType) {
                var data = groupedExtractedData[previewType] || [];
                var tableControl = Controls[typeToControls[previewType].table];
                tableControl.SetItemCount(0);
                tableControl.SetWidth("100%");
                tableControl.SetExtendableColumn("Description__");
                if (!isInternalUpdateRequest) {
                    // The preview tables in the CM don't have CompanyCode__ or VendorName__ columns
                    if (tableControl.CompanyCode__) {
                        tableControl.CompanyCode__.Hide(true);
                    }
                    if (tableControl.VendorNumber__) {
                        tableControl.VendorNumber__.Hide(true);
                    }
                }
                data.forEach(function (r) {
                    var rawData = r.ExtractedData;
                    var newItem = tableControl.AddItem();
                    newItem.SetValue("Number__", rawData.SupplierPartID);
                    newItem.SetValue("Description__", rawData.Name);
                    newItem.SetValue("ManufacturerName__", rawData.ManufacturerName);
                    newItem.SetValue("UnitPrice__", rawData.UnitPrice);
                    newItem.SetValue("Currency__", rawData.Currency);
                    newItem.SetValue("UnitOfMeasure__", rawData.UOM);
                    newItem.SetValue("UNSPSC__", rawData.UNSPSC);
                    if (isInternalUpdateRequest) {
                        newItem.SetValue("CompanyCode__", rawData.CompanyCode);
                        newItem.SetValue("VendorNumber__", rawData.VendorNumber);
                    }
                });
                Controls[typeToControls[previewType].panel].Hide(data.length == 0);
                Controls[typeToControls[previewType].panel].SetLabel(typeToControls[previewType].label, data.length);
                return data.length;
            }
            function DisplayPreviews(display, actionToDisplay) {
                if (actionToDisplay === void 0) { actionToDisplay = "ActionToDo"; }
                var linesDisplayed = 0;
                if (display) {
                    FillGroupedExternalData(actionToDisplay);
                    linesDisplayed += FillAndDisplayPreview("Added");
                    linesDisplayed += FillAndDisplayPreview("Modified");
                    linesDisplayed += FillAndDisplayPreview("Deleted");
                    return linesDisplayed;
                }
                Controls.PreviewAddedPane.Hide(true);
                Controls.PreviewModifiedPane.Hide(true);
                Controls.PreviewDeletedPane.Hide(true);
                return 0;
            }
            CM.DisplayPreviews = DisplayPreviews;
        })(CM = Purchasing.CM || (Purchasing.CM = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
