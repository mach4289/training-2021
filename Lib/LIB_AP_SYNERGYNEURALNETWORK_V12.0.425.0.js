///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_AP_SynergyNeuralNetwork",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "require": [
    "Lib_V12.0.425.0",
    "Lib_CrossCultural_V12.0.425.0",
    "Lib_CrossCultural_Date_V12.0.425.0",
    "Sys/Sys_Helpers_Date"
  ]
}*/
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var SynergyNeuralNetwork;
        (function (SynergyNeuralNetwork) {
            var apiResult = {};
            var thresholdConfidence = 0.1;
            var bestExtractedNetAmount;
            var currency;
            var options;
            function ExecuteAPI() {
                var jsonString;
                try {
                    jsonString = Process.GetSynergyExtraction("SynergyForInvoiceHeader");
                    apiResult = JSON.parse(jsonString);
                    return true;
                }
                catch (e) {
                    Log.Warn("Call to SynergyNeuralNetwork failed with model the SynergyForInvoiceHeader. Process.GetSynergyExtraction() return is: " + jsonString);
                    apiResult = {};
                    var nPages = Document.GetPageCount();
                    if (nPages > 10) {
                        Log.Warn("Too many pages to call the model SynergyForInvoiceHeader");
                    }
                }
                return false;
            }
            function GetSynergyAreas(candidate) {
                var boxes = [];
                var boxesToHighlight = candidate.box_per_page;
                if (boxesToHighlight) {
                    for (var _i = 0, _a = Object.keys(boxesToHighlight); _i < _a.length; _i++) {
                        var boxPerPage = _a[_i];
                        var box = boxesToHighlight[boxPerPage];
                        var boxArea = Document.GetArea(parseInt(boxPerPage, 10) - 1, box.left, box.top, box.right - box.left, box.bottom - box.top);
                        boxes.push(boxArea);
                    }
                }
                return boxes;
            }
            function SetSynergyValue(bestCandidate, areas, fieldToUpdate) {
                if (areas && areas.length > 0) {
                    Data.SetValue(fieldToUpdate, areas[0], bestCandidate.parsed_value);
                }
                else {
                    Data.SetValue(fieldToUpdate, bestCandidate.parsed_value);
                }
                Data.SetComputedValueSource(fieldToUpdate, "SynergyForInvoiceHeader");
                Data.SetComputed(fieldToUpdate, false);
            }
            //Story AP-FT-018357 - Stop to display the other candidates
            /*
            function HighlightSynergyAreas(areas: Area[], fieldToUpdate: string, idx: number): void
            {
                const highlightColorBorder = 0x000000;
                const highlightColorBackground = 0xFFCC00;
                for (const boxArea of areas)
                {
                    boxArea.Highlight(true, highlightColorBackground, highlightColorBorder, fieldToUpdate);
                }
            }*/
            var PostProcessing;
            (function (PostProcessing) {
                /**
                 * Function application order runs top-to-bottom.
                 */
                var pipe = function () {
                    var fns = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        fns[_i] = arguments[_i];
                    }
                    return function (x) { return fns.reduce(function (y, f) { return f(y); }, x); };
                };
                function getCandidateBox(c) {
                    var boxesToHighlight = c.box_per_page;
                    if (boxesToHighlight) {
                        for (var _i = 0, _a = Object.keys(boxesToHighlight); _i < _a.length; _i++) {
                            var boxPerPage = _a[_i];
                            return {
                                left: boxesToHighlight[boxPerPage].left,
                                right: boxesToHighlight[boxPerPage].right,
                                top: boxesToHighlight[boxPerPage].top,
                                bottom: boxesToHighlight[boxPerPage].bottom,
                                page: boxPerPage
                            };
                        }
                    }
                    return null;
                }
                function IsBoxIncludedIn(a, b) {
                    // Tolerances in pixels
                    var heightTolerance = 5;
                    var widthTolerance = 5;
                    var boxA = getCandidateBox(a);
                    var boxB = getCandidateBox(b);
                    return boxA.page === boxB.page
                        && boxA.top <= (boxB.top + heightTolerance)
                        && boxA.bottom >= (boxB.bottom - heightTolerance)
                        && boxA.left >= (boxB.left - widthTolerance)
                        && boxA.right <= (boxB.right + widthTolerance);
                }
                function IsFontSizeBigger(a, b) {
                    var boxA = getCandidateBox(a);
                    var boxB = getCandidateBox(b);
                    return (boxA.bottom - boxA.top) < (boxB.bottom - boxB.top);
                }
                /**
                 * Comparison function called on the sort functions
                 */
                function CompareBoxCandidates(a, b) {
                    var delta = Math.abs(b.mean_confidence - a.mean_confidence);
                    if (delta < 0.15) {
                        // Small drop in confidence
                        if (IsBoxIncludedIn(a, b)) {
                            // a is included in b
                            // b is better
                            return 1;
                        }
                        else if (IsBoxIncludedIn(b, a)) {
                            // b is included in a
                            // a is better
                            return -1;
                        }
                    }
                    // best confidence is better
                    return b.mean_confidence > a.mean_confidence ? 1 : -1;
                }
                function CompareFontSize(a, b) {
                    var delta = Math.abs(b.mean_confidence - a.mean_confidence);
                    if (delta < 0.15) {
                        if (IsFontSizeBigger(a, b)) {
                            return 1;
                        }
                        return -1;
                    }
                    return b.mean_confidence > a.mean_confidence ? 1 : -1;
                }
                function CompareCandidatesValue(a, b) {
                    var delta = Math.abs(b.mean_confidence - a.mean_confidence);
                    if (delta < 0.25 && a.parsed_value && b.parsed_value) {
                        var aString = a.parsed_value.toString();
                        var bString = b.parsed_value.toString();
                        if (bString !== aString) {
                            if (bString.indexOf(aString) > -1) {
                                return 1;
                            }
                            else if (aString.indexOf(bString) > -1) {
                                return -1;
                            }
                        }
                    }
                    return 0;
                }
                /**
                 * Post-processing functions applied on each field to update
                 */
                // Sort by box inclusion
                function TryGetLargerBox(candidates) {
                    return candidates.sort(CompareBoxCandidates);
                }
                // handle currency without decimal
                function TryNoDecimalCurrency(candidates) {
                    // check if currency has decimal
                    var hasDecimal = true;
                    if (currency && options.CurrencyHasDecimal && typeof options.CurrencyHasDecimal === "function") {
                        hasDecimal = options.CurrencyHasDecimal(currency);
                    }
                    if (!hasDecimal) {
                        for (var i = 0; i < candidates.length; i++) {
                            // clean raw value from known amount separators
                            var rawValue = candidates[i].raw_text;
                            var rawValues = rawValue.match(/-*[0-9]{1,3}([., "']{0,1}[0-9]{3})*/g);
                            // keep the bigger (as absolute) number
                            var parsedValue = rawValues[0] ? parseInt(rawValues[0].replace(/[., "']/g, ""), 10) : 0;
                            for (var j = 0; j < rawValues.length; j++) {
                                var value = rawValues[j] ? parseInt(rawValues[j].replace(/[., "']/g, ""), 10) : 0;
                                if (Math.abs(value) > Math.abs(parsedValue)) {
                                    parsedValue = value;
                                }
                            }
                            candidates[i].parsed_value = parsedValue;
                        }
                    }
                    return candidates;
                }
                // Sort by value inclusion
                function TryGetLargerFloat(candidates) {
                    if (candidates.length > 1) {
                        var candidateBiggerMeanConfidence = candidates[0];
                        candidates.sort(CompareCandidatesValue);
                        var bestIdxCandidate = 0;
                        var idx = 1;
                        while (idx < candidates.length && !IsBoxIncludedIn(candidateBiggerMeanConfidence, candidates[bestIdxCandidate])) {
                            if (candidates[bestIdxCandidate].parsed_value
                                && candidates[idx].parsed_value
                                && candidates[bestIdxCandidate].parsed_value === candidates[idx].parsed_value
                                && IsBoxIncludedIn(candidateBiggerMeanConfidence, candidates[idx])) {
                                bestIdxCandidate = idx;
                            }
                            idx++;
                        }
                        if (bestIdxCandidate > 0) {
                            var temp = candidates[bestIdxCandidate];
                            candidates[bestIdxCandidate] = candidates[0];
                            candidates[0] = temp;
                        }
                    }
                    return candidates;
                }
                // Return the extracted amount when the amount candidates are smaller than the extracted amount and the confidence is small too
                function TryBiggerThanNetAmount(candidates) {
                    if (bestExtractedNetAmount && bestExtractedNetAmount.parsed_value) {
                        var extractedNetAmount = bestExtractedNetAmount.parsed_value;
                        var i = candidates.length - 1;
                        while (i >= 0 && candidates[i].parsed_value <= extractedNetAmount && candidates[i].mean_confidence < 0.33) {
                            i--;
                        }
                        if (i < 0) {
                            candidates = [bestExtractedNetAmount];
                        }
                    }
                    return candidates;
                }
                // Sort by font size
                function TrySortByFontSize(candidates) {
                    return candidates.sort(CompareFontSize);
                }
                // Remove candidates with a font size too large
                function TryRemoveResultsWithBigFontSize(candidates) {
                    if (candidates.length > 0) {
                        var averageFontSize = 0;
                        for (var _i = 0, candidates_1 = candidates; _i < candidates_1.length; _i++) {
                            var candidate = candidates_1[_i];
                            var boxA = getCandidateBox(candidate);
                            averageFontSize = averageFontSize + boxA.bottom - boxA.top;
                        }
                        averageFontSize = averageFontSize / candidates.length;
                        for (var idx = candidates.length - 1; idx >= 0; idx--) {
                            var boxA = getCandidateBox(candidates[idx]);
                            if ((boxA.bottom - boxA.top) > 2 * averageFontSize) {
                                candidates.splice(idx, 1);
                            }
                        }
                    }
                    return candidates.sort(CompareFontSize);
                }
                // Modify date according to the culture and remove dates too far in the past and the future
                function TryFormatDate(candidates) {
                    var i = 0;
                    var formattedDate;
                    var isVendorCountryUS = Data.GetValue("VendorCountry__") === "US";
                    for (i = 0; i < candidates.length; i++) {
                        formattedDate = Lib.CrossCultural.Date.ConvertToDateOrFormattedString(candidates[i].raw_text, "yyyy-MM-dd", isVendorCountryUS, true);
                        if (formattedDate.length > 0) {
                            var tenYearsAgo = new Date();
                            tenYearsAgo.setFullYear(tenYearsAgo.getFullYear() - 10);
                            if (Sys.Helpers.Date.CompareDate(Sys.Helpers.Date.ISOSTringToDate(formattedDate[0]), new Date()) > 0
                                || Sys.Helpers.Date.CompareDate(Sys.Helpers.Date.ISOSTringToDate(formattedDate[0]), tenYearsAgo) < 0) {
                                candidates[i].parsed_value = "";
                            }
                            else {
                                candidates[i].parsed_value = formattedDate[0];
                            }
                        }
                        else {
                            candidates[i].parsed_value = "";
                        }
                    }
                    return candidates;
                }
                // Remove N° at the begining of the invoice number
                function TryTrimNo(candidates) {
                    var i;
                    for (i = 0; i < candidates.length; i++) {
                        var parsedValue = candidates[i].parsed_value.toString();
                        candidates[i].parsed_value = parsedValue.replace(/^n°/i, "");
                    }
                    return candidates;
                }
                /**
                * Global post-processing functions
                */
                function TryGetCandidateAndHighlight(fieldToUpdate, candidates) {
                    var bestCandidateIdx = -1;
                    for (var idx = 0; idx < candidates.length; idx++) {
                        var candidate = candidates[idx];
                        if (bestCandidateIdx === -1 && candidate.parsed_value) {
                            bestCandidateIdx = idx;
                        }
                        //Story AP-FT-018357 - Stop to display the other candidates
                        /*else
                        {
                            HighlightSynergyAreas(GetSynergyAreas(candidate), fieldToUpdate, idx);
                        }*/
                    }
                    return bestCandidateIdx >= 0 ? candidates[bestCandidateIdx] : null;
                }
                function TryGetBestCandidate(fieldToUpdate, candidates) {
                    var processedCandidates = candidates;
                    if (fieldToUpdate !== "InvoiceCurrency__") {
                        var idx = processedCandidates.length - 1;
                        while (idx >= 0 && processedCandidates[idx].mean_confidence < thresholdConfidence) {
                            processedCandidates.pop();
                            idx--;
                        }
                    }
                    switch (fieldToUpdate) {
                        case "InvoiceAmount__":
                            processedCandidates = pipe(TryNoDecimalCurrency, TryBiggerThanNetAmount, TryGetLargerFloat)(processedCandidates);
                            break;
                        case "ExtractedNetAmount__":
                            processedCandidates = pipe(TryNoDecimalCurrency, TryGetLargerFloat)(processedCandidates);
                            break;
                        case "InvoiceNumber__":
                            processedCandidates = pipe(TryGetLargerBox, TryTrimNo)(processedCandidates);
                            break;
                        case "InvoiceDate__":
                            processedCandidates = pipe(TryFormatDate)(processedCandidates);
                            break;
                        case "InvoiceCurrency__":
                            processedCandidates = pipe(TryRemoveResultsWithBigFontSize, TrySortByFontSize)(processedCandidates);
                            break;
                        default:
                            break;
                    }
                    return TryGetCandidateAndHighlight(fieldToUpdate, processedCandidates);
                }
                PostProcessing.TryGetBestCandidate = TryGetBestCandidate;
            })(PostProcessing || (PostProcessing = {}));
            function SetValueAndArea(field, replaceCurrentValue) {
                var fieldToUpdate = field + "__";
                if (replaceCurrentValue || Data.IsNullOrEmpty(fieldToUpdate)) {
                    var candidates = apiResult[field];
                    var bestCandidate = void 0;
                    if (candidates) {
                        bestCandidate = PostProcessing.TryGetBestCandidate(fieldToUpdate, candidates);
                        if (bestCandidate) {
                            var boxAreas = GetSynergyAreas(bestCandidate);
                            SetSynergyValue(bestCandidate, boxAreas, fieldToUpdate);
                        }
                    }
                    if (field === "InvoiceCurrency") {
                        if (bestCandidate) {
                            currency = bestCandidate.parsed_value;
                        }
                        else if (options.GetDefaultCurrency && typeof options.GetDefaultCurrency === "function") {
                            currency = options.GetDefaultCurrency();
                        }
                    }
                    if (field === "ExtractedNetAmount") {
                        bestExtractedNetAmount = bestCandidate;
                    }
                }
            }
            function SetRecognitionMethod() {
                var recognitionMethod = Data.GetValue("RecognitionMethod");
                if (recognitionMethod === "" || recognitionMethod === "FTR") {
                    Data.SetValue("RecognitionMethod", "SynergyNeuralNetwork");
                }
            }
            /**
            * FillHeaderFields
            * @param options Object
            * @param options.defaultCurrency string|function
            */
            function FillHeaderFields(opts) {
                options = opts || {};
                if (ExecuteAPI()) {
                    SetValueAndArea("InvoiceCurrency", false);
                    SetValueAndArea("ExtractedNetAmount", false);
                    SetValueAndArea("InvoiceAmount", false);
                    SetValueAndArea("InvoiceDate", false);
                    SetValueAndArea("InvoiceNumber", false);
                    SetRecognitionMethod();
                    return true;
                }
                return false;
            }
            SynergyNeuralNetwork.FillHeaderFields = FillHeaderFields;
        })(SynergyNeuralNetwork = AP.SynergyNeuralNetwork || (AP.SynergyNeuralNetwork = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
