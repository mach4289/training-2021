///#GLOBALS Sys
/* LIB_DEFINITION{
  "name": "Lib_AP_SAP_PurchaseOrder_Common",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "library: SAP FI post routines.",
  "require": [
    "Lib_Parameters_P2P_V12.0.425.0",
    "Sys/Sys_Helpers_String_SAP",
    "Sys/Sys_Helpers_Promise"
  ]
}*/
// eslint-disable-next-line @typescript-eslint/no-unused-vars
var Lib;
(function (Lib) {
    var AP;
    (function (AP) {
        var SAP;
        (function (SAP) {
            var PurchaseOrder;
            (function (PurchaseOrder) {
                var PlannedDeliveryCost = /** @class */ (function () {
                    /**
                     *
                     */
                    function PlannedDeliveryCost(sapDeliveryCost) {
                        // set default values
                        this.ConditionType = "";
                        this.ItemNumber = "";
                        this.Currency = "";
                        this.AccountNumber = "";
                        this.DeliveredAmount = 0;
                        this.DeliveredQuantity = 0;
                        this.InvoicedAmount = 0;
                        this.InvoicedQuantity = 0;
                        if (sapDeliveryCost) {
                            this.ConditionType = sapDeliveryCost.KSCHL;
                            this.ItemNumber = sapDeliveryCost.EBELP;
                            this.Currency = sapDeliveryCost.HSWAE;
                            this.AccountNumber = sapDeliveryCost.LIFNR;
                            if (sapDeliveryCost.SHKZG === "H") {
                                this.DeliveredAmount = parseFloat(sapDeliveryCost.DMBTR);
                                this.DeliveredQuantity = parseFloat(sapDeliveryCost.MENGE);
                            }
                            else {
                                this.InvoicedAmount = parseFloat(sapDeliveryCost.DMBTR);
                                this.InvoicedQuantity = parseFloat(sapDeliveryCost.MENGE);
                            }
                        }
                    }
                    PlannedDeliveryCost.clone = function (other) {
                        var result = new PlannedDeliveryCost();
                        result.ConditionType = other.ConditionType;
                        result.ItemNumber = other.ItemNumber;
                        result.Currency = other.Currency;
                        result.AccountNumber = other.AccountNumber;
                        result.DeliveredAmount = other.DeliveredAmount;
                        result.DeliveredQuantity = other.DeliveredQuantity;
                        result.InvoicedAmount = other.InvoicedAmount;
                        result.InvoicedQuantity = other.InvoicedQuantity;
                        return result;
                    };
                    return PlannedDeliveryCost;
                }());
                PurchaseOrder.PlannedDeliveryCost = PlannedDeliveryCost;
                /**
                * Class defining a PO item
                */
                var POItemData = /** @class */ (function () {
                    function POItemData(poNumber, poItem) {
                        this.Po_Number = poNumber;
                        this.Po_Item = poItem;
                        this.Quantity = 0;
                        this.Ret_Item = false;
                        this.Gr_Basediv = false;
                        this.Gr_Ind = false;
                        this.Conv_Den1 = 0;
                        this.Conv_Num1 = 0;
                        this.Item_Cat = "";
                        this.Est_Price = false;
                        this.Gr_Non_Val = false;
                        this.Net_Value = 0;
                        this.Unit = "";
                        this.Orderpr_Un = "";
                        this.Vendor = "";
                        this.Diff_Inv = "";
                        this.Short_Text = "";
                        this.Ref_Doc = "";
                        this.Iv_Qty = 0;
                        this.Iv_Qty_Po = 0;
                        this.Deliv_Qty = 0;
                        this.Val_Iv_For = 0;
                        this.Distribution = "";
                        this.ValidityPeriod_Start = "";
                        this.ValidityPeriod_End = "";
                    }
                    POItemData.prototype.IsServiceItem = function () {
                        return this.Item_Cat === "9";
                    };
                    POItemData.prototype.IsLimitItem = function () {
                        return this.Item_Cat === "1";
                    };
                    return POItemData;
                }());
                PurchaseOrder.POItemData = POItemData;
                //#region Planned Delivery Cost
                /**
                 * Cache of the PDC group by SAP query filter
                 * The cache value is an ESKMap<Array<ISAPDeliveryCost>> which correspond to the
                 * PDC group by Item Number
                 */
                PurchaseOrder._deliveryCostsCached = {};
                PurchaseOrder.cacheEKBE = {};
                /**
                 * Group an array of objects using a key
                 * @param {array} xs an array of objects
                 * @param {string} key a key
                 * @return an object associating a key to an array
                 */
                function GroupBy(xs, key) {
                    return xs.reduce(function (rv, x) {
                        (rv[x[key]] = rv[x[key]] || []).push(x);
                        return rv;
                    }, {});
                }
                PurchaseOrder.GroupBy = GroupBy;
                /**
                 * Get the array-formated pricing conditions from the configuration
                 * @return {string[]} an array of pricing condition codes (ex: ["FRA1", "FRB1"])
                 */
                function GetPlannedPricingConditions() {
                    var conditions = [];
                    var plannedPricingConditions = Sys.Parameters.GetInstance("AP").GetParameter("PlannedPricingConditions");
                    if (plannedPricingConditions) {
                        conditions = plannedPricingConditions.replace(/ /g, "").split("\n");
                    }
                    return conditions;
                }
                PurchaseOrder.GetPlannedPricingConditions = GetPlannedPricingConditions;
                /**
                * Create the SAPQuery filter depending on the pricing conditions
                * @param {string[]} conditions array of pricing condition codes
                * @return {string} a SAP filter
                */
                function CreatePricingConditionsFilter(conditions) {
                    var filter = "";
                    if (conditions.length) {
                        conditions.forEach(function (condition, idx) {
                            if (idx > 0) {
                                filter += " " + Sys.Helpers.SAP.GetQuerySeparator() + " OR ";
                            }
                            filter += "KSCHL = '" + condition + "'";
                        });
                    }
                    return filter;
                }
                PurchaseOrder.CreatePricingConditionsFilter = CreatePricingConditionsFilter;
                /**
                 * Get the condition descriptions
                 * @return {Promise<IConditionTypeDescriptions[]>} a promise of condition descriptions
                 */
                function GetConditionTypeDescription() {
                    var sapConf = Variable.GetValueAsString("SAPConfiguration");
                    var saptable = "T685T";
                    var pcFilter = PurchaseOrder.CreatePricingConditionsFilter(PurchaseOrder.GetPlannedPricingConditions());
                    var filter = "(" + pcFilter + ") AND SPRAS = '%SAPCONNECTIONLANGUAGE%'";
                    var sapFieldToEskerField = {
                        KSCHL: "KSCHL",
                        VTEXT: "VTEXT"
                    };
                    if (PurchaseOrder._conditionTypeDescriptionsCache && Object.keys(PurchaseOrder._conditionTypeDescriptionsCache).length) {
                        // If a SAP call has already been made, return the cache description
                        Log.Info("GetConditionTypeDescription cached");
                        return Sys.Helpers.Promise.Resolve(PurchaseOrder._conditionTypeDescriptionsCache);
                    }
                    return Sys.Helpers.Promise.Create(function (resolve, reject) {
                        // Cache is empty, call SAP to retrieve descriptions
                        Sys.GenericAPI.SAPQuery(sapConf, saptable, filter, Object.keys(sapFieldToEskerField), SAPQueryCallback, 200);
                        function SAPQueryCallback(result, error) {
                            if (error) {
                                Log.Error("SAP Query", error);
                                reject(error);
                                return;
                            }
                            PurchaseOrder._conditionTypeDescriptionsCache = result;
                            resolve(result);
                        }
                    });
                }
                PurchaseOrder.GetConditionTypeDescription = GetConditionTypeDescription;
                /**
                 * Query the EKBZ table to get the delivery costs for the items of the PO and add them to it
                 * @export
                 * @param {string} poNumber
                 * @param {PODetails} po
                 * @return {Promise<DeliveryCosts>}
                 */
                function GetDeliveryCostsForPO(poNumber, po) {
                    function getFilterForDeliveryCost(filterPoNumber, filterPoItems) {
                        var normalizedPoNumber = Sys.Helpers.String.SAP.NormalizeID(filterPoNumber, 10);
                        var itemNumbers = [];
                        // tslint:disable-next-line: forin
                        for (var key in filterPoItems) {
                            if (Object.prototype.hasOwnProperty.call(filterPoItems, key)) {
                                var poItem = filterPoItems[key];
                                if (!poItem.Cond_Type) {
                                    itemNumbers.push(Sys.Helpers.String.SAP.NormalizeID(poItem.Po_Item, 5));
                                }
                            }
                        }
                        var filterstr = ["EBELN = '" + normalizedPoNumber + "\"'"];
                        var separator = Sys.Helpers.SAP.GetQuerySeparator();
                        var filterItemNumbers = itemNumbers.join("' " + separator + " OR EBELP = '");
                        filterstr.push(" AND (EBELP = '" + filterItemNumbers + "')");
                        var pricingConditionsFilter = PurchaseOrder.CreatePricingConditionsFilter(PurchaseOrder.GetPlannedPricingConditions());
                        filterstr.push(" AND (" + pricingConditionsFilter + ")");
                        return filterstr.join(separator);
                    }
                    function getPOItem(items, itemNumber) {
                        if (items[itemNumber]) {
                            return items[itemNumber];
                        }
                        for (var _i = 0, _a = Object.keys(items); _i < _a.length; _i++) {
                            var key = _a[_i];
                            var poItem = items[key];
                            if (poItem.Po_Item === itemNumber) {
                                return poItem;
                            }
                        }
                        return null;
                    }
                    function addResultToPO(resultPO, pdcs) {
                        for (var _i = 0, pdcs_1 = pdcs; _i < pdcs_1.length; _i++) {
                            var pdc = pdcs_1[_i];
                            var poItem = getPOItem(resultPO.PO_ITEMS, pdc.ItemNumber);
                            var pdcItem = CreateDeliveryCostItem(poItem, pdc, PurchaseOrder._conditionTypeDescriptionsCache);
                            resultPO.PlannedDeliveryCosts.push(pdcItem);
                            resultPO.PlannedDeliveryCostsAmount = pdcItem.ExpectedAmount + (resultPO.PlannedDeliveryCostsAmount || 0);
                        }
                    }
                    function GetExternalCurrencyFactor(currency) {
                        return Sys.Helpers.Promise.Create(function (resolve) {
                            if (externalCurrencyFactors[currency]) {
                                // Factor exists in cache
                                resolve(externalCurrencyFactors[currency]);
                                return;
                            }
                            var customExternalFactors = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAPCurrenciesExternalFactors");
                            if (customExternalFactors && customExternalFactors[currency]) {
                                // Factor read by user-exit
                                externalCurrencyFactors[currency] = customExternalFactors[currency];
                                resolve(externalCurrencyFactors[currency]);
                                return;
                            }
                            var currencyFactorfilter = "CURRKEY = '" + currency + "'";
                            Sys.GenericAPI.SAPQuery(Variable.GetValueAsString("SAPConfiguration"), "TCURX", currencyFactorfilter, ["CURRDEC"], function (result, error) {
                                if (!error && result && result.length > 0) {
                                    var nbDecimals = Sys.Helpers.String.SAP.Trim(result[0].CURRDEC);
                                    externalCurrencyFactors[currency] = Sys.Helpers.String.SAP.ConvertDecimalToExternalFactor(nbDecimals);
                                }
                                else {
                                    if (error) {
                                        Log.Warn("Could not get external currency factor from SAP: (" + error + "), 1 will be used by default for currency '" + currency + "'");
                                    }
                                    externalCurrencyFactors[currency] = 1;
                                }
                                resolve(externalCurrencyFactors[currency]);
                            });
                        });
                    }
                    function fetchAndApplyExternalCurrencyFactor(deliveryCosts) {
                        if (deliveryCosts.length > 0) {
                            return GetExternalCurrencyFactor(deliveryCosts[0].Currency).then(function (factor) {
                                deliveryCosts.forEach(function (pdc) {
                                    pdc.DeliveredAmount *= factor;
                                    pdc.InvoicedAmount *= factor;
                                });
                            });
                        }
                        return Sys.Helpers.Promise.Resolve();
                    }
                    var poItems = po.PO_ITEMS;
                    var sapConf = Variable.GetValueAsString("SAPConfiguration");
                    var saptable = "EKBZ";
                    var filter = getFilterForDeliveryCost(poNumber, poItems);
                    var externalCurrencyFactors = {};
                    var sapFieldToEskerField = {
                        EBELN: "EBELN", EBELP: "EBELP",
                        STUNR: "STUNR", KSCHL: "KSCHL",
                        DMBTR: "DMBTR", WAERS: "WAERS",
                        MENGE: "MENGE", BPMNG: "BPMNG",
                        LIFNR: "LIFNR", WRBTR: "WRBTR",
                        SHKZG: "SHKZG", HSWAE: "HSWAE"
                    };
                    return Sys.Helpers.Promise.Create(function (resolve, reject) {
                        if (filter in PurchaseOrder._deliveryCostsCached) {
                            addResultToPO(po, PurchaseOrder._deliveryCostsCached[filter]);
                            resolve();
                            return;
                        }
                        Sys.GenericAPI.SAPQuery(sapConf, saptable, filter, Object.keys(sapFieldToEskerField), SAPQueryCallback, 200);
                        function SAPQueryCallback(results, error) {
                            if (error) {
                                Log.Error("SAP Query", error);
                                reject(error);
                            }
                            else {
                                var plannedDeliveryCosts_1 = GroupDeliveryCosts(results);
                                fetchAndApplyExternalCurrencyFactor(plannedDeliveryCosts_1).then(function () {
                                    // Cache
                                    PurchaseOrder._deliveryCostsCached[filter] = plannedDeliveryCosts_1;
                                    addResultToPO(po, plannedDeliveryCosts_1);
                                    resolve();
                                });
                            }
                        }
                    });
                }
                PurchaseOrder.GetDeliveryCostsForPO = GetDeliveryCostsForPO;
                /**
                * Group delivery cost by item number and merge duplicate delivery cost having the same condition type
                * @param {ISAPDeliveryCost[]} results The list of delivery costs to group and filter
                */
                function GroupDeliveryCosts(deliveryCosts) {
                    var finalDeliveryCosts = [];
                    var groupedDeliveryCosts = GroupBy(deliveryCosts, "EBELP"); // group by item number (convert to a Javascript object)
                    // tslint:disable-next-line: forin
                    for (var itemNumber in groupedDeliveryCosts) {
                        if (Object.prototype.hasOwnProperty.call(groupedDeliveryCosts, itemNumber)) {
                            var pdcByConditionType = GroupBy(groupedDeliveryCosts[itemNumber], "KSCHL"); // then group by condition type (convert to a Javascript object)
                            // tslint:disable-next-line: forin
                            for (var condType in pdcByConditionType) {
                                if (Object.prototype.hasOwnProperty.call(pdcByConditionType, condType)) {
                                    finalDeliveryCosts.push(pdcByConditionType[condType].reduce(mergeDeliveryCost, null));
                                }
                            }
                        }
                    }
                    return finalDeliveryCosts;
                }
                PurchaseOrder.GroupDeliveryCosts = GroupDeliveryCosts;
                function mergeDeliveryCost(pdc, sapDeliveryCost) {
                    var otherPdc = new PlannedDeliveryCost(sapDeliveryCost);
                    if (pdc === null) {
                        return otherPdc;
                    }
                    var result = PlannedDeliveryCost.clone(pdc);
                    result.DeliveredAmount += otherPdc.DeliveredAmount;
                    result.DeliveredQuantity += otherPdc.DeliveredQuantity;
                    result.InvoicedAmount += otherPdc.InvoicedAmount;
                    result.InvoicedQuantity += otherPdc.InvoicedQuantity;
                    return result;
                }
                /**
                * Create a delivery cost item based on the associated PO Item
                * @param {object} poItem current po item informations
                * @param {SapQueryObject} deliveryCost current delivery cost
                * @param {object} pdcDescriptions poItem descriptions
                * @return {Item} the delivery cost line item
                */
                function CreateDeliveryCostItem(poItem, deliveryCost, pdcDescriptions) {
                    var itemDescription = poItem.Short_Text;
                    var pdcPOItem = Sys.Helpers.Clone(poItem);
                    // For browse
                    pdcPOItem.Deliv_Qty = deliveryCost.DeliveredQuantity;
                    pdcPOItem.DeliveredAmount = deliveryCost.DeliveredAmount;
                    pdcPOItem.InvoicedAmount = deliveryCost.InvoicedAmount;
                    pdcPOItem.Iv_Qty = deliveryCost.InvoicedQuantity;
                    pdcPOItem.Quantity = poItem.Quantity;
                    pdcPOItem.OrderedAmount = 0;
                    // For line items table
                    pdcPOItem.ExpectedAmount = deliveryCost.DeliveredAmount - deliveryCost.InvoicedAmount;
                    pdcPOItem.ExpectedQuantity = deliveryCost.DeliveredQuantity - deliveryCost.InvoicedQuantity;
                    pdcPOItem.OpenInvoiceValue = pdcPOItem.ExpectedAmount;
                    pdcPOItem.OpenInvoiceQuantity = pdcPOItem.ExpectedQuantity;
                    pdcPOItem.Net_Value = pdcPOItem.DeliveredAmount / pdcPOItem.Deliv_Qty;
                    pdcPOItem.Net_Price = pdcPOItem.Net_Value;
                    pdcPOItem.Price_Unit = pdcPOItem.Net_Price;
                    pdcPOItem.Currency = deliveryCost.Currency;
                    pdcPOItem.Cond_Type = deliveryCost.ConditionType;
                    pdcPOItem.Vendor = Sys.Helpers.String.SAP.TrimLeadingZeroFromID(deliveryCost.AccountNumber);
                    pdcPOItem.Ref_Doc = "";
                    pdcPOItem.Ref_Doc_No = "";
                    if (pdcDescriptions) {
                        var pdcDescription = pdcDescriptions.filter(function (v) { return v.KSCHL === pdcPOItem.Cond_Type; });
                        if (pdcDescription.length && typeof pdcDescription[0].VTEXT !== "undefined") {
                            pdcPOItem.Short_Text = pdcDescription[0].VTEXT;
                        }
                    }
                    else {
                        pdcPOItem.Short_Text = Language.Translate("_Pricing Condition default description") + " " + pdcPOItem.Cond_Type + " - " + itemDescription;
                    }
                    return pdcPOItem;
                }
                PurchaseOrder.CreateDeliveryCostItem = CreateDeliveryCostItem;
                //#endregion
            })(PurchaseOrder = SAP.PurchaseOrder || (SAP.PurchaseOrder = {}));
        })(SAP = AP.SAP || (AP.SAP = {}));
    })(AP = Lib.AP || (Lib.AP = {}));
})(Lib || (Lib = {}));
