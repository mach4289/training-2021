///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_P2P_ExchangeRate",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "P2P library",
  "require": [
    "Lib_P2P_V12.0.425.0",
    "[Sys/Sys_GenericAPI_Server]",
    "[Sys/Sys_GenericAPI_Client]"
  ]
}*/
var Lib;
Lib.P2P.AddLib("ExchangeRate", function()
{
	// Lib.P2P.ExchangeRate
	var o =
	{
		GetCompanyCodeCurrency: function(companyCode, callback)
		{
			var companyCodeCurrency = null;
			var filter = "CompanyCode__=" + companyCode;
			if(!callback)
			{
				Sys.GenericAPI.Query("PurchasingCompanycodes__", filter, ["Currency__"], function(result, error)
				{
					if(!error && result && result.length > 0)
					{
						companyCodeCurrency = result[0].Currency__;
					}
				});
			}
			else
			{
				Sys.GenericAPI.Query("PurchasingCompanycodes__", filter, ["Currency__"], callback);
			}
			return companyCodeCurrency;
		},

		GetCompanyCodeCurrencies: function(companyCode, callback)
		{
			var currencies = [];
			var filter = "CompanyCode__=" + companyCode;
			if (!callback)
			{
				Sys.GenericAPI.Query("P2P - Exchange rate__", filter, ["CurrencyFrom__"], function(result, error)
				{
					if (error || !result)
					{
						return;
					}

					for (var i = 0; i < result.length; i++)
					{
						var r = result[i];
						if (currencies.indexOf(r.CurrencyFrom__) === -1)
						{
							currencies.push(r.CurrencyFrom__);
						}
					}

				}, null, 100);
			}
			else
			{
				Sys.GenericAPI.Query("P2P - Exchange rate__", filter, ["CurrencyFrom__"], callback, null, 100);
			}
			return currencies;
		},

		GetExchangeRate: function(companyCode, currency, callback)
		{
			var exchangeRate = 1;
			var filter = "&(CompanyCode__=" + companyCode + ")(CurrencyFrom__=" + currency +")";
			if (!callback)
			{
				var companyCurrency = this.GetCompanyCodeCurrency(companyCode);
				if (companyCurrency && companyCurrency !== currency)
				{
					Sys.GenericAPI.Query("P2P - Exchange rate__", filter, ["Rate__", "RatioFrom__", "RatioTo__"], function(result, error)
					{
						if(!error && result && result.length > 0)
						{
							exchangeRate = parseFloat(result[0].Rate__) * parseFloat(result[0].RatioTo__) / parseFloat(result[0].RatioFrom__);
						}
					});
					return exchangeRate;
				}
			}
			else
			{
				Sys.GenericAPI.Query("P2P - Exchange rate__", filter, ["Rate__", "RatioFrom__", "RatioTo__"], callback);
			}
			return 1;
		}
	};

	return o;
}
);