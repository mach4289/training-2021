/* eslint no-empty-function: "off", no-unused-vars: "off" */
/* LIB_DEFINITION
{
    "name": "LIB_CUSTOM_PARAMETERS",
    "libraryType": "LIB",
    "scriptType": "COMMON",
    "comment": "My custom parameters definition for any applications.",
	"versionable": false,
    "require": ["Sys/Sys_Parameters"]
}
*/

// eslint-disable-next-line no-redeclare
var Lib;
(function ()
{
	/**
     * @description This module allows you to set the parameters of any solutions
     * depending on the current execution environment (Development/QA/Production).
     * The environment is detected based on the sub-account identifier.
     */

	// If the environment detection fails, the parameters are set based on the default environment (PROD by default).

	/*
	Example: My custom parameters for Purchasing processes only (PAC instance).
	Sys.Parameters.GetInstance("PAC").Extend(
	// Each object associates one environment to a list of parameters and their values.
    // This shouldn't be used to declare new environments that were not declared at the init, mostly because the current running environment is compute during the init and won't take into account those new ones.
    {
    	"PROD": // Environment name: must exactly match on of those declared during init
        {
        	"ERP": "SAP"
		},
    	"QA":
        {
        	"ERP": "SAP"
        },
    	"DEV":
        {
        	"ERP": "SAP"
        }
    });
	*/
})();
