///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_Demo",
  "libraryType": "LIB",
  "scriptType": "SERVER",
  "comment": "Purchasing library",
  "require": [
    "Lib_ERP_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "Sys/Sys_Helpers_String",
    "Sys/Sys_Helpers_Date",
    "Lib_Purchasing_Items_V12.0.425.0"
  ]
}*/

var Lib;
Lib.Purchasing.AddLib("Demo", function ()
{
	/**
		* @exports Demo
		* @memberof Lib.Purchasing
		*/

	//////////////////////////////////////////
	// Private methods for Invoice generation
	//////////////////////////////////////////
	var RetrievePO = function (poNumber)
		{
			if (!Data.GetValue("/process/name").startsWith("Purchase order"))
			{
				return Lib.P2P.QueryPOFormData(poNumber);
			}
			return Data;
		},

		// generated filename: PO_INVOICE_<CompanyCode>_<ERP>_<InvoiceNumber>.pdf
		GenerateVendorInvoiceName = function (invoicenumber)
		{
			var invoicename = "PO_INVOICE_" + Data.GetValue("CompanyCode__") + "_" + Lib.ERP.GetERPName() + "_" + invoicenumber + ".pdf";
			Log.Info(invoicename + " generated");
			return invoicename;
		},

		AttachVendorInvoice_DOC = function (childCF, csvFilename)
		{
			// attaches the word template as the current document
			var language = Lib.P2P.GetValidatorOrOwner().GetVars().GetValue_String("Language", 0);
			var templateParameter = Lib.P2P.IsGRIVEnabledGlobally() ? "DemoInvoiceGRIVTemplateName" : "DemoInvoiceTemplateName";
			var Invoice_Template = Sys.Parameters.GetInstance("PAC").GetParameter(templateParameter);
			Invoice_Template = Lib.P2P.GetValidatorOrOwner().GetTemplateFile(Invoice_Template, language);

			var attach = childCF.AddAttachEx(Invoice_Template);
			var attachVars = attach.GetVars();

			attachVars.AddValue_Long("AttachToProcess", 1, true);
			attachVars.AddValue_String("AttachOutputFormat", "pdf", true);
			attachVars.AddValue_String("AttachConvertOptions", "?mailmergedatasource=%[Resources(AttachOutputName=" + csvFilename + ")]", true);
		},

		AttachVendorInvoice_CSV = function (poData, childCF, csvFilename, invoiceNumber, onlyDelivered)
		{
			const isMultiShipTo = Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false);
			Log.Info("[Lib.Purchasing.Demo] during AttachVendorInvoice_CSV => isMultiShipTo : " + isMultiShipTo);

			var csv = Lib.Purchasing.BuildCSVHeader(["VendorNumber", "VendorName", "VendorVATNumber", "VendorFaxNumber", "VendorAddress",
			"ShipToCompany", "ShipToContact", "ShipToAddress",
			"Invoice_number", "PO_number",
			"Currency", "TotalNetAmount", "TotalTaxAmount", "Total",
			"InvoiceDate", "DeliveryNote", "ItemNumber", "Description", "Quantity", "UnitPrice", "NetAmount", "TaxAmount", "LastRecord"]);

			// tax codes for SAP and generic demo
			var taxRatesForDemoInvoice = {
				"FR01": 20.0, // V4:20
				"1000": 19.0, // VA:19
				"US01": 19.0, // VA:19
				"3000": 8.0,   // I1:8
				"00001": 5.0,	// for JDE NY
				"00070": 20.0,	// for JDE FRIVA
				"204": 15.0,	// for EBS US
				"888": 19.6,	// for EBS FR
				"CRONUS International Ltd.": 10, // for NAV
				"CRONUS France S.A.": 19.6 // for NAV FR
			};

			var taxRate = 19.6; // Default value if no specific value found (good for JDE demo environment)
			var companyCode = poData.GetValue("CompanyCode__");
			if (companyCode in taxRatesForDemoInvoice)
			{
				taxRate = taxRatesForDemoInvoice[companyCode];
				Log.Info("[Lib.Purchasing.Demo] Tax rate used to generate demo invoice:" + taxRate);
			}
			else
			{
				Log.Warn("[Lib.Purchasing.Demo] No default tax rate found for company code:" + companyCode + ", using:" + taxRate);
			}

			var val = Lib.Purchasing.GetValueAsString;

			var table, deliveryDate, deliveryNote;
			var noGoodsReceiptItems = onlyDelivered;
			if (Sys.Helpers.IsArray(noGoodsReceiptItems))
			{
				// here, onlyDelivered contains PO line items having requested delivery date reached and no goods receipt needed
				Log.Info("[Lib.Purchasing.Demo] " + noGoodsReceiptItems.length + " PO line items having requested delivery date reached and no goods receipt needed");
				table =
				{
					GetItemCount: function ()
					{
						return noGoodsReceiptItems.length;
					},
					GetItem: function (index)
					{
						var PO2GRItemName =
						{
							"Number__": "ItemNumber__",
							"Description__": "ItemDescription__",
							"OrderedQuantity__": "ItemQuantity__",
							"ReceivedQuantity__": "ItemQuantity__",
							"UnitPrice__": "ItemUnitPrice__"
						};
						return {
							GetProperties: function (name)
							{
								return index < noGoodsReceiptItems.length ? noGoodsReceiptItems[index].GetProperties(PO2GRItemName[name]) : null;
							},
							GetValue: function(name)
							{
								return index < noGoodsReceiptItems.length ? noGoodsReceiptItems[index].GetValue(PO2GRItemName[name]) : null;
							}
						}
					}
				};
				deliveryDate = table.GetItemCount() > 0 ? noGoodsReceiptItems[0].GetValue("ItemRequestedDeliveryDate__") : new Date();
				deliveryNote = "No goods receipt items";
				onlyDelivered = false;
			}
			else
			{
				// from GoodReceipt process
				Log.Info("[Lib.Purchasing.Demo] from GoodReceipt process");
				table = Data.GetTable("LineItems__");
				deliveryDate = Data.GetValue("DeliveryDate__");
				deliveryNote = Data.GetValue("DeliveryNote__");
			}

			var nItems = table.GetItemCount();
			var itemIdx;
			var invoiceItem;

			var invoice = { totalNetAmount: 0, totalTaxAmount: 0, totalAmount: 0, invoiceItems: [] };

			var precision;
			if (nItems > 0)
			{
				var properties = table.GetItem(0).GetProperties("UnitPrice__");
				if (properties)
				{
					precision = properties.precision;
				}
			}

			// Add items
			for (itemIdx = 0; itemIdx < nItems; itemIdx++)
			{
				var currentItem = table.GetItem(itemIdx);
				var qt = currentItem.GetValue("ReceivedQuantity__");

				if ((onlyDelivered && qt != null && qt !== 0) || (!onlyDelivered))
				{
					invoiceItem = {
						deliveryNote: deliveryNote,
						number: currentItem.GetValue("Number__"),
						description: currentItem.GetValue("Description__"),
						quantity: onlyDelivered ? qt : currentItem.GetValue("OrderedQuantity__"),
						unitPrice: currentItem.GetValue("UnitPrice__"),
						netAmount: 0,
						taxAmount: 0,
						deliveredDate: deliveryDate,
						lineNumber : currentItem.GetValue("LineNumber__")
					};

					invoiceItem.netAmount = invoiceItem.unitPrice * invoiceItem.quantity;
					if (Lib.Purchasing.Items.IsAmountBasedItem(currentItem))
					{
						invoiceItem.quantity = null;
						invoiceItem.unitPrice = null;
					}

					invoiceItem.taxAmount = invoiceItem.netAmount * taxRate / 100;

					invoice.invoiceItems.push(invoiceItem);

					invoice.totalNetAmount += invoiceItem.netAmount;
					invoice.totalTaxAmount += invoiceItem.taxAmount;
					invoice.totalAmount += invoiceItem.netAmount + invoiceItem.taxAmount;
				}
			}

			// Is this the first non-canceled reception for the related PO ?
			// !!! This function is called before PO Items sync
			var poItemsFilter = "(&(!(Status__=Canceled))(DeliveryDate__=*)(PONumber__=" + poData.GetValue("OrderNumber__") + "))";
			var poItems = Lib.Purchasing.Items.GetItemsForDocumentSync(Lib.Purchasing.Items.POItemsDBInfo, poItemsFilter, "LineNumber__");
			poItems = Object.keys(poItems);
			if (!poItems || !poItems.length)
			{
				// This means it's the first time a GR is generated for the PO
				// Additional fees of the PO are all included in the invoice generated for the first GR
				// Add additional fees
				var feesTable = poData.GetTable("AdditionalFees__");
				for (itemIdx = 0; itemIdx < feesTable.GetItemCount(); itemIdx++)
				{
					var currentItem = feesTable.GetItem(itemIdx);
					var amount = parseFloat(currentItem.GetValue("Price__"));
					invoiceItem = {
						deliveryNote: deliveryNote,
						number: "",
						description: currentItem.GetValue("AdditionalFeeDescription__"),
						quantity: null,
						unitPrice: null,
						netAmount: amount,
						taxAmount: amount * taxRate / 100,
						deliveredDate: deliveryDate,
						lineNumber : ""
					};

					invoice.invoiceItems.push(invoiceItem);

					invoice.totalNetAmount += invoiceItem.netAmount;
					invoice.totalTaxAmount += invoiceItem.taxAmount;
					invoice.totalAmount += invoiceItem.netAmount + invoiceItem.taxAmount;
				}

			}

			// The vendor sends invoices in its own culture ==> generate invoice accordingly
			// If no vendor returned (ex. AlwaysCreateVendor = 0) we take the culture of the current owner of document
			var user = Lib.Purchasing.GetVendor(poData, poData.GetValue("VendorEmail__")) || Lib.P2P.GetValidatorOrOwner();
			var culture = user.GetVars().GetValue_String("Culture", 0);

			var nval = function(number, name, culture) { return number == null ? "" : Lib.Purchasing.GetNumberAsString(number, culture, precision); };

			var csvFixed = Lib.Purchasing.BuildCSVLine([val(poData, "VendorNumber__", culture), val(poData, "VendorName__", culture), val(poData, "VendorVatNumber__", culture),
			val(poData, "VendorFaxNumber__", culture), val(poData, "VendorAddress__", culture)]);

			var csvShipToContact = val(poData, "ShipToContact__", culture);

			var csvShipToCompany;
			var csvShipToAddress;
			var csvShipTo;

			if (!isMultiShipTo)
			{
				csvShipToCompany = val(poData, "ShipToCompany__", culture);
				csvShipToAddress = val(poData, "ShipToAddress__", culture);
				csvShipTo = Lib.Purchasing.BuildCSVLine([csvShipToCompany, csvShipToContact, csvShipToAddress]);
			}

			var csvFixed2 = Lib.Purchasing.BuildCSVLine([invoiceNumber, val(poData, "OrderNumber__", culture),
			val(poData, "Currency__", culture), nval(invoice.totalNetAmount, culture, precision), nval(invoice.totalTaxAmount, culture, precision), nval(invoice.totalAmount, culture, precision)]);

			var tablePoData = poData.GetTable("LineItems__");

			for (itemIdx in invoice.invoiceItems)
			{
				invoiceItem = invoice.invoiceItems[itemIdx];

				if (isMultiShipTo)
				{
					for (var lineIdx = 0; lineIdx < tablePoData.GetItemCount(); ++lineIdx)
					{
						var item = tablePoData.GetItem(lineIdx);
						if(parseInt(item.GetValue("LineItemNumber__")) === invoiceItem.lineNumber)
						{
							csvShipToCompany = item.GetValue("ItemShipToCompany__");
							csvShipToAddress = item.GetValue("ItemShipToAddress__");
							break;
						}
					}
					csvShipTo = Lib.Purchasing.BuildCSVLine([csvShipToCompany, csvShipToContact, csvShipToAddress]);
				}

				csv += csvFixed + ";" + csvShipTo + ";" + csvFixed2 + ";" + 
					Lib.Purchasing.BuildCSVLine([Sys.Helpers.Date.ToLocaleDate(invoiceItem.deliveredDate, culture), invoiceItem.deliveryNote, invoiceItem.number,
						invoiceItem.description, nval(invoiceItem.quantity, culture), nval(invoiceItem.unitPrice, culture, precision),
						nval(invoiceItem.netAmount, culture, precision), nval(invoiceItem.taxAmount, culture, precision),
						(itemIdx == invoice.invoiceItems.length - 1) ? "1" : ""]) + "\n";
			}

			// attaches .csv as a resource
			var attach = childCF.AddAttach();
			var attachVars = attach.GetVars();
			attachVars.AddValue_String("AttachEncoding", "UNICODE", true);
			attachVars.AddValue_String("AttachContent", csv, true);
			attachVars.AddValue_String("AttachType", "inline", true);
			attachVars.AddValue_String("AttachIsResource", "1", true);
			attachVars.AddValue_String("AttachOutputName", csvFilename, true);
		};

	//////////////////////////////////////////
	// Public methods for Invoice generation
	//////////////////////////////////////////
	var demo =
		{
			/**
			* @description Generate an invoice at each delivery for demo. The copy file connector and AP
			*			   package must be installed.
			*
			* @param poNumber number of the purchase order.
			* @param bDeferred deffers or not the new copy file process.
			* @param onlyDelivered only invoices or not the delivered items.
			*
			**/
			GenerateVendorInvoice: function (poNumber, bDeferred, onlyDelivered)
			{
				Log.Time("GenerateVendorInvoice");
				Log.Info("Generating vendor invoice for po number: " + poNumber);

				Log.Time("RetrievePO");
				var poData = RetrievePO(poNumber);
				Log.TimeEnd("RetrievePO");

				if (poData)
				{
					Log.Time("CreateTransport");
					var childCF = Process.CreateTransport("Copy");
					Log.TimeEnd("CreateTransport");
					if (childCF)
					{
						Log.Time("Sequence");
						var invoiceNumberSequence;
						try
						{
							var sequenceTable = "Vendor invoice";
							invoiceNumberSequence = Process.GetSequence("DemoInvNum", sequenceTable);
						}
						catch (e)
						{
							Log.Warn("Failed to get sequence, AP probably not enabled, will not generate vendor invoice");
							return;
						}

						var invoiceNumberSeqValue = invoiceNumberSequence.GetNextValue();
						var invoiceNumber = Lib.Purchasing.FormatSequenceNumber(invoiceNumberSeqValue, "DemoInvoiceNumberFormat");
						Log.TimeEnd("Sequence");

						// Creates the child process
						Log.Time("Attach DOC");
						AttachVendorInvoice_DOC(childCF, "PO.csv");
						Log.TimeEnd("Attach DOC");
						Log.Time("Attach CSV");
						AttachVendorInvoice_CSV(poData, childCF, "PO.csv", invoiceNumber, onlyDelivered);
						Log.TimeEnd("Attach CSV");

						Log.Time("Set vars");
						var vars = childCF.GetUninheritedVars();

						vars.AddValue_String("CopyPath", "SFTP2", true);

						var ftpFolderPathIn = "In";
						vars.AddValue_String("CopyFileName", "invoices\\" + ftpFolderPathIn + "\\" + GenerateVendorInvoiceName(invoiceNumber), true);

						if (bDeferred)
						{
							vars.AddValue_String("Deferred", "1", true);
							var def = new Date();
							def.setMinutes(def.getMinutes() + 1);
							vars.AddValue_String("DeferredDateTime", Sys.Helpers.Date.Date2DBDateTime(def), true);
						}
						Log.TimeEnd("Set vars");

						Log.Time("Process");
						childCF.Process();
						Log.TimeEnd("Process");
						Log.Info("vendor invoice has been generated");
					}
					else
					{
						Log.Info("copy file connector is not installed");
					}
				}
				else
				{
					Log.Info("No po data found with number: " + poNumber);
				}

				Log.TimeEnd("GenerateVendorInvoice");
			}
		};

	return demo;
});