///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_ShipTo",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Sys/Sys_Helpers",
    "Sys/Sys_Helpers_Promise",
    "Sys/Sys_OnDemand_Users",
    "Lib_P2P_V12.0.425.0",
    "Lib_Purchasing_V12.0.425.0",
    "[Lib_Purchasing_ShipTo_Client_V12.0.425.0]",
    "Sys/Sys_TechnicalData"
  ]
}*/
// Common interface
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var ShipTo;
        (function (ShipTo) {
            var shipToFieldsMapping = {
                "ID__": "DeliveryAddressID__",
                "ShipToCompany__": "ShipToCompany__",
                "ShipToContact__": "ShipToContact__",
                "ShipToPhone__": "ShipToPhone__",
                "ShipToEmail__": "ShipToEmail__"
            };
            var itemShipToFieldsMapping = {
                "ID__": "ItemDeliveryAddressID__",
                "ShipToCompany__": "ItemShipToCompany__"
            };
            function GetContactAttributes() {
                return Object.keys(shipToFieldsMapping).join("|");
            }
            ShipTo.GetContactAttributes = GetContactAttributes;
            ShipTo.defaultCountryCode = "";
            function FillPostalAddress(ShipToAddress, item) {
                Log.Time("ShipTo.FillPostalAddress");
                var fromCountryCode = Lib.Purchasing.ShipTo.defaultCountryCode;
                if (ShipToAddress.ForceCountry === "false") {
                    fromCountryCode = ShipToAddress.ToCountry;
                }
                else if (!fromCountryCode) {
                    fromCountryCode = "US";
                }
                var options = {
                    "isVariablesAddress": true,
                    "address": ShipToAddress,
                    "countryCode": fromCountryCode // Get country code from contract ModProvider
                };
                return Sys.Helpers.Promise.Create(function (resolve /*, reject*/) {
                    var itemOrData = item || Data;
                    var fieldName = item ? "ItemShipToAddress__" : "ShipToAddress__";
                    if (ShipToAddress.formattedpostaladdress) {
                        itemOrData.SetValue(fieldName, ShipToAddress.formattedpostaladdress);
                        resolve();
                    }
                    else {
                        var OnShipToAddressFormatResult = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.OnShipToAddressFormat", options);
                        if (OnShipToAddressFormatResult === false || OnShipToAddressFormatResult === null) {
                            Sys.GenericAPI.CheckPostalAddress(options, function (address) {
                                Log.Info("CheckPostalAddress input:  " + JSON.stringify(ShipToAddress));
                                Log.Info("CheckPostalAddress output: " + JSON.stringify(address));
                                Sys.Helpers.SilentChange(function () {
                                    if (!Sys.Helpers.IsEmpty(address.LastErrorMessage)) {
                                        itemOrData.SetValue(fieldName, null);
                                        itemOrData.SetWarning(fieldName, address.LastErrorMessage);
                                    }
                                    else {
                                        itemOrData.SetValue(fieldName, address.FormattedBlockAddress.replace(/^[^\r\n]+(\r|\n)+/, ""));
                                    }
                                });
                                Log.TimeEnd("ShipTo.FillPostalAddress");
                                resolve();
                            });
                        }
                        else {
                            itemOrData.SetValue(fieldName, OnShipToAddressFormatResult);
                            //Formated by user exit
                            resolve();
                        }
                    }
                });
            }
            ShipTo.FillPostalAddress = FillPostalAddress;
            function Fill(getValueFct, onlyAddress, item) {
                Log.Time("ShipTo.Fill");
                if (!onlyAddress) {
                    var itemOrData_1 = item || Data;
                    Sys.Helpers.Object.ForEach(item ? itemShipToFieldsMapping : shipToFieldsMapping, function (field, ctField) {
                        itemOrData_1.SetValue(field, getValueFct(ctField));
                    });
                }
                var vendorAddress = JSON.parse(Variable.GetValueAsString("VendorAddress") || null);
                var sameCountry;
                return Lib.P2P.CompanyCodesValue.QueryValues(Data.GetValue("CompanyCode__"))
                    .Then(function (CCValues) {
                    if (Lib.P2P.IsPR()) {
                        if (Object.keys(CCValues).length > 0) {
                            sameCountry = CCValues.Country__ == getValueFct("ShipToCountry__");
                        }
                    }
                    else {
                        sameCountry = vendorAddress && (vendorAddress.ToCountry == getValueFct("ShipToCountry__"));
                    }
                    var ShipToAddress = {
                        "ToName": "ToRemove",
                        "ToSub": getValueFct("ShipToSub__"),
                        "ToMail": getValueFct("ShipToStreet__"),
                        "ToPostal": getValueFct("ShipToZipCode__"),
                        "ToCountry": getValueFct("ShipToCountry__"),
                        "ToState": getValueFct("ShipToRegion__"),
                        "ToCity": getValueFct("ShipToCity__"),
                        "ForceCountry": sameCountry ? "false" : "true"
                    };
                    Sys.TechnicalData.SetValue("ShipToAddress", ShipToAddress);
                    Variable.SetValueAsString("ShipToAddress", JSON.stringify(ShipToAddress));
                    var promises = [];
                    ShipToAddress["formattedpostaladdress"] = getValueFct("formattedpostaladdress");
                    promises.push(FillPostalAddress(ShipToAddress, item));
                    if (vendorAddress) {
                        Log.Info("Filling vendor from shipto");
                        vendorAddress.ForceCountry = ShipToAddress.ForceCountry;
                        Variable.SetValueAsString("VendorAddress", JSON.stringify(vendorAddress));
                        promises.push(Lib.Purchasing.Vendor.FillPostalAddress(vendorAddress));
                    }
                    return Sys.Helpers.Synchronizer.All(promises);
                })
                    .Then(function () {
                    Log.TimeEnd("ShipTo.Fill");
                });
            }
            ShipTo.Fill = Fill;
            /**
            * Reset all field and error related to ShipTo
            */
            function Reset() {
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                        resetFields(item);
                        item.SetValue("ItemShipToAddress__", null);
                        item.SetError("ItemShipToAddress__", "");
                    });
                }
                else {
                    resetFields();
                    Variable.SetValueAsString("ShipToAddress", "");
                    Data.SetValue("ShipToAddress__", null);
                    Data.SetError("ShipToAddress__", "");
                }
            }
            ShipTo.Reset = Reset;
            function resetFields(item) {
                var itemOrData = item || Data;
                Sys.Helpers.Object.ForEach(item ? itemShipToFieldsMapping : shipToFieldsMapping, function (field) {
                    itemOrData.SetValue(field, null);
                    itemOrData.SetError(field, "");
                });
            }
            function QueryShipTo(deliveryAddessId, companyCode, callback) {
                var filter = "(&(ID__=" + deliveryAddessId + ")(CompanyCode__=" + companyCode + "))";
                var fields = ["ID__", "ShipToCompany__", "ShipToContact__", "ShipToPhone__", "ShipToEmail__", "ShipToSub__", "ShipToStreet__", "ShipToCity__", "ShipToZipCode__", "ShipToRegion__", "ShipToCountry__"];
                Sys.GenericAPI.Query("PurchasingShipTo__", filter, fields, function (result, error) {
                    callback(result, error);
                }, null, 1);
            }
            ShipTo.QueryShipTo = QueryShipTo;
            function QueryShipToByShipToCompany(shipToCompany, companyCode, callback) {
                var filter = "(&(ShipToCompany__=" + shipToCompany + ")(CompanyCode__=" + companyCode + "))";
                var fields = ["ID__", "ShipToCompany__", "ShipToContact__", "ShipToPhone__", "ShipToEmail__", "ShipToSub__", "ShipToStreet__", "ShipToCity__", "ShipToZipCode__", "ShipToRegion__", "ShipToCountry__"];
                Sys.GenericAPI.Query("PurchasingShipTo__", filter, fields, function (result, error) {
                    callback(result, error);
                }, null, 1);
            }
            ShipTo.QueryShipToByShipToCompany = QueryShipToByShipToCompany;
            function CheckDeliveryAddress(currentRole) {
                var isOk = true;
                var isRoleRequesterOrUndefined = !currentRole || (currentRole && Lib.Purchasing.IsRequester(currentRole));
                if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false) && isRoleRequesterOrUndefined) {
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        if (!Lib.Purchasing.IsLineItemEmpty(line)) {
                            if (Sys.Helpers.IsEmpty(line.GetValue("ItemShipToCompany__"))) {
                                isOk = false;
                                line.SetError("ItemShipToCompany__", "This field is required!");
                            }
                            if (Sys.Helpers.IsEmpty(line.GetValue("ItemShipToAddress__"))) {
                                isOk = false;
                                line.SetError("ItemShipToAddress__", "This field is required!");
                            }
                        }
                    });
                }
                return isOk;
            }
            ShipTo.CheckDeliveryAddress = CheckDeliveryAddress;
            function Init() {
                Log.Time("ShipTo.Init");
                return Sys.Helpers.Promise.Create(function (resolve) {
                    var shipToVars = Sys.TechnicalData.GetValue("ShipToAddress");
                    if (shipToVars) {
                        var vendorAddress = JSON.parse(Variable.GetValueAsString("VendorAddress") || null);
                        var sameCountry = vendorAddress && (vendorAddress.ToCountry == shipToVars["ToCountry__"]);
                        shipToVars["ForceCountry"] = sameCountry ? "false" : "true";
                        Variable.SetValueAsString("ShipToAddress", JSON.stringify(shipToVars));
                        var promises = [];
                        promises.push(FillPostalAddress(shipToVars));
                        if (vendorAddress) {
                            Log.Info("Filling vendor from shipto");
                            vendorAddress.ForceCountry = shipToVars.ForceCountry;
                            Variable.SetValueAsString("VendorAddress", JSON.stringify(vendorAddress));
                            promises.push(Lib.Purchasing.Vendor.FillPostalAddress(vendorAddress));
                        }
                        Sys.Helpers.Synchronizer.All(promises).then(function () {
                            Log.TimeEnd("ShipTo.Init");
                            resolve();
                        });
                    }
                    else if (Data.GetValue("DeliveryAddressID__") && Data.GetValue("CompanyCode__")) {
                        QueryShipTo(Data.GetValue("DeliveryAddressID__"), Data.GetValue("CompanyCode__"), function (result, error) {
                            if (error || !result || result.length == 0) {
                                if (Sys.ScriptInfo.IsClient()) {
                                    Sys.Helpers.Globals.Popup.Alert(error, true, null, "_error");
                                }
                                else {
                                    Lib.CommonDialog.NextAlert.Define("_error", error, { isError: false }, error);
                                }
                                Log.TimeEnd("ShipTo.Init");
                                resolve();
                            }
                            else {
                                Lib.Purchasing.ShipTo.Fill(function (field) { return result[0][field]; }, true)
                                    .Then(function () {
                                    Log.TimeEnd("ShipTo.Init");
                                    resolve();
                                });
                            }
                        });
                    }
                    else {
                        Log.TimeEnd("ShipTo.Init");
                        resolve();
                    }
                });
            }
            ShipTo.Init = Init;
        })(ShipTo = Purchasing.ShipTo || (Purchasing.ShipTo = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
