///#GLOBALS Lib Sys
/* LIB_DEFINITION
{
	"name": "LIB_DD_COMMON",
	"libraryType": "LIB",
	"scriptType": "COMMON",
	"comment": "The only purpose of this lybrary is to create Lib.DD for common scripts run at client side (T_T)",
	"require": [ ]
}
*/


/**
 * @namespace Lib.DD.AppDeliveries
 */
var Lib;
if (!Lib.DD)
{
	Lib.AddLib("DD", function()
	{
		return {};
	});
}
