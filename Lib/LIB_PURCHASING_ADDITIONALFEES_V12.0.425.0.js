///#GLOBALS Lib Sys
/* LIB_DEFINITION{
  "name": "Lib_Purchasing_AdditionalFees",
  "libraryType": "LIB",
  "scriptType": "COMMON",
  "comment": "Purchasing library",
  "require": [
    "Lib_Purchasing_V12.0.425.0",
    "[Sys/Sys_GenericAPI_Client]",
    "[Sys/Sys_GenericAPI_Server]",
    "Sys/Sys_Helpers"
  ]
}*/
var Lib;
(function (Lib) {
    var Purchasing;
    (function (Purchasing) {
        var AdditionalFees;
        (function (AdditionalFees) {
            var cachedValues = {};
            function areCachedValues(companyCode, ids) {
                if (ids) {
                    ids.forEach(function (id) {
                        if (!cachedValues[companyCode] || !cachedValues[companyCode][id]) {
                            return false;
                        }
                        return true;
                    });
                }
                return false;
            }
            function QueryValues(companyCode, requiredIds) {
                var ids = [];
                if (requiredIds && !Sys.Helpers.IsArray(requiredIds)) {
                    ids = [requiredIds];
                }
                else {
                    ids = requiredIds;
                }
                return Sys.Helpers.Promise.Create(function (resolve) {
                    // when we want the currencies, return cache value only if the currencies have been loaded
                    if (areCachedValues(companyCode, ids)) {
                        resolve(cachedValues[companyCode]);
                    }
                    else if (companyCode) {
                        var filter_1 = "(&(|(CompanyCode__=" + companyCode + ")(CompanyCode__=))";
                        ids.forEach(function (id) {
                            filter_1 += "(AdditionalFeeID__=" + id + ")";
                        });
                        filter_1 += ")";
                        var options = {
                            table: "P2P - Additional Fee__",
                            filter: filter_1,
                            attributes: ["MaxAmount__", "AdditionalFeeID__", "CompanyCode__", "Description__"]
                        };
                        Sys.GenericAPI.PromisedQuery(options)
                            .Then(function (queryResults) {
                            if (queryResults.length > 0) {
                                queryResults.forEach(function (r) {
                                    if (!cachedValues[r.CompanyCode__]) {
                                        cachedValues[r.CompanyCode__] = {};
                                    }
                                    cachedValues[r.CompanyCode__][r.AdditionalFeeID__] = {
                                        AdditionalFeeID__: r.AdditionalFeeID__,
                                        Description__: r.Description__,
                                        MaxAmount__: r.MaxAmount__
                                    };
                                });
                            }
                            else {
                                cachedValues[companyCode] = {};
                            }
                            resolve(cachedValues[companyCode]);
                        })
                            .Catch(function () {
                            cachedValues[companyCode] = {};
                            resolve(cachedValues[companyCode]);
                        });
                    }
                    else {
                        cachedValues[companyCode] = {};
                        resolve(cachedValues[companyCode]);
                    }
                });
            }
            AdditionalFees.QueryValues = QueryValues;
            function GetValues(companyCode, additionalFeeID) {
                if (cachedValues[companyCode] && cachedValues[companyCode][additionalFeeID]) {
                    return cachedValues[companyCode][additionalFeeID];
                }
                return null;
            }
            AdditionalFees.GetValues = GetValues;
            function PushValues(companyCode, newFee) {
                if (!cachedValues[companyCode]) {
                    cachedValues[companyCode] = {};
                }
                cachedValues[companyCode][newFee.AdditionalFeeID__] = newFee;
            }
            AdditionalFees.PushValues = PushValues;
        })(AdditionalFees = Purchasing.AdditionalFees || (Purchasing.AdditionalFees = {}));
    })(Purchasing = Lib.Purchasing || (Lib.Purchasing = {}));
})(Lib || (Lib = {}));
