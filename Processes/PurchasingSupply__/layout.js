{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Name__": "LabelName__",
																	"LabelName__": "Name__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"SupplyID__": "LabelSupplyID__",
																	"LabelSupplyID__": "SupplyID__",
																	"BuyerLogin__": "LabelBuyerLogin__",
																	"LabelBuyerLogin__": "BuyerLogin__",
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"RecipientLogin__": "LabelRecipientLogin__",
																	"LabelRecipientLogin__": "RecipientLogin__",
																	"DefaultGLAccount__": "LabelDefaultGLAccount__",
																	"LabelDefaultGLAccount__": "DefaultGLAccount__",
																	"NoGoodsReceipt__": "LabelNoGoodsReceipt__",
																	"LabelNoGoodsReceipt__": "NoGoodsReceipt__",
																	"ParentSupplyID__": "LabelParentSupplyID__",
																	"LabelParentSupplyID__": "ParentSupplyID__",
																	"FullName__": "LabelFullName__",
																	"LabelFullName__": "FullName__",
																	"DefaultCostType__": "LabelDefaultCostType__",
																	"LabelDefaultCostType__": "DefaultCostType__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 11,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Name__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelName__": {
																				"line": 4,
																				"column": 1
																			},
																			"Description__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 6,
																				"column": 1
																			},
																			"SupplyID__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelSupplyID__": {
																				"line": 3,
																				"column": 1
																			},
																			"BuyerLogin__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelBuyerLogin__": {
																				"line": 7,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"RecipientLogin__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelRecipientLogin__": {
																				"line": 8,
																				"column": 1
																			},
																			"DefaultGLAccount__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelDefaultGLAccount__": {
																				"line": 10,
																				"column": 1
																			},
																			"NoGoodsReceipt__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelNoGoodsReceipt__": {
																				"line": 11,
																				"column": 1
																			},
																			"ParentSupplyID__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelParentSupplyID__": {
																				"line": 2,
																				"column": 1
																			},
																			"FullName__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelFullName__": {
																				"line": 5,
																				"column": 1
																			},
																			"DefaultCostType__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelDefaultCostType__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1
																			},
																			"stamp": 27
																		},
																		"LabelParentSupplyID__": {
																			"type": "Label",
																			"data": [
																				"ParentSupplyID__"
																			],
																			"options": {
																				"label": "_ParentSupplyID",
																				"version": 0,
																				"hidden": false
																			},
																			"stamp": 36
																		},
																		"ParentSupplyID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ParentSupplyID__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ParentSupplyID",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autocompletable": false,
																				"hidden": false,
																				"RestrictSearch": true,
																				"PreFillFTS": true
																			},
																			"stamp": 37
																		},
																		"LabelSupplyID__": {
																			"type": "Label",
																			"data": [
																				"SupplyID__"
																			],
																			"options": {
																				"label": "SupplyID",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"SupplyID__": {
																			"type": "ShortText",
																			"data": [
																				"SupplyID__"
																			],
																			"options": {
																				"label": "SupplyID",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 17
																		},
																		"LabelName__": {
																			"type": "Label",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "Name",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"Name__": {
																			"type": "ShortText",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "Name",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"length": 100,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 15
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 19
																		},
																		"LabelBuyerLogin__": {
																			"type": "Label",
																			"data": [
																				"BuyerLogin__"
																			],
																			"options": {
																				"label": "BuyerLogin",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"BuyerLogin__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"BuyerLogin__"
																			],
																			"options": {
																				"label": "BuyerLogin",
																				"activable": true,
																				"width": "500",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"autocompletable": true,
																				"browsable": true
																			},
																			"stamp": 25
																		},
																		"LabelRecipientLogin__": {
																			"type": "Label",
																			"data": [
																				"RecipientLogin__"
																			],
																			"options": {
																				"label": "RecipientLogin",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"RecipientLogin__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"RecipientLogin__"
																			],
																			"options": {
																				"label": "RecipientLogin",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"PreFillFTS": true,
																				"version": 0,
																				"browsable": true
																			},
																			"stamp": 29
																		},
																		"LabelDefaultGLAccount__": {
																			"type": "Label",
																			"data": [
																				"DefaultGLAccount__"
																			],
																			"options": {
																				"label": "_DefaultGLAccount",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"DefaultGLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"DefaultGLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_DefaultGLAccount",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 31
																		},
																		"LabelDefaultCostType__": {
																			"type": "Label",
																			"data": [
																				"DefaultCostType__"
																			],
																			"options": {
																				"label": "_DefaultCostType"
																			},
																			"stamp": 40
																		},
																		"DefaultCostType__": {
																			"type": "ComboBox",
																			"data": [
																				"DefaultCostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_DefaultCostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 41
																		},
																		"LabelNoGoodsReceipt__": {
																			"type": "Label",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"NoGoodsReceipt__": {
																			"type": "CheckBox",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"FullName__": {
																			"type": "ShortText",
																			"data": [
																				"FullName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FullName",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 39
																		},
																		"LabelFullName__": {
																			"type": "Label",
																			"data": [
																				"FullName__"
																			],
																			"options": {
																				"label": "_FullName",
																				"version": 0
																			},
																			"stamp": 38
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 41,
	"data": []
}