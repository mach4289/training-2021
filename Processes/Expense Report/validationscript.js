var Validation;
(function (Validation) {
    ///#GLOBALS Lib Sys
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    var wkf = Lib.Expense.Report.Workflow;
    wkf.controller.Define(wkf.parameters);
    Log.Info("-- Expense Report Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
    /** ********* **/
    /** FUNCTIONS **/
    /** ********* **/
    function Save() {
        // The standard Save action does not save the EDDMessageVars, in our case ValidityDateTime
        // --> use Approve action + PreventApproval
        var contributor = Lib.Expense.Report.Workflow.controller.GetContributorAt(0);
        Lib.Expense.Report.Workflow.NotifyEndOfContributionOnMobile(contributor);
        Process.PreventApproval();
        Process.LeaveForm();
    }
    function SubmitExpenseReport() {
        Log.Info("Submit expense report...");
        // After first submit workflow cannot be rebuild
        wkf.controller.AllowRebuild(false);
        var sequenceStep = wkf.controller.GetContributorIndex();
        var currentContributor = wkf.controller.GetContributorAt(sequenceStep);
        if (!wkf.controller.DoAction(currentContributor.action)) {
            Process.PreventApproval();
        }
    }
    function BackToUser() {
        Log.Info("Back to user...");
        wkf.controller.AllowRebuild(true);
        if (!wkf.controller.DoAction(wkf.enums.actions.sentBack)) {
            Process.PreventApproval();
        }
    }
    function DeleteExpenseReport() {
        Log.Info("Delete expense report...");
        if (!wkf.controller.DoAction(wkf.enums.actions.deleted)) {
            Process.PreventApproval();
        }
    }
    function ForwardExpenseReport() {
        Log.Info("Forward expense report...");
        if (!wkf.controller.DoAction(wkf.enums.actions.forward)) {
            Process.PreventApproval();
        }
    }
    function RequestFurtherApproval() {
        Log.Info("Request further approval");
        if (!wkf.controller.DoAction(wkf.enums.actions.requestFurtherApproval)) {
            Process.PreventApproval();
        }
    }
    function AtLeastOneDuplicateIsDifferent() {
        // ExpenseID (ExpenseMSNEX) of expense that have at least one possible duplicate are write inside technicalData
        var technicalData = JSON.parse(Data.GetValue("TechnicalData__"));
        var allExpenseNumberAlreadyCheck = technicalData.hasOwnProperty('expenseAlreadyViewAsDuplicate') ? technicalData.expenseAlreadyViewAsDuplicate : [];
        var possibleNewDuplicates = [];
        Sys.Helpers.Data.ForEachTableItem("ExpensesTable__", function (expense) {
            //This expense was not possible duplicate when loading client side ?
            if (allExpenseNumberAlreadyCheck.indexOf(expense.GetValue("ExpenseMSNEX__").toString()) === -1) {
                possibleNewDuplicates.push(expense);
            }
        });
        return Lib.Expense.Report.DuplicateCheck.IsDuplicateDetectedInList(possibleNewDuplicates);
    }
    function ExpenseReportSubmissionFromMobile(expensesList) {
        function LoadUserProperties() {
            return Sys.Helpers.Promise.Create(function (resolve) {
                Log.Info("Loading user properties");
                Lib.P2P.UserProperties.QueryValues(Lib.P2P.GetValidatorOrOwner().GetValue("login"))
                    .Then(function (UserPropertiesValues) {
                    var allowedCompanyCodes = UserPropertiesValues.GetAllowedCompanyCodes().split("\n");
                    var companyCode = Data.GetValue("CompanyCode__");
                    Data.SetValue("UserNumber__", UserPropertiesValues.UserNumber__);
                    if (!companyCode) {
                        companyCode = UserPropertiesValues.CompanyCode__;
                        if (!companyCode && allowedCompanyCodes.length) {
                            companyCode = allowedCompanyCodes[0];
                        }
                    }
                    else {
                        // Reset CompanyCode__ in order to let the framework to check allowed table value in next CompanyCode__.SetValue(companyCode)
                        Data.SetValue("CompanyCode__", "");
                    }
                    Data.SetValue("CompanyCode__", companyCode);
                    Lib.Expense.Report.LoadCompanyCodesValue()
                        .Then(function () {
                        Log.Info("User properties loaded");
                        resolve();
                    });
                });
            });
        }
        function LoadExpenseReportTypeTable() {
            return Sys.Helpers.Promise.Create(function (resolve) {
                Log.Info("Expense report type table loading");
                if (Data.GetValue("ExpenseReportStatus__") === "Draft") {
                    Lib.Expense.Report.QueryExpenseReportType(Data.GetValue("CompanyCode__"))
                        .Then(function (records) {
                        if (records.length === 1) {
                            Data.SetValue("ExpenseReportTypeName__", records[0].ExpenseReportTypeName__);
                            Data.SetValue("ExpenseReportTypeID__", records[0].ID__);
                        }
                        else if (records.length > 1) {
                            // > 1 in case no ERT is defined
                            var defaultID_1 = Data.GetValue("ExpenseReportTypeID__");
                            var defaultType = Sys.Helpers.Array.Find(records, function (type) {
                                return type.ID__ == defaultID_1;
                            });
                            if (defaultType !== undefined) {
                                Data.SetValue("ExpenseReportTypeName__", defaultType.ExpenseReportTypeName__);
                            }
                            else {
                                // No need to clear the ExpenseReportTypeID__, since ExpenseReportTypeName__ required
                                Log.Warn("The default Expense Report type specified by the admin doesn't exist OR none is specified.");
                            }
                        }
                        Log.Info("Expense report type table loaded (draft state)");
                        resolve();
                    });
                }
                else {
                    Log.Info("Expense report type table loaded (nothing done)");
                    resolve();
                }
            });
        }
        function SetUserBaseInfo() {
            return Sys.Helpers.Promise.Create(function (resolve) {
                if (!Data.GetValue("User__") && !Data.GetValue("UserName__")) {
                    Data.SetValue("User__", Lib.P2P.GetValidatorOrOwner().GetValue("login"));
                    Data.SetValue("UserName__", Lib.P2P.GetValidatorOrOwner().GetValue("displayname"));
                }
                resolve();
            });
        }
        function InitWorkflow() {
            Log.Info("Initializing workflow");
            var wkf = Lib.Expense.Report.Workflow;
            wkf.controller.Define(wkf.parameters);
            RefreshWorkflow();
            Log.Info("Initialization of workflow done");
        }
        function RefreshExpensesTable() {
            return Sys.Helpers.Promise.Create(function (resolve) {
                Log.Info("Refreshing of expenses table");
                // TODO => Here the chain of promise is broken (any rejection of this method is lost)
                Lib.Expense.Report.FillExpensesTable(expensesList);
                Log.Info("Refreshing of expenses table done");
                resolve();
            });
        }
        function RefreshWorkflow() {
            var wkf = Lib.Expense.Report.Workflow;
            if (wkf.controller.GetTableIndex() === 0) {
                wkf.controller.SetRolesSequence([
                    wkf.enums.roles.user,
                    wkf.enums.roles.manager,
                    wkf.enums.roles.controller
                ]);
            }
            else if (wkf.controller.RebuildAllowed()) {
                // The workflow can be changed before BackToUser,
                wkf.controller.Rebuild();
            }
        }
        SetUserBaseInfo()
            .Then(LoadUserProperties)
            .Then(LoadExpenseReportTypeTable)
            .Then(RefreshExpensesTable)
            .Then(InitWorkflow)
            .Then(function () {
            var err = Lib.Expense.Report.Workflow.parameters.getLastError();
            if (err != null) {
                if (err instanceof Sys.WorkflowEngine.Error) {
                    Lib.CommonDialog.NextAlert.Define("_Expense report creation error", "_Workflow error from mobile", { isError: true, behaviorName: "onUnexpectedError" });
                    Process.PreventApproval();
                }
                else if (err instanceof Sys.WorkflowEngine.ErrorNoPopUp) {
                    Process.PreventApproval();
                }
                else {
                    Lib.Expense.OnUnexpectedError(err);
                    // Process.PreventApproval(); is done in OnUnexpectedError
                }
                var contributor = Lib.Expense.Report.Workflow.controller.GetContributorAt(0);
                if (contributor) {
                    Lib.Expense.Report.Workflow.SendEmailNotification(contributor, "_Expense report failed", "Expense_Email_ExpenseReportError.htm");
                    Lib.Expense.Report.Workflow.NotifyEndOfContributionOnMobile(contributor);
                }
                else {
                    Log.Warn("No error notification mail sent: contributor is null");
                }
            }
            else {
                Process.RecallScript("PostValidation_Post", true);
            }
        });
    }
    Variable.SetValueAsString("LastActionName", currentName);
    /** ******** **/
    /** RUN PART **/
    /** ******** **/
    Lib.Expense.InitTechnicalFields();
    Lib.P2P.SetTablesToIndex(["ExpensesTable__", "ReportWorkflow__"]);
    if (currentName == "" && currentAction == "") {
        if (Data.GetValue("State") == 50) {
            if (Sys.Helpers.Data.IsTrue(Variable.GetValueAsString("SubmittedFromMobileApp"))) {
                var expenses = void 0;
                try {
                    expenses = JSON.parse(Variable.GetValueAsString("SelectedExpenseIDs"));
                    ExpenseReportSubmissionFromMobile(expenses);
                }
                catch (_a) {
                    Lib.CommonDialog.NextAlert.Define("_No expenses in expense report title", "_No expenses to submit in expense report", { isError: true });
                }
            }
            else {
                Process.PreventApproval();
            }
        }
    }
    else if (currentName === "PostValidation_Post") {
        SubmitExpenseReport();
    }
    else if (currentName !== "" && currentAction !== "") {
        if (currentAction === "approve_asynchronous" || currentAction === "approve") {
            Lib.CommonDialog.NextAlert.Reset();
            switch (currentName) {
                case "save":
                    break;
                case "Save":
                    Save();
                    break;
                case "SubmitExpenses":
                    if (Data.IsFormInError()) {
                        Log.Info("Form in error.");
                    }
                    else if (Data.GetValue("ExpenseReportStatus__") === "To control") {
                        AtLeastOneDuplicateIsDifferent().
                            Then(function (result) {
                            //At least one duplicate wasn't exist at loading client side
                            if (result) {
                                Log.Warn("Some expense are now Duplicate but was not at client loading of the flexible form");
                                Lib.CommonDialog.NextAlert.Define("_Duplicate Expense not validate", "_Duplicate Expense not validate warning", { isError: false });
                                Process.PreventApproval();
                            }
                            else {
                                SubmitExpenseReport();
                            }
                        })
                            .Catch(function (error) {
                            Log.Error("An error occurred while checking expense duplicate: " + error);
                            Lib.CommonDialog.NextAlert.Define("_Error during validate duplicate title", "_Error during validate duplicate description", { isError: true });
                            Process.PreventApproval();
                        });
                    }
                    else {
                        SubmitExpenseReport();
                    }
                    break;
                case "BackToUser":
                    BackToUser();
                    break;
                case "DeleteReport":
                    DeleteExpenseReport();
                    break;
                case "Approve_Forward":
                    if (!Data.IsFormInError()) {
                        ForwardExpenseReport();
                    }
                    break;
                case "Approve_RequestFurtherApproval":
                    if (!Data.IsFormInError()) {
                        RequestFurtherApproval();
                    }
                    break;
                default:
                    Lib.Expense.OnUnknownAction(currentAction, currentName);
                    break;
            }
        }
        else if (currentAction !== "reprocess" && currentAction !== "reprocess_asynchronous" && currentAction !== "save") {
            Lib.Expense.OnUnknownAction(currentAction, currentName);
        }
    }
    else {
        Lib.Expense.OnUnknownAction(currentAction, currentName);
    }
})(Validation || (Validation = {}));
