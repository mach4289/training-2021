{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1496,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1496
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1496
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "0%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 0,
													"width": 100,
													"height": 100
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning"
											},
											"*": {
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner__",
																						"htmlContent": "Expense report",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 14
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 15,
													"*": {
														"ExpenseReportInformation": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "Exp_information.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ExpenseReportInformation",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 16,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TotalAmount__": "LabelTotalAmount__",
																			"LabelTotalAmount__": "TotalAmount__",
																			"Description__": "LabelDescription__",
																			"LabelDescription__": "Description__",
																			"CC_Currency__": "LabelCC_Currency__",
																			"LabelCC_Currency__": "CC_Currency__",
																			"ExpenseReportNumber__": "LabelExpenseReportNumber__",
																			"LabelExpenseReportNumber__": "ExpenseReportNumber__",
																			"ExpenseReportStatus__": "LabelExpenseReportStatus__",
																			"LabelExpenseReportStatus__": "ExpenseReportStatus__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"User__": "LabelUser__",
																			"LabelUser__": "User__",
																			"UserName__": "LabelUserName__",
																			"LabelUserName__": "UserName__",
																			"SubmissionDate__": "LabelSubmissionDate__",
																			"LabelSubmissionDate__": "SubmissionDate__",
																			"VIRuidEx__": "LabelVIRuidEx__",
																			"LabelVIRuidEx__": "VIRuidEx__",
																			"UserNumber__": "LabelUserNumber__",
																			"LabelUserNumber__": "UserNumber__",
																			"RefundableAmount__": "LabelRefundableAmount__",
																			"LabelRefundableAmount__": "RefundableAmount__",
																			"NonRefundableAmount__": "LabelNonRefundableAmount__",
																			"LabelNonRefundableAmount__": "NonRefundableAmount__",
																			"ExpenseReportTypeName__": "LabelExpenseReportTypeName__",
																			"LabelExpenseReportTypeName__": "ExpenseReportTypeName__",
																			"ExpenseReportTypeID__": "LabelExpenseReportTypeID__",
																			"LabelExpenseReportTypeID__": "ExpenseReportTypeID__",
																			"OriginalOwnerId__": "LabelOriginalOwnerId__",
																			"LabelOriginalOwnerId__": "OriginalOwnerId__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__"
																		},
																		"version": 0
																	},
																	"stamp": 17,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TotalAmount__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelTotalAmount__": {
																						"line": 13,
																						"column": 1
																					},
																					"Description__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelDescription__": {
																						"line": 12,
																						"column": 1
																					},
																					"CC_Currency__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelCC_Currency__": {
																						"line": 9,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 1,
																						"column": 1
																					},
																					"ExpenseReportNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelExpenseReportNumber__": {
																						"line": 2,
																						"column": 1
																					},
																					"ExpenseReportStatus__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelExpenseReportStatus__": {
																						"line": 15,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 8,
																						"column": 1
																					},
																					"User__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelUser__": {
																						"line": 3,
																						"column": 1
																					},
																					"UserName__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelUserName__": {
																						"line": 4,
																						"column": 1
																					},
																					"SubmissionDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelSubmissionDate__": {
																						"line": 6,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 16,
																						"column": 1
																					},
																					"VIRuidEx__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelVIRuidEx__": {
																						"line": 17,
																						"column": 1
																					},
																					"UserNumber__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelUserNumber__": {
																						"line": 5,
																						"column": 1
																					},
																					"RefundableAmount__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelRefundableAmount__": {
																						"line": 14,
																						"column": 1
																					},
																					"NonRefundableAmount__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelNonRefundableAmount__": {
																						"line": 7,
																						"column": 1
																					},
																					"ExpenseReportTypeName__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelExpenseReportTypeName__": {
																						"line": 11,
																						"column": 1
																					},
																					"ExpenseReportTypeID__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelExpenseReportTypeID__": {
																						"line": 10,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 18,
																						"column": 1
																					}
																				},
																				"lines": 18,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 18,
																			"*": {
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 19
																				},
																				"LabelExpenseReportNumber__": {
																					"type": "Label",
																					"data": [
																						"ExpenseReportNumber__"
																					],
																					"options": {
																						"label": "_ExpenseReportNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 20
																				},
																				"ExpenseReportNumber__": {
																					"type": "ShortText",
																					"data": [
																						"ExpenseReportNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseReportNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 21
																				},
																				"LabelUser__": {
																					"type": "Label",
																					"data": [
																						"User__"
																					],
																					"options": {
																						"label": "_User",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 22
																				},
																				"User__": {
																					"type": "ShortText",
																					"data": [
																						"User__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_User",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 23
																				},
																				"LabelUserName__": {
																					"type": "Label",
																					"data": [
																						"UserName__"
																					],
																					"options": {
																						"label": "_UserName",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 24
																				},
																				"UserName__": {
																					"type": "ShortText",
																					"data": [
																						"UserName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_UserName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 25
																				},
																				"LabelUserNumber__": {
																					"type": "Label",
																					"data": [
																						"UserNumber__"
																					],
																					"options": {
																						"label": "_UserNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 26
																				},
																				"UserNumber__": {
																					"type": "ShortText",
																					"data": [
																						"UserNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_UserNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 27
																				},
																				"LabelSubmissionDate__": {
																					"type": "Label",
																					"data": [
																						"SubmissionDate__"
																					],
																					"options": {
																						"label": "_SubmissionDate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 28
																				},
																				"SubmissionDate__": {
																					"type": "DateTime",
																					"data": [
																						"SubmissionDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_SubmissionDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 29
																				},
																				"LabelNonRefundableAmount__": {
																					"type": "Label",
																					"data": [
																						"NonRefundableAmount__"
																					],
																					"options": {
																						"label": "_NonRefundableAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 30
																				},
																				"NonRefundableAmount__": {
																					"type": "Decimal",
																					"data": [
																						"NonRefundableAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_NonRefundableAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 31
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 32
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"readonly": true,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 33
																				},
																				"LabelCC_Currency__": {
																					"type": "Label",
																					"data": [
																						"CC_Currency__"
																					],
																					"options": {
																						"label": "_CC_Currency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 34
																				},
																				"CC_Currency__": {
																					"type": "ShortText",
																					"data": [
																						"CC_Currency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_CC_Currency",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 35
																				},
																				"LabelDescription__": {
																					"type": "Label",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"label": "_ExpenseReportDescription",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"Description__": {
																					"type": "LongText",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseReportDescription",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"minNbLines": 3
																					},
																					"stamp": 37
																				},
																				"LabelExpenseReportTypeName__": {
																					"type": "Label",
																					"data": [
																						"ExpenseReportTypeName__"
																					],
																					"options": {
																						"label": "_ExpenseReportTypeName",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"ExpenseReportTypeName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ExpenseReportTypeName__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_ExpenseReportTypeName",
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 39
																				},
																				"LabelExpenseReportStatus__": {
																					"type": "Label",
																					"data": [
																						"ExpenseReportStatus__"
																					],
																					"options": {
																						"label": "_ExpenseReportStatus",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 40
																				},
																				"ExpenseReportStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"ExpenseReportStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_To approve",
																							"2": "_To control",
																							"3": "_Validated",
																							"4": "_Deleted"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "To approve",
																							"2": "To control",
																							"3": "Validated",
																							"4": "Deleted"
																						},
																						"label": "_ExpenseReportStatus",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 41
																				},
																				"LabelTotalAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalAmount__"
																					],
																					"options": {
																						"label": "_TotalAmount",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"TotalAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 43
																				},
																				"LabelRefundableAmount__": {
																					"type": "Label",
																					"data": [
																						"RefundableAmount__"
																					],
																					"options": {
																						"label": "_RefundableAmount",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"RefundableAmount__": {
																					"type": "Decimal",
																					"data": [
																						"RefundableAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_RefundableAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 45
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"LabelVIRuidEx__": {
																					"type": "Label",
																					"data": [
																						"VIRuidEx__"
																					],
																					"options": {
																						"label": "_VIRuidEx",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 47
																				},
																				"VIRuidEx__": {
																					"type": "ShortText",
																					"data": [
																						"VIRuidEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VIRuidEx",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 200,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 48
																				},
																				"ExpenseReportTypeID__": {
																					"type": "ShortText",
																					"data": [
																						"ExpenseReportTypeID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseReportTypeID",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 49
																				},
																				"LabelExpenseReportTypeID__": {
																					"type": "Label",
																					"data": [
																						"ExpenseReportTypeID__"
																					],
																					"options": {
																						"label": "_ExpenseReportTypeID",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 50
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 224
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"length": 5000,
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 225
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 51,
													"*": {
														"Expenses": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "50",
																"iconURL": "Exp_table.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Expenses",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 52,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BrowseExpense__": "LabelBrowseExpense__",
																			"LabelBrowseExpense__": "BrowseExpense__"
																		},
																		"version": 0
																	},
																	"stamp": 53,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ExpensesTable__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 1,
																						"column": 1
																					},
																					"AddExpense__": {
																						"line": 4,
																						"column": 1
																					},
																					"BrowseExpense__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelBrowseExpense__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 6,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 54,
																			"*": {
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 55
																				},
																				"ExpensesTable__": {
																					"type": "Table",
																					"data": [
																						"ExpensesTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 21,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ExpensesTable",
																						"subsection": null,
																						"readonly": false
																					},
																					"stamp": 56,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 57,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 58
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 59
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 60,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "20"
																									},
																									"stamp": 227,
																									"data": [],
																									"*": {
																										"Duplicate__": {
																											"type": "Label",
																											"data": [
																												"Duplicate__"
																											],
																											"options": {
																												"label": "",
																												"minwidth": "20"
																											},
																											"stamp": 228,
																											"position": [
																												"_Button"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 61,
																									"data": [],
																									"*": {
																										"ExpenseNumber__": {
																											"type": "Label",
																											"data": [
																												"ExpenseNumber__"
																											],
																											"options": {
																												"label": "_ExpenseNumber",
																												"version": 0
																											},
																											"stamp": 62,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"Date__": {
																											"type": "Label",
																											"data": [
																												"Date__"
																											],
																											"options": {
																												"label": "_Date",
																												"version": 0
																											},
																											"stamp": 64,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 65,
																									"data": [],
																									"*": {
																										"ExpenseType__": {
																											"type": "Label",
																											"data": [
																												"ExpenseType__"
																											],
																											"options": {
																												"label": "_ExpenseType",
																												"version": 0
																											},
																											"stamp": 66,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 67,
																									"data": [],
																									"*": {
																										"ExpenseDescription__": {
																											"type": "Label",
																											"data": [
																												"ExpenseDescription__"
																											],
																											"options": {
																												"label": "_ExpenseDescription",
																												"version": 0
																											},
																											"stamp": 68,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 69,
																									"data": [],
																									"*": {
																										"VendorName__": {
																											"type": "Label",
																											"data": [
																												"VendorName__"
																											],
																											"options": {
																												"label": "_VendorName",
																												"version": 0
																											},
																											"stamp": 70,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 71,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Label",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"label": "_Amount",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 72,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 73,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"label": "",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 74,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75,
																									"data": [],
																									"*": {
																										"LocalAmount__": {
																											"type": "Label",
																											"data": [
																												"LocalAmount__"
																											],
																											"options": {
																												"label": "_LocalAmount",
																												"version": 0
																											},
																											"stamp": 76,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 77,
																									"data": [],
																									"*": {
																										"LocalCurrency__": {
																											"type": "Label",
																											"data": [
																												"LocalCurrency__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 78,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 79,
																									"data": [],
																									"*": {
																										"CurrencyRate__": {
																											"type": "Label",
																											"data": [
																												"CurrencyRate__"
																											],
																											"options": {
																												"label": "_CurrencyRate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 80,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 81,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "Label",
																											"data": [
																												"CompanyCode__"
																											],
																											"options": {
																												"label": "CompanyCode",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 82,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 83,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "Label",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"label": "_GL Account",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 84,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 85,
																									"data": [],
																									"*": {
																										"ExpenseMSNEX__": {
																											"type": "Label",
																											"data": [
																												"ExpenseMSNEX__"
																											],
																											"options": {
																												"label": "_ExpenseMSNEX",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 86,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 87,
																									"data": [],
																									"*": {
																										"Refundable__": {
																											"type": "Label",
																											"data": [
																												"Refundable__"
																											],
																											"options": {
																												"label": "_Refundable",
																												"version": 0
																											},
																											"stamp": 88,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 89,
																									"data": [],
																									"*": {
																										"CostCenterId__": {
																											"type": "Label",
																											"data": [
																												"CostCenterId__"
																											],
																											"options": {
																												"label": "_CostCenterId",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 90,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 91,
																									"data": [],
																									"*": {
																										"Billable__": {
																											"type": "Label",
																											"data": [
																												"Billable__"
																											],
																											"options": {
																												"label": "_Billable",
																												"version": 0
																											},
																											"stamp": 92,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 93,
																									"data": [],
																									"*": {
																										"OriginalOwnerId__": {
																											"type": "Label",
																											"data": [
																												"OriginalOwnerId__"
																											],
																											"options": {
																												"label": "_OriginalOwnerId",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 94,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 213,
																									"data": [],
																									"*": {
																										"OwnerName__": {
																											"type": "Label",
																											"data": [
																												"OwnerName__"
																											],
																											"options": {
																												"label": "_OwnerName",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 214,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 217,
																									"data": [],
																									"*": {
																										"ReimbursableLocalAmount__": {
																											"type": "Label",
																											"data": [
																												"ReimbursableLocalAmount__"
																											],
																											"options": {
																												"label": "_ReimbursableLocalAmount",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 218,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 221,
																									"data": [],
																									"*": {
																										"NonReimbursableLocalAmount__": {
																											"type": "Label",
																											"data": [
																												"NonReimbursableLocalAmount__"
																											],
																											"options": {
																												"label": "_NonReimbursableLocalAmount",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 222,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 95,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 226,
																									"data": [],
																									"*": {
																										"Duplicate__": {
																											"type": "FormButton",
																											"data": [
																												"Duplicate__"
																											],
																											"options": {
																												"buttonLabel": "",
																												"label": "",
																												"width": "20",
																												"textPosition": "text-right",
																												"textSize": "MS",
																												"textStyle": "default",
																												"textColor": "default",
																												"nextprocess": {
																													"processName": "AP - Application Settings Wizard",
																													"attachmentsMode": "all",
																													"willBeChild": true,
																													"returnToOriginalUrl": false
																												},
																												"urlImage": "",
																												"urlImageOverlay": "",
																												"style": 4,
																												"action": "none",
																												"url": ""
																											},
																											"stamp": 229,
																											"position": [
																												"_Button"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 96,
																									"data": [],
																									"*": {
																										"ExpenseNumber__": {
																											"type": "ShortText",
																											"data": [
																												"ExpenseNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ExpenseNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 97,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 98,
																									"data": [],
																									"*": {
																										"Date__": {
																											"type": "DateTime",
																											"data": [
																												"Date__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_Date",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"readonly": true
																											},
																											"stamp": 99,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 100,
																									"data": [],
																									"*": {
																										"ExpenseType__": {
																											"type": "ShortText",
																											"data": [
																												"ExpenseType__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ExpenseType",
																												"activable": true,
																												"width": 240,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 101,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 102,
																									"data": [],
																									"*": {
																										"ExpenseDescription__": {
																											"type": "LongText",
																											"data": [
																												"ExpenseDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ExpenseDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"minNbLines": 1,
																												"numberOfLines": 999
																											},
																											"stamp": 103,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 104,
																									"data": [],
																									"*": {
																										"VendorName__": {
																											"type": "ShortText",
																											"data": [
																												"VendorName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_VendorName",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 105,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 106,
																									"data": [],
																									"*": {
																										"Amount__": {
																											"type": "Decimal",
																											"data": [
																												"Amount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 107,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 108,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 109,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 110,
																									"data": [],
																									"*": {
																										"LocalAmount__": {
																											"type": "Decimal",
																											"data": [
																												"LocalAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_LocalAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true
																											},
																											"stamp": 111,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 112,
																									"data": [],
																									"*": {
																										"LocalCurrency__": {
																											"type": "ShortText",
																											"data": [
																												"LocalCurrency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 113,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 114,
																									"data": [],
																									"*": {
																										"CurrencyRate__": {
																											"type": "Decimal",
																											"data": [
																												"CurrencyRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CurrencyRate",
																												"precision_internal": 5,
																												"precision_current": 5,
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 115,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 116,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "ShortText",
																											"data": [
																												"CompanyCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "CompanyCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"hidden": true
																											},
																											"stamp": 117,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 118,
																									"data": [],
																									"*": {
																										"GLAccount__": {
																											"type": "ShortText",
																											"data": [
																												"GLAccount__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_GL Account",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 30,
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 119,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 120,
																									"data": [],
																									"*": {
																										"ExpenseMSNEX__": {
																											"type": "ShortText",
																											"data": [
																												"ExpenseMSNEX__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ExpenseMSNEX",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"hidden": true
																											},
																											"stamp": 121,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 122,
																									"data": [],
																									"*": {
																										"Refundable__": {
																											"type": "CheckBox",
																											"data": [
																												"Refundable__"
																											],
																											"options": {
																												"label": "_Refundable",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"readonly": true
																											},
																											"stamp": 123,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 124,
																									"data": [],
																									"*": {
																										"CostCenterId__": {
																											"type": "ShortText",
																											"data": [
																												"CostCenterId__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CostCenterId",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"hidden": true
																											},
																											"stamp": 125,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 126,
																									"data": [],
																									"*": {
																										"Billable__": {
																											"type": "CheckBox",
																											"data": [
																												"Billable__"
																											],
																											"options": {
																												"label": "_Billable",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 127,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 128,
																									"data": [],
																									"*": {
																										"OriginalOwnerId__": {
																											"type": "ShortText",
																											"data": [
																												"OriginalOwnerId__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_OriginalOwnerId",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"length": 500
																											},
																											"stamp": 129,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 212,
																									"data": [],
																									"*": {
																										"OwnerName__": {
																											"type": "ShortText",
																											"data": [
																												"OwnerName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_OwnerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 215,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 216,
																									"data": [],
																									"*": {
																										"ReimbursableLocalAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ReimbursableLocalAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ReimbursableLocalAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 219,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 220,
																									"data": [],
																									"*": {
																										"NonReimbursableLocalAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NonReimbursableLocalAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_NonReimbursableLocalAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 223,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 130
																				},
																				"AddExpense__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_AddExpense",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImage": "Exp_Report_add_Expense_button.png",
																						"style": 4
																					},
																					"stamp": 131
																				},
																				"LabelBrowseExpense__": {
																					"type": "Label",
																					"data": [
																						"BrowseExpense__"
																					],
																					"options": {
																						"label": "_BrowseExpense",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 132
																				},
																				"BrowseExpense__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"BrowseExpense__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_BrowseExpense",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"hidden": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"MultipleSelection": true
																					},
																					"stamp": 133
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 134
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 135,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Documents",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 136
																}
															},
															"stamp": 137,
															"data": []
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 138,
													"*": {
														"ApprovalWorkflow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "Report_workflow.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ApprovalWorkflow",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 139,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__"
																		},
																		"version": 0
																	},
																	"stamp": 140,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line4__": {
																						"line": 1,
																						"column": 1
																					},
																					"Comments__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 3,
																						"column": 1
																					},
																					"ReportWorkflow__": {
																						"line": 4,
																						"column": 1
																					},
																					"ComputingWorkflow__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelComputingWorkflow__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line6__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 141,
																			"*": {
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 142
																				},
																				"Comments__": {
																					"type": "LongText",
																					"data": [
																						"Comments__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"numberOfLines": 3,
																						"label": "_Comments",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 143
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 144
																				},
																				"ReportWorkflow__": {
																					"type": "Table",
																					"data": [
																						"ReportWorkflow__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 10,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ReportWorkflow",
																						"readonly": true
																					},
																					"stamp": 145,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 146,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 147
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 148
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 149,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 150,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 151,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 152,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "Label",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"version": 0
																											},
																											"stamp": 153,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 154,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "Label",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"version": 0
																											},
																											"stamp": 155,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 156,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "Label",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"label": "_WRKFDate",
																												"version": 0
																											},
																											"stamp": 157,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 158,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "Label",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"version": 0
																											},
																											"stamp": 159,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 160,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "Label",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"version": 0
																											},
																											"stamp": 161,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 162,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"version": 0
																											},
																											"stamp": 163,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 164,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"version": 0
																											},
																											"stamp": 165,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 231,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "Label",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"label": "_WRKFActualApprover",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 232,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "Label",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"label": "_WRKFRequestDateTime",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 236,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 166,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 168,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFUserName",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 170,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFRole",
																												"activable": true,
																												"width": 160,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 172,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFDate",
																												"activable": true,
																												"width": "150",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 174,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 175,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "LongText",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"numberOfLines": 5,
																												"label": "_WRKFComment",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 176,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 177,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFAction",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 178,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 179,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFIndex",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 180,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 181,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 182,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true
																									},
																									"stamp": 230,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFActualApprover",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 80,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 233,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true
																									},
																									"stamp": 234,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFRequestDateTime",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 237,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelComputingWorkflow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 183
																				},
																				"ComputingWorkflow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"width": "10",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"><div>",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 184
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 185
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Document data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 0,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {},
																				"colspans": []
																			},
																			"data": [
																				"fields"
																			],
																			"*": {},
																			"stamp": 186
																		}
																	},
																	"stamp": 187,
																	"data": []
																}
															},
															"stamp": 188,
															"data": []
														}
													},
													"stamp": 189,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 190,
															"data": []
														}
													},
													"stamp": 191,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 192,
															"data": []
														}
													},
													"stamp": 193,
													"data": []
												}
											},
											"stamp": 194,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 100,
													"width": 0,
													"height": 100
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"label": "_Document Preview",
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 195
																}
															},
															"stamp": 196,
															"data": []
														}
													},
													"stamp": 197,
													"data": []
												}
											},
											"stamp": 198,
											"data": []
										}
									},
									"stamp": 199,
									"data": []
								}
							},
							"stamp": 200,
							"data": []
						}
					},
					"stamp": 201,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1496
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 202,
							"data": []
						},
						"DeleteReport": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_DeleteReport",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"action": "approve",
								"version": 0
							},
							"stamp": 203
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Saveandquit",
								"action": "approve",
								"submit": true,
								"version": 0
							},
							"stamp": 204,
							"data": []
						},
						"DownloadCrystalReportsDataFile": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_DownloadCrystalReportsDataFile",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 205
						},
						"Retry": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Retry",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 206
						},
						"PreviewExpenseReport": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_PreviewExpenseReport",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"action": "none"
							},
							"stamp": 207
						},
						"BackToUser": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_BackToUser",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 208
						},
						"SubmitExpenses": {
							"type": "SubmitButton",
							"options": {
								"label": "_SubmitExpenses",
								"action": "none",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"style": 1,
								"url": "",
								"urlImageOverlay": ""
							},
							"stamp": 209,
							"data": []
						}
					},
					"stamp": 210,
					"data": []
				}
			},
			"stamp": 211,
			"data": []
		}
	},
	"stamps": 237,
	"data": []
}