///#GLOBALS Lib
/** **************** **/
/** Global variables **/
/** **************** **/
Controls.ERP__.Wait(true);
var g_apParameters = Sys.Parameters.GetInstance("AP");
var g_p2pParameters = Sys.Parameters.GetInstance("P2P");
var g_topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
// Workflow
var g_selectedAddApprover = null;
var g_bApproverAdded = false;
// predefinitions - objects are implemented later in the code
var LayoutHelper;
var RequestTeaching;
//stack taxCode to get taxRate in one call
var g_taxCodes = {};
//save currency to have old value available when it si changed
var g_invoiceCurrency = "";
/** *************** **/
/** GENERIC HELPERS **/
/** *************** **/
/**
 * Returns a reliable value of Comment__ field content.
 * On some browsers (IE<10), the placeholder could be returned as its value.
 */
var g_CommentPlaceHolder = Language.Translate("_Enter your comment...");

function getReliableComment()
{
	var commentValue = Controls.Comment__.GetValue();
	if (commentValue === g_CommentPlaceHolder)
	{
		commentValue = "";
	}
	return commentValue;
}

function decimalToString(decimalValue)
{
	// Convert a decimal value to a string formatted in the culture of the current user
	// Use a decimal control to do this
	var oldValue = Controls.NetAmount__.GetValue();
	Controls.NetAmount__.SetValue(decimalValue);
	var str = Controls.NetAmount__.GetText();
	Controls.NetAmount__.SetValue(oldValue);
	return str;
}

function resetParameters()
{
	var setParametersCallback = function (lastAlertLevel, lastTouchlessEnabled, templateName, lineItemsImporterMapping)
	{
		Data.SetValue("DuplicateCheckAlertLevel__", lastAlertLevel);
		Data.SetValue("TouchlessEnabled__", lastTouchlessEnabled && lastTouchlessEnabled !== "0");
		Data.SetValue("CodingTemplate__", templateName);
		Variable.SetValueAsString("LineItemsImporterMapping", lineItemsImporterMapping ? lineItemsImporterMapping : "");
	};
	var companyCode = Data.GetValue("CompanyCode__");
	var vendorNumber = Data.GetValue("VendorNumber__");
	Lib.AP.Parameters.GetParameters(companyCode, vendorNumber, setParametersCallback);
}
/** ************* **/
/** LAYOUT HELPER **/
/** ************* **/
/**
 * @class An helper class to manage the several kinds of line item
 */
var InvoiceLineItem = {
	/**
	 * NON-PO lines count
	 */
	iNonPOLinesCount: 0,
	/**
	 * GRIV PO lines count
	 */
	iGRIVPOLinesCount: 0,
	/**
	 * Index where new line item will be inserted
	 */
	iIndexForNewItem: -1,
	subFocusKey:
	{
		focused: false,
		// other properties should match existing LineItems__ column
		OrderNumber__: null,
		GoodsReceipt__: null,
		ItemNumber__: null
	},
	dispatchKey:
	{
		OrderNumber__: null,
		GoodsReceipt__: null,
		ItemNumber__: null,
		PriceCondition__: null
	},
	/**
	 * Initialize the counter of Non-PO line item
	 */
	InitializeNonPOLineCount: function ()
	{
		var table = Data.GetTable("LineItems__");
		for (var i = 0; i < table.GetItemCount(); i++)
		{
			var item = table.GetItem(i);
			if (InvoiceLineItem.IsGLLineItem(item))
			{
				InvoiceLineItem.iNonPOLinesCount++;
			}
		}
	},
	/**
	 * Initialize the counter of GRIV PO line item
	 */
	InitializeGRIVPOLineCount: function ()
	{
		var table = Data.GetTable("LineItems__");
		for (var i = 0; i < table.GetItemCount(); i++)
		{
			var item = table.GetItem(i);
			if (item && InvoiceLineItem.IsGRIVPOLineItem(item))
			{
				InvoiceLineItem.iGRIVPOLinesCount++;
				if (InvoiceLineItem.iGRIVPOLinesCount === 1)
				{
					LayoutHelper.SwitchToGRIV();
				}
			}
		}
	},
	/**
	 * Check if the line is a PO line item
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the LineType of the line is equal to PO
	 */
	IsPOLineItem: function (item)
	{
		if (!item.GetValue("LineType__"))
		{
			return Lib.AP.InvoiceType.isPOInvoice();
		}
		return Lib.P2P.InvoiceLineItem.IsPOLineItem(item);
	},
	/**
	 * Check if the line is a PO line item
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the LineType of the line is equal to PO
	 */
	IsPOGLLineItem: function (item)
	{
		if (!item.GetValue("LineType__"))
		{
			return Lib.AP.InvoiceType.isPOGLInvoice();
		}
		return Lib.P2P.InvoiceLineItem.IsPOGLLineItem(item);
	},
	/**
	 * Check if the line is a GL line item
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the LineType of the line is equal to GL
	 */
	IsGLLineItem: function (item)
	{
		if (!item.GetValue("LineType__"))
		{
			return Lib.AP.InvoiceType.isGLInvoice();
		}
		return Lib.P2P.InvoiceLineItem.IsGLLineItem(item);
	},
	/**
	 * Check if the line is a GL line item
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the LineType of the line is equal to GL
	 */
	IsGRIVPOLineItem: function (item)
	{
		return InvoiceLineItem.IsPOLineItem(item) && item.GetValue("GoodsReceipt__");
	},
	/**
	 * Check if the line is a GL line item
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the LineType of the line is equal to GL
	 */
	IsEmpty: function (item)
	{
		if (InvoiceLineItem.IsPOLineItem(item))
		{
			return InvoiceLineItem.IsPOLineItemEmpty(item);
		}
		else if (InvoiceLineItem.IsPOGLLineItem(item))
		{
			return InvoiceLineItem.IsPOGLLineItemEmpty(item);
		}
		// Non-PO Line item are managed like GL line item
		return InvoiceLineItem.IsGLLineItemEmpty(item);
	},
	IsGenericLineEmpty: function (item)
	{
		return Lib.P2P.InvoiceLineItem.IsEmpty(item);
	},
	/**
	 * Check if a GL line item is empty
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the line is empty
	 */
	IsGLLineItemEmpty: function (item)
	{
		return InvoiceLineItem.IsGenericLineEmpty(item) && !item.GetValue("GLAccount__") && !item.GetValue("CostCenter__");
	},
	/**
	 * Check if a PO line item is empty
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the line is empty
	 */
	IsPOLineItemEmpty: function (item)
	{
		return InvoiceLineItem.IsGenericLineEmpty(item) && !item.GetValue("OrderNumber__") && !item.GetValue("ItemNumber__") && !item.GetValue("Quantity__");
	},
	/**
	 * Check if a POGL line item is empty
	 * @param {Data} item The data of the line to test
	 * @return boolean True if the line is empty
	 */
	IsPOGLLineItemEmpty: function (item)
	{
		return InvoiceLineItem.IsGLLineItemEmpty(item) && InvoiceLineItem.IsPOLineItemEmpty(item);
	},
	ManageCodingsLayout: function (control, isPosted, isPo)
	{
		var isController = currentStepIsController();
		//Codings
		control.GLDescription__.SetReadOnly(true);
		control.CCDescription__.SetReadOnly(true);
		control.ProjectCodeDescription__.SetReadOnly(true);
		var codingsParameters = [
		{
			control: control.GLAccount__,
			parameter: "WorkflowReviewersCanModifyGLAccount"
		},
		{
			control: control.CostType__,
			parameter: "WorkflowReviewersCanModifyGLAccount"
		},
		{
			control: control.CostCenter__,
			parameter: "WorkflowReviewersCanModifyCostCenter"
		},
		{
			control: control.ProjectCode__,
			parameter: "WorkflowReviewersCanModifyProjectCode"
		},
		{
			control: control.BusinessArea__,
			parameter: "WorkflowReviewersCanModifyBusinessArea"
		},
		{
			control: control.InternalOrder__,
			parameter: "WorkflowReviewersCanModifyInternalOrder"
		},
		{
			control: control.WBSElement__,
			parameter: "WorkflowReviewersCanModifyWBSElement"
		},
		{
			control: control.Assignment__,
			parameter: "WorkflowReviewersCanModifyAssignments"
		},
		{
			control: control.TradingPartner__,
			parameter: "WorkflowReviewersCanModifyTradingPartner"
		},
		{
			control: control.CompanyCode__,
			parameter: "WorkflowReviewersCanModifyCompanyCode"
		}];
		for (var _i = 0, codingsParameters_1 = codingsParameters; _i < codingsParameters_1.length; _i++)
		{
			var coding = codingsParameters_1[_i];
			var readOnly = false;
			var acctAssCat = control.AcctAssCat__ ? control.AcctAssCat__.GetValue() : "";
			var multiAssignmentAllowed = isPo && Lib.ERP.IsSAP() && Lib.AP.SAP.IsMultiAccountAssignment(acctAssCat);
			var isPoReadOnly = isPo && !multiAssignmentAllowed;
			if (isPoReadOnly || isPosted || !coding.control.IsVisible())
			{
				readOnly = true;
			}
			else if (isController)
			{
				readOnly = g_apParameters.GetParameter(coding.parameter) !== "1";
			}
			Lib.AP.GetInvoiceDocumentLayout().SetReadOnly(coding.control, readOnly);
		}
	},
	/**
	 * Update the layout of the row at the specified index
	 * @param {Control} control The Table or Row on which apply the layout update
	 */
	SetGLLayout: function (control)
	{
		var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
		InvoiceLineItem.ManageCodingsLayout(control, isPosted, false);
		control.Description__.SetReadOnly(!currentStepCanModifyLineItems() || isPosted);
		control.Quantity__.SetReadOnly(true);
		control.OrderNumber__.SetRequired(false);
		control.Buyer__.SetReadOnly(true);
		control.Receiver__.SetReadOnly(true);
	},
	/**
	 * Update the layout of the row at the specified index
	 * @param {Control} control The Table or Row on which apply the layout update
	 */
	SetPOLayout: function (control)
	{
		var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
		var isSubsequentDoc = Data.GetValue("SubsequentDocument__");
		//Codings
		InvoiceLineItem.ManageCodingsLayout(control, isPosted, true);
		control.Quantity__.SetReadOnly(!currentStepCanModifyLineItems() || isPosted || isSubsequentDoc);
		control.OrderNumber__.SetRequired(!Controls.ManualLink__.IsChecked());
		control.Description__.SetReadOnly(true);
		control.Buyer__.SetReadOnly(true);
		control.Receiver__.SetReadOnly(true);
		if (Lib.P2P.InvoiceLineItem.IsAmountBasedRow(control))
		{
			control.Quantity__.Hide(true);
			control.ExpectedQuantity__.Hide(true);
		}
	},
	/**
	 * Update the layout of the row at the specified index
	 * @param {Control} control The Table or Row on which apply the layout update
	 */
	SetPOGLLayout: function (control)
	{
		var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
		var isSubsequentDoc = Data.GetValue("SubsequentDocument__");
		InvoiceLineItem.ManageCodingsLayout(control, isPosted, false);
		control.Quantity__.SetReadOnly(!currentStepCanModifyLineItems() || isPosted || isSubsequentDoc);
		control.Description__.SetReadOnly(isPosted);
		control.OrderNumber__.SetRequired(false);
		control.Buyer__.SetReadOnly(true);
		control.Receiver__.SetReadOnly(true);
	},
	/**
	 * Update the layout of the row at the specified index
	 * @param {Control} control The Table or Row on which apply the layout update
	 */
	SetConsignmentLayout: function (control)
	{
		var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
		//Codings
		InvoiceLineItem.ManageCodingsLayout(control, isPosted, true);
		control.Quantity__.SetReadOnly(!currentStepCanModifyLineItems() || isPosted);
		control.OrderNumber__.SetRequired(false);
		control.Description__.SetReadOnly(true);
		control.Buyer__.SetReadOnly(true);
		control.Receiver__.SetReadOnly(true);
		control.Amount__.SetReadOnly(!currentStepCanModifyLineItems() || isPosted);
	},
	/**
	 * Update the Order Number field style of the row at the specified index
	 * @param {Control} control The Table or Row on which apply the field style
	 */
	SetPOLink: function (control)
	{
		if (control.IsLocalPO__.GetValue())
		{
			control.OrderNumber__.DisplayAs(
			{
				type: "Link"
			});
		}
		else
		{
			control.OrderNumber__.DisplayAs(
			{
				type: ""
			});
		}
	},
	/**
	 * Get the dispatchKey
	 * @param {Item} item The item on which to get the dispatch key
	 * @returns {string} the dispatch key
	 */
	GetDispatchKey: function (item)
	{
		var key = "";
		if (item && InvoiceLineItem.IsPOLineItem(item))
		{
			for (var val in InvoiceLineItem.dispatchKey)
			{
				if (Object.prototype.hasOwnProperty.call(InvoiceLineItem.dispatchKey, val))
				{
					key += item.GetValue(val) + "_";
				}
			}
		}
		return key;
	},
	/**
	 * Get the dispatched lines total amount and quantity
	 * @returns {DispatchMap} a dispatch map (mapping dispatched lines to their total amount and quantity)
	 */
	GetDispatchedLinesAmountQuantity: function ()
	{
		var map = {};
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			var key = InvoiceLineItem.GetDispatchKey(item);
			if (key)
			{
				if (!map[key])
				{
					map[key] = {
						amount: item.GetValue("Amount__"),
						quantity: item.GetValue("Quantity__")
					};
				}
				else
				{
					map[key].amount += item.GetValue("Amount__");
					map[key].quantity += item.GetValue("Quantity__");
				}
			}
		}
		return map;
	},
	/**
	 * Update the subFocusKey to be able to set specific style on duplicate and planned delivery costs lines
	 * @param {TableRow} row The line used as reference
	 */
	SetSubFocusKey: function (row)
	{
		var item = row ? row.GetItem() : null;
		if (item && InvoiceLineItem.IsPOLineItem(item))
		{
			InvoiceLineItem.subFocusKey.focused = true;
			for (var val in InvoiceLineItem.subFocusKey)
			{
				if (Object.prototype.hasOwnProperty.call(InvoiceLineItem.subFocusKey, val) && val !== "focused")
				{
					InvoiceLineItem.subFocusKey[val] = row[val] ? row[val].GetValue() : null;
				}
			}
		}
		else
		{
			InvoiceLineItem.subFocusKey.focused = false;
		}
	},
	/**
	 * Set the dispatchKey
	 * @param {TableRow} row The line used as reference
	 */
	SetDispatchKey: function (row)
	{
		var item = row ? row.GetItem() : null;
		if (item && InvoiceLineItem.IsPOLineItem(item))
		{
			for (var val in InvoiceLineItem.dispatchKey)
			{
				if (Object.prototype.hasOwnProperty.call(InvoiceLineItem.dispatchKey, val))
				{
					InvoiceLineItem.dispatchKey[val] = row[val] ? row[val].GetValue() : null;
				}
			}
		}
	},
	/**
	 * Check if a PO line item is breakdowned
	 * @param {TableRow} row The line to test
	 * @return boolean True if the line is breakdowned
	 */
	IsSubFocused: function (row)
	{
		var item = row ? row.GetItem() : null;
		if (InvoiceLineItem.subFocusKey.focused && item && InvoiceLineItem.IsPOLineItem(item))
		{
			var bMatch = true;
			for (var val in InvoiceLineItem.subFocusKey)
			{
				if (Object.prototype.hasOwnProperty.call(InvoiceLineItem.subFocusKey, val) && val !== "focused")
				{
					bMatch = bMatch && Object.prototype.hasOwnProperty.call(row, val) && row[val].GetValue() === InvoiceLineItem.subFocusKey[val];
				}
			}
			return bMatch;
		}
		return false;
	},
	/**
	 * Set/Unset the breakdown style on a line
	 * @param {TableRow} row The line to test
	 */
	SetSubFocusStyle: function (row)
	{
		if (InvoiceLineItem.IsSubFocused(row))
		{
			row.AddRowStyle("grouped");
		}
		else
		{
			row.RemoveRowStyle("grouped");
		}
	},
	RefreshSubFocusStyles: function (row)
	{
		InvoiceLineItem.SetSubFocusKey(row);
		var nbLines = Math.min(Controls.LineItems__.GetItemCount(), Controls.LineItems__.GetLineCount());
		for (var i = 0; i < nbLines; i++)
		{
			InvoiceLineItem.SetSubFocusStyle(Controls.LineItems__.GetRow(i));
		}
	},
	/**
	 * Update the layout of the row at the specified index
	 * @param {TableRow} row The row to update
	 */
	UpdateRowLayoutToLineType: function (row)
	{
		if (row && row.LineType__)
		{
			switch (row.LineType__.GetValue())
			{
			case Lib.P2P.LineType.PO:
				InvoiceLineItem.SetPOLayout(row);
				break;
			case Lib.P2P.LineType.POGL:
				InvoiceLineItem.SetPOGLLayout(row);
				break;
			case Lib.P2P.LineType.GL:
				InvoiceLineItem.SetGLLayout(row);
				break;
			case Lib.P2P.LineType.CONSIGNMENT:
				InvoiceLineItem.SetConsignmentLayout(row);
				break;
			default:
				InvoiceLineItem.SetGLLayout(row);
				break;
			}
			InvoiceLineItem.SetPOLink(row);
		}
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnRefreshLineItemRowEnd", row);
	},
	/**
	 * Update the layout of the table
	 */
	UpdateTableLayoutToLineType: function ()
	{
		// Update all visible rows
		var nbLines = Math.min(Controls.LineItems__.GetItemCount(), Controls.LineItems__.GetLineCount());
		var nbAmountBasedItems = 0;
		for (var i = 0; i < nbLines; i++)
		{
			var row = Controls.LineItems__.GetRow(i);
			if (row && row.IsVisible())
			{
				InvoiceLineItem.UpdateRowLayoutToLineType(row);
				var item = Controls.LineItems__.GetItem(i);
				if (Lib.P2P.InvoiceLineItem.IsAmountBasedItem(item))
				{
					nbAmountBasedItems++;
				}
			}
		}
		// If the invoice contains only amount based items
		// hide quantity columns
		if (nbLines > 0 && nbAmountBasedItems === nbLines)
		{
			Controls.LineItems__.Quantity__.Hide(true);
			Controls.LineItems__.ExpectedQuantity__.Hide(true);
		}
		Controls.LineItems__.TaxAmount__.SetReadOnly(Controls.CalculateTax__.IsChecked());
	},
	/**
	 * Delete all lines items
	 */
	ClearLineItems: function ()
	{
		var invoiceDocument = Lib.AP.GetInvoiceDocument();
		var lineItemsTable = Data.GetTable("LineItems__");
		lineItemsTable.SetItemCount(0);
		Lib.AP.ResetLineItemsCache();
		InvoiceLineItem.iNonPOLinesCount = 0;
		InvoiceLineItem.iGRIVPOLinesCount = 0;
		Controls.NetAmount__.SetValue(invoiceDocument.layout.computeHeaderAmount());
		Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
		invoiceDocument.ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
		invoiceDocument.layout.updateLocalAmounts(Data.GetValue("ExchangeRate__"));
		checkBalance(true);
		// Clear reconciliation warning
		if (Variable.GetValueAsString("ReconciledByHeader"))
		{
			Variable.SetValueAsString("ReconciledByHeader", false);
			Controls.ReconcileWarning__.Hide(true);
		}
	},
	/**
	 * Update visible/hide columns and define which cells can be modified
	 * depending the invoice type and the line type
	 */
	UpdateLineItemsLayout: function ()
	{
		// The line items visible columns depend on Invoice Type
		// Visible columns for Non-PO Invoice: Description | Amount | G/L Account | Cost Center | Tax Code | Tax Amount
		// Visible columns for PO Invoice: Order Number | Item Number | Description | Expected quantity | Expected Amount | Quantity | Amount | Tax Code | Tax Amount
		// Visible columns for POGL Invoice: Order Number | Item Number | Description | Expected quantity | Expected Amount | Quantity | Amount | G/L Account | Cost Center | Tax Code | Tax Amount
		// If the workflow is in progress or the invoice is already posted,
		if (!currentStepCanModifyLineItems() || Controls.ERPPostingDate__.GetValue() || Lib.AP.InvoiceType.isConsignmentInvoice())
		{
			// disable add/remove lines
			Controls.LineItems__.SetReadOnly(true);
		}
		else
		{
			// enable add/remove lines
			Controls.LineItems__.SetReadOnly(false);
		}
		// Update columns/cells visibility and read only
		var updateLineFunction, lineType;
		switch (Controls.InvoiceType__.GetValue())
		{
		case Lib.AP.InvoiceType.PO_INVOICE:
			updateLineFunction = InvoiceLineItem.updatePOLineItemsLayout;
			lineType = Lib.P2P.LineType.PO;
			break;
		case Lib.AP.InvoiceType.PO_INVOICE_AS_FI:
			updateLineFunction = InvoiceLineItem.updatePOGLLineItemsLayout;
			lineType = Lib.P2P.LineType.POGL;
			break;
		case Lib.AP.InvoiceType.CONSIGNMENT:
			updateLineFunction = InvoiceLineItem.updateConsignmentLineItemsLayout;
			lineType = Lib.P2P.LineType.CONSIGNMENT;
			break;
		default:
			updateLineFunction = InvoiceLineItem.updateGLLineItemsLayout;
			lineType = Lib.P2P.LineType.GL;
			break;
		}
		//Force update first line type
		var row = Controls.LineItems__.GetRow(0);
		if (row && row.LineType__)
		{
			var nbItems = Data.GetTable("LineItems__").GetItemCount();
			var isItemEmpty = row.GetItem() && InvoiceLineItem.IsEmpty(row.GetItem());
			if (nbItems === 0 || isItemEmpty)
			{
				row.LineType__.SetValue(lineType);
				if (lineType === Lib.P2P.LineType.GL)
				{
					row.ItemType__.SetValue(Lib.P2P.ItemType.AMOUNT_BASED);
				}
			}
		}
		updateLineFunction();
		this.UpdateTableLayoutToLineType();
		Lib.AP.GetInvoiceDocument().OnRefreshLineItemsTableEnd();
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnRefreshLineItemsTableEnd", Controls.LineItems__);
	},
	ManageCodingsVisibility: function (hasGLlines)
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		var forceDisplay = hasGLlines || Lib.ERP.IsSAP();
		layout.Hide(Controls.LineItems__.GLAccount__, !forceDisplay || g_apParameters.GetParameter("CodingEnableGLAccount") !== "1");
		layout.Hide(Controls.LineItems__.GLDescription__, !forceDisplay || g_apParameters.GetParameter("CodingEnableGLAccount") !== "1");
		layout.Hide(Controls.LineItems__.CostCenter__, !forceDisplay || g_apParameters.GetParameter("CodingEnableCostCenter") !== "1");
		layout.Hide(Controls.LineItems__.CCDescription__, !forceDisplay || g_apParameters.GetParameter("CodingEnableCostCenter") !== "1");
		layout.Hide(Controls.LineItems__.ProjectCode__, !hasGLlines || g_apParameters.GetParameter("CodingEnableProjectCode") !== "1");
		layout.Hide(Controls.LineItems__.ProjectCodeDescription__, !hasGLlines || g_apParameters.GetParameter("CodingEnableProjectCode") !== "1");
		// PO invoice, even GL line, does not support trading partner
		layout.Hide(Controls.LineItems__.TradingPartner__, !forceDisplay || g_apParameters.GetParameter("CodingEnableTradingPartner") !== "1" || Lib.AP.InvoiceType.isPOInvoice());
		layout.Hide(Controls.LineItems__.BusinessArea__, !forceDisplay || g_apParameters.GetParameter("CodingEnableBusinessArea") !== "1");
		layout.Hide(Controls.LineItems__.InternalOrder__, !forceDisplay || g_apParameters.GetParameter("CodingEnableInternalOrder") !== "1");
		layout.Hide(Controls.LineItems__.WBSElement__, !forceDisplay || g_apParameters.GetParameter("CodingEnableWBSElement") !== "1");
		layout.Hide(Controls.LineItems__.Assignment__, !forceDisplay || g_apParameters.GetParameter("CodingEnableAssignments") !== "1");
		layout.Hide(Controls.LineItems__.CompanyCode__, !forceDisplay || g_apParameters.GetParameter("CodingEnableCompanyCode") !== "1");
		layout.Hide(Controls.Assignment__, !forceDisplay || g_apParameters.GetParameter("CodingEnableAssignments") !== "1");
		// Show CostType only if the feature is activated
		layout.Hide(Controls.LineItems__.CostType__, !(g_p2pParameters.GetParameter("EnablePurchasingGlobalSetting") === "1" && Sys.Parameters.GetInstance("PAC").GetParameter("DisplayCostType") && forceDisplay));
	},
	/**
	 * Update the line items layout for GL invoice
	 */
	updateGLLineItemsLayout: function ()
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		InvoiceLineItem.ManageCodingsVisibility(true);
		//Erp specific
		layout.Hide(Controls.Investment__, true);
		Controls.SubsequentDocument__.Check(false);
		Controls.LineItems__.ItemType__.Hide(true);
		Controls.LineItems__.PreviousBudgetID__.Hide(true);
		Controls.LineItems__.OrderNumber__.Hide(true);
		Controls.LineItems__.GoodIssue__.Hide(true);
		Controls.LineItems__.ItemNumber__.Hide(true);
		Controls.LineItems__.PartNumber__.Hide(true);
		Controls.LineItems__.Quantity__.Hide(true);
		Controls.LineItems__.ExpectedQuantity__.Hide(true);
		Controls.LineItems__.ExpectedAmount__.Hide(true);
		Controls.LineItems__.DeliveryNote__.Hide(true);
		Controls.LineItems__.GoodsReceipt__.Hide(true);
		Controls.LineItems__.Buyer__.Hide(true);
		Controls.LineItems__.Receiver__.Hide(true);
		Controls.LineItems__.UnitOfMeasureCode__.Hide(true);
		InvoiceLineItem.SetGLLayout(Controls.LineItems__);
		Controls.OrderNumber__.SetBrowsable(false);
		Controls.OrderNumber__.Hide(false);
		Controls.GoodIssue__.Hide(true);
		layout.Hide(Controls.AlternativePayee__, false);
		Controls.UnplannedDeliveryCosts__.Hide(true);
		Controls.UnplannedDeliveryCosts__.SetValue(null);
		Controls.Line_Items_Actions.Hide(false);
		Controls.ButtonLoadTemplate__.Hide(false);
		Controls.ButtonSaveTemplate__.Hide(false);
		Controls.ButtonLoadClipboard__.Hide(false);
		ParameterDetails.hide();
	},
	/**
	 * Update the line items layout for PO invoices
	 */
	updatePOLineItemsLayout: function ()
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		InvoiceLineItem.ManageCodingsVisibility(InvoiceLineItem.iNonPOLinesCount >= 1);
		Controls.LineItems__.ItemType__.Hide(true);
		Controls.LineItems__.PreviousBudgetID__.Hide(true);
		Controls.LineItems__.OrderNumber__.Hide(false);
		Controls.LineItems__.GoodIssue__.Hide(true);
		Controls.LineItems__.ItemNumber__.Hide(true);
		Controls.LineItems__.PartNumber__.Hide(false);
		Controls.LineItems__.Quantity__.Hide(false);
		Controls.LineItems__.ExpectedQuantity__.Hide(false);
		Controls.LineItems__.ExpectedAmount__.Hide(false);
		Controls.LineItems__.DeliveryNote__.Hide(!Controls.GRIV__.IsChecked());
		Controls.LineItems__.GoodsReceipt__.Hide(true);
		Controls.LineItems__.Buyer__.Hide(false);
		Controls.LineItems__.Receiver__.Hide(false);
		Controls.LineItems__.UnitOfMeasureCode__.Hide(true);
		if (Controls.GRIV__.IsChecked() || InvoiceLineItem.iGRIVPOLinesCount > 0)
		{
			LayoutHelper.SwitchToGRIV();
		}
		// Hide or show new columns if GL line exist
		if (InvoiceLineItem.iNonPOLinesCount >= 1)
		{
			InvoiceLineItem.UpdateTableLayoutToLineType();
		}
		else
		{
			InvoiceLineItem.SetPOLayout(Controls.LineItems__);
		}
		layout.Hide(Controls.Investment__, !Lib.P2P.IsInvestmentEnabled());
		Controls.OrderNumber__.SetBrowsable(!ProcessInstance.isReadOnly);
		Controls.OrderNumber__.Hide(false);
		Controls.GoodIssue__.Hide(true);
		layout.Hide(Controls.AlternativePayee__, true);
		layout.Hide(Controls.UnplannedDeliveryCosts__, false);
		Controls.ButtonLoadTemplate__.Hide(true);
		Controls.ButtonSaveTemplate__.Hide(true);
		Controls.ButtonLoadClipboard__.Hide(true);
		Controls.Line_Items_Actions.Hide(true);
		ParameterDetails.hide();
	},
	/**
	 * Update the line items layout for PO invoices
	 */
	updateConsignmentLineItemsLayout: function ()
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		InvoiceLineItem.ManageCodingsVisibility(true);
		//Erp specific
		Controls.LineItems__.ItemType__.Hide(true);
		Controls.LineItems__.PreviousBudgetID__.Hide(true);
		layout.Hide(Controls.Investment__, true);
		Controls.LineItems__.OrderNumber__.Hide(true);
		Controls.LineItems__.GoodIssue__.Hide(false);
		Controls.LineItems__.ItemNumber__.Hide(true);
		Controls.LineItems__.PartNumber__.Hide(false);
		Controls.LineItems__.Quantity__.Hide(false);
		Controls.LineItems__.ExpectedQuantity__.Hide(true);
		Controls.LineItems__.ExpectedAmount__.Hide(true);
		Controls.LineItems__.DeliveryNote__.Hide(true);
		Controls.LineItems__.GoodsReceipt__.Hide(true);
		Controls.LineItems__.Buyer__.Hide(true);
		Controls.LineItems__.Receiver__.Hide(true);
		InvoiceLineItem.SetConsignmentLayout(Controls.LineItems__);
		Controls.OrderNumber__.SetBrowsable(false);
		Controls.OrderNumber__.Hide(true);
		Controls.GoodIssue__.Hide(false);
		layout.Hide(Controls.AlternativePayee__, false);
		Controls.UnplannedDeliveryCosts__.Hide(true);
		Controls.UnplannedDeliveryCosts__.SetValue(null);
		Controls.Line_Items_Actions.Hide(false);
		Controls.ButtonLoadTemplate__.Hide(false);
		Controls.ButtonSaveTemplate__.Hide(false);
		Controls.ButtonLoadClipboard__.Hide(false);
		ParameterDetails.hide();
	},
	/**
	 * Update the line items layout for PO as FI invoices
	 */
	updatePOGLLineItemsLayout: function ()
	{
		InvoiceLineItem.ManageCodingsVisibility(true);
		Controls.LineItems__.ItemType__.Hide(true);
		Controls.LineItems__.PreviousBudgetID__.Hide(true);
		Controls.LineItems__.OrderNumber__.Hide(false);
		Controls.LineItems__.GoodIssue__.Hide(true);
		Controls.LineItems__.ItemNumber__.Hide(true);
		Controls.LineItems__.PartNumber__.Hide(false);
		Controls.LineItems__.Quantity__.Hide(false);
		Controls.LineItems__.ExpectedQuantity__.Hide(false);
		Controls.LineItems__.ExpectedAmount__.Hide(false);
		Controls.LineItems__.DeliveryNote__.Hide(!Controls.GRIV__.IsChecked());
		Controls.LineItems__.GoodsReceipt__.Hide(true);
		Controls.LineItems__.Buyer__.Hide(false);
		Controls.LineItems__.Receiver__.Hide(false);
		if (Controls.GRIV__.IsChecked())
		{
			LayoutHelper.SwitchToGRIV();
		}
		if (!Controls.ERPPostingDate__.GetValue())
		{
			InvoiceLineItem.UpdateTableLayoutToLineType();
		}
		Controls.OrderNumber__.SetBrowsable(!ProcessInstance.isReadOnly);
		Controls.OrderNumber__.Hide(false);
		Controls.GoodIssue__.Hide(true);
		Controls.AlternativePayee__.Hide(true);
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.UnplannedDeliveryCosts__, false);
		Controls.CodingTemplate__.Hide(true);
		ParameterDetails.hide();
	},
	/**
	 * Update line item with default GL line values
	 * @param {Data} item Destination table item
	 */
	UpdateGLLineItem: function (item)
	{
		if (item.GetValue("LineType__") !== Lib.P2P.LineType.GL)
		{
			item.SetValue("LineType__", Lib.P2P.LineType.GL);
			item.SetValue("TaxAmount__", 0.0);
			// GL lines are amount based since we don't need to specify any quantity
			item.SetValue("ItemType__", Lib.P2P.ItemType.AMOUNT_BASED);
			InvoiceLineItem.iNonPOLinesCount++;
			if (InvoiceLineItem.iNonPOLinesCount === 1)
			{
				InvoiceLineItem.UpdateLineItemsLayout();
			}
		}
	},
	/**
	 * Update line item with default PO line values
	 * @param {Data} item Destination table item
	 */
	UpdatePOLineItem: function (item)
	{
		item.SetValue("LineType__", Lib.P2P.LineType.PO);
		item.SetValue("TaxAmount__", 0.0);
	},
	/**
	 * Update line item with default PO line values
	 * @param {Data} item Destination table item
	 */
	UpdatePOGLLineItem: function (item)
	{
		item.SetValue("LineType__", Lib.P2P.LineType.POGL);
		item.SetValue("TaxAmount__", 0.0);
	},
	/**
	 * Update line item with default PO line values
	 * @param {Data} item Destination table item
	 */
	UpdateConsignmentLineItem: function (item)
	{
		item.SetValue("LineType__", Lib.P2P.LineType.CONSIGNMENT);
		item.SetValue("TaxAmount__", 0.0);
	},
	/**
	 * Add a table line
	 * @param {Table} table Destination table
	 * @param {integer} itemIndex Index to insert the item to
	 * @return newly create table line item
	 */
	AddLineItem: function (table, itemIndex)
	{
		var item;
		var cnt = table.GetItemCount();
		if (itemIndex || itemIndex === 0)
		{
			InvoiceLineItem.iIndexForNewItem = itemIndex;
		}
		if (InvoiceLineItem.iIndexForNewItem < 0)
		{
			InvoiceLineItem.iIndexForNewItem = cnt;
		}
		if (InvoiceLineItem.iIndexForNewItem >= cnt)
		{
			// append line
			table.SetItemCount(++cnt);
			item = table.GetItem(cnt - 1);
		}
		else
		{
			// insert line
			var nextItem = table.GetItem(InvoiceLineItem.iIndexForNewItem);
			item = nextItem.AddItem(true);
		}
		InvoiceLineItem.iIndexForNewItem++;
		return item;
	},
	/**
	 * Add a table line with default GL lines values
	 * @param {Table} table Destination table
	 * @return newly created table GL line item
	 */
	AddGLLineItem: function (table)
	{
		var item = InvoiceLineItem.AddLineItem(table);
		InvoiceLineItem.UpdateGLLineItem(item);
		return item;
	},
	/**
	 * Add a table line with default PO line default values
	 * @param {Table} table Destination table
	 * @return newly created table PO line item
	 */
	AddPOLineItem: function (table, index)
	{
		var item = InvoiceLineItem.AddLineItem(table, index);
		InvoiceLineItem.UpdatePOLineItem(item);
		return item;
	},
	/**
	 * Add a table line with default PO_FI line default values
	 * @param {Table} table Destination table
	 * @return newly created table PO line item
	 */
	AddPOGLLineItem: function (table)
	{
		var item = InvoiceLineItem.AddLineItem(table);
		InvoiceLineItem.UpdatePOGLLineItem(item);
		return item;
	},
	/**
	 * Update state informations when a line is deleted
	 * @param {Data} item The removed data
	 */
	RemoveLineItem: function (item)
	{
		if (item)
		{
			if (InvoiceLineItem.IsGLLineItem(item))
			{
				InvoiceLineItem.iNonPOLinesCount--;
			}
			else if (InvoiceLineItem.IsGRIVPOLineItem(item))
			{
				InvoiceLineItem.iGRIVPOLinesCount--;
				if (InvoiceLineItem.iGRIVPOLinesCount === 0)
				{
					LayoutHelper.SwitchToNonGRIV();
				}
			}
		}
	},
	/**
	 * Manage the OnDuplicateItem of a Table control
	 * @param {Data} item The line which was added
	 * @param {integer} index An integer representing the added line index
	 * @param {Data} sourceItem The line which was duplicated
	 * @param {integer} sourceIndex An integer representing the duplicated line index
	 */
	OnDuplicateItem: function (item /*, index: number, sourceItem: Item, sourceIndex: number*/ )
	{
		var acctAssCat = item.GetValue("AcctAssCat__");
		var itemIsDuplicable = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.IsItemDuplicable", item);
		if ((itemIsDuplicable === false) || (!itemIsDuplicable && InvoiceLineItem.IsPOLineItem(item) && !Lib.AP.SAP.IsMultiAccountAssignment(acctAssCat)))
		{
			item.Remove();
			Popup.Alert("_PO item dispatching is not available for the selected item", false, null, "_PO item dispatching");
		}
		else
		{
			InvoiceLineItem.iIndexForNewItem++;
			Controls.NetAmount__.SetValue(Lib.AP.GetInvoiceDocumentLayout().computeHeaderAmount());
			Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
			Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
			Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
			checkBalance(true);
			checkPOInvoiceLineItems();
		}
	},
	/**
	 * Manage the OnAddItem of a Table control
	 * @param {Data} item The line which was added
	 * @param {integer} index An integer representing the added line index
	 */
	OnAddItem: function (item, index)
	{
		InvoiceLineItem.iIndexForNewItem = index;
		var itemCount = Controls.LineItems__.GetItemCount();
		if (!Lib.AP.InvoiceType.isGLInvoice() && itemCount > 1)
		{
			// Remove default line and ask for line type
			item.Remove();
			Popup.Dialog("_Kind of line popup title", null, fillInsertPOLineOrGLLineDialog, commitInsertPOLineOrGLLineDialog);
		}
		else
		{
			switch (Data.GetValue("InvoiceType__"))
			{
			case Lib.AP.InvoiceType.NON_PO_INVOICE:
				InvoiceLineItem.UpdateGLLineItem(item);
				// When a new line is added, the tax code and jurisdiction code are pre-filled from the first line
				if (itemCount > 1)
				{
					var copyfrom = +!index;
					item.SetValue("TaxCode__", Controls.LineItems__.GetItem(copyfrom).GetValue("TaxCode__"));
					item.SetValue("TaxRate__", Controls.LineItems__.GetItem(copyfrom).GetValue("TaxRate__"));
					item.SetValue("MultiTaxRates__", Controls.LineItems__.GetItem(copyfrom).GetValue("MultiTaxRates__"));
					item.SetValue("TaxJurisdiction__", Controls.LineItems__.GetItem(copyfrom).GetValue("TaxJurisdiction__"));
				}
				break;
			case Lib.AP.InvoiceType.PO_INVOICE:
				InvoiceLineItem.UpdatePOLineItem(item);
				break;
			case Lib.AP.InvoiceType.PO_INVOICE_AS_FI:
				InvoiceLineItem.UpdatePOGLLineItem(item);
				break;
			case Lib.AP.InvoiceType.CONSIGNMENT:
				InvoiceLineItem.UpdateConsignmentLineItem(item);
				break;
			default:
				InvoiceLineItem.UpdateGLLineItem(item);
				break;
			}
			InvoiceLineItem.FillLineItemDescription(item, index);
			InvoiceLineItem.FillLineItemAssignment(item, index);
		}
	},
	/**
	 * Fill Item Description with InvoiceDescription__
	 * @param {Data} item The line which was added
	 * @param {integer} index An integer representing the added line index
	 */
	FillLineItemDescription: function (item, index)
	{
		var text = Controls.InvoiceDescription__.GetValue();
		// structures should be defined
		// only fill if not already posted
		// and if item hasn't description
		if (text && item && index >= 0 && !Controls.ERPPostingDate__.GetValue() && !item.GetValue("Description__"))
		{
			// check LineType
			// don't add description for POGL lines
			// don't add description for PO lines without order number
			if (item.GetValue("LineType__") &&
				item.GetValue("LineType__") !== Lib.P2P.LineType.POGL &&
				(item.GetValue("LineType__") !== Lib.P2P.LineType.PO || item.GetValue("OrderNumber__")))
			{
				item.SetValue("Description__", text);
			}
		}
	},
	/**
	 * Fill Item Assignment with Assignment__ header field
	 * @param {Data} item The line which was added
	 * @param {integer} index An integer representing the added line index
	 */
	FillLineItemAssignment: function (item, index)
	{
		var text = Controls.Assignment__.GetValue();
		if (text && item && index >= 0 && item.GetValue("LineType__") === Lib.P2P.LineType.GL && !Controls.ERPPostingDate__.GetValue() && !item.GetValue("Assignment__"))
		{
			item.SetValue("Assignment__", text);
		}
	}
};

function cleanUpLineItems()
{
	if (Lib.AP.InvoiceType.isGLInvoice())
	{
		// Clear any empty line in PO or POGL Invoice mode only
		return;
	}
	// clear empty lines (always keep at least one line)
	var j = 0;
	var cnt = Controls.LineItems__.GetItemCount();
	while (j < cnt && cnt > 1)
	{
		var item = Controls.LineItems__.GetItem(j);
		var isPOItemEmpty = InvoiceLineItem.IsPOLineItem(item) && InvoiceLineItem.IsPOLineItemEmpty(item);
		var isPOGLItemEmpty = InvoiceLineItem.IsPOGLLineItem(item) && InvoiceLineItem.IsPOGLLineItemEmpty(item);
		if (isPOItemEmpty || isPOGLItemEmpty)
		{
			item.Remove();
			cnt = Controls.LineItems__.GetItemCount();
		}
		else
		{
			j++;
		}
	}
}

function GetFillGLSAPParameters(item)
{
	return {
		"value": item.GetValue("GLAccount__"),
		"formCodeField": "GLAccount__",
		"formDescriptionField": "GLDescription__",
		"disableButtons": LayoutHelper.DisableButtons,
		"getFunction": getGLAccountDescription,
		"companyCode": Lib.AP.GetLineItemCompanyCode(item)
	};
}

function GetFillCCSAPParameters(item)
{
	return {
		"value": item.GetValue("CostCenter__"),
		"formCodeField": "CostCenter__",
		"formDescriptionField": "CCDescription__",
		"disableButtons": LayoutHelper.DisableButtons,
		"getFunction": getCostCenterDescriptionFromId,
		"companyCode": Lib.AP.GetLineItemCompanyCode(item)
	};
}

function GetFillGLERPParameters(item)
{
	return {
		"formCodeField": "GLAccount__",
		"formFields": ["GLDescription__", "Group__"],
		"tableFields": ["Description__", "Group__"],
		"tableKeyField": "Account__",
		"table": "AP - G/L accounts__",
		"disableButtons": LayoutHelper.DisableButtons,
		"companyCode": Lib.AP.GetLineItemCompanyCode(item)
	};
}

function GetFillCCERPParameters(item)
{
	return {
		"formCodeField": "CostCenter__",
		"formField": "CCDescription__",
		"tableField": "CostCenter__",
		"table": "AP - Cost centers__",
		"disableButtons": LayoutHelper.DisableButtons,
		"companyCode": Lib.AP.GetLineItemCompanyCode(item)
	};
}

function GetFillProjectCodeERPParameters(item)
{
	return {
		"formCodeField": "ProjectCode__",
		"formField": "ProjectCodeDescription__",
		"tableField": "ProjectCode__",
		"table": "P2P - Project codes__",
		"disableButtons": LayoutHelper.DisableButtons,
		"companyCode": Lib.AP.GetLineItemCompanyCode(item)
	};
}

function fillGLAndCCDescriptionForSAP(item, doneCallback)
{
	var expectedCalls = 2;

	function done()
	{
		expectedCalls--;
		if (expectedCalls === 0 && doneCallback)
		{
			doneCallback();
		}
	}
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, GetFillGLSAPParameters(item), done);
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, GetFillCCSAPParameters(item), done);
}

function fillDescriptionsForERP(item, doneCallback)
{
	var projectCodeEnabled = g_apParameters.GetParameter("CodingEnableProjectCode") === "1";
	var expectedCalls = projectCodeEnabled ? 3 : 2;

	function done()
	{
		expectedCalls--;
		if (expectedCalls === 0 && doneCallback)
		{
			doneCallback(item);
		}
	}
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, GetFillGLERPParameters(item), done);
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, GetFillCCERPParameters(item), done);
	if (projectCodeEnabled)
	{
		Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, GetFillProjectCodeERPParameters(item), done);
	}
}

function fillGLAndCCDescriptions(item, doneCallback)
{
	if (Lib.ERP.IsSAP())
	{
		fillGLAndCCDescriptionForSAP(item, doneCallback);
	}
	else
	{
		fillDescriptionsForERP(item, doneCallback);
	}
}

function genericAndSapPOGLAddLineItem(itemMap)
{
	var newItem = Lib.AP.InvoiceType.isPOGLInvoice() ?
		InvoiceLineItem.AddPOGLLineItem(Controls.LineItems__) :
		InvoiceLineItem.AddPOLineItem(Controls.LineItems__, Controls.LineItems__.GetItemCount());
	var expectedAmount = itemMap.DELIVEREDAMOUNT__ - itemMap.INVOICEDAMOUNT__;
	var expectedQuantity = itemMap.DELIVEREDQUANTITY__ - itemMap.INVOICEDQUANTITY__;
	newItem.SetValue("OrderNumber__", itemMap.ORDERNUMBER__);
	newItem.SetValue("IsLocalPO__", itemMap.ISLOCALPO__);
	newItem.SetValue("ItemNumber__", itemMap.ITEMNUMBER__);
	newItem.SetValue("Description__", itemMap.DESCRIPTION__);
	newItem.SetValue("TaxAmount__", itemMap.TAXAMOUNT__);
	newItem.SetValue("OpenQuantity__", itemMap.ORDEREDQUANTITY__ - itemMap.INVOICEDQUANTITY__);
	newItem.SetValue("OpenAmount__", itemMap.ORDEREDAMOUNT__ - itemMap.INVOICEDAMOUNT__);
	newItem.SetValue("Quantity__", expectedQuantity > 0 ? expectedQuantity : "");
	newItem.SetValue("Amount__", expectedAmount > 0 ? expectedAmount : "");
	newItem.SetValue("ExpectedQuantity__", expectedQuantity);
	newItem.SetValue("ExpectedAmount__", expectedAmount);
	newItem.SetValue("TaxCode__", itemMap.TAXCODE__);
	newItem.SetValue("VendorNumber__", itemMap.VENDORNUMBER__);
	newItem.SetValue("PartNumber__", itemMap.PARTNUMBER__);
	newItem.SetValue("DeliveryNote__", itemMap.DELIVERYNOTE__);
	newItem.SetValue("GoodsReceipt__", itemMap.GOODRECEIPT__);
	newItem.SetValue("DifferentInvoicingParty__", itemMap.DIFFERENTINVOICINGPARTY__);
	newItem.SetValue("NoGoodsReceipt__", itemMap.NOGOODSRECEIPT__);
	newItem.SetValue("UnitOfMeasureCode__", itemMap.UNITOFMEASURECODE__);
	// Done in SAP but no column available
	//newItem.SetValue("GRIV__", itemMap.GRIV__);
	if (itemMap.GRIV__ === "1")
	{
		InvoiceLineItem.iGRIVPOLinesCount++;
		if (InvoiceLineItem.iGRIVPOLinesCount === 1)
		{
			LayoutHelper.SwitchToGRIV();
		}
	}
	var unitPrice = parseFloat(itemMap.UNITPRICE__);
	if (isNaN(unitPrice))
	{
		unitPrice = Lib.P2P.ComputeUnitPrice(itemMap.ORDEREDAMOUNT__, itemMap.ORDEREDQUANTITY__);
	}
	newItem.SetValue("UnitPrice__", unitPrice);
	newItem.SetValue("GLAccount__", itemMap.GLACCOUNT__);
	newItem.SetValue("CostType__", itemMap.COSTTYPE__);
	newItem.SetValue("Group__", itemMap.GROUP__);
	newItem.SetValue("CostCenter__", itemMap.COSTCENTER__);
	newItem.SetValue("ProjectCode__", itemMap.PROJECTCODE__);
	newItem.SetValue("PreviousBudgetID__", itemMap.BUDGETID__);
	newItem.SetValue("Buyer__", itemMap.BUYER__);
	newItem.SetValue("Receiver__", itemMap.RECEIVER__);
	newItem.SetValue("ItemType__", itemMap.ITEMTYPE__);
	newItem.SetValue("WBSElementID__", itemMap.WBSELEMENTID__);
	newItem.SetValue("WBSElement__", itemMap.WBSELEMENT__);
	newItem.SetValue("InternalOrder__", itemMap.INTERNALORDER__);
	newItem.SetValue("FreeDimension1__", itemMap.FREEDIMENSION1__);
	newItem.SetValue("FreeDimension1ID__", itemMap.FREEDIMENSION1ID__);
	var customDimensions = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetCustomDimensions");
	if (customDimensions && customDimensions.poItems)
	{
		for (var _i = 0, _a = customDimensions.poItems; _i < _a.length; _i++)
		{
			var poItem = _a[_i];
			newItem.SetValue(poItem.nameInForm, itemMap[poItem.nameInTable.toUpperCase()]);
		}
	}
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.PurchaseOrder.OnAddPOLine", newItem, itemMap);
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.FillCostType", newItem);
	getTaxRateAndUpdateItem(newItem, null, true);
	fillGLAndCCDescriptions(newItem);
	Lib.AP.AddOrderNumber(itemMap.ORDERNUMBER__);
	if (!newItem.GetValue("CostType__"))
	{
		Lib.P2P.fillCostTypeFromGLAccount(newItem);
	}
}

function handleCustomDimensions(item, po, poItem, acctAss)
{
	function setCustomDimensions(items, currentPoItem)
	{
		Sys.Helpers.Array.ForEach(items, function (poSAPItem)
		{
			if (poSAPItem && !Sys.Helpers.IsEmpty(poSAPItem.nameInForm) && !Sys.Helpers.IsEmpty(poSAPItem.nameInSAP))
			{
				var itemValue = currentPoItem[poSAPItem.nameInSAP] || "";
				var formatter = poSAPItem.fieldFormatter;
				if (itemValue && typeof formatter === "function")
				{
					itemValue = formatter(itemValue);
				}
				item.SetValue(poSAPItem.nameInForm, itemValue);
			}
		});
	}
	var customDimensions = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetCustomDimensions");
	if (customDimensions && customDimensions.poSAPItems)
	{
		for (var key in customDimensions.poSAPItems)
		{
			if (Object.prototype.hasOwnProperty.call(customDimensions.poSAPItems, key))
			{
				var poDetailsElement = void 0;
				switch (key)
				{
				case "PO_ITEMS":
					poDetailsElement = poItem;
					break;
				case "PO_HEADER":
				case "PO_ADDRESS":
					poDetailsElement = po[key];
					break;
				case "PO_ITEM_ACCOUNT_ASSIGNMENT":
					poDetailsElement = acctAss;
					break;
				default:
					Log.Error("customDimensions.poSAPItems." + key + " not supported yet in GetCustomDimensions");
				}
				if (poDetailsElement && customDimensions.poSAPItems[key] && customDimensions.poSAPItems[key].length)
				{
					setCustomDimensions(customDimensions.poSAPItems[key], poDetailsElement);
				}
			}
		}
	}
}

function sapMMAddLineItem(poItem, po)
{
	var item = InvoiceLineItem.AddPOLineItem(Controls.LineItems__, Controls.LineItems__.GetItemCount());
	item.SetValue("ItemNumber__", poItem.Po_Item);
	item.SetValue("Description__", poItem.Short_Text);
	item.SetValue("ExpectedAmount__", poItem.ExpectedAmount);
	item.SetValue("ExpectedQuantity__", poItem.ExpectedQuantity);
	item.SetValue("OpenAmount__", poItem.OpenInvoiceValue);
	item.SetValue("OpenQuantity__", poItem.OpenInvoiceQuantity);
	item.SetValue("Amount__", poItem.ExpectedAmount > 0 ? poItem.ExpectedAmount : "");
	item.SetValue("Quantity__", poItem.ExpectedQuantity > 0 ? poItem.ExpectedQuantity : "");
	item.SetValue("OrderedAmount__", poItem.OrderedAmount);
	item.SetValue("OrderedQuantity__", poItem.Quantity);
	item.SetValue("DeliveredAmount__", poItem.DeliveredAmount);
	item.SetValue("DeliveredQuantity__", poItem.Deliv_Qty);
	item.SetValue("InvoicedAmount__", poItem.InvoicedAmount);
	item.SetValue("InvoicedQuantity__", poItem.Iv_Qty);
	item.SetValue("GRIV__", poItem.Gr_Basediv);
	if (poItem.Gr_Basediv)
	{
		InvoiceLineItem.iGRIVPOLinesCount++;
		if (InvoiceLineItem.iGRIVPOLinesCount === 1)
		{
			LayoutHelper.SwitchToGRIV();
		}
	}
	item.SetValue("DeliveryNote__", poItem.Ref_Doc_No);
	item.SetValue("GoodsReceipt__", poItem.Ref_Doc);
	item.SetValue("TaxCode__", poItem.Tax_Code);
	item.SetValue("TaxJurisdiction__", poItem.Tax_Jur_Cd);
	item.SetValue("VendorNumber__", poItem.Vendor);
	item.SetValue("OrderNumber__", poItem.Po_Number);
	item.SetValue("IsLocalPO__", false);
	item.SetValue("UnitPrice__", poItem.Net_Price);
	item.SetValue("PartNumber__", poItem.Material);
	item.SetValue("Buyer__", poItem.Buyer);
	item.SetValue("Receiver__", poItem.Receiver);
	item.SetValue("DifferentInvoicingParty__", po.PO_HEADER.DIFF_INV);
	item.SetValue("AcctAssCat__", poItem.AcctAssCat + poItem.Distribution);
	var acctAss = Lib.AP.SAP.PurchaseOrder.GetAccountAssignmentInfo(po.PO_ITEM_ACCOUNT_ASSIGNMENT, poItem);
	if (acctAss)
	{
		var glAccount = acctAss.G_L_ACCT ? Sys.Helpers.String.SAP.TrimLeadingZeroFromID(acctAss.G_L_ACCT) : "";
		var costCenter = acctAss.COST_CTR ? Sys.Helpers.String.SAP.TrimLeadingZeroFromID(acctAss.COST_CTR) : "";
		var businessArea = acctAss.BUS_AREA || "";
		var internalOrder = acctAss.ORDER_NO ? Sys.Helpers.String.SAP.TrimLeadingZeroFromID(acctAss.ORDER_NO) : "";
		var wbsElement = acctAss.WBS_ELEM_E || "";
		var acctAssQuantity = acctAss.QUANTITY || "";
		var acctAssPerc = acctAss.DISTR_PERC || "";
		item.SetValue("GLAccount__", glAccount);
		item.SetValue("CostCenter__", costCenter);
		item.SetValue("BusinessArea__", businessArea);
		item.SetValue("InternalOrder__", internalOrder);
		item.SetValue("WBSElement__", wbsElement);
		if (acctAssQuantity && acctAssPerc)
		{
			var amount = item.GetValue("Amount__") > 0 ? item.GetValue("Amount__") * acctAssPerc / 100 : "";
			var quantity = item.GetValue("Quantity__") > 0 ? item.GetValue("Quantity__") * acctAssPerc / 100 : "";
			item.SetValue("Amount__", amount);
			item.SetValue("Quantity__", quantity);
		}
		fillGLAndCCDescriptionForSAP(item);
	}
	if (poItem.Cond_Type)
	{
		item.SetValue("PriceCondition__", poItem.Cond_Type);
	}
	handleCustomDimensions(item, po, poItem, acctAss);
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.PurchaseOrder.OnAddPOLine", item, poItem, po);
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.FillCostType", item);
	getTaxRateAndUpdateItem(item, setItemTaxRateAndTaxAmount, true);
	if (!item.GetValue("CostType__"))
	{
		Lib.P2P.fillCostTypeFromGLAccount(item);
	}
}
/**
 * @class An help class to manage layout
 */
LayoutHelper = {
	currentLayout: null,
	disableGroup:
	{},
	buttonsDisabled: 0,
	isReadOnlyRestored: true,
	readOnlyPanelsMap:
	{},
	MakePanelsReadOnlyForBrowsePOOpening: function ()
	{
		if (LayoutHelper.isReadOnlyRestored)
		{
			LayoutHelper.readOnlyPanelMap = {};
			for (var c in Controls)
			{
				if (Object.prototype.hasOwnProperty.call(Controls, c) &&
					Controls[c] &&
					Controls[c].GetControls &&
					Controls[c].SetReadOnly)
				{
					LayoutHelper.readOnlyPanelMap[c] = Controls[c].IsReadOnly();
					Controls[c].SetReadOnly(true);
				}
			}
			LayoutHelper.isReadOnlyRestored = false;
		}
	},
	RestorePanelsReadOnlyForBrowsePOClosing: function ()
	{
		for (var c in LayoutHelper.readOnlyPanelMap)
		{
			if (Object.prototype.hasOwnProperty.call(LayoutHelper.readOnlyPanelMap, c) &&
				Object.prototype.hasOwnProperty.call(Controls, c) &&
				Controls[c])
			{
				Controls[c].SetReadOnly(LayoutHelper.readOnlyPanelMap[c]);
			}
		}
		LayoutHelper.isReadOnlyRestored = true;
		// Force to refresh interface
		InvoiceLineItem.UpdateLineItemsLayout();
		LayoutHelper.AdaptProcessLayoutToStep();
	},
	/** Button bar for AP */
	FirstStepButtonBar: function ()
	{
		Controls.SetAside.Hide(false);
		if (!Controls.ERPLinkingDate__.GetValue())
		{
			var isTeachingDisabled = Sys.Parameters.GetInstance("AP").GetParameter("DisableTeaching") === "1";
			Controls.Request_teaching.Hide(isTeachingDisabled);
			Controls.Reject.Hide(false);
			Controls.Reprocess.Hide(false);
		}
		if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.controller) === 0 &&
			Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.approver) === 0)
		{
			this.LastStepButtonBar();
		}
		else
		{
			var bHideSimulate = false;
			if (!Controls.ERPLinkingDate__.GetValue())
			{
				if (Controls.ManualLink__.IsChecked())
				{
					Controls.Post.SetLabel(Lib.AP.GetInvoiceDocumentLayout().GetPostAndRequestApprovalManualLinkLabel());
					bHideSimulate = true;
				}
				else if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.controller) === 0)
				{
					if (!Lib.AP.WorkflowCtrl.IsCurrentContributorLowPrivilegeAP())
					{
						if (!Lib.AP.InvoiceType.isConsignmentInvoice())
						{
							Controls.Post.SetLabel("_Post and Request Approval");
						}
						else if (!Data.GetValue("ERPInvoiceNumber__"))
						{
							Controls.Post.SetLabel("_SettleAndWaitForClearingAndRequestApproval");
						}
						else
						{
							Controls.Post.SetLabel("_WaitForClearingAndRequestApproval");
						}
					}
					else
					{
						Controls.Post.SetLabel("_Request Post");
					}
				}
				else
				{
					Controls.Post.SetLabel("_Request Approval");
				}
			}
			else
			{
				Controls.Post.SetLabel("_Request Approval");
				if (Controls.ERPLinkingDate__.GetValue() || Controls.ManualLink__.IsChecked())
				{
					bHideSimulate = true;
				}
			}
			Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.Simulate, bHideSimulate);
		}
	},
	/** Button bar the last approbation and the final post */
	LastStepButtonBar: function ()
	{
		if (!Controls.ERPLinkingDate__.GetValue())
		{
			Controls.Reject.Hide(false);
			Controls.Reprocess.Hide(false);
			if (Controls.ManualLink__.IsChecked())
			{
				if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.approver) > 0)
				{
					Controls.Post.SetLabel(Lib.AP.GetInvoiceDocumentLayout().GetPostAndRequestApprovalManualLinkLabel());
				}
				else
				{
					Controls.Post.SetLabel(Lib.AP.GetInvoiceDocumentLayout().GetPostManualLinkLabel());
				}
				Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.Simulate, true);
			}
			else
			{
				if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.controller) === 0 &&
					Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.approver) === 0 &&
					!Lib.AP.WorkflowCtrl.IsCurrentContributorLowPrivilegeAP())
				{
					if (!Lib.AP.InvoiceType.isConsignmentInvoice())
					{
						Controls.Post.SetLabel("_Post");
					}
					else if (!Data.GetValue("ERPInvoiceNumber__"))
					{
						Controls.Post.SetLabel("_Post and Wait for Clearing");
					}
					else
					{
						Controls.Post.SetLabel("_Wait for Clearing");
					}
				}
				else if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.controller) > 0)
				{
					Controls.Post.SetLabel("_Request Approval");
				}
				else if (Lib.AP.WorkflowCtrl.IsCurrentContributorLowPrivilegeAP())
				{
					Controls.Post.SetLabel("_Request Post");
				}
				else if (!Lib.AP.InvoiceType.isConsignmentInvoice())
				{
					Controls.Post.SetLabel("_Post and Request Approval");
				}
				else if (!Data.GetValue("ERPInvoiceNumber__"))
				{
					Controls.Post.SetLabel("_SettleAndWaitForClearingAndRequestApproval");
				}
				else
				{
					Controls.Post.SetLabel("_WaitForClearingAndRequestApproval");
				}
				Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.Simulate, false);
			}
		}
		else if (Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.approver) > 0)
		{
			Controls.Post.SetLabel("_Request Approval");
		}
		else
		{
			Controls.Post.SetLabel("_Approve");
		}
	},
	/** Button bar for approvers */
	WorkflowButtonBar: function ()
	{
		Controls.BackToAP.Hide(false);
		Controls.OnHold.Hide(false);
		Controls.Post.SetLabel("_Approve");
		Controls.AddApprover.Hide(false);
		if (Lib.AP.WorkflowCtrl.BackToPreviousPossible())
		{
			Controls.BackToPrevious.Hide(false);
		}
		if (currentStepIsController())
		{
			Controls.AddApprover.SetLabel("_Add controller");
		}
		else
		{
			Controls.AddApprover.SetLabel("_Add approver");
		}
	},
	ShowOrHideEditButton: function ()
	{
		Controls.Edit_.Hide(ProcessInstance.isEditing || !userIsAPOrAdmin());
	},
	/** Button bar in case of unblock error*/
	ToPayButtonBar: function ()
	{
		Controls.Post.Hide();
		this.ShowOrHideEditButton();
	},
	/** Button bar in case of unblock error*/
	PaidButtonBar: function ()
	{
		Controls.Post.Hide();
		this.ShowOrHideEditButton();
	},
	WaitForClearingButtonBar: function ()
	{
		Controls.SetAside.Hide(true);
		Controls.Post.Hide(true);
		Controls.Save.Hide(true);
	},
	ShouldHideSetAside: function ()
	{
		return !currentStepIsApStart() || ProcessInstance.isReadOnly || ProcessInstance.isEditing;
	},
	InitButtons: function ()
	{
		Controls.BackToPrevious.Hide();
		Controls.Reject.Hide();
		Controls.BackToAP.Hide();
		Controls.OnHold.Hide();
		Controls.AddApprover.Hide();
		Controls.SetAside.Hide(this.ShouldHideSetAside());
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.Simulate, true);
		Controls.Request_teaching.Hide();
		Controls.Reprocess.Hide();
	},
	UpdateButtonBar: function ()
	{
		// Change the Post button label and hide the "Reject" and "Resubmit" buttons for approvers according to the current workflow step
		this.InitButtons();
		var invoiceStatus = Controls.InvoiceStatus__.GetValue();
		if (!ProcessInstance.isReadOnly)
		{
			if (isInApprovalWorkflow())
			{
				this.WorkflowButtonBar();
			}
			else if (invoiceStatus === Lib.AP.InvoiceStatus.ToVerify || invoiceStatus === Lib.AP.InvoiceStatus.SetAside || invoiceStatus === Lib.AP.InvoiceStatus.Received)
			{
				this.FirstStepButtonBar();
			}
			else if (invoiceStatus === Lib.AP.InvoiceStatus.ToPost)
			{
				this.LastStepButtonBar();
			}
			else if (invoiceStatus === Lib.AP.InvoiceStatus.ToPay)
			{
				this.ToPayButtonBar();
			}
			else if (invoiceStatus === Lib.AP.InvoiceStatus.Paid)
			{
				this.PaidButtonBar();
			}
			else if (invoiceStatus === Lib.AP.InvoiceStatus.WaitForClearing)
			{
				this.WaitForClearingButtonBar();
			}
		}
		else
		{
			Controls.ButtonLoadTemplate__.Hide(true);
			Controls.ButtonSaveTemplate__.Hide(true);
			Controls.ButtonLoadClipboard__.Hide(true);
			Controls.Line_Items_Actions.Hide(true);
			// For posted invoices hide the Edit button for profiles other than AP Specialist
			if (invoiceStatus === Lib.AP.InvoiceStatus.ToPay || invoiceStatus === Lib.AP.InvoiceStatus.Paid || invoiceStatus === Lib.AP.InvoiceStatus.Reversed)
			{
				this.ShowOrHideEditButton();
			}
		}
		Lib.AP.GetInvoiceDocumentLayout().UpdateLayout();
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnUpdateButtonBarEnd");
	},
	RevalidateLineItems: function ()
	{
		var requiredFields = Lib.AP.GetInvoiceDocument()
			.GetRequiredFields(Sys.Helpers.TryGetFunction("Lib.AP.Customization.Common.GetRequiredFields"));
		if (!Data.GetValue("ManualLink__"))
		{
			var lineItems = Data.GetTable("LineItems__");
			var nbItems = lineItems.GetItemCount();
			for (var i = 0; i < nbItems; i++)
			{
				var row = lineItems.GetItem(i);
				if (InvoiceLineItem.IsEmpty(row))
				{
					var columns = Controls.LineItems__.GetColumnsOrder();
					for (var _i = 0, columns_1 = columns; _i < columns_1.length; _i++)
					{
						var column = columns_1[_i];
						row.SetError(column);
					}
				}
				else
				{
					validateLine(row, requiredFields);
				}
			}
		}
	},
	RecomputeRequired: function ()
	{
		Lib.AP.GetInvoiceDocumentLayout().UpdateLayout();
	},
	AreBoutonsDisabled: function ()
	{
		return LayoutHelper.buttonsDisabled > 0;
	},
	DisableButtons: function (disable, group)
	{
		var currentState = LayoutHelper.AreBoutonsDisabled();
		var groupName = group || "*";
		if (disable)
		{
			LayoutHelper.buttonsDisabled++;
			if (LayoutHelper.disableGroup[groupName] && LayoutHelper.disableGroup[groupName].count)
			{
				LayoutHelper.disableGroup[groupName].count++;
			}
			else
			{
				LayoutHelper.disableGroup[groupName] = {
					count: 1
				};
			}
		}
		else
		{
			LayoutHelper.buttonsDisabled--;
			if (!LayoutHelper.disableGroup[groupName])
			{
				Log.Error("Enabling of buttons on a unitialized group '" + groupName + "'");
			}
			else if (LayoutHelper.disableGroup[groupName].count === 0)
			{
				Log.Warn("Enabling of buttons from group '" + groupName + "' is call too often.");
			}
			else
			{
				LayoutHelper.disableGroup[groupName].count--;
			}
		}
		if (LayoutHelper.buttonsDisabled <= 0)
		{
			// Check balance to eventually re-enable ButtonSaveTemplate__
			checkBalance(false);
			RequestTeaching.RefreshButtonState();
		}
		else
		{
			Controls.ButtonSaveTemplate__.SetDisabled(true);
			Controls.Request_teaching.SetDisabled(true);
		}
		if (LayoutHelper.ShowWaitScreen)
		{
			if (LayoutHelper.AreBoutonsDisabled())
			{
				Controls.ButtonLoadClipboard__.Wait(true);
			}
			else if (!LayoutHelper.AreBoutonsDisabled())
			{
				Controls.ButtonLoadClipboard__.Wait(false);
				LayoutHelper.ShowWaitScreen = false;
			}
		}
		var newState = LayoutHelper.AreBoutonsDisabled();
		if (currentState !== newState)
		{
			Controls.SetAside.SetDisabled(newState);
			Controls.Simulate.SetDisabled(newState);
			Controls.Post.SetDisabled(newState);
			Controls.Reject.SetDisabled(newState);
			Controls.Reprocess.SetDisabled(newState);
			Controls.Save.SetDisabled(newState);
			Controls.ButtonLoadTemplate__.SetDisabled(newState);
			Controls.ButtonLoadClipboard__.SetDisabled(newState);
			Controls.BackToAP.SetDisabled(newState);
			Controls.OnHold.SetDisabled(newState);
			Controls.AddApprover.SetDisabled(newState);
			Controls.BackToPrevious.SetDisabled(newState);
			Controls.UpdatePayment.SetDisabled(newState);
		}
	},
	UpdateLayout: function (doLoadTemplate)
	{
		LayoutHelper.currentLayout = Controls.InvoiceType__.GetValue();
		LayoutHelper.UpdateManualLinkLayout();
		// Clear existing line items
		InvoiceLineItem.ClearLineItems();
		// Update visible columns of line items
		InvoiceLineItem.UpdateLineItemsLayout();
		if (doLoadTemplate !== false)
		{
			loadTemplate();
			handleTaxComputation();
		}
		LayoutHelper.AdaptProcessLayoutToStep();
		// Update button bar (post/request approval)
		LayoutHelper.UpdateButtonBar();
		Log.Info("Controls.ManualLink__.IsChecked():" + Controls.ManualLink__.IsChecked() + ", isPOInvoice(): " + Lib.AP.InvoiceType.isPOInvoice() + "!Controls.ManualLink__.IsChecked() || !isPOInvoice()=" + (!Controls.ManualLink__.IsChecked() || !Lib.AP.InvoiceType.isPOInvoice()));
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnUpdateLayoutEnd");
	},
	RevertInvoiceType: function ()
	{
		// Revert value
		Controls.InvoiceType__.SetValue(LayoutHelper.currentLayout);
	},
	ConfirmInvoiceTypeUpdate: function ()
	{
		if (!LayoutHelper.IsTableEmpty())
		{
			Popup.Confirm("_This action will delete the current line items.", false, LayoutHelper.UpdateLayout, LayoutHelper.RevertInvoiceType, "_Warning");
		}
		else
		{
			LayoutHelper.UpdateLayout();
		}
	},
	IsTableEmpty: function ()
	{
		// Check if table is empty
		// Pay attention the value of the combobox was already updated
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (!InvoiceLineItem.IsEmpty(item))
			{
				return false;
			}
		}
		return true;
	},
	AdaptProcessLayoutToInvoiceType: function (isInvoiceTypeChanged)
	{
		if (isInvoiceTypeChanged)
		{
			LayoutHelper.ConfirmInvoiceTypeUpdate();
		}
		else
		{
			LayoutHelper.currentLayout = Controls.InvoiceType__.GetValue();
			InvoiceLineItem.UpdateLineItemsLayout();
		}
	},
	AdaptProcessLayoutToStep: function ()
	{
		if (isInApprovalWorkflow())
		{
			// The form is in approver mode (payment approval or workflow completed)
			LayoutHelper.SetApprovalLayout();
		}
		else
		{
			// The form is in the data capture mode (AP or back to AP)
			// merge conditions of the both conditions
			var inDataCaptureMode = void 0;
			if (Lib.ERP.IsSAP())
			{
				inDataCaptureMode = !Controls.ERPLinkingDate__.GetValue() && !Controls.ManualLink__.IsChecked();
			}
			else
			{
				inDataCaptureMode = !Controls.ERPPostingDate__.GetValue();
			}
			if (!inDataCaptureMode)
			{
				LayoutHelper.SetPostedLayout();
				if (Controls.ManualLink__.IsChecked())
				{
					var layout = Lib.AP.GetInvoiceDocumentLayout();
					layout.EnableManualLink();
					layout.UpdateLayoutForManualLink();
					layout.Hide(Controls.ERPMMInvoiceNumber__, false);
				}
			}
			else
			{
				LayoutHelper.SetDataCaptureLayout();
			}
		}
		var useLightLayout = !userIsAPOrAdmin() && !reviewersCanModifyLineItems();
		var customLayout = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnDetermineLayout");
		if (typeof customLayout === "number")
		{
			useLightLayout = customLayout === 1;
		}
		if (useLightLayout)
		{
			LayoutHelper.SwitchToLightLayout();
		}
		else
		{
			Controls.HeaderDataPanelForApproversLeft.Hide(true);
			Controls.HeaderDataPanelForApproversRight.Hide(true);
			Lib.AP.ArchivedInvoices.SetEditingLayoutIfNeeded();
		}
		Controls.PaymentDetails.Hide(Controls.InvoiceStatus__.GetValue() !== Lib.AP.InvoiceStatus.Paid);
	},
	SwitchToLightLayout: function ()
	{
		// Use simplified layout: hide useless panels
		Controls.HeaderDataPanelForApproversLeft.Hide(false);
		Controls.HeaderDataPanelForApproversRight.Hide(false);
		Controls.HeaderDataPanel.Hide(true);
		Controls.Invoice_Processing.Hide(true);
		Controls.ERP_Details.Hide(true);
		Controls.VendorInformation.Hide(true);
		Controls.Parameters.Hide(true);
		Controls.CalculateTax__.Hide(true);
		Controls.CodingTemplate__.Hide(true);
		Controls.ButtonSaveTemplate__.Hide(true);
		Controls.ButtonLoadTemplate__.Hide(true);
		Controls.ButtonLoadClipboard__.Hide(true);
		Controls.Line_Items_Actions.Hide(true);
		Controls.ReverseInvoice.Hide(true);
		// Hide Extended WHT panel
		Controls.ExtendedWithholdingTaxPane.Hide(true);
		// Hide some buttons
		Controls.ContactVendor__.Hide(true);
		Controls.SupplierInformationManagementLink__.Hide(true);
		// Hide columns in item list
		Controls.LineItems__.TaxCode__.Hide(true);
		Controls.LineItems__.TaxAmount__.Hide(true);
		Controls.LineItems__.ExpectedQuantity__.Hide(true);
		Controls.LineItems__.ExpectedAmount__.Hide(true);
		Controls.LineItems__.CCDescription__.Hide(false);
		Controls.LineItems__.ProjectCodeDescription__.Hide(false);
		Controls.LineItems__.TaxJurisdiction__.Hide(true);
		Controls.NetAmountForApprovers__.Focus();
		this.UpdateExceptionsTypePanel(
		{
			// Cannot modify Exception
			panelIsReadOnly: true,
			panelIsHidden: !Controls.CurrentException__.GetValue()
		});
	},
	/**
	 * Hide technical fields as long as debug mode is not set
	 */
	HideTechnicalFields: function ()
	{
		// Header fields
		if (Controls.CurrentAttachmentFlag__.IsVisible())
		{
			// compatibility On upgrade
			// KeepFieldsProperties=1 on process may not have set hidden=true in layout
			Controls.HoldingComment__.Hide(true);
			Controls.CurrentAttachmentFlag__.Hide(true);
			Controls.ManualLink__.Hide(true);
			Controls.ExtractedNetAmount__.Hide(true);
			Controls.GRIV__.Hide(true);
			Controls.LastValidatorName__.Hide(true);
			Controls.LastValidatorUserId__.Hide(true);
			Controls.LastArchiveEditor__.Hide(true);
			Controls.LastArchiveEditionDate__.Hide(true);
			Controls.TouchlessPossible__.Hide(true);
			Controls.ERPPostingDate__.Hide(true);
			Controls.ERPLinkingDate__.Hide(true);
			Controls.ERPPaymentBlocked__.Hide(true);
			Controls.PaymentApprovalStatus__.Hide(true);
			Controls.SAP_Simulation_Result__.Hide(true);
			Controls.VerificationDate__.Hide(true);
			Controls.ScheduledActionDate__.Hide(true);
			Controls.ScheduledAction__.Hide(true);
			Controls.PortalRuidEx__.Hide(true);
			Controls.DigitalSignature__.Hide(true);
			Controls.Configuration__.Hide(true);
			Controls.ArchiveRuidEx__.Hide(true);
			Controls.ArchiveProcessLink__.Hide(true);
			Controls.ArchiveProcessLinkGenerated__.Hide(true);
			Controls.SelectedBankAccountID__.Hide(true);
			Controls.BudgetExportStatus__.Hide(true);
			Controls.ERPPostingError__.Hide(true);
			Controls.ERPAckRuidEx__.Hide(true);
			Controls.LastExportDate__.Hide(true);
			Controls.LastPaymentApprovalExportDate__.Hide(true);
			//Local amounts
			Controls.ExchangeRate__.Hide(true);
			Controls.LocalInvoiceAmount__.Hide(true);
			Controls.LocalNetAmount__.Hide(true);
			Controls.LocalTaxAmount__.Hide(true);
			Controls.LocalCurrency__.Hide(true);
			Controls.LocalEstimatedDiscountAmount__.Hide(true);
			Controls.LocalEstimatedLatePaymentFee__.Hide(true);
			//Coding template
			Controls.CodingTemplate__.Hide(true);
		}
		// Tables columns
		Controls.LineItems__.Keyword__.Hide(true);
		Controls.LineItems__.MultiTaxRates__.Hide(true);
		Controls.LineItems__.LineType__.Hide(true);
		Controls.LineItems__.PriceCondition__.Hide(true);
		Controls.LineItems__.TaxRate__.Hide(true);
		Controls.LineItems__.VendorNumber__.Hide(true);
		Controls.LineItems__.OpenAmount__.Hide(true);
		Controls.LineItems__.OpenQuantity__.Hide(true);
		Controls.LineItems__.PartNumber__.Hide(true);
		Controls.LineItems__.UnitPrice__.Hide(true);
		Controls.LineItems__.CCDescription__.Hide(true);
		Controls.LineItems__.ProjectCodeDescription__.Hide(true);
		Controls.LineItems__.WBSElementID__.Hide(true);
		Controls.LineItems__.FreeDimension1__.Hide(true);
		Controls.LineItems__.FreeDimension1ID__.Hide(true);
		Controls.LineItems__.IsLocalPO__.Hide(true);
		Controls.LineItems__.DifferentInvoicingParty__.Hide(true);
		Controls.LineItems__.AcctAssCat__.Hide(true);
		Controls.LineItems__.NoGoodsReceipt__.Hide(true);
		Controls.LineItems__.Group__.Hide(true);
		Controls.LineItems__.BudgetID__.Hide(true);
		Controls.LineItems__.TechnicalDetails__.Hide(true);
		Controls.BankDetails__.BankDetails_Type__.Hide(true);
		Controls.BankDetails__.BankDetails_ID__.Hide(true);
		// Workflow
		Controls.ApproversList__.ApproverID__.Hide(true);
		Controls.ApproversList__.WorkflowIndex__.Hide(true);
		Controls.ApproversList__.Approved__.Hide(true);
		Controls.ApproversList__.ApproverEmail__.Hide(true);
		Controls.ApproversList__.ApproverAction__.Hide(true);
		Controls.ApproversList__.WRKFIsGroup__.Hide(true);
		Controls.ApproversList__.ApprovalRequestDate__.Hide(true);
		Controls.ApproversList__.ActualApprover__.Hide(true);
		// History panel
		Controls.Comment.Hide(true);
		// deleted fields: avoid update glitches
		if (Controls.ToggleHistoryViewForApprovers__)
		{
			Controls.ToggleHistoryViewForApprovers__.Hide(true);
		}
		if (Controls.ConsignmentNumber__)
		{
			Controls.ConsignmentNumber__.Hide(true);
		}
		if (Controls.LineItems__.ConsignmentNumber__)
		{
			Controls.LineItems__.ConsignmentNumber__.Hide(true);
		}
	},
	UpdateManualLinkLayout: function ()
	{
		Lib.AP.GetInvoiceDocumentLayout().UpdateLayoutForManualLink();
	},
	SetCommonLayoutRestriction: function ()
	{
		LayoutHelper.UpdateManualLinkLayout();
		var bHideAsideReason = Controls.InvoiceStatus__.GetValue() !== Lib.AP.InvoiceStatus.SetAside && Controls.InvoiceStatus__.GetValue() !== Lib.AP.InvoiceStatus.OnHold;
		Controls.AsideReason__.Hide(bHideAsideReason);
		Controls.AsideReasonForApprovers__.Hide(bHideAsideReason);
		Controls.BackToAPReason__.Hide(true);
		Controls.RejectReason__.Hide(true);
		Controls.ERPInvoiceNumber__.SetBrowsable(false);
		Controls.ERPInvoiceNumber__.SetReadOnly(true);
		Controls.ERPMMInvoiceNumber__.SetBrowsable(false);
		Controls.ERPMMInvoiceNumber__.SetReadOnly(true);
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.ERPMMInvoiceNumber__, !Lib.AP.InvoiceType.isPOInvoice());
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.ERPClearingDocumentNumber__, !Lib.AP.InvoiceType.isConsignmentInvoice());
		// Can not add or remove reference
		Controls.DocumentsPanel.AllowChangeOrUploadReferenceDocument(false);
		Controls.Parameters.SetReadOnly(true);
		Controls.Parameters.Hide(true);
		Controls.Invoice_Processing.SetReadOnly(true);
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.Investment__, !Lib.P2P.IsInvestmentEnabled() || !Lib.AP.InvoiceType.isPOInvoice());
		// Fields allowed for approvers
		Controls.Comment__.SetReadOnly(ProcessInstance.isReadOnly);
		// Can remove only its document
		Controls.DocumentsPanel.SetNumberOfNonRemovableAttachements(Controls.CurrentAttachmentFlag__.GetValue());
		Controls.Payment.SetReadOnly(true);
		Controls.Details.SetReadOnly(true);
	},
	/**
	 * Put some fields in read-only and approver specific parameters
	 */
	SetApprovalLayout: function ()
	{
		LayoutHelper.SetCommonLayoutRestriction();
		var onHoldWarning = "";
		if (Controls.InvoiceStatus__.GetValue() === Lib.AP.InvoiceStatus.OnHold)
		{
			onHoldWarning = "_OnHold invoice status warning";
		}
		Data.SetWarning("InvoiceStatus__", onHoldWarning);
		Controls.HeaderDataPanel.SetReadOnly(true);
		Controls.OrderNumber__.SetBrowsable(false);
		Controls.VendorInformation.SetReadOnly(true);
		Controls.ERP_Details.SetReadOnly(true);
		Controls.CalculateTax__.SetReadOnly(!reviewersCanModifyLineItems());
		Controls.SAPPaymentMethod__.SetBrowsable(false);
		Controls.AlternativePayee__.SetBrowsable(false);
		Controls.ContractNumber__.SetBrowsable(false);
		Controls.ContractNumber__.SetReadOnly(true);
		Controls.ContractNumberDetails__.SetBrowsable(false);
		Controls.ContractNumberDetails__.SetReadOnly(true);
		Controls.ButtonLoadTemplate__.Hide(true);
		Controls.ButtonSaveTemplate__.Hide(true);
		Controls.ButtonLoadClipboard__.Hide(true);
		Controls.Line_Items_Actions.Hide(true);
		Controls.LineItems__.HideTableRowMenu(!reviewersCanModifyLineItems());
		this.UpdateWorkflowDataPanel(
		{
			approversListRowMenuIsHidden: true,
			approversListIsReadOnly: false,
			approverFieldIsBrowsable: false,
			approverFieldIsReadOnly: true
		});
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.UpdatePayment, !userCanUpdatePayment());
		// Allow adding reviewers or controllers
		handleApproversListEvents();
		this.UpdateExceptionsTypePanel(
		{
			// Cannot modify Exception
			panelIsReadOnly: true,
			panelIsHidden: !Controls.CurrentException__.GetValue()
		});
		BankDetails.hideAll();
		Controls.ExtendedWithholdingTaxPane.SetReadOnly(true);
	},
	InitPurchaseOrdersBrowse: function ()
	{
		if (!Lib.ERP.IsSAP() || Lib.AP.InvoiceType.isPOGLInvoice())
		{
			var invoiceDocument = Lib.AP.GetInvoiceDocument();
			Controls.OrderNumber__.OnBrowse = invoiceDocument.OnBrowsePurchaseOrders(Controls.OrderNumber__, genericAndSapPOGLAddLineItem, cleanUpLineItems, GetTaxRateForItemList, LayoutHelper);
		}
		else
		{
			var invoiceDocument = Lib.AP.GetInvoiceDocument();
			Controls.OrderNumber__.OnBrowse = invoiceDocument.OnBrowsePurchaseOrders(Controls.OrderNumber__, sapMMAddLineItem, cleanUpLineItems, GetTaxRateForItemList, LayoutHelper);
		}
	},
	/**
	 * Put some fields in read-only when the invoice was posted
	 */
	SetPostedLayout: function ()
	{
		LayoutHelper.SetCommonLayoutRestriction();
		Data.SetWarning("InvoiceStatus__", "");
		Controls.HeaderDataPanel.SetReadOnly(true);
		Controls.OrderNumber__.SetBrowsable(false);
		Controls.VendorInformation.SetReadOnly(true);
		Controls.Invoice_Processing.SetReadOnly(true);
		Controls.CalculateTax__.SetReadOnly(true);
		this.UpdateWorkflowDataPanel(
		{
			// Restore the ability to insert / delete lines in table in case of a back to AP
			approversListIsReadOnly: Lib.AP.InvoiceType.isConsignmentInvoice(),
			approverFieldIsReadOnly: true,
			approverFieldIsBrowsable: !ProcessInstance.isReadOnly
		});
		Lib.AP.GetInvoiceDocumentLayout().Hide(Controls.UpdatePayment, !userCanUpdatePayment());
		Controls.SAPPaymentMethod__.SetBrowsable(false);
		Controls.AlternativePayee__.SetBrowsable(false);
		Controls.BusinessArea__.SetBrowsable(false);
		Controls.ContractNumber__.SetBrowsable(false);
		Controls.ContractNumber__.SetReadOnly(true);
		Controls.ContractNumberDetails__.SetBrowsable(false);
		Controls.ContractNumberDetails__.SetReadOnly(true);
		Controls.ButtonLoadTemplate__.Hide(true);
		Controls.ButtonSaveTemplate__.Hide(true);
		Controls.ButtonLoadClipboard__.Hide(true);
		Controls.Line_Items_Actions.Hide(true);
		handleApproversListEvents();
		this.UpdateExceptionsTypePanel(
		{
			// Cannot modify Exception
			panelIsReadOnly: true,
			panelIsHidden: !Controls.CurrentException__.GetValue()
		});
		BankDetails.hide();
		Controls.ExtendedWithholdingTaxPane.SetReadOnly(true);
	},
	InitDecimalField: function (control)
	{
		if (control.GetValue() === null)
		{
			control.SetValue(0);
		}
	},
	ShouldEnableManualLink: function (invoiceStatus)
	{
		return !ProcessInstance.isReadOnly &&
			(invoiceStatus === Lib.AP.InvoiceStatus.ToPost || invoiceStatus === Lib.AP.InvoiceStatus.ToVerify || invoiceStatus === Lib.AP.InvoiceStatus.SetAside) &&
			!Lib.AP.InvoiceType.isPOGLInvoice() && !Lib.AP.InvoiceType.isConsignmentInvoice() && !Lib.AP.WorkflowCtrl.IsCurrentContributorLowPrivilegeAP();
	},
	/**
	 * Update the layout for data capture mode and performs basic checking upon data
	 */
	SetDataCaptureLayout: function ()
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		Controls.HeaderDataPanel.SetReadOnly(false);
		Controls.OrderNumber__.SetBrowsable(!ProcessInstance.isReadOnly && !Lib.AP.InvoiceType.isGLInvoice());
		Controls.VendorInformation.SetReadOnly(false);
		Controls.Invoice_Processing.SetReadOnly(false);
		Controls.CalculateTax__.SetReadOnly(false);
		Controls.Payment.SetReadOnly(false);
		Controls.Details.SetReadOnly(false);
		Controls.ContractNumber__.SetBrowsable(!ProcessInstance.isReadOnly);
		Controls.ContractNumber__.SetReadOnly(false);
		Controls.ContractNumberDetails__.SetBrowsable(false);
		Controls.ContractNumberDetails__.SetReadOnly(false);
		Controls.SAPPaymentMethod__.SetBrowsable(true);
		Controls.AlternativePayee__.SetBrowsable(true);
		Controls.BusinessArea__.SetBrowsable(true);
		layout.Hide(Controls.Investment__, !Lib.P2P.IsInvestmentEnabled() || !Lib.AP.InvoiceType.isPOInvoice());
		layout.Hide(Controls.ERPClearingDocumentNumber__, !Lib.AP.InvoiceType.isConsignmentInvoice());
		Controls.Investment__.Check(false);
		Controls.VendorContactEmail__.SetReadOnly(Sys.Helpers.IsEmpty(Controls.VendorNumber__.GetValue()) || !Sys.Helpers.IsEmpty(Controls.VendorContactEmail__.GetValue()));
		LayoutHelper.UpdateManualLinkLayout();
		var invoiceStatus = Controls.InvoiceStatus__.GetValue();
		Controls.AsideReason__.Hide(invoiceStatus !== Lib.AP.InvoiceStatus.SetAside);
		Controls.AsideReasonForApprovers__.Hide(invoiceStatus !== Lib.AP.InvoiceStatus.SetAside);
		Controls.BackToAPReason__.Hide(true);
		Controls.RejectReason__.Hide(true);
		layout.Hide(Controls.UpdatePayment, true);
		if (this.ShouldEnableManualLink(invoiceStatus))
		{
			layout.EnableManualLink();
			layout.UpdateLayoutForManualLink();
			layout.Hide(Controls.ERPMMInvoiceNumber__, false);
		}
		else
		{
			layout.DisableManualLink();
			layout.UpdateLayoutForManualLink();
			layout.Hide(Controls.ERPMMInvoiceNumber__, !Lib.AP.InvoiceType.isPOInvoice());
		}
		var reconciledByHeader = Variable.GetValueAsString("ReconciledByHeader");
		if (invoiceStatus === Lib.AP.InvoiceStatus.ToVerify &&
			reconciledByHeader &&
			reconciledByHeader.toUpperCase() === "TRUE")
		{
			Controls.ReconcileWarning__.SetWarningStyle(true);
			Controls.ReconcileWarning__.Hide(false);
		}
		Controls.TemplateWarning__.SetWarningStyle(true);
		Controls.TemplateWarning__.Hide(Data.GetWarning("CodingTemplate__") === null);
		Data.SetWarning("InvoiceStatus__", "");
		// Restore rights on the attachments list
		Controls.DocumentsPanel.SetNumberOfNonRemovableAttachements(0);
		Controls.DocumentsPanel.AllowChangeOrUploadReferenceDocument(true);
		this.UpdateWorkflowDataPanel(
		{
			// Restore the ability to insert / delete lines in table in case of a back to AP
			approversListIsReadOnly: false,
			approverFieldIsReadOnly: true,
			approverFieldIsBrowsable: !ProcessInstance.isReadOnly
		});
		// Restore the visibility of parameters panel
		Controls.Parameters.SetReadOnly(false);
		Controls.Parameters.Hide(false);
		// Add events handlers
		handleApproversListEvents();
		this.InitPurchaseOrdersBrowse();
		// initialize amounts
		LayoutHelper.InitDecimalField(Controls.NetAmount__);
		LayoutHelper.InitDecimalField(Controls.TaxAmount__);
		LayoutHelper.InitDecimalField(Controls.Balance__);
		var firstItem = Controls.LineItems__.GetItem(0);
		if (firstItem && firstItem.GetValue("TaxAmount__") === null)
		{
			firstItem.SetValue("TaxAmount__", 0);
		}
		Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
		// Check balance
		checkBalance(false);
		// Check PO line items and GL line items
		checkPOInvoiceLineItems();
		this.UpdateExceptionsTypePanel(
		{
			// FT-008110 - Price mismatch exception
			panelIsHidden: false,
			panelIsReadOnly: false
		});
		// To activate feature call ExceptionsType.Hide(false) instead
	},
	/**
	 * Check the header field and show the column on line items
	 */
	SwitchToGRIV: function ()
	{
		Controls.GRIV__.Check();
		Controls.LineItems__.DeliveryNote__.Hide(false);
	},
	/**
	 * Check the header field and show the column on line items
	 */
	SwitchToNonGRIV: function ()
	{
		Controls.GRIV__.Check(false);
		Controls.LineItems__.DeliveryNote__.Hide(true);
	},
	InitEventHistoryPanel: function ()
	{
		// Connversation options
		var options = {
			ignoreIfExists: false,
			notifyByEmail: true
		};
		Controls.ConversationUI__.SetOptions(options);
		// Show event history
		if (Data.GetValue("PortalRuidex__") && Data.GetValue("VendorContactEmail__"))
		{
			// Add vendor name into Event history panel title
			var title = Language.Translate("_Event_history", false, Data.GetValue("VendorName__"));
			Controls.Event_history.SetText(title);
			Controls.Event_history.Hide(false);
		}
		else
		{
			Controls.Event_history.Hide(true);
		}
	},
	DisplayContractsElements: function ()
	{
		if (g_p2pParameters.GetParameter("EnableContractGlobalSetting") === "1")
		{
			Controls.ContractNumber__.Hide(false);
			Controls.ContractNumberDetails__.Hide(false);
		}
	},
	InitWorkflowDataPanel: function ()
	{
		Lib.AP.WorkflowCtrl.UpdateLayout();
	},
	UpdateExceptionsTypePanel: function (options)
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		if (Sys.Parameters.GetInstance("AP").GetParameter("WorkflowDisableRules") === "1")
		{
			options.panelIsHidden = true;
			options.panelIsReadOnly = true;
			options.currentExceptionIsHidden = true;
		}
		var panelIsReadOnly = options.panelIsReadOnly || false;
		var currentExceptionIsHidden = options.currentExceptionIsHidden ? panelIsReadOnly && !Controls.CurrentException__.GetValue() : false;
		var panelIsHidden = options.panelIsHidden !== undefined ? options.panelIsHidden && !Controls.CurrentException__.GetValue() : currentExceptionIsHidden;
		Controls.ExceptionsType.SetReadOnly(panelIsReadOnly);
		Controls.ExceptionsType.Hide(panelIsHidden);
		layout.Hide(Controls.CurrentException__, currentExceptionIsHidden);
	},
	UpdateWorkflowDataPanel: function (options)
	{
		var layout = Lib.AP.GetInvoiceDocumentLayout();
		var noWorkflowRules = Sys.Parameters.GetInstance("AP").GetParameter("WorkflowDisableRules") === "1";
		if (noWorkflowRules === true)
		{
			options.approversListIsReadOnly = true;
			options.approverFieldIsReadOnly = true;
			options.approverFieldIsBrowsable = false;
		}
		var otherFieldAreReadOnly = true;
		if (noWorkflowRules === true)
		{
			Controls.WorkflowDataPanel.SetReadOnly(true);
			Controls.WorkflowDataPanel.Hide(true);
		}
		if (noWorkflowRules === true && !Controls.Comment__.GetValue())
		{
			Controls.Comment__.SetReadOnly(true);
			layout.Hide(Controls.Comment__, true);
		}
		if (options.approversListIsReadOnly === true || options.approversListIsReadOnly === false)
		{
			Controls.ApproversList__.SetReadOnly(options.approversListIsReadOnly);
			if (options.approversListIsReadOnly && Controls.ApproversList__.GetItemCount() === 0)
			{
				layout.Hide(Controls.ApproversList__, true);
			}
		}
		if (options.approversListRowMenuIsHidden === true || options.approversListRowMenuIsHidden === false)
		{
			Controls.ApproversList__.HideTableRowMenu(options.approversListRowMenuIsHidden);
		}
		if (options.approverFieldIsReadOnly === true || options.approverFieldIsReadOnly === false)
		{
			Controls.ApproversList__.Approver__.SetReadOnly(options.approverFieldIsReadOnly);
		}
		if (options.approverFieldIsBrowsable === true || options.approverFieldIsBrowsable === false)
		{
			Controls.ApproversList__.Approver__.SetBrowsable(options.approverFieldIsBrowsable);
		}
		Controls.ApproversList__.ApprovalDate__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApproverID__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.Approved__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApproverEmail__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApproverAction__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApproverComment__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.WorkflowIndex__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApproverLabelRole__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.LineMarker__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ApprovalRequestDate__.SetReadOnly(otherFieldAreReadOnly);
		Controls.ApproversList__.ActualApprover__.SetReadOnly(otherFieldAreReadOnly);
	},
	SetHelperIDBasedOnCurrentStepRole: function ()
	{
		if (currentStepIsApprover())
		{
			Process.SetHelpId(2527);
		}
		else if (currentStepIsController())
		{
			Process.SetHelpId(2528);
		}
		else
		{
			Process.SetHelpId(2008);
		}
	}
};
var ParameterDetails;
(function (ParameterDetails)
{
	ParameterDetails.originalValues = {
		DuplicateCheckAlertLevel__: "",
		TouchlessEnabled__: false
	};

	function hideAll()
	{
		Controls.ShowParameters__.Hide(true);
		ParameterDetails.hide();
	}
	ParameterDetails.hideAll = hideAll;

	function hide()
	{
		Controls.ParametersWarning__.Hide(true);
		Controls.ShowParameters__.SetText("_Show Parameters");
		Controls.ComputingParametersWaiting__.Hide(true);
		Controls.Duplicates__.Hide(true);
		Controls.DuplicateCheckAlertLevel__.Hide(true);
		Controls.AutomaticProcessing__.Hide(true);
		Controls.TouchlessEnabled__.Hide(true);
		Controls.TouchlessDone__.Hide(true);
		Controls.Duplicates__.Hide(true);
	}
	ParameterDetails.hide = hide;

	function show(warningDuplicateCheck, warningTouchlessCheck)
	{
		Controls.ShowParameters__.SetText("_Hide Parameters");
		var isGLWithoutTouchless = Lib.AP.InvoiceType.isGLInvoice() && Sys.Parameters.GetInstance("AP").GetParameter("EnableTouchlessForNonPoInvoice") !== "1";
		if (warningDuplicateCheck || warningTouchlessCheck)
		{
			Controls.ParametersWarning__.Hide(false);
			var warningMessage = "";
			var originalDuplicate = parseInt(ParameterDetails.originalValues.DuplicateCheckAlertLevel__, 10);
			var originalTouchless = ParameterDetails.originalValues.TouchlessEnabled__;
			var duplicateLabels = [
				"Do not alert on duplicates",
				"If at least 1 value is identical",
				"If at least 2 values are identical",
				"If at least 3 values are identical"
			];
			if (warningDuplicateCheck && warningTouchlessCheck)
			{
				warningMessage = Language.Translate(warningDuplicateCheck + warningTouchlessCheck.replace("{0}", "{1}"), true, Language.Translate(duplicateLabels[originalDuplicate]), Language.Translate(originalTouchless ? "_enabled" : "_disabled"));
			}
			else if (warningDuplicateCheck)
			{
				warningMessage = Language.Translate(warningDuplicateCheck, true, Language.Translate(duplicateLabels[originalDuplicate]));
			}
			else
			{
				warningMessage = Language.Translate(warningTouchlessCheck, true, Language.Translate(originalTouchless ? "_enabled" : "_disabled"));
			}
			Controls.ParametersWarning__.SetText(warningMessage);
		}
		else
		{
			Controls.ParametersWarning__.Hide(true);
		}
		Controls.Duplicates__.Hide(false);
		Controls.DuplicateCheckAlertLevel__.Hide(false);
		Controls.AutomaticProcessing__.Hide(isGLWithoutTouchless);
		Controls.TouchlessEnabled__.Hide(isGLWithoutTouchless);
		Controls.TouchlessDone__.Hide(isGLWithoutTouchless);
	}
	ParameterDetails.show = show;

	function onClick()
	{
		Controls.DuplicateCheckAlertLevel__.OnChange = null;
		Controls.TouchlessEnabled__.OnChange = null;
		if (Controls.Duplicates__.IsVisible())
		{
			ParameterDetails.hide();
		}
		else
		{
			Controls.ComputingParametersWaiting__.Hide(false);
			Controls.ComputingParametersWaiting__.Focus();
			if (Sys.Helpers.IsEmpty(ParameterDetails.originalValues.DuplicateCheckAlertLevel__))
			{
				ParameterDetails.originalValues.DuplicateCheckAlertLevel__ = Data.GetValue("DuplicateCheckAlertLevel__");
			}
			if (Sys.Helpers.IsEmpty(ParameterDetails.originalValues.TouchlessEnabled__))
			{
				var touchlessEnabled = Data.GetValue("TouchlessEnabled__");
				if (!touchlessEnabled)
				{
					ParameterDetails.originalValues.TouchlessEnabled__ = false;
				}
				else
				{
					ParameterDetails.originalValues.TouchlessEnabled__ = true;
				}
			}
			resetParameters();
			var callbackCheckForParametersChanges = function ()
			{
				Controls.ComputingParametersWaiting__.Hide(true);
				var warningDuplicateCheckContent = "";
				var warningTouchlessCheckContent = "";
				var isGLWithoutTouchless = Lib.AP.InvoiceType.isGLInvoice() && Sys.Parameters.GetInstance("AP").GetParameter("EnableTouchlessForNonPoInvoice") !== "1";
				if (ParameterDetails.originalValues.DuplicateCheckAlertLevel__ !== Data.GetValue("DuplicateCheckAlertLevel__"))
				{
					warningDuplicateCheckContent = "_Alert on duplicates changed {0}";
				}
				if (!isGLWithoutTouchless && ParameterDetails.originalValues.TouchlessEnabled__ !== Data.GetValue("TouchlessEnabled__"))
				{
					warningTouchlessCheckContent = "_Touchless changed {0}";
				}
				ParameterDetails.show(warningDuplicateCheckContent, warningTouchlessCheckContent);
				Controls.DuplicateCheckAlertLevel__.OnChange = ParameterDetails.onParametersChange;
				Controls.TouchlessEnabled__.OnChange = ParameterDetails.onParametersChange;
				Controls.DuplicateCheckAlertLevel__.Focus();
				Controls.TouchlessEnabled__.Focus();
			};
			if (Query.IsPendingRequest())
			{
				Query.NotifyOnCurrentRequestsDone(callbackCheckForParametersChanges);
			}
			else
			{
				callbackCheckForParametersChanges();
			}
		}
	}
	ParameterDetails.onClick = onClick;

	function onParametersChange()
	{
		Variable.SetValueAsString("ParametersChanged", "true");
	}
	ParameterDetails.onParametersChange = onParametersChange;
})(ParameterDetails || (ParameterDetails = {}));
var EmbeddedViews;
(function (EmbeddedViews)
{
	var viewsContext = {
		vendorRelatedContractsContext:
		{
			ctrlView: Controls.VendorRelatedContractsView__,
			ctrlNoItem: Controls.VendorRelatedContractsNoItem__,
			ctrlPanel: Controls.VendorRelatedContractsViewPanel,
			ctrlButton: Controls.ToggleVendorRelatedContractsView__,
			viewName: "_AP_SAP_View_Vendor related contracts - embedded",
			processName: "P2P - Contract",
			buttonShowLabel: "_ShowVendorRelatedContractsView",
			buttonHideLabel: "_HideVendorRelatedContractsView",
			hideButtonMethod: function ()
			{
				return g_p2pParameters.GetParameter("EnableContractGlobalSetting") !== "1";
			},
			fillViewMethod: function (context)
			{
				var selectedVendorNumber = Data.GetValue("VendorNumber__");
				var selectedVendorName = Data.GetValue("VendorName__");
				var selectedCompanyCode = Data.GetValue("CompanyCode__");
				if (selectedVendorName)
				{
					Controls.VendorRelatedContractsViewPanel.SetLabel(Language.Translate("_VendorRelatedContractsPanelForVendorX", true, selectedVendorName));
				}
				else
				{
					Controls.VendorRelatedContractsViewPanel.SetLabel(Language.Translate("_VendorRelatedContractsPanelDefault", true));
				}
				if (selectedVendorNumber && selectedCompanyCode)
				{
					var MAX_CONTRACT_IN_VIEW = 100;
					var processFields = "MsnEx";
					var sortOrder = "ReferenceNumber__ ASC";
					var filter = "(VendorNumber__=" + selectedVendorNumber + ")";
					filter = filter.AddCompanyCodeFilter(selectedCompanyCode);
					if (!context.viewFilter || context.viewFilter !== filter)
					{
						var disable = !Controls.VendorNumber__.GetValue() || !Controls.VendorName__.GetValue();
						Controls.ToggleVendorRelatedContractsView__.SetDisabled(disable);
						context.viewFilter = filter;
						Query.DBQuery(fillEmbeddedViewViewCallback, "CDNAME#" + "P2P - Contract", processFields, filter, sortOrder, MAX_CONTRACT_IN_VIEW, context);
					}
				}
				else
				{
					hideViewControl(context);
				}
			}
		},
		historyContext:
		{
			ctrlView: Controls.HistoryView__,
			ctrlNoItem: Controls.HistoryViewNoItem__,
			ctrlPanel: Controls.HistoryViewPanel,
			ctrlButton: Controls.ToggleHistoryView__,
			viewName: "_AP_SAP_View_Vendor history - embedded",
			processName: Process.GetName(),
			buttonShowLabel: "_ShowHistoryView",
			buttonHideLabel: "_HideHistoryView",
			fillViewMethod: function (context)
			{
				var selectedVendorNumber = Data.GetValue("VendorNumber__");
				var selectedVendorName = Data.GetValue("VendorName__");
				var selectedCompanyCode = Data.GetValue("CompanyCode__");
				var currentMsnEx = null;
				var processId = ProcessInstance.id;
				if (processId && processId.indexOf(".") >= 0)
				{
					currentMsnEx = processId.split(".")[1];
				}
				if (selectedVendorName)
				{
					Controls.HistoryViewPanel.SetLabel(Language.Translate("_HistoryViewPanelForVendorX", true, selectedVendorName));
				}
				else
				{
					Controls.HistoryViewPanel.SetLabel(Language.Translate("_HistoryViewPanelDefault", true));
				}
				if (selectedVendorNumber && selectedCompanyCode)
				{
					var MAX_INVOICE_IN_VIEW = 5;
					var processFields = "MsnEx";
					var sortOrder = "InvoiceDate__ DESC";
					var filter = "&(VendorNumber__=" + selectedVendorNumber + ")(InvoiceStatus__!=Received)(InvoiceStatus__!=To verify)(InvoiceStatus__!=Rejected)(State<=100)";
					if (currentMsnEx)
					{
						filter += "(MsnEx!=" + currentMsnEx + ")";
					}
					filter = filter.AddCompanyCodeFilter(selectedCompanyCode);
					if (!context.viewFilter || context.viewFilter !== filter)
					{
						context.viewFilter = filter;
						Query.DBQuery(fillEmbeddedViewViewCallback, "CDNAME#" + Process.GetName(), processFields, filter, sortOrder, MAX_INVOICE_IN_VIEW, context);
					}
				}
				else
				{
					hideViewControl(context);
				}
			}
		}
	};

	function hideEmbeddedViewPanel(context)
	{
		context.ctrlPanel.Hide(true);
		context.ctrlButton.SetText(context.buttonShowLabel);
		context.viewFilter = null;
		if (context.hideButtonMethod && context.hideButtonMethod())
		{
			context.ctrlButton.Hide(true);
		}
		else
		{
			var disable = !Controls.VendorNumber__.GetValue() || !Controls.VendorName__.GetValue();
			context.ctrlButton.SetDisabled(disable);
		}
	}
	/**
	 * Query.DBQuery callback
	 * @this ViewContext
	 * @param callbackResult
	 */
	function fillEmbeddedViewViewCallback(callbackResult)
	{
		var filter = {};
		if (callbackResult)
		{
			var queryValue = callbackResult.GetQueryValue();
			if (queryValue.Records && queryValue.Records.length > 0)
			{
				if (queryValue.Records.length > 0)
				{
					filter.MsnEx = [];
				}
				for (var _i = 0, _a = queryValue.Records; _i < _a.length; _i++)
				{
					var record = _a[_i];
					filter.MsnEx.push(record[0]);
				}
			}
		}
		fillView(this, filter);
	}

	function displayViewControl(context, jsonViewFilter)
	{
		context.ctrlView.SetFilterParameters(jsonViewFilter);
		context.ctrlView.Hide(false);
		context.ctrlNoItem.Hide(true);
		context.ctrlView.Apply();
	}

	function hideViewControl(context)
	{
		context.ctrlView.SetFilterParameters(
		{
			MsnEx: "0"
		});
		context.ctrlView.Hide(true);
		context.ctrlNoItem.Hide(false);
		context.ctrlView.Apply();
	}

	function initAllViewControls()
	{
		Sys.Helpers.Object.ForEach(viewsContext, function (context)
		{
			context.ctrlView.SetView("_My documents-AP-Embedded", context.viewName, context.processName);
			context.ctrlView.CheckProfileTab(false);
		});
	}

	function fillView(context, filter)
	{
		if (filter && filter.MsnEx)
		{
			displayViewControl(context, filter);
		}
		else
		{
			hideViewControl(context);
		}
	}

	function OnClickToggleButton(contextName)
	{
		var context = viewsContext[contextName];
		if (context)
		{
			var paneVisible = context.ctrlPanel.IsVisible();
			context.ctrlPanel.Hide(paneVisible);
			if (!paneVisible)
			{
				context.ctrlButton.SetText(context.buttonHideLabel);
				context.fillViewMethod(context);
			}
			else
			{
				context.ctrlButton.SetText(context.buttonShowLabel);
			}
		}
	}
	EmbeddedViews.OnClickToggleButton = OnClickToggleButton;

	function InitAllEmbeddedViewPanels()
	{
		EmbeddedViews.HideAllEmbeddedViewPanels();
	}
	EmbeddedViews.InitAllEmbeddedViewPanels = InitAllEmbeddedViewPanels;

	function FillVisibleViews()
	{
		Sys.Helpers.Object.ForEach(viewsContext, function (context)
		{
			if (context.ctrlPanel.IsVisible())
			{
				context.fillViewMethod(context);
			}
		});
	}
	EmbeddedViews.FillVisibleViews = FillVisibleViews;

	function SetDisabledAllButtons(disable)
	{
		Sys.Helpers.Object.ForEach(viewsContext, function (context)
		{
			context.ctrlButton.SetDisabled(disable);
		});
	}
	EmbeddedViews.SetDisabledAllButtons = SetDisabledAllButtons;

	function HideAllEmbeddedViewPanels()
	{
		Sys.Helpers.Object.ForEach(viewsContext, function (context)
		{
			hideEmbeddedViewPanel(context);
		});
	}
	EmbeddedViews.HideAllEmbeddedViewPanels = HideAllEmbeddedViewPanels;
	initAllViewControls();
})(EmbeddedViews || (EmbeddedViews = {}));
/**
 * Handle all events and functionality for the Holds pane
 */
var Holds;
(function (Holds)
{
	/**
	 * Only display the pane and the table if active holds exists
	 */
	function manageVisibility()
	{
		Controls.ActiveHoldsCount__.Hide(true);
		Controls.LastHoldReleaseDate__.Hide(true);
		var count = Data.GetValue("ActiveHoldsCount__");
		Controls.HoldsPane.Hide(!count || count <= 0);
	}
	Holds.manageVisibility = manageVisibility;
})(Holds || (Holds = {}));
/**
 * Handle all events and functionality for the bank details button and panel
 */
var BankDetails;
(function (BankDetails)
{
	/**
	 * Fill the BankDetails__ table with the associated bank accounts
	 * @param {Object[]} account array of bank account informations
	 * @param {string} account.Country Country of the bank
	 * @param {string} account.Name Name of the bank
	 * @param {string} account.Account Account number
	 * @param {string} account.Holder Name of the holder of the account
	 * @param {string} account.IBAN IBAN
	 */
	function fillTableWithResults(bankAccountsArray)
	{
		Controls.BankDetails__.SetItemCount(0);
		if (!bankAccountsArray)
		{
			displayTableOrNoItem(false);
			return;
		}
		Controls.BankDetails__.SetItemCount(bankAccountsArray.length);
		var erpManager = Lib.AP.GetInvoiceDocument();
		Controls.BankDetails__.BankDetails_Select__.Hide(erpManager.IsBankDetailsSelectionDisabled());
		for (var i = 0; i < bankAccountsArray.length; i++)
		{
			var item = Controls.BankDetails__.GetItem(i);
			var account = bankAccountsArray[i];
			for (var p in account)
			{
				if (Object.prototype.hasOwnProperty.call(account, p))
				{
					item.SetValue("BankDetails_" + p + "__", account[p]);
				}
			}
			if (erpManager.IsBankDetailsItemSelected(item))
			{
				item.SetValue("BankDetails_Select__", true);
			}
		}
		displayTableOrNoItem(false);
	}

	function displayTableOrNoItem(queryInProgress)
	{
		var accountsExists = Controls.BankDetails__.GetItemCount() > 0;
		if (queryInProgress)
		{
			Controls.ComputingBankDetailsWaiting__.Hide(false);
			Controls.Spacer_line__.Hide(true);
			Controls.BankDetailsNoItem__.Hide(true);
			Controls.BankDetails__.Hide(true);
		}
		else
		{
			Controls.ComputingBankDetailsWaiting__.Hide(true);
			Controls.Spacer_line__.Hide(accountsExists);
			Controls.BankDetailsNoItem__.Hide(accountsExists);
			Controls.BankDetails__.Hide(!accountsExists);
		}
	}

	function hideAll()
	{
		Controls.ShowBankDetails__.Hide(true);
		BankDetails.hide();
	}
	BankDetails.hideAll = hideAll;

	function hide()
	{
		Controls.BankDetailsPane.Hide(true);
		Controls.ComputingBankDetailsWaiting__.Hide(true);
		Controls.ShowBankDetails__.SetText("_ShowBankDetails");
		Controls.BankDetailsPane.SetLabel(Language.Translate("_BankDetailsPane", true));
		Controls.ShowBankDetails__.SetDisabled(!Controls.VendorNumber__.GetValue() || !Controls.VendorName__.GetValue());
		displayTableOrNoItem(false);
	}
	BankDetails.hide = hide;

	function show()
	{
		Controls.ShowBankDetails__.SetText("_HideBankDetails");
		Controls.BankDetailsPane.Hide(false);
		displayTableOrNoItem(false);
	}
	BankDetails.show = show;

	function getBankDetails(erpManager)
	{
		displayTableOrNoItem(true);
		Controls.ShowBankDetails__.SetDisabled(!Controls.VendorNumber__.GetValue() || !Controls.VendorName__.GetValue());
		var vendorName = Data.GetValue("VendorName__");
		if (vendorName)
		{
			Controls.BankDetailsPane.SetLabel(Language.Translate("_BankDetailsPaneForVendorX", true, vendorName));
		}
		else
		{
			Controls.BankDetailsPane.SetLabel(Language.Translate("_BankDetailsPane", true));
		}
		var parameters = {
			companyCode: Data.GetValue("CompanyCode__"),
			vendorNumber: Data.GetValue("VendorNumber__")
		};
		erpManager.GetVendorBankDetails(parameters, fillTableWithResults);
	}
	BankDetails.getBankDetails = getBankDetails;

	function onClick()
	{
		if (Controls.BankDetailsPane.IsVisible())
		{
			BankDetails.hide();
		}
		else
		{
			BankDetails.show();
			BankDetails.getBankDetails(Lib.AP.GetInvoiceDocument());
		}
	}
	BankDetails.onClick = onClick;
	/**
	 * event when Bank details selection changes
	 * @this CheckBox in Bank Detail
	 */
	function onCheckBankDetails()
	{
		var row = this.GetRow();
		var backupSelectValue = row.BankDetails_Select__.GetValue();
		var erpManager = Lib.AP.GetInvoiceDocument();
		// store the selected banktype
		erpManager.StoreSelectedBankDetails(row);
		// reset all checkboxes
		for (var i = 0; i < Data.GetTable("BankDetails__").GetItemCount(); i++)
		{
			Data.GetTable("BankDetails__").GetItem(i).SetValue("BankDetails_Select__", false);
		}
		// ensure current line is checked
		row.BankDetails_Select__.SetValue(backupSelectValue);
	}
	BankDetails.onCheckBankDetails = onCheckBankDetails;

	function onRefreshRow(index)
	{
		var erpManager = Lib.AP.GetInvoiceDocument();
		if (!erpManager.IsBankDetailsSelectionDisabled())
		{
			var row = Controls.BankDetails__.GetRow(index);
			if (row)
			{
				row.BankDetails_Select__.SetValue(false);
				if (!erpManager.IsBankDetailsRowSelectable(row))
				{
					row.BankDetails_Select__.SetReadOnly(true);
				}
				else
				{
					row.BankDetails_Select__.SetReadOnly(false);
					if (erpManager.IsBankDetailsItemSelected(row.GetItem()))
					{
						row.BankDetails_Select__.SetValue(true);
					}
				}
			}
		}
	}
	BankDetails.onRefreshRow = onRefreshRow;
})(BankDetails || (BankDetails = {}));
/** ***************** **/
/** SPECIFIC HANDLERS **/
/** ***************** **/
/**
 * @class Help to manage PO invoice line item
 */
var POLineItemHelper = {
	// Determine the behavior of line item check when posting
	// Possible values are:
	//		idx =- 1, check if there is an empty line item (quantity or amount) and popup a confirmation message to ignore them in post
	//		idx >= 0, check has been performed and an empty line item has been found (the value is the one of its index)
	idx: -1,
	ShouldCheck: function ()
	{
		return this.idx === -1;
	},
	HasEmptyLine: function ()
	{
		return this.idx >= 0;
	},
	SetEmptyLine: function (idx)
	{
		this.idx = idx;
	},
	Clear: function ()
	{
		this.idx = -1;
	}
};
/**
 *@class Help to manage ERP communication and display in a FlexibleForm
 */
var ERP = {
	IsPostOk: function ()
	{
		// Close or go to the next document
		if (!ProcessInstance.Next("next"))
		{
			ProcessInstance.Quit("quit");
		}
	},
	ShowERPPopup: function ()
	{
		this.ShowPostERPPopup();
		this.ShowUnblockPaymentPopup();
		this.ShowMissingERPInvoiceNumberPopup();
	},
	ShowManualUnblockPaymentConfirmation: function ()
	{
		Popup.Dialog("_Confirmation", null, function (dialog)
		{
			dialog.HideDefaultButtons();
			dialog.AddDescription("ConfirmationMessage").SetText("_Manually unblock in SAP confirmation");
			dialog.AddDescription("ConfirmationMessage2").SetText("_Manually unblock in SAP confirmation line 2");
			dialog.AddButton("ConfirmButton", "_Confirm button");
			dialog.AddButton("CancelButton", "_Cancel");
		}, null, null, function (dialog, tabId, event, control)
		{
			if (event === "OnClick")
			{
				if (control.GetName() === "ConfirmButton")
				{
					ButtonsBehavior.approve("ManualUnblockPayment");
				}
				dialog.Cancel();
			}
		});
	},
	ShowUnblockPaymentPopup: function ()
	{
		var fillCB = function (dialog)
		{
			var ERPErrors = Data.GetError("ERPInvoiceNumber__");
			dialog.AddDescription("Description").SetText("_ERP Unblock payment failed with the following reason:");
			dialog.AddDescription("ErrorDetails").SetText(ERPErrors);
			if (!ProcessInstance.isReadOnly)
			{
				dialog.AddDescription("Tip").SetText("_ERPUnblockPaymentButtonsDescription");
				dialog.HideDefaultButtons();
				dialog.AddButton("RetryButton", "_Retry payment unblock");
				dialog.AddButton("ManuallyDoneButton", "_Manually done");
			}
		};
		var handleCB = function (dialog, tabId, event, control)
		{
			if (tabId === null && event === "OnClick")
			{
				dialog.Cancel();
				switch (control.GetName())
				{
				case "RetryButton":
					ButtonsBehavior.approve("RetryUnblockPayment", true);
					break;
				case "ManuallyDoneButton":
					ERP.ShowManualUnblockPaymentConfirmation();
					break;
				default:
					break;
				}
			}
		};
		if (Data.GetValue("ERPPaymentBlocked__") && Data.GetValue("InvoiceStatus__") === "To pay")
		{
			Popup.Dialog("_Unblock payment warning", null, fillCB, null, null, handleCB);
		}
	},
	ShowPostERPPopup: function ()
	{
		if (Data.GetActionName() !== "Post" || Controls.ManualLink__.IsChecked())
		{
			return;
		}
		var ERPPostErrors = Data.GetError("ERPInvoiceNumber__");
		var ERPInvoiceNumber = Data.GetValue("ERPInvoiceNumber__");
		if (ERPPostErrors)
		{
			Popup.Alert(ERPPostErrors, true, null, "_SAP Posting errors");
		}
		else if (ERPInvoiceNumber)
		{
			Popup.Alert(Language.Translate("_SAP Posting ok") + "\r\n\r\n" + Language.Translate("_SAP Invoice number") + " : " + ERPInvoiceNumber, false, this.IsPostOk, "_SAP Post successful");
		}
	},
	/**
	 * This function checks if the simulation result should be displayed
	 */
	ShouldDisplaySimulationPopup: function ()
	{
		return Variable.GetValueAsString("Simulation_Report") &&
			!ProcessInstance.isReadOnly &&
			Data.GetActionName() === "Simulate";
	},
	DisplaySimulationResult: function ()
	{
		if (this.ShouldDisplaySimulationPopup())
		{
			Controls.SAP_Simulation_Result__.SetHTML(Variable.GetValueAsString("Simulation_Report"));
			Popup.Dialog("_Simulation results", null, this.FillSimulationPopup);
		}
	},
	FillSimulationPopup: function (dialog)
	{
		var htmlControl = dialog.AddHTML("HTML_POPUP", "", "0");
		htmlControl.SetCSS(Controls.SAP_Simulation_Result__.GetCSS());
		htmlControl.SetHTML(Controls.SAP_Simulation_Result__.GetHTML());
	},
	ShowMissingERPInvoiceNumberPopup: function ()
	{
		var fillERPInvoiceNumber = function (dialog)
		{
			dialog.AddDescription("Description").SetText(Language.Translate("_ErrorGettingERPInvoiceNumber", true, Data.GetValue("ERPMMInvoiceNumber__")));
			if (!ProcessInstance.isReadOnly)
			{
				dialog.AddDescription("Tip").SetText("_ERPUnblockPaymentButtonsDescription");
				dialog.HideDefaultButtons();
				dialog.AddButton("RetryPostButton", "_RetryERPInvoiceNumber");
				dialog.AddButton("ManualERPInvoiceNumberButton", "_FillERPInvoiceNumberManually");
			}
		};
		var handleERPInvoiceNumber = function (dialog, tabId, event, control)
		{
			if (tabId === null && event === "OnClick")
			{
				dialog.Cancel();
				switch (control.GetName())
				{
				case "RetryPostButton":
					Controls.Post.OnClick();
					break;
				case "ManualERPInvoiceNumberButton":
					ERP.ShowManualERPInvoiceNumberPopup();
					break;
				default:
					break;
				}
			}
		};
		if (!Data.IsNullOrEmpty("ERPMMInvoiceNumber__") && Data.IsNullOrEmpty("ERPInvoiceNumber__"))
		{
			Popup.Dialog("_MissingERPInvoiceNumber", null, fillERPInvoiceNumber, null, null, handleERPInvoiceNumber);
		}
	},
	ShowManualERPInvoiceNumberPopup: function ()
	{
		var fillERPInvoiceNumberManually = function (dialog)
		{
			dialog.AddDescription("Description").SetText(Language.Translate("_ERPInvoiceNumberManuallyFilled", true, Data.GetValue("ERPMMInvoiceNumber__")));
			if (!ProcessInstance.isReadOnly)
			{
				dialog.AddText("ManualERPInvoiceNumber", "_FIInvoiceNumber");
				dialog.HideDefaultButtons();
				dialog.AddButton("ValidateERPInvoiceNumber", "_ValidateAndPost");
				dialog.AddButton("Cancel", "_Cancel");
			}
		};
		var handleERPInvoiceNumberManually = function (dialog, tabId, event, control)
		{
			if (tabId === null && event === "OnClick")
			{
				dialog.Cancel();
				if (control.GetName() === "ValidateERPInvoiceNumber")
				{
					Data.SetValue("ERPInvoiceNumber__", dialog.GetControl("ManualERPInvoiceNumber").GetText());
					Controls.Post.OnClick();
				}
				else
				{
					ERP.ShowMissingERPInvoiceNumberPopup();
				}
			}
		};
		Popup.Dialog("_FillERPInvoiceNumber", null, fillERPInvoiceNumberManually, null, null, handleERPInvoiceNumberManually);
	}
};
/**
 * @class Help to manage DuplicateCheck display in a FlexibleForm
 */
var DuplicateCheck = {
	/**
	 * Contains the result of the server side duplicate check
	 */
	Result: null,
	ActionPost: false,
	/**
	 * Create the table that will contains all the duplicate invoices
	 * @param {object} dialog The Dialog object in which the table will be display
	 * @param {string} tableName The expected name of the table to create
	 * @return {Table} The created table
	 */
	CreateResultTable: function (dialog, tableName)
	{
		var table = dialog.AddTable(tableName, null, 250);
		table.AddTextColumn("InvoiceNumber", "_Invoice number", 90);
		table.AddDecimalColumn("InvoiceAmount", "_Amount", 80);
		table.AddDateColumn("InvoiceDate", "_Date", 80);
		table.AddTextColumn("InvoiceStatus", "_Invoice status", 120);
		table.AddTextColumn("OwnerLogin", "_Owner", 300);
		var link = table.AddLinkColumn("View", " ", 50);
		link.SetText("_View");
		link.SetURL("");
		table.SetReadOnly(true);
		table.SetRowToolsHidden(true);
		return table;
	},
	/**
	 * Help to fill a line item from a duplicate invoice
	 * @param {TableItem} item The line item to fill
	 * @param {DuplicateItem} duplicate A duplicateItem which contains the requested informations
	 */
	FillResultItem: function (item, duplicate)
	{
		item.SetValue("InvoiceNumber", duplicate.controlsToCheck.InvoiceNumber__);
		item.SetValue("InvoiceAmount", duplicate.controlsToCheck.InvoiceAmount__);
		item.SetValue("InvoiceDate", duplicate.controlsToCheck.InvoiceDate__);
		item.SetValue("InvoiceStatus", Language.Translate(duplicate.additionalAttributes.InvoiceStatus__));
		item.SetValue("OwnerLogin", duplicate.additionalAttributes.OwnerLogin);
	},
	/**
	 * This function fill the duplicate check popup
	 * @param {object} dialog The Dialog object to fill
	 */
	FillAlertDuplicateDialog: function (dialog)
	{
		dialog.SetHelpId(2521);
		var duplicateWarning = dialog.AddTitle("duplicateLabel1", "");
		duplicateWarning.SetText(Language.Translate("_Risk of duplicates with the following invoice(s)"));
		var tableCurrentInvoice = DuplicateCheck.CreateResultTable(dialog, "duplicateTable");
		tableCurrentInvoice.SetItemCount(DuplicateCheck.Result.length);
		for (var i = 0; i < DuplicateCheck.Result.length; i++)
		{
			var duplicate = DuplicateCheck.Result[i];
			DuplicateCheck.FillResultItem(tableCurrentInvoice.GetItem(i), duplicate);
		}
		var actionName = Data.GetActionName();
		var actionIsEligible = actionName && actionName !== "Resubmit" && actionName !== "Reprocess" && actionName !== "Save" && actionName !== "Request_teaching";
		if (DuplicateCheck.ActionPost || actionIsEligible)
		{
			dialog.HideDefaultButtons();
			dialog.AddButton("backToInvoice", "_Back to the invoice");
			var ignoreButton = dialog.AddButton("ignoreDuplicate", Language.Translate("_Confirm duplicate {0}", false, Language.Translate(Controls.Post.GetLabel())));
			ignoreButton.SetWarningStyle();
		}
		Controls.Post.Wait(false);
	},
	/**
	 * This function handle all events from the popup
	 * @param {object} dialog The Dialog from which event are raised
	 * @param {integer} tabId The index of the tab from which event are raised
	 * @param {string} event The event name raised
	 * @param {object} control The control which raised the event
	 */
	HandleAlertDuplicateDialog: function (dialog, tabId, event, control)
	{
		if (control.GetType() === "Button" && event === "OnClick")
		{
			dialog.Cancel();
			if (control.GetName() === "ignoreDuplicate")
			{
				Variable.SetValueAsString("DuplicateCheckResult", "");
				Variable.SetValueAsString("DoNotCheckDuplicates", "1");
				ButtonsBehavior.approve("Post");
			}
		}
		else if (control.GetType() === "Link" && event === "OnClick")
		{
			var lineNumber = control.GetRow().GetLineNumber() - 1;
			if (lineNumber < DuplicateCheck.Result.length)
			{
				control.SetURL("FlexibleForm.aspx?action=run&layout=_flexibleform&ReadOnly=1&Id=" + DuplicateCheck.Result[lineNumber].RUIDEX.replace(/#/g, "%23") + "&OnQuit=Close");
			}
		}
	},
	/**
	 * This function checks if the duplicate popup is supposed to be shown
	 */
	ShouldPopup: function (checkAction)
	{
		var shouldPopup = Variable.GetValueAsString("DoNotCheckDuplicates") !== "1" && !ProcessInstance.isReadOnly && currentStepIsApStart() && !Controls.ERPPostingDate__.GetValue();
		if (shouldPopup && checkAction)
		{
			var currentAction = Data.GetActionName();
			if (currentAction)
			{
				shouldPopup = currentAction === "Reprocess" || currentAction === "Post";
			}
			else
			{
				shouldPopup = Data.GetValue("InvoiceStatus__") !== Lib.AP.InvoiceStatus.SetAside;
			}
		}
		return shouldPopup;
	},
	/**
	 * This function check if duplicate invoices exist and decided to display or not the popup
	 */
	DoCheck: function (checkAction)
	{
		if (this.ShouldPopup(checkAction))
		{
			// Retrieve duplicates array from external var
			var duplicateCheckResults = Variable.GetValueAsString("DuplicateCheckResult");
			var duplicateResult = duplicateCheckResults ? JSON.parse(duplicateCheckResults) : null;
			if (duplicateResult && duplicateResult.length > 0)
			{
				this.Result = duplicateResult;
				Popup.Dialog("_Duplicate detected", null, this.FillAlertDuplicateDialog, null, null, this.HandleAlertDuplicateDialog, null);
				return false;
			}
		}
		return true;
	},
	/** Check if any duplicate invoices exist
	 * Fill the variable DuplicateCheckResult with all the duplicates found
	 * @return true if no duplicate, false if at least one duplicate found
	 */
	DuplicateCheck: function (callBackDuplicate)
	{
		// Disable DuplicateCheck if invoice was already posted
		if (Data.GetValue("ERPPostingDate__"))
		{
			Log.Info("No duplicate check after posting invoice");
			return;
		}
		var duplicateKeyControls = ["CompanyCode__", "VendorNumber__"];
		var controlsToCheck = ["InvoiceNumber__", "InvoiceDate__", "InvoiceAmount__"];
		var queryOptions = {
			// 16 months to cover a fiscal year
			MaxDateRangeInDays: 480,
			DateRangeFieldName: "InvoiceDate__",
			SortOnControlName: "InvoiceDate__",
			AdditionalAttributes: ["InvoiceStatus__", "OwnerId"],
			CustomFilter: ""
		};
		var invoiceRef = Lib.AP.ParseInvoiceDocumentNumber(Data.GetValue("InvoiceReferenceNumber__"), true);
		if (invoiceRef && invoiceRef.documentNumber)
		{
			if (!invoiceRef.isFI)
			{
				Log.Info("PO Invoice reference detected: ignore " + Data.GetValue("InvoiceReferenceNumber__") + " from duplicate check");
				queryOptions.CustomFilter += "(!(ERPMMInvoiceNumber__=" + Data.GetValue("InvoiceReferenceNumber__") + "))";
			}
			else
			{
				Log.Info("Non-PO Invoice reference detected: ignore " + Data.GetValue("InvoiceReferenceNumber__") + " from duplicate check");
				queryOptions.CustomFilter += "(!(ERPInvoiceNumber__=" + Data.GetValue("InvoiceReferenceNumber__") + "))";
			}
		}
		Lib.DuplicateCheck.CheckDuplicate(duplicateKeyControls, controlsToCheck, queryOptions, parseInt(Data.GetValue("DuplicateCheckAlertLevel__"), 10), callBackDuplicate);
	},
	/**
	 * Check duplicate if needed
	 * Call the approveCallBack if duplicate check does not return duplicates or if user validates duplicates
	 */
	DoDuplicateCheck: function (approveCallBack)
	{
		//If we have to do the duplicate check
		this.ActionPost = true;
		if (DuplicateCheck.ShouldPopup(false))
		{
			var duplicateCheckCallBack = function (duplicates)
			{
				if (duplicates && duplicates.length > 0)
				{
					for (var _i = 0, duplicates_1 = duplicates; _i < duplicates_1.length; _i++)
					{
						var duplicateItem = duplicates_1[_i];
						var login = duplicateItem.additionalAttributes.OwnerId;
						login = login.substring(login.indexOf("cn=") + 3, login.indexOf(","));
						duplicateItem.additionalAttributes.OwnerLogin = login;
					}
					Variable.SetValueAsString("DuplicateCheckResult", JSON.stringify(duplicates));
				}
				else
				{
					Variable.SetValueAsString("DuplicateCheckResult", "");
				}
				Controls.Post.Wait(false);
				if (DuplicateCheck.DoCheck(false))
				{
					approveCallBack();
				}
			};
			DuplicateCheck.DuplicateCheck(duplicateCheckCallBack);
		}
		else
		{
			approveCallBack();
		}
	}
};
/**
 * Help to manage TaxCode Browse page
 */
var TaxCodeHelper = {
	showSnackBar: false,
	cachedValues: [],
	onBrowse: Controls.LineItems__.TaxCode__.OnBrowse,
	getMultipleTaxesSeparator: function ()
	{
		return Lib.AP.TaxHelper.getMultipleTaxesSeparator();
	},
	resetCache: function ()
	{
		TaxCodeHelper.cachedValues = [];
		Controls.LineItems__.TaxCode__.SetMultiSelectionMode(Lib.AP.TaxHelper.useMultipleTaxes());
	},
	init: function ()
	{
		Controls.LineItems__.TaxCode__.SetMultiSelectionMode(Lib.AP.TaxHelper.useMultipleTaxes());
		Controls.LineItems__.TaxCode__.OnEnter = Controls.LineItems__.TaxCode__.OnFocus = function ()
		{
			TaxCodeHelper.showSnackBar = false;
			TaxCodeHelper.resetCache();
		};
		Controls.LineItems__.TaxCode__.OnBrowse = function ()
		{
			var args = [];
			for (var _i = 0; _i < arguments.length; _i++)
			{
				args[_i] = arguments[_i];
			}
			TaxCodeHelper.showSnackBar = true;
			TaxCodeHelper.resetCache();
			TaxCodeHelper.onBrowse.apply(this, args);
		};
		Controls.LineItems__.TaxCode__.OnSelectItem = function (item)
		{
			var useMultipleTaxes = Lib.AP.TaxHelper.useMultipleTaxes();
			var value = item.GetValue("TaxCode__");
			// as the tax code is automatically set by the framework
			// we only have to consider the multiple taxes case to override the value
			if (useMultipleTaxes)
			{
				var newCodeAdded = false;
				if (TaxCodeHelper.cachedValues.indexOf(value) === -1)
				{
					TaxCodeHelper.cachedValues.push(value);
					newCodeAdded = true;
				}
				else if (TaxCodeHelper.showSnackBar)
				{
					// duplicate
					Popup.Snackbar(
					{
						message: Language.Translate("_Tax code already added", true, value),
						status: "info"
					});
				}
				value = TaxCodeHelper.cachedValues.join(TaxCodeHelper.getMultipleTaxesSeparator());
				this.SetValue(value);
				if (newCodeAdded && TaxCodeHelper.showSnackBar)
				{
					Popup.Snackbar(
					{
						message: Language.Translate("_Tax code updated", true, value),
						status: "success"
					});
				}
			}
		};
	}
};
/** Approvers **/
function currentStepIsApStart()
{
	return Lib.AP.WorkflowCtrl.GetCurrentStepRole() === Lib.AP.WorkflowCtrl.roles.apStart;
}

function currentStepIsController()
{
	return Lib.AP.WorkflowCtrl.GetCurrentStepRole() === Lib.AP.WorkflowCtrl.roles.controller;
}

function currentStepIsApprover()
{
	return Lib.AP.WorkflowCtrl.GetCurrentStepRole() === Lib.AP.WorkflowCtrl.roles.approver;
}

function reviewersCanModifyLineItems()
{
	return g_apParameters.GetParameter("WorkflowReviewersCanAddLineItems") === "1" && Data.GetValue("InvoiceType__") === Lib.AP.InvoiceType.NON_PO_INVOICE;
}

function currentStepCanModifyLineItems()
{
	var controllerCanModify = currentStepIsController() && reviewersCanModifyLineItems();
	return currentStepIsApStart() || controllerCanModify;
}

function addApprover()
{
	var insertApproverDialog = null;
	var currentRole = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
	if (currentRole === Lib.AP.WorkflowCtrl.roles.controller)
	{
		Popup.Dialog("_Add controller", null, fillInsertApproverDialog, commitInsertApproverDialog, null, handleInsertApproverDialog);
	}
	else
	{
		Popup.Dialog("_Add approver", null, fillInsertApproverDialog, commitInsertApproverDialog, null, handleInsertApproverDialog);
	}

	function fillInsertApproverDialog(dialog)
	{
		insertApproverDialog = dialog;
		var approverCtrl;
		if (currentRole === Lib.AP.WorkflowCtrl.roles.controller)
		{
			approverCtrl = dialog.AddText("approver", "_Controller");
		}
		else
		{
			approverCtrl = dialog.AddText("approver", "_Approver");
		}
		approverCtrl.SetWidth(252);
		approverCtrl.SetReadOnly(true);
		approverCtrl.SetBrowsable(true);
		dialog.RequireControl(approverCtrl);
		var commentCtrl = dialog.AddMultilineText("comment", "_Comment", 252);
		commentCtrl.SetValue(getReliableComment());
		dialog.AddDescription("msg");
		// Pop browse page for approvers automatically
		dialog.Hold(true);
		if (g_selectedAddApprover)
		{
			fillConfirmation(g_selectedAddApprover);
		}
		else
		{
			browseApprovers(currentRole).Then(fillConfirmation);
		}
	}

	function handleInsertApproverDialog(dialog, tabId, event, control)
	{
		if (event === "OnBrowse" && control.GetName() === "approver")
		{
			browseApprovers(currentRole).Then(fillConfirmation);
		}
	}

	function fillConfirmation(approver)
	{
		if (approver)
		{
			g_selectedAddApprover = approver;
			insertApproverDialog.Hold(false);
			insertApproverDialog.GetControl("approver").SetValue(approver.displayName);
			insertApproverDialog.GetControl("msg").SetText("_Add approver confirm message", approver.displayName);
		}
		else
		{
			// If no approver has been selected, close the popup
			insertApproverDialog.Cancel();
		}
	}

	function commitInsertApproverDialog()
	{
		//Update comment
		Controls.Comment__.SetValue(insertApproverDialog.GetControl("comment").GetValue());
		//Insert approver in control and forward
		if (!g_bApproverAdded)
		{
			Lib.AP.WorkflowCtrl.AddContributorAt(Lib.AP.WorkflowCtrl.GetCurrentStep(), g_selectedAddApprover, currentRole);
		}
		g_bApproverAdded = true;
		ButtonsBehavior.approve("AddApprover");
	}
}

function browseApprovers(role)
{
	var title = role === Lib.AP.WorkflowCtrl.roles.controller ? "_Controller Information" : "_Approver Information";
	var additionalFilterArray = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.GetContributorsExtraFilter", role) || [];
	additionalFilterArray.push("CUSTOMER=0");
	additionalFilterArray.push("VENDOR=0");
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		Lib.P2P.Browse.BrowseUsers(title, null, additionalFilterArray, true).Then(resolve);
	});
}
/**
 * Browse Add line item type in PO mode
 */
function fillInsertPOLineOrGLLineDialog(dialog)
{
	var browseMessageCtrl = dialog.AddDescription("HelpText", null);
	browseMessageCtrl.SetText("_Kind of line popup question");
	dialog.AddSeparator();
	var radio = dialog.AddRadioButton("lineTypeChoice");
	radio.SetText("_NON-PO Line\n_PO Line");
	radio.SetValue("_PO Line");
}

function commitInsertPOLineOrGLLineDialog(dialog, tabId)
{
	if (!tabId)
	{
		// callback from base dialog
		var newValue = dialog.GetControl("lineTypeChoice").GetValue();
		switch (newValue)
		{
		case "_PO Line":
			Controls.OrderNumber__.OnBrowse();
			break;
		case "_NON-PO Line":
			InvoiceLineItem.AddGLLineItem(Controls.LineItems__);
			cleanUpLineItems();
			break;
		default:
			break;
		}
	}
}
/** Amounts and balance **/
function computeCallback(amountField)
{
	var invoiceDocument = Lib.AP.GetInvoiceDocument();
	Controls.NetAmount__.SetValue(invoiceDocument.layout.computeHeaderAmount());
	Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
	invoiceDocument.ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
	invoiceDocument.layout.updateLocalAmounts(Data.GetValue("ExchangeRate__"));
	if (amountField)
	{
		Data.SetValue("Local" + amountField, Data.GetValue(amountField) * Data.GetValue("ExchangeRate__"));
	}
	checkBalance(true);
}

function onFinalizeGetSAPTaxRate()
{
	computeTaxAmount();
	computeCallback();
	LayoutHelper.DisableButtons(false, "GetTaxRate");
}

function onSuccessGetSAPTaxRate(lineItem, taxRate)
{
	var itemList = g_taxCodes[lineItem.GetValue("TaxCode__")];
	for (var _i = 0, itemList_1 = itemList; _i < itemList_1.length; _i++)
	{
		var item = itemList_1[_i];
		setItemTaxRateAndTaxAmount(item, taxRate);
	}
}

function onErrorGetSAPTaxRate(lineItem, error, errorField)
{
	var itemList = g_taxCodes[lineItem.GetValue("TaxCode__")];
	for (var _i = 0, itemList_2 = itemList; _i < itemList_2.length; _i++)
	{
		var item = itemList_2[_i];
		if (error)
		{
			if (!errorField)
			{
				errorField = "TaxCode__";
			}
			item.SetError(errorField, error);
			setItemTaxRateAndTaxAmount(item, 0);
		}
	}
}

function onFinalizeGetTaxRateForTable()
{
	// update balance
	computeTaxAmount();
	computeCallback();
	LayoutHelper.DisableButtons(false, "GetTaxRateForTable");
}

function GetTaxRateForItemList()
{
	return Sys.Helpers.Promise.Create(function (resolve, reject)
	{
		var taxCodesCount = Object.keys(g_taxCodes).length;
		if (taxCodesCount > 0)
		{
			var invoiceDocument_1 = Lib.AP.GetInvoiceDocument();
			if (Lib.ERP.IsSAP())
			{
				LayoutHelper.DisableButtons(true, "GetTaxRate");
				var taxArray = [];
				for (var taxCode in g_taxCodes)
				{
					if (Object.prototype.hasOwnProperty.call(g_taxCodes, taxCode))
					{
						taxArray.push(g_taxCodes[taxCode][0]);
					}
				}
				taxArray.reduce(function (p, currentTax)
					{
						return p.Then(function ()
						{
							return invoiceDocument_1.GetTaxRateAsync(currentTax)
								.Then(function (result)
								{
									onSuccessGetSAPTaxRate(result.item, result.taxRate);
								})
								.Catch(function (result)
								{
									onErrorGetSAPTaxRate(result.item, result.error, result.errorField);
								});
						});
					}, Sys.Helpers.Promise.Resolve())
					.Then(function ()
					{
						onFinalizeGetSAPTaxRate();
						resolve();
					})
					.Catch(function ()
					{
						onFinalizeGetSAPTaxRate();
						reject();
					});
			}
			else
			{
				LayoutHelper.DisableButtons(true, "GetTaxRateForTable");
				invoiceDocument_1.GetTaxRateForTableAsync(g_taxCodes)
					.Then(function (itemsTaxRate)
					{
						for (var _i = 0, itemsTaxRate_1 = itemsTaxRate; _i < itemsTaxRate_1.length; _i++)
						{
							var tax = itemsTaxRate_1[_i];
							if (tax.exists)
							{
								updateItemsWithTaxRate(tax.items, tax.taxRates, tax.taxRoundingPriorities);
							}
							else
							{
								setTaxCodeInErrorWithInvalidTaxes(tax.items, "Field value does not belong to table!");
							}
						}
						onFinalizeGetTaxRateForTable();
						resolve();
					});
			}
		}
		else
		{
			//Compute all header amount when coming from browse PO
			computeCallback();
			resolve();
		}
	});
}

function getTaxRateAndUpdateItem(item, overrideSuccessCallback, stackCall)
{
	if (item.GetValue("TaxCode__") && Controls.CalculateTax__.IsChecked())
	{
		var taxCode = item.GetValue("TaxCode__");
		if (stackCall)
		{
			var isPresent = typeof g_taxCodes[taxCode] === "object";
			if (!isPresent)
			{
				g_taxCodes[taxCode] = [];
			}
			g_taxCodes[taxCode].push(item);
		}
		else
		{
			LayoutHelper.DisableButtons(true, "GetTaxRate");
			Lib.AP.GetInvoiceDocument().GetTaxRate(item,
				// success callback
				overrideSuccessCallback ? overrideSuccessCallback : updateItemWithTaxRate,
				// error callback
				function (it, error, field)
				{
					if (error)
					{
						if (field)
						{
							it.SetError(field, error);
						}
						else
						{
							Popup.Alert(error, true, null, "_Error while computing the tax amount");
						}
					}
					updateItemWithTaxRate(it, 0);
				},
				// finalize callback
				function ()
				{
					LayoutHelper.DisableButtons(false, "GetTaxRate");
				});
		}
	}
	else if (!stackCall)
	{
		updateItemWithTaxRate(item, 0);
	}
}

function autolearnExtractedNetAmount()
{
	// Autolearn ExtractedNetAmount if balance is good and amount was modified
	if (!Controls.Balance__.GetError() && Controls.NetAmount__.GetValue() !== Controls.ExtractedNetAmount__.GetValue())
	{
		Controls.ExtractedNetAmount__.SetReadOnly(false);
		Controls.ExtractedNetAmount__.SetValue(Controls.NetAmount__.GetValue());
		// remove the computed flag so the field is autolearned
		Data.SetComputed("ExtractedNetAmount__", false);
		Controls.ExtractedNetAmount__.SetReadOnly(true);
		Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, false);
	}
}

function setItemTaxRateAndTaxAmount(item, taxRates, roundingModes)
{
	var TaxHelper = Lib.ERP.IsSAP() ? Lib.AP.SAP.TaxHelper : Lib.AP.TaxHelper;
	var taxRate = TaxHelper.setTaxRate(item, taxRates, roundingModes);
	var taxAmount = Sys.Helpers.Round(Lib.AP.ApplyTaxRate(item.GetValue("Amount__"), taxRate, item.GetValue("MultiTaxRates__")), Lib.AP.GetAmountPrecision());
	item.SetValue("TaxAmount__", taxAmount);
}

function setTaxCodeInErrorWithInvalidTaxes(items, error)
{
	if (!items || items.length === 0 || !error)
	{
		return;
	}
	for (var _i = 0, items_1 = items; _i < items_1.length; _i++)
	{
		var item = items_1[_i];
		item.SetError("TaxCode__", error);
	}
}
/**
 * Set the taxRate to the items
 * @param {Item[]} items The list of LineItems to update
 * @param {float} taxRate The tax rate amount computed to assign to the items
 */
function updateItemsWithTaxRate(items, taxRate, taxRoundingPriorities)
{
	if (items)
	{
		for (var _i = 0, items_2 = items; _i < items_2.length; _i++)
		{
			var item = items_2[_i];
			setItemTaxRateAndTaxAmount(item, taxRate, taxRoundingPriorities);
		}
	}
}

function updateItemWithTaxRate(item, taxrate, taxRoundingModes)
{
	setItemTaxRateAndTaxAmount(item, taxrate, taxRoundingModes);
	computeTaxAmount();
	checkBalance(true);
}

function computeTaxAmount()
{
	Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
	Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
}

function isBalanceInThreshold()
{
	var threshold = parseFloat(Variable.GetValueAsString("BalanceThreshold"));
	var balance = Data.GetValue("Balance__");
	return Math.abs(balance) <= threshold;
}

function doCheckBalance()
{
	checkBalance(true);
}

function checkBalance(computeAmounts)
{
	var balance = 0;
	if (Data.GetValue("ManualLink__"))
	{
		Controls.Balance__.SetValue(0);
		Controls.Balance__.SetError("");
		Controls.Balance__.SetWarning("");
	}
	var deliveryCosts = Controls.UnplannedDeliveryCosts__.GetValue();
	if (!deliveryCosts)
	{
		if (computeAmounts)
		{
			var invoiceamount = Controls.InvoiceAmount__.GetValue();
			var netamount = Controls.NetAmount__.GetValue();
			var taxamount = Controls.TaxAmount__.GetValue();
			balance = invoiceamount - netamount - taxamount;
			balance = Sys.Helpers.Round(balance, Lib.AP.GetAmountPrecision());
			Controls.Balance__.SetValue(balance);
			Lib.AP.WorkflowCtrl.Rebuild(true, currentStepIsApStart(), "balanceUpdated");
		}
		else
		{
			balance = Controls.Balance__.GetValue();
		}
		if (!isBalanceInThreshold())
		{
			Controls.Balance__.SetError("_Balance is not null");
		}
		else if (balance !== 0)
		{
			Controls.Balance__.SetError("");
			Controls.Balance__.SetWarning("_Balance is not null");
		}
		else
		{
			Controls.Balance__.SetError("");
			Controls.Balance__.SetWarning("");
		}
	}
	else
	{
		Controls.Balance__.SetValue(0);
		Controls.Balance__.SetError("");
		Controls.Balance__.SetWarning("_Balance is not compute when unplanned delivery costs are specified");
	}
	Controls.ButtonSaveTemplate__.SetDisabled(balance !== 0);
}

function checkInvoiceCurrency()
{
	function callbackUpdateCompanyCodeCurrency(results, error)
	{
		var currencyExists = false;
		if (!error && results && results.length > 0)
		{
			var invoiceCurrency = Data.GetValue("InvoiceCurrency__");
			for (var _i = 0, results_1 = results; _i < results_1.length; _i++)
			{
				var result = results_1[_i];
				if (invoiceCurrency === result.CurrencyFrom__)
				{
					currencyExists = true;
					g_invoiceCurrency = invoiceCurrency;
					break;
				}
			}
		}
		if (currencyExists)
		{
			Data.SetError("InvoiceCurrency__", "");
			updateExchangeRate(doCheckBalance);
		}
		else
		{
			setExchangeRate(0, doCheckBalance);
		}
	}
	Lib.P2P.ExchangeRate.GetCompanyCodeCurrencies(Controls.CompanyCode__.GetValue(), callbackUpdateCompanyCodeCurrency);
}

function isInApprovalWorkflow()
{
	var invoiceStatus = Controls.InvoiceStatus__.GetValue();
	return invoiceStatus === Lib.AP.InvoiceStatus.ToApprove || invoiceStatus === Lib.AP.InvoiceStatus.ToApproveBeforeClearing || invoiceStatus === Lib.AP.InvoiceStatus.OnHold;
}

function userIsAPOrAdmin()
{
	var isAPOrAdmin = User.profileName === "Accounts Payable Profile" || User.profileRole === "accountManagement";
	var customIsAPOrAdmin = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.IncludeCustomAPProfiles", isAPOrAdmin);
	return typeof customIsAPOrAdmin === "boolean" ? customIsAPOrAdmin : isAPOrAdmin;
}

function checkPOInvoiceLineItemTableVendor()
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (InvoiceLineItem.IsPOLineItem(item) || InvoiceLineItem.IsPOGLLineItem(item))
			{
				checkPOInvoiceLineItemVendor(item);
			}
		}
	}
}

function checkPOInvoiceLineItemVendor(item)
{
	if ((InvoiceLineItem.IsPOLineItem(item) || InvoiceLineItem.IsPOGLLineItem(item)) &&
		(!(item.GetValue("OrderNumber__") || item.GetValue("GoodIssue__")) || validatePOInvoiceLine(item)))
	{
		item.SetWarning("OrderNumber__");
		item.SetWarning("GoodIssue__");
	}
}

function checkConsignmentLineItemQuantity(item)
{
	if (Lib.P2P.InvoiceLineItem.IsConsignmentLineItem(item) && !Data.GetValue("ERPInvoiceNumber__"))
	{
		var warning = "";
		//Quantity
		var expectedQuantity = item.GetValue("ExpectedQuantity__");
		var netQuantity = item.GetValue("Quantity__");
		if (netQuantity > expectedQuantity)
		{
			warning = Language.Translate("_This value exceeds the expected quantity ({0})", false, decimalToString(expectedQuantity));
			item.SetWarning("Quantity__", warning);
		}
	}
}

function checkPOInvoiceLineItemAmount(item, dispatchMap)
{
	if (InvoiceLineItem.IsPOLineItem(item) || InvoiceLineItem.IsPOGLLineItem(item))
	{
		var warning = "";
		if (!dispatchMap)
		{
			dispatchMap = InvoiceLineItem.GetDispatchedLinesAmountQuantity();
		}
		var key = InvoiceLineItem.GetDispatchKey(item);
		var netAmount = dispatchMap[key] ? dispatchMap[key].amount : item.GetValue("Amount__");
		var hasRefDocument = item.GetValue("OrderNumber__") || item.GetValue("GoodIssue__");
		if (hasRefDocument && netAmount > 0)
		{
			var openAmount = item.GetValue("OpenAmount__");
			var expectedAmount = item.GetValue("ExpectedAmount__");
			if (netAmount > expectedAmount)
			{
				warning = Language.Translate("_This value exceeds the expected amount ({0})", false, decimalToString(expectedAmount));
			}
			else if (netAmount > openAmount)
			{
				warning = Language.Translate("_This value exceeds the amount still to be invoiced ({0})", false, decimalToString(openAmount));
			}
		}
		item.SetWarning("Amount__", warning);
	}
}

function checkPOInvoiceLineItemAmountQuantity(item)
{
	if (InvoiceLineItem.IsPOLineItem(item) || InvoiceLineItem.IsPOGLLineItem(item))
	{
		if (item.GetValue("Quantity__") === null)
		{
			if (item.GetValue("Amount__") === null)
			{
				item.SetError("Amount__");
			}
			else if (!Data.GetValue("SubsequentDocument__"))
			{
				item.SetError("Quantity__", "This field is required!");
			}
		}
		if (item.GetValue("Amount__") === null)
		{
			if (item.GetValue("Quantity__") === null)
			{
				item.SetError("Quantity__");
			}
			else
			{
				item.SetError("Amount__", "This field is required!");
			}
		}
	}
}

function checkPOInvoiceLineItems()
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		var dispatchMap = InvoiceLineItem.GetDispatchedLinesAmountQuantity();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			checkPOInvoiceLineItemAmount(item, dispatchMap);
			Lib.AP.GetInvoiceDocumentLayout().checkPOInvoiceLineItemQuantity(item, InvoiceLineItem, dispatchMap);
			checkPOInvoiceLineItemAmountQuantity(item);
			checkPOInvoiceLineItemVendor(item);
			checkConsignmentLineItemQuantity(item);
		}
	}
}
/** ******************************** **/
/** CHECK ORDER NUMBER/VENDOR NUMBER **/
/** ******************************** **/
function validatePOInvoiceLine(item)
{
	var valid = true;
	var diffInvoicingParty = item.GetValue("DifferentInvoicingParty__");
	if (Lib.ERP.IsSAP())
	{
		diffInvoicingParty = Sys.Helpers.String.SAP.TrimLeadingZeroFromID(item.GetValue("DifferentInvoicingParty__"));
	}
	var validVendorNumber = [item.GetValue("VendorNumber__") || "", diffInvoicingParty || ""];
	var vendorNumber = Data.GetValue("VendorNumber__");
	if (!vendorNumber ||
		Sys.Helpers.Array.FindIndex(validVendorNumber, function (id)
		{
			return id && id.toLowerCase() === vendorNumber.toLowerCase();
		}) === -1)
	{
		item.SetWarning("OrderNumber__", "_This order number is not associated to the current vendor");
		item.SetWarning("GoodIssue__", "_This good issue number is not associated to the current vendor");
		valid = false;
	}
	return valid;
}

function validateLine(item, requiredFields)
{
	var valid = true;
	// Validate all the required fields
	if (requiredFields)
	{
		for (var field in requiredFields.LineItems__)
		{
			if (requiredFields.isRequired(requiredFields.LineItems__[field], item) && !item.GetValue(field))
			{
				item.SetError(field, "This field is required!");
				valid = false;
			}
		}
	}
	var lineType = item.GetValue("LineType__");
	if (lineType === Lib.P2P.LineType.PO || lineType === Lib.P2P.LineType.POGL)
	{
		valid = validatePOInvoiceLine(item) && valid;
	}
	return valid;
}

function validateLineItems(requiredFields)
{
	var valid = true;
	if (!Data.GetValue("ManualLink__"))
	{
		var lineItems = Data.GetTable("LineItems__");
		var nbItems = lineItems.GetItemCount();
		if (nbItems <= 0)
		{
			valid = false;
		}
		for (var i = 0; i < nbItems; i++)
		{
			valid = validateLine(lineItems.GetItem(i), requiredFields) && valid;
			var tableRow = Controls.LineItems__.GetRow(i);
			if (tableRow)
			{
				tableRow.TaxCode__.Focus();
				tableRow.OrderNumber__.Focus();
			}
		}
	}
	return valid;
}

function DisableTableValuesOnly(control, arrayParams)
{
	for (var _i = 0, arrayParams_1 = arrayParams; _i < arrayParams_1.length; _i++)
	{
		var param = arrayParams_1[_i];
		if (control && control[param] && control[param].IsReadOnly())
		{
			if (typeof control[param].SetAllowTableValuesOnly === "function")
			{
				control[param].SetError("");
				control[param].SetAllowTableValuesOnly(false);
			}
			else
			{
				Log.Warn("Lib.AP.UncheckedHeaderFieldsDuringApproval - field " + param + " is not a DatabaseComboBox");
			}
		}
	}
}

function DisableTableValuesOnlyInApprovalWkf()
{
	if (isInApprovalWorkflow())
	{
		DisableTableValuesOnly(Controls, Lib.AP.UncheckedHeaderFieldsDuringApproval);
		var nbLines = Math.min(Controls.LineItems__.GetItemCount(), Controls.LineItems__.GetLineCount());
		for (var i = 0; i < nbLines; i++)
		{
			DisableTableValuesOnly(Controls.LineItems__.GetRow(i), Lib.AP.UncheckedLineItemsFieldsDuringApproval);
		}
	}
}

function validateHeader(requiredFields)
{
	Controls.CompanyCode__.Focus();
	Controls.PostingDate__.Focus();
	Controls.VendorNumber__.Focus();
	Controls.VendorName__.Focus();
	Controls.InvoiceDate__.Focus();
	Controls.InvoiceAmount__.Focus();
	Controls.InvoiceCurrency__.Focus();
	if (requiredFields)
	{
		for (var field in requiredFields.Header)
		{
			if (requiredFields.isRequired(requiredFields.Header[field]))
			{
				Lib.AP.GetInvoiceDocumentLayout().CallControlFunction(null, field, "Focus");
			}
		}
	}
	//Back to the 1st field to show errors on InvoiceCurrency
	Controls.CompanyCode__.Focus();
}
/** ********************* **/
/** Vendor Contact Helper **/
/** ********************* **/
var vendorContact = {
	/**
	 * Retrieve the ShortLogin__ of the vendor Contact from the Vendors links table
	 *
	 * @param {string} vendorNumber : vendorNumber filter for the query
	 * @param {string} companyCode : companyCode filter for the query
	 * @param {function} callback : Callback when the query is finished
	 */
	GetVendorLinkRecord: function (vendorNumber, companyCode, callback)
	{
		if (!vendorNumber)
		{
			Log.Warn("GetVendorLinkRecord : No vendor number, cannot find vendor");
			return;
		}
		var filter = "(Number__=" + vendorNumber + ")";
		if (companyCode)
		{
			filter = "(&(CompanyCode__=" + companyCode + ")" + filter + ")";
		}
		Sys.GenericAPI.Query("AP - Vendors links__", filter, ["ShortLogin__"], callback, null, 1);
	},
	/**
	 * Retrieve the EmailAddress and the DiplayName of the vendor Contact
	 *
	 * @param {string} vendorNumber : vendorNumber filter for the query
	 * @param {string} companyCode : companyCode filter for the query
	 * @param {function} callback : Callback when the query is finished
	 */
	GetVendorContactUser: function (vendorNumber, companyCode, callback)
	{
		var GetVendorLinkDetail = function (result, error)
		{
			if (result && result[0])
			{
				var filter = "(&(VENDOR=1)(Login=" + User.accountId + "$" + result[0].ShortLogin__ + "))";
				Sys.GenericAPI.Query("ODUSER", filter, ["DiplayName", "EmailAddress"], callback, null, 1);
			}
			else if (error)
			{
				Log.Warn("GetVendorContactUser : " + error);
			}
			else if (callback)
			{
				callback();
			}
			else
			{
				Controls.VendorContactEmail__.SetValue("");
				Controls.VendorContactEmail__.SetReadOnly(false);
			}
		};
		this.GetVendorLinkRecord(vendorNumber, companyCode, GetVendorLinkDetail);
	},
	FillVendorContactEmail: function ()
	{
		Controls.VendorContactEmail__.SetError("");
		Controls.VendorContactEmail__.SetReadOnly(true);
		Controls.ContactVendor__.SetDisabled(true);
		var vendorNumber = Controls.VendorNumber__.GetValue(),
			companyCode = Controls.CompanyCode__.GetValue(),
			fillEmail = function (result, error)
			{
				if (result && result[0] && !Sys.Helpers.IsEmpty(result[0].EmailAddress))
				{
					Controls.VendorContactEmail__.SetValue(result[0].EmailAddress);
					Controls.ContactVendor__.SetDisabled(false);
				}
				else
				{
					Controls.VendorContactEmail__.SetValue("");
					if (error)
					{
						Log.Warn("FillVendorContactEmail : " + error);
					}
					else if (Sys.Parameters.GetInstance("AP").GetParameter("EnablePortalAccountCreation") !== "0")
					{
						Controls.VendorContactEmail__.SetReadOnly(false);
					}
				}
			};
		if (vendorNumber && companyCode)
		{
			this.GetVendorContactUser(Controls.VendorNumber__.GetValue(), Controls.CompanyCode__.GetValue(), fillEmail);
		}
		else
		{
			Controls.VendorContactEmail__.SetValue("");
		}
	}
};
/** **************** **/
/** Link to SIM      **/
/** **************** **/
function OpenCompanyDashboard()
{
	var vendorNumber = Controls.VendorNumber__.GetValue(),
		companyCode = Controls.CompanyCode__.GetValue();
	if (vendorNumber && companyCode)
	{
		Lib.P2P.SIM.OpenCompanyDashboard(vendorNumber, companyCode);
	}
}
Controls.SupplierInformationManagementLink__.SetDisabled(!Data.GetValue("CompanyCode__") || !Data.GetValue("VendorNumber__"));
/** **************** **/
/** Simulation Popup **/
/** **************** **/
function handleSimulationResult(simulateResult, simulationReport)
{
	function handleAlertSimulationDialog(dialog, tabId, event, control)
	{
		if (control.GetType() === "Button" && event === "OnClick")
		{
			dialog.Cancel();
			if (control.GetName() === "PostFromSimulationPopUp")
			{
				Controls.Post.OnClick();
			}
		}
	}

	function fillSimulationPopup(dialog)
	{
		var htmlControl = dialog.AddHTML("HTML_POPUP", "", "0");
		dialog.HideDefaultButtons();
		dialog.AddButton("backToInvoice", "_Back to the invoice");
		if (simulationReport.CounterByType.errors === 0)
		{
			var buttonPostFromSimulationPopUp = dialog.AddButton("PostFromSimulationPopUp", Controls.Post.GetLabel());
			buttonPostFromSimulationPopUp.SetSubmitStyle();
		}
		htmlControl.SetCSS(Controls.SAP_Simulation_Result__.GetCSS());
		htmlControl.SetHTML(Controls.SAP_Simulation_Result__.GetHTML());
	}
	// Show simulation popup
	Controls.SAP_Simulation_Result__.SetHTML(simulateResult);
	Popup.Dialog("_Simulation results", null, fillSimulationPopup, null, null, handleAlertSimulationDialog, null);
}
/** ************************* **/
/** Load lines from clipboard **/
/** ************************* **/
var LoadClipboard = {
	// By default, the mapping will be built based on the header line
	mapping: null,
	noMapping: [],
	noMappingKey: "_Load clipboard no mapping",
	rawData: "",
	showPaste: true,
	showPreview: false,
	data: null,
	comboMap:
	{},
	reversedComboMap:
	{},
	mappingComboValues: [],
	availableColumns: null,
	mappingCtrls: null,
	init: function ()
	{
		// Try to get mapping from user exit
		// Else use the one from the parameters
		// Else autodetermine mapping from header line
		var loadClipboardMapping = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.GetLineItemsImporterMapping");
		if (LoadClipboard.checkMapping(loadClipboardMapping))
		{
			LoadClipboard.mapping = loadClipboardMapping;
			Log.Info("LoadClipboard - Using mapping from users exit");
		}
		else
		{
			LoadClipboard.mapping = {
				columns: []
			};
			// Try to load mapping from vendor parameters
			var lineItemsImporterMapping = Variable.GetValueAsString("LineItemsImporterMapping");
			if (lineItemsImporterMapping)
			{
				try
				{
					LoadClipboard.mapping = Sys.Helpers.ParseObject(lineItemsImporterMapping);
					Log.Info("LoadClipboard - Using mapping from parameters table");
				}
				catch (err)
				{
					Log.Warn("LoadClipboard - Line items importer mapping, wrong JSON format");
				}
			}
		}
		LoadClipboard.noMapping = [];
		LoadClipboard.rawData = "";
		LoadClipboard.showPaste = true;
		LoadClipboard.showPreview = false;
		LoadClipboard.data = null;
		LoadClipboard.comboMap = {};
		LoadClipboard.reversedComboMap = {};
		LoadClipboard.mappingComboValues = [LoadClipboard.noMappingKey];
		// Add default columns for mapping design
		if (!loadClipboardMapping || !loadClipboardMapping.customMap)
		{
			LoadClipboard.addColumnInMap("Description__");
			LoadClipboard.addColumnInMap("Amount__");
			LoadClipboard.addColumnInMap("GLAccount__");
			LoadClipboard.addColumnInMap("CostCenter__");
			LoadClipboard.addColumnInMap("TaxCode__");
		}
		else
		{
			var customMap = loadClipboardMapping.customMap;
			for (var i = 0; i < customMap.length; i++)
			{
				LoadClipboard.addColumnInMap(customMap[i]);
			}
		}
	},
	addColumnInMap: function (c)
	{
		if (!LoadClipboard.reversedComboMap[c])
		{
			LoadClipboard.comboMap[Language.Translate(Controls.LineItems__[c].GetLabel())] = c;
			LoadClipboard.reversedComboMap[c] = Language.Translate(Controls.LineItems__[c].GetLabel());
			LoadClipboard.mappingComboValues.push(Controls.LineItems__[c].GetLabel());
		}
	},
	checkMapping: function (mapping)
	{
		// Check that mapping loaded is valid
		return mapping && mapping.columns && Sys.Helpers.IsArray(mapping.columns);
	},
	getSeparator: function (mapping)
	{
		return mapping && mapping.separator ? mapping.separator : "\t";
	},
	hasHeader: function (mapping)
	{
		return mapping && Sys.Helpers.IsBoolean(mapping.hasHeader) ? mapping.hasHeader : true;
	},
	getAvailableColumns: function ()
	{
		if (LoadClipboard.availableColumns)
		{
			return LoadClipboard.availableColumns;
		}
		var columns = Controls.LineItems__.GetColumnsOrder();
		LoadClipboard.availableColumns = {};
		for (var _i = 0, columns_2 = columns; _i < columns_2.length; _i++)
		{
			var columnName = columns_2[_i];
			var column = Controls.LineItems__[columnName];
			var obj = {
				name: columnName,
				type: column.GetType()
			};
			LoadClipboard.availableColumns[columnName.toLowerCase()] = obj;
			LoadClipboard.availableColumns[Language.Translate(column.GetLabel()).toLowerCase()] = obj;
		}
		return LoadClipboard.availableColumns;
	},
	nameify: function (name)
	{
		return name.replace(/\s|\W/g, "_").replace(/\b_*/g, "");
	},
	guessColumn: function (headerName)
	{
		var map = LoadClipboard.getAvailableColumns();
		var candidate = map[headerName.toLowerCase()];
		if (candidate)
		{
			return {
				columnName: candidate.name,
				type: candidate.type,
				found: true
			};
		}
		return {
			columnName: LoadClipboard.nameify(headerName),
			type: "Text",
			found: false
		};
	},
	addOrModifyMapping: function (mapping, m)
	{
		if (mapping && m && m.lineItemsColumn)
		{
			for (var _i = 0, _a = mapping.columns; _i < _a.length; _i++)
			{
				var column = _a[_i];
				if (m.lineItemsColumn === column.lineItemsColumn)
				{
					column.header = m.header;
					column.type = m.type;
					return column;
				}
				if (m.header === column.header)
				{
					column.lineItemsColumn = m.lineItemsColumn;
					column.type = m.type;
					return column;
				}
			}
			mapping.columns.push(m);
		}
		return null;
	},
	parseHeader: function (header, mapping)
	{
		LoadClipboard.showPreview = false;
		if (!header)
		{
			return null;
		}
		LoadClipboard.noMapping = [];
		var lineFormat = [];
		var fields = header.split(LoadClipboard.getSeparator(mapping));
		for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++)
		{
			var field = fields_1[_i];
			var mappingFound = false;
			var headerColumnName = field.trim();
			if (mapping)
			{
				for (var c = 0; c < mapping.columns.length && !mappingFound; c++)
				{
					if (headerColumnName === mapping.columns[c].header)
					{
						lineFormat.push(mapping.columns[c]);
						mappingFound = true;
					}
				}
			}
			if (!mappingFound)
			{
				// Add new column to mapping
				var guess = LoadClipboard.guessColumn(headerColumnName);
				var m = {
					lineItemsColumn: guess.columnName,
					header: headerColumnName,
					type: guess.type,
					noMapping: false
				};
				if (!guess.found)
				{
					m.noMapping = true;
					LoadClipboard.noMapping.push(headerColumnName);
					Log.Warn("LoadClipboard - no line items mapping found for column " + headerColumnName);
				}
				else
				{
					// Show this column in the possible values in mapping edition
					LoadClipboard.addColumnInMap(guess.columnName);
					// We have at least one column mapped, show the preview
					LoadClipboard.showPreview = true;
				}
				LoadClipboard.addOrModifyMapping(mapping, m);
				lineFormat.push(m);
			}
			else
			{
				// We have at least one column mapped, show the preview
				LoadClipboard.showPreview = true;
			}
		}
		if (LoadClipboard.shouldDisplayMappingDialog())
		{
			LoadClipboard.showMappingDialog();
		}
		return lineFormat;
	},
	convertValue: function (value, convertFunc)
	{
		if (Sys.Helpers.IsFunction(convertFunc))
		{
			return convertFunc(value);
		}
		else if (Sys.Helpers.IsString(convertFunc))
		{
			return Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts." + convertFunc, value);
		}
		return value;
	},
	parseLine: function (line, mapping, lineFormat)
	{
		var l = {};
		var fields = line.split(LoadClipboard.getSeparator(mapping));
		for (var f = 0; f < fields.length && f < lineFormat.length; f++)
		{
			if (lineFormat[f].lineItemsColumn)
			{
				l[lineFormat[f].lineItemsColumn] = LoadClipboard.convertValue(fields[f], lineFormat[f].convert);
			}
		}
		return l;
	},
	parseData: function (text, mapping)
	{
		var data = null;
		if (text)
		{
			data = [];
			var lines = text.split("\n");
			if (lines.length <= 0)
			{
				return null;
			}
			var n = 0;
			var hasHeader = LoadClipboard.hasHeader(mapping);
			var lineFormat = [];
			if (hasHeader)
			{
				// parse header and build the expected line format from mapping
				lineFormat = LoadClipboard.parseHeader(lines[0].trim(), mapping);
				// Skip header
				n = 1;
				if (!LoadClipboard.showPreview)
				{
					// No need to go further, the preview will not be displayed
					return data;
				}
			}
			else if (mapping)
			{
				lineFormat = mapping.columns;
			}
			for (; n < lines.length; n++)
			{
				var line = lines[n];
				if (line)
				{
					data.push(LoadClipboard.parseLine(line, mapping, lineFormat));
				}
			}
		}
		return data;
	},
	fillPreview: function (dialog, data)
	{
		if (data)
		{
			// Show the first lines in preview results
			var previewTable = dialog.GetControl("preview_table");
			previewTable.SetItemCount(0);
			for (var _i = 0, data_1 = data; _i < data_1.length; _i++)
			{
				var dataItem = data_1[_i];
				var line = previewTable.AddItem(false);
				for (var fld in dataItem)
				{
					if (Object.prototype.hasOwnProperty.call(dataItem, fld))
					{
						line.SetValue(fld, dataItem[fld]);
					}
				}
			}
		}
	},
	showPreviewDialog: function ()
	{
		LoadClipboard.data = LoadClipboard.parseData(LoadClipboard.rawData, LoadClipboard.mapping);
		if (LoadClipboard.showPreview && !LoadClipboard.shouldDisplayMappingDialog())
		{
			Popup.Dialog("_Load clipboard preview dialog title", null, LoadClipboard.fillPreviewDialog, LoadClipboard.commitPreviewDialog, null, LoadClipboard.handlePreviewDialog, LoadClipboard.cancelPreviewDialog);
		}
		else if (!LoadClipboard.shouldDisplayMappingDialog())
		{
			// Keep this dialog open when no other dialog is already opened
			LoadClipboard.ShowDialog();
		}
	},
	showMappingDialog: function ()
	{
		Popup.Dialog("_Load clipboard mapping incomplete", null, LoadClipboard.fillMappingPopup, LoadClipboard.commitMappingPopup, null, LoadClipboard.handleMappingPopup);
	},
	// --------------------
	// Mapping dialog
	fillMappingPopup: function (dialog)
	{
		if (LoadClipboard.noMapping.length > 0)
		{
			var errorMsg = dialog.AddDescription("errorMsg");
			errorMsg.SetText(Language.Translate("_Load clipboard No mapping found for columns '{0}'", true, LoadClipboard.noMapping.join("', '")));
		}
		// design mapping
		if (LoadClipboard.mapping && LoadClipboard.mapping.columns && LoadClipboard.mapping.columns.length > 0)
		{
			LoadClipboard.mappingCtrls = {};
			for (var _i = 0, _a = LoadClipboard.mapping.columns; _i < _a.length; _i++)
			{
				var col = _a[_i];
				var mCtrl = dialog.AddComboBox(col.lineItemsColumn, col.header);
				if (mCtrl)
				{
					mCtrl.SetText(LoadClipboard.mappingComboValues.join("\n"));
					if (!col.noMapping)
					{
						mCtrl.SetValue(LoadClipboard.reversedComboMap[col.lineItemsColumn]);
					}
				}
				LoadClipboard.mappingCtrls[col.header] = mCtrl;
			}
		}
		var link = dialog.AddLink("LinkToDoc__");
		link.SetText("_Load clipboard dialog help link");
		link.SetOpenInCurrentWindow(true);
		link.SetURL("#");
	},
	commitMappingPopup: function ()
	{
		// Save new mapping
		if (LoadClipboard.mappingCtrls)
		{
			LoadClipboard.mapping.columns = [];
			for (var c in LoadClipboard.mappingCtrls)
			{
				if (Object.prototype.hasOwnProperty.call(LoadClipboard.mappingCtrls, c))
				{
					var ctrl = LoadClipboard.mappingCtrls[c];
					if (ctrl && ctrl.GetValue() && ctrl.GetValue() !== Language.Translate(LoadClipboard.noMappingKey))
					{
						var guess = LoadClipboard.guessColumn(ctrl.GetValue());
						var m = {
							lineItemsColumn: guess.columnName,
							header: c,
							type: guess.type
						};
						var column = LoadClipboard.addOrModifyMapping(LoadClipboard.mapping, m);
						if (column)
						{
							delete column.noMapping;
						}
					}
				}
			}
		}
		// Show preview with new mapping
		LoadClipboard.showPreviewDialog();
	},
	handleMappingPopup: function (dialog, tabId, event, control)
	{
		if (event === "OnClick" && control.GetName() === "LinkToDoc__")
		{
			Process.ShowHelp(2540);
		}
	},
	shouldDisplayMappingDialog: function ()
	{
		return !LoadClipboard.showPreview && LoadClipboard.noMapping.length > 0;
	},
	// --------------------
	// Paste dialog
	fillPasteDialog: function (dialog)
	{
		if (!LoadClipboard.checkMapping(LoadClipboard.mapping))
		{
			var errorCtrl = dialog.AddDescription("errorMsg", "");
			errorCtrl.SetText("_Load clipboard mapping undefined or invalid");
			errorCtrl.SetErrorStyle();
			return;
		}
		var pasteArea = dialog.AddMultilineText("pasteZone");
		pasteArea.SetLineCount(10);
		pasteArea.SetWidth(450);
		pasteArea.SetPlaceholder(Language.Translate("_Paste your data here"));
		var okButton = dialog.GetControl("buttonok");
		okButton.SetText(Language.Translate("_Load clipboard Show preview"));
	},
	commitPasteDialog: function (dialog)
	{
		// Show preview dialog
		var control = dialog.GetControl("pasteZone");
		if (control)
		{
			LoadClipboard.rawData = control.GetValue();
			LoadClipboard.showPreviewDialog();
		}
	},
	// --------------------
	// Preview dialog
	fillPreviewDialog: function (dialog)
	{
		// Hold dialog until table is filled to make sure it is centered on screen
		dialog.Hold(true);
		var okButton = dialog.GetControl("buttonok");
		okButton.SetText(Language.Translate("_Load clipboard append lines"));
		var previewTable = dialog.AddTable("preview_table");
		previewTable.SetReadOnly(true);
		previewTable.SetLineCount(5);
		for (var _i = 0, _a = LoadClipboard.mapping.columns; _i < _a.length; _i++)
		{
			var fldprops = _a[_i];
			var addColumn = void 0;
			if (fldprops.type === "Decimal")
			{
				addColumn = previewTable.AddDecimalColumn;
			}
			else
			{
				addColumn = previewTable.AddTextColumn;
			}
			if (addColumn && !fldprops.noMapping)
			{
				addColumn.call(previewTable, fldprops.lineItemsColumn, Controls.LineItems__[fldprops.lineItemsColumn].GetLabel(), fldprops.width ? fldprops.width : 100);
			}
		}
		LoadClipboard.fillPreview(dialog, LoadClipboard.data);
		dialog.AddButton("editMapping", "_Load clipboard Edit mapping");
		dialog.Hold(false);
	},
	handlePreviewDialog: function (dialog, tabId, event, control)
	{
		if (event === "OnClick" && control.GetName() === "editMapping")
		{
			LoadClipboard.showPaste = false;
			dialog.Cancel();
			LoadClipboard.showMappingDialog();
		}
	},
	cancelPreviewDialog: function ()
	{
		if (LoadClipboard.showPaste)
		{
			// Return to paste dialog
			LoadClipboard.ShowDialog();
		}
	},
	addLines: function ()
	{
		// preview accepted
		var data = LoadClipboard.data;
		if (data)
		{
			var lineItems = Data.GetTable("LineItems__");
			var lineItemsCount = lineItems.GetItemCount();
			var item = lineItems.GetItem(lineItemsCount - 1);
			if (Lib.P2P.InvoiceLineItem.IsGenericLineEmpty(item))
			{
				item.Remove();
				lineItemsCount--;
			}
			var finalItemCount = lineItemsCount + data.length;
			lineItems.SetItemCount(finalItemCount);
			var items = [];
			for (var i = lineItemsCount; i < finalItemCount; i++)
			{
				item = lineItems.GetItem(i);
				if (item)
				{
					var rowData = data[i - lineItemsCount];
					item.SetValue("LineType__", Lib.P2P.LineType.GL);
					item.SetValue("ItemType__", Lib.P2P.ItemType.AMOUNT_BASED);
					for (var field in rowData)
					{
						if (Object.prototype.hasOwnProperty.call(rowData, field))
						{
							item.SetValue(field, rowData[field]);
						}
					}
					getTaxRateAndUpdateItem(item, null, true);
					if (!item.GetValue("CostType__"))
					{
						Lib.P2P.fillCostTypeFromGLAccount(item);
					}
					items.push(item);
				}
			}
			LayoutHelper.DisableButtons(true, "AddlinesPromises");
			items.reduce(function (p, l)
			{
				return p.Then(function ()
				{
					return Sys.Helpers.Promise.Create(function (resolve)
					{
						fillGLAndCCDescriptions(l, resolve);
					});
				});
			}, Sys.Helpers.Promise.Resolve()).Then(function ()
			{
				LayoutHelper.DisableButtons(false, "AddlinesPromises");
				GetTaxRateForItemList().Then(function ()
				{
					LoadClipboard.endAddLines();
				});
			});
		}
		else
		{
			LoadClipboard.endAddLines();
		}
	},
	/***
	 * Backup the mapping and reactivate the buttons
	 */
	endAddLines: function ()
	{
		// Will save mapping into P2P - Parameters table (with LineItemsImporterMapping process variable)
		if (!Variable.GetValueAsString("LineItemsImporterMapping") && LoadClipboard.mapping)
		{
			Variable.SetValueAsString("LineItemsImporterMapping", Sys.Helpers.SerializeObject(LoadClipboard.mapping));
		}
		LayoutHelper.DisableButtons(false, "Addlines");
	},
	commitPreviewDialog: function ()
	{
		LayoutHelper.ShowWaitScreen = true;
		LayoutHelper.DisableButtons(true, "Addlines");
		// asynchronous call for dialog to be closed right away and for wait screen to be visible
		setTimeout(LoadClipboard.addLines, 20);
	},
	// Entry point
	ShowDialog: function ()
	{
		LoadClipboard.init();
		Popup.Dialog("_Load clipboard paste dialog title", null, LoadClipboard.fillPasteDialog, LoadClipboard.commitPasteDialog);
	}
};
/** ******************************** **/
/** Assign standard buttons controls **/
/** ******************************** **/
/** asynchronous/synchronous buttons helpers**/
var ButtonsBehavior = {
	bWaitForPostResult: null,
	nonBlockingErrors: [
	{
		field: "ERPInvoiceNumber__",
		error: ""
	}],
	WaitForPostResult: function ()
	{
		if (ButtonsBehavior.bWaitForPostResult === null)
		{
			ButtonsBehavior.bWaitForPostResult = Sys.Parameters.GetInstance("P2P_" + Lib.ERP.GetERPName()).GetParameter("WaitForPostResult");
		}
		return ButtonsBehavior.bWaitForPostResult;
	},
	init: function ()
	{
		Controls.BackToAP.OnClick = ButtonsBehavior.onclickBackToAP;
		Controls.BackToPrevious.OnClick = ButtonsBehavior.onclickBackToPrevious;
		Controls.Reject.OnClick = ButtonsBehavior.onclickReject;
		Controls.Simulate.OnClick = ButtonsBehavior.onclickSimulate;
		Controls.Post.OnClick = ButtonsBehavior.onclickPost;
		Controls.OnHold.OnClick = ButtonsBehavior.onclickOnHold;
		Controls.SetAside.OnClick = ButtonsBehavior.onclickSetAside;
		Controls.ToggleHistoryView__.OnClick = ButtonsBehavior.onclickToggleHistoryView;
		Controls.ToggleVendorRelatedContractsView__.OnClick = ButtonsBehavior.onclickToggleVendorRelatedContractsView;
		Controls.ButtonSaveTemplate__.OnClick = ButtonsBehavior.onclickSaveTemplate;
		Controls.ButtonLoadTemplate__.OnClick = ButtonsBehavior.onclickLoadTemplate;
		Controls.ButtonLoadClipboard__.OnClick = ButtonsBehavior.onclickLoadClipboard;
		if (Controls.SaveEditing_)
		{
			Controls.SaveEditing_.OnClick = ButtonsBehavior.onclickSaveEditing_;
		}
		Controls.ReverseInvoice.OnClick = ButtonsBehavior.onClickReverseInvoice;
		Controls.ShowBankDetails__.OnClick = BankDetails.onClick;
		Controls.ShowParameters__.OnClick = ParameterDetails.onClick;
	},
	approve: function (actionName, bForceSync)
	{
		var isAsync = !bForceSync && (isInApprovalWorkflow() || Controls.ManualLink__.IsChecked() || !ButtonsBehavior.WaitForPostResult());
		if (isAsync)
		{
			ProcessInstance.ApproveAsynchronous(actionName);
		}
		else
		{
			ProcessInstance.Approve(actionName);
		}
	},
	backToAP: function (backToAPtitle, backToAPReasonListLabel, actionName)
	{
		Lib.CommonDialog.PopupReason(
		{
			title: backToAPtitle,
			reasonListLabel: backToAPReasonListLabel,
			possibleValues: Controls.BackToAPReason__.GetText(),
			currentReason: Controls.BackToAPReason__.GetValue(),
			currentComment: getReliableComment(),
			onClickOk: function (result)
			{
				Controls.BackToAPReason__.SetValue(result.reason);
				Controls.Comment__.SetValue(result.comment ? result.comment : "");
				ButtonsBehavior.approve(actionName);
			}
		});
	},
	holdingInvoice: function (holdingTitle, holdingReasonListLabel, actionName)
	{
		var dialog = {
			title: holdingTitle,
			reasonListLabel: holdingReasonListLabel,
			possibleValues: Controls.AsideReason__.GetAvailableValues().join("\n"),
			currentReason: Controls.AsideReason__.GetValue(),
			currentComment: getReliableComment(),
			limitDate: null,
			onClickOk: null
		};
		if (actionName === "OnHold")
		{
			dialog.limitDate = "_LimitDate";
			dialog.onClickOk = function (result)
			{
				Controls.AsideReason__.SetValue(result.reason);
				Controls.Comment__.SetValue(result.comment ? result.comment : "");
				Controls.ScheduledActionDate__.SetValue(result.limitDate ? result.limitDate : "");
				Controls.ScheduledAction__.SetValue("onHoldExpiration");
				Controls.HasBeenOnHold__.SetValue(true);
				ButtonsBehavior.approve(actionName);
			};
		}
		else
		{
			dialog.onClickOk = function (result)
			{
				Controls.AsideReason__.SetValue(result.reason);
				Controls.Comment__.SetValue(result.comment ? result.comment : "");
				ButtonsBehavior.approve(actionName);
			};
		}
		Lib.CommonDialog.PopupReason(dialog);
	},
	reject: function (rejectTitle, rejectListLabel, actionName)
	{
		var portalWarning = "";
		var vendorName = Controls.VendorName__.GetValue();
		if (!Controls.PortalRuidEx__.GetValue() && vendorName)
		{
			portalWarning = Language.Translate("_Publish on vendor portal for {0}", true, vendorName);
		}
		Lib.CommonDialog.PopupReason(
		{
			title: rejectTitle,
			helpId: 2522,
			reasonListLabel: rejectListLabel,
			possibleValues: Controls.RejectReason__.GetText(),
			currentReason: Controls.RejectReason__.GetValue(),
			currentComment: getReliableComment(),
			confirmationMessage: portalWarning,
			confirmationValue: true,
			onClickOk: function (result)
			{
				Controls.RejectReason__.SetValue(result.reason);
				Controls.Comment__.SetValue(result.comment ? result.comment : "");
				ButtonsBehavior.approve(actionName);
				if (portalWarning && result.confirmationValue)
				{
					Variable.SetValueAsString("PublishOnReject", "true");
				}
			}
		});
	},
	checkException: function ()
	{
		var allowPosting = true;
		var exception = Data.GetValue("CurrentException__") &&
			(Variable.GetValueAsString("isExtractionReviewException") || Variable.GetValueAsString("manualExceptionType") === "_WorkflowType_Invoice exception");
		var nextRole = Lib.AP.WorkflowCtrl.GetNextStepRole();
		if (exception && nextRole !== Lib.AP.WorkflowCtrl.roles.controller)
		{
			Popup.Alert(["_The exception {0} is set but no reviewer is selected.", Data.GetValue("CurrentException__")], false, null, "_Cannot post invoice");
			allowPosting = false;
		}
		return allowPosting;
	},
	removeEmptyExtendedWHT: function ()
	{
		var WHTTable = Data.GetTable("ExtendedWithholdingTax__");
		var i = 0;
		// don't add empty line while cleaning
		Controls.ExtendedWithholdingTax__.SetAtLeastOneLine(false);
		while (i < WHTTable.GetItemCount())
		{
			var item = WHTTable.GetItem(i);
			if (!item.GetValue("WHTType__") || !item.GetValue("WHTCode__"))
			{
				item.Remove();
			}
			else
			{
				i++;
			}
		}
	},
	checkforEmptyPOLineItem: function ()
	{
		if (POLineItemHelper.ShouldCheck())
		{
			var lineItemsTable = Data.GetTable("LineItems__");
			var nbItems = lineItemsTable.GetItemCount();
			for (var i = 0; i < nbItems; i++)
			{
				if (Lib.P2P.InvoiceLineItem.IsRemovable(lineItemsTable.GetItem(i)))
				{
					POLineItemHelper.SetEmptyLine(i);
					break;
				}
			}
			if (POLineItemHelper.HasEmptyLine())
			{
				Popup.Confirm("_Some items are incomplete and will be ignored, do you want to continue ?", false, ButtonsBehavior.removeEmptyPOLineItems, ButtonsBehavior.warnEmptyPOLineItems, "_Incomplete item detected");
				return false;
			}
		}
		return true;
	},
	removeEmptyPOLineItems: function ()
	{
		var lineItemsTable = Data.GetTable("LineItems__");
		var i = 0;
		while (i < lineItemsTable.GetItemCount())
		{
			var item = lineItemsTable.GetItem(i);
			if (Lib.P2P.InvoiceLineItem.IsRemovable(item))
			{
				item.Remove();
			}
			else
			{
				i++;
			}
		}
		if (lineItemsTable.GetItemCount() > 0)
		{
			ButtonsBehavior.validateAndApprove(true);
		}
		// must check again if validation has not been done
		POLineItemHelper.Clear();
	},
	warnEmptyPOLineItems: function ()
	{
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = POLineItemHelper.idx; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (InvoiceLineItem.IsPOLineItem(item) && !item.GetValue("Amount__") && !item.GetValue("Quantity__"))
			{
				item.SetWarning("Amount__", "_Line with empty amount will be ignored.");
				item.SetWarning("Quantity__", "_Line with empty quantity will be ignored.");
			}
		}
		POLineItemHelper.Clear();
	},
	removeNonBlockingErrors: function ()
	{
		for (var _i = 0, _a = ButtonsBehavior.nonBlockingErrors; _i < _a.length; _i++)
		{
			var nonBlockingError = _a[_i];
			nonBlockingError.error = Data.GetError(nonBlockingError.field);
			Data.SetError(nonBlockingError.field, "");
		}
	},
	resetNonBlockingErrors: function ()
	{
		for (var _i = 0, _a = ButtonsBehavior.nonBlockingErrors; _i < _a.length; _i++)
		{
			var nonBlockingErrors = _a[_i];
			Data.SetError(nonBlockingErrors.field, nonBlockingErrors.error);
		}
	},
	validateData: function ()
	{
		var isValid;
		var wkfExceptionError = Data.GetError("CurrentException__");
		var wkfException = Data.GetValue("CurrentException__");
		var nextRole = Lib.AP.WorkflowCtrl.GetNextStepRole();
		if (!wkfExceptionError && wkfException && nextRole && nextRole !== Lib.AP.WorkflowCtrl.roles.approver)
		{
			// A valid exception is set, and there is someone next in the workflow
			Log.Warn("In exception workflow: ignore errors and continue with the action");
			isValid = true;
		}
		else
		{
			DisableTableValuesOnlyInApprovalWkf();
			var erpManager = Lib.AP.GetInvoiceDocument();
			var requiredFields = erpManager.GetRequiredFields(Sys.Helpers.TryGetFunction("Lib.AP.Customization.Common.GetRequiredFields"));
			validateHeader(requiredFields);
			validateLineItems(requiredFields);
			erpManager.ValidateData();
			isValid = !Process.ShowFirstError() && (isInApprovalWorkflow() || isBalanceInThreshold());
		}
		var customIsValid = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.OnValidateForm", isValid);
		var promisifiedIsValid;
		if (typeof customIsValid === "boolean")
		{
			promisifiedIsValid = Sys.Helpers.Promise.Resolve(customIsValid);
		}
		else if (Sys.Helpers.Promise.IsPromise(customIsValid))
		{
			promisifiedIsValid = customIsValid;
		}
		else
		{
			promisifiedIsValid = Sys.Helpers.Promise.Resolve(isValid);
		}
		return promisifiedIsValid;
	},
	validateAndApprove: function (isAPspecialist)
	{
		Controls.Post.Wait(true);
		ButtonsBehavior.removeNonBlockingErrors();
		return ButtonsBehavior.validateData()
			.Then(function (isValid)
			{
				if (isValid)
				{
					//Reccuring invoice
					var workflow = Sys.WorkflowController.Create(Data, Variable, Language, Controls, User);
					var recurringInvoiceAction = Variable.GetValueAsString("RecurringInvoiceAutoValidation");
					var currentRole = Lib.AP.WorkflowCtrl.GetCurrentStepRole();
					var currentContributor = workflow.GetNbContributors() > 0 && workflow.GetContributorAt(workflow.GetContributorIndex());
					var showRecurringDialog = (recurringInvoiceAction === "prompt") && currentContributor &&
						(currentRole === Lib.AP.WorkflowCtrl.roles.approver) && (currentContributor.login === User.loginId);
					if (!isInApprovalWorkflow() && !currentStepIsController())
					{
						autolearnExtractedNetAmount();
					}
					if (isAPspecialist && parseInt(Data.GetValue("DuplicateCheckAlertLevel__"), 10) > 0)
					{
						var callBack = function ()
						{
							Controls.Post.Wait(false);
							ButtonsBehavior.approve("Post");
						};
						DuplicateCheck.DoDuplicateCheck(callBack);
					}
					else if (showRecurringDialog)
					{
						Popup.Dialog("_RecurrentInvoiceDialogTitle", null, function (dialog)
						{
							dialog.HideDefaultButtons();
							dialog.AddDescription("Description").SetText("_RecurrentInvoiceDialogMessage");
							var yesButton = dialog.AddButton("YesButton", "_Yes");
							yesButton.SetSubmitStyle();
							dialog.AddButton("NoButton", "_No");
						}, null, null, function (dialog, tabId, event, control)
						{
							if (event === "OnClick")
							{
								switch (control.GetName())
								{
								case "YesButton":
									Variable.SetValueAsString("RecurringInvoiceAutoValidation", "true");
									break;
								case "NoButton":
								default:
									Variable.SetValueAsString("RecurringInvoiceAutoValidation", "false");
									break;
								}
								Controls.Post.Wait(false);
								ButtonsBehavior.approve("Post");
								dialog.Cancel();
							}
						});
					}
					else
					{
						Controls.Post.Wait(false);
						ButtonsBehavior.approve("Post");
					}
				}
				else
				{
					ButtonsBehavior.resetNonBlockingErrors();
					Controls.Post.Wait(false);
				}
			})
			.Catch(function (error)
			{
				Log.Error("Unhandled promise: " + error + ". Prevent posting.");
				ButtonsBehavior.resetNonBlockingErrors();
				Controls.Post.Wait(false);
			});
	},
	onclickPost: function ()
	{
		var post = true;
		if (isInApprovalWorkflow())
		{
			if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.OnHold)
			{
				post = false;
				Popup.Confirm("_The payment for this invoice has been previously put on hold, do you confirm the payment approval ?", false, function ()
				{
					ButtonsBehavior.validateAndApprove(false);
				}, null, "_Invoice on hold");
			}
		}
		else if (currentStepIsApStart())
		{
			ButtonsBehavior.removeNonBlockingErrors();
			post = ButtonsBehavior.checkException();
			if (post && (Lib.AP.InvoiceType.isPOInvoice() || Lib.AP.InvoiceType.isPOGLInvoice()))
			{
				post = ButtonsBehavior.checkforEmptyPOLineItem();
			}
			// remove empty WHT line
			if (Lib.ERP.IsSAP() && Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "") === "Extended")
			{
				ButtonsBehavior.removeEmptyExtendedWHT();
			}
			if (Lib.AP.InvoiceType.isConsignmentInvoice() &&
				Lib.AP.WorkflowCtrl.GetNbRemainingContributorWithRole(Lib.AP.WorkflowCtrl.roles.approver) === 0 &&
				!Data.GetValue("ERPInvoiceNumber__"))
			{
				post = false;
				var lineItems = Data.GetTable("LineItems__");
				var goodIssueNumbers = [];
				for (var i = 0; i < lineItems.GetItemCount(); i++)
				{
					var item = lineItems.GetItem(i);
					var goodIssueNumber = item.GetValue("GoodIssue__");
					if (goodIssueNumbers.indexOf(goodIssueNumber) === -1)
					{
						goodIssueNumbers.push(goodIssueNumber);
					}
				}
				var isValid_1 = true;
				var currentSettlementNumber_1 = null;
				goodIssueNumbers.reduce(function (p, goodIssueNumber)
				{
					return p.then(function ()
					{
						return Sys.Helpers.Promise.Create(function (resolve)
						{
							function checkSettlement(FIDocumentNumber)
							{
								if (currentSettlementNumber_1 === null)
								{
									currentSettlementNumber_1 = FIDocumentNumber;
								}
								else if (currentSettlementNumber_1 !== FIDocumentNumber)
								{
									isValid_1 = false;
								}
								resolve();
							}
							Lib.AP.SAP.Consignment.CheckRKWA(goodIssueNumber, checkSettlement);
						});
					});
				}, Sys.Helpers.Promise.Resolve()).then(function ()
				{
					if (isValid_1 && currentSettlementNumber_1 !== "")
					{
						Data.SetValue("ERPinvoiceNumber__", currentSettlementNumber_1);
						Popup.Confirm(["_Existing settlement found {0}", currentSettlementNumber_1], false, function ()
						{
							ButtonsBehavior.validateAndApprove(true);
						}, null, "_Existing settlement found title");
					}
					else if (!isValid_1)
					{
						Popup.Alert("_SAP changement detected", true, null, "_SAP changement detected title");
					}
					else
					{
						ButtonsBehavior.validateAndApprove(true);
					}
				});
			}
		}
		if (post)
		{
			// no popup was triggered - client side validation required
			ButtonsBehavior.validateAndApprove(!isInApprovalWorkflow());
			post = false;
		}
		return post;
	},
	onclickBackToAP: function ()
	{
		ButtonsBehavior.backToAP("_Back to AP", "_Select back to ap reason", "BackToAP");
		return false;
	},
	onclickBackToPrevious: function ()
	{
		ButtonsBehavior.approve("BackToPrevious");
		return false;
	},
	onclickReject: function ()
	{
		ButtonsBehavior.reject("_Reject", "_Select reject reason", "Reject");
		return false;
	},
	onclickOnHold: function ()
	{
		ButtonsBehavior.holdingInvoice("_Set on hold", "_Select on hold reason", "OnHold");
		return false;
	},
	onclickSetAside: function ()
	{
		ButtonsBehavior.holdingInvoice("_Set aside title", "_Select aside reason", "Set_aside");
		return false;
	},
	onclickToggleHistoryView: function ()
	{
		EmbeddedViews.OnClickToggleButton("historyContext");
	},
	onclickToggleVendorRelatedContractsView: function ()
	{
		EmbeddedViews.OnClickToggleButton("vendorRelatedContractsContext");
	},
	onclickSaveTemplate: function ()
	{
		var lineItemsTable = Data.GetTable("LineItems__");
		if (lineItemsTable.GetItemCount() > Lib.AP.Parameters.limitLinesItems)
		{
			Popup.Alert(["_You cannot save a template with more than {0} lines items.", Lib.AP.Parameters.limitLinesItems.toString()], false, null, "_Warning");
			return;
		}
		var templateName = Data.GetValue("CodingTemplate__");
		var saveTemplate = function ()
		{
			Data.SetValue("CodingTemplate__", templateName);
			ProcessInstance.Approve("saveTemplate");
		};
		var fillSaveAs = function (dialog)
		{
			var ctrl_Template_Name = dialog.AddText("template_name", "_Template name");
			dialog.RequireControl(ctrl_Template_Name);
			ctrl_Template_Name.SetValue(templateName);
		};
		/**
		 * Query.DBQuery callback
		 * @this QueryResult
		 */
		var existingTemplateNameCallBack = function ()
		{
			var err = this.GetQueryError();
			if (err)
			{
				Popup.Alert(err);
			}
			else if (this.GetRecordsCount() > 0)
			{
				Popup.Confirm("_Confirm", false, saveTemplate, popupSelectionNameTemplate, "_the template name already exists");
			}
			else
			{
				saveTemplate();
			}
		};
		var validateDialog = function (dialog)
		{
			templateName = dialog.GetControl("template_name").GetValue();
			if (!templateName)
			{
				return;
			}
			// only keep 50 first chars (field size in database)
			templateName = templateName.substring(0, 50);
			var filter = "&(CompanyCode__=" + Controls.CompanyCode__.GetValue() + ")(Template__=" + templateName + ")";
			Query.DBQuery(existingTemplateNameCallBack, "AP - Templates__", "CompanyCode__|Template__", filter, "", 1);
		};
		var popupSelectionNameTemplate = function ()
		{
			Popup.Dialog("_Save template", null, fillSaveAs, null, validateDialog, null, null);
		};
		popupSelectionNameTemplate();
	},
	onclickLoadTemplate: function ()
	{
		Controls.CodingTemplate__.DoBrowse();
	},
	onclickLoadClipboard: function ()
	{
		LoadClipboard.ShowDialog();
	},
	onclickSimulate: function ()
	{
		var _this = this;
		validateHeader();
		var lastPostError = Data.GetError("ERPInvoiceNumber__");
		Data.SetError("ERPInvoiceNumber__", "");
		var callback = function (simulateResult, simulationReport)
		{
			handleSimulationResult(simulateResult, simulationReport);
			LayoutHelper.DisableButtons(false, "onclickSimulate");
			Data.SetError("ERPInvoiceNumber__", lastPostError);
			_this.Wait(false);
		};
		if (!Process.ShowFirstError())
		{
			LayoutHelper.DisableButtons(true, "onclickSimulate");
			this.Wait(true);
			Lib.AP.SAP.Invoice.Simulate.ERPSimulate(callback, true);
		}
		else
		{
			// restore previous posting error
			Data.SetError("ERPInvoiceNumber__", lastPostError);
		}
		return false;
	},
	onclickSaveEditing_: function ()
	{
		Lib.AP.ArchivedInvoices.OnSave();
	},
	/**
	 * Handle the click of the ReverseInvoice button
	 * Display a popup to confirm the user click
	 */
	onClickReverseInvoice: function ()
	{
		Lib.CommonDialog.PopupOkExtended(
		{
			"title": "_Reverse invoice",
			"explanationLabel": "_Reverse invoice explanation message",
			"allowComment": true,
			"currentComment": Data.GetValue("Comment__"),
			"onClickOk": function (result)
			{
				Data.SetValue("Comment__", result.comment);
				Lib.AP.ArchivedInvoices.ReverseInvoice();
			}
		});
	}
};

function onAddApproverClick()
{
	Controls.ApproversList__.Wait(true);
	addApprover();
}
/** Teaching **/
RequestTeaching = {
	/** Checks that the teaching is possible, and initialises button action */
	Init: function ()
	{
		Controls.Request_teaching.OnClick = RequestTeaching.DialogDisplay;
		this.RefreshButtonState();
	},
	RefreshButtonState: function ()
	{
		var docAttached = Variable.GetValueAsString("DocAttached");
		var isTeachingDisabled = Sys.Parameters.GetInstance("AP").GetParameter("DisableTeaching") === "1";
		Controls.Request_teaching.SetDisabled(isTeachingDisabled || docAttached === null || docAttached === "0");
	},
	/** Set teaching parameter (the value must be converted to string)*/
	SetParameter: function (name, value)
	{
		if (!value)
		{
			value = "";
		}
		else if (typeof value === "object")
		{
			// Date
			value = Sys.Helpers.Date.Date2DBDateTime(value);
		}
		else
		{
			// Number, boolean
			value = value.toString();
		}
		Variable.SetValueAsString(name, value);
	},
	FillDialog: function (dialog)
	{
		dialog.AddSeparator();
		var messageControl = dialog.AddDescription("message");
		messageControl.SetText("_You are about to send a teaching request for vendor number: {0}", Controls.VendorNumber__.GetValue());
		dialog.AddSeparator();
		dialog.AddSeparator();
		var label = dialog.AddDescription("label");
		label.SetText("_Comments:");
		var commentsControl = dialog.AddMultilineText("comments");
		commentsControl.SetLineCount(10);
	},
	/** Commit dialog callback: Retrieves the dialog results and call the server script*/
	CommitDialogBox: function (dialog)
	{
		RequestTeaching.SetParameter("TeachingValidationMessage", dialog.GetControl("Comments").GetValue());
		ButtonsBehavior.approve("Request_teaching", true);
		Log.Info("Request teaching sent to " + Variable.GetValueAsString("TeachingOwnerID"));
	},
	DialogDisplay: function ()
	{
		// Specify teaching parameters
		var owner = Variable.GetValueAsString("TeachingOwnerID");
		Log.Info("TeachingOwnerID:" + owner);
		// If TeachingOwnerID doesn't exist, a teaching request will be sent to the current user
		if (!owner)
		{
			Variable.SetValueAsString("TeachingOwnerID", User.loginId);
		}
		else
		{
			Variable.SetValueAsString("TeachingOwnerID", owner);
		}
		// Customize these variables to match your process. Refer to Process.SendToTeaching() for more information about these variables.
		Variable.SetValueAsString("TeachingSubject", "Teaching request");
		Variable.SetValueAsString("TeachingValidationMessage", "");
		Variable.SetValueAsString("TeachingBusinessDocumentAmount", Controls.NetAmount__.GetValue());
		Variable.SetValueAsString("TeachingBusinessDocumentDate", Sys.Helpers.Date.Date2DBDateTime(Controls.InvoiceDate__.GetValue()));
		Variable.SetValueAsString("TeachingBusinessDocumentNumber", Controls.InvoiceNumber__.GetValue());
		Variable.SetValueAsString("TeachingBusinessPartnerName", Controls.VendorName__.GetValue());
		Variable.SetValueAsString("TeachingBusinessPartnerNumber", Controls.VendorNumber__.GetValue());
		var teachingKeyControl = Process.GetTeachingKeyControl();
		if (teachingKeyControl)
		{
			// This process has a teaching key field (customer number): it is required for teaching
			var teachingKeyValue = teachingKeyControl.GetValue();
			if (!teachingKeyValue)
			{
				// The teaching key field should not be empty
				teachingKeyControl.Focus();
				Popup.Alert(["_The {0} is required for teaching", Language.Translate(teachingKeyControl.GetLabelAlt())], true, null, "_Teaching request");
			}
			else
			{
				// Confirmation of the value of the teaching key field
				Popup.Dialog("_Teaching request", Controls.Request_teaching, RequestTeaching.FillDialog, RequestTeaching.CommitDialogBox);
			}
		}
		else
		{
			// No teaching key field
			Popup.Confirm("_You are about to send a teaching request", false, RequestTeaching.CommitDialogBox, null, "_Teaching request");
		}
	}
};
/** Invoice header **/
/**
 * Event when a vendor is selected from the browse page
 * @this DatabaseComboBox
 * @param item the browse item selected
 */
function onVendorSelectItem(item)
{
	var isSelectedVendorDifferentFromCurrentVendor = (this.GetName() === "VendorName__") &&
		(Controls.VendorNumber__.GetValue() !== item.GetValue("Number__")) &&
		(Controls.VendorName__.GetValue() === item.GetValue("Name__"));
	Controls.VendorNumber__.SetValue(item.GetValue("Number__"));
	Controls.VendorName__.SetValue(item.GetValue("Name__"));
	Controls.VendorStreet__.SetValue(item.GetValue("Street__"));
	Controls.VendorCity__.SetValue(item.GetValue("City__"));
	Controls.VendorZipCode__.SetValue(item.GetValue("PostalCode__"));
	Controls.VendorRegion__.SetValue(item.GetValue("Region__"));
	Controls.VendorCountry__.SetValue(item.GetValue("Country__"));
	Controls.PaymentTerms__.SetValue(item.GetValue("PaymentTermCode__"));
	Controls.VendorPOBox__.SetValue(item.GetValue("PostOfficeBox__"));
	// Fire the vendor OnChange event to populate vendor related fields if the event would not be
	// fired automatically (Can occur if two different vendors have the same name).
	if (isSelectedVendorDifferentFromCurrentVendor)
	{
		Controls.VendorNumber__.OnChange();
	}
}

function retrieveWithholdingTaxInfo()
{
	var language = "";
	var WHTTable = Data.GetTable("ExtendedWithholdingTax__");
	/**
	 * Query.SAPQuery callback
	 * @this QueryResult
	 */
	function getWHTTypeCallback()
	{
		var count = this.GetRecordsCount();
		if (!this.GetQueryError() && count > 0)
		{
			WHTTable.SetItemCount(count);
			var _loop_1 = function (i)
			{
				var type = this_1.GetQueryValue("WITHT", i);
				var code = this_1.GetQueryValue("WT_WITHCD", i);
				var WHTItem = WHTTable.GetItem(i);
				WHTItem.SetValue("WHTType__", type);
				WHTItem.SetValue("WHTCode__", code);
				var descriptionFilter = code ? "(WITHT = '" + type + "' AND WT_WITHCD = '" + code + "')" : "(WITHT = '" + type + "')";
				descriptionFilter = "( SPRAS = '" + language + "' AND LAND1 = '" + Data.GetValue("VendorCountry__") + "' AND " + descriptionFilter + ")";
				Sys.GenericAPI.SAPQuery(Variable.GetValueAsString("SAPConfiguration"), "T059ZT", descriptionFilter, ["TEXT40"], function (results)
				{
					if (results && results.length)
					{
						WHTItem.SetValue("WHTDescription__", results[0].TEXT40);
					}
				}, 1);
			};
			var this_1 = this;
			for (var i = 0; i < count; i++)
			{
				_loop_1(i);
			}
		}
		LayoutHelper.DisableButtons(false, "onVendorChange");
	}

	function getLanguageCallback(lang)
	{
		language = lang;
		var filter = "( BUKRS = '" + Data.GetValue("CompanyCode__") + "' AND LIFNR = '" + Sys.Helpers.String.SAP.NormalizeID(Data.GetValue("VendorNumber__"), 10) + "' )";
		Query.SAPQuery(getWHTTypeCallback, Variable.GetValueAsString("SAPConfiguration"), "LFBW", "WITHT|WT_WITHCD", filter, 20, 0);
	}
	WHTTable.SetItemCount(0);
	Sys.Helpers.Browse.SAPGetConnectionLanguage(getLanguageCallback);
}

function RefreshExtendedWHTAmount(row)
{
	if (row.WHTType__.GetValue() && row.WHTCode__.GetValue())
	{
		// forbids 0 as a valid value to avoid misunderstandind between empty, 0 and other values
		if (!row.WHTBaseAmount__.GetText())
		{
			row.WHTBaseAmount__.SetError("This field is required!");
		}
		else if (row.WHTBaseAmount__.GetValue() === 0)
		{
			row.WHTBaseAmount__.SetError("_value must be different from 0.");
		}
		else
		{
			row.WHTBaseAmount__.SetError();
		}
		if (row.WHTTaxAmount__.GetValue() === 0)
		{
			row.WHTTaxAmount__.SetError("_value must be different from 0. Empty field for ERP automatic computing.");
		}
		else
		{
			row.WHTTaxAmount__.SetError();
		}
	}
	else
	{
		row.WHTBaseAmount__.SetError();
		row.WHTTaxAmount__.SetError();
	}
	// use GetText to make the difference between 0 and empty
	var WHTTaxAmount = row.WHTTaxAmount__.GetText();
	// set data to be sure to have access to the value in simulation
	var item = row.GetItem();
	item.SetValue("WHTTaxAmountAuto__", !WHTTaxAmount);
	// set control for immediat visibility
	row.WHTTaxAmountAuto__.Check(!WHTTaxAmount);
}

function onExtendedWHTRefreshRow(index)
{
	RefreshExtendedWHTAmount(Controls.ExtendedWithholdingTax__.GetRow(index));
}

function ExtendedWHTAmountChanged()
{
	RefreshExtendedWHTAmount(this.GetRow());
}

function RefreshExtendedWHTAmounts()
{
	var count = Controls.ExtendedWithholdingTax__.GetItemCount();
	for (var i = 0; i < count; i++)
	{
		var row = Controls.ExtendedWithholdingTax__.GetRow(i);
		RefreshExtendedWHTAmount(row);
	}
}

function recomputeTaxOnDimensionChangeIfNeeded(item)
{
	if (!Controls.ManualLink__.IsChecked() && Lib.ERP.IsSAP())
	{
		var recomputeTax = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SAP.ShouldAddAccDataParameterForTaxComputation");
		if (Sys.Helpers.IsBoolean(recomputeTax) && recomputeTax)
		{
			if (item)
			{
				getTaxRateAndUpdateItem(item);
			}
			else
			{
				for (var i = 0; i < Controls.LineItems__.GetItemCount(); i++)
				{
					getTaxRateAndUpdateItem(Controls.LineItems__.GetItem(i), null, true);
				}
			}
		}
	}
}
/**
 * event triggered when Vendor (name or number) changes
 * @this DatabaseComboBox
 * @param manualLinkParams
 */
function onVendorChange(manualLinkParams)
{
	var isVendorNameChanged = this.GetName() === "VendorName__";
	var waitforWHT = false;
	LayoutHelper.DisableButtons(true, "onVendorChange");
	Data.SetValue("CodingTemplate__", "");
	if (!this.GetValue())
	{
		callBack();
	}
	else if (isVendorNameChanged)
	{
		Lib.P2P.Browse.GetVendorByName(callBack, Data.GetValue("CompanyCode__"), this.GetValue());
	}
	else
	{
		Lib.P2P.Browse.GetVendorByNumber(callBack, Data.GetValue("CompanyCode__"), this.GetValue());
	}

	function callBack(queryResult)
	{
		var erpManager = Lib.AP.GetInvoiceDocument();
		var withholdingTaxParameter = Lib.ERP.IsSAP() ? Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "") : "";
		var hasResult = queryResult && !queryResult.GetQueryError() && queryResult.Records && queryResult.Records.length > 0;
		recomputeTaxOnDimensionChangeIfNeeded(null);
		if (!hasResult)
		{
			if (isVendorNameChanged)
			{
				Controls.VendorNumber__.SetValue("");
			}
			else
			{
				Controls.VendorName__.SetValue("");
			}
			Controls.VendorStreet__.SetValue("");
			Controls.VendorPOBox__.SetValue("");
			Controls.VendorCity__.SetValue("");
			Controls.VendorZipCode__.SetValue("");
			Controls.VendorRegion__.SetValue("");
			Controls.VendorCountry__.SetValue("");
			var customFields = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.GetVendorCustomFields");
			if (customFields)
			{
				Sys.Helpers.Array.ForEach(customFields, function (field)
				{
					Data.SetValue(field.nameInForm, "");
				});
			}
			Variable.SetValueAsString("DefaultAlternativePayee", "");
			if (!manualLinkParams)
			{
				// Keep value from invoice when called from GetInvoiceDocument
				Controls.PaymentTerms__.SetValue("");
				Controls.WithholdingTax__.SetValue("");
				if (withholdingTaxParameter === "Extended")
				{
					var WHTTable = Data.GetTable("ExtendedWithholdingTax__");
					WHTTable.SetItemCount(0);
				}
			}
			EmbeddedViews.HideAllEmbeddedViewPanels();
			BankDetails.hide();
			Controls.SupplierInformationManagementLink__.SetDisabled(true);
			if (queryResult && queryResult.GetQueryError())
			{
				Log.Error("You might need to install the latest view definition on your SAP server");
			}
		}
		else
		{
			var ind_1 = 0;
			// Interrogate vendor query results if more than 1 result has been returned, set vendor fields below accordingly
			if (queryResult.Records.length > 1)
			{
				for (var i = 0; i < queryResult.Records.length; i++)
				{
					var formName = Controls.VendorName__.GetValue();
					var formNumber = Controls.VendorNumber__.GetValue();
					var queryName = queryResult.GetQueryValue("Name__", i);
					var queryNumber = queryResult.GetQueryValue("Number__", i);
					if (formName === queryName && formNumber === queryNumber)
					{
						ind_1 = i;
						break;
					}
				}
			}
			Controls.VendorNumber__.SetValue(queryResult.GetQueryValue("Number__", ind_1));
			Controls.VendorName__.SetValue(queryResult.GetQueryValue("Name__", ind_1));
			Controls.VendorStreet__.SetValue(queryResult.GetQueryValue("Street__", ind_1));
			Controls.VendorPOBox__.SetValue(queryResult.GetQueryValue("PostOfficeBox__", ind_1));
			Controls.VendorCity__.SetValue(queryResult.GetQueryValue("City__", ind_1));
			Controls.VendorZipCode__.SetValue(queryResult.GetQueryValue("PostalCode__", ind_1));
			Controls.VendorRegion__.SetValue(queryResult.GetQueryValue("Region__", ind_1));
			Controls.VendorCountry__.SetValue(queryResult.GetQueryValue("Country__", ind_1));
			var customFields = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.GetVendorCustomFields");
			if (customFields)
			{
				Sys.Helpers.Array.ForEach(customFields, function (field)
				{
					Data.SetValue(field.nameInForm, queryResult.GetQueryValue(field.nameInTable, ind_1));
				});
			}
			if (!manualLinkParams)
			{
				// Keep value from invoice when called from GetInvoiceDocument
				Controls.PaymentTerms__.SetValue(queryResult.GetQueryValue("PaymentTermCode__", ind_1));
				Controls.WithholdingTax__.SetValue(queryResult.GetQueryValue("WithholdingTax__", ind_1));
				if (withholdingTaxParameter === "Extended")
				{
					retrieveWithholdingTaxInfo();
					waitforWHT = true;
				}
			}
			EmbeddedViews.FillVisibleViews();
			var disable = !Controls.VendorNumber__.GetValue() || !Controls.VendorName__.GetValue();
			EmbeddedViews.SetDisabledAllButtons(disable);
			checkIBAN(true);
			BankDetails.getBankDetails(erpManager);
			Controls.SupplierInformationManagementLink__.SetDisabled(false);
		}
		erpManager.ComputePaymentAmountsAndDates(true, true);
		erpManager.ResetBankDetailsSelection();
		// Reset extracted contract number because VendorNumber changed
		Data.SetValue("ContractNumber__", "");
		if (hasResult && manualLinkParams && manualLinkParams.manualLink && manualLinkParams.GetExtendedWithholdingTax)
		{
			// SAP request dependant on VendorCountry__
			manualLinkParams.GetExtendedWithholdingTax.apply(null, manualLinkParams.GetExtendedWithholdingTaxParams ? manualLinkParams.GetExtendedWithholdingTaxParams : []);
		}
		if (!waitforWHT)
		{
			LayoutHelper.DisableButtons(false, "onVendorChange");
		}
		checkPOInvoiceLineItemTableVendor();
		vendorContact.FillVendorContactEmail();
		if (Lib.AP.InvoiceType.isConsignmentInvoice() && Data.GetValue("VendorNumber__"))
		{
			Lib.AP.SAP.CheckVendorNumber();
		}
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnVendorChange");
	}
	Lib.AP.AnomalyDetection.OnChangeRefreshAnomaly();
	// reload P2P Parameters table values
	resetParameters();
}

function waitScreenDuringLoadTemplateAction(display)
{
	Controls.ButtonLoadTemplate__.Wait(display);
}

function loadTemplate()
{
	if (Lib.AP.InvoiceType.isGLInvoice())
	{
		var vendorNumber = Data.GetValue("VendorNumber__");
		var companyCode = Data.GetValue("CompanyCode__");
		var lineItemsTable = Data.GetTable("LineItems__");
		var templateName = Data.GetValue("CodingTemplate__");
		var extractedNetAmount = Data.GetValue("ExtractedNetAmount__");
		lineItemsTable.SetItemCount(0);
		var noResultCallback = function ()
		{
			if (Data.GetValue("CodingTemplate__"))
			{
				Data.SetWarning("CodingTemplate__", "_TemplateWarning");
			}
		};
		var callbackForEachLine = function (item)
		{
			getTaxRateAndUpdateItem(item, null, true);
			fillGLAndCCDescriptions(item);
		};
		var callbackFinal = function ()
		{
			GetTaxRateForItemList();
		};
		var invoiceDoc = Lib.AP.GetInvoiceDocument();
		invoiceDoc.LoadTemplate(extractedNetAmount, companyCode, vendorNumber, templateName, lineItemsTable, noResultCallback, callbackForEachLine, callbackFinal, waitScreenDuringLoadTemplateAction);
	}
}

function setGRIVMode(ignoreRefresh)
{
	if (Lib.ERP.IsSAP())
	{
		if (!ignoreRefresh)
		{
			Data.SetValue("GRIV__", false);
		}
	}
	else if (!Data.GetValue("GRIV__") && g_apParameters.IsReady())
	{
		Data.SetValue("GRIV__", Lib.P2P.IsGRIVEnabledGlobally());
	}
	if (ignoreRefresh !== true)
	{
		LayoutHelper.AdaptProcessLayoutToInvoiceType(false);
	}
}

function switchERPAndUpdateLayout()
{
	Lib.AP.GetInvoiceDocumentLayout().Reset();
	Lib.AP.ResetERPManager();
	Lib.AP.GetInvoiceDocumentLayout().Init();
	LayoutHelper.UpdateLayout();
	setGRIVMode();
}

function OnChangeVendorContactEmail()
{
	var newEmail = Controls.VendorContactEmail__.GetValue();
	var error = "";
	if (newEmail && !Sys.Helpers.String.IsEmail(newEmail))
	{
		error = "_Not a valid email address";
	}
	Controls.VendorContactEmail__.SetError(error);
	Controls.ContactVendor__.SetDisabled(Sys.Helpers.IsEmpty(newEmail));
}

function onERPChange()
{
	if (!FormCleaner.isDataCleanForERP())
	{
		Popup.Confirm("_This action will delete company code related fields.", false, function ()
		{
			FormCleaner.adaptDataToERP();
			switchERPAndUpdateLayout();
		}, FormCleaner.revertERP, "_Warning");
	}
	else
	{
		FormCleaner.adaptDataToERP();
		switchERPAndUpdateLayout();
	}
}

function onERPInvoiceNumberChange()
{
	Lib.AP.GetInvoiceDocumentLayout().ManualLinkERPInvoiceNumber(Controls.ERPInvoiceNumber__, LayoutHelper, InvoiceLineItem);
}

function onMMERPInvoiceNumberChange()
{
	Lib.AP.GetInvoiceDocumentLayout().ManualLinkERPInvoiceNumber(Controls.ERPMMInvoiceNumber__, LayoutHelper, InvoiceLineItem);
}

function onContactVendorClick()
{
	if (!Controls.PortalRuidEx__.GetValue())
	{
		if (!Controls.VendorContactEmail__.GetError())
		{
			var OK = function ()
			{
				ProcessInstance.Approve("ContactVendor");
			};
			var Cancel = function ()
			{
				return false;
			};
			Popup.Confirm(Language.Translate("_Confirm create CI and contact vendor", true, Controls.VendorName__.GetValue()), false, OK, Cancel, "_Contact the vendor");
		}
		else
		{
			Controls.VendorContactEmail__.ShowErrorMessage();
		}
	}
	else
	{
		Controls.ConversationUI__.Focus();
	}
	return false;
}
Controls.ContactVendor__.SetDisabled(Sys.Helpers.IsEmpty(Controls.VendorContactEmail__.GetValue()));

function onInvoiceTypeChange()
{
	//Do not update others fields until the user validate the popup
	LayoutHelper.AdaptProcessLayoutToInvoiceType(true);
	Controls.InvoiceDescription__.SetValue("");
	Data.SetValue("CurrentException__", "");
}

function onInvoiceAmountChange()
{
	Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, true);
	Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
	checkBalance(true);
	Lib.AP.AnomalyDetection.OnChangeRefreshAnomaly();
}

function onUnplannedDeliveryCostsChange()
{
	checkBalance(true);
}

function onInvoiceCurrencyChange()
{
	// g_invoiceCurrency is updated later by checkInvoiceCurrency because we want to store a new value only if it is valid
	var oldCurrency = g_invoiceCurrency;
	checkInvoiceCurrency();
	convertAmounts(oldCurrency)
		.Then(function (needBalanceUpdate)
		{
			Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
			if (needBalanceUpdate)
			{
				checkBalance(true);
			}
		});
}

function onCurrentExceptionChange()
{
	Variable.SetValueAsString("isExtractionReviewException", "");
	var currentException = Data.GetValue("CurrentException__");
	if (currentException)
	{
		var options = {
			table: "WFRule",
			filter: "(&(Active=1)(name=" + currentException + "))",
			attributes: ["WorkflowType"],
			maxRecords: 1
		};
		Sys.GenericAPI.PromisedQuery(options)
			.Then(function (queryResults)
			{
				if (queryResults.length > 0)
				{
					queryResults.forEach(function (r)
					{
						Variable.SetValueAsString("manualExceptionType", r.WorkflowType);
					});
				}
				Lib.AP.WorkflowCtrl.Rebuild(true, true, "exceptionChanged");
			});
	}
	else
	{
		Variable.SetValueAsString("manualExceptionType", "");
		Lib.AP.WorkflowCtrl.Rebuild(true, true, "exceptionChanged");
	}
}

function onContractNumberChange()
{
	handleContractValidity();
}

function onContractNumberSelect()
{
	handleContractValidity();
}

function handleContractValidity()
{
	if (Sys.Parameters.GetInstance("P2P").GetParameter("EnableContractGlobalSetting", "") === "1")
	{
		Lib.AP.Contract.HandleContractValidity();
	}
}
var FormCleaner;
(function (FormCleaner)
{
	//Allow to cancel OnChange actions
	var previousCompanyCode = Data.GetValue("CompanyCode__");
	var previousERP = Data.GetValue("ERP__");
	var columnsToCheck = ["OrderNumber__", "ItemNumber__", "Description__", "GLAccount__", "CostCenter__", "TaxCode__"];
	// Save new company code and clear or update fields that needs to
	function adaptDataToCompanyCode()
	{
		var companyCodeHasChanged = previousCompanyCode !== Data.GetValue("CompanyCode__");
		previousCompanyCode = Data.GetValue("CompanyCode__");
		// When user changes the ERP with combobox, the company code doesn't change.
		if (previousCompanyCode && companyCodeHasChanged)
		{
			Lib.P2P.CompanyCodesValue.QueryValues(previousCompanyCode).Then(function (CCValues)
			{
				if (Object.keys(CCValues).length > 0)
				{
					var newERP = CCValues.ERP__;
					if (newERP)
					{
						previousERP = newERP;
						Data.SetValue("ERP__", previousERP);
						Controls.InvoiceType__.SetText(Lib.AP.GetInvoiceTypeList());
					}
					else
					{
						Log.Info("No ERP defined for the company code '" + previousCompanyCode + "'");
					}
					var newConf_1 = CCValues.DefaultConfiguration__;
					var previousConf = Data.GetValue("Configuration__");
					if (!newConf_1)
					{
						Log.Warn("No default configuration found for company code " + previousCompanyCode + ", no change");
					}
					else if (newConf_1 && previousConf && newConf_1 !== previousConf)
					{
						var popupMsg = Language.Translate("_The default configuration will be changed according to the new company code {0} From  {1} to {2}", true, previousCompanyCode, previousConf, newConf_1);
						Popup.Confirm(popupMsg, false, function ()
						{
							return applyConfigurationChange(newConf_1);
						}, null, "Warning");
					}
				}
			});
		}
		//Clear data related to company code filter
		Data.SetValue("VendorNumber__", "");
		Data.SetValue("OrderNumber__", "");
		Data.SetValue("ContractNumber__", "");
		Data.SetValue("CurrentException__", "");
		Data.SetValue("ERPInvoiceNumber__", "");
		Data.SetValue("ERPMMInvoiceNumber__", "");
		Data.SetValue("Investment__", "");
		Controls.VendorNumber__.OnChange();
		var invoiceType = Data.GetValue("InvoiceType__");
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		switch (invoiceType)
		{
		case Lib.AP.InvoiceType.NON_PO_INVOICE:
			for (var i = 0; i < nbItems; i++)
			{
				var item = lineItemsTable.GetItem(i);
				item.SetValue("CostCenter__", "");
				item.SetValue("GLAccount__", "");
				item.SetValue("GLDescription__", "");
				item.SetValue("TaxCode__", "");
				item.SetValue("TaxJurisdiction__", "");
				item.SetError("TaxJurisdiction__");
				item.SetValue("TaxRate__", "");
				item.SetValue("MultiTaxRates__", "");
				item.SetValue("TaxAmount__", "");
				item.SetValue("CompanyCode__", "");
				item.SetValue("ProjectCode__", "");
			}
			break;
		case Lib.AP.InvoiceType.CONSIGNMENT:
		case Lib.AP.InvoiceType.PO_INVOICE:
			lineItemsTable.SetItemCount(0);
			break;
		default:
			Log.Info("Unsupported invoice type '" + invoiceType + "'");
			break;
		}
		Controls.NetAmount__.SetValue(Lib.AP.GetInvoiceDocumentLayout().computeHeaderAmount());
		Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
		Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, true);
		updateLocalCurrency();
		updateExchangeRate(doCheckBalance);
		Lib.AP.AnomalyDetection.OnChangeRefreshAnomaly();
	}
	FormCleaner.adaptDataToCompanyCode = adaptDataToCompanyCode;
	// Restore to previous company code
	function revertCompanyCode()
	{
		Controls.CompanyCode__.SetValue(previousCompanyCode);
	}
	FormCleaner.revertCompanyCode = revertCompanyCode;
	// Save new erp name and clear fields that needs to
	function adaptDataToERP()
	{
		previousERP = Data.GetValue("ERP__");
		// No clear of CompanyCode because ERP depends on CompanyCode
		FormCleaner.adaptDataToCompanyCode();
	}
	FormCleaner.adaptDataToERP = adaptDataToERP;
	// Restore previous erp name
	function revertERP()
	{
		// Revert value
		Controls.ERP__.SetValue(previousERP);
	}
	FormCleaner.revertERP = revertERP;
	// Tells if erp can be changed without notice
	function isDataCleanForERP()
	{
		return FormCleaner.isDataClean();
	}
	FormCleaner.isDataCleanForERP = isDataCleanForERP;
	// Tells if company code can be changed without notice
	function isDataClean()
	{
		if (Data.GetValue("VendorNumber__") || Data.GetValue("OrderNumber__") || Data.GetValue("CurrentException__"))
		{
			return false;
		}
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (!FormCleaner.isLineItemClean(item))
			{
				return false;
			}
		}
		return true;
	}
	FormCleaner.isDataClean = isDataClean;

	function isLineItemClean(item)
	{
		for (var _i = 0, columnsToCheck_1 = columnsToCheck; _i < columnsToCheck_1.length; _i++)
		{
			var columnToCheck = columnsToCheck_1[_i];
			if (item.GetValue(columnToCheck))
			{
				return false;
			}
		}
		return true;
	}
	FormCleaner.isLineItemClean = isLineItemClean;
})(FormCleaner || (FormCleaner = {}));

function applyCompanyCodeChange()
{
	var previousERP = Data.GetValue("ERP__");
	FormCleaner.adaptDataToCompanyCode();
	var currentERP = Data.GetValue("ERP__");
	if (currentERP !== previousERP)
	{
		switchERPAndUpdateLayout();
	}
}

function applyConfigurationChange(conf)
{
	Sys.Parameters.GetInstance("AP").Reload(conf);
	Sys.Parameters.GetInstance("AP").IsReady(function ()
	{
		Log.Info("Manual change of company code - Resubmit with new matching configuration: " + conf);
		ProcessInstance.ResubmitAsynchronous("reprocess",
		{
			CompanyCode: Data.GetValue("CompanyCode__"),
			configuration: conf,
			tableParameters: Variable.GetValueAsString("tableParameters"),
			forwardToCorrectAP: "1"
		});
	});
}

function onCompanyCodeChange()
{
	// To be sure all information have been retrieved.
	// Allow to call every method synchronously
	Lib.P2P.CompanyCodesValue.QueryValues(Data.GetValue("CompanyCode__")).Then(function ( /*ccValues*/ )
	{
		if (!FormCleaner.isDataClean())
		{
			Popup.Confirm("_This action will delete company code related fields.", false, applyCompanyCodeChange, FormCleaner.revertCompanyCode, "_Warning");
		}
		else
		{
			applyCompanyCodeChange();
		}
	});
}

function onOrderNumberChange()
{
	Lib.AP.TrimOrderNumbers();
}
/**
 * event when line item Assignment number changes
 * @this ShortText in Line Item
 */
function onConsignmentNumberChange()
{
	Lib.AP.SAP.Consignment.OnChange(this.GetValue(), fillGLAndCCDescriptions, GetTaxRateForItemList, LayoutHelper);
}

function onInvoiceDescriptionChange()
{
	var count = Controls.LineItems__.GetItemCount();
	for (var i = 0; i < count; i++)
	{
		InvoiceLineItem.FillLineItemDescription(Controls.LineItems__.GetItem(i), i);
	}
}

function onAssignementChange()
{
	var count = Controls.LineItems__.GetItemCount();
	for (var i = 0; i < count; i++)
	{
		InvoiceLineItem.FillLineItemAssignment(Controls.LineItems__.GetItem(i), i);
	}
}

function getCostCenterDescriptionFromId(CCId, callback, companyCode)
{
	Lib.P2P.Browse.GetCostCenterDescription(callback, companyCode, CCId);
}
/**
 * event when line item cost center changes
 * @this DatabaseComboBox in Line Item
 */
function onCostCenterChange()
{
	var item = this.GetItem();
	recomputeTaxOnDimensionChangeIfNeeded(item);
	var param = Lib.ERP.IsSAP() ? GetFillCCSAPParameters(item) : GetFillCCERPParameters(item);
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, param);
	Lib.AP.WorkflowCtrl.UpdateWorkflowOnDimensionUpdate("costCenterUpdated");
}
/** event when line item Company code changes
 * @this DatabaseComboBox in Line Item
 */
function onCrossItemCompanyCodeChange()
{
	Lib.AP.Browse.UpdateFieldsOnCrossCompanyCodeUpdate(this);
	Lib.AP.WorkflowCtrl.UpdateWorkflowOnDimensionUpdate("crossCompanyCodeUpdated");
}
/** event when line item WBS element changes
 * @this DatabaseComboBox in Line Item
 */
function onWBSElementChange()
{
	var _this = this;
	var callback = function (wbsElemID)
	{
		if (!wbsElemID)
		{
			_this.GetItem().SetValue("WBSElementID__", "");
		}
		else
		{
			_this.GetItem().SetValue("WBSElementID__", wbsElemID);
		}
		LayoutHelper.DisableButtons(false, "onWBSElementChange");
	};
	LayoutHelper.DisableButtons(true, "onWBSElementChange");
	if (!this.GetValue())
	{
		callback("");
	}
	else
	{
		Lib.AP.Browse.GetWBSElementID(callback, Data.GetValue("CompanyCode__"), this.GetValue());
	}
}
/** event when line item Order number is clicked on
 * @this ShortText in Line Item
 */
function onOrderNumberClick()
{
	LayoutHelper.DisableButtons(true, "onOrderNumberClick");
	/**
	 * Query.DBQuery callback
	 * @this QueryResult
	 */
	var callbackSearchPO = function ()
	{
		var queryValue = this.GetQueryValue();
		if (queryValue.Records && queryValue.Records.length > 0)
		{
			Process.OpenLink(queryValue.Records[0][1]);
		}
		else
		{
			Popup.Alert("_Purchase order not found or access denied", false, null, "_Purchase order not found title");
		}
		LayoutHelper.DisableButtons(false, "onOrderNumberClick");
	};
	var attributes = "MsnEx|ValidationURL";
	var filter = "OrderNumber__=" + this.GetValue();
	Query.DBQuery(callbackSearchPO, "CDNAME#Purchase order V2", attributes, filter, "", 1);
}

function onLineItemDelete(item)
{
	Controls.NetAmount__.SetValue(Lib.AP.GetInvoiceDocumentLayout().computeHeaderAmount());
	Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
	Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
	Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
	checkBalance(true);
	InvoiceLineItem.RemoveLineItem(item);
	checkPOInvoiceLineItems();
	InvoiceLineItem.UpdateLineItemsLayout();
}
/**
 * event when line item Amount changes
 * @this Decimal in Line Item
 */
function lineItemsAmountChanged()
{
	var item = this.GetItem();
	if (Controls.CalculateTax__.IsChecked())
	{
		var taxrate = this.GetRow().TaxRate__.GetValue();
		var netamount = this.GetRow().Amount__.GetValue();
		var multitaxrates = this.GetRow().MultiTaxRates__.GetValue();
		var taxamount = Sys.Helpers.Round(Lib.AP.ApplyTaxRate(netamount, taxrate, multitaxrates), Lib.AP.GetAmountPrecision());
		this.GetRow().TaxAmount__.SetValue(taxamount);
		Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
	}
	else
	{
		this.GetRow().TaxRate__.SetValue(0);
	}
	// If an item is not a GL Item and is Amount based, the quantity is hidden and set to be equal to the amount
	if (Lib.P2P.InvoiceLineItem.IsAmountBasedRow(this.GetRow()) && !Lib.P2P.InvoiceLineItem.IsGLLineItem(item))
	{
		var amount = this.GetRow().Amount__.GetValue();
		this.GetRow().Quantity__.SetValue(amount);
	}
	Controls.NetAmount__.SetValue(Lib.AP.GetInvoiceDocumentLayout().computeHeaderAmount());
	Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
	Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
	checkBalance(true);
	if (item)
	{
		checkPOInvoiceLineItems();
	}
}
/**
 * event when line item Quantity changes
 * @this Decimal in Line Item
 */
function lineItemsQuantityChanged()
{
	var item = this.GetItem();
	if (item)
	{
		checkPOInvoiceLineItems();
	}
	var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
	if (!isPosted)
	{
		var actionName = "quantityChanged";
		var guessException = Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.Workflow.AutoGuessException",
		{
			name: actionName
		}, true, true) || false;
		if (guessException)
		{
			Lib.AP.WorkflowCtrl.Rebuild(true, currentStepIsApStart(), actionName);
		}
	}
}
/**
 * event when line item Tax code changes
 * @this DatabaseComboBox in Line Item
 */
function taxCodeConvertedInUpperCase()
{
	var taxCode = this.GetValue();
	if (taxCode && Lib.ERP.IsSAP())
	{
		this.SetValue(taxCode.substring(0, 2).toUpperCase());
	}
}
/**
 * event when line item Tax code changes
 * @this DatabaseComboBox in Line Item
 */
function calculateTaxAmounts()
{
	var taxCode = this.GetValue();
	if (taxCode)
	{
		if (Lib.ERP.IsSAP())
		{
			this.SetValue(taxCode.substring(0, 2).toUpperCase());
		}
		getTaxRateAndUpdateItem(this.GetItem());
	}
	else
	{
		updateItemWithTaxRate(this.GetItem(), 0);
	}
}
var browseItemTaxJurisdiction = {
	dialog: null,
	searchControlFocused: false,
	onCloseCallback: null,
	rowCount: 20,
	rowCountPerPage: 10,
	resultTableName: "resultTable",
	// Column configuration
	columns: [
	{
		id: "TXJCD",
		label: "_Jurisdiction code",
		type: "STR",
		width: 80
	},
	{
		id: "TEXT1",
		label: "_Description",
		type: "STR",
		width: 500
	}],
	// Search criterion configuration
	searchCriterias: [
	{
		id: "COUNTRY",
		label: "_Country",
		required: true,
		toUpper: true,
		visible: true,
		filterId: "TEXT1",
		defaultValue: "",
		width: 180
	},
	{
		id: "REGION",
		label: "_Region",
		required: false,
		toUpper: true,
		visible: true,
		filterId: "TEXT1",
		defaultValue: "",
		width: 180
	},
	{
		id: "COUNTY",
		label: "_County",
		required: false,
		toUpper: true,
		visible: true,
		filterId: "TEXT1",
		defaultValue: "",
		width: 180
	},
	{
		id: "CITY",
		label: "_City",
		required: false,
		toUpper: true,
		visible: true,
		filterId: "TEXT1",
		defaultValue: "",
		width: 180
	},
	{
		id: "ZIPCODE",
		label: "_Zip code",
		required: false,
		toUpper: true,
		visible: true,
		filterId: "TEXT1",
		defaultValue: "",
		width: 180
	}],
	// Buffer for filter values to avoid inconsistencies during queries
	filterValues: null,
	BAPIParams:
	{
		"EXPORTS":
		{
			"DEST": "",
			"LOCATION_DATA":
			{
				"COUNTRY": "",
				"CITY": "",
				"STATE": "",
				"COUNTY": "",
				"ZIPCODE": "",
				"TXJCD": "",
				"TXJCD_L1": "",
				"TXJCD_L2": "",
				"TXJCD_L3": "",
				"TXJCD_L4": ""
			}
		},
		"USECACHE": true
	},
	// popup
	init: function (callback)
	{
		this.onCloseCallback = callback;
		Popup.Dialog("_Line item jurisdiction code", null, this.fillSearchDialog, null, null, this.handleSearchDialog);
	},
	// handle results return
	returnResultsExternal: function (jsonResult)
	{
		if (jsonResult.TABLES.LOCATION_RESULTS && jsonResult.TABLES.LOCATION_RESULTS.length > 0)
		{
			browseItemTaxJurisdiction.fillTableWithResultFromExternalJurisdictionQuery(jsonResult);
			browseItemTaxJurisdiction.completedCallBackWithoutMessage();
		}
		else
		{
			browseItemTaxJurisdiction.completedCallBackWithNoMatchMessage();
		}
	},
	fillTableWithResultFromExternalJurisdictionQuery: function (jsonResult)
	{
		var table = browseItemTaxJurisdiction.dialog.GetControl(browseItemTaxJurisdiction.resultTableName);
		var nbResults = jsonResult.TABLES.LOCATION_RESULTS.length;
		table.SetItemCount(nbResults);
		for (var iRecord = 0; iRecord < nbResults; iRecord++)
		{
			var item = table.GetItem(iRecord);
			var descriptionTab = [];
			var record = jsonResult.TABLES.LOCATION_RESULTS[iRecord];
			if (record.COUNTRY && record.COUNTRY.length > 0)
			{
				descriptionTab.push(record.COUNTRY);
			}
			if (record.STATE && record.STATE.length > 0)
			{
				descriptionTab.push(record.STATE);
			}
			if (record.COUNTY && record.COUNTY.length > 0)
			{
				descriptionTab.push(record.COUNTY);
			}
			if (record.CITY && record.CITY.length > 0)
			{
				descriptionTab.push(record.CITY);
			}
			if (record.ZIPCODE && record.ZIPCODE.length > 0)
			{
				descriptionTab.push(record.ZIPCODE);
			}
			item.SetValue("TXJCD", record.TXJCD);
			item.SetValue("TEXT1", descriptionTab.join(","));
		}
	},
	// request
	request: function ()
	{
		Sys.Helpers.Browse.ResetTable(browseItemTaxJurisdiction.dialog);
		//Store filter values
		browseItemTaxJurisdiction.filterValues = {};
		for (var _i = 0, _a = browseItemTaxJurisdiction.searchCriterias; _i < _a.length; _i++)
		{
			var searchCriteria = _a[_i];
			browseItemTaxJurisdiction.filterValues[searchCriteria.id] = browseItemTaxJurisdiction.dialog.GetControl("searchCriteria_" + searchCriteria.id).GetValue();
		}
		browseItemTaxJurisdiction.searchTaxProcedure();
	},
	// search Tax Procedudure Info
	searchTaxProcedure: function ()
	{
		Query.SAPQuery(browseItemTaxJurisdiction.searchTaxProcedureCallback, Variable.GetValueAsString("SAPConfiguration"), "T005", "KALSM", "LAND1 = '" + browseItemTaxJurisdiction.filterValues.COUNTRY + "'", 1, 0);
	},
	searchTaxProcedureCallback: function ()
	{
		if (this.GetQueryError())
		{
			Sys.Helpers.Browse.CompletedCallBack(browseItemTaxJurisdiction.dialog, this, true);
			return;
		}
		var taxProcedure = this.GetQueryValue("KALSM", 0);
		if (taxProcedure)
		{
			Query.SAPQuery(browseItemTaxJurisdiction.searchTaxRFCDestCallback, Variable.GetValueAsString("SAPConfiguration"), "TTXD", "KALSM|XEXTN|LENG1|LENG2|LENG3|LENG4|RFCDEST", "KALSM = '" + taxProcedure + "'", 1, 0);
		}
		else
		{
			browseItemTaxJurisdiction.completedCallBackWithNoMatchMessage();
		}
	},
	searchTaxRFCDestCallback: function ()
	{
		if (this.GetQueryError())
		{
			Sys.Helpers.Browse.CompletedCallBack(browseItemTaxJurisdiction.dialog, this, true);
			return;
		}
		var taxProcedure = this.GetQueryValue("KALSM", 0);
		var external = this.GetQueryValue("XEXTN", 0);
		var length1 = this.GetQueryValue("LENG1", 0);
		var length2 = this.GetQueryValue("LENG2", 0);
		var length3 = this.GetQueryValue("LENG3", 0);
		var length4 = this.GetQueryValue("LENG4", 0);
		var rfcDest = this.GetQueryValue("RFCDEST", 0);
		if (external === "")
		{
			Sys.Helpers.Browse.SAPGetConnectionLanguage(function (language)
			{
				browseItemTaxJurisdiction.queryJuridisdictionInternal(taxProcedure, length1, length2, length3, length4, language);
			});
		}
		else
		{
			browseItemTaxJurisdiction.queryJurisdictionExternal(rfcDest, length1, length2, length3, length4);
		}
	},
	// query for Jurisdiction
	queryJuridisdictionInternal: function (taxProcedure, length1, length2, length3, length4, language)
	{
		Log.Info("internal query for jurisdiction");
		var state = browseItemTaxJurisdiction.filterValues.REGION;
		state = Sys.Helpers.String.PadRight(state ? state : "", "_", length1);
		var county = browseItemTaxJurisdiction.filterValues.COUNTY;
		county = Sys.Helpers.String.PadRight(county ? county : "", "_", length2);
		var city = browseItemTaxJurisdiction.filterValues.CITY;
		city = Sys.Helpers.String.PadRight(city ? city : "", "_", length3);
		var LocalTax = "";
		LocalTax = Sys.Helpers.String.PadRight(LocalTax, "_", length4);
		var txjcdFilter = state + county + city + LocalTax;
		txjcdFilter = txjcdFilter.replace(" ", "_");
		var filter = "SPRAS = '" + language + "' AND KALSM = '" + taxProcedure + "' AND TXJCD LIKE '" + txjcdFilter + "'";
		Query.SAPQuery(browseItemTaxJurisdiction.queryInternalCallback, Variable.GetValueAsString("SAPConfiguration"), "TTXJT", "TXJCD|TEXT1", filter, browseItemTaxJurisdiction.rowCount, 0);
	},
	queryInternalCallback: function ()
	{
		Sys.Helpers.Browse.CompletedCallBack(browseItemTaxJurisdiction.dialog, this, true);
		Sys.Helpers.Browse.FillTableFromQueryResult(browseItemTaxJurisdiction.dialog, "resultTable", this);
	},
	queryJurisdictionExternal: function (rfcDest, length1, length2, length3, length4)
	{
		Log.Info("external query for jurisdiction");
		if (rfcDest)
		{
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.DEST = rfcDest.toUpperCase();
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.COUNTRY = browseItemTaxJurisdiction.filterValues.COUNTRY;
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.STATE = browseItemTaxJurisdiction.filterValues.REGION;
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.ZIPCODE = browseItemTaxJurisdiction.filterValues.ZIPCODE;
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.CITY = browseItemTaxJurisdiction.filterValues.CITY;
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.COUNTY = browseItemTaxJurisdiction.filterValues.COUNTY;
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.TXJCD_L1 = length1.toString();
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.TXJCD_L2 = length2.toString();
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.TXJCD_L3 = length3.toString();
			browseItemTaxJurisdiction.BAPIParams.EXPORTS.LOCATION_DATA.TXJCD_L4 = length4.toString();
			browseItemTaxJurisdiction.callBAPIJurisdiction();
		}
		else
		{
			browseItemTaxJurisdiction.completedCallBackWithNoMatchMessage();
		}
	},
	callBAPIJurisdiction: function ()
	{
		Query.SAPCallBapi(browseItemTaxJurisdiction.returnResultsExternal, Variable.GetValueAsString("SAPConfiguration"), "Z_ESK_DETERMINE_JURISDICTION", browseItemTaxJurisdiction.BAPIParams);
	},
	// Draw and display the browse page
	fillSearchDialog: function (newDialog)
	{
		browseItemTaxJurisdiction.dialog = newDialog;
		Sys.Helpers.Browse.FillSearchDialog(newDialog, browseItemTaxJurisdiction.columns, browseItemTaxJurisdiction.searchCriterias, browseItemTaxJurisdiction.rowCount, browseItemTaxJurisdiction.rowCountPerPage, "", false, false);
	},
	// Handle event in browse page
	handleSearchDialog: function (handleDialog, action, event, control, parameter)
	{
		this.searchControlFocused = Sys.Helpers.Browse.HandleSearchDialog(handleDialog, action, event, control, parameter, browseItemTaxJurisdiction.searchControlFocused, browseItemTaxJurisdiction.request, browseItemTaxJurisdiction.returnSelectionCallback);
	},
	/* Update the main form with the user selection */
	returnSelectionCallback: function (_control, tableItem)
	{
		var selectedValue = tableItem.TXJCD.GetValue();
		if (selectedValue)
		{
			browseItemTaxJurisdiction.onCloseCallback(selectedValue);
			browseItemTaxJurisdiction.dialog.Cancel();
		}
	},
	setResultMessage: function (msg)
	{
		var resultCtrl = browseItemTaxJurisdiction.dialog.GetControl("resultMessage");
		if (resultCtrl)
		{
			resultCtrl.SetWarningStyle();
			if (msg)
			{
				resultCtrl.SetText(msg);
			}
		}
	},
	setWaitingIcon: function (hide)
	{
		var waitingIcon = browseItemTaxJurisdiction.dialog.GetControl("waitingIcon");
		if (waitingIcon)
		{
			waitingIcon.Hide(hide);
		}
	},
	completedCallBackWithNoMatchMessage: function ()
	{
		browseItemTaxJurisdiction.setResultMessage("_No match");
		browseItemTaxJurisdiction.setWaitingIcon(true);
	},
	completedCallBackWithoutMessage: function ()
	{
		browseItemTaxJurisdiction.setResultMessage();
		browseItemTaxJurisdiction.setWaitingIcon(true);
	}
};
/**
 * event when line item Tax jurisdiction is browsed
 * @this ShortText in Line Item
 */
function onTaxJurisdictionBrowse()
{
	this.Wait(true);
	var item = this.GetItem();
	browseItemTaxJurisdiction.init(browseItemTaxJurisdictionCallback);

	function browseItemTaxJurisdictionCallback(taxJurisdiction)
	{
		if (taxJurisdiction)
		{
			item.SetError("TaxJurisdiction__");
			item.SetValue("TaxJurisdiction__", taxJurisdiction);
			getTaxRateAndUpdateItem(item);
		}
		else if (Lib.AP.SAP.g_taxJurisdictionRequiredCache.Get(Data.GetValue("CompanyCode__")))
		{
			item.SetError("TaxJurisdiction__", "This field is required!");
			updateItemWithTaxRate(item, 0);
		}
	}
	return true;
}
/**
 * event when line item Tax jurisdiction changes
 * @this ShortText in Line Item
 */
function calculateTaxJurisdiction()
{
	var item = this.GetItem();
	var taxJurisdiction = this.GetValue();
	if (taxJurisdiction || !Lib.AP.SAP.g_taxJurisdictionRequiredCache.IsDefined(Data.GetValue("CompanyCode__")))
	{
		item.SetError("TaxJurisdiction__");
		getTaxRateAndUpdateItem(item);
	}
	else if (Lib.AP.SAP.g_taxJurisdictionRequiredCache.Get(Data.GetValue("CompanyCode__")))
	{
		item.SetError("TaxJurisdiction__", "This field is required!");
		updateItemWithTaxRate(item, 0);
	}
}

function handleTaxComputation(initEventsHandler)
{
	if (Controls.CalculateTax__.IsChecked())
	{
		Controls.LineItems__.TaxAmount__.SetReadOnly(true);
		Controls.LineItems__.TaxCode__.OnChange = calculateTaxAmounts;
		Controls.LineItems__.TaxJurisdiction__.OnChange = calculateTaxJurisdiction;
		Controls.LineItems__.TaxAmount__.OnChange = null;
		if (!initEventsHandler)
		{
			// Recalculate tax amount for each lines
			var table = Data.GetTable("LineItems__");
			for (var i = 0; i < table.GetItemCount(); i++)
			{
				getTaxRateAndUpdateItem(table.GetItem(i));
			}
		}
		Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
	}
	else
	{
		Controls.LineItems__.TaxAmount__.SetReadOnly(false);
		Controls.LineItems__.TaxCode__.OnChange = taxCodeConvertedInUpperCase;
		Controls.LineItems__.TaxJurisdiction__.OnChange = null;
		Controls.LineItems__.TaxAmount__.OnChange = function ()
		{
			Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
			Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
			checkBalance(true);
		};
		Controls.TaxAmount__.SetValue(Lib.AP.TaxHelper.computeHeaderTaxAmount());
	}
}

function onLineItemRefreshRow(index)
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		InvoiceLineItem.UpdateRowLayoutToLineType(Controls.LineItems__.GetRow(index));
		InvoiceLineItem.FillLineItemDescription(Controls.LineItems__.GetItem(index), index);
		InvoiceLineItem.FillLineItemAssignment(Controls.LineItems__.GetItem(index), index);
	}
	InvoiceLineItem.SetSubFocusStyle(Controls.LineItems__.GetRow(index));
	InvoiceLineItem.SetDispatchKey(Controls.LineItems__.GetRow(index));
}

function onLineItemFocusRow(index)
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		InvoiceLineItem.RefreshSubFocusStyles(Controls.LineItems__.GetRow(index));
	}
}

function onLineItemBlurRow( /*index: number*/ )
{
	if (!Lib.AP.InvoiceType.isGLInvoice())
	{
		InvoiceLineItem.RefreshSubFocusStyles();
	}
}

function getGLAccountDescription(GLAcctId, callback, companyCode)
{
	Lib.P2P.Browse.GetGlAccountDescription(callback, companyCode, GLAcctId);
}
/**
 * event when line item G/L account changes
 * @this DatabaseComboBox in Line Item
 */
function glAccountOnChange()
{
	var item = this.GetItem();
	recomputeTaxOnDimensionChangeIfNeeded(item);
	var param = Lib.ERP.IsSAP() ? GetFillGLSAPParameters(item) : GetFillGLERPParameters(item);
	Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, param);
	Lib.AP.WorkflowCtrl.UpdateWorkflowOnDimensionUpdate("glAccountUpdated");
	if (Lib.AP.InvoiceType.isGLInvoice())
	{
		Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.FillCostType", item);
	}
	Lib.P2P.fillCostTypeFromGLAccount(item);
}

function onDocumentDeleted()
{
	Variable.SetValueAsString("DocAttached", "");
	LayoutHelper.UpdateButtonBar();
	RequestTeaching.RefreshButtonState();
}
/** *********************** **/
/** initialization Function **/
/** *********************** **/
function userCanUpdatePayment()
{
	// User can update the payment details if the invoice is to pay and the user is an ap specialist (i.e. has the profile "Accounts Payable Profile")
	// Please change this condition if you use other profiles.
	return Controls.InvoiceStatus__.GetValue() === "To pay" && userIsAPOrAdmin();
}

function handleApproversListEvents()
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("WorkflowDisableRules") === "1")
	{
		return;
	}
	/**
	 * @this ShortText in Approvers List
	 */
	Controls.ApproversList__.Approver__.OnBrowse = function ()
	{
		var update = this.GetValue();
		var idx = parseInt(this.GetRow().WorkflowIndex__.GetValue(), 10);
		Controls.ApproversList__.Wait(true);
		browseApprovers(Lib.AP.WorkflowCtrl.GetStepRole(idx)).Then(function (approver)
		{
			if (approver)
			{
				if (update)
				{
					Lib.AP.WorkflowCtrl.UpdateContributorAt(idx, approver);
				}
				else
				{
					// if we come from an ampty line insert result above
					Lib.AP.WorkflowCtrl.AddContributorAt(idx, approver);
				}
			}
		});
	};
	Controls.ApproversList__.OnCheckIfItemDeletable = function (item, idx)
	{
		return Lib.AP.WorkflowCtrl.IsContributorDeletableWithRole(idx, Lib.AP.WorkflowCtrl.roles.approver) || Lib.AP.WorkflowCtrl.IsContributorDeletableWithRole(idx, Lib.AP.WorkflowCtrl.roles.controller);
	};
	Controls.ApproversList__.OnDeleteItem = function (item, idx)
	{
		Lib.AP.WorkflowCtrl.RemoveContributorAt(idx);
		LayoutHelper.UpdateButtonBar();
	};
	Controls.ApproversList__.OnAddItem = function (item, idx)
	{
		item.Remove();

		function addContributorBelow(contributorRole)
		{
			browseApprovers(contributorRole).Then(function (contributor)
			{
				if (contributor)
				{
					Lib.AP.WorkflowCtrl.AddContributorAt(idx, contributor, contributorRole);
					LayoutHelper.UpdateButtonBar();
				}
			});
		}
		var role = Lib.AP.WorkflowCtrl.GetNextRole(idx - 1);
		if (role)
		{
			addContributorBelow(role);
		}
		else
		{
			// Ambiguity: ask the user
			Popup.Menu(
			{
				options: [
				{
					label: "_Add a controller",
					value: Lib.AP.WorkflowCtrl.roles.controller
				},
				{
					label: "_Add an approver",
					value: Lib.AP.WorkflowCtrl.roles.approver
				}]
			}, addContributorBelow);
		}
	};
}
/** *************************** **/
/** Helpers Currency conversion **/
/** *************************** **/
function updateLocalCurrency()
{
	function callbackUpdateLocalCurrency(result, error)
	{
		if (!error && result && result.length > 0)
		{
			Data.SetValue("LocalCurrency__", result[0].Currency__);
		}
		else
		{
			//An error occured (companycode or currency selected may be empty or invalid), reset local currency
			Data.SetValue("LocalCurrency__", Data.GetValue("InvoiceCurrency__"));
		}
	}
	Lib.P2P.ExchangeRate.GetCompanyCodeCurrency(Controls.CompanyCode__.GetValue(), callbackUpdateLocalCurrency);
}

function setExchangeRate(exchangeRate, callback)
{
	Data.SetValue("ExchangeRate__", exchangeRate);
	Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(exchangeRate);
	if (callback)
	{
		callback();
	}
}

function updateExchangeRate(callback)
{
	function callbackUpdateExchangeRate(result, error)
	{
		var exchangeRate;
		if (!error && result && result.length > 0)
		{
			exchangeRate = parseFloat(result[0].Rate__) * parseFloat(result[0].RatioTo__) / parseFloat(result[0].RatioFrom__);
		}
		else
		{
			//An error occured (companycode may be empty or invalid), reset local amounts with an exchange rate = 0
			exchangeRate = 0;
		}
		setExchangeRate(exchangeRate, callback);
	}
	Lib.P2P.ExchangeRate.GetExchangeRate(Data.GetValue("CompanyCode__"), Data.GetValue("InvoiceCurrency__"), callbackUpdateExchangeRate);
}

function convertAmounts(oldCurrency)
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		// Call GetDetails once for each PO to update browse data with new currency
		// Callback get the exchangeRate and handles the conversion of the line items
		var newCurrency = Controls.InvoiceCurrency__.GetValue();
		var orderNumberList = Lib.AP.GetOrderNumbersAsArray();
		if (Lib.ERP.IsSAP() && newCurrency && orderNumberList.length > 0)
		{
			var amountsModified_1 = false;
			orderNumberList.reduce(function (p, orderNumber)
				{
					return p.then(function ()
					{
						return Sys.Helpers.Promise.Create(function (orderNumberResolved)
						{
							var callbackConvertToForeignCurrency = function (exchangeRate)
							{
								if (exchangeRate !== 1)
								{
									var lineItemsTable = Data.GetTable("LineItems__");
									var nbItems = lineItemsTable.GetItemCount();
									for (var i = 0; i < nbItems; ++i)
									{
										var item = lineItemsTable.GetItem(i);
										if (item && item.GetValue("OrderNumber__") === orderNumber)
										{
											// Convert item amounts and compute item tax amount if needed
											if (item.IsComputed("Amount__") && !item.IsNullOrEmpty("Amount__"))
											{
												item.SetValue("Amount__", Lib.AP.ApplyExchangeRate(item.GetValue("Amount__"), exchangeRate));
											}
											item.SetValue("OpenAmount__", Lib.AP.ApplyExchangeRate(item.GetValue("OpenAmount__"), exchangeRate));
											item.SetValue("ExpectedAmount__", Lib.AP.ApplyExchangeRate(item.GetValue("ExpectedAmount__"), exchangeRate));
											item.SetValue("UnitPrice__", Lib.AP.ApplyExchangeRate(item.GetValue("UnitPrice__"), exchangeRate));
											if (Controls.CalculateTax__.IsChecked())
											{
												var taxrate = item.GetValue("TaxRate__");
												var netamount = item.GetValue("Amount__");
												var multitaxrates = item.GetValue("MultiTaxRates__");
												var taxamount = Sys.Helpers.Round(Lib.AP.ApplyTaxRate(netamount, taxrate, multitaxrates), Lib.AP.GetAmountPrecision());
												item.SetValue("TaxAmount__", taxamount);
											}
											else
											{
												item.SetValue("TaxRate__", 0);
											}
											amountsModified_1 = true;
										}
									}
								}
								orderNumberResolved();
							};
							var callbackGetDetails = function (po)
							{
								if (po)
								{
									Lib.AP.SAP.PurchaseOrder.GetExternalCurrencyExchangeRate(newCurrency, oldCurrency, po.PO_HEADER.DOC_DATE)
										.Then(callbackConvertToForeignCurrency);
								}
								else
								{
									orderNumberResolved();
								}
							};
							Lib.AP.SAP.PurchaseOrder.GetDetails(callbackGetDetails, orderNumber, newCurrency, true);
						});
					});
				}, Sys.Helpers.Promise.Resolve())
				.Then(function ()
				{
					// If any item was converted, recompute header amounts that are based on items' amounts
					if (amountsModified_1)
					{
						Controls.NetAmount__.SetValue(Lib.AP.GetInvoiceDocumentLayout().computeHeaderAmount());
						Lib.AP.GetInvoiceDocument().ComputePaymentAmountsAndDates(true, Lib.AP.IsCreditNote());
						Lib.AP.GetInvoiceDocumentLayout().updateLocalAmounts(Data.GetValue("ExchangeRate__"));
						checkPOInvoiceLineItems();
					}
					resolve(amountsModified_1);
				});
		}
		else
		{
			resolve(false);
		}
	});
}

function checkExtractionScriptTimeout()
{
	var displayPopup = Variable.GetValueAsString("AlertExtractionScriptTimeout") && Variable.GetValueAsString("AlertExtractionScriptTimeout") !== "false";

	function removeAlert()
	{
		ProcessInstance.SetSilentChange(true);
		Variable.SetValueAsString("AlertExtractionScriptTimeout", false);
		ProcessInstance.SetSilentChange(false);
	}
	if (displayPopup)
	{
		if (Variable.GetValueAsString("AlertExtractionScriptTimeout") === "NoFTR")
		{
			Popup.Alert("_The first time recognition have not been performed", false, removeAlert, "_Too long extraction");
		}
		else
		{
			Popup.Alert("_The PO lines details have not been filled", false, removeAlert, "_Too long extraction");
		}
	}
}
/**
 * event when Subsequent document changes
 * @this CheckBox
 */
function handleSubsequentDocument()
{
	var isSubsequentDoc = this && this.GetValue ? this.GetValue() : Data.GetValue("SubsequentDocument__");
	Controls.LineItems__.Quantity__.SetReadOnly(isSubsequentDoc);
	if (isSubsequentDoc)
	{
		Controls.LineItems__.Quantity__.SetRequired(false);
		Log.Info("'Subsequent document' ticked: Reset quantity on all lines + focus 'Invoice reference number'");
		if (Controls.Payment)
		{
			Controls.Payment.Collapse(false);
			Controls.Payment.Hide(false);
			Controls.InvoiceReferenceNumber__.Focus();
		}
		var lineItemsTable = Data.GetTable("LineItems__");
		var nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; ++i)
		{
			var item = lineItemsTable.GetItem(i);
			if (item)
			{
				item.SetValue("Quantity__", "");
				item.SetWarning("Quantity__", "");
				item.SetError("Quantity__", "");
				checkPOInvoiceLineItemAmountQuantity(item);
			}
		}
	}
	else
	{
		checkBalance(false);
		checkPOInvoiceLineItems();
	}
	var isPosted = Boolean(Controls.ERPPostingDate__.GetValue());
	Lib.AP.WorkflowCtrl.Rebuild(true, !isPosted, isSubsequentDoc ? "subsequentDocBoxTicked" : "subsequentDocBoxUnticked");
}

function DisplayAlertIfAny()
{
	// display ERP posting error if any
	if (!Sys.Helpers.IsEmpty(Data.GetValue("ERPPostingError__")))
	{
		Data.SetError("ERPInvoiceNumber__", Data.GetValue("ERPPostingError__"));
	}
	// display workflow errors if any
	if (Variable.GetValueAsString("WorkflowError"))
	{
		Log.Error(Variable.GetValueAsString("WorkflowError"));
		if (!ProcessInstance.isReadOnly)
		{
			Popup.Alert(Variable.GetValueAsString("WorkflowError"), true, null, Data.GetActionLabel() || "_Error");
			Variable.SetValueAsString("WorkflowError", "");
		}
	}
	else
	{
		// display ERP posting error in a popup
		ERP.ShowERPPopup();
	}
	// display extraction scripts timeout
	checkExtractionScriptTimeout();
	// Next alerts...
	Lib.CommonDialog.NextAlert.Show(
	{
		"contactVendor":
		{
			Popup: function ()
			{
				setTimeout(function ()
				{
					Controls.ConversationUI__.Focus();
				});
			}
		}
	});
}

function checkInvoiceSignature(docIdx)
{
	Controls.PreviewPanel.ClearBannerMessages("digitallySignedFail");
	Controls.PreviewPanel.ClearBannerMessages("digitallySigned");
	var html = "",
		cssClass = "",
		helpId = 2520;
	// helpID 2520 = Sign & 2519 = Check Signature
	var result = Variable.GetValueAsString("InvoiceSignature");
	if (!docIdx || Attach.IsProcessedDocument(docIdx))
	{
		switch (result)
		{
		case "SignatureDone":
			html = "<span class='fa fa-check fa-lg' /> <span>" + Language.Translate("_Invoice signature ok") + "</span>";
			cssClass = "digitallySigned";
			helpId = 2520;
			break;
		case "SignatureFailed":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice signature ko") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2520;
			break;
		case "InvalidSignatureParameters":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice signature ko due to wrong parameters") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2520;
			break;
		case "AlreadySigned":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice already signed") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2520;
			break;
		case "VerificationError":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice signed not verified") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2519;
			break;
		case "SignatureInvalid":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice signed ko") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2519;
			break;
		case "InvalidParameters":
			html = "<span class='fa fa-times fa-lg' /> <span>" + Language.Translate("_Invoice signed ko due to wrong parameters") + "</span>";
			cssClass = "digitallySignedFail";
			helpId = 2519;
			break;
		case "SignatureOk":
			html = "<span class='fa fa-check fa-lg' /> <span>" + Language.Translate("_Invoice signed ok") + "</span>";
			cssClass = "digitallySigned";
			helpId = 2519;
			break;
		default:
			helpId = 2520;
			break;
		}
		if (html)
		{
			var detail = Variable.GetValueAsString("InvoiceSignatureDetail");
			if (detail)
			{
				html += ": " + detail;
			}
			else
			{
				html += ".";
			}
			Controls.PreviewPanel.AddBannerMessage(html, cssClass, helpId, Language.Translate("_Learn more"));
		}
	}
}

function initializeEventHandlers()
{
	/** Header fields **/
	Controls.ERP__.OnChange = onERPChange;
	Controls.CompanyCode__.OnChange = onCompanyCodeChange;
	Controls.InvoiceType__.OnChange = onInvoiceTypeChange;
	Controls.ERPInvoiceNumber__.OnChange = onERPInvoiceNumberChange;
	Controls.ERPMMInvoiceNumber__.OnChange = onMMERPInvoiceNumberChange;
	Controls.VendorNumber__.OnChange = onVendorChange;
	Controls.VendorName__.OnChange = onVendorChange;
	Controls.VendorNumber__.OnSelectItem = onVendorSelectItem;
	Controls.VendorName__.OnSelectItem = onVendorSelectItem;
	Controls.VendorContactEmail__.OnChange = OnChangeVendorContactEmail;
	Controls.ContactVendor__.OnClick = onContactVendorClick;
	Controls.InvoiceAmount__.OnChange = onInvoiceAmountChange;
	Controls.InvoiceCurrency__.OnChange = onInvoiceCurrencyChange;
	Controls.CurrentException__.OnChange = onCurrentExceptionChange;
	Controls.OrderNumber__.OnChange = onOrderNumberChange;
	Controls.GoodIssue__.OnChange = onConsignmentNumberChange;
	Controls.InvoiceDescription__.OnChange = onInvoiceDescriptionChange;
	Controls.Assignment__.OnChange = onAssignementChange;
	Controls.UnplannedDeliveryCosts__.OnChange = onUnplannedDeliveryCostsChange;
	Controls.ContractNumber__.OnChange = onContractNumberChange;
	Controls.ContractNumber__.OnSelectItem = onContractNumberSelect;
	Controls.ContractNumberDetails__.OnChange = onContractNumberChange;
	Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1ID__", "Code__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"));
	Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1ID__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1__", "Description__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"));
	Controls.CodingTemplate__.OnSelectItem = loadTemplate;
	/** BankDetails **/
	Controls.BankDetails__.OnRefreshRow = BankDetails.onRefreshRow;
	/** Extended Withholding taxes **/
	var withholdingTaxParameter;
	if (Lib.ERP.IsSAP())
	{
		withholdingTaxParameter = Sys.Parameters.GetInstance("AP").GetParameter("TaxesWithholdingTax", "");
	}
	if (withholdingTaxParameter && withholdingTaxParameter === "Extended")
	{
		Controls.ExtendedWithholdingTax__.WHTType__.OnChange = ExtendedWHTAmountChanged;
		Controls.ExtendedWithholdingTax__.WHTCode__.OnChange = ExtendedWHTAmountChanged;
		Controls.ExtendedWithholdingTax__.WHTBaseAmount__.OnChange = ExtendedWHTAmountChanged;
		Controls.ExtendedWithholdingTax__.WHTTaxAmount__.OnChange = ExtendedWHTAmountChanged;
		Controls.ExtendedWithholdingTax__.OnRefreshRow = onExtendedWHTRefreshRow;
	}
	/** Line Items **/
	Controls.LineItems__.HideTableRowDuplicate(false);
	Controls.LineItems__.Amount__.OnChange = lineItemsAmountChanged;
	Controls.LineItems__.Quantity__.OnChange = lineItemsQuantityChanged;
	Controls.LineItems__.TaxCode__.OnChange = calculateTaxAmounts;
	Controls.LineItems__.TaxJurisdiction__.OnChange = calculateTaxJurisdiction;
	Controls.LineItems__.TaxJurisdiction__.OnBrowse = onTaxJurisdictionBrowse;
	Controls.LineItems__.GLAccount__.OnChange = glAccountOnChange;
	Controls.LineItems__.CostCenter__.OnChange = onCostCenterChange;
	Controls.LineItems__.WBSElement__.OnChange = onWBSElementChange;
	Controls.LineItems__.CompanyCode__.OnChange = onCrossItemCompanyCodeChange;
	// Initialize projectcodedescription__
	Controls.LineItems__.ProjectCode__.OnSelectItem = function (item)
	{
		var currentItem = this.GetItem();
		if (item)
		{
			currentItem.SetValue("ProjectCodeDescription__", item.GetValue("Description__"));
		}
	};
	Controls.LineItems__.OrderNumber__.OnClick = onOrderNumberClick;
	Controls.LineItems__.OnDuplicateItem = InvoiceLineItem.OnDuplicateItem;
	Controls.LineItems__.OnAddItem = InvoiceLineItem.OnAddItem;
	Controls.LineItems__.OnDeleteItem = onLineItemDelete;
	Controls.LineItems__.OnRefreshRow = onLineItemRefreshRow;
	Controls.LineItems__.OnFocusRow = onLineItemFocusRow;
	Controls.LineItems__.OnBlurRow = onLineItemBlurRow;
	Controls.CalculateTax__.OnChange = handleTaxComputation;
	Controls.SubsequentDocument__.OnChange = handleSubsequentDocument;
	/** Workflow **/
	Controls.ApproversList__.OnDeleteItem = null;
	Controls.ApproversList__.OnAddItem = null;
	/** Document Panel**/
	// Update toolbar when the reference document is removed
	Controls.DocumentsPanel.OnDocumentDeleted = onDocumentDeleted;
	/** Toolbar **/
	if (Sys.Parameters.GetInstance("AP").GetParameter("WorkflowDisableRules") !== "1")
	{
		Controls.AddApprover.OnClick = onAddApproverClick;
	}
	/** check signature**/
	Controls.DocumentsPanel.OnDocumentSelected = checkInvoiceSignature;
	/** bank details **/
	Controls.BankDetails__.BankDetails_Select__.OnChange = BankDetails.onCheckBankDetails;
	/** SIM */
	Controls.SupplierInformationManagementLink__.OnClick = OpenCompanyDashboard;
}

function hideFields()
{
	LayoutHelper.HideTechnicalFields();
	// Force Hiding these fields
	// These fields may be displayed in previous invoice in start validation mode
	Controls.ComputingWorkflow__.Hide(true);
	Controls.ReconcileWarning__.Hide(true);
	Controls.TemplateWarning__.Hide(true);
	Controls.ManualLinkExplanation__.Hide(true);
	Controls.SourceDocument__.Hide(true);
}

function generateProcessArchiveLink()
{
	var archiveProcessLink = Controls.ArchiveProcessLink__.GetValue();
	if (archiveProcessLink)
	{
		Controls.ArchiveProcessLinkGenerated__.SetURL(archiveProcessLink);
		Controls.ArchiveProcessLinkGenerated__.Hide(false);
	}
}
var IBANStylesEnum = {
	warning: "highlight-warning",
	success: "highlight-success"
};
var IBANWarningMessageKey = "_Extracted IBAN does not match";
var IBANWarningMessage = Language.Translate(IBANWarningMessageKey);

function cleanCheckIBANDisplay()
{
	Controls.ExtractedIBAN__.RemoveStyle(IBANStylesEnum.warning);
	Controls.ExtractedIBAN__.RemoveStyle(IBANStylesEnum.success);
	g_topMessageWarning.Remove(g_topMessageWarning.Find(IBANWarningMessage));
}

function checkIBAN(force)
{
	var bankDetailsDisplay = false;
	// only refresh warning or error
	if (Data.GetValue("ExtractedIBAN__"))
	{
		Controls.ExtractedIBAN__.Hide(false);
		var shouldCheck = Boolean(Data.GetWarning("ExtractedIBAN__")) || Boolean(Data.GetError("ExtractedIBAN__"));
		if ((force || shouldCheck) && currentStepIsApStart())
		{
			var params = {
				companyCode: Data.GetValue("CompanyCode__"),
				vendorNumber: Data.GetValue("VendorNumber__"),
				iban: Data.GetValue("ExtractedIBAN__")
			};
			// check existence but no automatic selection
			Lib.AP.GetInvoiceDocument().GetVendorBankDetails(params, function (vendorsBankAccounts)
			{
				cleanCheckIBANDisplay();
				if (vendorsBankAccounts && vendorsBankAccounts.length > 0)
				{
					Data.SetWarning("ExtractedIBAN__", "");
					Data.SetError("ExtractedIBAN__", "");
					Controls.ExtractedIBAN__.AddStyle(IBANStylesEnum.success);
					return;
				}
				if (!Sys.Helpers.TryCallFunction("Lib.AP.Customization.Common.SetAlertWhenExtractedIBANDoesNotMatch", Controls.ExtractedIBAN__))
				{
					Data.SetWarning("ExtractedIBAN__", IBANWarningMessageKey);
					Controls.ExtractedIBAN__.AddStyle(IBANStylesEnum.warning);
					g_topMessageWarning.Add(IBANWarningMessage);
				}
				BankDetails.getBankDetails(Lib.AP.GetInvoiceDocument());
				bankDetailsDisplay = true;
				if (!force)
				{
					BankDetails.show();
				}
			});
		}
		else if (Data.GetValue("ExtractedIBAN__"))
		{
			Controls.ExtractedIBAN__.AddStyle(IBANStylesEnum.success);
		}
	}
	else if (Controls.ExtractedIBAN__.IsReadOnly())
	{
		Controls.ExtractedIBAN__.Hide(true);
	}
	if (!force && !bankDetailsDisplay)
	{
		BankDetails.hide();
	}
}

function initializeForm()
{
	/** ******************* **/
	/** Form initialization **/
	/** ******************* **/
	hideFields();
	Controls.ERP__.Hide(!Sys.Parameters.GetInstance("P2P").GetParameter("EnableERPSelection"));
	if (ProcessInstance.isReadOnly)
	{
		Controls.Comment__.Hide(true);
		Controls.SpacerComment__.Hide(true);
	}
	else
	{
		Controls.Comment__.SetPlaceholder(g_CommentPlaceHolder);
	}
	if (Controls.PostingDate__.IsRequired())
	{
		Controls.PostingDate__.SetRequired(false);
	}
	Controls.PostingDate__.SetPlaceholder(Sys.Helpers.Date.ToLocaleDateEx(new Date(), User.culture));
	generateProcessArchiveLink();
	Controls.LastValidatorName__.SetValue(User.fullName);
	Controls.LastValidatorUserId__.SetValue(User.loginId);
	handleTaxComputation(true);
	// Initialize Workflow
	if (!ProcessInstance.isReadOnly)
	{
		Lib.AP.WorkflowCtrl.InitRolesSequence();
	}
	Lib.AP.GetInvoiceDocumentLayout().Init();
	LayoutHelper.InitWorkflowDataPanel();
	// User exit for reordering the LineItems__ table
	var newColumnsOrder = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.GetColumnsOrder", Controls.LineItems__.GetColumnsOrder());
	if (newColumnsOrder &&
		Object.prototype.toString.call(newColumnsOrder) === "[object Array]")
	{
		Controls.LineItems__.SetColumnsOrder(newColumnsOrder);
	}
	// initializes the teaching request mechanism
	RequestTeaching.Init();
	InvoiceLineItem.InitializeNonPOLineCount();
	InvoiceLineItem.InitializeGRIVPOLineCount();
	// Set layout according to type (PO or non PO)
	setGRIVMode(true);
	LayoutHelper.AdaptProcessLayoutToInvoiceType(false);
	EmbeddedViews.InitAllEmbeddedViewPanels();
	checkIBAN(false);
	Holds.manageVisibility();
	ParameterDetails.hide();
	// FT-022103 - Contract matching - Display matching invoices on contracts
	LayoutHelper.DisplayContractsElements();
	// Init EventHistory panel (Conversation)
	LayoutHelper.InitEventHistoryPanel();
	// Set layout according to the step in the workflow
	LayoutHelper.AdaptProcessLayoutToStep();
	// Update buttons according to the state in the workflow
	LayoutHelper.UpdateButtonBar();
	// ensure Withholding tax amounts are correct
	RefreshExtendedWHTAmounts();
	//Check invoice signature
	checkInvoiceSignature();
	DuplicateCheck.DoCheck(true);
	var invoiceDoc = Lib.AP.GetInvoiceDocument();
	invoiceDoc.CustomizeButtonsBehavior(ButtonsBehavior);
	ButtonsBehavior.init();
	TaxCodeHelper.init();
	ERP.DisplaySimulationResult();
	if (Data.GetValue("ExchangeRate__") === 0 && !isInApprovalWorkflow())
	{
		checkInvoiceCurrency();
	}
	DisplayAlertIfAny();
	Lib.P2P.DisplayArchiveDurationWarning("AP", function ()
	{
		g_topMessageWarning.Add(Language.Translate("_Archive duration warning"));
	});
	// display invoice reversed warning
	if (Data.GetValue("InvoiceStatus__") === Lib.AP.InvoiceStatus.Reversed)
	{
		g_topMessageWarning.Add(Language.Translate("_Reversed invoice warning"));
	}
	Lib.P2P.DisplayBackupUserWarning("AP", function (displayName)
	{
		g_topMessageWarning.Add(Language.Translate("_View on bealf of {0}", false, displayName));
	});
	// Initialize saved currency with extracted value
	g_invoiceCurrency = Data.GetValue("InvoiceCurrency__");
	if (Variable.GetValueAsString("IsExtendedValidityPeriod") === "true" && Data.GetValue("State") < 100)
	{
		var expirationDate = Sys.Helpers.Date.ToLocaleDateEx(Data.GetValue("ValidityDateTime"), User.culture);
		g_topMessageWarning.Add(Language.Translate("_ExtendedValidityPeriodWarning {0}", false, expirationDate));
	}
	var sourceDocument = Data.GetValue("SourceDocument__");
	if (sourceDocument && Lib.AP.WorkflowCtrl.GetCurrentStepRole() === "_Role APStart")
	{
		Controls.SourceDocument__.Hide(false);
		var mapper = Lib.AP.MappingManager.GetMapper(Data.GetValue("ReceptionMethod__"));
		if (mapper && mapper.OnSourceDocucmentClick)
		{
			Controls.SourceDocument__.DisplayAs(
			{
				type: "Link"
			});
			Controls.SourceDocument__.OnClick = mapper.OnSourceDocucmentClick;
			if (mapper.SourceDocucmentLabel)
			{
				Controls.SourceDocument__.SetLabel(Language.Translate(mapper.SourceDocucmentLabel));
			}
		}
	}
	else
	{
		Controls.SourceDocument__.Hide(true);
	}
	LayoutHelper.SetHelperIDBasedOnCurrentStepRole();
	handleContractValidity();
}

function needToWaitConfigQuery()
{
	if (g_apParameters.IsReady() && g_p2pParameters.IsReady() && Variable.GetValueAsString("tableParameters") && Lib.ERP.GetERPName("AP"))
	{
		return false;
	}
	return true;
}

function synchronizeConfigQuery()
{
	g_apParameters.IsReady(function ()
	{
		if (!g_p2pParameters.IsReady())
		{
			g_p2pParameters.IsReady(run);
		}
		else
		{
			run();
		}
	});
}

function run()
{
	ProcessInstance.SetSilentChange(true);
	Log.Info("Start HTML page script execution...");
	if (!Data.GetValue("Configuration__"))
	{
		Data.SetValue("Configuration__", Variable.GetValueAsString("Configuration"));
	}
	// Init workfowUI
	Lib.AP.WorkflowCtrl.Init(Controls, User, ProcessInstance, LayoutHelper, Lib.AP.GetInvoiceDocument());
	// Init ERP name
	Lib.ERP.InitERPName(null, false, "AP");
	initializeEventHandlers();
	initializeForm();
	Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.OnHTMLScriptEnd");
	Controls.ERP__.Wait(false);
	Log.Info("HTML page script execution complete");
	ProcessInstance.SetSilentChange(false);
}
if (needToWaitConfigQuery())
{
	// The user most likely clicked on "Enter an invoice"
	// We need to wait for the AP configuration query response (the ERP (at least) is required before initializing the form)
	Log.Info("Loading configuration...");
	hideFields();
	synchronizeConfigQuery();
}
else
{
	// Everything required to initialize the form is already set.
	// Do not wait configuration query response.
	run();
}