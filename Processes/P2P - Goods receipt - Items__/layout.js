{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"GRNumber__": "LabelGRNumber__",
																	"LabelGRNumber__": "GRNumber__",
																	"DeliveryCompleted__": "LabelDeliveryCompleted__",
																	"LabelDeliveryCompleted__": "DeliveryCompleted__",
																	"Quantity__": "LabelQuantity__",
																	"LabelQuantity__": "Quantity__",
																	"RequisitionNumber__": "LabelRequisitionNumber__",
																	"LabelRequisitionNumber__": "RequisitionNumber__",
																	"DeliveryDate__": "LabelDeliveryDate__",
																	"LabelDeliveryDate__": "DeliveryDate__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"DeliveryNote__": "LabelDeliveryNote__",
																	"LabelDeliveryNote__": "DeliveryNote__",
																	"InvoicedAmount__": "LabelInvoicedAmount__",
																	"LabelInvoicedAmount__": "InvoicedAmount__",
																	"Amount__": "LabelAmount__",
																	"LabelAmount__": "Amount__",
																	"InvoicedQuantity__": "LabelInvoicedQuantity__",
																	"LabelInvoicedQuantity__": "InvoicedQuantity__",
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__",
																	"OrderNumber__": "LabelOrderNumber__",
																	"LabelOrderNumber__": "OrderNumber__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"Status__": "LabelStatus__",
																	"LabelStatus__": "Status__",
																	"ItemUnit__": "LabelItemUnit__",
																	"LabelItemUnit__": "ItemUnit__",
																	"ItemUnitDescription__": "LabelItemUnitDescription__",
																	"LabelItemUnitDescription__": "ItemUnitDescription__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__",
																	"ProjectCode__": "LabelProjectCode__",
																	"LabelProjectCode__": "ProjectCode__",
																	"ProjectCodeDescription__": "LabelProjectCodeDescription__",
																	"LabelProjectCodeDescription__": "ProjectCodeDescription__",
																	"FreeDimension1__": "LabelFreeDimension1__",
																	"LabelFreeDimension1__": "FreeDimension1__",
																	"FreeDimension1ID__": "LabelFreeDimension1ID__",
																	"LabelFreeDimension1ID__": "FreeDimension1ID__",
																	"IsReplenishmentItem__": "LabelIsReplenishmentItem__",
																	"LabelIsReplenishmentItem__": "IsReplenishmentItem__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 25,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"GRNumber__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelGRNumber__": {
																				"line": 7,
																				"column": 1
																			},
																			"DeliveryCompleted__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelDeliveryCompleted__": {
																				"line": 18,
																				"column": 1
																			},
																			"Quantity__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelQuantity__": {
																				"line": 13,
																				"column": 1
																			},
																			"RequisitionNumber__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelRequisitionNumber__": {
																				"line": 8,
																				"column": 1
																			},
																			"DeliveryDate__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelDeliveryDate__": {
																				"line": 11,
																				"column": 1
																			},
																			"Ligne_d_espacement__": {
																				"line": 3,
																				"column": 1
																			},
																			"Ligne_d_espacement2__": {
																				"line": 1,
																				"column": 1
																			},
																			"Explanations__": {
																				"line": 2,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"DeliveryNote__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelDeliveryNote__": {
																				"line": 10,
																				"column": 1
																			},
																			"InvoicedAmount__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelInvoicedAmount__": {
																				"line": 16,
																				"column": 1
																			},
																			"Amount__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelAmount__": {
																				"line": 12,
																				"column": 1
																			},
																			"InvoicedQuantity__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelInvoicedQuantity__": {
																				"line": 17,
																				"column": 1
																			},
																			"LineNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 6,
																				"column": 1
																			},
																			"OrderNumber__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelOrderNumber__": {
																				"line": 5,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 20,
																				"column": 1
																			},
																			"Status__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelStatus__": {
																				"line": 9,
																				"column": 1
																			},
																			"ItemUnit__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelItemUnit__": {
																				"line": 14,
																				"column": 1
																			},
																			"ItemUnitDescription__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelItemUnitDescription__": {
																				"line": 15,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 19,
																				"column": 1
																			},
																			"ProjectCode__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelProjectCode__": {
																				"line": 21,
																				"column": 1
																			},
																			"ProjectCodeDescription__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelProjectCodeDescription__": {
																				"line": 22,
																				"column": 1
																			},
																			"FreeDimension1__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelFreeDimension1__": {
																				"line": 23,
																				"column": 1
																			},
																			"FreeDimension1ID__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelFreeDimension1ID__": {
																				"line": 24,
																				"column": 1
																			},
																			"IsReplenishmentItem__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelIsReplenishmentItem__": {
																				"line": 25,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"Ligne_d_espacement2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Ligne d'espacement2",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"Explanations__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "L",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Explanations",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 38
																		},
																		"Ligne_d_espacement__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Ligne d'espacement",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 40
																		},
																		"LabelOrderNumber__": {
																			"type": "Label",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"label": "_OrderNumber",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"OrderNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_OrderNumber",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 62
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"LineNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_LineNumber",
																				"activable": true,
																				"width": 230,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 60
																		},
																		"LabelGRNumber__": {
																			"type": "Label",
																			"data": [
																				"GRNumber__"
																			],
																			"options": {
																				"label": "_GRNumber",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"GRNumber__": {
																			"type": "ShortText",
																			"data": [
																				"GRNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_GRNumber",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 23
																		},
																		"LabelRequisitionNumber__": {
																			"type": "Label",
																			"data": [
																				"RequisitionNumber__"
																			],
																			"options": {
																				"label": "_RequisitionNumber",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"RequisitionNumber__": {
																			"type": "ShortText",
																			"data": [
																				"RequisitionNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RequisitionNumber",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelStatus__": {
																			"type": "Label",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"label": "_Status",
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"Status__": {
																			"type": "ComboBox",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Goods to receive",
																					"1": "_Goods received",
																					"2": "_Goods canceled"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "To receive",
																					"1": "Received",
																					"2": "Canceled"
																				},
																				"label": "_Status",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 66
																		},
																		"LabelDeliveryNote__": {
																			"type": "Label",
																			"data": [
																				"DeliveryNote__"
																			],
																			"options": {
																				"label": "_DeliveryNote",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"DeliveryNote__": {
																			"type": "ShortText",
																			"data": [
																				"DeliveryNote__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_DeliveryNote",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 42
																		},
																		"LabelDeliveryDate__": {
																			"type": "Label",
																			"data": [
																				"DeliveryDate__"
																			],
																			"options": {
																				"label": "_DeliveryDate",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"DeliveryDate__": {
																			"type": "DateTime",
																			"data": [
																				"DeliveryDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_DeliveryDate",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"LabelAmount__": {
																			"type": "Label",
																			"data": [
																				"Amount__"
																			],
																			"options": {
																				"label": "_Amount",
																				"version": 0
																			},
																			"stamp": 53
																		},
																		"Amount__": {
																			"type": "Decimal",
																			"data": [
																				"Amount__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Amount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 54
																		},
																		"LabelQuantity__": {
																			"type": "Label",
																			"data": [
																				"Quantity__"
																			],
																			"options": {
																				"label": "_Quantity",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"Quantity__": {
																			"type": "Decimal",
																			"data": [
																				"Quantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Quantity",
																				"precision_internal": 3,
																				"precision_current": 3,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 27
																		},
																		"LabelItemUnit__": {
																			"type": "Label",
																			"data": [
																				"S_lection_dans_une_table__"
																			],
																			"options": {
																				"label": "_ItemUnit",
																				"version": 0
																			},
																			"stamp": 67
																		},
																		"ItemUnit__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnit__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnit",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"length": 3,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 68
																		},
																		"LabelItemUnitDescription__": {
																			"type": "Label",
																			"data": [
																				"S_lection_dans_une_table__"
																			],
																			"options": {
																				"label": "_ItemUnitDescription",
																				"version": 0
																			},
																			"stamp": 69
																		},
																		"ItemUnitDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnitDescription__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnitDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 70
																		},
																		"LabelInvoicedAmount__": {
																			"type": "Label",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"label": "_Invoiced amount",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"InvoicedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced amount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 48
																		},
																		"LabelInvoicedQuantity__": {
																			"type": "Label",
																			"data": [
																				"InvoicedQuantity__"
																			],
																			"options": {
																				"label": "_Invoiced quantity",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"InvoicedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedQuantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced quantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 52
																		},
																		"LabelDeliveryCompleted__": {
																			"type": "Label",
																			"data": [
																				"DeliveryCompleted__"
																			],
																			"options": {
																				"label": "_DeliveryCompleted",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"DeliveryCompleted__": {
																			"type": "CheckBox",
																			"data": [
																				"DeliveryCompleted__"
																			],
																			"options": {
																				"label": "_DeliveryCompleted",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 71
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 72
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "_BudgetID",
																				"version": 0
																			},
																			"stamp": 63
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BudgetID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 64
																		},
																		"LabelProjectCode__": {
																			"type": "Label",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"label": "_ProjectCode",
																				"version": 0
																			},
																			"stamp": 73
																		},
																		"ProjectCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"RestrictSearch": true
																			},
																			"stamp": 74
																		},
																		"LabelProjectCodeDescription__": {
																			"type": "Label",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"label": "_ProjectCodeDescription",
																				"version": 0
																			},
																			"stamp": 75
																		},
																		"ProjectCodeDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCodeDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 76
																		},
																		"LabelFreeDimension1__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"label": "_FreeDimension1",
																				"version": 0
																			},
																			"stamp": 77
																		},
																		"FreeDimension1__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_FreeDimension1",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 500
																			},
																			"stamp": 78
																		},
																		"LabelFreeDimension1ID__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"label": "_FreeDimension1ID",
																				"version": 0
																			},
																			"stamp": 79
																		},
																		"FreeDimension1ID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_FreeDimension1ID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"length": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 80
																		},
																		"LabelIsReplenishmentItem__": {
																			"type": "Label",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem",
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 81
																		},
																		"IsReplenishmentItem__": {
																			"type": "CheckBox",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"possibleValues": {},
																				"possibleKeys": {},
																				"hidden": true,
																				"readonly": true,
																				"version": 0
																			},
																			"stamp": 82
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 82,
	"data": []
}