///#GLOBALS Lib
var options = {
	maxElementsPerRecall: 50,
	limitHistoryToLastDays: 30
};
Process.SetTimeOut(3600);
var NOT_FINISHED = 0;
if (Lib.AP.UpdateWaitingGR.UpdateInvoicesWaitingForGR(options) === NOT_FINISHED)
{
	Process.RecallScript("update_gr_in_progress", true);
}