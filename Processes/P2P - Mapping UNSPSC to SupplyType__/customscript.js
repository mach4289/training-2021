///#GLOBALS Lib Sys

Controls.SupplyType__.SetAttributes(['SupplyID__']);
Controls.SupplyType__.OnSelectItem = function (item)
{
    Controls.SupplyTypeId__.SetValue(item.GetValue("SupplyID__"));
};