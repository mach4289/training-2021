{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "vertical",
				"splitterOptions": {
					"sizeable": false,
					"sidebarWidth": 320,
					"unit": "px",
					"fixedSlider": true
				},
				"version": 0,
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-left": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"hidden": false,
										"box": {
											"top": "0px",
											"left": "0px",
											"width": "320px"
										}
									},
									"*": {
										"form-content-left-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"CustomerDetailsPane": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "CustomerDetails",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight"
													},
													"stamp": 3,
													"data": [],
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 4,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Customer_Info__": {
																				"line": 1,
																				"column": 1
																			},
																			"ActionMenu__": {
																				"line": 2,
																				"column": 1
																			},
																			"NavMenu__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 5,
																	"*": {
																		"NavMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<input id=\"onLoad\" type=\"hidden\" onclick=\"OnLoad(event);\">\n<input id=\"onPackageLoad\" type=\"hidden\" onclick=\"OnPackageLoad(event);\">\n<input id=\"onChangeTab\" type=\"hidden\" onclick=\"OnChangeTab(event);\">\n<div id=\"NavMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-sidebar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"overview\" onclick=\"OnClickTab(this.id);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-dashboard\"></span>\n\t\t\t\t<span id=\"overviewLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"creditScore\" style=\"display:none\" data-package=\"AR\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-credit\"></span>\n\t\t\t\t<span id=\"creditScoreLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"orders\" style=\"display:none\" data-package=\"COP/AR\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-order\"></span>\n\t\t\t\t<span id=\"ordersLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"invoices\" style=\"display:none\" data-package=\"AR\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link \">\n\t\t\t\t<span class=\"icon esk-ifont-invoice\"></span>\n\t\t\t\t<span id=\"invoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"remittances\" style=\"display:none\" data-package=\"AR\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-remittance\"></span>\n\t\t\t\t<span id=\"remittancesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"payments\" style=\"display:none\" data-package=\"AR\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-payment\"></span>\n\t\t\t\t<span id=\"paymentsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"deductions\" style=\"display:none\" data-package=\"DEDUCTIONS\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-remittance\"></span>\n\t\t\t\t<span id=\"deductionsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"relatedDoc\" style=\"display:none\" data-package=\"DD\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon esk-ifont-related_documents\"></span>\n\t\t\t\t<span id=\"relatedDocLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"contacts\" style=\"display:none\" data-package=\"AR/CM\" onclick=\"OnClickTab(this.id);\" href=\"#\"\n\t\t\t   class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-users fa-lg\"></span>\n\t\t\t\t<span id=\"contactsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a href=\"#\" id=\"activity\" onclick=\"OnClickTab(this.id);\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-line-chart fa-lg\"></span>\n\t\t\t\t<span id=\"activityLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a href=\"#\" id=\"settings\" onclick=\"OnClickTab(this.id);\" class=\"nav-link\">\n\t\t\t\t<span class=\"icon fa fa-gear fa-lg\"></span>\n\t\t\t\t<span id=\"settingsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item\" hidden>\n\t\t\t<a href=\"#\" id=\"moreDetails\" onclick=\"OnClickTab(this.id);\" class=\"nav-link\">\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"NavMenu__\" }, document.location.origin);\n\tvar tabItems = document.getElementById(\"NavMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickTab(tabID)\n\t{\n\t\tfor (var i = 0; i < tabItems.length; i++)\n\t\t\ttabItems[i].className = \"nav-link\";\n\n\t\tvar tab = document.getElementById(tabID);\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"NavMenu__\", args: tab.id }, document.location.origin);\n\t}\n\n\tfunction OnChangeTab(evt)\n\t{\n\t\tOnClickTab(evt.tabId);\n\t}\n\tfunction OnLoad(evt)\n\t{\n\t\tfor (var lbl in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(lbl + \"Lbl\");\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[lbl];\n\t\t}\n\t\tfor (var i = 0; i < tabItems.length; i++)\n\t\t{\n\t\t\tvar packages = tabItems[i].getAttribute(\"data-package\");\n\t\t\tif (packages)\n\t\t\t{\n\t\t\t\tpackages = packages.split('/');\n\t\t\t\tfor (var j = 0; j < packages.length; j++)\n\t\t\t\t{\n\t\t\t\t\tif (evt.appInstances[packages[j]])\n\t\t\t\t\t{\n\t\t\t\t\t\tif (typeof evt.IsDisplay == \"function\" && evt.IsDisplay(tabItems[i].id))\n\t\t\t\t\t\t\ttabItems[i].style.display = \"\";\n\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\t\tvar startMenu = document.getElementById(evt.startMenu);\n\t\tstartMenu.className = \"nav-link active\";\n\t}\n</script>",
																				"width": "100%",
																				"version": 0,
																				"css": ".FlexibleFormPanelLight .nav-body .nav-sidebar .nav-link span[class^=\"icon esk\"] {\n\tfont-size: 24px;\n\tfloat: none;\n\tline-height: normal;\n\theight: 24px;\n}\n\n.FlexibleFormPanelLight .nav-body .nav-sidebar .nav-link {\n\talign-items: center !important;\n}\n"
																			},
																			"stamp": 6
																		},
																		"Customer_Info__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML content",
																				"htmlContent": "<input id=\"onCustomerInfoLoad\" type=\"hidden\" onclick=\"onCustomerInfoLoad(event);\">\n<div>\n\t<div id=\"logoCustomer\">\n\t\t<i class=\"fa fa-user-circle fa-4x\" aria-hidden=\"true\"></i>\n\t</div>\n\t<div id=\"infoCustomer\">\n\t\t<div class=\"customerName\"></div>\n\t\t<div class=\"customerNumber\"></div>\n\t</div>\n\t<div id=\"customerDetail\">\n\t\t<div class=\"nameContact\"></div>\n\t\t<div class=\"information email\"></div>\n\t\t<div class=\"information phoneNumber\" style=\"display:none\"><i class=\"fa fa-phone fa-fw\" style=\"margin-right: 5px\" aria-hidden=\"true\"></i><a id=\"phoneNumber\"></a></div>\n\t\t<div class=\"information deliveryMethod\" style=\"display:none\"><i class=\"fa fa-truck fa-fw\" style=\"margin-right: 5px\" aria-hidden=\"true\"></i><a id=\"deliveryMethod\"></a></div>\n\t</div>\n</div>\n\n\n<script>\n\twindow.postMessage({ eventName: \"onCustomerInfoLoad\", control: \"Customer_Info__\" }, document.location.origin);\n\tfunction onCustomerInfoLoad(evt)\n\t{\n\n\t\tvar div = document.getElementsByClassName(\"customerName\");\n\t\tdiv[0].innerText = evt.customerInfo.name !== \"\" ? evt.customerInfo.name : \"\\u00A0\";\n\n\t\tdiv = document.getElementsByClassName(\"customerNumber\");\n\t\tdiv[0].innerText = evt.customerInfo.customerNumber !== \"\" ? evt.customerInfo.customerNumber : \"\\u00A0\";\n\n\t\tif (evt.customerInfo.subName !== \"\")\n\t\t{\n\t\t\tdiv = document.getElementsByClassName(\"nameContact\");\n\t\t\tdiv[0].innerText = evt.customerInfo.subName;\n\t\t}\n\t\tif (evt.customerInfo.email !== \"\")\n\t\t{\n\t\t\tdiv = document.getElementsByClassName(\"email\");\n\t\t\tdiv[0].innerText = evt.customerInfo.email;\n\t\t}\n\n\t\tif (evt.customerInfo.phoneNumber !== \"\")\n\t\t{\n\t\t\tvar elem = document.getElementById(\"phoneNumber\");\n\t\t\telem.text = evt.customerInfo.phoneNumber;\n\t\t\tdiv = document.getElementsByClassName(\"phoneNumber\");\n\t\t\tdiv[0].style.display = \"block\";\n\t\t}\n\n\t\tif (evt.customerInfo.deliveryMethod !== \"\")\n\t\t{\n\t\t\tvar elem = document.getElementById(\"deliveryMethod\");\n\t\t\telem.text = evt.customerInfo.deliveryMethod;\n\t\t\tdiv = document.getElementsByClassName(\"deliveryMethod\");\n\t\t\tdiv[0].style.display = \"block\";\n\t\t}\n\n\n\t}\n</script>\n",
																				"width": "100%",
																				"version": 0,
																				"css": "#logoCustomer {\n\ttext-align: left;\n\twidth: 20%;\n\tfloat: left;\n\tcolor: #1a2732;\n}\n\n#infoCustomer {\n\ttext-align: left;\n\twidth: 80%;\n\tmargin-bottom: 15px;\n}\n\n\t#infoCustomer .customerName {\n\t\tfont-size: 20px;\n\t\tline-height: 1.4;\n\t\tfont-weight: bold;\n\t\tcolor: #1a2732;\n\t}\n\n\t#infoCustomer .customerNumber {\n\t\tfont-size: 16px;\n\t\tline-height: 1.6;\n\t\tcolor: #1a2732;\n\t}\n\n#customerDetail {\n\tmargin-left: -10px;\n\tmargin-right: -10px;\n\tbackground-color: #f2f2f2;\n\tpadding-bottom: 20px;\n}\n\n.information {\n\tmargin-left: 10px;\n\tmargin-bottom: 3px;\n}\n\n.nameContact {\n\tmargin-left: 10px;\n\tcolor: #1a2732;\n\tfont-size: 18px;\n\tmargin-bottom: 10px;\n\tpadding-top: 10px;\n}\n"
																			},
																			"stamp": 7
																		},
																		"ActionMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "ActionMenu__",
																				"width": "100%",
																				"version": 0,
																				"htmlContent": "<input id=\"onActionLoad\" type=\"hidden\" onclick=\"onActionLoad(event);\">\n<div class=\"backgroundButton\">\n\t<div class=\"nav-body\">\n\t\t<ul class=\"nav-topbar\">\n\t\t\t<li class=\"nav-item action-link\">\n\t\t\t\t<a id=\"MoreDetails\" onclick=\"OnClickTab('moreDetails')\">_More_Details</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item-divider action-divider\"></li>\n\t\t\t<li class=\"nav-item action-link\">\n\t\t\t\t<a id=\"Edit\" onclick=\"EditAction()\">_Edit</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item-divider action-divider\"></li>\n\t\t\t<li class=\"nav-item action-link\">\n\t\t\t\t<a id=\"ResendWelcomeEmail\" onclick=\"ResendWelcomeEmailAction()\">_Resend welcome email</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item-divider action-divider\"></li>\n\t\t\t<li class=\"nav-item action-link\">\n\t\t\t\t<a id=\"ResetPassword\" onclick=\"ResetPasswordAction()\">_Reset password</a>\n\t\t\t</li>\n\t\t</ul>\n\t</div>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onActionLoad\", control: \"ActionMenu__\" }, document.location.origin);\n\tfunction onActionLoad(evt)\n\t{\n\t\tfor (var element in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(element);\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[element];\n\t\t}\n\t}\n\n\tfunction EditAction()\n\t{\n\t\twindow.postMessage({ eventName: \"EditAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\n\tfunction ResendWelcomeEmailAction()\n\t{\n\t\twindow.postMessage({ eventName: \"WelcomeEmailAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\n\tfunction ResetPasswordAction()\n\t{\n\t\twindow.postMessage({ eventName: \"ResetPasswordAction\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n\tfunction MoreDetails()\n\t{\n\t\twindow.postMessage({ eventName: \"MoreDetails\", control: \"ActionMenu__\" }, document.location.origin);\n\t}\n</script>",
																				"css": ".action-link {\n\tcolor: #0098aa;\n\ttext-decoration: underline;\n}\n\n.action-divider {\n\tbackground-color: #0098aa !important;\n\tmargin-right: 10px !important;\n\tmargin-left: 10px !important;\n}\n\n.backgroundButton {\n\tmargin-left: -10px;\n\tmargin-right: -10px;\n\tbackground-color: #f2f2f2;\n\tmargin-top: -22px;\n\tmargin-bottom: -10px;\n\tpadding-left: 10px;\n\tpadding-bottom: 3px;\n}\n"
																			},
																			"stamp": 8
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 9,
									"data": []
								},
								"form-content-right": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"hidden": false,
										"box": {
											"top": "0px",
											"left": "320px",
											"width": "calc(100 % - 320px)"
										}
									},
									"*": {
										"form-content-right-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 10,
											"*": {
												"CompanyPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "none",
														"labelsAlignment": "right",
														"label": "Company",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"hidden": true,
														"version": 0
													},
													"stamp": 11,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 12,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CompanyName__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line12__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 13,
																	"*": {
																		"Spacer_line12__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line12",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"CompanyName__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "L",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"label": "_CompanyName",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 15
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 16,
											"*": {
												"AlertPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "none",
														"labelsAlignment": "right",
														"label": "_AlertPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 17,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"style": "bd-callout-FieldManager",
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 18,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"AlertsContainer__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 19,
																	"*": {
																		"AlertsContainer__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_AlertContainer",
																				"version": 0,
																				"width": "100%",
																				"htmlContent": "<input id=\"onDisplayAlert\" type=\"hidden\" onclick=\"OnAddAlert(event);\">\n<input id=\"onAlertSetDismissBtnVisibility\" type=\"hidden\" onclick=\"OnAlertSetDismissBtnVisibility(event);\">\n<input id=\"onAlertSetdetailsBtnVisibility\" type=\"hidden\" onclick=\"OnAlertSetdetailsBtnVisibility(event);\">\n<input id=\"onHideAlert\" type=\"hidden\" onclick=\"OnHideAlert(event);\">\n<div id=\"alertsContainer\"></div>\n\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"AlertsContainer__\" }, document.location.origin);\n\n\tvar alerts = {};\n\tvar Alert = function (opt, containerElem)\n\t{\n\t\tfunction $getBtn(def, className, idName, callbackName, callBackFuncName)\n\t\t{\n\t\t\tvar btnElem = document.createElement(\"button\");\n\t\t\tbtnElem.innerText = def.title;\n\t\t\tbtnElem.className = className;\n\t\t\tbtnElem.id = idName;\n\n\t\t\tif (typeof def.onClick === \"function\")\n\t\t\t{\n\t\t\t\tthis[callbackName] = def.onClick;\n\t\t\t\tbtnElem.onclick = function () { Alert[callBackFuncName](opt.id); };\n\t\t\t}\n\n\t\t\treturn btnElem;\n\t\t}\n\n\t\tthis.rootElem = document.createElement(\"div\");\n\t\tthis.rootElem.className = \"bd-callout bd-callout-warning\";\n\t\tthis.rootElem.id = opt.id;\n\n\t\tvar msgElem = document.createElement(\"h4\");\n\t\tmsgElem.innerText = opt.message;\n\t\tthis.rootElem.appendChild(msgElem);\n\n\t\tif (opt.dismissBtn)\n\t\t{\n\t\t\tthis.dismissBtn = $getBtn.call(this, opt.dismissBtn, \"button-callout button-callout-danger\", \"DismissAlertButton\", \"dismissCallback\", \"OnDismissAlertClick\");\n\t\t\tthis.rootElem.appendChild(this.dismissBtn);\n\t\t}\n\n\t\tif (opt.detailsBtn)\n\t\t{\n\t\t\tthis.detailsBtn = $getBtn.call(this, opt.detailsBtn, \"button-callout button-callout-warning creditScore\", \"CreditScoreButton\", \"detailCallback\", \"OnDetailAlertClick\");\n\t\t\tthis.rootElem.appendChild(this.detailsBtn);\n\t\t}\n\n\t\tcontainerElem.appendChild(this.rootElem);\n\n\t};\n\n\tAlert.OnDismissAlertClick = function (alertId) { alerts[alertId].dismissCallback(); }\n\tAlert.OnDetailAlertClick = function (alertId) { alerts[alertId].detailCallback(); }\n\n\tfunction OnAddAlert(evt)\n\t{\n\t\tif (evt.id && !alerts[evt.id])\n\t\t{\n\t\t\tconst containerElem = document.getElementById(\"alertsContainer\");\n\t\t\talerts[evt.id] = new Alert(evt, containerElem);\n\t\t}\n\t}\n\n\tfunction OnAlertSetDismissBtnVisibility(evt)\n\t{\n\t\tif (alerts[evt.id] && alerts[evt.id].dismissBtn)\n\t\t\talerts[evt.id].dismissBtn.style.display = evt.show ? \"\" : \"none\";\n\t}\n\n\tfunction OnAlertSetdetailsBtnVisibility(evt)\n\t{\n\t\tif (alerts[evt.id] && alerts[evt.id].detailsBtn)\n\t\t\talerts[evt.id].detailsBtn.style.display = evt.show ? \"\" : \"none\";\n\t}\n\n\tfunction OnHideAlert(evt)\n\t{\n\t\tif (alerts[evt.id])\n\t\t\talerts[evt.id].rootElem.style.display = \"none\";\n\t}\n</script>",
																				"css": ".bd-callout-FieldManager {\n\tpadding: 0;\n}\n\n\t.bd-callout-FieldManager .secondColBody {\n\t\tpadding: 0 !important;\n\t}\n\n.bd-callout {\n\tborder-left: 4px solid;\n\tborder-radius: 4px;\n}\n\n    .bd-callout:not(:first-child) {\n        margin-top: 10px;\n    }\n\n\t.bd-callout::after {\n\t\tcontent: \"\";\n\t\tdisplay: block;\n\t\tclear: both;\n\t}\n\n\t.bd-callout .button-callout {\n\t\tborder: 1px;\n\t\tborder-style: solid;\n\t\tbox-sizing: border-box;\n\t\tborder-radius: 4px;\n\t\tpadding: 4px;\n\t\tmargin: 6px;\n\t\tfloat: right;\n\t\tcursor: pointer;\n\t\tfont: normal 16px 'Roboto Bold';\n\t}\n\n\t.bd-callout .button-callout-danger {\n\t\tbackground-color: #E42518;\n\t\tborder-color: #E42518;\n\t\tcolor: #fce9e7;\n\t}\n\n\t.bd-callout .button-callout-warning {\n\t\tbackground-color: #ff8700;\n\t\tborder-color: #ff8700;\n\t\tcolor: #fff3e5;\n\t}\n\n\t.bd-callout .button-callout-info {\n\t\tbackground-color: #0082c0;\n\t\tborder-color: #0082c0;\n\t\tcolor: #e5f2f9;\n\t}\n\n\t.bd-callout h4 {\n\t\tfloat: left;\n\t\tline-height: 15px;\n\t\tpadding: 4px;\n\t\tmargin: 6px;\n\t\tfont: normal 16px 'Roboto Bold';\n\t}\n\n\t.bd-callout.bd-callout-info {\n\t\tborder-left-color: #0082c0;\n\t\tbackground-color: #e5f2f9;\n\t}\n\n\t\t.bd-callout.bd-callout-info h4 {\n\t\t\tcolor: #0082c0;\n\t\t}\n\n\t.bd-callout.bd-callout-warning {\n\t\tborder-left-color: #ff8700;\n\t\tbackground-color: #fff3e5;\n\t}\n\n\t\t.bd-callout.bd-callout-warning h4 {\n\t\t\tcolor: #ff8700;\n\t\t}\n\n\t.bd-callout.bd-callout-danger {\n\t\tborder-left-color: #E42518;\n\t\tbackground-color: #fce9e7;\n\t}\n\n\t\t.bd-callout.bd-callout-danger h4 {\n\t\t\tcolor: #E42518;\n\t\t}\n",
																				"hidden": true
																			},
																			"stamp": 20
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-14": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"additionalCssStyle": "flexParent",
												"version": 0
											},
											"stamp": 21,
											"*": {
												"RiskPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "RiskPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"version": 0
													},
													"stamp": 22,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 23,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TotalExposure__": {
																				"line": 1,
																				"column": 1
																			},
																			"CreditLimitCounter__": {
																				"line": 2,
																				"column": 1
																			},
																			"CreditLimitUtilization__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 24,
																	"*": {
																		"CreditLimitCounter__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "",
																				"width": 230,
																				"version": 0,
																				"counterLabel": "_CreditLimit",
																				"counterType": "Manual",
																				"uiCounterType": "Manual",
																				"displayFormat": "{0}",
																				"viewName": null,
																				"tabName": null,
																				"viewData": null
																			},
																			"stamp": 25
																		},
																		"TotalExposure__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"hidden": true,
																				"label": "",
																				"width": 230,
																				"version": 0,
																				"counterLabel": "_TotalExposure",
																				"counterType": "Manual",
																				"uiCounterType": "Manual",
																				"displayFormat": "{0}",
																				"viewName": null,
																				"tabName": null,
																				"viewData": null
																			},
																			"stamp": 26
																		},
																		"CreditLimitUtilization__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_CreditLimitUtilization",
																				"version": 0,
																				"htmlContent": "<div class=\"progressBarComponent\">\n\t<input id=\"onCreditLimitUtilizationInit\" type=\"hidden\" onclick=\"onCreditLimitUtilizationInit(event)\" />\n\t<script>\n\t\tfunction onCreditLimitUtilizationInit(evt)\n\t\t{\n\t\t\tvar formatter = new Intl.NumberFormat(evt.culture, {\n\t\t\t\tstyle: 'currency',\n\t\t\t\tcurrency: evt.currency,\n\t\t\t\tminimumFractionDigits: 0,\n\t\t\t\tmaximumFractionDigits: 0\n\t\t\t});\n\n\t\t\tif (evt.progressValue >= evt.totalValue)\n\t\t\t{\n\t\t\t\tdocument.getElementById('progressBarCircle2').setAttribute('stroke', '#f12938');\n\t\t\t\tdocument.getElementById('progressBarText').classList.add(\"progressBarTextRed\");\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\tdocument.getElementById('progressBarCircle2').setAttribute('stroke', '#0098aa');\n\t\t\t\tdocument.getElementById('progressBarText').classList.add(\"progressBarTextBlue\");\n\t\t\t}\n\n\t\t\tdocument.getElementById('progressBarText').innerText = Math.round(evt.progressValue / evt.totalValue * 100) + ' %';\n\n\t\t\tvar circumference = 48 * 2 * Math.PI;\n\t\t\tvar strokeLength = circumference / 100 * Math.round(evt.progressValue / evt.totalValue * 100);\n\n\t\t\tdocument.getElementById('progressBarLoader').style.display = \"none\";\n\t\t\tdocument.getElementById('progressBarObject').style.display = \"flex\";\n\t\t\t\n\t\t\tdocument.getElementById('progressBarCircle2').setAttribute('stroke-dasharray', strokeLength + ',' + circumference);\n\t\t\tif (evt.progressLegend !== \"\" && evt.totalLegend !== \"\")\n\t\t\t{\n\t\t\t\tdocument.getElementById('progressBarLegendItem1').innerText = evt.progressLegend + '\\r\\n' + formatter.format(evt.progressValue);\n\n\t\t\t\tdocument.getElementById('progressBarLegendSymbol2').className = 'progressBarSquare';\n\t\t\t\tdocument.getElementById('progressBarLegendItem2').innerText = evt.totalLegend + '\\r\\n' + formatter.format(evt.availableCredit);\n\t\t\t\tif (evt.progressValue >= evt.totalValue)\n\t\t\t\t{\n\t\t\t\t\tdocument.getElementById('progressBarLegendSymbol1').className = 'progressBarSquare bad';\n\t\t\t\t}\n\t\t\t\telse\n\t\t\t\t{\n\t\t\t\t\tdocument.getElementById('progressBarLegendSymbol1').className = 'progressBarSquare ok';\n\t\t\t\t}\n\t\t\t}\n\t\t}\n\n\t\twindow.postMessage({ eventName: 'onLoad', control: 'CreditLimitUtilization__' }, document.location.origin);\n\n\t</script>\n\n\t<div id=\"progressBarObject\">\n\t\t<div id=\"progressBar\" class=\"progressBar\">\n\t\t\t<svg id=\"svg\" class=\"circle-chart\" viewBox=\"0 0 120 120\" xmlns=\"http://www.w3.org/2000/svg\">\n\t\t\t\t<circle id=\"progressBarCircle1\" class=\"circle-chart__background\" stroke=\"#dfe5e6\" stroke-width=\"14\" fill=\"none\" cx=\"60\" cy=\"60\" r=\"48\" />\n\t\t\t\t<circle id=\"progressBarCircle2\" class=\"circle-chart__circle\" transform=\"rotate(-90 60 60)\" stroke-width=\"14\" fill=\"none\" cx=\"60\" cy=\"60\" r=\"48\" />\n\t\t\t</svg>\n\t\t\t<div id=\"progressBarText\" class=\"progressBarText\"></div>\n\t\t</div>\n\t\t<div id=\"progressBarLegend\" class=\"progressBarLegend\">\n\t\t\t<table>\n\t\t\t\t<tr>\n\t\t\t\t\t<td><div id=\"progressBarLegendSymbol1\" class=\"progressBarSquare\" /></td>\n\t\t\t\t\t<td><div id=\"progressBarLegendItem1\"></div></td>\n\t\t\t\t</tr>\n\t\t\t\t<tr></tr>\n\t\t\t\t<tr>\n\t\t\t\t\t<td><div id=\"progressBarLegendSymbol2\" class=\"progressBarSquare\" /></td>\n\t\t\t\t\t<td><div id=\"progressBarLegendItem2\"></div></td>\n\t\t\t\t</tr>\n\t\t\t</table>\n\t\t</div>\n\t</div>\n\t<div id=\"progressBarLoader\" class=\"pb-circleLoader-wrapper\">\n\t\t<div class=\"pb-circleLoader-inside\"></div>\n\t</div>\n</div>",
																				"css": "@keyframes circle-chart-fill {\n    to {\n        stroke-dasharray: 0 301;\n    }\n}\n\n@keyframes circle-chart-appear {\n    to {\n        opacity: 1;\n        transform: translateY(0);\n    }\n}\n\n@keyframes pb-spin {\n    0% {\n        transform: rotate(360deg);\n    }\n\n    100% {\n        transform: rotate(0deg);\n    }\n}\n\n@ .circle-chart__circle {\n    animation: circle-chart-fill 2s reverse; /* 1 */\n    transform: rotate(-90deg); /* 2, 3 */\n    transform-origin: center; /* 4 */\n}\n\n@ .circle-chart__info {\n    animation: circle-chart-appear 2s forwards;\n    opacity: 0;\n    transform: translateY(0.3em);\n}\n\n#progressBarObject {\n\tdisplay: none;\n\tfont-family: \"Roboto\";\n}\n\n@ .progressBarComponent {\n\twhite-space: nowrap;\n}\n\n@ .progressBar {\n    display: inline-block;\n    width: 80px;\n    height: 80px;\n    margin-left: 40px;\n    margin-right: 20px;\n}\n\n@ .progressBarLegend {\n    text-align: left;\n}\n\n@ .progressBarText {\n    position: relative;\n    top: -66%;\n    text-align: center;\n    font-size: 14px;\n    color: #394a59;\n}\n\n@ .progressBarTextBlue {\n    color: #0098aa;\n}\n\n@ .progressBarTextRed {\n    color: #f12938;\n}\n\n@ .pb-circleLoader-wrapper {\n    width: 60px;\n    height: 60px;\n    border-radius: 50%;\n    position: relative;\n    overflow: hidden;\n    text-align: left;\n    margin-left: 26px;\n    animation: pb-spin 2s linear infinite;\n}\n\n@ .pb-circleLoader-inside {\n    width: 100%;\n    height: 50%;\n    position: relative;\n    margin-top: 50%;\n    background: linear-gradient(90deg, rgb(220,220,220), rgb(150,150,150));\n}\n\n@ .pb-circleLoader-inside:before {\n    content: '';\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    margin-top: -50%;\n    background: linear-gradient(90deg, rgb(50,50,50), rgb(160,160,160));\n}\n\n@ .pb-circleLoader-inside:after {\n    content: '';\n    width: 80%;\n    height: 160%;\n    position: absolute;\n    margin-top: -40%;\n    margin-left: 10%;\n    background: white;\n    border-radius: 50%;\n}\n\n@ .progressBarSquare {\n\theight: 10px;\n\twidth: 10px;\n\tbackground-color: #dfe5e6;\n\tborder-radius: 25%;\n\tmargin-right: 6px;\n}\n\n@ .progressBarSquare.bad {\n\t\tbackground-color: #f12938;\n}\n\n@ .progressBarSquare.ok {\n\t\tbackground-color: #0098aa;\n}\n",
																				"width": "100%",
																				"hidden": true
																			},
																			"stamp": 27
																		}
																	}
																}
															}
														}
													}
												},
												"RiskCategoryPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_RiskCategoryTitle",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"styleFlex": "0 0 200px",
														"version": 0
													},
													"stamp": 28,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 29,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"RiskCategoryHTML__": {
																				"line": 2,
																				"column": 1
																			},
																			"RiskCategoryTitle__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 30,
																	"*": {
																		"RiskCategoryTitle__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_RiskCategoryTitle",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"RiskCategoryHTML__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_RiskCategory",
																				"version": 0,
																				"htmlContent": "<input id=\"onDisplayRiskCategory\" type=\"hidden\" onclick=\"onDisplayRiskCategory(event);\">\n<div class=\"container\">\n\t<div id=\"RiskCategoryBadgeValue\" class=\"badge\">\n\t\t<span id=\"RiskCategoryValue\"></span>\n\t</div>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"RiskCategoryHTML__\" }, document.location.origin);\n\tfunction onDisplayRiskCategory(evt)\n\t{\n\t\tdocument.getElementById(\"RiskCategoryValue\").innerText = evt.value;\n\t\tdocument.getElementById(\"RiskCategoryBadgeValue\").classList.add(evt.color || \"default\");\n\t}\n</script>",
																				"css": "@ .container {\n    display: inline-block;\n    flex-direction: column;\n    white-space: normal;\n    margin-top: 15px;\n    width: 100%;\n}\n\n@ .container .badge {\n    font-size: 20px;\n    padding: 0.5em;\n    border-radius: 4px;\n    border-width: 1px;\n    display: flex;\n    justify-content: center;\n    min-width: 40px;\n}\n\n@ .container .badge.veryLow {\n    background-color: #5EAD25;\n    color: white;\n}\n\n@ .container .badge.low {\n    background-color: #3FBFAD;\n    color: white;\n}\n\n@ .container .badge.moderate {\n    background-color: #F9BE00;\n    color: white;\n}\n\n@ .container .badge.medium {\n    background-color: #FF8700;\n    color: white;\n}\n\n@ .container .badge.high {\n    background-color: #D92530;\n    color: white;\n}\n\n@ .container .badge.extreme {\n    background-color: #943838;\n    color: white;\n}\n\n@ .container .badge.outofbusiness {\n    background-color: #1A2732;\n    color: white;\n}\n\n@ .container .badge.none {\n    background-color: #C0C0C0;\n    color: white;\n}\n\n@ .container .badge.default {\n    background-color: white;\n    color: #008998;\n}\n\n@ .container .badge > span {\n    font-size: 20px;\n    font-family: Roboto Bold;\n    line-height: normal;\n    display: block;\n    text-transform: uppercase;\n}\n",
																				"width": "100%"
																			},
																			"stamp": 32
																		}
																	}
																}
															}
														}
													}
												},
												"PaymentBehaviorPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_PaymentTermsTitle",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"styleFlex": "0 0 200px",
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"hidden": true,
														"version": 0
													},
													"stamp": 33,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelPaymentTerms__": "PaymentTerms__",
																	"PaymentTerms__": "LabelPaymentTerms__"
																},
																"version": 0
															},
															"stamp": 34,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelPaymentTerms__": {
																				"line": 1,
																				"column": 1
																			},
																			"PaymentTerms__": {
																				"line": 1,
																				"column": 2
																			},
																			"PaymentTermsDisplay__": {
																				"line": 4,
																				"column": 1
																			},
																			"PaymentTermsTitle__": {
																				"line": 2,
																				"column": 1
																			},
																			"SpacerPaymentTerms__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 35,
																	"*": {
																		"PaymentTerms__": {
																			"type": "ComboBox",
																			"data": [
																				"PaymentTerms__"
																			],
																			"options": {
																				"readonly": true,
																				"possibleValues": {
																					"0": "",
																					"1": "Cash In Advance",
																					"2": "Cash On Delivery",
																					"3": "Net 7 Days",
																					"4": "Net 10 Days",
																					"5": "Net 30 Days",
																					"6": "2/10 Net 30",
																					"7": "End of Month",
																					"8": "Net 45 Days",
																					"9": "Net 60 Days",
																					"10": "Net 90 Days"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "cia",
																					"2": "cod",
																					"3": "net7",
																					"4": "net10",
																					"5": "net30",
																					"6": "2/10net30",
																					"7": "eom",
																					"8": "net45",
																					"9": "net60",
																					"10": "net90"
																				},
																				"label": "_PaymentTerms",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"hidden": true,
																				"DisplayedColumns": ""
																			},
																			"stamp": 36
																		},
																		"LabelPaymentTerms__": {
																			"type": "Label",
																			"data": [
																				"PaymentTerms__"
																			],
																			"options": {
																				"label": "_PaymentTerms",
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"PaymentTermsTitle__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_PaymentTermsTitle",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"SpacerPaymentTerms__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default"
																			},
																			"stamp": 39
																		},
																		"PaymentTermsDisplay__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "MS",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_PaymentTermsTitle",
																				"version": 0
																			},
																			"stamp": 40
																		}
																	}
																}
															}
														}
													}
												},
												"ADPPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_ADPTitle",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "center",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"styleFlex": "0 0 200px",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 41,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 42,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ADPTitle__": {
																				"line": 1,
																				"column": 1
																			},
																			"ADPCounter__": {
																				"line": 4,
																				"column": 1
																			},
																			"ADPNoData__": {
																				"line": 3,
																				"column": 1
																			},
																			"ADPSpacer__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 43,
																	"*": {
																		"ADPTitle__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_ADPTitle",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"ADPCounter__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "",
																				"width": 230,
																				"counterType": "Manual",
																				"uiCounterType": "Manual"
																			},
																			"stamp": 45
																		},
																		"ADPSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "_Spacer",
																				"version": 0,
																				"hidden": false
																			},
																			"stamp": 46
																		},
																		"ADPNoData__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_NotEnoughDataTermSync",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 47
																		}
																	}
																}
															}
														}
													}
												},
												"PayerRatingPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_PayerRatingTitle",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"styleFlex": "0 0 200px",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"hidden": true,
														"version": 0,
														"labelLength": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 48,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 49,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"PayerRatingHTML__": {
																				"line": 4,
																				"column": 1
																			},
																			"PayerRatingTitle__": {
																				"line": 1,
																				"column": 1
																			},
																			"PayerRatingSpacer__": {
																				"line": 2,
																				"column": 1
																			},
																			"PayerRatingDescription__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 50,
																	"*": {
																		"PayerRatingTitle__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_PayerRatingTitle",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"PayerRatingSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "_Spacer",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 52
																		},
																		"PayerRatingDescription__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "_PayerRatingDescription",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 53
																		},
																		"PayerRatingHTML__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"hidden": false,
																				"label": "_PayerRating",
																				"version": 0,
																				"width": "100%",
																				"htmlContent": "<input id=\"onDisplayPayerRating\" type=\"hidden\" onclick=\"onDisplayPayerRating(event);\">\n<div id=\"PayerRatingLoader\" class=\"loader-wrapper\" style=\"width: 110px;\">\n    <div class=\"loader-item\">\n        <div class=\"animated-background\" style=\"height: 50px;\">\n            <div class=\"background-masker line1\"></div>\n        </div>\n    </div>\n</div>\n<div id=\"PayerRatingContainer\" class=\"containerPayerRating\"  style=\"display: none;\">\n\t\t<span id=\"PayerRatingValue\"></span>\n\t\t<span id=\"stars\" class=\"starList\">\n\t\t\t<i class=\"fa fa-star-o TermSyncStars\" aria-hidden=\"true\"></i>\n\t\t\t<i class=\"fa fa-star-o TermSyncStars\" aria-hidden=\"true\"></i>\n\t\t\t<i class=\"fa fa-star-o TermSyncStars\" aria-hidden=\"true\"></i>\n\t\t\t<i class=\"fa fa-star-o TermSyncStars\" aria-hidden=\"true\"></i>\n\t\t</span>\n</div>\n<script>\n\tfunction onDisplayPayerRating(evt)\n    {\n        document.getElementById(\"PayerRatingLoader\").style.display = \"none\";\n\t\tif (evt.stars)\n\t\t{\n\t\t\tdocument.getElementById(\"PayerRatingContainer\").style.display = \"\";\n\t\t\tvar displayedStars = document.getElementsByClassName(\"TermSyncStars\");\n\t\t\tfor (var i = 0; i < evt.stars; i++)\n\t\t\t{\n\t\t\t\tdisplayedStars[i].classList.remove(\"fa-star-o\");\n\t\t\t\tdisplayedStars[i].classList.add(\"fa-star\");\n\t\t\t}\n\t\t}\n\t}\n</script>",
																				"css": "@ .containerPayerRating {\n    display: grid;\n    font-size: 20px;\n    font-family: Roboto;\n    justify-content: center;\n    text-align: center;\n\tmargin-top: -5px;\n}\n\n@ .starList {\n\tcolor:gold;\n}\n\n#PayerRatingLoader {\n    margin: 20px auto 0 auto;\n}"
																			},
																			"stamp": 54
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-30": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"additionalCssStyle": "flexParent",
												"version": 0
											},
											"stamp": 55,
											"*": {
												"OrdersCountersPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Orders",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center"
													},
													"stamp": 56,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {}
															},
															"stamp": 57,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"OrdersToValidate__": {
																				"line": 1,
																				"column": 1
																			},
																			"BlockedOrdersCounter__": {
																				"line": 2,
																				"column": 1
																			},
																			"ImportedOrderCounter__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"stamp": 58,
																	"*": {
																		"OrdersToValidate__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "Orders to validate",
																				"width": 230,
																				"version": 0,
																				"viewName": "_Customer orders waiting for validation",
																				"tabName": "_Customer orders",
																				"viewData": "Customer Order Processing",
																				"uiCounterType": "CountAndSum",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Count\",\"upFormat\":\"{0}\",\"downFormat\":\"{0}\"}",
																				"counterLabel": "_Orders to validate",
																				"warningThreshold": "1",
																				"errorThreshold": "",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Currency__",
																				"counterTypeParam": "Total__",
																				"delayLoad": true,
																				"clickable": true,
																				"hidden": true
																			},
																			"stamp": 59
																		},
																		"ImportedOrderCounter__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"hidden": true,
																				"label": "",
																				"width": 230,
																				"version": 0,
																				"counterLabel": "_ImportedOrdersCounter",
																				"counterType": "Manual",
																				"uiCounterType": "Manual",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} order(s)\"}",
																				"clickable": true,
																				"viewName": null,
																				"tabName": null,
																				"viewData": null
																			},
																			"stamp": 60
																		},
																		"BlockedOrdersCounter__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "BlockedOrdersCounter",
																				"width": 230,
																				"viewName": "_Blocked Imported Orders - embedded",
																				"tabName": "_ImportedOrders - embedded",
																				"viewData": "CM - Orders Import__",
																				"uiCounterType": "SumAndCount",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} order(s)\"}",
																				"counterLabel": "_BlockedOrdersCounter",
																				"warningThreshold": "",
																				"errorThreshold": "1",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Currency__",
																				"counterTypeParam": "Order_Total__",
																				"numberFormatting": "Automatic",
																				"delayLoad": true,
																				"clickable": true,
																				"hidden": true
																			},
																			"stamp": 61
																		}
																	}
																}
															}
														}
													}
												},
												"CountersPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Invoices",
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"hidden": true,
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 62,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 63,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TotalOutstanding__": {
																				"line": 3,
																				"column": 1
																			},
																			"Invoices60Days__": {
																				"line": 4,
																				"column": 1
																			},
																			"CreditNotes60Days__": {
																				"line": 5,
																				"column": 1
																			},
																			"Disputes__": {
																				"line": 2,
																				"column": 1
																			},
																			"Overdue__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 64,
																	"*": {
																		"Overdue__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "Overdue",
																				"width": 230,
																				"viewName": "_Customer invoices payment late - embedded",
																				"tabName": "_EInvoice supplier - embedded",
																				"viewData": "Customer invoices (supplier copy)",
																				"uiCounterType": "SumAndCount",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																				"counterLabel": "_Overdue",
																				"warningThreshold": "",
																				"errorThreshold": "",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Invoice_currency__",
																				"counterTypeParam": "Invoice_outstanding_amount__",
																				"numberFormatting": "Automatic",
																				"delayLoad": true,
																				"clickable": true,
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"TotalOutstanding__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"hidden": true,
																				"label": "",
																				"width": 230,
																				"version": 0,
																				"counterLabel": "_Total outstanding",
																				"counterType": "Manual",
																				"uiCounterType": "Manual",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																				"clickable": true,
																				"viewName": null,
																				"tabName": null,
																				"viewData": null
																			},
																			"stamp": 66
																		},
																		"Invoices60Days__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "Invoices60Days",
																				"width": 230,
																				"version": 0,
																				"viewName": "_Customer invoices 60 days - embedded",
																				"tabName": "_EInvoice supplier - embedded",
																				"viewData": "Customer invoices (supplier copy)",
																				"uiCounterType": "SumAndCount",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																				"counterLabel": "_Invoices 60 days",
																				"warningThreshold": "",
																				"errorThreshold": "",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Invoice_currency__",
																				"counterTypeParam": "Invoice_total_amount__",
																				"numberFormatting": "Automatic",
																				"hidden": true,
																				"delayLoad": true,
																				"clickable": true
																			},
																			"stamp": 67
																		},
																		"CreditNotes60Days__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "CreditNotes60Days",
																				"width": 230,
																				"version": 0,
																				"viewName": "_Customer credits 60 days - embedded",
																				"tabName": "_EInvoice supplier - embedded",
																				"viewData": "Customer invoices (supplier copy)",
																				"uiCounterType": "SumAndCount",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} credit notes\"}",
																				"counterLabel": "_Credit notes 60 days",
																				"warningThreshold": "",
																				"errorThreshold": "",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Invoice_currency__",
																				"counterTypeParam": "Invoice_total_amount__",
																				"numberFormatting": "Automatic",
																				"hidden": true,
																				"delayLoad": true,
																				"clickable": true
																			},
																			"stamp": 68
																		},
																		"Disputes__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "Disputes",
																				"width": 230,
																				"version": 0,
																				"viewName": "_Disputed invoices supplier - embedded",
																				"tabName": "_EInvoice supplier - embedded",
																				"viewData": "Customer invoices (supplier copy)",
																				"uiCounterType": "SumAndCount",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} invoice(s)\"}",
																				"counterLabel": "_Disputes",
																				"warningThreshold": "",
																				"errorThreshold": "0",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Invoice_currency__",
																				"counterTypeParam": "Dispute_Amount__",
																				"numberFormatting": "Automatic",
																				"hidden": true,
																				"delayLoad": true,
																				"clickable": true
																			},
																			"stamp": 69
																		}
																	}
																}
															}
														}
													}
												},
												"DeductionsCountersPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"flexPanel": true,
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Deductions counters",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "center"
													},
													"stamp": 70,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {}
															},
															"stamp": 71,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DeductionsToValidate__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"stamp": 72,
																	"*": {
																		"DeductionsToValidate__": {
																			"type": "Counter",
																			"data": false,
																			"options": {
																				"label": "Deductions to validate",
																				"width": 230,
																				"version": 0,
																				"viewName": "_View_To approve embedded",
																				"tabName": "_Tab_Deductions embedded",
																				"viewData": "Deductions Processing",
																				"uiCounterType": "CountAndSum",
																				"counterType": "CountAndSum",
																				"displayFormat": "{\"upCounter\":\"Sum\",\"upFormat\":\"{0}\",\"downFormat\":\"_{0} deductions(s)\"}",
																				"counterLabel": "_Deductions to validate",
																				"warningThreshold": "1",
																				"errorThreshold": "",
																				"counterSumType": "Currency",
																				"counterSumCurrencyParam": "Currency__",
																				"counterTypeParam": "Settlement_amount__",
																				"numberFormatting": "Automatic",
																				"delayLoad": true,
																				"clickable": true,
																				"hidden": true
																			},
																			"stamp": 73
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-17": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"additionalCssStyle": "flexParent",
												"version": 0
											},
											"stamp": 74,
											"*": {
												"ReportsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"flexPanel": true,
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_ReportsPane",
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"hidden": true,
														"version": 0
													},
													"stamp": 75,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 76,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"InvoicesAndCreditNotes__": {
																				"line": 1,
																				"column": 1
																			},
																			"AgedBalance__": {
																				"line": 2,
																				"column": 1
																			},
																			"Historic__": {
																				"line": 3,
																				"column": 1
																			},
																			"ClaimAmountByMonth__": {
																				"line": 4,
																				"column": 1
																			},
																			"ClaimAmountByClaimType__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 77,
																	"*": {
																		"InvoicesAndCreditNotes__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "InvoicesAndCreditNotes",
																				"width": 230,
																				"reportName": "_ReportName_AR_Invoices count",
																				"reportTitle": "_Number of invoices and credit notes (3 months)",
																				"delayLoad": true,
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 78
																		},
																		"AgedBalance__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "_AgedBalance",
																				"width": 230,
																				"version": 0,
																				"reportName": "_ReportName_AR_Local Aged balance invoices and credit notes",
																				"reportTitle": "_AgedBalancedReport",
																				"delayLoad": true,
																				"hidden": true
																			},
																			"stamp": 79
																		},
																		"Historic__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "Historic",
																				"width": 230,
																				"reportWidth": "ExtraLarge",
																				"reportName": "",
																				"reportTitle": "_Aging historic",
																				"delayLoad": false,
																				"hidden": true
																			},
																			"stamp": 80
																		},
																		"ClaimAmountByMonth__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "_ClaimAmountByMonth",
																				"width": 230,
																				"version": 0,
																				"reportWidth": "Medium",
																				"reportHeight": "Medium",
																				"reportName": "_REPORT_Deductions amount by months and claims type",
																				"reportTitle": "_ClaimAmountByMonthReport",
																				"delayLoad": false,
																				"hidden": true
																			},
																			"stamp": 81
																		},
																		"ClaimAmountByClaimType__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "_ClaimAmountByClaimType",
																				"width": 230,
																				"version": 0,
																				"reportWidth": "Medium",
																				"reportHeight": "Medium",
																				"reportName": "_REPORT_Deduction repartition by claims type",
																				"reportTitle": "_ClaimAmountByClaimTypeReport",
																				"delayLoad": false,
																				"hidden": true
																			},
																			"stamp": 82
																		}
																	}
																}
															}
														}
													}
												},
												"CreditScorePane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"flexPanel": true,
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_CreditScorePane",
														"panelStyle": "FlexibleFormPanelLight flexPanel",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 83,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 84,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CreditScore_GaugeContainer__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 85,
																	"*": {
																		"CreditScore_GaugeContainer__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "CreditScore_GaugeContainer",
																				"htmlContent": "<input id=\"onAddGauge\" type=\"hidden\" onclick=\"OnAddGauge(event);\">\n<input id=\"onLoadGaugeContainer\" type=\"hidden\" onclick=\"OnLoadGaugeContainer(event);\">\n<input id=\"onRefreshGauge\" type=\"hidden\" onclick=\"onRefreshGauge(event)\">\n<input id=\"onSetError\" type=\"hidden\" onclick=\"onSetError(event)\">\n<input id=\"onNoSelection\" type=\"hidden\" onclick=\"onNoSelection(event)\">\n<input id=\"onOpenB64Pdf\" type=\"hidden\" onclick=\"onOpenB64Pdf(event)\">\n\n<div id=\"gaugeContainerLoader\"></div>\n<div id=\"gaugeContainer\"></div>\n\n<script>\n\n    var Tools = {\n        CloneObject: function (o)\n\t\t{\n            return o ? JSON.parse(JSON.stringify(o)) : null;\n        },\n        HtmlToElement: function (html)\n        {\n            var template = document.createElement('template');\n            if (template.content)\n            {\n                template.innerHTML = html.trim();\n                return template.content.firstChild;\n            }\n            else\n            {\n                template = document.createElement('div');\n                template.innerHTML = html.trim();\n                return template.firstChild;\n            }\n        }\n    };\n\n    var gauges = {};\n    var colors = { green: \"#3fbfad\", yellow: \"#f2ae00\", orange: \"#ff6c0c\", red: \"#d3222a\", black: \"#1a2732\", grey: \"#dfe5e6\" };\n\n    var Gauge = function (opts, containerElem, isFirstOne)\n    {\n        this.Init(opts);\n        this._nbTilesPerLine = 2;\n        this._animationDuration = 500;\n        this._refreshRate = 1000 / 60;\n        this._actualDuration = 0;\n        this._circleData = { x: 63, y: 63, rayon: 56, lineWidth: 7 };\n        this._canvasSize = this._circleData.x * 2;\n\n        if (isFirstOne)\n            containerElem.appendChild(Tools.HtmlToElement('<div class=\"gaugeDivider\"></div>'));\n\n        this._logoHtml = '<div class=\"logowrapper\" style=\"background-image: url(' + opts.logoPath + ');\" onerror=\"Gauge.OnImgError(this);\"></div>';\n        this._loaderHtml = '<div class=\"loader\"></div>';\n        var html = this._logoHtml + this.GetHtml(opts);\n\n        if (opts.delayGauge)\n        {\n            this.elem = opts.delayGauge.elem;\n            this.elem.innerHTML = html;\n            this.InitDrawing();\n        }\n        else\n        {\n            this.elem = Tools.HtmlToElement('<div id=\"' + opts.name + '\" class=\"gauge\">' + html + '</div >');\n            this.InitDrawing();\n            containerElem.appendChild(this.elem);\n        }\n    };\n\n    Gauge.Control = \"CreditScore_GaugeContainer__\";\n    Gauge.OnImgError = function (imgElem) { imgElem.style.display = \"none\"; };\n    Gauge.OnRefreshClick = function (gaugeId, action) { gauges[gaugeId].DisplayLoader(); window.postMessage({ eventName: \"OnRefreshClick\", control: Gauge.Control, gaugeId: gaugeId, action: action }, document.location.origin); };\n    Gauge.OnDelayLoadClick = function (gaugeId) { gauges[gaugeId].DisplayLoader(); window.postMessage({ eventName: \"onDelayLoadClick\", control: Gauge.Control, gaugeId: gaugeId }, document.location.origin); };\n    Gauge.Factory = function (opts, container, gauges) { return opts && opts.isDelayed ? new DelayedGauge(opts, container, Object.keys(gauges).length) : new Gauge(opts, container, Object.keys(gauges).length); }\n    Gauge.OnViewMoreClick = function (gaugeId) { return gauges[gaugeId].onViewMoreClick && gauges[gaugeId].onViewMoreClick(gauges[gaugeId]); };\n\n    Gauge.prototype = {\n        Init: function (opts)\n        {\n            var gaugeColor = opts.gauge.color;\n            this.name = opts.name,\n            this.gauge = Tools.CloneObject(opts.gauge);\n            this.footer = Tools.CloneObject(opts.footer);\n            this.tiles = Tools.CloneObject(opts.footer.tiles);\n            this.alerts = Tools.CloneObject(opts.footer.alerts);\n\n            this.gauge.color = colors.green;\n            this.gauge.subRingColor = colors.grey;\n\n            if (!gaugeColor && this.gauge.thresholds)\n            {\n                var thValFind = false;\n                for (var i = 0; i < this.gauge.thresholds.length && !thValFind; i++)\n                {\n                    if (this.gauge.value <= this.gauge.thresholds[i].maxVal)\n                    {\n                        thValFind = true;\n                        this.gauge.color = colors[this.gauge.thresholds[i].color] || this.gauge.thresholds[i].color;\n                        if (this.gauge.value === this.gauge.scale.min.val)\n                            this.gauge.subRingColor = this.gauge.color;\n                    }\n                }\n            }\n        },\n\n        DisplayLoader: function ()\n        {\n            this.elem.innerHTML = this._logoHtml + this._loaderHtml;\n        },\n\n        InitDrawing: function ()\n        {\n            var canvas = this.elem.getElementsByTagName(\"canvas\")[0];\n            this.ctx = canvas.getContext(\"2d\");\n            this.ctx.translate(0.5, 0.5);\n        },\n        GetHtml: function (opts)\n        {\n            var variationIndicatorClass = \"fa fa-pause-circle-o fa-rotate-90\";\n            if (this.gauge.variation === \"up\")\n                variationIndicatorClass = \"fa fa-arrow-circle-up green\";\n            else if (this.gauge.variation === \"down\")\n                variationIndicatorClass = \"fa fa-arrow-circle-down red\";\n\n            var html = '<div class=\"canvasContainer\"><canvas width=\"' + this._canvasSize + '\" height=\"' + this._canvasSize + '\"></canvas>';\n            html += this.gauge.scoreName ?\n                '<div class=\"values\"><div class=\"value\">' + this.gauge.displayedValue + '</div><div class=\"scoreName\">' + this.gauge.scoreName + '</div>' :\n                '<div class=\"values noName\"><div class=\"value\">' + this.gauge.displayedValue + '</div>';\n            \n            html += '<div class=\"variationIndicator\"><i class=\"' + variationIndicatorClass + '\"></i></div></div>\\\n<div class=\"scale scaleMax\">' + this.gauge.scale.max.val + '</div> <div class=\"scale scaleMin\">' + this.gauge.scale.min.val + '</div>\\\n</div><div class=\"footer\"><div class=\"header\">';\n\n            if (this.footer.viewMoreLink)\n            {\n                if (this.footer.viewMoreLink.url)\n                {\n\t\t\t\t\thtml += '<a href=\"ViewMore\" onclick=\"window.open(\\'' + this.footer.viewMoreLink.url + '\\')\">' + this.footer.viewMoreLink.label + '</a>';\n\t\t\t\t}\n\t\t\t\telse if (opts.footer.viewMoreLink.onClick && typeof opts.footer.viewMoreLink.onClick === \"function\")\n                {\n\t\t\t\t\tthis.onViewMoreClick = opts.footer.viewMoreLink.onClick;\n\t\t\t\t\tif (!opts.footer.viewMoreLink.hidden)\n\t\t\t\t\t{\n\t\t\t\t\t\thtml += '<a href=\"ViewMore\" onclick=\"Gauge.OnViewMoreClick(\\'' + this.name + '\\'); return false;\">' + this.footer.viewMoreLink.label + '</a>';\n\t\t\t\t\t}\n\t\t\t\t\telse\n\t\t\t\t\t{\n\t\t\t\t\t\thtml += '<a style=\"visibility:hidden;\" href=\"ViewMore\" onclick=\"Gauge.OnViewMoreClick(\\'' + this.name + '\\'); return false;\">' + this.footer.viewMoreLink.label + '</a>';\n\t\t\t\t\t}\n\n                }\n            }\n\n            if (this.footer.editLink && !this.footer.editLink.hidden)\n                html += '<a href=\"Edit\" onclick=\"Gauge.OnRefreshClick(\\'' + this.name + '\\', \\'edit\\');return false;\">' + this.footer.editLink.label + '</a>';\n\t\t\t\n            html += '</div><div class=\"tilesContainer\">';\n\n            var htmlTiles = \"\";\n            if (this.tiles && this.tiles.length > 0)\n            {\n                var nbLineTiles = 0;\n                var template = '<div class=\"tile\">\\\n<div class=\"inTile {0}\">\\\n    <div class=\"content {1}\">\\\n        <div class=\"value\">{2}</div>\\\n        <div class=\"title\" >{3}</div>\\\n    </div>\\\n</div></div>';\n\n                for (var i = 0; i < this.tiles.length; i++)\n                {\n                    var tile = this.tiles[i];\n                    if (nbLineTiles === 0)\n                        htmlTiles += '<div class=\"tileLine\">';\n\n                    var htmlTile = template\n                        .replace(\"{0}\", nbLineTiles === 0 ? ' first' : '')\n                        .replace(\"{1}\", tile.indicator || '')\n                        .replace(\"{2}\", tile.value)\n                        .replace(\"{3}\", tile.title);\n\n                    htmlTiles += htmlTile;\n                    nbLineTiles++;\n                    if (nbLineTiles === this._nbTilesPerLine)\n                    {\n                        nbLineTiles = 0;\n                        htmlTiles += '</div>';\n                    }\n                }\n            }\n\n            html += htmlTiles + '</div>';\n            if (this.alerts && this.alerts.list && this.alerts.list.length > 0)\n            {\n                var htmlAlerts = \"\";\n\t\t\t\tvar htmlAlertsTemplate = '<div class=alertsContainer><div class=\"alerts {0}\">{1}</div></div>'\n\t\t\t\tvar htmlSingleAlertTemplate = this.alerts.list.length > 1 ?\n\t\t\t\t\t'<div class=\"alertItem\"><i class=\"fa fa-circle-o bullet\"></i>{0}</div>' :\n\t\t\t\t\t'<div class=\"alertItem\">{0}</div>';\n\n                for (var i = 0; i < this.alerts.list.length; i++)\n                    htmlAlerts += htmlSingleAlertTemplate.replace(\"{0}\", this.alerts.list[i]);\n\n                html += htmlAlertsTemplate\n                    .replace(\"{0}\", this.alerts.indicator)\n                    .replace(\"{1}\", htmlAlerts) + '</div>';\n            }\n            \n            return html;\n        },\n        GetDelayedHtml: function (opts)\n        {\n            return '<div class=\"delayLoadBtn\" onclick=\"Gauge.OnDelayLoadClick(\\'' + opts.name + '\\')\">Load</div>';\n        },\n        DrawCircle: function (angleStart, degreeEnd)\n        {\n            this.ctx.arc(this._circleData.x, this._circleData.y, this._circleData.rayon, angleStart + Math.PI / 2, degreeEnd + Math.PI / 2);\n            this.ctx.stroke();\n        },\n        DrawFrame: function ()\n        {\n            var angleEnd = this.gauge.value * 3 * Math.PI * this._actualDuration / (this._animationDuration * this.gauge.scale.max.val * 2);\n            this.ctx.lineWidth = this._circleData.lineWidth;\n\n            if (this.gauge.value != this.gauge.scale.max.val || this._actualDuration < this._animationDuration)\n            {\n                this.ctx.beginPath();\n                this.ctx.strokeStyle = this.gauge.subRingColor;\n                this.DrawCircle(angleEnd, -Math.PI / 2);\n            }\n\n            this.ctx.beginPath();\n            this.ctx.strokeStyle = this.gauge.color;\n            this.DrawCircle(0, angleEnd);\n        },\n        ClearGauge: function ()\n        {\n            this.ctx.clearRect(0, 0, 126, 126);\n        },\n        Anime: function ()\n        {\n            if (this._actualDuration >= this._animationDuration)\n            {\n                this._actualDuration = this._animationDuration;\n                clearInterval(this.gaugeIntervalID);\n            }\n            this.ClearGauge();\n            this.DrawFrame();\n            this._actualDuration += this._refreshRate;\n        },\n        Start: function ()\n        {\n            var that = this;\n            this._actualDuration = 0;\n            this.gaugeIntervalID = setInterval(function () { that.Anime.apply(that); }, that._refreshRate);\n        },\n        Refresh: function (opts)\n        {\n\n            this.Init(opts);\n            this.elem.innerHTML = this._logoHtml + this.GetHtml(opts);\n            this.InitDrawing();\n            this.Start();\n        },\n        SetError: function (error, opts)\n        {\n            this.Init(opts);\n            this.elem.innerHTML = this._logoHtml + '<div class=\"textError\">' + error + '</div>';\n        },\n        SetNoScore: function (error, opts)\n        {\n            this.footer = Tools.CloneObject(opts.footer);\n            var html = this._logoHtml + '<div class=\"footer\"><div class=\"header\">';\n\n            if (this.footer.editLink && !this.footer.editLink.hidden)\n            {\n                html += '<a href=\"Edit\" onclick=\"Gauge.OnRefreshClick(\\'' + opts.name + '\\', \\'edit\\');return false;\">' + this.footer.editLink.label + '</a>';\n            }\n            html += '</div></div><div class=\"textError\">' + error + '</div>';\n            this.elem.innerHTML = html;\n        },\n        Reset: function (opts)\n        {\n            this.Init(opts);\n            this.elem.innerHTML = this._logoHtml + this.GetHtml(opts);\n            this.InitDrawing();\n            this.Start();\n        }\n    };\n\n    var DelayedGauge = function (opts, containerElem, isFirstOne)\n    {\n        if (isFirstOne)\n            containerElem.appendChild(Tools.HtmlToElement('<div class=\"gaugeDivider\"></div>'));\n\n        this._name = opts.name;\n        this._logoHtml = '<div class=\"logowrapper\" style=\"background-image: url(' + opts.logoPath + ');\" onerror=\"Gauge.OnImgError(this);\"></div>';\n        this._loaderHtml = '<div class=\"loader\"></div>';\n\n        var html = '<div id=\"' + opts.name + '\" class=\"gauge\">' + this._logoHtml;\n        html += opts.directLoad ? this._loaderHtml : '<div class=\"delayLoadBtn\" onclick=\"Gauge.OnDelayLoadClick(\\'' + this._name + '\\')\">' + opts.loadLabel + '</div>';\n        html += '</div > ';\n\n        this.elem = Tools.HtmlToElement(html);\n        containerElem.appendChild(this.elem);\n    }\n\n    DelayedGauge.prototype = {\n        Start: function () { },\n        DisplayLoader: function ()\n        {\n            this.elem.innerHTML = this._logoHtml + this._loaderHtml;\n        },\n        Refresh: function (opts)\n        {\n            opts.delayGauge = this;\n            gauges[this._name] = new Gauge(opts);\n            gauges[this._name].Start();\n        },\n        SetError: function (error)\n        {\n            this.elem.innerHTML = this._logoHtml + '<div class=\"textError\">' + error + '</div>'\n        },\n        SetNoScore: function (error, opts)\n        {\n            this.footer = Tools.CloneObject(opts.footer);\n            var html = this._logoHtml + '<div class=\"footer\"><div class=\"header\">';\n\n            if (this.footer.editLink && !this.footer.editLink.hidden)\n            {\n                html += '<a href=\"Edit\" onclick=\"Gauge.OnRefreshClick(\\'' + opts.name + '\\', \\'edit\\');return false;\">' + this.footer.editLink.label + '</a>';\n            }\n            html += '</div></div><div class=\"textError\">' + error + '</div>';\n            this.elem.innerHTML = html;\n        },\n        Reset: function (opts)\n        {\n            this.elem.innerHTML = this._logoHtml + '<div class=\"delayLoadBtn\" onclick=\"Gauge.OnDelayLoadClick(\\'' + opts.name + '\\')\">' + opts.loadLabel + '</div>';\n        }\n    };\n\n    window.postMessage({ eventName: \"onLoad\", control: Gauge.Control }, document.location.origin);\n    function OnLoadGaugeContainer(evt)\n    {\n        document.getElementById(\"gaugeContainerLoader\").innerHTML = evt.imgElemHtml;\n    }\n\n    function OnAddGauge(evt)\n    {\n        document.getElementById(\"gaugeContainerLoader\").style.display = \"none\";\n        const containerElem = document.getElementById(\"gaugeContainer\");\n        gauges[evt.data.name] = Gauge.Factory(evt.data, containerElem, gauges);\n        gauges[evt.data.name].Start();\n    }\n\n    function onRefreshGauge(evt)\n    {\n        gauges[evt.data.name].Refresh(evt.data);\n    }\n\n    function onSetError(evt)\n    {\n        if (evt.data && evt.noScore)\n        {\n            gauges[evt.data.name].SetNoScore(evt.data.msg, evt.data);\n            return;\n        }\n        else if (evt.data)\n        {\n            gauges[evt.data.name].SetError(evt.data.msg, evt.data);\n        }\n        else\n        {\n            document.getElementById(\"gaugeContainerLoader\").style.display = \"none\";\n            const containerElem = document.getElementById(\"gaugeContainer\");\n            containerElem.innerText = evt.error;\n        }\n    }\n\n    function onNoSelection(evt)\n    {\n        if (evt.data.noSelection && gauges[evt.data.name].gauge)\n        {\n            gauges[evt.data.name].name = evt.data.name;\n            gauges[evt.data.name].Reset(gauges[evt.data.name]);\n            return;\n        }\n        gauges[evt.data.name].Reset(evt.data);\n    }\n\n    function onOpenB64Pdf(evt)\n    {\n        const byteCharacters = atob(evt.b64PdfData);\n        const sliceSize = 512;\n        const byteArrays = [];\n\n        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize)\n        {\n            const slice = byteCharacters.slice(offset, offset + sliceSize);\n            const byteNumbers = new Array(slice.length);\n\n            for (var i = 0; i < slice.length; i++)\n                byteNumbers[i] = slice.charCodeAt(i);\n\n            const byteArray = new Uint8Array(byteNumbers);\n            byteArrays.push(byteArray);\n        }\n\n        const newBlob = new Blob(byteArrays, { type: \"application/pdf\" });\n\n        if (window.navigator && window.navigator.msSaveOrOpenBlob)\n        {\n            window.navigator.msSaveOrOpenBlob(newBlob);\n            return;\n        }\n\n        const data = window.URL.createObjectURL(newBlob);\n        const link = document.createElement('a');\n        link.href = data;\n        link.target = \"_blank\"\n        //link.download=\"file.pdf\";\n        link.click();\n        setTimeout(function ()\n        {\n            // For Firefox it is necessary to delay revoking the ObjectURL\n            window.URL.revokeObjectURL(data);\n        }, 100);\n    }\n\n</script>",
																				"css": "@keyframes fadein {\n\tfrom {\n\t\topacity: 0;\n\t}\n\n\tto {\n\t\topacity: 1;\n\t}\n}\n\n@keyframes loader {\n\t0% {\n\t\t-webkit-transform: rotate(0deg);\n\t\ttransform: rotate(0deg);\n\t}\n\n\t100% {\n\t\t-webkit-transform: rotate(360deg);\n\t\ttransform: rotate(360deg);\n\t}\n}\n\n#gaugeContainer {\n\tdisplay: flex;\n\tjustify-content: flex-start;\n\tflex-flow: row wrap;\n}\n\n.gaugeDivider {\n\tmargin: 5px 15px;\n}\n\n.gauge {\n\tfont-family: Roboto;\n\tmargin: 2px;\n\tdisplay: flex;\n\tflex-direction: column;\n\tpadding-top: 5px;\n}\n\n\t.gauge .canvasContainer {\n\t\tposition: relative;\n\t\twidth: 112px;\n\t\tmargin: 5px auto 10px auto;\n\t}\n\n\t\t.gauge .canvasContainer canvas {\n\t\t\tmargin: auto;\n\t\t\tdisplay: block;\n\t\t}\n\n\t\t.gauge .canvasContainer .scale {\n\t\t\tline-height: 1;\n\t\t\tcolor: #1a2732;\n\t\t\tfont-size: 14px;\n\t\t}\n\n\t\t.gauge .canvasContainer .scaleMax {\n\t\t\tposition: absolute;\n\t\t\ttop: 67px;\n\t\t\tright: -22px;\n\t\t\twidth: 30px;\n\t\t\ttext-align: center;\n\t\t}\n\n\t\t.gauge .canvasContainer .scaleMin {\n\t\t\tposition: absolute;\n\t\t\ttop: 111.5px;\n\t\t\tleft: 66.5px;\n\t\t}\n\n\n\t\t.gauge .canvasContainer .values {\n\t\t\tposition: absolute;\n\t\t\ttop: 28px;\n\t\t\tline-height: 1;\n\t\t\tcolor: #1a2732;\n\t\t\twidth: 126px;\n\t\t\ttext-align: center;\n\t\t\tanimation: fadein 1.5s;\n\t\t}\n\n\t\t\t.gauge .canvasContainer .values.noName {\n\t\t\t\ttop: 36px;\n\t\t\t}\n\n\t\t\t.gauge .canvasContainer .values div {\n\t\t\t\tline-height: 1;\n\t\t\t}\n\n\t\t\t.gauge .canvasContainer .values .value {\n\t\t\t\tfont-size: 40px;\n\t\t\t\tfont-family: Roboto bold;\n\t\t\t}\n\n\t\t\t.gauge .canvasContainer .values .scoreName {\n\t\t\t\tfont-size: 16px;\n\t\t\t\tfont-family: Roboto;\n\t\t\t}\n\n\t\t.gauge .canvasContainer .variationIndicator {\n\t\t\tcolor: #8996a0;\n\t\t\tfont-size: 16px;\n\t\t}\n\n\t\t\t.gauge .canvasContainer .variationIndicator .green {\n\t\t\t\tcolor: #3fbfad;\n\t\t\t}\n\n\t\t\t.gauge .canvasContainer .variationIndicator .red {\n\t\t\t\tcolor: #f12938;\n\t\t\t}\n\n\n\t.gauge .footer {\n\t\tcolor: #1a2732;\n\t\tfont-size: 14px;\n\t\tpadding: 5px 0;\n\t\tdisplay: flex;\n\t\tflex-direction: column;\n\t}\n\n\t\t.gauge .footer .header {\n\t\t\tdisplay: flex;\n\t\t\tjustify-content: space-between;\n\t\t\tmargin-left: 2px;\n\t\t\tfont-family: 'Roboto Bold';\n\t\t\tmargin-bottom: 3px;\n\t\t}\n\t\t\t.gauge .footer .header a {\n\t\t\t\tbackground-color: transparent;\n\t\t\t\tcolor: #008998;\n\t\t\t\tborder: 1px;\n\t\t\t\tborder-color: #008998;\n\t\t\t\tborder-style: solid;\n\t\t\t\tbox-sizing: border-box;\n\t\t\t\tborder-radius: 4px;\n\t\t\t\tpadding: 4px;\n\t\t\t\ttext-decoration: none;\n\t\t\t}\n\n\t\t\t\t.gauge .footer .header a:hover {\n\t\t\t\t\tbackground-color: #008998;\n\t\t\t\t\tcolor: white;\n\t\t\t\t\tborder-color: #008998;\n\t\t\t\t}\n\n\t\t.gauge .footer .title {\n\t\t\ttext-align: left;\n\t\t\tmargin-right: 5px;\n\t\t\tcolor: #8996a0;\n\t\t\twhite-space: nowrap;\n\t\t}\n\n\t\t.gauge .footer .tilesContainer {\n\t\t\tcolor: #1a2732;\n\t\t\tdisplay: table;\n\t\t}\n\n\t\t\t.gauge .footer .tilesContainer .tileLine {\n\t\t\t\tdisplay: table-row;\n\t\t\t}\n\n\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile {\n\t\t\t\t\tdisplay: table-cell;\n\t\t\t\t}\n\n\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile {\n\t\t\t\t\t\tborder: 1px solid #dfe5e6;\n\t\t\t\t\t\tborder-radius: 6px;\n\t\t\t\t\t\tfont-size: 12px;\n\t\t\t\t\t\toverflow: hidden;\n\t\t\t\t\t\tmargin: 5px 0 0 2.5px;\n\t\t\t\t\t}\n\n\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .title {\n\t\t\t\t\t\twhite-space: nowrap;\n\t\t\t\t\t\tfont-family: Roboto;\n\t\t\t\t\t}\n\n\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile.first {\n\t\t\t\t\t\tmargin-left: 0;\n\t\t\t\t\t\tmargin-right: 2.5px;\n\t\t\t\t\t}\n\n\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile .content {\n\t\t\t\t\t\tborder-left: 4px solid #dfe5e6;\n\t\t\t\t\t\tpadding: 2px 3px;\n\t\t\t\t\t}\n\n\t\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile .content.favourable {\n\t\t\t\t\t\t\tborder-left-color: #3fbfad;\n\t\t\t\t\t\t}\n\n\t\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile .content.alert {\n\t\t\t\t\t\t\tborder-left-color: #f12938;\n\t\t\t\t\t\t}\n\n\t\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile .content.warning {\n\t\t\t\t\t\t\tborder-left-color: #ff8700;\n\t\t\t\t\t\t\tbackground-color: #fff3e5;\n\t\t\t\t\t\t}\n\n\t\t\t\t\t\t.gauge .footer .tilesContainer .tileLine .tile .inTile .content .value {\n\t\t\t\t\t\t\tfont-family: Roboto Bold;\n\t\t\t\t\t\t}\n\n\t\t.gauge .footer .alertsContainer {\n\t\t\tborder: 1px solid #dfe5e6;\n\t\t\tborder-radius: 6px;\n\t\t\tmargin: 5px 0;\n\t\t\toverflow: hidden;\n\t\t}\n\n\t\t.gauge .footer .alerts {\n\t\t\tborder-left: solid 4px #dfe5e6;\n\t\t\tpadding: 5px 0;\n\t\t}\n\n\t\t\t.gauge .footer .alerts.favourable {\n\t\t\t\tborder-left-color: #3fbfad;\n\t\t\t\tbackground-color: #e5f4f6;\n\t\t\t}\n\n\t\t\t.gauge .footer .alerts.warning {\n\t\t\t\tbackground-color: #fff3e5;\n\t\t\t\tborder-left-color: #ff8700;\n\t\t\t}\n\n\t\t\t.gauge .footer .alerts.alert {\n\t\t\t\tbackground-color: #fee9eb;\n\t\t\t\tborder-left-color: #f12938;\n\t\t\t}\n\n\t\t.gauge .footer .alerts .bullet {\n\t\t\t\tfont-size: 7px;\n\t\t\t\tdisplay: inline;\n\t\t\t\tvertical-align: middle;\n\t\t\t\tmargin-right: 5px;\n\t\t\t\tfont-weight: 900;\n\t\t\t\tposition: relative;\n\t\t\t\ttop: -1px;\n\t\t\t}\n\n\t\t.gauge .footer .alerts .alertItem {\n\t\t\twhite-space: normal;\n\t\t\tmargin-left: 6px;\n\t\t\tpadding-left: 1px;\n\t\t\tpadding-right: 5px;\n\t\t}\t\t\t\n\t\n\t.gauge .delayLoadBtn {\n\t\tborder: 1px solid #008998;\n\t\twidth: 112px;\n\t\tmargin: 25px auto;\n\t\ttext-align: center;\n\t\tpadding: 2px;\n\t\tborder-radius: 6px;\n\t\tcolor: #008998;\n\t\tcursor: pointer;\n\t\tfont-size: 14px;\n\t}\n\n\t\t.gauge .delayLoadBtn:hover {\n\t\t\tcolor: #fff;\n\t\t\tbackground-color: #008998;\n\t\t}\n\n\t.gauge .loader {\n\t\tborder-radius: 50%;\n\t\twidth: 107px;\n\t\theight: 107px;\n\t\tmargin: 5px auto;\n\t\tborder-top: 7px solid #dfe5e6;\n\t\tborder-right: 7px solid #dfe5e6;\n\t\tborder-bottom: 7px solid #dfe5e6;\n\t\tborder-left: 7px solid #3fbfad;\n\t\tanimation: loader 1.7s infinite linear;\n\t}\n\n\t.gauge .textError {\n\t\ttext-align: center;\n\t\tfont-size: 0.9em;\n\t\theight: 3.5em;\n\t\tline-height: 3.5em;\n\t}\n\n.logowrapper {\n\twidth: 150px;\n\theight: 35px;\n\tmargin: 10px auto 10px auto;\n\tbackground-size: contain;\n\tbackground-repeat: no-repeat;\n\tbackground-position: center;\n}\n",
																				"version": 0,
																				"width": "100%"
																			},
																			"stamp": 86
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-29": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 87,
											"*": {
												"CreditApplicationPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_CreditApplicationPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 88,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 89,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CreditApplicationView__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 90,
																	"*": {
																		"CreditApplicationView__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"openInReadOnlyMode": false,
																				"width": "100%",
																				"label": "_CreditApplicationView",
																				"version": 0,
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true
																			},
																			"stamp": 91
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-31": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 92,
											"*": {
												"CreditHistoryPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_CreditHistoryPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 93,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 94,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CreditHistoryView__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 95,
																	"*": {
																		"CreditHistoryView__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"openInReadOnlyMode": false,
																				"width": "100%",
																				"label": "_CreditHistoryView",
																				"version": 0,
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true
																			},
																			"stamp": 96
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-19": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 97,
											"*": {
												"InvoicesPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Invoices",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 98,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 99,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"InvoiceMenu__": {
																				"line": 1,
																				"column": 1
																			},
																			"Invoices__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line15__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 100,
																	"*": {
																		"InvoiceMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_InvoiceMenu",
																				"version": 0,
																				"htmlContent": "<input id=\"onInvoicesLoad\" type=\"hidden\" onclick=\"OnInvoicesLoad(event);\">\n<input id=\"onChangeInvoiceView\" type=\"hidden\" onclick=\"OnChangeInvoiceView(event);\">\n<div id=\"InvoicesMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"allInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link active\">\n\t\t\t\t<span id=\"allInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"overdueInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"overdueInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"outstandingInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"outstandingInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"60daysInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"60daysInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"creditNotesInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"creditNotesInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"disputeInvoices\" onclick=\"OnClickInvoicesTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"disputeInvoicesLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"InvoiceMenu__\" }, document.location.origin);\n\tvar tabEvtInvoicesItems = document.getElementById(\"InvoicesMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickInvoicesTab(tab)\n\t{\n\t\tfor (var i = 0; i < tabEvtInvoicesItems.length; i++)\n\t\t\ttabEvtInvoicesItems[i].className = \"nav-link\";\n\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"InvoiceMenu__\", args: tab.id }, document.location.origin);\n\t}\n\n\tfunction OnChangeInvoiceView(evt)\n\t{\n\t\tfor (var i = 0; i < tabEvtInvoicesItems.length; i++)\n\t\t{\n\t\t\tif (tabEvtInvoicesItems[i].id === evt.viewName)\n\t\t\t{\n\t\t\t\tOnClickInvoicesTab(tabEvtInvoicesItems[i]);\n\t\t\t\treturn;\n\t\t\t}\n\t\t}\n\t}\n\n\tfunction OnInvoicesLoad(evt)\n\t{\n\t\tfor (var tab in evt.tabs)\n\t\t{\n\t\t\tvar spanElem = document.getElementById(tab + \"Lbl\");\n\t\t\tif (spanElem)\n\t\t\t{\n\t\t\t\tspanElem.innerText = evt.tabs[tab].trad;\n\t\t\t}\n\t\t\tvar elem = document.getElementById(tab);\n\t\t\tif (elem && evt.tabs[tab].hidden)\n\t\t\t{\n\t\t\t\telem.style.display = \"none\";\n\t\t\t\telem.parentNode.previousElementSibling.style.display = \"none\";\n\t\t\t}\n\n\t\t}\n\t}\n</script>",
																				"width": "100%"
																			},
																			"stamp": 101
																		},
																		"Invoices__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true,
																				"openInReadOnlyMode": false,
																				"label": "View",
																				"version": 0
																			},
																			"stamp": 102
																		},
																		"Spacer_line15__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line15",
																				"version": 0
																			},
																			"stamp": 103
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-21": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 104,
											"*": {
												"HistoryPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_History",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 105,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"CreationDateTime__": "LabelCreationDateTime__",
																	"LabelCreationDateTime__": "CreationDateTime__",
																	"LastLoginDateTime__": "LabelLastLoginDateTime__",
																	"LabelLastLoginDateTime__": "LastLoginDateTime__",
																	"LastWelcomeEmailSendDate__": "LabelLastWelcomeEmailSendDate__",
																	"LabelLastWelcomeEmailSendDate__": "LastWelcomeEmailSendDate__",
																	"EInvoicingAcceptanceDate__": "LabelEInvoicingAcceptanceDate__",
																	"LabelEInvoicingAcceptanceDate__": "EInvoicingAcceptanceDate__"
																},
																"version": 0
															},
															"stamp": 106,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CreationDateTime__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCreationDateTime__": {
																				"line": 1,
																				"column": 1
																			},
																			"LastLoginDateTime__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelLastLoginDateTime__": {
																				"line": 2,
																				"column": 1
																			},
																			"LastWelcomeEmailSendDate__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelLastWelcomeEmailSendDate__": {
																				"line": 3,
																				"column": 1
																			},
																			"EInvoicingAcceptanceDate__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelEInvoicingAcceptanceDate__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 107,
																	"*": {
																		"LabelCreationDateTime__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_CreationDateTime",
																				"version": 0
																			},
																			"stamp": 108
																		},
																		"CreationDateTime__": {
																			"type": "RealDateTime",
																			"data": [],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_CreationDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Date",
																				"version": 0
																			},
																			"stamp": 109
																		},
																		"LabelLastLoginDateTime__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_LastLoginDateTime",
																				"version": 0
																			},
																			"stamp": 110
																		},
																		"LastLoginDateTime__": {
																			"type": "RealDateTime",
																			"data": [],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_LastLoginDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Date",
																				"version": 0
																			},
																			"stamp": 111
																		},
																		"LabelLastWelcomeEmailSendDate__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_LastWelcomeEmailSendDate",
																				"version": 0
																			},
																			"stamp": 112
																		},
																		"LastWelcomeEmailSendDate__": {
																			"type": "RealDateTime",
																			"data": [],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_LastWelcomeEmailSendDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Date",
																				"version": 0
																			},
																			"stamp": 113
																		},
																		"LabelEInvoicingAcceptanceDate__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_EInvoicingAcceptanceDate",
																				"version": 0
																			},
																			"stamp": 114
																		},
																		"EInvoicingAcceptanceDate__": {
																			"type": "RealDateTime",
																			"data": [],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_EInvoicingAcceptanceDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Date",
																				"version": 0
																			},
																			"stamp": 115
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-23": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 116,
											"*": {
												"ActivityPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Activity",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 117,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 118,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"EventsHistory_NavMenu__": {
																				"line": 1,
																				"column": 1
																			},
																			"EventHistoryView__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 119,
																	"*": {
																		"EventsHistory_NavMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "EventsHistory_NavMenu",
																				"htmlContent": "<input id=\"onEventHistoryLoad\" type=\"hidden\" onclick=\"OnEventHistoryLoad(event);\">\n<div id=\"EventHistoryMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"allEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"allEventsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a href=\"#\" id=\"persoInfEditEvents\" onclick=\"OnClickHistoryTab(this);\" class=\"nav-link active\">\n\t\t\t\t<span id=\"persoInfEditEventsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"actionsEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"actionsEventsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"loginEvents\" onclick=\"OnClickHistoryTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"loginEventsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"EventsHistory_NavMenu__\" }, document.location.origin);\n\tvar tabEvtHistoryItems = document.getElementById(\"EventHistoryMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickHistoryTab(tab)\n\t{\n\t\tfor (var i = 0; i < tabEvtHistoryItems.length; i++)\n\t\t\ttabEvtHistoryItems[i].className = \"nav-link\";\n\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"EventsHistory_NavMenu__\", args: tab.id }, document.location.origin);\n\t}\n\tfunction OnEventHistoryLoad(evt)\n\t{\n\t\tfor (var lbl in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(lbl + \"Lbl\");\n\t\t\tif (elem)\n\t\t\t\telem.innerText = evt.tabs[lbl];\n\t\t}\n\t}\n</script>",
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 120
																		},
																		"EventHistoryView__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": false,
																				"openInReadOnlyMode": true,
																				"label": "View",
																				"version": 0
																			},
																			"stamp": 121
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-25": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 122,
											"*": {
												"GeneralPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_General",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 123,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelCustomerID__": "CustomerID__",
																	"CustomerID__": "LabelCustomerID__",
																	"LabelBusinessPartnerId__": "BusinessPartnerId__",
																	"BusinessPartnerId__": "LabelBusinessPartnerId__",
																	"LabelCustomerIdDisplay__": "CustomerIdDisplay__",
																	"CustomerIdDisplay__": "LabelCustomerIdDisplay__",
																	"ProfileLink__": "LabelProfileLink__",
																	"LabelProfileLink__": "ProfileLink__",
																	"Payer_ID__": "LabelPayer_ID__",
																	"LabelPayer_ID__": "Payer_ID__"
																},
																"version": 0
															},
															"stamp": 124,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelCustomerID__": {
																				"line": 1,
																				"column": 1
																			},
																			"CustomerID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelBusinessPartnerId__": {
																				"line": 3,
																				"column": 1
																			},
																			"BusinessPartnerId__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCustomerIdDisplay__": {
																				"line": 2,
																				"column": 1
																			},
																			"CustomerIdDisplay__": {
																				"line": 2,
																				"column": 2
																			},
																			"ProfileLink__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelProfileLink__": {
																				"line": 4,
																				"column": 1
																			},
																			"Payer_ID__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelPayer_ID__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 125,
																	"*": {
																		"BusinessPartnerId__": {
																			"type": "ShortText",
																			"data": [
																				"BusinessPartnerId__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BusinessPartnerId",
																				"activable": true,
																				"width": "150",
																				"browsable": false,
																				"readonly": true,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 126
																		},
																		"LabelCustomerIdDisplay__": {
																			"type": "Label",
																			"data": [
																				"CustomerIdDisplay__"
																			],
																			"options": {
																				"label": "_CustomerID",
																				"version": 0
																			},
																			"stamp": 127
																		},
																		"LabelBusinessPartnerId__": {
																			"type": "Label",
																			"data": [
																				"BusinessPartnerId__"
																			],
																			"options": {
																				"label": "_BusinessPartnerId",
																				"version": 0
																			},
																			"stamp": 128
																		},
																		"CustomerIdDisplay__": {
																			"type": "ShortText",
																			"data": [
																				"CustomerIdDisplay__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_CustomerID",
																				"activable": true,
																				"width": "150",
																				"browsable": false,
																				"readonly": true,
																				"notInDB": true,
																				"hidden": false,
																				"version": 0
																			},
																			"stamp": 129
																		},
																		"CustomerID__": {
																			"type": "ShortText",
																			"data": [
																				"CustomerID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_CustomerID",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"hidden": true,
																				"browsable": false,
																				"readonly": true,
																				"uniqueIndex": true
																			},
																			"stamp": 130
																		},
																		"LabelCustomerID__": {
																			"type": "Label",
																			"data": [
																				"CustomerID__"
																			],
																			"options": {
																				"label": "_CustomerID",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 131
																		},
																		"LabelProfileLink__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "_Profile",
																				"version": 0
																			},
																			"stamp": 132
																		},
																		"ProfileLink__": {
																			"type": "Link",
																			"data": false,
																			"options": {
																				"version": 1,
																				"link": "",
																				"openInCurrentWindow": true,
																				"label": "_Profile"
																			},
																			"stamp": 133
																		},
																		"LabelPayer_ID__": {
																			"type": "Label",
																			"data": [
																				"Payer_ID__"
																			],
																			"options": {
																				"label": "_Payer ID"
																			},
																			"stamp": 134
																		},
																		"Payer_ID__": {
																			"type": "ShortText",
																			"data": [
																				"Payer_ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Payer ID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true,
																				"readonly": false,
																				"browsable": false,
																				"autocompletable": false,
																				"length": 1024
																			},
																			"stamp": 135
																		}
																	}
																}
															}
														}
													}
												},
												"SecurityPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Security",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 136,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"ResetPassword__": "LabelResetPassword__",
																	"LabelResetPassword__": "ResetPassword__",
																	"Lock__": "LabelLock__",
																	"LabelLock__": "Lock__",
																	"PasswordFree__": "LabelPasswordFree__",
																	"LabelPasswordFree__": "PasswordFree__"
																},
																"version": 0
															},
															"stamp": 137,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ResetPassword__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelResetPassword__": {
																				"line": 3,
																				"column": 1
																			},
																			"Lock__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelLock__": {
																				"line": 2,
																				"column": 1
																			},
																			"PasswordFree__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelPasswordFree__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 138,
																	"*": {
																		"LabelPasswordFree__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_PasswordFree",
																				"version": 0
																			},
																			"stamp": 139
																		},
																		"PasswordFree__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_PasswordFree",
																				"activable": true,
																				"width": "30",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 140
																		},
																		"LabelLock__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Lock",
																				"version": 0
																			},
																			"stamp": 141
																		},
																		"Lock__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_Lock",
																				"activable": true,
																				"width": "30",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 142
																		},
																		"ResetPassword__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_ResetPassword",
																				"label": "",
																				"version": 0,
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "color1",
																				"nextprocess": {
																					"processName": "AR - Application Global Settings__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"style": 6,
																				"width": "",
																				"action": "none",
																				"hidden": false,
																				"url": ""
																			},
																			"stamp": 143
																		},
																		"LabelResetPassword__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 144
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-27": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 145,
											"*": {
												"InvoiceDeliveryPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_InvoiceDelivery",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 146,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"DeliveryMethod__": "LabelDeliveryMethod__",
																	"LabelDeliveryMethod__": "DeliveryMethod__",
																	"LabelEDIPartnerCustomer__": "EDIPartnerCustomer__",
																	"EDIPartnerCustomer__": "LabelEDIPartnerCustomer__",
																	"SendCopyToEmailAddresses__": "LabelSendCopyToEmailAddresses__",
																	"LabelSendCopyToEmailAddresses__": "SendCopyToEmailAddresses__",
																	"PostalMail__": "LabelPostalMail__",
																	"LabelPostalMail__": "PostalMail__",
																	"EmailAttachment__": "LabelEmailAttachment__",
																	"LabelEmailAttachment__": "EmailAttachment__",
																	"Fax__": "LabelFax__",
																	"LabelFax__": "Fax__",
																	"PortalPublication__": "LabelPortalPublication__",
																	"LabelPortalPublication__": "PortalPublication__",
																	"EDI__": "LabelEDI__",
																	"LabelEDI__": "EDI__",
																	"OtherDelivery__": "LabelOtherDelivery__",
																	"LabelOtherDelivery__": "OtherDelivery__",
																	"LabelRequest_validation_for_all_invoices__": "Request_validation_for_all_invoices__",
																	"Request_validation_for_all_invoices__": "LabelRequest_validation_for_all_invoices__",
																	"LabelNbAdditionalModCopies__": "NbAdditionalModCopies__",
																	"NbAdditionalModCopies__": "LabelNbAdditionalModCopies__",
																	"LabelCustomer_AP_portal__": "Customer_AP_portal__",
																	"Customer_AP_portal__": "LabelCustomer_AP_portal__",
																	"DoNotGroupEmails__": "LabelDoNotGroupEmails__",
																	"LabelDoNotGroupEmails__": "DoNotGroupEmails__",
																	"Options__": "LabelOptions__",
																	"LabelOptions__": "Options__",
																	"Disable_ESignature__": "LabelDisable_ESignature__",
																	"LabelDisable_ESignature__": "Disable_ESignature__"
																},
																"version": 0
															},
															"stamp": 147,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DeliveryMethod__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDeliveryMethod__": {
																				"line": 1,
																				"column": 1
																			},
																			"LabelEDIPartnerCustomer__": {
																				"line": 2,
																				"column": 1
																			},
																			"EDIPartnerCustomer__": {
																				"line": 2,
																				"column": 2
																			},
																			"SendCopyToEmailAddresses__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelSendCopyToEmailAddresses__": {
																				"line": 3,
																				"column": 1
																			},
																			"PostalMail__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelPostalMail__": {
																				"line": 8,
																				"column": 1
																			},
																			"EmailAttachment__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelEmailAttachment__": {
																				"line": 6,
																				"column": 1
																			},
																			"Fax__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelFax__": {
																				"line": 7,
																				"column": 1
																			},
																			"PortalPublication__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelPortalPublication__": {
																				"line": 9,
																				"column": 1
																			},
																			"EDI__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelEDI__": {
																				"line": 10,
																				"column": 1
																			},
																			"OtherDelivery__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelOtherDelivery__": {
																				"line": 11,
																				"column": 1
																			},
																			"LabelRequest_validation_for_all_invoices__": {
																				"line": 14,
																				"column": 1
																			},
																			"Request_validation_for_all_invoices__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelNbAdditionalModCopies__": {
																				"line": 17,
																				"column": 1
																			},
																			"NbAdditionalModCopies__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelCustomer_AP_portal__": {
																				"line": 12,
																				"column": 1
																			},
																			"Customer_AP_portal__": {
																				"line": 12,
																				"column": 2
																			},
																			"Spacer_line6__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer_line7__": {
																				"line": 13,
																				"column": 1
																			},
																			"DeliveryMethodAllowed__": {
																				"line": 5,
																				"column": 1
																			},
																			"DoNotGroupEmails__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelDoNotGroupEmails__": {
																				"line": 15,
																				"column": 1
																			},
																			"Spacer_line18__": {
																				"line": 18,
																				"column": 1
																			},
																			"Options__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelOptions__": {
																				"line": 19,
																				"column": 1
																			},
																			"Disable_ESignature__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelDisable_ESignature__": {
																				"line": 16,
																				"column": 1
																			}
																		},
																		"lines": 19,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 148,
																	"*": {
																		"NbAdditionalModCopies__": {
																			"type": "Integer",
																			"data": [
																				"NbAdditionalModCopies__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_NbAdditionalModCopies",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "50",
																				"defaultValue": "0",
																				"browsable": false
																			},
																			"stamp": 149
																		},
																		"LabelNbAdditionalModCopies__": {
																			"type": "Label",
																			"data": [
																				"NbAdditionalModCopies__"
																			],
																			"options": {
																				"label": "_NbAdditionalModCopies",
																				"version": 0
																			},
																			"stamp": 150
																		},
																		"Request_validation_for_all_invoices__": {
																			"type": "CheckBox",
																			"data": [
																				"Request_validation_for_all_invoices__"
																			],
																			"options": {
																				"label": "_Request validation for all invoices",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 151
																		},
																		"LabelRequest_validation_for_all_invoices__": {
																			"type": "Label",
																			"data": [
																				"Request_validation_for_all_invoices__"
																			],
																			"options": {
																				"label": "_Request validation for all invoices",
																				"version": 0
																			},
																			"stamp": 152
																		},
																		"EDIPartnerCustomer__": {
																			"type": "ComboBox",
																			"data": [
																				"EDIPartnerCustomer__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "ariba",
																					"2": "bavel",
																					"3": "b-process",
																					"4": "cegedim",
																					"5": "coupa",
																					"6": "edicom",
																					"7": "edt",
																					"8": "generix",
																					"9": "obs",
																					"10": "prologue",
																					"11": "seres",
																					"12": "direct-commerce",
																					"13": "pec",
																					"14": "intecon",
																					"15": "tradeshift",
																					"16": "basware",
																					"17": "io-market",
																					"18": "osakidetza",
																					"19": "pagero",
																					"20": "saphety",
																					"21": "esker-edi-services",
																					"22": "e-integration",
																					"23": "ob10",
																					"24": "tenor",
																					"25": "peppol",
																					"26": "peppol-3.0",
																					"27": "peppol-sg",
																					"28": "peppol-hr",
																					"29": "peppol-anz",
																					"30": "austria-pa",
																					"31": "belgium-pa",
																					"32": "england-pa",
																					"33": "denmark-pa",
																					"34": "germany-pa",
																					"35": "germany-pa-email",
																					"36": "germany-pa-bbw",
																					"37": "netherlands-pa",
																					"38": "chorus-pro",
																					"39": "italian-pa",
																					"40": "italian-b2b",
																					"41": "luxembourg-pa",
																					"42": "norway-pa",
																					"43": "poland-pa",
																					"44": "spanish-pa",
																					"45": "spanish-b2b",
																					"46": "sweden-pa",
																					"47": "swiss-pa"
																				},
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "EMPTY",
																					"1": "ariba",
																					"2": "bavel",
																					"3": "b-process",
																					"4": "cegedim",
																					"5": "coupa",
																					"6": "edicom",
																					"7": "edt",
																					"8": "generix",
																					"9": "obs",
																					"10": "prologue",
																					"11": "seres",
																					"12": "direct-commerce",
																					"13": "pec",
																					"14": "intecon",
																					"15": "tradeshift",
																					"16": "basware",
																					"17": "io-market",
																					"18": "osakidetza",
																					"19": "pagero",
																					"20": "saphety",
																					"21": "esker-edi-services",
																					"22": "e-integration",
																					"23": "ob10",
																					"24": "tenor",
																					"25": "peppol",
																					"26": "peppol-3.0",
																					"27": "peppol-sg",
																					"28": "peppol-hr",
																					"29": "peppol-anz",
																					"30": "austria-pa",
																					"31": "belgium-pa",
																					"32": "england-pa",
																					"33": "denmark-pa",
																					"34": "germany-pa",
																					"35": "germany-pa-email",
																					"36": "germany-pa-bbw",
																					"37": "netherlands-pa",
																					"38": "chorus-pro",
																					"39": "italian-pa",
																					"40": "italian-b2b",
																					"41": "luxembourg-pa",
																					"42": "norway-pa",
																					"43": "poland-pa",
																					"44": "spanish-pa",
																					"45": "spanish-b2b",
																					"46": "sweden-pa",
																					"47": "swiss-pa"
																				},
																				"label": "_Delivery system",
																				"activable": true,
																				"width": "230",
																				"readonly": false,
																				"version": 0
																			},
																			"stamp": 153
																		},
																		"LabelEDIPartnerCustomer__": {
																			"type": "Label",
																			"data": [
																				"EDIPartnerCustomer__"
																			],
																			"options": {
																				"label": "_Delivery system",
																				"version": 0
																			},
																			"stamp": 154
																		},
																		"Spacer_line6__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line6",
																				"version": 0
																			},
																			"stamp": 155
																		},
																		"DeliveryMethodAllowed__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"label": "_DeliveryMethodAllowed",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 156
																		},
																		"LabelDeliveryMethod__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_DeliveryMethod",
																				"version": 0
																			},
																			"stamp": 157
																		},
																		"DeliveryMethod__": {
																			"type": "ComboBox",
																			"data": [],
																			"options": {
																				"possibleValues": {
																					"0": "_DeliveryDefault",
																					"1": "_DeliveryEmail",
																					"2": "_DeliveryFax",
																					"3": "_DeliveryMOD",
																					"4": "_DeliveryPortal",
																					"5": "_DeliveryEDI",
																					"6": "_DeliveryAPPortal",
																					"7": "_DeliveryOther",
																					"8": "_DeliveryNone"
																				},
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "DeliveryMethod_DEFAULT",
																					"1": "SM",
																					"2": "FGFAXOUT",
																					"3": "MOD",
																					"4": "PORTAL",
																					"5": "EDI",
																					"6": "EXTERNALPORTAL",
																					"7": "OTHER",
																					"8": "NONE"
																				},
																				"version": 1,
																				"label": "_DeliveryMethod",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"notInDB": true,
																				"dataType": "String",
																				"readonly": true
																			},
																			"stamp": 158
																		},
																		"LabelSendCopyToEmailAddresses__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_SendCopyToEmailAddresses",
																				"version": 0
																			},
																			"stamp": 159
																		},
																		"SendCopyToEmailAddresses__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SendCopyToEmailAddresses",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 160
																		},
																		"LabelEmailAttachment__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_EmailAttachment",
																				"version": 0
																			},
																			"stamp": 161
																		},
																		"EmailAttachment__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_EmailAttachment",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 162
																		},
																		"LabelFax__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Fax",
																				"version": 0
																			},
																			"stamp": 163
																		},
																		"Fax__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_Fax",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 164
																		},
																		"LabelPostalMail__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_PostalMail",
																				"version": 0
																			},
																			"stamp": 165
																		},
																		"PostalMail__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_PostalMail",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 166
																		},
																		"LabelPortalPublication__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_PortalPublication",
																				"version": 0
																			},
																			"stamp": 167
																		},
																		"LabelCustomer_AP_portal__": {
																			"type": "Label",
																			"data": [
																				"Customer_AP_portal__"
																			],
																			"options": {
																				"label": "_Customer AP portal",
																				"version": 0
																			},
																			"stamp": 168
																		},
																		"PortalPublication__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_PortalPublication",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 169
																		},
																		"Customer_AP_portal__": {
																			"type": "ComboBox",
																			"data": [
																				"Customer_AP_portal__"
																			],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_Customer AP portal",
																				"activable": true,
																				"width": "360",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 170
																		},
																		"Spacer_line7__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line7",
																				"version": 0
																			},
																			"stamp": 171
																		},
																		"LabelEDI__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_EDI",
																				"version": 0
																			},
																			"stamp": 172
																		},
																		"EDI__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_EDI",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 173
																		},
																		"LabelDoNotGroupEmails__": {
																			"type": "Label",
																			"data": [
																				"DoNotGroupEmails__"
																			],
																			"options": {
																				"label": "_Do not group emails",
																				"version": 0
																			},
																			"stamp": 174
																		},
																		"DoNotGroupEmails__": {
																			"type": "CheckBox",
																			"data": [
																				"DoNotGroupEmails__"
																			],
																			"options": {
																				"label": "_Do not group emails",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 175
																		},
																		"LabelDisable_ESignature__": {
																			"type": "Label",
																			"data": [
																				"Disable_ESignature__"
																			],
																			"options": {
																				"label": "_Disable_ESignature"
																			},
																			"stamp": 176
																		},
																		"Disable_ESignature__": {
																			"type": "CheckBox",
																			"data": [
																				"Disable_ESignature__"
																			],
																			"options": {
																				"label": "_Disable_ESignature",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 177
																		},
																		"LabelOtherDelivery__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_OtherDelivery",
																				"version": 0
																			},
																			"stamp": 178
																		},
																		"OtherDelivery__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_OtherDelivery",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 179
																		},
																		"Spacer_line18__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line18",
																				"version": 0
																			},
																			"stamp": 180
																		},
																		"LabelOptions__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Specific options",
																				"version": 0
																			},
																			"stamp": 181
																		},
																		"Options__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_Specific options",
																				"activable": true,
																				"width": "350",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 182
																		}
																	}
																}
															}
														}
													}
												},
												"CoupaCustomerInformationPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Coupa Customer Information Pane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 542,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Coupa_Customer_Domain__": "LabelCoupa_Customer_Domain__",
																	"LabelCoupa_Customer_Domain__": "Coupa_Customer_Domain__",
																	"Coupa_Customer_Id__": "LabelCoupa_Customer_Id__",
																	"LabelCoupa_Customer_Id__": "Coupa_Customer_Id__",
																	"Coupa_Supplier_Domain__": "LabelCoupa_Supplier_Domain__",
																	"LabelCoupa_Supplier_Domain__": "Coupa_Supplier_Domain__",
																	"Coupa_Supplier_Id__": "LabelCoupa_Supplier_Id__",
																	"LabelCoupa_Supplier_Id__": "Coupa_Supplier_Id__",
																	"Coupa_Customer_Password__": "LabelCoupa_Customer_Password__",
																	"LabelCoupa_Customer_Password__": "Coupa_Customer_Password__",
																	"Coupa_Customer_URL_Domain__": "LabelCoupa_Customer_URL_Domain__",
																	"LabelCoupa_Customer_URL_Domain__": "Coupa_Customer_URL_Domain__",
																	"Coupa_Customer_Contract_Id__": "LabelCoupa_Customer_Contract_Id__",
																	"LabelCoupa_Customer_Contract_Id__": "Coupa_Customer_Contract_Id__"
																},
																"version": 0
															},
															"stamp": 543,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Coupa_Customer_Domain__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCoupa_Customer_Domain__": {
																				"line": 2,
																				"column": 1
																			},
																			"Coupa_Customer_Id__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCoupa_Customer_Id__": {
																				"line": 3,
																				"column": 1
																			},
																			"Coupa_Supplier_Domain__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCoupa_Supplier_Domain__": {
																				"line": 4,
																				"column": 1
																			},
																			"Coupa_Supplier_Id__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCoupa_Supplier_Id__": {
																				"line": 5,
																				"column": 1
																			},
																			"Coupa_Customer_Password__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCoupa_Customer_Password__": {
																				"line": 6,
																				"column": 1
																			},
																			"Coupa_Customer_URL_Domain__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCoupa_Customer_URL_Domain__": {
																				"line": 1,
																				"column": 1
																			},
																			"Coupa_Customer_Contract_Id__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelCoupa_Customer_Contract_Id__": {
																				"line": 7,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 544,
																	"*": {
																		"LabelCoupa_Customer_URL_Domain__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Customer_URL_Domain__"
																			],
																			"options": {
																				"label": "_Coupa Customer URL Domain",
																				"version": 0
																			},
																			"stamp": 560
																		},
																		"Coupa_Customer_URL_Domain__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Customer_URL_Domain__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Customer URL Domain",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Coupa Customer URL Domain Tooltip",
																				"helpURL": "1851",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 561
																		},
																		"LabelCoupa_Customer_Domain__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Customer_Domain__"
																			],
																			"options": {
																				"label": "_Coupa Customer Domain",
																				"version": 0
																			},
																			"stamp": 550
																		},
																		"Coupa_Customer_Domain__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Customer_Domain__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Customer Domain",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Coupa Customer Domain Tooltip",
																				"helpURL": "1851",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 551
																		},
																		"LabelCoupa_Customer_Id__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Customer_Id__"
																			],
																			"options": {
																				"label": "_Coupa Customer Id",
																				"version": 0
																			},
																			"stamp": 552
																		},
																		"Coupa_Customer_Id__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Customer_Id__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Customer Id",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Coupa Customer Id Tooltip",
																				"helpURL": "1851",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 553
																		},
																		"LabelCoupa_Supplier_Domain__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Supplier_Domain__"
																			],
																			"options": {
																				"label": "_Coupa Supplier Domain",
																				"version": 0
																			},
																			"stamp": 554
																		},
																		"Coupa_Supplier_Domain__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Supplier_Domain__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Supplier Domain",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 555
																		},
																		"LabelCoupa_Supplier_Id__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Supplier_Id__"
																			],
																			"options": {
																				"label": "_Coupa Supplier Id",
																				"version": 0
																			},
																			"stamp": 556
																		},
																		"Coupa_Supplier_Id__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Supplier_Id__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Supplier Id",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 557
																		},
																		"LabelCoupa_Customer_Password__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Customer_Password__"
																			],
																			"options": {
																				"label": "_Coupa Customer Secret",
																				"version": 0
																			},
																			"stamp": 558
																		},
																		"Coupa_Customer_Password__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Customer_Password__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Customer Secret",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 559
																		},
																		"LabelCoupa_Customer_Contract_Id__": {
																			"type": "Label",
																			"data": [
																				"Coupa_Customer_Contract_Id__"
																			],
																			"options": {
																				"label": "_Coupa Customer Contract Id",
																				"version": 0
																			},
																			"stamp": 560
																		},
																		"Coupa_Customer_Contract_Id__": {
																			"type": "ShortText",
																			"data": [
																				"Coupa_Customer_Contract_Id__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Coupa Customer Contract Id",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 561
																		}
																	}
																}
															}
														}
													}
												},
												"FACEPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_FACE Pane",
														"leftImageURL": "",
														"version": 0,
														"hideTitle": false,
														"panelStyle": "FlexibleFormPanelLight",
														"removeMargins": false,
														"hidden": true,
														"elementsAlignment": "left"
													},
													"stamp": 183,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"FACE_CenterID_1__": "LabelFACE_CenterID_1__",
																	"LabelFACE_CenterID_1__": "FACE_CenterID_1__",
																	"FACE_CenterName_1__": "LabelFACE_CenterName_1__",
																	"LabelFACE_CenterName_1__": "FACE_CenterName_1__",
																	"FACE_CenterBuilding_1__": "LabelFACE_CenterBuilding_1__",
																	"LabelFACE_CenterBuilding_1__": "FACE_CenterBuilding_1__",
																	"FACE_CenterStreet_1__": "LabelFACE_CenterStreet_1__",
																	"LabelFACE_CenterStreet_1__": "FACE_CenterStreet_1__",
																	"FACE_CenterPOBox_1__": "LabelFACE_CenterPOBox_1__",
																	"LabelFACE_CenterPOBox_1__": "FACE_CenterPOBox_1__",
																	"FACE_CenterZipCode_1__": "LabelFACE_CenterZipCode_1__",
																	"LabelFACE_CenterZipCode_1__": "FACE_CenterZipCode_1__",
																	"FACE_CenterCity_1__": "LabelFACE_CenterCity_1__",
																	"LabelFACE_CenterCity_1__": "FACE_CenterCity_1__",
																	"FACE_CenterState_1__": "LabelFACE_CenterState_1__",
																	"LabelFACE_CenterState_1__": "FACE_CenterState_1__",
																	"FACE_CenterCountry_1__": "LabelFACE_CenterCountry_1__",
																	"LabelFACE_CenterCountry_1__": "FACE_CenterCountry_1__",
																	"FACE_CenterID_3__": "LabelFACE_CenterID_3__",
																	"LabelFACE_CenterID_3__": "FACE_CenterID_3__",
																	"FACE_CenterName_3__": "LabelFACE_CenterName_3__",
																	"LabelFACE_CenterName_3__": "FACE_CenterName_3__",
																	"FACE_CenterBuilding_2__": "LabelFACE_CenterBuilding_2__",
																	"LabelFACE_CenterBuilding_2__": "FACE_CenterBuilding_2__",
																	"FACE_CenterBuilding_3__": "LabelFACE_CenterBuilding_3__",
																	"LabelFACE_CenterBuilding_3__": "FACE_CenterBuilding_3__",
																	"FACE_CenterStreet_2__": "LabelFACE_CenterStreet_2__",
																	"LabelFACE_CenterStreet_2__": "FACE_CenterStreet_2__",
																	"FACE_CenterStreet_3__": "LabelFACE_CenterStreet_3__",
																	"LabelFACE_CenterStreet_3__": "FACE_CenterStreet_3__",
																	"FACE_CenterPOBox_2__": "LabelFACE_CenterPOBox_2__",
																	"LabelFACE_CenterPOBox_2__": "FACE_CenterPOBox_2__",
																	"FACE_CenterPOBox_3__": "LabelFACE_CenterPOBox_3__",
																	"LabelFACE_CenterPOBox_3__": "FACE_CenterPOBox_3__",
																	"FACE_CenterZipCode_2__": "LabelFACE_CenterZipCode_2__",
																	"LabelFACE_CenterZipCode_2__": "FACE_CenterZipCode_2__",
																	"FACE_CenterZipCode_3__": "LabelFACE_CenterZipCode_3__",
																	"LabelFACE_CenterZipCode_3__": "FACE_CenterZipCode_3__",
																	"FACE_CenterCity_2__": "LabelFACE_CenterCity_2__",
																	"LabelFACE_CenterCity_2__": "FACE_CenterCity_2__",
																	"FACE_CenterCity_3__": "LabelFACE_CenterCity_3__",
																	"LabelFACE_CenterCity_3__": "FACE_CenterCity_3__",
																	"FACE_CenterState_2__": "LabelFACE_CenterState_2__",
																	"LabelFACE_CenterState_2__": "FACE_CenterState_2__",
																	"FACE_CenterState_3__": "LabelFACE_CenterState_3__",
																	"LabelFACE_CenterState_3__": "FACE_CenterState_3__",
																	"FACE_CenterCountry_2__": "LabelFACE_CenterCountry_2__",
																	"LabelFACE_CenterCountry_2__": "FACE_CenterCountry_2__",
																	"FACE_CenterCountry_3__": "LabelFACE_CenterCountry_3__",
																	"LabelFACE_CenterCountry_3__": "FACE_CenterCountry_3__",
																	"FACE_CenterID_2__": "LabelFACE_CenterID_2__",
																	"LabelFACE_CenterID_2__": "FACE_CenterID_2__",
																	"FACE_CenterName_2__": "LabelFACE_CenterName_2__",
																	"LabelFACE_CenterName_2__": "FACE_CenterName_2__",
																	"FACE_VAT_ID__": "LabelFACE_VAT_ID__",
																	"LabelFACE_VAT_ID__": "FACE_VAT_ID__"
																},
																"version": 0
															},
															"stamp": 184,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Spacer_line__": {
																				"line": 13,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 25,
																				"column": 1
																			},
																			"Second_administrative_center__": {
																				"line": 14,
																				"column": 1
																			},
																			"Third_administrative_center__": {
																				"line": 26,
																				"column": 1
																			},
																			"Spacer_line3__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer_line4__": {
																				"line": 16,
																				"column": 1
																			},
																			"Spacer_line5__": {
																				"line": 28,
																				"column": 1
																			},
																			"FACE_CenterID_1__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelFACE_CenterID_1__": {
																				"line": 3,
																				"column": 1
																			},
																			"FACE_CenterName_1__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelFACE_CenterName_1__": {
																				"line": 5,
																				"column": 1
																			},
																			"FACE_CenterBuilding_1__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelFACE_CenterBuilding_1__": {
																				"line": 6,
																				"column": 1
																			},
																			"FACE_CenterStreet_1__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelFACE_CenterStreet_1__": {
																				"line": 7,
																				"column": 1
																			},
																			"FACE_CenterPOBox_1__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelFACE_CenterPOBox_1__": {
																				"line": 8,
																				"column": 1
																			},
																			"FACE_CenterZipCode_1__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelFACE_CenterZipCode_1__": {
																				"line": 9,
																				"column": 1
																			},
																			"FACE_CenterCity_1__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelFACE_CenterCity_1__": {
																				"line": 10,
																				"column": 1
																			},
																			"FACE_CenterState_1__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelFACE_CenterState_1__": {
																				"line": 11,
																				"column": 1
																			},
																			"FACE_CenterCountry_1__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelFACE_CenterCountry_1__": {
																				"line": 12,
																				"column": 1
																			},
																			"FACE_CenterID_3__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelFACE_CenterID_3__": {
																				"line": 27,
																				"column": 1
																			},
																			"FACE_CenterName_3__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelFACE_CenterName_3__": {
																				"line": 29,
																				"column": 1
																			},
																			"FACE_CenterBuilding_2__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelFACE_CenterBuilding_2__": {
																				"line": 18,
																				"column": 1
																			},
																			"FACE_CenterBuilding_3__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelFACE_CenterBuilding_3__": {
																				"line": 30,
																				"column": 1
																			},
																			"FACE_CenterStreet_2__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelFACE_CenterStreet_2__": {
																				"line": 19,
																				"column": 1
																			},
																			"FACE_CenterStreet_3__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelFACE_CenterStreet_3__": {
																				"line": 31,
																				"column": 1
																			},
																			"FACE_CenterPOBox_2__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelFACE_CenterPOBox_2__": {
																				"line": 20,
																				"column": 1
																			},
																			"FACE_CenterPOBox_3__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelFACE_CenterPOBox_3__": {
																				"line": 32,
																				"column": 1
																			},
																			"FACE_CenterZipCode_2__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelFACE_CenterZipCode_2__": {
																				"line": 21,
																				"column": 1
																			},
																			"FACE_CenterZipCode_3__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelFACE_CenterZipCode_3__": {
																				"line": 33,
																				"column": 1
																			},
																			"FACE_CenterCity_2__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelFACE_CenterCity_2__": {
																				"line": 22,
																				"column": 1
																			},
																			"FACE_CenterCity_3__": {
																				"line": 34,
																				"column": 2
																			},
																			"LabelFACE_CenterCity_3__": {
																				"line": 34,
																				"column": 1
																			},
																			"FACE_CenterState_2__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelFACE_CenterState_2__": {
																				"line": 23,
																				"column": 1
																			},
																			"FACE_CenterState_3__": {
																				"line": 35,
																				"column": 2
																			},
																			"LabelFACE_CenterState_3__": {
																				"line": 35,
																				"column": 1
																			},
																			"FACE_CenterCountry_2__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelFACE_CenterCountry_2__": {
																				"line": 24,
																				"column": 1
																			},
																			"FACE_CenterCountry_3__": {
																				"line": 36,
																				"column": 2
																			},
																			"LabelFACE_CenterCountry_3__": {
																				"line": 36,
																				"column": 1
																			},
																			"First_administrative_center__": {
																				"line": 2,
																				"column": 1
																			},
																			"FACE_CenterID_2__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelFACE_CenterID_2__": {
																				"line": 15,
																				"column": 1
																			},
																			"FACE_CenterName_2__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelFACE_CenterName_2__": {
																				"line": 17,
																				"column": 1
																			},
																			"FACE_VAT_ID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelFACE_VAT_ID__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 36,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 185,
																	"*": {
																		"LabelFACE_VAT_ID__": {
																			"type": "Label",
																			"data": [
																				"FACE_VAT_ID__"
																			],
																			"options": {
																				"label": "_FACE_VAT_ID",
																				"version": 0
																			},
																			"stamp": 186
																		},
																		"FACE_VAT_ID__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_VAT_ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_VAT_ID",
																				"activable": true,
																				"width": "260",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 187
																		},
																		"First_administrative_center__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_First administrative center",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 188
																		},
																		"LabelFACE_CenterID_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterID_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterID_1",
																				"version": 0
																			},
																			"stamp": 189
																		},
																		"FACE_CenterID_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterID_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterID_1",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false,
																				"length": 100
																			},
																			"stamp": 190
																		},
																		"Spacer_line3__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "8",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line3",
																				"version": 0
																			},
																			"stamp": 191
																		},
																		"LabelFACE_CenterName_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterName_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterName_1",
																				"version": 0
																			},
																			"stamp": 192
																		},
																		"FACE_CenterName_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterName_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterName_1",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false,
																				"length": 100
																			},
																			"stamp": 193
																		},
																		"LabelFACE_CenterBuilding_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterBuilding_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterBuilding_1",
																				"version": 0
																			},
																			"stamp": 194
																		},
																		"FACE_CenterBuilding_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterBuilding_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterBuilding_1",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 195
																		},
																		"LabelFACE_CenterStreet_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterStreet_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterStreet_1",
																				"version": 0
																			},
																			"stamp": 196
																		},
																		"FACE_CenterStreet_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterStreet_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterStreet_1",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 197
																		},
																		"LabelFACE_CenterPOBox_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterPOBox_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterPOBox_1",
																				"version": 0
																			},
																			"stamp": 198
																		},
																		"FACE_CenterPOBox_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterPOBox_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterPOBox_1",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 199
																		},
																		"LabelFACE_CenterZipCode_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterZipCode_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterZipCode_1",
																				"version": 0
																			},
																			"stamp": 200
																		},
																		"FACE_CenterZipCode_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterZipCode_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterZipCode_1",
																				"activable": true,
																				"width": "80",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 201
																		},
																		"LabelFACE_CenterCity_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCity_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterCity_1",
																				"version": 0
																			},
																			"stamp": 202
																		},
																		"FACE_CenterCity_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCity_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCity_1",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 203
																		},
																		"LabelFACE_CenterState_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterState_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterState_1",
																				"version": 0
																			},
																			"stamp": 204
																		},
																		"FACE_CenterState_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterState_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterState_1",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 205
																		},
																		"LabelFACE_CenterCountry_1__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCountry_1__"
																			],
																			"options": {
																				"label": "_FACE_CenterCountry_1",
																				"version": 0
																			},
																			"stamp": 206
																		},
																		"FACE_CenterCountry_1__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCountry_1__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCountry_1",
																				"activable": true,
																				"width": "160",
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false
																			},
																			"stamp": 207
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 208
																		},
																		"Second_administrative_center__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Second administrative center",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 209
																		},
																		"LabelFACE_CenterID_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterID_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterID_2",
																				"version": 0
																			},
																			"stamp": 210
																		},
																		"FACE_CenterID_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterID_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterID_2",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"length": 100,
																				"autocompletable": false
																			},
																			"stamp": 211
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "8",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line4",
																				"version": 0
																			},
																			"stamp": 212
																		},
																		"LabelFACE_CenterName_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterName_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterName_2",
																				"version": 0
																			},
																			"stamp": 213
																		},
																		"FACE_CenterName_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterName_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterName_2",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false,
																				"length": 100,
																				"autocompletable": false
																			},
																			"stamp": 214
																		},
																		"LabelFACE_CenterBuilding_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterBuilding_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterBuilding_2",
																				"version": 0
																			},
																			"stamp": 215
																		},
																		"FACE_CenterBuilding_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterBuilding_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterBuilding_2",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 216
																		},
																		"LabelFACE_CenterStreet_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterStreet_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterStreet_2",
																				"version": 0
																			},
																			"stamp": 217
																		},
																		"FACE_CenterStreet_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterStreet_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterStreet_2",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 218
																		},
																		"LabelFACE_CenterPOBox_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterPOBox_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterPOBox_2",
																				"version": 0
																			},
																			"stamp": 219
																		},
																		"FACE_CenterPOBox_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterPOBox_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterPOBox_2",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 220
																		},
																		"LabelFACE_CenterZipCode_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterZipCode_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterZipCode_2",
																				"version": 0
																			},
																			"stamp": 221
																		},
																		"FACE_CenterZipCode_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterZipCode_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterZipCode_2",
																				"activable": true,
																				"width": "80",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 222
																		},
																		"LabelFACE_CenterCity_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCity_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterCity_2",
																				"version": 0
																			},
																			"stamp": 223
																		},
																		"FACE_CenterCity_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCity_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCity_2",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 224
																		},
																		"LabelFACE_CenterState_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterState_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterState_2",
																				"version": 0
																			},
																			"stamp": 225
																		},
																		"FACE_CenterState_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterState_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterState_2",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 226
																		},
																		"LabelFACE_CenterCountry_2__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCountry_2__"
																			],
																			"options": {
																				"label": "_FACE_CenterCountry_2",
																				"version": 0
																			},
																			"stamp": 227
																		},
																		"FACE_CenterCountry_2__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCountry_2__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCountry_2",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 228
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line2",
																				"version": 0
																			},
																			"stamp": 229
																		},
																		"Third_administrative_center__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Third administrative center",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 230
																		},
																		"LabelFACE_CenterID_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterID_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterID_3",
																				"version": 0
																			},
																			"stamp": 231
																		},
																		"FACE_CenterID_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterID_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterID_3",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"length": 100,
																				"autocompletable": false
																			},
																			"stamp": 232
																		},
																		"Spacer_line5__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "8",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line5",
																				"version": 0
																			},
																			"stamp": 233
																		},
																		"LabelFACE_CenterName_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterName_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterName_3",
																				"version": 0
																			},
																			"stamp": 234
																		},
																		"FACE_CenterName_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterName_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterName_3",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"length": 100,
																				"autocompletable": false
																			},
																			"stamp": 235
																		},
																		"LabelFACE_CenterBuilding_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterBuilding_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterBuilding_3",
																				"version": 0
																			},
																			"stamp": 236
																		},
																		"FACE_CenterBuilding_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterBuilding_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterBuilding_3",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 237
																		},
																		"LabelFACE_CenterStreet_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterStreet_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterStreet_3",
																				"version": 0
																			},
																			"stamp": 238
																		},
																		"FACE_CenterStreet_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterStreet_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterStreet_3",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 239
																		},
																		"LabelFACE_CenterPOBox_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterPOBox_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterPOBox_3",
																				"version": 0
																			},
																			"stamp": 240
																		},
																		"FACE_CenterPOBox_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterPOBox_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterPOBox_3",
																				"activable": true,
																				"width": 260,
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 241
																		},
																		"LabelFACE_CenterZipCode_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterZipCode_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterZipCode_3",
																				"version": 0
																			},
																			"stamp": 242
																		},
																		"FACE_CenterZipCode_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterZipCode_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterZipCode_3",
																				"activable": true,
																				"width": "80",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 243
																		},
																		"LabelFACE_CenterCity_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCity_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterCity_3",
																				"version": 0
																			},
																			"stamp": 244
																		},
																		"FACE_CenterCity_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCity_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCity_3",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 245
																		},
																		"LabelFACE_CenterState_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterState_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterState_3",
																				"version": 0
																			},
																			"stamp": 246
																		},
																		"FACE_CenterState_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterState_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterState_3",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 247
																		},
																		"LabelFACE_CenterCountry_3__": {
																			"type": "Label",
																			"data": [
																				"FACE_CenterCountry_3__"
																			],
																			"options": {
																				"label": "_FACE_CenterCountry_3",
																				"version": 0
																			},
																			"stamp": 248
																		},
																		"FACE_CenterCountry_3__": {
																			"type": "ShortText",
																			"data": [
																				"FACE_CenterCountry_3__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_FACE_CenterCountry_3",
																				"activable": true,
																				"width": "160",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 249
																		}
																	}
																}
															}
														}
													}
												},
												"ChorusPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Chorus",
														"leftImageURL": "",
														"version": 0,
														"hideTitle": false,
														"panelStyle": "FlexibleFormPanelLight",
														"removeMargins": false,
														"hidden": true,
														"elementsAlignment": "left"
													},
													"stamp": 250,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"CHORUS_CodeServiceExecutant__": "LabelCHORUS_CodeServiceExecutant__",
																	"LabelCHORUS_CodeServiceExecutant__": "CHORUS_CodeServiceExecutant__",
																	"Chorus_Company_Name__": "LabelChorus_Company_Name__",
																	"LabelChorus_Company_Name__": "Chorus_Company_Name__",
																	"Chorus_Address__": "LabelChorus_Address__",
																	"LabelChorus_Address__": "Chorus_Address__",
																	"Chorus_Additional_Address__": "LabelChorus_Additional_Address__",
																	"LabelChorus_Additional_Address__": "Chorus_Additional_Address__",
																	"Chorus_Postal_Code__": "LabelChorus_Postal_Code__",
																	"LabelChorus_Postal_Code__": "Chorus_Postal_Code__",
																	"Chorus_City__": "LabelChorus_City__",
																	"LabelChorus_City__": "Chorus_City__",
																	"Chorus_Phone__": "LabelChorus_Phone__",
																	"LabelChorus_Phone__": "Chorus_Phone__",
																	"Chorus_Structure_Identifier__": "LabelChorus_Structure_Identifier__",
																	"LabelChorus_Structure_Identifier__": "Chorus_Structure_Identifier__",
																	"Chorus_EJ_Number_Required__": "LabelChorus_EJ_Number_Required__",
																	"LabelChorus_EJ_Number_Required__": "Chorus_EJ_Number_Required__",
																	"Chorus_EJ_Or_Service_Code_Required__": "LabelChorus_EJ_Or_Service_Code_Required__",
																	"LabelChorus_EJ_Or_Service_Code_Required__": "Chorus_EJ_Or_Service_Code_Required__",
																	"Chorus_Payment__": "LabelChorus_Payment__",
																	"LabelChorus_Payment__": "Chorus_Payment__",
																	"Chorus_Executing_Service_Code_Required__": "LabelChorus_Executing_Service_Code_Required__",
																	"LabelChorus_Executing_Service_Code_Required__": "Chorus_Executing_Service_Code_Required__",
																	"Chorus_Service_Identifier__": "LabelChorus_Service_Identifier__",
																	"LabelChorus_Service_Identifier__": "Chorus_Service_Identifier__",
																	"Chorus_Service_Name__": "LabelChorus_Service_Name__",
																	"LabelChorus_Service_Name__": "Chorus_Service_Name__",
																	"Chorus_Service_Actif__": "LabelChorus_Service_Actif__",
																	"LabelChorus_Service_Actif__": "Chorus_Service_Actif__",
																	"Chorus_Structure_Actif__": "LabelChorus_Structure_Actif__",
																	"LabelChorus_Structure_Actif__": "Chorus_Structure_Actif__",
																	"Chorus_Service_EJ_Number_Required__": "LabelChorus_Service_EJ_Number_Required__",
																	"LabelChorus_Service_EJ_Number_Required__": "Chorus_Service_EJ_Number_Required__",
																	"Billing_Invoice_Type__": "LabelBilling_Invoice_Type__",
																	"LabelBilling_Invoice_Type__": "Billing_Invoice_Type__",
																	"Billing_Identifier_type__": "LabelBilling_Identifier_type__",
																	"LabelBilling_Identifier_type__": "Billing_Identifier_type__",
																	"Billing_Identifier__": "LabelBilling_Identifier__",
																	"LabelBilling_Identifier__": "Billing_Identifier__",
																	"Billing_Country__": "LabelBilling_Country__",
																	"LabelBilling_Country__": "Billing_Country__",
																	"Billing_Registered_Name__": "LabelBilling_Registered_Name__",
																	"LabelBilling_Registered_Name__": "Billing_Registered_Name__"
																},
																"version": 0
															},
															"stamp": 251,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CHORUS_CodeServiceExecutant__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelCHORUS_CodeServiceExecutant__": {
																				"line": 15,
																				"column": 1
																			},
																			"Chorus_Company_Name__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelChorus_Company_Name__": {
																				"line": 3,
																				"column": 1
																			},
																			"Chorus_Address__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelChorus_Address__": {
																				"line": 5,
																				"column": 1
																			},
																			"Chorus_Additional_Address__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelChorus_Additional_Address__": {
																				"line": 6,
																				"column": 1
																			},
																			"Chorus_Postal_Code__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelChorus_Postal_Code__": {
																				"line": 7,
																				"column": 1
																			},
																			"Chorus_City__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelChorus_City__": {
																				"line": 8,
																				"column": 1
																			},
																			"Chorus_Phone__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelChorus_Phone__": {
																				"line": 9,
																				"column": 1
																			},
																			"Chorus_Structure_Identifier__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelChorus_Structure_Identifier__": {
																				"line": 2,
																				"column": 1
																			},
																			"Chorus_EJ_Number_Required__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelChorus_EJ_Number_Required__": {
																				"line": 11,
																				"column": 1
																			},
																			"Chorus_EJ_Or_Service_Code_Required__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelChorus_EJ_Or_Service_Code_Required__": {
																				"line": 12,
																				"column": 1
																			},
																			"Chorus_Payment__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelChorus_Payment__": {
																				"line": 14,
																				"column": 1
																			},
																			"Chorus_Executing_Service_Code_Required__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelChorus_Executing_Service_Code_Required__": {
																				"line": 10,
																				"column": 1
																			},
																			"Chorus_Service_Identifier__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelChorus_Service_Identifier__": {
																				"line": 17,
																				"column": 1
																			},
																			"Chorus_Service_Name__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelChorus_Service_Name__": {
																				"line": 18,
																				"column": 1
																			},
																			"Public_structure__": {
																				"line": 1,
																				"column": 1
																			},
																			"Public_service__": {
																				"line": 16,
																				"column": 1
																			},
																			"Chorus_Service_Actif__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelChorus_Service_Actif__": {
																				"line": 19,
																				"column": 1
																			},
																			"Chorus_Structure_Actif__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelChorus_Structure_Actif__": {
																				"line": 4,
																				"column": 1
																			},
																			"Chorus_Service_EJ_Number_Required__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelChorus_Service_EJ_Number_Required__": {
																				"line": 20,
																				"column": 1
																			},
																			"Billing_Framework__": {
																				"line": 21,
																				"column": 1
																			},
																			"Billing_Invoice_Type__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelBilling_Invoice_Type__": {
																				"line": 22,
																				"column": 1
																			},
																			"Billing_Identifier_type__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelBilling_Identifier_type__": {
																				"line": 24,
																				"column": 1
																			},
																			"Billing_Identifier__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelBilling_Identifier__": {
																				"line": 25,
																				"column": 1
																			},
																			"Billing_Country__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelBilling_Country__": {
																				"line": 23,
																				"column": 1
																			},
																			"Billing_Registered_Name__": {
																				"line": 26,
																				"column": 2
																			},
																			"LabelBilling_Registered_Name__": {
																				"line": 26,
																				"column": 1
																			},
																			"Chorus_isContractingAuthorityOnly__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelChorus_isContractingAuthorityOnly__": {
																				"line": 13,
																				"column": 1
																			}
																		},
																		"lines": 26,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 252,
																	"*": {
																		"Public_structure__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Public structure",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 253
																		},
																		"LabelChorus_Structure_Identifier__": {
																			"type": "Label",
																			"data": [
																				"Chorus_Structure_Identifier__"
																			],
																			"options": {
																				"label": "_Siret",
																				"version": 0
																			},
																			"stamp": 254
																		},
																		"Chorus_Structure_Identifier__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Chorus_Structure_Identifier__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Siret",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "100",
																				"browsable": true,
																				"RestrictSearch": true
																			},
																			"stamp": 255
																		},
																		"LabelChorus_Company_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Company Name",
																				"version": 0
																			},
																			"stamp": 256
																		},
																		"Chorus_Company_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Company Name",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 257
																		},
																		"LabelChorus_Structure_Actif__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Structure Actif",
																				"version": 0
																			},
																			"stamp": 258
																		},
																		"Chorus_Structure_Actif__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_Chorus Structure Actif",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 259
																		},
																		"LabelChorus_Address__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Address",
																				"version": 0
																			},
																			"stamp": 260
																		},
																		"Chorus_Address__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Address",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 261
																		},
																		"LabelChorus_Additional_Address__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Additional Address",
																				"version": 0
																			},
																			"stamp": 262
																		},
																		"Chorus_Additional_Address__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Additional Address",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 263
																		},
																		"LabelChorus_Postal_Code__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Postal Code",
																				"version": 0
																			},
																			"stamp": 264
																		},
																		"Chorus_Postal_Code__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Postal Code",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 265
																		},
																		"LabelChorus_City__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus City",
																				"version": 0
																			},
																			"stamp": 266
																		},
																		"Chorus_City__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus City",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 267
																		},
																		"LabelChorus_Phone__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Phone",
																				"version": 0
																			},
																			"stamp": 268
																		},
																		"Chorus_Phone__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Phone",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 269
																		},
																		"LabelChorus_Executing_Service_Code_Required__": {
																			"type": "Label",
																			"data": [
																				"Chorus_Executing_Service_Code_Required__"
																			],
																			"options": {
																				"label": "_Chorus Executing Service Code Required",
																				"version": 0
																			},
																			"stamp": 270
																		},
																		"Chorus_Executing_Service_Code_Required__": {
																			"type": "CheckBox",
																			"data": [
																				"Chorus_Executing_Service_Code_Required__"
																			],
																			"options": {
																				"label": "_Chorus Executing Service Code Required",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 271
																		},
																		"LabelChorus_EJ_Number_Required__": {
																			"type": "Label",
																			"data": [
																				"Chorus_EJ_Number_Required__"
																			],
																			"options": {
																				"label": "_Chorus EJ Number Required",
																				"version": 0
																			},
																			"stamp": 272
																		},
																		"Chorus_EJ_Number_Required__": {
																			"type": "CheckBox",
																			"data": [
																				"Chorus_EJ_Number_Required__"
																			],
																			"options": {
																				"label": "_Chorus EJ Number Required",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 273
																		},
																		"LabelChorus_EJ_Or_Service_Code_Required__": {
																			"type": "Label",
																			"data": [
																				"Chorus_EJ_Or_Service_Code_Required__"
																			],
																			"options": {
																				"label": "_Chorus EJ Or Service Code Required",
																				"version": 0
																			},
																			"stamp": 274
																		},
																		"Chorus_EJ_Or_Service_Code_Required__": {
																			"type": "CheckBox",
																			"data": [
																				"Chorus_EJ_Or_Service_Code_Required__"
																			],
																			"options": {
																				"label": "_Chorus EJ Or Service Code Required",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 275
																		},
																		"LabelChorus_isContractingAuthorityOnly__": {
																			"type": "Label",
																			"data": [
																				"Chorus_isContractingAuthorityOnly__"
																			],
																			"options": {
																				"label": "__Chorus_isContractingAuthorityOnly",
																				"version": 0
																			},
																			"stamp": 276
																		},
																		"Chorus_isContractingAuthorityOnly__": {
																			"type": "CheckBox",
																			"data": [
																				"Chorus_isContractingAuthorityOnly__"
																			],
																			"options": {
																				"label": "__Chorus_isContractingAuthorityOnly",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"version": 0
																			},
																			"stamp": 277
																		},
																		"LabelChorus_Payment__": {
																			"type": "Label",
																			"data": [
																				"Chorus_Payment__"
																			],
																			"options": {
																				"label": "_Chorus Payment",
																				"version": 0
																			},
																			"stamp": 278
																		},
																		"Chorus_Payment__": {
																			"type": "CheckBox",
																			"data": [
																				"Chorus_Payment__"
																			],
																			"options": {
																				"label": "_Chorus Payment",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 279
																		},
																		"LabelCHORUS_CodeServiceExecutant__": {
																			"type": "Label",
																			"data": [
																				"CHORUS_CodeServiceExecutant__"
																			],
																			"options": {
																				"label": "_CHORUS_Code_service_executant",
																				"version": 0
																			},
																			"stamp": 280
																		},
																		"CHORUS_CodeServiceExecutant__": {
																			"type": "ShortText",
																			"data": [
																				"CHORUS_CodeServiceExecutant__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_CHORUS_Code_service_executant",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 281
																		},
																		"Public_service__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Public service",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 282
																		},
																		"LabelChorus_Service_Identifier__": {
																			"type": "Label",
																			"data": [
																				"Chorus_Service_Identifier__"
																			],
																			"options": {
																				"label": "_Chorus Service Identifier",
																				"version": 0
																			},
																			"stamp": 283
																		},
																		"Chorus_Service_Identifier__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Chorus_Service_Identifier__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Chorus Service Identifier",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "100",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 284
																		},
																		"LabelChorus_Service_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Service Name",
																				"version": 0
																			},
																			"stamp": 285
																		},
																		"Chorus_Service_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Chorus Service Name",
																				"activable": true,
																				"width": "400",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 286
																		},
																		"LabelChorus_Service_Actif__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Service Actif",
																				"version": 0
																			},
																			"stamp": 287
																		},
																		"Chorus_Service_Actif__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_Chorus Service Actif",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 288
																		},
																		"LabelChorus_Service_EJ_Number_Required__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Chorus Service EJ Number Required",
																				"version": 0
																			},
																			"stamp": 289
																		},
																		"Chorus_Service_EJ_Number_Required__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_Chorus Service EJ Number Required",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 290
																		},
																		"Billing_Framework__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "_Billing Framework",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 291
																		},
																		"LabelBilling_Invoice_Type__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_combo_box__"
																			],
																			"options": {
																				"label": "_Billing Invoice Type",
																				"version": 0
																			},
																			"stamp": 292
																		},
																		"Billing_Invoice_Type__": {
																			"type": "ComboBox",
																			"data": [
																				"Billing_Invoice_Type__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_Invoice sent by supplier",
																					"2": "_Invoice sent by subcontractor",
																					"3": "_Invoice sent by cocontractor"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "EMPTY",
																					"1": "A1",
																					"2": "A9",
																					"3": "A12"
																				},
																				"label": "_Billing Invoice Type",
																				"activable": true,
																				"width": 360,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 293
																		},
																		"LabelBilling_Country__": {
																			"type": "Label",
																			"data": [
																				"Billing_Country__"
																			],
																			"options": {
																				"label": "_Billing Country",
																				"version": 0
																			},
																			"stamp": 294
																		},
																		"Billing_Country__": {
																			"type": "ShortText",
																			"data": [
																				"Billing_Country__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Billing Country",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 2,
																				"version": 0
																			},
																			"stamp": 295
																		},
																		"LabelBilling_Identifier_type__": {
																			"type": "Label",
																			"data": [
																				"Billing_Identifier_type__"
																			],
																			"options": {
																				"label": "_Billing Identifier type",
																				"version": 0
																			},
																			"stamp": 296
																		},
																		"Billing_Identifier_type__": {
																			"type": "ComboBox",
																			"data": [
																				"Billing_Identifier_type__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_Siret",
																					"2": "_UE Entity",
																					"3": "_Not UE Entity",
																					"4": "_RIDET",
																					"5": "_Tahiti Number",
																					"6": "_Pending Immatriculation"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "EMPTY",
																					"1": "1",
																					"2": "2",
																					"3": "3",
																					"4": "4",
																					"5": "5",
																					"6": "6"
																				},
																				"label": "_Billing Identifier type",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 297
																		},
																		"LabelBilling_Identifier__": {
																			"type": "Label",
																			"data": [
																				"Billing_Identifier__"
																			],
																			"options": {
																				"label": "_Billing Identifier",
																				"version": 0
																			},
																			"stamp": 298
																		},
																		"Billing_Identifier__": {
																			"type": "ShortText",
																			"data": [
																				"Billing_Identifier__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Billing Identifier",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 18,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 299
																		},
																		"LabelBilling_Registered_Name__": {
																			"type": "Label",
																			"data": [
																				"Billing_Registered_Name__"
																			],
																			"options": {
																				"label": "_Billing Registered Name",
																				"version": 0
																			},
																			"stamp": 300
																		},
																		"Billing_Registered_Name__": {
																			"type": "ShortText",
																			"data": [
																				"Billing_Registered_Name__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Billing Registered Name",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 301
																		}
																	}
																}
															}
														}
													}
												},
												"SimpleIDPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_SimpleID Pane",
														"leftImageURL": "",
														"version": 0,
														"hideTitle": false,
														"panelStyle": "FlexibleFormPanelLight",
														"removeMargins": false,
														"hidden": true,
														"elementsAlignment": "left"
													},
													"stamp": 302,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"PartnerCustomID__": "LabelPartnerCustomID__",
																	"LabelPartnerCustomID__": "PartnerCustomID__",
																	"Billexco_HTML__": "LabelBillexco_HTML__",
																	"LabelBillexco_HTML__": "Billexco_HTML__",
																	"PartnerCustomIDType__": "LabelPartnerCustomIDType__",
																	"LabelPartnerCustomIDType__": "PartnerCustomIDType__",
																	"LabelPEC_Email__": "PEC_Email__",
																	"PEC_Email__": "LabelPEC_Email__",
																	"Party_identification__": "LabelParty_identification__",
																	"LabelParty_identification__": "Party_identification__",
																	"Party_identification_type__": "LabelParty_identification_type__",
																	"LabelParty_identification_type__": "Party_identification_type__"
																},
																"version": 0
															},
															"stamp": 303,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"PartnerCustomID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelPartnerCustomID__": {
																				"line": 1,
																				"column": 1
																			},
																			"Billexco_HTML__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelBillexco_HTML__": {
																				"line": 2,
																				"column": 1
																			},
																			"PartnerCustomIDType__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelPartnerCustomIDType__": {
																				"line": 3,
																				"column": 1
																			},
																			"LabelPEC_Email__": {
																				"line": 4,
																				"column": 1
																			},
																			"PEC_Email__": {
																				"line": 4,
																				"column": 2
																			},
																			"Party_identification__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelParty_identification__": {
																				"line": 5,
																				"column": 1
																			},
																			"Party_identification_type__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelParty_identification_type__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 304,
																	"*": {
																		"PEC_Email__": {
																			"type": "ShortText",
																			"data": [
																				"PEC_Email__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PEC_Email",
																				"activable": true,
																				"width": "260",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 305
																		},
																		"LabelPEC_Email__": {
																			"type": "Label",
																			"data": [
																				"PEC_Email__"
																			],
																			"options": {
																				"label": "_PEC_Email",
																				"version": 0
																			},
																			"stamp": 306
																		},
																		"Billexco_HTML__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_Business link for",
																				"version": 0
																			},
																			"stamp": 307
																		},
																		"LabelBillexco_HTML__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "_Business link for",
																				"version": 0
																			},
																			"stamp": 308
																		},
																		"LabelPartnerCustomID__": {
																			"type": "Label",
																			"data": [
																				"PartnerCustomID__"
																			],
																			"options": {
																				"label": "_Partner Custom ID",
																				"version": 0
																			},
																			"stamp": 309
																		},
																		"PartnerCustomID__": {
																			"type": "ShortText",
																			"data": [
																				"PartnerCustomID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Partner Custom ID",
																				"activable": true,
																				"width": "260",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 310
																		},
																		"LabelPartnerCustomIDType__": {
																			"type": "Label",
																			"data": [
																				"PartnerCustomIDType__"
																			],
																			"options": {
																				"label": "_Partner Custom ID Type",
																				"version": 0
																			},
																			"stamp": 311
																		},
																		"PartnerCustomIDType__": {
																			"type": "ShortText",
																			"data": [
																				"PartnerCustomIDType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Partner Custom ID Type",
																				"activable": true,
																				"width": "260",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 312
																		},
																		"LabelParty_identification__": {
																			"type": "Label",
																			"data": [
																				"Party_identification__"
																			],
																			"options": {
																				"label": "_Party identification"
																			},
																			"stamp": 313
																		},
																		"Party_identification__": {
																			"type": "ShortText",
																			"data": [
																				"Party_identification__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Party identification",
																				"activable": true,
																				"width": "260",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 314
																		},
																		"LabelParty_identification_type__": {
																			"type": "Label",
																			"data": [
																				"Party_identification_type__"
																			],
																			"options": {
																				"label": "_Party identification type"
																			},
																			"stamp": 315
																		},
																		"Party_identification_type__": {
																			"type": "ShortText",
																			"data": [
																				"Party_identification_type__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Party identification type",
																				"activable": true,
																				"width": "260",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 316
																		}
																	}
																}
															}
														}
													}
												},
												"CustomerAPPortalPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Customer AP portal",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 317,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Customer_AP_portal_login__": "LabelCustomer_AP_portal_login__",
																	"LabelCustomer_AP_portal_login__": "Customer_AP_portal_login__",
																	"Customer_AP_portal_password_temp__": "LabelCustomer_AP_portal_password_temp__",
																	"LabelCustomer_AP_portal_password_temp__": "Customer_AP_portal_password_temp__",
																	"Customer_AP_portal_password__": "LabelCustomer_AP_portal_password__",
																	"LabelCustomer_AP_portal_password__": "Customer_AP_portal_password__",
																	"Customer_AP_portal_save_as_draft__": "LabelCustomer_AP_portal_save_as_draft__",
																	"LabelCustomer_AP_portal_save_as_draft__": "Customer_AP_portal_save_as_draft__"
																},
																"version": 0
															},
															"stamp": 318,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Customer_AP_portal_login__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCustomer_AP_portal_login__": {
																				"line": 1,
																				"column": 1
																			},
																			"Customer_AP_portal_password_temp__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCustomer_AP_portal_password_temp__": {
																				"line": 2,
																				"column": 1
																			},
																			"Customer_AP_portal_password__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCustomer_AP_portal_password__": {
																				"line": 3,
																				"column": 1
																			},
																			"Customer_AP_portal_save_as_draft__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCustomer_AP_portal_save_as_draft__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 319,
																	"*": {
																		"LabelCustomer_AP_portal_login__": {
																			"type": "Label",
																			"data": [
																				"Customer_AP_portal_login__"
																			],
																			"options": {
																				"label": "_Customer AP portal login",
																				"version": 0
																			},
																			"stamp": 320
																		},
																		"Customer_AP_portal_login__": {
																			"type": "ShortText",
																			"data": [
																				"Customer_AP_portal_login__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Customer AP portal login",
																				"activable": true,
																				"width": "360",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 321
																		},
																		"LabelCustomer_AP_portal_password_temp__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Customer AP portal password",
																				"version": 0
																			},
																			"stamp": 322
																		},
																		"Customer_AP_portal_password_temp__": {
																			"type": "Password",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Customer AP portal password",
																				"activable": true,
																				"width": "360",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"length": 50,
																				"version": 0
																			},
																			"stamp": 323
																		},
																		"LabelCustomer_AP_portal_password__": {
																			"type": "Label",
																			"data": [
																				"Customer_AP_portal_password__"
																			],
																			"options": {
																				"label": "_Customer AP portal password",
																				"version": 0
																			},
																			"stamp": 324
																		},
																		"Customer_AP_portal_password__": {
																			"type": "ShortText",
																			"data": [
																				"Customer_AP_portal_password__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Customer AP portal password",
																				"activable": true,
																				"width": "360",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 3000,
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 325
																		},
																		"LabelCustomer_AP_portal_save_as_draft__": {
																			"type": "Label",
																			"data": [
																				"Customer_AP_portal_save_as_draft__"
																			],
																			"options": {
																				"label": "_Save as draft",
																				"version": 0
																			},
																			"stamp": 326
																		},
																		"Customer_AP_portal_save_as_draft__": {
																			"type": "CheckBox",
																			"data": [
																				"Customer_AP_portal_save_as_draft__"
																			],
																			"options": {
																				"label": "_Save as draft",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 327
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-22": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 328,
											"*": {
												"SOPPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Sales Order Processing pane",
														"leftImageURL": "",
														"removeMargins": false,
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true,
														"elementsAlignment": "left"
													},
													"stamp": 329,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelNotifications__": "Notifications__",
																	"Notifications__": "LabelNotifications__",
																	"LabelSend_confirmation_notification__": "Send_confirmation_notification__",
																	"Send_confirmation_notification__": "LabelSend_confirmation_notification__",
																	"LabelSales_organization__": "Sales_organization__",
																	"Sales_organization__": "LabelSales_organization__",
																	"LabelDistribution_channel__": "Distribution_channel__",
																	"Distribution_channel__": "LabelDistribution_channel__",
																	"LabelBusiness_area__": "Business_area__",
																	"Business_area__": "LabelBusiness_area__",
																	"OMConfiguration__": "LabelOMConfiguration__",
																	"LabelOMConfiguration__": "OMConfiguration__"
																},
																"version": 0
															},
															"stamp": 330,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelNotifications__": {
																				"line": 1,
																				"column": 1
																			},
																			"Notifications__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelSend_confirmation_notification__": {
																				"line": 2,
																				"column": 1
																			},
																			"Send_confirmation_notification__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelSales_organization__": {
																				"line": 3,
																				"column": 1
																			},
																			"Sales_organization__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDistribution_channel__": {
																				"line": 4,
																				"column": 1
																			},
																			"Distribution_channel__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelBusiness_area__": {
																				"line": 5,
																				"column": 1
																			},
																			"Business_area__": {
																				"line": 5,
																				"column": 2
																			},
																			"OMConfiguration__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelOMConfiguration__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 331,
																	"*": {
																		"Business_area__": {
																			"type": "ShortText",
																			"data": [
																				"Business_area__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Business area",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 332
																		},
																		"LabelBusiness_area__": {
																			"type": "Label",
																			"data": [
																				"Business_area__"
																			],
																			"options": {
																				"label": "_Business area",
																				"version": 0
																			},
																			"stamp": 333
																		},
																		"Distribution_channel__": {
																			"type": "ShortText",
																			"data": [
																				"Distribution_channel__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Distribution channel",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 334
																		},
																		"LabelDistribution_channel__": {
																			"type": "Label",
																			"data": [
																				"Distribution_channel__"
																			],
																			"options": {
																				"label": "_Distribution channel",
																				"version": 0
																			},
																			"stamp": 335
																		},
																		"Sales_organization__": {
																			"type": "ShortText",
																			"data": [
																				"Sales_organization__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Sales organization",
																				"activable": true,
																				"width": "260",
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 336
																		},
																		"LabelSales_organization__": {
																			"type": "Label",
																			"data": [
																				"Sales_organization__"
																			],
																			"options": {
																				"label": "_Sales organization",
																				"version": 0
																			},
																			"stamp": 337
																		},
																		"Send_confirmation_notification__": {
																			"type": "CheckBox",
																			"data": [
																				"Send_confirmation_notification__"
																			],
																			"options": {
																				"label": "_Send confirmation notification",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 338
																		},
																		"LabelSend_confirmation_notification__": {
																			"type": "Label",
																			"data": [
																				"Send_confirmation_notification__"
																			],
																			"options": {
																				"label": "_Send confirmation notification",
																				"version": 0
																			},
																			"stamp": 339
																		},
																		"Notifications__": {
																			"type": "ComboBox",
																			"data": [
																				"Notifications__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "Order notifications"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": 0,
																					"1": "Order notifications"
																				},
																				"label": "_Notifications",
																				"activable": true,
																				"width": "260"
																			},
																			"stamp": 340
																		},
																		"LabelNotifications__": {
																			"type": "Label",
																			"data": [
																				"Notifications__"
																			],
																			"options": {
																				"label": "_Notifications",
																				"version": 0
																			},
																			"stamp": 341
																		},
																		"LabelOMConfiguration__": {
																			"type": "Label",
																			"data": [
																				"OMConfiguration__"
																			],
																			"options": {
																				"label": "_OMConfiguration",
																				"version": 0
																			},
																			"stamp": 541
																		},
																		"OMConfiguration__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"OMConfiguration__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_OMConfiguration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 542
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-9": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 342,
											"*": {
												"TermSyncPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_TermSync",
														"leftImageURL": "",
														"removeMargins": false,
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 343,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"CashCollectionGroup__": "LabelCashCollectionGroup__",
																	"LabelCashCollectionGroup__": "CashCollectionGroup__",
																	"ChatEnabled__": "LabelChatEnabled__",
																	"LabelChatEnabled__": "ChatEnabled__",
																	"Reminders__": "LabelReminders__",
																	"LabelReminders__": "Reminders__",
																	"Sales_representative__": "LabelSales_representative__",
																	"LabelSales_representative__": "Sales_representative__"
																},
																"version": 0
															},
															"stamp": 344,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CashCollectionGroup__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCashCollectionGroup__": {
																				"line": 1,
																				"column": 1
																			},
																			"ChatEnabled__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelChatEnabled__": {
																				"line": 2,
																				"column": 1
																			},
																			"Reminders__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelReminders__": {
																				"line": 3,
																				"column": 1
																			},
																			"Sales_representative__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelSales_representative__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 345,
																	"*": {
																		"LabelCashCollectionGroup__": {
																			"type": "Label",
																			"data": [
																				"CashCollectionGroup__"
																			],
																			"options": {
																				"label": "_Cash Collection Group",
																				"version": 0
																			},
																			"stamp": 346
																		},
																		"CashCollectionGroup__": {
																			"type": "ShortText",
																			"data": [
																				"CashCollectionGroup__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Cash Collection Group",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 347
																		},
																		"LabelChatEnabled__": {
																			"type": "Label",
																			"data": [
																				"ChatEnabled__"
																			],
																			"options": {
																				"label": "_Chat Enabled",
																				"version": 0
																			},
																			"stamp": 348
																		},
																		"ChatEnabled__": {
																			"type": "CheckBox",
																			"data": [
																				"ChatEnabled__"
																			],
																			"options": {
																				"label": "_Chat Enabled",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 349
																		},
																		"LabelReminders__": {
																			"type": "Label",
																			"data": [
																				"Reminders__"
																			],
																			"options": {
																				"label": "_Reminders",
																				"version": 0
																			},
																			"stamp": 350
																		},
																		"Reminders__": {
																			"type": "CheckBox",
																			"data": [
																				"Reminders__"
																			],
																			"options": {
																				"label": "_Reminders",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 351
																		},
																		"LabelSales_representative__": {
																			"type": "Label",
																			"data": [
																				"Sales_representative__"
																			],
																			"options": {
																				"label": "_Sales representative",
																				"version": 0
																			},
																			"stamp": 352
																		},
																		"Sales_representative__": {
																			"type": "ShortText",
																			"data": [
																				"Sales_representative__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Sales representative",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 353
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-7": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 354,
											"*": {
												"CustomerListPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_CustomerList",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 355,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 356,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CustomerList__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line14__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line17__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 357,
																	"*": {
																		"Spacer_line14__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line14",
																				"version": 0
																			},
																			"stamp": 358
																		},
																		"CustomerList__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true,
																				"openInReadOnlyMode": true,
																				"label": "View",
																				"version": 0
																			},
																			"stamp": 359
																		},
																		"Spacer_line17__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line17",
																				"version": 0
																			},
																			"stamp": 360
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 361,
											"*": {
												"RelatedDocumentsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_RelatedDocuments",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "center",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 362,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 363,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"RelatedDocumentsView__": {
																				"line": 2,
																				"column": 1
																			},
																			"UploadDocument__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 364,
																	"*": {
																		"UploadDocument__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_UploadDocument",
																				"label": "",
																				"version": 0,
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "default",
																				"urlImageOverlay": "",
																				"width": "",
																				"action": "none",
																				"url": ""
																			},
																			"stamp": 365
																		},
																		"RelatedDocumentsView__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": false,
																				"openInReadOnlyMode": true,
																				"label": "Related documents view",
																				"version": 0
																			},
																			"stamp": 366
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-6": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 367,
											"*": {
												"OrdersPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Orders",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 368,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 369,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"OrdersView__": {
																				"line": 2,
																				"column": 1
																			},
																			"OrdersMenu__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 370,
																	"*": {
																		"OrdersMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_OrdersMenu",
																				"htmlContent": "<input id=\"onOrdersLoad\" type=\"hidden\" onclick=\"OnOrdersLoad(event);\">\n<input id=\"onChangeOrderView\" type=\"hidden\" onclick=\"OnChangeOrderView(event);\">\n<div id=\"OrdersMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"ordersToValidate\" onclick=\"OnClickOrdersTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"ordersToValidateLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"allImportedOrders\" onclick=\"OnClickOrdersTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"allImportedOrdersLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"blockedOrders\" onclick=\"OnClickOrdersTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"blockedOrdersLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"OrdersMenu__\" }, document.location.origin);\n\tvar tabEvtOrdersItems = document.getElementById(\"OrdersMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickOrdersTab(tab)\n\t{\n\t\tfor (var i = 0; i < tabEvtOrdersItems.length; i++)\n\t\t\ttabEvtOrdersItems[i].className = \"nav-link\";\n\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"OrdersMenu__\", args: tab.id }, document.location.origin);\n\t}\n\n\tfunction OnChangeOrderView(evt)\n\t{\n\t\tfor (var i = 0; i < tabEvtOrdersItems.length; i++)\n\t\t{\n\t\t\tif (tabEvtOrdersItems[i].id === evt.viewName)\n\t\t\t{\n\t\t\t\tOnClickOrdersTab(tabEvtOrdersItems[i]);\n\t\t\t\treturn;\n\t\t\t}\n\t\t}\n\t}\n\n\tfunction OnOrdersLoad(evt)\n\t{\n\t\tvar isFirstTab = true;\n\t\tfor (var tab in evt.tabs)\n\t\t{\n\t\t\tvar spanElem = document.getElementById(tab + \"Lbl\");\n\t\t\tif (spanElem)\n\t\t\t{\n\t\t\t\tspanElem.innerText = evt.tabs[tab].trad;\n\t\t\t}\n\t\t\tvar elem = document.getElementById(tab);\n\t\t\tif(elem && isFirstTab && !evt.tabs[tab].hidden)\n\t\t\t{\n\t\t\t\tisFirstTab = false;\n\t\t\t\telem.classList.add(\"active\");\n\t\t\t}\n\t\t\tif (elem && evt.tabs[tab].hidden)\n\t\t\t{\n\t\t\t\telem.style.display = \"none\";\n\t\t\t\telem.parentNode.style.display = \"none\";\n\t\t\t\tif(elem.parentNode.nextElementSibling)\n\t\t\t\t{\n\t\t\t\t\telem.parentNode.nextElementSibling.style.display = \"none\";\n\t\t\t\t}\n\t\t\t}\n\n\t\t}\n\t}\n</script>",
																				"width": "100%"
																			},
																			"stamp": 371
																		},
																		"OrdersView__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true,
																				"openInReadOnlyMode": false,
																				"label": "_Orders",
																				"version": 0
																			},
																			"stamp": 372
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-10": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 373,
											"*": {
												"GeneralInformationPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_General_Information_Pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 374,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Company__": "LabelCompany__",
																	"LabelCompany__": "Company__",
																	"Customer_Number__": "LabelCustomer_Number__",
																	"LabelCustomer_Number__": "Customer_Number__",
																	"Title__": "LabelTitle__",
																	"LabelTitle__": "Title__",
																	"First_Name__": "LabelFirst_Name__",
																	"LabelFirst_Name__": "First_Name__",
																	"Middle_Name__": "LabelMiddle_Name__",
																	"LabelMiddle_Name__": "Middle_Name__",
																	"Last_Name__": "LabelLast_Name__",
																	"LabelLast_Name__": "Last_Name__",
																	"Display_Name__": "LabelDisplay_Name__",
																	"LabelDisplay_Name__": "Display_Name__",
																	"Additional_Information__": "LabelAdditional_Information__",
																	"LabelAdditional_Information__": "Additional_Information__",
																	"Email_Address__": "LabelEmail_Address__",
																	"LabelEmail_Address__": "Email_Address__",
																	"Phone_Number__": "LabelPhone_Number__",
																	"LabelPhone_Number__": "Phone_Number__",
																	"Mobile_Number__": "LabelMobile_Number__",
																	"LabelMobile_Number__": "Mobile_Number__",
																	"Fax_Number__": "LabelFax_Number__",
																	"LabelFax_Number__": "Fax_Number__"
																},
																"version": 0
															},
															"stamp": 375,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Company__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany__": {
																				"line": 1,
																				"column": 1
																			},
																			"Customer_Number__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCustomer_Number__": {
																				"line": 2,
																				"column": 1
																			},
																			"Title__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelTitle__": {
																				"line": 3,
																				"column": 1
																			},
																			"First_Name__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelFirst_Name__": {
																				"line": 4,
																				"column": 1
																			},
																			"Middle_Name__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelMiddle_Name__": {
																				"line": 5,
																				"column": 1
																			},
																			"Last_Name__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelLast_Name__": {
																				"line": 6,
																				"column": 1
																			},
																			"Display_Name__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelDisplay_Name__": {
																				"line": 7,
																				"column": 1
																			},
																			"Additional_Information__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelAdditional_Information__": {
																				"line": 8,
																				"column": 1
																			},
																			"Email_Address__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelEmail_Address__": {
																				"line": 9,
																				"column": 1
																			},
																			"Phone_Number__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelPhone_Number__": {
																				"line": 10,
																				"column": 1
																			},
																			"Mobile_Number__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelMobile_Number__": {
																				"line": 11,
																				"column": 1
																			},
																			"Fax_Number__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelFax_Number__": {
																				"line": 12,
																				"column": 1
																			},
																			"GeneralInformationPane_Loader__": {
																				"line": 13,
																				"column": 1
																			}
																		},
																		"lines": 13,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 376,
																	"*": {
																		"LabelCompany__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Company",
																				"version": 0
																			},
																			"stamp": 377
																		},
																		"Company__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Company",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 378
																		},
																		"LabelCustomer_Number__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_CustomerNumber",
																				"version": 0
																			},
																			"stamp": 379
																		},
																		"Customer_Number__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_CustomerNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 380
																		},
																		"LabelTitle__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Title",
																				"version": 0
																			},
																			"stamp": 381
																		},
																		"Title__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Title",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"readonly": true
																			},
																			"stamp": 382
																		},
																		"LabelFirst_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_FirstName",
																				"version": 0
																			},
																			"stamp": 383
																		},
																		"First_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FirstName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"readonly": true
																			},
																			"stamp": 384
																		},
																		"LabelMiddle_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_MiddleName",
																				"version": 0
																			},
																			"stamp": 385
																		},
																		"Middle_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_MiddleName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 386
																		},
																		"LabelLast_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_LastName",
																				"version": 0
																			},
																			"stamp": 387
																		},
																		"Last_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_LastName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 388
																		},
																		"LabelDisplay_Name__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_DisplayName",
																				"version": 0
																			},
																			"stamp": 389
																		},
																		"Display_Name__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DisplayName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 390
																		},
																		"LabelAdditional_Information__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Additional information",
																				"version": 0
																			},
																			"stamp": 391
																		},
																		"Additional_Information__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 1,
																				"numberOfLines": 5,
																				"label": "_Additional information",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 392
																		},
																		"LabelEmail_Address__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_EmailAddress",
																				"version": 0
																			},
																			"stamp": 393
																		},
																		"Email_Address__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_EmailAddress",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 394
																		},
																		"LabelPhone_Number__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_PhoneNumber",
																				"version": 0
																			},
																			"stamp": 395
																		},
																		"Phone_Number__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_PhoneNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 396
																		},
																		"LabelMobile_Number__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_MobileNumber",
																				"version": 0
																			},
																			"stamp": 397
																		},
																		"Mobile_Number__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_MobileNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 398
																		},
																		"LabelFax_Number__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_FaxNumber",
																				"version": 0
																			},
																			"stamp": 399
																		},
																		"Fax_Number__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FaxNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 400
																		},
																		"GeneralInformationPane_Loader__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 401
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-11": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 402,
											"*": {
												"MailAddressPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Mail_Address_Pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 403,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"MailSub__": "LabelMailSub__",
																	"LabelMailSub__": "MailSub__",
																	"Street__": "LabelStreet__",
																	"LabelStreet__": "Street__",
																	"Zip_Code__": "LabelZip_Code__",
																	"LabelZip_Code__": "Zip_Code__",
																	"City__": "LabelCity__",
																	"LabelCity__": "City__",
																	"Mail_State__": "LabelMail_State__",
																	"LabelMail_State__": "Mail_State__",
																	"Country__": "LabelCountry__",
																	"LabelCountry__": "Country__",
																	"POBox__": "LabelPOBox__",
																	"LabelPOBox__": "POBox__"
																},
																"version": 0
															},
															"stamp": 404,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"MailSub__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelMailSub__": {
																				"line": 1,
																				"column": 1
																			},
																			"Street__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelStreet__": {
																				"line": 2,
																				"column": 1
																			},
																			"Zip_Code__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelZip_Code__": {
																				"line": 4,
																				"column": 1
																			},
																			"City__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCity__": {
																				"line": 5,
																				"column": 1
																			},
																			"Mail_State__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelMail_State__": {
																				"line": 6,
																				"column": 1
																			},
																			"Country__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelCountry__": {
																				"line": 7,
																				"column": 1
																			},
																			"MailAddressPane_Loader__": {
																				"line": 8,
																				"column": 1
																			},
																			"POBox__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelPOBox__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 8,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 405,
																	"*": {
																		"LabelMailSub__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_MailSub",
																				"version": 0
																			},
																			"stamp": 406
																		},
																		"MailSub__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_MailSub",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 407
																		},
																		"LabelStreet__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Street",
																				"version": 0
																			},
																			"stamp": 408
																		},
																		"Street__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Street",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 409
																		},
																		"LabelPOBox__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_POBox",
																				"version": 0
																			},
																			"stamp": 410
																		},
																		"POBox__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_POBox",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 411
																		},
																		"LabelZip_Code__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Zip code",
																				"version": 0
																			},
																			"stamp": 412
																		},
																		"Zip_Code__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Zip code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 413
																		},
																		"LabelCity__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_City",
																				"version": 0
																			},
																			"stamp": 414
																		},
																		"City__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_City",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 415
																		},
																		"LabelMail_State__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_MailState",
																				"version": 0
																			},
																			"stamp": 416
																		},
																		"Mail_State__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_MailState",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 417
																		},
																		"LabelCountry__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Country",
																				"version": 0
																			},
																			"stamp": 418
																		},
																		"Country__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Country",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 419
																		},
																		"MailAddressPane_Loader__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 420
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-20": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 421,
											"*": {
												"RegionalSettingsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Regional_Settings_Pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 422,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Language__": "LabelLanguage__",
																	"LabelLanguage__": "Language__",
																	"Regional_Settings__": "LabelRegional_Settings__",
																	"LabelRegional_Settings__": "Regional_Settings__",
																	"Export_Encoding__": "LabelExport_Encoding__",
																	"LabelExport_Encoding__": "Export_Encoding__",
																	"Time_Zone__": "LabelTime_Zone__",
																	"LabelTime_Zone__": "Time_Zone__"
																},
																"version": 0
															},
															"stamp": 423,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Language__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelLanguage__": {
																				"line": 1,
																				"column": 1
																			},
																			"Regional_Settings__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelRegional_Settings__": {
																				"line": 2,
																				"column": 1
																			},
																			"Export_Encoding__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelExport_Encoding__": {
																				"line": 3,
																				"column": 1
																			},
																			"Time_Zone__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelTime_Zone__": {
																				"line": 4,
																				"column": 1
																			},
																			"RegionalSettingsPane_Loader__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 424,
																	"*": {
																		"LabelLanguage__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Language",
																				"version": 0
																			},
																			"stamp": 425
																		},
																		"Language__": {
																			"type": "ComboBox",
																			"data": [],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_Language",
																				"activable": true,
																				"width": 400,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String"
																			},
																			"stamp": 426
																		},
																		"LabelRegional_Settings__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Culture",
																				"version": 0
																			},
																			"stamp": 427
																		},
																		"Regional_Settings__": {
																			"type": "ComboBox",
																			"data": [],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_Culture",
																				"activable": true,
																				"width": 400,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"notInDB": true,
																				"dataType": "String",
																				"readonly": true
																			},
																			"stamp": 428
																		},
																		"LabelExport_Encoding__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_ExportEncoding",
																				"version": 0
																			},
																			"stamp": 429
																		},
																		"Export_Encoding__": {
																			"type": "ComboBox",
																			"data": [],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_ExportEncoding",
																				"activable": true,
																				"width": 400,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"notInDB": true,
																				"dataType": "String",
																				"readonly": true
																			},
																			"stamp": 430
																		},
																		"LabelTime_Zone__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_TimeZone",
																				"version": 0
																			},
																			"stamp": 431
																		},
																		"Time_Zone__": {
																			"type": "ComboBox",
																			"data": [],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_TimeZone",
																				"activable": true,
																				"width": 400,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"notInDB": true,
																				"dataType": "String",
																				"readonly": true
																			},
																			"stamp": 432
																		},
																		"RegionalSettingsPane_Loader__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 433
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-24": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 434,
											"*": {
												"CustomerAdvancedFieldsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Customer_Advanced_Fields_Pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 435,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Additional_Field1__": "LabelAdditional_Field1__",
																	"LabelAdditional_Field1__": "Additional_Field1__",
																	"Additional_Field2__": "LabelAdditional_Field2__",
																	"LabelAdditional_Field2__": "Additional_Field2__",
																	"Additional_Field3__": "LabelAdditional_Field3__",
																	"LabelAdditional_Field3__": "Additional_Field3__",
																	"Additional_Field4__": "LabelAdditional_Field4__",
																	"LabelAdditional_Field4__": "Additional_Field4__",
																	"Additional_Field5__": "LabelAdditional_Field5__",
																	"LabelAdditional_Field5__": "Additional_Field5__"
																},
																"version": 0
															},
															"stamp": 436,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Additional_Field1__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelAdditional_Field1__": {
																				"line": 1,
																				"column": 1
																			},
																			"Additional_Field2__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelAdditional_Field2__": {
																				"line": 2,
																				"column": 1
																			},
																			"Additional_Field3__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelAdditional_Field3__": {
																				"line": 3,
																				"column": 1
																			},
																			"Additional_Field4__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelAdditional_Field4__": {
																				"line": 4,
																				"column": 1
																			},
																			"Additional_Field5__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelAdditional_Field5__": {
																				"line": 5,
																				"column": 1
																			},
																			"CustomerAdvancedFieldsPane_Loader__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 437,
																	"*": {
																		"LabelAdditional_Field1__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_AdditionalField1",
																				"version": 0
																			},
																			"stamp": 438
																		},
																		"Additional_Field1__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AdditionalField1",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 439
																		},
																		"LabelAdditional_Field2__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_AdditionalField2",
																				"version": 0
																			},
																			"stamp": 440
																		},
																		"Additional_Field2__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AdditionalField2",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 441
																		},
																		"LabelAdditional_Field3__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_AdditionalField3",
																				"version": 0
																			},
																			"stamp": 442
																		},
																		"Additional_Field3__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AdditionalField3",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 443
																		},
																		"LabelAdditional_Field4__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_AdditionalField4",
																				"version": 0
																			},
																			"stamp": 444
																		},
																		"Additional_Field4__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AdditionalField4",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 445
																		},
																		"LabelAdditional_Field5__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_AdditionalField5",
																				"version": 0
																			},
																			"stamp": 446
																		},
																		"Additional_Field5__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AdditionalField5",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 447
																		},
																		"CustomerAdvancedFieldsPane_Loader__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 448
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-26": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 449,
											"*": {
												"CustomerIdentifiers": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_CustomerIdentifiers",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 450,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"TaxID__": "LabelTaxID__",
																	"LabelTaxID__": "TaxID__",
																	"TvaIntra__": "LabelTvaIntra__",
																	"LabelTvaIntra__": "TvaIntra__",
																	"FederalTaxId__": "LabelFederalTaxId__",
																	"LabelFederalTaxId__": "FederalTaxId__",
																	"DUNS__": "LabelDUNS__",
																	"LabelDUNS__": "DUNS__",
																	"SIRET__": "LabelSIRET__",
																	"LabelSIRET__": "SIRET__",
																	"SIREN__": "LabelSIREN__",
																	"LabelSIREN__": "SIREN__",
																	"CodeAPE__": "LabelCodeAPE__",
																	"LabelCodeAPE__": "CodeAPE__",
																	"TIN__": "LabelTIN__",
																	"LabelTIN__": "TIN__",
																	"ABN__": "LabelABN__",
																	"LabelABN__": "ABN__"
																},
																"version": 0
															},
															"stamp": 451,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TaxID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelTaxID__": {
																				"line": 1,
																				"column": 1
																			},
																			"TvaIntra__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelTvaIntra__": {
																				"line": 2,
																				"column": 1
																			},
																			"FederalTaxId__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelFederalTaxId__": {
																				"line": 3,
																				"column": 1
																			},
																			"DUNS__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDUNS__": {
																				"line": 4,
																				"column": 1
																			},
																			"SIRET__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelSIRET__": {
																				"line": 5,
																				"column": 1
																			},
																			"SIREN__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelSIREN__": {
																				"line": 6,
																				"column": 1
																			},
																			"NIC__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelNIC__": {
																				"line": 7,
																				"column": 1
																			},
																			"CodeAPE__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCodeAPE__": {
																				"line": 8,
																				"column": 1
																			},
																			"TIN__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelTIN__": {
																				"line": 9,
																				"column": 1
																			},
																			"ABN__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelABN__": {
																				"line": 10,
																				"column": 1
																			}
																		},
																		"lines": 10,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 452,
																	"*": {
																		"LabelTaxID__": {
																			"type": "Label",
																			"data": [
																				"TaxID__"
																			],
																			"options": {
																				"label": "TaxID"
																			},
																			"stamp": 453
																		},
																		"TaxID__": {
																			"type": "ShortText",
																			"data": [
																				"TaxID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "TaxID",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 454
																		},
																		"LabelTvaIntra__": {
																			"type": "Label",
																			"data": [
																				"TvaIntra__"
																			],
																			"options": {
																				"label": "TvaIntra"
																			},
																			"stamp": 455
																		},
																		"TvaIntra__": {
																			"type": "ShortText",
																			"data": [
																				"TvaIntra__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "TvaIntra",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 456
																		},
																		"LabelFederalTaxId__": {
																			"type": "Label",
																			"data": [
																				"FederalTaxId__"
																			],
																			"options": {
																				"label": "FederalTaxId"
																			},
																			"stamp": 457
																		},
																		"FederalTaxId__": {
																			"type": "ShortText",
																			"data": [
																				"FederalTaxId__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "FederalTaxId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 458
																		},
																		"LabelDUNS__": {
																			"type": "Label",
																			"data": [
																				"DUNS__"
																			],
																			"options": {
																				"label": "DUNS"
																			},
																			"stamp": 459
																		},
																		"DUNS__": {
																			"type": "ShortText",
																			"data": [
																				"DUNS__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "DUNS",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 460
																		},
																		"LabelSIRET__": {
																			"type": "Label",
																			"data": [
																				"SIRET__"
																			],
																			"options": {
																				"label": "SIRET"
																			},
																			"stamp": 461
																		},
																		"SIRET__": {
																			"type": "ShortText",
																			"data": [
																				"SIRET__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "SIRET",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 462
																		},
																		"LabelSIREN__": {
																			"type": "Label",
																			"data": [
																				"SIREN__"
																			],
																			"options": {
																				"label": "SIREN"
																			},
																			"stamp": 463
																		},
																		"SIREN__": {
																			"type": "ShortText",
																			"data": [
																				"SIREN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "SIREN",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 464
																		},
																		"LabelNIC__": {
																			"type": "Label",
																			"data": [
																				"NIC__"
																			],
																			"options": {
																				"label": "NIC"
																			},
																			"stamp": 465
																		},
																		"NIC__": {
																			"type": "ShortText",
																			"data": [
																				"NIC__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "NIC",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 466
																		},
																		"LabelCodeAPE__": {
																			"type": "Label",
																			"data": [
																				"CodeAPE__"
																			],
																			"options": {
																				"label": "CodeAPE"
																			},
																			"stamp": 467
																		},
																		"CodeAPE__": {
																			"type": "ShortText",
																			"data": [
																				"CodeAPE__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "CodeAPE",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 468
																		},
																		"LabelTIN__": {
																			"type": "Label",
																			"data": [
																				"TIN__"
																			],
																			"options": {
																				"label": "TIN"
																			},
																			"stamp": 469
																		},
																		"TIN__": {
																			"type": "ShortText",
																			"data": [
																				"TIN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "TIN",
																				"activable": true,
																				"width": 230,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 470
																		},
																		"LabelABN__": {
																			"type": "Label",
																			"data": [
																				"ABN__"
																			],
																			"options": {
																				"label": "ABN",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 471
																		},
																		"ABN__": {
																			"type": "ShortText",
																			"data": [
																				"ABN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "ABN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"readonly": true,
																				"hidden": true
																			},
																			"stamp": 472
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-15": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 473,
											"*": {
												"WelcomeSettingsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_Welcome_Settings_Pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": true
													},
													"stamp": 474,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Display_Welcome_Page__": "LabelDisplay_Welcome_Page__",
																	"LabelDisplay_Welcome_Page__": "Display_Welcome_Page__"
																},
																"version": 0
															},
															"stamp": 475,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Display_Welcome_Page__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDisplay_Welcome_Page__": {
																				"line": 1,
																				"column": 1
																			},
																			"WelcomeSettingsPane_Loader__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 476,
																	"*": {
																		"LabelDisplay_Welcome_Page__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_DisplayWelcomePageOnNextLogin",
																				"version": 0
																			},
																			"stamp": 477
																		},
																		"Display_Welcome_Page__": {
																			"type": "CheckBox",
																			"data": [],
																			"options": {
																				"label": "_DisplayWelcomePageOnNextLogin",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "Boolean",
																				"version": 0
																			},
																			"stamp": 478
																		},
																		"WelcomeSettingsPane_Loader__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"version": 0
																			},
																			"stamp": 479
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-5": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 480,
											"*": {
												"CreditScoreSettingsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "CreditScoreSettingsPane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"hidden": true,
														"version": 0
													},
													"stamp": 481,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"CreditScore_CompanyIdentifier__": "LabelCreditScore_CompanyIdentifier__",
																	"LabelCreditScore_CompanyIdentifier__": "CreditScore_CompanyIdentifier__",
																	"CreditScore_Enabled__": "LabelCreditScore_Enabled__",
																	"LabelCreditScore_Enabled__": "CreditScore_Enabled__",
																	"CreditLimit__": "LabelCreditLimit__",
																	"LabelCreditLimit__": "CreditLimit__",
																	"NextReviewCM__": "LabelNextReviewCM__",
																	"LabelNextReviewCM__": "NextReviewCM__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"ThirdPartiesIds__": "LabelThirdPartiesIds__",
																	"LabelThirdPartiesIds__": "ThirdPartiesIds__",
																	"ShowAlerts__": "LabelShowAlerts__",
																	"LabelShowAlerts__": "ShowAlerts__",
																	"RiskCategory__": "LabelRiskCategory__",
																	"LabelRiskCategory__": "RiskCategory__",
																	"EnableHistoric__": "LabelEnableHistoric__",
																	"LabelEnableHistoric__": "EnableHistoric__",
																	"OutstandingAlert__": "LabelOutstandingAlert__",
																	"LabelOutstandingAlert__": "OutstandingAlert__",
																	"Outstanding__": "LabelOutstanding__",
																	"LabelOutstanding__": "Outstanding__",
																	"OverCreditLimit__": "LabelOverCreditLimit__",
																	"LabelOverCreditLimit__": "OverCreditLimit__",
																	"ADP__": "LabelADP__",
																	"LabelADP__": "ADP__"
																},
																"version": 0
															},
															"stamp": 482,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CreditScore_CompanyIdentifier__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCreditScore_CompanyIdentifier__": {
																				"line": 3,
																				"column": 1
																			},
																			"CreditScore_Enabled__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCreditScore_Enabled__": {
																				"line": 2,
																				"column": 1
																			},
																			"CreditLimit__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCreditLimit__": {
																				"line": 4,
																				"column": 1
																			},
																			"NextReviewCM__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelNextReviewCM__": {
																				"line": 5,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 6,
																				"column": 1
																			},
																			"LabelThirdPartiesIds__": {
																				"line": 7,
																				"column": 1
																			},
																			"ThirdPartiesIds__": {
																				"line": 7,
																				"column": 2
																			},
																			"ShowAlerts__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelShowAlerts__": {
																				"line": 8,
																				"column": 1
																			},
																			"RiskCategory__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelRiskCategory__": {
																				"line": 10,
																				"column": 1
																			},
																			"EnableHistoric__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelEnableHistoric__": {
																				"line": 1,
																				"column": 1
																			},
																			"OutstandingAlert__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelOutstandingAlert__": {
																				"line": 9,
																				"column": 1
																			},
																			"Outstanding__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelOutstanding__": {
																				"line": 11,
																				"column": 1
																			},
																			"OverCreditLimit__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelOverCreditLimit__": {
																				"line": 12,
																				"column": 1
																			},
																			"ADP__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelADP__": {
																				"line": 13,
																				"column": 1
																			}
																		},
																		"lines": 13,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 483,
																	"*": {
																		"LabelCreditScore_Enabled__": {
																			"type": "Label",
																			"data": [
																				"CreditScore_Enabled__"
																			],
																			"options": {
																				"label": "_CreditScore_Enabled",
																				"version": 0
																			},
																			"stamp": 484
																		},
																		"LabelOutstandingAlert__": {
																			"type": "Label",
																			"data": [
																				"OutstandingAlert__"
																			],
																			"options": {
																				"label": "_OutstandingAlert",
																				"hidden": true
																			},
																			"stamp": 485
																		},
																		"OutstandingAlert__": {
																			"type": "CheckBox",
																			"data": [
																				"OutstandingAlert__"
																			],
																			"options": {
																				"label": "_OutstandingAlert",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"hidden": true
																			},
																			"stamp": 486
																		},
																		"CreditScore_Enabled__": {
																			"type": "CheckBox",
																			"data": [
																				"CreditScore_Enabled__"
																			],
																			"options": {
																				"label": "_CreditScore_Enabled",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 487
																		},
																		"CreditScore_CompanyIdentifier__": {
																			"type": "ShortText",
																			"data": [
																				"CreditScore_CompanyIdentifier__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_CreditScore_CompanyIdentifier",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 488
																		},
																		"LabelCreditScore_CompanyIdentifier__": {
																			"type": "Label",
																			"data": [
																				"CreditScore_CompanyIdentifier__"
																			],
																			"options": {
																				"label": "_CreditScore_CompanyIdentifier",
																				"version": 0
																			},
																			"stamp": 489
																		},
																		"LabelCreditLimit__": {
																			"type": "Label",
																			"data": [
																				"CreditLimit__"
																			],
																			"options": {
																				"label": "_CreditLimit",
																				"hidden": true
																			},
																			"stamp": 490
																		},
																		"CreditLimit__": {
																			"type": "Integer",
																			"data": [
																				"CreditLimit__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_CreditLimit",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"hidden": true,
																				"browsable": false
																			},
																			"stamp": 491
																		},
																		"LabelNextReviewCM__": {
																			"type": "Label",
																			"data": [
																				"NextReviewCM__"
																			],
																			"options": {
																				"label": "_NextReviewCM",
																				"hidden": true
																			},
																			"stamp": 492
																		},
																		"NextReviewCM__": {
																			"type": "DateTime",
																			"data": [
																				"NextReviewCM__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_NextReviewCM",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true
																			},
																			"stamp": 493
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "_Currency",
																				"hidden": true
																			},
																			"stamp": 494
																		},
																		"Currency__": {
																			"type": "ShortText",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Currency",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true,
																				"browsable": false
																			},
																			"stamp": 495
																		},
																		"LabelThirdPartiesIds__": {
																			"type": "Label",
																			"data": [
																				"ThirdPartiesIds__"
																			],
																			"options": {
																				"label": "_ThirdPartiesIds",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 496
																		},
																		"ThirdPartiesIds__": {
																			"type": "LongText",
																			"data": [
																				"ThirdPartiesIds__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_ThirdPartiesIds",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 497
																		},
																		"LabelShowAlerts__": {
																			"type": "Label",
																			"data": [
																				"ShowAlerts__"
																			],
																			"options": {
																				"label": "_ShowAlerts",
																				"hidden": true
																			},
																			"stamp": 498
																		},
																		"ShowAlerts__": {
																			"type": "CheckBox",
																			"data": [
																				"ShowAlerts__"
																			],
																			"options": {
																				"label": "_ShowAlerts",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true
																			},
																			"stamp": 499
																		},
																		"LabelRiskCategory__": {
																			"type": "Label",
																			"data": [
																				"ComboBox__"
																			],
																			"options": {
																				"label": "_RiskCategory",
																				"hidden": true
																			},
																			"stamp": 500
																		},
																		"RiskCategory__": {
																			"type": "ComboBox",
																			"data": [
																				"RiskCategory__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_RiskNone",
																					"1": "_RiskVeryLow",
																					"2": "_RiskLow",
																					"3": "_RiskModerate",
																					"4": "_RiskMedium",
																					"5": "_RiskHigh",
																					"6": "_RiskExtreme",
																					"7": "_RiskOutOfBusiness"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "0",
																					"2": "1",
																					"3": "2",
																					"4": "3",
																					"5": "4",
																					"6": "5",
																					"7": "6"
																				},
																				"label": "_RiskCategory",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"hidden": true,
																				"readonly": true
																			},
																			"stamp": 501
																		},
																		"LabelEnableHistoric__": {
																			"type": "Label",
																			"data": [
																				"EnableHistoric__"
																			],
																			"options": {
																				"label": "_EnableHistoric",
																				"version": 0
																			},
																			"stamp": 502
																		},
																		"EnableHistoric__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableHistoric__"
																			],
																			"options": {
																				"label": "_EnableHistoric",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 503
																		},
																		"LabelOutstanding__": {
																			"type": "Label",
																			"data": [
																				"Outstanding__"
																			],
																			"options": {
																				"label": "_Outstanding",
																				"hidden": true
																			},
																			"stamp": 504
																		},
																		"Outstanding__": {
																			"type": "Decimal",
																			"data": [
																				"Outstanding__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Outstanding",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false,
																				"hidden": true
																			},
																			"stamp": 505
																		},
																		"LabelOverCreditLimit__": {
																			"type": "Label",
																			"data": [
																				"OverCreditLimit__"
																			],
																			"options": {
																				"label": "_OverCreditLimit",
																				"hidden": true
																			},
																			"stamp": 506
																		},
																		"OverCreditLimit__": {
																			"type": "Decimal",
																			"data": [
																				"OverCreditLimit__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_OverCreditLimit",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false,
																				"hidden": true
																			},
																			"stamp": 507
																		},
																		"LabelADP__": {
																			"type": "Label",
																			"data": [
																				"ADP__"
																			],
																			"options": {
																				"label": "_AverageDaysToPay",
																				"hidden": true
																			},
																			"stamp": 508
																		},
																		"ADP__": {
																			"type": "Decimal",
																			"data": [
																				"ADP__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_AverageDaysToPay",
																				"precision_internal": 1,
																				"precision_current": 1,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"precision_min": null,
																				"enablePlusMinus": false,
																				"hidden": true,
																				"autocompletable": false
																			},
																			"stamp": 509
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-4": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 510,
											"*": {
												"CashApplicationSettingsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "left",
														"label": "_CashApplicationSettingsPane",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0
													},
													"stamp": 511,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"CA_TouchlessEnabled__": "LabelCA_TouchlessEnabled__",
																	"LabelCA_TouchlessEnabled__": "CA_TouchlessEnabled__"
																},
																"version": 0
															},
															"stamp": 512,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"CA_TouchlessEnabled__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCA_TouchlessEnabled__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 513,
																	"*": {
																		"LabelCA_TouchlessEnabled__": {
																			"type": "Label",
																			"data": [
																				"CA_TouchlessEnabled__"
																			],
																			"options": {
																				"label": "_CA Enable touchless",
																				"version": 0
																			},
																			"stamp": 514
																		},
																		"CA_TouchlessEnabled__": {
																			"type": "CheckBox",
																			"data": [
																				"CA_TouchlessEnabled__"
																			],
																			"options": {
																				"label": "_CA Enable touchless",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 515
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-12": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 516,
											"*": {
												"DeductionsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Deductions pane",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 517,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 518,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DeductionsList__": {
																				"line": 2,
																				"column": 1
																			},
																			"DeductionsMenu__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 519,
																	"*": {
																		"DeductionsMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_DeductionsMenu",
																				"width": "100%",
																				"htmlContent": "<input id=\"OnDeductionsLoad\" type=\"hidden\" onclick=\"OnDeductionsLoad(event);\">\n<input id=\"OnChangeDeductionsView\" type=\"hidden\" onclick=\"OnChangeDeductionsView(event);\">\n<div id=\"deductionsMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"alldeductions\" onclick=\"OnClickdeductionsTab(this);\" href=\"#\" class=\"nav-link active\">\n\t\t\t\t<span id=\"alldeductionsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"deductionsToVerify\" onclick=\"OnClickdeductionsTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"deductionsToVerifyLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"deductionsToApprove\" onclick=\"OnClickdeductionsTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"deductionsToApproveLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"deductionsToPost\" onclick=\"OnClickdeductionsTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"deductionsToPostLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"deductionsResolved\" onclick=\"OnClickdeductionsTab(this);\" href=\"#\" class=\"nav-link\">\n\t\t\t\t<span id=\"deductionsResolvedLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"DeductionsMenu__\" }, document.location.origin);\n\tvar tabEvtdeductionsItems = document.getElementById(\"deductionsMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickdeductionsTab(tab)\n\t{\n\t\tfor (var i = 0; i < tabEvtdeductionsItems.length; i++)\n\t\t\ttabEvtdeductionsItems[i].className = \"nav-link\";\n\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"DeductionsMenu__\", args: tab.id }, document.location.origin);\n\t}\n\n\tfunction OnChangeDeductionsView(evt)\n\t{\n\t\tfor (var i = 0; i < tabEvtdeductionsItems.length; i++)\n\t\t{\n\t\t\tif (tabEvtdeductionsItems[i].id === evt.viewName)\n\t\t\t{\n\t\t\t\tOnClickdeductionsTab(tabEvtdeductionsItems[i]);\n\t\t\t\treturn;\n\t\t\t}\n\t\t}\n\t}\n\n\tfunction OnDeductionsLoad(evt)\n\t{\n\t\tfor (var tab in evt.tabs)\n\t\t{\n\t\t\tvar spanElem = document.getElementById(tab + \"Lbl\");\n\t\t\tif (spanElem)\n\t\t\t{\n\t\t\t\tspanElem.innerText = evt.tabs[tab].trad;\n\t\t\t}\n\t\t\tvar elem = document.getElementById(tab);\n\t\t\tif (elem && evt.tabs[tab].hidden)\n\t\t\t{\n\t\t\t\telem.style.display = \"none\";\n\t\t\t\telem.parentNode.previousElementSibling.style.display = \"none\";\n\t\t\t}\n\n\t\t}\n\t}\n</script>",
																				"version": 0
																			},
																			"stamp": 520
																		},
																		"DeductionsList__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"openInReadOnlyMode": false,
																				"width": "100%",
																				"label": "View",
																				"version": 0,
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true
																			},
																			"stamp": 521
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-8": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 522,
											"*": {
												"RemittancesPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"panelStyle": "FlexibleFormPanelLight",
														"label": "_Remittances",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 523,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 524,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Remittances__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 525,
																	"*": {
																		"Remittances__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true,
																				"openInReadOnlyMode": false,
																				"label": "View",
																				"version": 0
																			},
																			"stamp": 526
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-13": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 527,
											"*": {
												"PaymentsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Payments",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight"
													},
													"stamp": 528,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {}
															},
															"stamp": 529,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Payments__": {
																				"line": 1,
																				"column": 1
																			},
																			"Payment_NavMenu__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"stamp": 530,
																	"*": {
																		"Payment_NavMenu__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_Payment_NavMenu__",
																				"version": 0,
																				"htmlContent": "<input id=\"onPaymentLoad\" type=\"hidden\" onclick=\"OnPaymentLoad(event);\">\n<div id=\"PaymentMenu\" class=\"nav-body\" style=\"\">\n\t<ul class=\"nav-topbar\">\n\t\t<li class=\"nav-item\">\n\t\t\t<a id=\"cashAppPayments\" onclick=\"OnClickPaymentTab(this);\" href=\"#\" class=\"nav-link\" style=\"display:none\" >\n\t\t\t\t<span id=\"cashAppPaymentsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t\t<li class=\"nav-item-divider\"></li>\n\t\t<li class=\"nav-item\">\n\t\t\t<a href=\"#\" id=\"portalPayments\" onclick=\"OnClickPaymentTab(this);\" class=\"nav-link\" style=\"display:none\" >\n\t\t\t\t<span id=\"portalPaymentsLbl\" />\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<script>\n\twindow.postMessage({ eventName: \"onLoad\", control: \"Payment_NavMenu__\" }, document.location.origin);\n\tvar tabPaymentItems = document.getElementById(\"PaymentMenu\").getElementsByClassName(\"nav-link\");\n\tfunction OnClickPaymentTab(tab)\n\t{\n\t\tfor (var i = 0; i < tabPaymentItems.length; i++)\n\t\t\ttabPaymentItems[i].className = \"nav-link\";\n\n\t\ttab.className = \"nav-link active\";\n\t\twindow.postMessage({ eventName: \"onClick\", control: \"Payment_NavMenu__\", args: tab.id }, document.location.origin);\n\t}\n\tfunction OnPaymentLoad(evt)\n\t{\n\t\tfor (var lbl in evt.tabs)\n\t\t{\n\t\t\tvar elem = document.getElementById(lbl + \"Lbl\");\n\t\t\tif (elem)\n\t\t\t{\n\t\t\t\telem.innerText = evt.tabs[lbl];\n\t\t\t}\n\t\t\tfor (var i = 0; i < tabPaymentItems.length; i++)\n\t\t\t{\n\t\t\t\tif (typeof evt.IsDisplay == \"function\" && evt.IsDisplay(tabPaymentItems[i].id))\n\t\t\t\t{\n\t\t\t\t\ttabPaymentItems[i].style.display = \"inherit\";\n\t\t\t\t}\n\t\t\t}\n\t\t\ttabPaymentItems[evt.defaultTab].classList.add(\"active\");\n\t\t}\n\t}\n</script>",
																				"width": "100%"
																			},
																			"stamp": 531
																		},
																		"Payments__": {
																			"type": "AdminList",
																			"data": false,
																			"options": {
																				"width": "100%",
																				"restrictToCurrentJobId": false,
																				"showActions": false,
																				"openInNewTab": true,
																				"openInReadOnlyMode": false,
																				"label": "View",
																				"version": 0
																			},
																			"stamp": 532
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 533,
									"data": []
								}
							},
							"stamp": 534,
							"data": []
						}
					},
					"stamp": 535,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 536,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 537,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 538,
							"data": []
						}
					},
					"stamp": 539,
					"data": []
				}
			},
			"stamp": 540,
			"data": []
		}
	},
	"stamps": 563,
	"data": []
}