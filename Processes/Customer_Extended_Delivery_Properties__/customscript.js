///#GLOBALS Lib Sys

Sys.AR.NeedConfiguration = false;
var PackageHelper = {
	DefaultStartMenu: "overview",
	AR: {
		counters: [],
		countersByFeatures:
		{
			Payment: Sys.AR.GetParameter("EnablePaymentFeature") ?
				[Controls.Overdue__, Controls.TotalOutstanding__] :
				[Controls.Invoices60Days__, Controls.CreditNotes60Days__],

			Dispute: Sys.AR.GetParameter("EnableDisputeFeature") ? [Controls.Disputes__] : [],
			CM: Sys.AR.GetParameter("EnableCreditApplication") ? [Controls.BlockedOrdersCounter__, Controls.ImportedOrderCounter__, Controls.TotalExposure__] : []
		},
		HTMLContents: Sys.AR.GetParameter("EnableCreditApplication") && Data.GetValue("CreditLimit__") ? [Controls.CreditLimitUtilization__] : [],
		reports: [],
		reportsByFeatures: {
			Payment: Sys.AR.GetParameter("EnablePaymentFeature") ?
				[Controls.AgedBalance__] :
				[Controls.InvoicesAndCreditNotes__],
			Credit: Controls.EnableHistoric__.GetValue() && Sys.AR.GetParameter("EnableCreditApplication") ? [Controls.Historic__] : []
		},
		settings: [
			{ "InvoiceDeliveryPane": null },
			{ "TermSyncPane": { "Enable_Termsync_Synchronization": true } },
			Sys.AR.GetParameter("EnableCreditApplication") ? { CreditScoreSettingsPane: null } : {}
		]
	},
	DD: {
		counters: [],
		HTMLContents: [],
		reports: [],
		startMenu: "relatedDoc"
	},
	//SOP
	COP: {
		counters: [Controls.OrdersToValidate__],
		HTMLContents: [],
		reports: [],
		startMenu: "overview",
		settings: [
			{ "SOPPane": null }
		]
	},
	CA: {
		counters: [],
		HTMLContents: [],
		reports: [],
		settings: [],
		startMenu: "overview"
	},
	DEDUCTIONS: {
		counters: [Controls.DeductionsToValidate__],
		HTMLContents: [],
		reports: [Controls.ClaimAmountByMonth__, Controls.ClaimAmountByClaimType__],
		settings: [],
		startMenu: "overview"
	},
	getValues: function (type)
	{
		var controls = [];
		for (var p in ProcessInstance.extendedProperties.appInstances)
		{
			if (ProcessInstance.extendedProperties.appInstances[p] && this[p] && this[p][type])
			{
				controls = controls.concat(this[p][type]);

				var featureSettingsKey = type + "ByFeatures";
				if (this[p][featureSettingsKey])
				{
					for (var feature in this[p][featureSettingsKey])
					{
						controls = controls.concat(this[p][featureSettingsKey][feature]);
					}
				}
			}
		}
		return controls;
	},
	getReports: function () { return this.getValues("reports"); },
	getHTMLContents: function () { return this.getValues("HTMLContents"); },
	getCounters: function () { return this.getValues("counters"); },
	getStartMenu: function ()
	{
		var packages = Object.keys(ProcessInstance.extendedProperties.appInstances);
		var packagesUsed = [];
		for (let index = 0; index < packages.length; index++)
		{
			if (PackageHelper[packages[index]])
			{
				packagesUsed.push(packages[index]);
			}
		}
		var urlParameter = Process.GetURLParameter("openMenu");
		if (urlParameter && PackageHelper[urlParameter] && PackageHelper[urlParameter].startMenu)
		{
			return PackageHelper[urlParameter].startMenu;
		}
		return packagesUsed.length === 1 ? PackageHelper[packagesUsed[0]].startMenu || this.DefaultStartMenu : this.DefaultStartMenu;
	},
	getSettings: function ()
	{
		return this.getValues("settings");
	}
};

var APPortalHelper = {
	_encryptionInProgress: false,
	_saveAndExitFormAfterEncryption: false,
	_dummyPassword: "********",
	_originalEncryptedPassword: Controls.Customer_AP_portal_password__.GetValue(),
	_isExternalPortalCustomer: ProcessInstance.extendedProperties.preferredDeliveryMethod === "EXTERNALPORTAL",
	Init: function ()
	{
		Controls.Customer_AP_portal_password__.Hide(true);
		if (Controls.Customer_AP_portal_password__.GetValue())
		{
			Controls.Customer_AP_portal_password_temp__.SetValue(this._dummyPassword);
		}

		if (this._isExternalPortalCustomer)
		{
			var allowedAPPortals = Sys.Helpers.TryCallFunction("Lib.AR.Customization.ExternalPortal.GetAPPortals") || [];
			if (User.isInDemoAccount)
			{
				allowedAPPortals.push("Robot to Ariba");
			}

			// Check each portal has been defined as 'option=value' form.
			for (var portalIndex = 0; portalIndex < allowedAPPortals.length; portalIndex++)
			{
				var allowedPortal = allowedAPPortals[portalIndex];
				if (typeof allowedPortal === "string" && allowedPortal.length > 0 && allowedPortal.indexOf("=") < 0)
				{
					// Otherwise we format portal name properly.
					allowedAPPortals[portalIndex] = allowedPortal + "=" + allowedPortal;
				}
			}

			allowedAPPortals = ["EMPTY="].concat(allowedAPPortals);
			Controls.Customer_AP_portal__.SetAvailableValues(allowedAPPortals);
		}

		this.InitEvent();
	},
	InitEvent: function ()
	{
		var that = this;
		Controls.Customer_AP_portal_password_temp__.OnChange = function ()
		{
			var tempPassword = Controls.Customer_AP_portal_password_temp__.GetValue();

			if (!tempPassword)
			{
				Controls.Customer_AP_portal_password__.SetValue("");
			}
			else if (tempPassword === that._dummyPassword)
			{
				Controls.Customer_AP_portal_password__.SetValue(that._originalEncryptedPassword);
			}
			else
			{
				that.EncryptPassword(tempPassword);
			}
		};

		Controls.Save.OnClick = function ()
		{
			if (that._encryptionInProgress)
			{
				Controls.Customer_AP_portal_password__.Wait(true);
				this._saveAndExitFormAfterEncryption = true;
				return false;
			}

			return true;
		};
	},
	EncryptPasswordCallback: function (encryptResult)
	{
		this._encryptionInProgress = false;

		Controls.Customer_AP_portal_password__.Wait(false);

		if (encryptResult.success)
		{
			Controls.Customer_AP_portal_password__.SetValue(encryptResult.cipherText);

			if (this._saveAndExitFormAfterEncryption)
			{
				this._saveAndExitFormAfterEncryption = false;
				ProcessInstance.SaveAndQuit("Save");
			}
		}
		else
		{
			this._saveAndExitFormAfterEncryption = false;
			Log.Error("Crypto.Encrypt: " + encryptResult.error);

			Popup.Alert(Language.Translate("_An error occurred while saving new password"),
				true,
				null,
				Language.Translate("_Error"));
		}
	},
	EncryptPassword: function (password)
	{
		var prodPublicKey = "m13piH6XPSyUtCHDDDchnlvu03oc1d1e+s0aSH0IZpQuOCt+KEMOQg03JdeLaL4dR+V04JH2IgxBMNmabl55eBH/ogGk692cbWdl/kB22i9nKywuaFxiAHXwp94/aK96+o4eGAItrQDLTBbFbRsxYEpL21NEdNox11ES+ZA7Bic=";
		var qaPublicKey = "tYnfy87VcKV5QhOqmhMMxB+252wv2G/YVdjoge4QlArCaf7XdLbTQd8trMbwHV0RAau7L2HBlrf8xL6i1++fZkKCPzIIfMW2Ov8Pb8F5zgywmPNV+DMjotTbMIQkqGU7cnMYnViXqgnTi016CMya2R1wy59/oPICG36/i3kyD4E=";

		var publicKey = ProcessInstance.isProductionPlatform ?
			prodPublicKey :
			qaPublicKey;

		this._encryptionInProgress = true;

		Crypto.Encrypt({ callback: this.EncryptPasswordCallback.bind(this), data: password, algorithm: "rsa", key: publicKey });
	}
};

//Do some GetParameter
var EDIHelper = {
	Init: function ()
	{
		var allEdiDeliverySystems = Controls.EDIPartnerCustomer__.GetAvailableValues();
		var isExternalPortalCustomer = ProcessInstance.extendedProperties.preferredDeliveryMethod === "EXTERNALPORTAL";
		Controls.CoupaCustomerInformationPane.SetHelpId("1851");

		if (!isExternalPortalCustomer)
		{
			var currentValue = Controls.EDIPartnerCustomer__.GetValue();
			var ediDeliverySystems = Sys.AR.Client.FilterEnabledEdiDeliverySystems(currentValue, allEdiDeliverySystems);
			Sys.AR.Client.SetComboBoxAvailableValues(Controls.EDIPartnerCustomer__, ediDeliverySystems);
		}

		this.InitEvent();
	},
	InitEvent: function ()
	{
		Controls.Chorus_Structure_Identifier__.OnColumnFormating = this.TranslateChorusStatus;
		Controls.Chorus_Service_Identifier__.OnColumnFormating = this.TranslateChorusStatus;
		Controls.EDIPartnerCustomer__.OnChange = this.OnDeliverySystemChange;
		Controls.Billing_Country__.OnChange = function ()
		{
			Controls.Billing_Country__.SetValue(Controls.Billing_Country__.GetValue().toUpperCase());
		};
	},
	GetPartner: function (partner)
	{
		var allPartners = {
			pec: ["pec", "italian-b2b"],
			tenor: ["tenor", "edicom", "b-process", "cegedim", "ob10", "obs", "prologue", "generix", "ariba", "bavel", "edt", "seres", "direct-commerce"],
			indicom: ["italian-fa", "italian-pa", "italian-b2b"],
			eIntegration: ["intecon", "tradeshift", "basware", "e-integration", "io-market", "pagero", "saphety", "esker-edi-services", "spanish-b2b", "germany-pa", "germany-pa-email"],
			face: ["spanish-pa"],
			billexco: ["swiss-pa"],
			chorus: ["chorus-pro"],
			peppol: ["belgium-pa", "austria-pa", "denmark-pa", "england-pa", "germany-pa-bbw", "netherlands-pa", "norway-pa", "poland-pa", "sweden-pa",
				"luxembourg-pa", "peppol", "peppol-sg", "peppol-hr", "peppol-3.0", "peppol-anz"],
			germanPAEmail: ["germany-pa-email"]
		};

		return {
			isPec: allPartners.pec.indexOf(partner) >= 0,
			isTenor: allPartners.tenor.indexOf(partner) >= 0,
			isIndicom: allPartners.indicom.indexOf(partner) >= 0,
			isEIntegration: allPartners.eIntegration.indexOf(partner) >= 0,
			isFace: allPartners.face.indexOf(partner) >= 0,
			isBillexco: allPartners.billexco.indexOf(partner) >= 0,
			isChorusPro: allPartners.chorus.indexOf(partner) >= 0,
			isPeppol: allPartners.peppol.indexOf(partner) >= 0,
			isGermanPAEmail: allPartners.germanPAEmail.indexOf(partner) >= 0
		};
	},
	TranslateChorusStatus: function (attributeName, value)
	{
		if (attributeName === "Actif")
		{
			switch (value)
			{
				case "0":
					value = "_Inactive";
					break;
				case "1":
					value = "_Active";
					break;
				default:
					value = "_Inactive";
			}
		}
		return value;
	},
	DoesPeppolParticipantExistsCallback: function (response)
	{
		ProcessInstance.SetSilentChange(true);
		if (response.status !== 200)
		{
			Data.SetError("PartnerCustomID__", "_The Participant ID was not found in the PEPPOL SMP");
			Data.SetError("PartnerCustomIDType__", "_The Participant ID was not found in the PEPPOL SMP");
		}
		else
		{
			Data.SetError("PartnerCustomID__", "");
			Data.SetError("PartnerCustomIDType__", "");
		}
		ProcessInstance.SetSilentChange(false);
	},
	CheckPeppolParticipantIfNeeded: function ()
	{
		var checkPeppolCustomer = Sys.AR.GetParameter("Check_Peppol_participant_ID_and_supported_types__") === "1";
		if (checkPeppolCustomer)
		{
			var id = Controls.PartnerCustomID__.GetValue();
			var idType = Controls.PartnerCustomIDType__.GetValue();
			if (id && idType)
			{
				var peppolTargetEnvironment = "PROD";
				idType = Sys.AR.EDI.Peppol.Common.TryMapPEPPOL20ToPEPPOL30EndpointIdType(idType);
				Sys.AR.Client.Peppol.ParticipantExists(idType, id, peppolTargetEnvironment, EDIHelper.DoesPeppolParticipantExistsCallback);
			}
		}
	},
	OnDeliverySystemChange: function ()
	{
		Controls.Customer_AP_portal__.Hide(!APPortalHelper._isExternalPortalCustomer);
		Controls.CustomerAPPortalPane.Hide(!APPortalHelper._isExternalPortalCustomer);

		Controls.EDIPartnerCustomer__.Hide(APPortalHelper._isExternalPortalCustomer);
		var partnerName = Controls.EDIPartnerCustomer__.GetValue();
		var partner = EDIHelper.GetPartner(partnerName);

		Controls.SimpleIDPane.Hide(!partner.isTenor && !partner.isIndicom && !partner.isBillexco && !partner.isPeppol && !partner.isEIntegration && !partner.isPec);
		Controls.PartnerCustomID__.Hide(partnerName === "pec");
		Controls.PEC_Email__.Hide(!partner.isPec);
		Controls.FACEPane.Hide(!partner.isFace);
		Controls.ChorusPane.Hide(!partner.isChorusPro);
		Controls.PartnerCustomIDType__.Hide(!partner.isPeppol && !partner.isEIntegration && !partner.isTenor && !partner.isChorusPro);
		Controls.Party_identification__.Hide(!partner.isGermanPAEmail);
		Controls.Party_identification_type__.Hide(!partner.isGermanPAEmail);
		Controls.CoupaCustomerInformationPane.Hide(partnerName !== "coupa");

		if (partner.isChorusPro)
		{
			Sys.AR.Client.ChorusPro.Init();
			Controls.CHORUS_CodeServiceExecutant__.Hide(true);
		}

		if (partner.isBillexco)
		{
			Controls.PartnerCustomID__.SetBrowsable(true);
			Controls.PartnerCustomID__.OnBrowse = Sys.AR.Client.Billexco.PA.OnBrowsePAsDialog;
			Controls.PartnerCustomID__.OnChange = EDIHelper.OnDeliverySystemChange;

			// If the customer has a business link, let's find it on the system data
			// to display the correct Billexo_HTML__ field on init
			var PAString = "";
			var businessLink = Controls.PartnerCustomID__.GetValue();
			if (businessLink)
			{
				var jsonPA = Sys.AR.Client.Billexco.PA.GetPAJSONByBusinessLink(businessLink);
				PAString = jsonPA ?
					jsonPA.Company_Name__ + " - " + jsonPA.NIF__ :
					"";
			}
			Sys.AR.Client.Billexco.PA.FormatAndDisplayBillexcoHTMLField(PAString);
		}
		if (partner.isPeppol)
		{
			Controls.PartnerCustomID__.OnChange = EDIHelper.OnDeliverySystemChange;
			Controls.PartnerCustomIDType__.OnChange = EDIHelper.OnDeliverySystemChange;
			EDIHelper.CheckPeppolParticipantIfNeeded();
		}
		else
		{
			Controls.PartnerCustomID__.OnChange = null;
			Controls.PartnerCustomID__.SetBrowsable(false);
			Controls.Billexco_HTML__.Hide(true);
		}
	}
};

var CMHelper =
{
	Init: function ()
	{
		if (!Sys.AR.GetParameter("EnableCreditApplication"))
		{ return; }

		Controls.CreditScore_CompanyIdentifier__.OnChange = this.onChange;
		Controls.PaymentTermsDisplay__.SetLabel(
			Language.Translate(
				Controls.PaymentTerms__.GetAvailableValues(false, true)[Controls.PaymentTerms__.GetValue()]
			).toUpperCase()
		);
	},
	onChange: function ()
	{
		ProcessInstance.UpdateRecord(function (lastErrorMessage)
		{
			if (!lastErrorMessage)
			{
				ProcessInstance.SetSilentChange(true);
				Data.SetValue("ThirdPartiesIds__", "");
				ProcessInstance.SetSilentChange(false);
			}
		}, { ThirdPartiesIds__: "" });
	}
};

var ARHelper =
{
	Init: function ()
	{
		if (!ProcessInstance.extendedProperties.appInstances.AR)
		{
			return;
		}
		Lib.CM.CustomerHelper.GetTotalExposure({
			customerId: NavMenuHelper.Menus.invoices._customerId,
			creditLimit: Data.GetValue("CreditLimit__"),
			callback: function (data)
			{
				if (data.error)
				{
					Controls.TotalOutstanding__.SetError("_Error");
					Controls.ImportedOrderCounter__.SetError("_Error");
					Controls.TotalExposure__.SetError("_Error");
					return;
				}

				Controls.TotalOutstanding__.SetValue({
					value: Lib.CM.CustomerHelper.CurrencyFormat(data.totalReceivablesAmount, data.currency),
					subValue: Language.Translate("_{0} invoice(s)", true, data.nbReceivablesInvoices)
				});

				Controls.ImportedOrderCounter__.SetValue({
					value: Lib.CM.CustomerHelper.CurrencyFormat(data.pendingOrdersAmount, data.currency),
					subValue: Language.Translate("_{0} order(s)", true, data.nbPendingOrders)
				});

				Controls.TotalExposure__.SetValue({
					value: Lib.CM.CustomerHelper.CurrencyFormat(data.totalExposure)
				});
			}
		});
	}
};

var TermSyncHelper = {
	_termSyncEnabled: false,
	Init: function ()
	{
		TermSyncHelper._termSyncEnabled = Sys.AR.GetParameter("Enable_Termsync_Synchronization", false);
		if (TermSyncHelper._termSyncEnabled)
		{
			Sys.AR.TermSync.Client.GetCustomerInfos(
				ProcessInstance.extendedProperties.businessPartnerID, Data.GetValue("Currency__") || "EUR",
				function (customerInfos)
				{
					if (customerInfos)
					{
						NavMenuHelper.Menus.overview.DisplayADP(customerInfos.AverageDSO);
						TermSyncHelper.DisplayPayerRating(customerInfos.PayerRating);
					}
					else
					{
						NavMenuHelper.Menus.overview.DisplayADP(null);
						TermSyncHelper.DisplayPayerRating(null);
					}
				});
		}
	},
	DisplayPayerRating: function (rating)
	{
		if (rating === "unknown" || !rating)
		{
			Controls.PayerRatingHTML__.FireEvent("onDisplayPayerRating", {});
			Controls.PayerRatingDescription__.SetText("_NotEnoughDataTermSync");
			Controls.PayerRatingSpacer__.Hide(false);
			Controls.PayerRatingDescription__.Hide(false);
		}
		else
		{
			Controls.PayerRatingHTML__.FireEvent("onDisplayPayerRating", Lib.CM.CustomerHelper.GetPayerRatingLevel(rating));
			Controls.PayerRatingDescription__.SetText(rating);
			Controls.PayerRatingSpacer__.SetHeight("10px");
			Controls.PayerRatingSpacer__.Hide(false);
			Controls.PayerRatingDescription__.Hide(false);
		}
	}
};

var CustomerDetailsHelper = {
	isMainContact: false,
	mainContactLogin: "",
	company: {},
	Init: function ()
	{
		var customerId = Controls.CustomerID__.GetValue();
		if (customerId)
		{
			var customerIdParts = customerId.split('$');
			if (customerIdParts.length === 2)
			{
				customerId = customerIdParts[1];
			}
			Controls.CustomerIdDisplay__.SetText(customerId);
		}

		if (ProcessInstance.extendedProperties)
		{
			var props = ProcessInstance.extendedProperties;
			Controls.BusinessPartnerId__.SetValue(ProcessInstance.extendedProperties.businessPartnerID);
			var customerInfos = {
				"name": props.displayName,
				"customerID": props.login.split("$")[1],
				"email": props.emailAddress,
				"deliveryMethod": props.preferredDeliveryMethod,
				"phoneNumber": props.phoneNumber,
				"profileId": props.profileId,
				"profileName": props.profileName,
				"company": props.company,
				"businessPartnerID": props.businessPartnerID
			};

			this.isMainContact = props.businessPartnerID === customerId || !props.businessPartnerID;
			this.mainContactLogin = props.businessPartnerID ? props.login.substring(0, props.login.indexOf('$') + 1) + props.businessPartnerID : props.login;
			this.SetDetails(customerInfos);

			if (Sys.AR.GetParameter("EnableCreditApplication"))
			{
				this.GetCompanyProps(function ()
				{
					if (!this.isMainContact)
					{
						ProcessInstance.SetSilentChange(true);
						Controls.CreditScore_Enabled__.SetValue(this.company.creditScoreEnabled);
						ProcessInstance.SetSilentChange(false);
					}
				});
			}

			Controls.DeliveryMethod__.SetValue(props.preferredDeliveryMethod);
			var allowDMControls = {
				SM: Controls.EmailAttachment__, MOD: Controls.PostalMail__, FGFAXOUT: Controls.Fax__,
				PORTAL: Controls.PortalPublication__, EDI: Controls.EDI__, OTHER: Controls.OtherDelivery__
			};

			for (var i = 0; i < props.allowedDeliveryMethods.length; i++)
			{
				if (allowDMControls[props.allowedDeliveryMethods[i]])
				{
					allowDMControls[props.allowedDeliveryMethods[i]].SetValue(true);
				}
			}

			Controls.SendCopyToEmailAddresses__.SetValue(props.deliveryCopyEmail);
			Controls.CreationDateTime__.SetValue(props.creationDateTime);
			Controls.LastLoginDateTime__.SetValue(props.lastConnectionDateTime);
			Controls.LastWelcomeEmailSendDate__.SetValue(props.lastWelcomeEmailDateTime);
			Controls.EInvoicingAcceptanceDate__.SetValue(props.eAgreementDateTime);
			Controls.Options__.SetValue(props.options);
			Controls.PasswordFree__.SetValue((parseInt(props.loginType, 10) & 16) === 16);	//16 => mask for login free password
			Controls.Lock__.SetValue(props.locked);
		}
	},
	SetDetails: function (details)
	{
		//main contact
		if (this.isMainContact)
		{
			Controls.Customer_Info__.SetHTML(Controls.Customer_Info__.GetHTML().replace("fa-user-circle", "fa-building-o"));
		}
		else if (details.company)
		{
			Controls.CompanyName__.SetText(Language.Translate("_CompanyName", false) + " " + details.company);
		}

		var deliveryMethodTrad = details.deliveryMethod ? Language.Translate("_" + details.deliveryMethod + "_customerDetails", false) : "";
		//GeneralPane in preferences
		Controls.ProfileLink__.SetText(details.profileName);
		Controls.ProfileLink__.SetURL("../profile.aspx?id=" + details.profileId);

		Controls.Customer_Info__.BindEvent("onCustomerInfoLoad", function ()
		{
			Controls.Customer_Info__.FireEvent("onCustomerInfoLoad", {
				customerInfo: {
					name: CustomerDetailsHelper.isMainContact ? details.company : details.name,
					customerNumber: details.customerID,
					subName: CustomerDetailsHelper.isMainContact ? details.name : "",
					phoneNumber: details.phoneNumber,
					email: details.email,
					deliveryMethod: deliveryMethodTrad
				}
			});
		});

	},
	FillOrHideField: function (value, control)
	{
		if (value)
		{
			control.SetText(value);
		}
		else
		{
			control.Hide(true);
		}
	},
	FillUserInformation: function (callback)
	{
		var mapping = {
			ADDITIONALFIELD1: "Additional_Field1__",
			ADDITIONALFIELD2: "Additional_Field2__",
			ADDITIONALFIELD3: "Additional_Field3__",
			ADDITIONALFIELD4: "Additional_Field4__",
			ADDITIONALFIELD5: "Additional_Field5__",
			CITY: "City__",
			COMPANY: "Company__",
			COUNTRY: "Country__",
			//DELIVERYCOPYEMAIL: "Send_Copy_Email__",
			DESCRIPTION: "Additional_Information__",
			DISPLAYNAME: "Display_Name__",
			EMAILADDRESS: "Email_Address__",
			FAXNUMBER: "Fax_Number__",
			FIRSTNAME: "First_Name__",
			LASTNAME: "Last_Name__",
			MAILSTATE: "Mail_State__",
			MAILSUB: "MailSub__",
			MIDDLENAME: "Middle_Name__",
			MOBILENUMBER: "Mobile_Number__",
			PHONENUMBER: "Phone_Number__",
			STREET: "Street__",
			POBOX: "POBox__",
			TITLE: "Title__",
			ZIPCODE: "Zip_Code__",
			DISPLAYWELCOMESCREEN: "Display_Welcome_Page__"
		};

		var complexMapping = {
			LANGUAGE: function (val)
			{
				Language.GetAvailableLanguages({
					userType: "customer", callback: function (data)
					{
						if (!data.error && data.languages && data.languages[val])
						{
							Controls.Language__.SetText(data.languages[val].Display);
						}

						readyState.Ready("language");
					}
				});
			},
			CULTURE: function (val)
			{
				Language.GetAvailableCultures({
					callback: function (data)
					{
						if (!data.error && data.cultures && data.cultures[val])
						{
							Controls.Regional_Settings__.SetText(data.cultures[val].Name);
						}
						readyState.Ready("culture");
					}
				});
			},
			EXPORTENCODING: function (val)
			{
				val = val || "_Default";
				Language.Translate({
					async: true,
					key: "_Culture",
					module: "user",
					callback: function (dataCulture)
					{
						if (!dataCulture.error)
						{
							Language.Translate({
								async: true,
								key: val,
								module: "exportencoding",
								replacements: [dataCulture.formatted],
								callback: function (dataEncoding)
								{
									if (!dataEncoding.error)
									{
										Controls.Export_Encoding__.SetText(dataEncoding.formatted);
									}

									readyState.Ready("encoding");
								}
							});
						}
					}
				});
			},
			TIMEZONEINDEX: function (val)
			{
				Language.Translate({
					async: true,
					key: val,
					module: "timezone",
					callback: function (data)
					{
						if (!data.error)
						{
							Controls.Time_Zone__.SetText(data.formatted);
						}

						readyState.Ready("timezone");
					}
				});
			}
		};

		var readyState = {
			callback: callback,
			Ready: function (module)
			{
				this[module] = true;
				if (this.callback && this.language && this.culture && this.encoding && this.timezone)
				{
					this.callback();
					ProcessInstance.SetSilentChange(false);
				}
			}
		};

		Query.DBQuery(function ()
		{
			ProcessInstance.SetSilentChange(true);
			for (var prop in mapping)
			{
				Controls[mapping[prop]].SetValue(this.GetQueryValue(prop));
			}

			for (var cProps in complexMapping)
			{
				complexMapping[cProps](this.GetQueryValue(cProps), this);
			}

			Controls.Customer_Number__.SetValue(ProcessInstance.extendedProperties.businessPartnerID);
			readyState.Ready("common");

		}, "ODUSER", Object.keys(mapping).concat(Object.keys(complexMapping)).join("|"), "(msn=" + ProcessInstance.extendedProperties.msn + ")", null, 1, null, "FastSearch=1");
	},
	_queryGetCompanyPropsDone: false,
	GetCompanyProps: function (callback)
	{
		if (this.isMainContact)
		{
			if (!Data.GetValue("CreditScore_CompanyIdentifier__"))
			{
				ProcessInstance.SetSilentChange(true);
				Data.SetValue("CreditScore_CompanyIdentifier__", ProcessInstance.extendedProperties.msn);
				this.saveScoreId = true;
				ProcessInstance.SetSilentChange(false);
			}

			this.company = { creditScoreId: Data.GetValue("CreditScore_CompanyIdentifier__"), creditScoreEnabled: Sys.Helpers.String.ToBoolean(Data.GetValue("CreditScore_Enabled__")) };
			if (typeof callback === "function")
			{
				callback.apply(this);
				return;
			}
		}

		if (!this._queryGetCompanyPropsDone && this.mainContactLogin)
		{
			Query.DBQuery(function ()
			{
				CustomerDetailsHelper.company = { creditScoreId: this.GetQueryValue("CreditScore_CompanyIdentifier__"), creditScoreEnabled: Sys.Helpers.String.ToBoolean(this.GetQueryValue("CreditScore_Enabled__")) };
				if (typeof callback === "function")
				{
					callback.apply(CustomerDetailsHelper);
				}

			}, "Customer_Extended_Delivery_Properties__", "CreditScore_CompanyIdentifier__|CreditScore_Enabled__", "(CustomerID__=" + this.mainContactLogin + ")");

			this._queryGetCompanyPropsDone = true;
		}
	}
};

var NavMenuHelper = {
	_defaultRightPanes: {},
	Menus: {
		overview: {
			panes: { CountersPane: true, ReportsPane: true, OrdersCountersPane: true, RiskPane: true, RiskCategoryPane: true, PaymentBehaviorPane: true, ADPPane: true, PayerRatingPane: true, DeductionsCountersPane: true },
			init: function ()
			{
				var counters = PackageHelper.getCounters();
				var reports = PackageHelper.getReports();
				var htmlContents = PackageHelper.getHTMLContents();
				var controls = counters.concat(reports).concat(htmlContents);

				this.panes.CountersPane = ProcessInstance.extendedProperties.appInstances.AR && counters.length > 0;
				this.panes.OrdersCountersPane = (ProcessInstance.extendedProperties.appInstances.COP || Sys.AR.GetParameter("EnableCreditApplication")) && counters.length > 0;
				this.panes.RiskPane = Sys.AR.GetParameter("EnableCreditApplication");
				this.panes.RiskCategoryPane = Sys.AR.GetParameter("EnableCreditApplication");
				this.panes.PaymentBehaviorPane = Sys.AR.GetParameter("EnableCreditApplication");
				this.panes.PayerRatingPane = TermSyncHelper._termSyncEnabled;
				this.panes.DeductionsCountersPane = ProcessInstance.extendedProperties.appInstances.DEDUCTIONS;
				this.panes.ReportsPane = reports.length > 0;
				this.panes.ADPPane = ProcessInstance.extendedProperties.appInstances.AR;

				var customerId = ProcessInstance.extendedProperties.businessPartnerID;
				if (!customerId)
				{
					customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}
				var customerFilter = "(|(Customer_ID__=" + customerId + ")(CustomerNumber__=" + customerId + "))";
				for (var i = 0; i < controls.length; i++)
				{
					controls[i].Hide(false);
					if (typeof controls[i].SetAdditionalFilter === "function")
					{
						controls[i].SetAdditionalFilter(customerFilter);
						controls[i].Refresh();
					}
				}

				if (Sys.AR.GetParameter("EnableCreditApplication") && Data.GetValue("EnableHistoric__"))
				{
					Lib.CM.CustomerHelper.GetHistoricalData({
						customerId: customerId,
						dataLabelsFormatter: Lib.CM.CustomerHelper.CurrencyFormat,
						callback: function (historicalData)
						{
							Controls.Historic__.SetValue(historicalData);
						}
					});
				}

				if (!TermSyncHelper._termSyncEnabled)
				{
					NavMenuHelper.Menus.overview.DisplayADP(Data.GetValue("ADP__"));
				}
			},
			DisplayADP: function (value)
			{
				if (value)
				{
					Controls.ADPSpacer__.SetHeight("5px");
					Controls.ADPCounter__.SetValue({ value: value });
				}
				else
				{
					Controls.ADPCounter__.Hide(true);
					Controls.ADPNoData__.Hide(false);
				}
			}
		},
		invoices: {
			_views: {
				allInvoices: "_Invoices customer - embedded",
				overdueInvoices: "_Customer invoices payment late - embedded",
				outstandingInvoices: "_Customer invoices unpaid - embedded",
				"60daysInvoices": "_Customer invoices 60 days - embedded",
				creditNotesInvoices: "_Customer credits 60 days - embedded",
				disputeInvoices: "_Disputed invoices supplier - embedded"
			},
			_customerId: ProcessInstance.extendedProperties.businessPartnerID,
			panes: { InvoicesPane: true },
			init: function ()
			{
				if (!this._customerId)
				{
					this._customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}

				var that = this;
				Controls.InvoiceMenu__.BindEvent("onClick", function (evt)
				{
					that.changeInvoicesView(that._views[evt.args]);
				});
				Controls.InvoiceMenu__.BindEvent("onLoad", function ()
				{
					var tabs = {};
					for (var t in that._views)
					{
						tabs[t] = {};
						tabs[t].trad = Language.Translate(that._views[t]);
						if ((t === "60daysInvoices" || t === "creditNotesInvoices") && Sys.AR.GetParameter("EnablePaymentFeature"))
						{
							tabs[t].hidden = true;
						}
						else if ((t === "overdueInvoices" || t === "outstandingInvoices") && !Sys.AR.GetParameter("EnablePaymentFeature"))
						{
							tabs[t].hidden = true;
						}
						else if (t === "disputeInvoices" && !Sys.AR.GetParameter("EnableDisputeFeature"))
						{
							tabs[t].hidden = true;
						}
					}
					Controls.InvoiceMenu__.FireEvent("onInvoicesLoad", { tabs: tabs });
				});
				Controls.Overdue__.OnClick = function ()
				{
					that.displayView("overdueInvoices");
				};
				Controls.TotalOutstanding__.OnClick = function ()
				{
					that.displayView("outstandingInvoices");
				};
				Controls.Disputes__.OnClick = function ()
				{
					that.displayView("disputeInvoices");
				};
				Controls.Invoices60Days__.OnClick = function ()
				{
					that.displayView("60daysInvoices");
				};
				Controls.CreditNotes60Days__.OnClick = function ()
				{
					that.displayView("creditNotesInvoices");
				};
				this.changeInvoicesView(this._views.allInvoices);
			},
			displayView: function (viewName)
			{
				Controls.NavMenu__.FireEvent("onChangeTab", { tabId: "invoices" });
				Controls.InvoiceMenu__.FireEvent("onChangeInvoiceView", { viewName: viewName });
			},
			changeInvoicesView: function (viewName)
			{
				Controls.Invoices__.SetView({
					tabName: "_EInvoice supplier - embedded",
					filter: "(Customer_ID__=" + this._customerId + ")",
					viewName: viewName,
					processOrTableName: "Customer invoices (supplier copy)",
					checkProfileTab: false
				});
				Controls.Invoices__.Apply();
			}
		},
		payments: {
			_views_payments: {
				cashAppPayments: "_View_Payments for customers embedded",
				portalPayments: "_View_Payments portal for customers embedded"
			},
			panes: { PaymentsPane: true },
			init: function ()
			{
				var that = this;

				Controls.Payment_NavMenu__.BindEvent("onClick", function (evt)
				{
					that.changePaymentView(that._views_payments[evt.args]);
				});
				Controls.Payment_NavMenu__.BindEvent("onLoad", function ()
				{
					var trads = {};
					for (var t in that._views_payments)
					{
						trads[t] = Language.Translate(that._views_payments[t]);
					}
					Controls.Payment_NavMenu__.FireEvent("onPaymentLoad", {
						IsDisplay: function (id)
						{
							if (id === "cashAppPayments")
							{
								return Sys.AR.GetParameter("EnableCashApplication");
							}
							if (id === "portalPayments")
							{
								return Sys.AR.GetParameter("EnablePaymentFeature");
							}
							return true;
						},
						defaultTab: Sys.AR.GetParameter("EnablePaymentFeature") ? 1 : 0,
						tabs: trads
					});
				});
			},
			onStart: function ()
			{
				if (Sys.AR.GetParameter("EnablePaymentFeature"))
				{
					this.changePaymentView(this._views_payments.portalPayments);
				}
				else
				{
					this.changePaymentView(this._views_payments.cashAppPayments);
				}
			},
			changePaymentView: function (viewName)
			{
				var customerId = ProcessInstance.extendedProperties.businessPartnerID;

				if (!customerId)
				{
					customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}

				if (viewName === "_View_Payments for customers embedded")
				{
					Controls.Payments__.SetView({
						tabName: "_Tab_Payments embedded",
						filter: "(&(Line_Col_customer_id__=" + customerId + ")(Status_Alloc__=FULLY_ALLOCATED))",
						viewName: viewName,
						checkProfileTab: false,
						processOrTableName: null
					});
				}
				else
				{
					Controls.Payments__.SetView({
						tabName: "_Tab_Payments embedded",
						filter: "(&(Customer_id__=" + customerId + "))",
						viewName: viewName,
						checkProfileTab: false,
						processOrTableName: "AR System Invoices Payment"
					});
				}

				Controls.Payments__.Apply();
			}
		},
		creditScore: {
			_initialized: false,
			panes: {
				CreditScorePane: true,
				RiskPane: true,
				RiskCategoryPane: true,
				PaymentBehaviorPane: true,
				CreditApplicationPane: true,
				ADPPane: true,
				CreditHistoryPane: true,
				PayerRatingPane: TermSyncHelper._termSyncEnabled
			},
			gaugeContainer: Controls.CreditScore_GaugeContainer__,
			init: function ()
			{
				Query.DBQuery(function ()
				{
					if (!CustomerDetailsHelper.isMainContact)
					{ return; }

					Controls.CreditLimitCounter__.AddAction({ action: "review", label: "_Review" });
					var state = parseInt(this.GetQueryValue("state"), 10);
					if (!isNaN(state) && state < 100)
					{
						Controls.CreditLimitCounter__.SetLabel({ rightIcon: { awesomeClasses: "fa fa-exclamation-triangle", color: "#ff8700" } });
						var currentCredAppId = this.GetQueryValue("ruidex");
						Controls.CreditLimitCounter__.OnAction = function (action)
						{
							if (action === 'review')
							{
								Process.OpenMessage(currentCredAppId, true);
							}
						};
					}
					else
					{
						Controls.CreditLimitCounter__.OnAction = function (action)
						{
							if (action === 'review')
							{
								Lib.CM.CreditApplicationReview.Show({
									customerId: CustomerDetailsHelper.mainContactLogin,
									creditLimit: Data.GetValue("CreditLimit__"),
									currency: Data.GetValue("Currency__"),
									paymentTerms: Controls.PaymentTerms__.GetValue(),
									riskCategory: Controls.RiskCategory__.GetValue(),
									paymentTermsValues: Controls.PaymentTerms__.GetAvailableValues(),
									riskCategoriesValues: Controls.RiskCategory__.GetAvailableValues(),
									adp: Data.GetValue("ADP__"),
									credAppCreationCallback: function (data)
									{
										Controls.CreditLimitCounter__.SetLabel({ rightIcon: { awesomeClasses: "fa fa-exclamation-triangle", color: "#ff8700" } });
										Controls.CreditLimitCounter__.OnAction = function (action)
										{
											if (action === 'review')
											{
												if (data.error)
												{ Popup.Alert("_The previous review request is in error. Check it and try again.", true, null, "Error"); }
												else
												{ Process.OpenMessage(data.ruid, true); }
											}
										};

										var snackOpts = data.error ?
											{ message: Language.Translate("_Review request is in error"), status: "error", closable: true } :
											{ message: Language.Translate("_Review request is successfully created"), status: "success", closable: true };

										Popup.Snackbar(snackOpts);
									}
								});
							}
						};
					}
				},
					"CDNAME#CM - Credit Application",
					"ruidex|state",
					"(&(CustomerLogin__=" + CustomerDetailsHelper.mainContactLogin + ")(state<=100)(deleted=0))",
					"SubmitDateTime DESC",
					1);

				Controls.RiskCategoryHTML__.BindEvent("onLoad", function ()
				{
					var value = Controls.RiskCategory__.GetValue();
					var translatedValue = "";
					var color = "";
					switch (value)
					{
						case "0":
							translatedValue = Language.Translate("_RiskVeryLow");
							color = "veryLow";
							break;
						case "1":
							translatedValue = Language.Translate("_RiskLow");
							color = "low";
							break;
						case "2":
							translatedValue = Language.Translate("_RiskModerate");
							color = "moderate";
							break;
						case "3":
							translatedValue = Language.Translate("_RiskMedium");
							color = "medium";
							break;
						case "4":
							translatedValue = Language.Translate("_RiskHigh");
							color = "high";
							break;
						case "5":
							translatedValue = Language.Translate("_RiskExtreme");
							color = "extreme";
							break;
						case "6":
							translatedValue = Language.Translate("_RiskOutOfBusiness");
							color = "outofbusiness";
							break;
						default:
							translatedValue = Language.Translate("_RiskNone");
							color = "none";
					}

					Controls.RiskCategoryHTML__.FireEvent("onDisplayRiskCategory", { value: translatedValue, color: color });
				});
				if (!Sys.AR.GetParameter("EnableCreditApplication"))
				{ return; }
				var nextReview = Controls.NextReviewCM__.GetValue() || null;
				Controls.CreditLimitCounter__.SetValue({
					value: Data.GetValue("CreditLimit__") ? Lib.CM.CustomerHelper.CurrencyFormat(Data.GetValue("CreditLimit__")) : "N/A",
					subText: nextReview ? Language.Translate("_NextReviewCounter", false, Sys.Helpers.Date.ToLocaleDateEx(nextReview, User.culture)) : null
				});


				var counters = PackageHelper.getCounters();
				this.panes.PayerRatingPane = TermSyncHelper._termSyncEnabled;

				var customerId = ProcessInstance.extendedProperties.businessPartnerID;

				if (!customerId)
				{
					customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}

				Controls.CreditApplicationView__.SetView({
					tabName: "_CA tab",
					filter: "(CustomerNumber__=" + customerId + ")",
					viewName: "_CAEmbedded",
					processOrTableName: "CM - Credit Application",
					checkProfileTab: false
				});
				Controls.CreditHistoryView__.SetView({
					tabName: "_Tables",
					filter: "(CompanyID__=" + customerId + ")",
					viewName: "_view_O2C_Audit_Embedded",
					processOrTableName: "O2C - Audit__",
					checkProfileTab: false
				});

				if (Lib.CM)
				{
					Lib.CM.ScoringProviders.Init(this.gaugeContainer);
				}
			},
			onStart: function ()
			{
				if (!this._initialized)
				{
					this._initialized = true;
					//TODO REQUEST ODUSER TO GET Address postalCode country if CreditRiskMonitor isn't present
					var company = {
						companyName: ProcessInstance.extendedProperties.company,
						countryCode: ProcessInstance.extendedProperties.country.toUpperCase(),
						thirdPartiesIds: JSON.parse(Data.GetValue("ThirdPartiesIds__")) || {},
						scoreId: CustomerDetailsHelper.company.creditScoreId
						//streetAddress: null,
						//cityName: null,
						//postalCode: null,
						//countryCode: null
					};

					var creditScoreEnabled = Controls.CreditScore_Enabled__.GetValue();
					Lib.CM.ScoringProviders.OnStart({
						company: company,
						StoreThirdPartiesId: function (businessId, thirdId)
						{
							creditScoreEnabled = true;
							company.thirdPartiesIds[thirdId] = businessId;
							var recordValues = { ThirdPartiesIds__: JSON.stringify(company.thirdPartiesIds), CreditScore_Enabled__: 1 };
							if (CustomerDetailsHelper.saveScoreId)
							{
								recordValues.CreditScore_CompanyIdentifier__ = Data.GetValue("CreditScore_CompanyIdentifier__");
							}

							ProcessInstance.UpdateRecord(function (lastErrorMessage)
							{
								if (!lastErrorMessage)
								{
									ProcessInstance.SetSilentChange(true);
									Data.SetValue("ThirdPartiesIds__", recordValues.ThirdPartiesIds__);
									Data.SetValue("CreditScore_Enabled__", true);
									ProcessInstance.SetSilentChange(false);
								}
							}, recordValues);
						},
						EnabledCreditScore: function ()
						{
							if (CustomerDetailsHelper.isMainContact && !creditScoreEnabled)
							{
								creditScoreEnabled = true;
								var recordValues = { CreditScore_Enabled__: 1 };
								if (CustomerDetailsHelper.saveScoreId)
								{
									recordValues.CreditScore_CompanyIdentifier__ = Data.GetValue("CreditScore_CompanyIdentifier__");
								}

								ProcessInstance.UpdateRecord(
									function (lastErrorMessage)
									{
										if (!lastErrorMessage)
										{
											ProcessInstance.SetSilentChange(true);
											Data.SetValue("CreditScore_Enabled__", true);
											ProcessInstance.SetSilentChange(false);
										}
									}, recordValues
								);
							}
						}
					});
				}
				Controls.CreditApplicationView__.Apply();
				Controls.CreditHistoryView__.Apply();
			}
		},
		remittances: {
			panes: { RemittancesPane: true },
			init: function ()
			{
				var customerId = ProcessInstance.extendedProperties.businessPartnerID;
				if (!customerId)
				{
					customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}
				Controls.Remittances__.SetView({
					tabName: "_Tab_Remittance advices embedded",
					filter: "(&(Line_Col_customer_id__=" + customerId + ")(State=100))",
					viewName: "_View_Remittance advices embedded all customers",
					checkProfileTab: false
				});
			},
			onStart: function ()
			{
				Controls.Remittances__.Apply();
			}
		},
		deductions: {
			_views: {
				alldeductions: "_View_All embedded",
				deductionsToVerify: "_View_To verify embedded",
				deductionsToApprove: "_View_To approve embedded",
				deductionsToPost: "_View_To post embedded",
				deductionsResolved: "_View_Resolved embedded"
			},
			_customerId: ProcessInstance.extendedProperties.businessPartnerID ? ProcessInstance.extendedProperties.businessPartnerID : ProcessInstance.extendedProperties.login.split("$")[1],
			panes: { DeductionsPane: true },
			init: function ()
			{
				var that = this;
				Controls.DeductionsToValidate__.OnClick = function ()
				{
					that.displayView("deductionsToApprove");
				};
				Controls.DeductionsMenu__.BindEvent("onClick", function (evt)
				{
					that.changeDeductionsView(that._views[evt.args]);
				});
				Controls.DeductionsMenu__.BindEvent("onLoad", function ()
				{
					var tabs = {};
					for (var t in that._views)
					{
						tabs[t] = {};
						tabs[t].trad = Language.Translate(that._views[t]);
					}
					Controls.DeductionsMenu__.FireEvent("OnDeductionsLoad", { tabs: tabs });
				});
				this.changeDeductionsView(this._views.alldeductions);
			},
			changeDeductionsView: function (viewName)
			{
				Controls.DeductionsList__.SetView({
					tabName: "_Tab_Deductions embedded",
					filter: "(&(Customer_ID__=" + this._customerId + "))",
					viewName: viewName,
					checkProfileTab: false,
					processOrTableName: "Deductions Processing"
				});
				Controls.DeductionsList__.Apply();
			},
			displayView: function (viewName)
			{
				Controls.NavMenu__.FireEvent("onChangeTab", { tabId: "deductions" });
				Controls.DeductionsMenu__.FireEvent("OnChangeDeductionsView", { viewName: viewName });
			}
		},
		orders: {
			_views: {
				ordersToValidate: {
					name: "_Customer orders waiting for validation",
					tab: "_Customer orders",
					processOrTableName: "Customer Order Processing"
				},
				allImportedOrders: {
					name: "_All Imported Orders - embedded",
					tab: "_ImportedOrders - embedded",
					processOrTableName: "CM - Orders Import__"
				},
				blockedOrders: {
					name: "_Blocked Imported Orders - embedded",
					tab: "_ImportedOrders - embedded",
					processOrTableName: "CM - Orders Import__"
				}
			},
			_customerId: ProcessInstance.extendedProperties.businessPartnerID,
			panes: { OrdersPane: true },
			init: function ()
			{
				if (!this._customerId)
				{
					this._customerId = ProcessInstance.extendedProperties.login.split("$")[1];
				}
				var that = this;
				Controls.OrdersMenu__.BindEvent("onClick", function (evt)
				{
					that.changeOrdersView(that._views[evt.args]);
				});
				Controls.OrdersMenu__.BindEvent("onLoad", function ()
				{
					var tabs = {};
					for (var t in that._views)
					{
						tabs[t] = {};
						tabs[t].trad = Language.Translate(that._views[t].name);

						if ((!Sys.AR.GetParameter("EnableCreditApplication") && (t === "allImportedOrders" || t === "blockedOrders"))
							|| (!ProcessInstance.extendedProperties.appInstances.COP && t === "ordersToValidate"))
						{
							tabs[t].hidden = true;
						}
					}
					Controls.OrdersMenu__.FireEvent("onOrdersLoad", { tabs: tabs });
				});
				Controls.OrdersToValidate__.OnClick = function ()
				{
					that.displayView("ordersToValidate");
				};
				Controls.ImportedOrderCounter__.OnClick = function ()
				{
					that.displayView("allImportedOrders");
				};
				Controls.BlockedOrdersCounter__.OnClick = function ()
				{
					that.displayView("blockedOrders");
				};
				this.changeOrdersView(ProcessInstance.extendedProperties.appInstances.COP ? this._views.ordersToValidate : this._views.allImportedOrders);
			},
			displayView: function (viewName)
			{
				Controls.NavMenu__.FireEvent("onChangeTab", { tabId: "orders" });
				Controls.OrdersMenu__.FireEvent("onChangeOrderView", { viewName: viewName });
			},
			changeOrdersView: function (view)
			{
				Controls.OrdersView__.SetView({
					tabName: view.tab,
					filter: "(|(Customer_ID__=" + this._customerId + ")(CustomerNumber__=" + this._customerId + "))",
					viewName: view.name,
					processOrTableName: view.processOrTableName,
					checkProfileTab: false
				});
				Controls.OrdersView__.Apply();
			}
		},
		contacts: {
			panes: { CustomerListPane: true },
			init: function ()
			{
				var viewFilter = "";
				if (!ProcessInstance.extendedProperties.businessPartnerID)
				{
					viewFilter = "(&(login=" + ProcessInstance.extendedProperties.login + "))";
				}
				else
				{
					viewFilter = "(&(BusinessPartnerId=" + ProcessInstance.extendedProperties.businessPartnerID + "))";
				}
				Controls.CustomerList__.SetView({
					tabName: "_Customer list tab - embedded",
					viewName: "_Customer list - embedded",
					filter: viewFilter,
					checkProfileTab: false
				});
			},
			onStart: function ()
			{
				Controls.CustomerList__.Apply();
			}
		},
		activity: {
			_views: {
				allEvents: "_Default audit-history view",
				loginEvents: "_Login audit-history view",
				persoInfEditEvents: "_Modification audit-history view",
				actionsEvents: "_Transport audit-history view"
			},
			panes: { HistoryPane: true, ActivityPane: true },
			init: function ()
			{
				var that = this;
				Controls.EventsHistory_NavMenu__.BindEvent("onClick", function (evt)
				{
					that.changeActivityView(that._views[evt.args]);
				});
				Controls.EventsHistory_NavMenu__.BindEvent("onLoad", function ()
				{
					var trads = {};
					for (var t in that._views)
					{
						trads[t] = Language.Translate(that._views[t]);
					}
					Controls.NavMenu__.FireEvent("onEventHistoryLoad", { tabs: trads });
				});
			},
			onStart: function ()
			{
				this.changeActivityView(this._views.persoInfEditEvents);
			},
			changeActivityView: function (viewName)
			{
				Controls.EventHistoryView__.SetView({
					viewName: viewName,
					filter: "(&(RecipientType=AUD)(|(OwnerID=" + ProcessInstance.extendedProperties.ownerId + ")(SourceMsn=" + ProcessInstance.extendedProperties.msn + ")))",
					checkProfileTab: false,
					isSystem: true
				});

				Controls.EventHistoryView__.Apply();
			}
		},
		settings: {
			panes: { GeneralPane: true, SecurityPane: true },
			init: function ()
			{
				var settings = PackageHelper.getSettings();
				for (var i = 0; i < settings.length; i++)
				{
					var displayPane = true;
					var paneSettings = settings[i];
					for (var paneName in paneSettings)
					{
						var paneDisplayCondition = paneSettings[paneName];
						for (var globalSettingName in paneDisplayCondition)
						{
							var globalSettingCondition = paneDisplayCondition[globalSettingName];
							var globalSettingValue = Sys.AR.GetParameter(globalSettingName);
							displayPane = globalSettingValue === globalSettingCondition;
						}

						this.panes[paneName] = displayPane;
					}
				}

				Controls.Notifications__.OnChange = function ()
				{
					Controls.Send_confirmation_notification__.Hide(Controls.Notifications__.GetValue() !== "Order notifications");
				};
				Controls.Notifications__.OnChange();

				Controls.ResetPassword__.OnClick = ActionMenuHelper.ResetPassword;
				Controls.CreditScore_Enabled__.SetReadOnly(!CustomerDetailsHelper.isMainContact);
			},
			onStart: EDIHelper.OnDeliverySystemChange
		},
		relatedDoc: {
			_availableValues: [],
			panes: { RelatedDocumentsPane: true },
			init: function ()
			{
				var recipientId = ProcessInstance.extendedProperties.businessPartnerID || ProcessInstance.extendedProperties.login.split("$")[1];
				var filter = "(Recipient_ID__=" + recipientId + ")";
				Controls.RelatedDocumentsView__.SetView({
					tabName: "_RecipientDocumentsEmbeddedTab",
					filter: filter,
					viewName: "_RelatedDocumentsEmbedded",
					processOrTableName: "DD - SenderForm",
					checkProfileTab: false
				});
				if (!Sys.AR.GetParameter("EnableCreditApplication"))
				{
					Controls.UploadDocument__.Hide(true);
				}
			},
			onStart: function ()
			{
				var that = this;
				Controls.RelatedDocumentsView__.Apply();
				Controls.UploadDocument__.OnClick = function ()
				{
					Popup.Dialog("_UploadDocumentTitle", null, that.FillCallback, that.CommitCallback, that.ValidateCallback, null, null);
				};
				var filterDD = "(&(Enable_Configuration__=1)(Family__='Customer documents'))";
				var attributesDDToQuery = ["Document_Type__"];
				if (!this._availableValues.length)
				{
					Sys.GenericAPI.Query("DD - Application Settings__", filterDD, attributesDDToQuery, this.HandleDocumentsType, null, "NO_LIMIT", { useConstantQueryCache: true });
				}
			},
			HandleDocumentsType: function (records)
			{
				for (var j = 0; j < records.length; j++)
				{
					if (records[j].Document_Type__)
					{
						NavMenuHelper.Menus.relatedDoc._availableValues.push(records[j].Document_Type__ + "=" + records[j].Document_Type__);
					}
				}
			},
			FillCallback: function (dialog)
			{
				var ctrlFileUploader = dialog.AddFileUploader("ctrlFiles", "_Document");
				ctrlFileUploader.SetAllowUploadingOnlyOneFile(true);
				var ctrlType = dialog.AddComboBox("ctrlType", "_DocumentType");
				ctrlType.SetAvailableValues(NavMenuHelper.Menus.relatedDoc._availableValues);
				var ctrlError = dialog.AddDescription("ctrlError", "");
				ctrlError.SetText("_DocumentMandatory");
				ctrlError.AddStyle("text-highlight-warning");
				dialog.MaskControl(ctrlError, true);
				dialog.RequireControl("ctrlType", true);
			},
			CommitCallback: function (dialog)
			{
				var ctrlFiles = dialog.GetControl("ctrlFiles");
				var files = ctrlFiles.GetUploadedFiles();
				var documentType = dialog.GetControl("ctrlType").GetValue();
				Process.CreateProcessInstance(
					"DD - SenderForm",
					{
						Recipient_ID__: ProcessInstance.extendedProperties.businessPartnerID
					},
					{
						ForceDelivery__: "NONE",
						ManuallySubmitted__: "1",
						Configuration: documentType
					},
					{
						files: files,
						callback: function ()
						{
							Popup.Snackbar({
								message: Language.Translate("_Document uploaded"),
								status: "success"
							});
						}
					}
				);

			},
			ValidateCallback: function (dialog, tabId, event, control)
			{
				var ctrlFiles = dialog.GetControl("ctrlFiles");
				var ctrlError = dialog.GetControl("ctrlError");
				var files = ctrlFiles.GetUploadedFiles();
				dialog.MaskControl(ctrlError, !(files.length !== 1));
				return files.length === 1;
			}
		},
		moreDetails: {
			panes: { CompanyPane: false, GeneralInformationPane: true, MailAddressPane: true, RegionalSettingsPane: true, CustomerAdvancedFieldsPane: true, WelcomeSettingsPane: true },
			init: function ()
			{
				for (var pane in NavMenuHelper.Menus.moreDetails.panes)
				{
					if (Controls[pane + "_Loader__"])
					{
						Controls[pane + "_Loader__"].SetHTML(Sys.AR.Client.GetFieldsLoaderHtmlContent());

						var controls = Controls[pane].GetControls();
						for (var i = 0; i < controls.length; i++)
						{
							controls[i].Hide(controls[i].GetName() !== pane + "_Loader__");
						}
					}
				}
			},
			_customerDetailsAlreadyFill: false,
			onStart: function ()
			{
				if (!NavMenuHelper.Menus.moreDetails._customerDetailsAlreadyFill)
				{
					CustomerDetailsHelper.FillUserInformation(function ()
					{
						for (var pane in NavMenuHelper.Menus.moreDetails.panes)
						{
							var controls = Controls[pane].GetControls();
							for (var i = 0; i < controls.length; i++)
							{
								controls[i].Hide(controls[i].GetName() === pane + "_Loader__");
							}
						}
					});

					if (CustomerDetailsHelper.isMainContact && Sys.AR.GetParameter("EnableCreditApplication"))
					{
						Sys.GenericAPI.Query("CreditScore - Identifier__", "(|(Country__=" + ProcessInstance.extendedProperties.country.toUpperCase() + ")(Country__=''))", ["IdentifierType__"], function (records)
						{
							ProcessInstance.SetSilentChange(true);

							var taxIdAdd = false;

							for (var j = 0; j < records.length; j++)
							{
								if (records[j].IdentifierType__ === "FederalTaxId" || records[j].IdentifierType__ === "TvaIntra")
								{
									taxIdAdd = true;
								}

								if (Controls[records[j].IdentifierType__ + "__"])
								{
									Controls[records[j].IdentifierType__ + "__"].Hide(false);
									Controls[records[j].IdentifierType__ + "__"].OnChange = CMHelper.onChange;
								}
							}

							if (!taxIdAdd)
							{
								Controls.TaxID__.Hide(false);
								Controls.TaxID__.OnChange = CMHelper.onChange;
							}

							ProcessInstance.SetSilentChange(false);
						}, null, "NO_LIMIT", { useConstantQueryCache: true });
					}

					NavMenuHelper.Menus.moreDetails._customerDetailsAlreadyFill = true;
				}
				Controls.CustomerIdentifiers.Hide(!(CustomerDetailsHelper.isMainContact && Sys.AR.GetParameter("EnableCreditApplication")));
			}
		},
	},
	Init: function ()
	{
		for (var m in this.Menus)
		{
			if (this.Menus[m].init)
			{
				this.Menus[m].init();
			}
		}
		if (!CustomerDetailsHelper.isMainContact)
			this._defaultRightPanes.CompanyPane = true;
		if (Data.GetValue("ShowAlerts__"))
			this._defaultRightPanes.AlertPane = true;

		this.OnClick({ args: PackageHelper.getStartMenu() });
		Controls.NavMenu__.BindEvent("onLoad", function ()
		{
			Controls.NavMenu__.FireEvent("onLoad", {
				IsDisplay: function (id)
				{
					if (id === "remittances")
					{
						return Sys.Helpers.String.ToBoolean(Sys.AR.GetParameter("EnableRemittanceAdvices"));
					}
					if (id === "payments")
					{
						var cashapp = Sys.AR.GetParameter("EnableCashApplication");
						var ar = ProcessInstance.extendedProperties.appInstances.AR;
						var paymentEnabled = Sys.AR.GetParameter("EnablePaymentFeature");
						return cashapp || (ar && paymentEnabled);
					}
					if (id === "creditScore")
					{
						var creditApp = Sys.AR.GetParameter("EnableCreditApplication");
						var ar = ProcessInstance.extendedProperties.appInstances.AR;
						return creditApp && ar;
					}
					if (id === "orders")
					{
						var cop = ProcessInstance.extendedProperties.appInstances.COP;
						var creditApp = Sys.AR.GetParameter("EnableCreditApplication");
						return cop || creditApp;
					}
					return true;
				},
				appInstances: ProcessInstance.extendedProperties.appInstances,
				tabs: {
					overview: Language.Translate("_NavOverview"),
					contacts: Language.Translate("_NavContacts"),
					activity: Language.Translate("_NavActivity"),
					settings: Language.Translate("_NavSettings"),
					invoices: Language.Translate("_NavInvoices"),
					payments: Language.Translate("_NavPayments"),
					remittances: Language.Translate("_NavRemittances"),
					deductions: Language.Translate("_NavDeductions"),
					creditScore: Language.Translate("_NavCreditScore"),
					orders: Language.Translate("_NavOrders"),
					relatedDoc: Language.Translate("_NavRelatedDoc"),
					moreDetails: Language.Translate("_NavMoreDetails")
				},
				startMenu: PackageHelper.getStartMenu()
			});
		});
		Controls.NavMenu__.BindEvent("onClick", this.OnClick);
		Process.SetHelpId(1851);
	},
	OnClick: function (evt)
	{
		var panes = Controls.form_content_right.GetPanes();
		var menu = NavMenuHelper.Menus[evt.args];
		var panesToShow = {};
		for (var pane in NavMenuHelper._defaultRightPanes)
		{
			panesToShow[pane] = NavMenuHelper._defaultRightPanes[pane];
		}
		for (var paneMenu in menu.panes)
		{
			panesToShow[paneMenu] = menu.panes[paneMenu];
		}
		for (var i = 0; i < panes.length; i++)
		{
			panes[i].Hide(!panesToShow[panes[i].GetName()]);
		}

		if (menu.onStart)
		{
			menu.onStart();
		}

		AlertsHelper.HandleDetailsBtnVisibility({ alertId: "CreditAgenciesUpdatesAlertTitle", hidden: evt.args === "creditScore" });
	}
};

var ActionMenuHelper = {
	Init: function ()
	{
		Controls.ActionMenu__.BindEvent("onActionLoad", function ()
		{
			Controls.ActionMenu__.FireEvent("onActionLoad", {
				appInstances: ProcessInstance.extendedProperties.appInstances,
				tabs: {
					MoreDetails: Language.Translate("_More_Details"),
					Edit: Language.Translate("_Edit"),
					ResendWelcomeEmail: Language.Translate("_ResendWelcomeEmail"),
					ResetPassword: Language.Translate("_ResetPassword")
				}
			});
		});
		Controls.ActionMenu__.BindEvent("MoreDetails", this.MoreDetailsAction);
		Controls.ActionMenu__.BindEvent("EditAction", this.Edit);
		Controls.ActionMenu__.BindEvent("WelcomeEmailAction", this.ResendWelcomeEmail);
		Controls.ActionMenu__.BindEvent("ResetPasswordAction", this.ResetPassword);
	},
	Edit: function ()
	{
		var editCustomerUrl = Sys.AR.Customer.GetModifyCustomerUrl(ProcessInstance.extendedProperties.msn) + "&extendedPropsId=" + encodeURIComponent(ProcessInstance.id);
		Process.OpenLink({ url: editCustomerUrl, inCurrentTab: true, onQuit: "back" });
	},
	ResendWelcomeEmail: function ()
	{
		Popup.Confirm("_Resend welcome email confirmation", false, ActionMenuHelper.SendWelcomeEmail, null, "_Resend welcome email title");
	},
	ResetPassword: function ()
	{
		Popup.Confirm("_Reset password confirmation", false, ActionMenuHelper.SendResetPasswordEmail, null, "_Reset password title");
	},
	SendWelcomeEmail: function ()
	{
		Sys.AR.Customer.ResendWelcomeEmail(ProcessInstance.extendedProperties.msn);
	},
	SendResetPasswordEmail: function ()
	{
		Sys.AR.Customer.ResetPassword(ProcessInstance.extendedProperties.msn);
	}
};

var AlertsHelper = {
	GetHandler: function ()
	{
		if (!AlertsHelper._handler)
		{
			AlertsHelper._handler = new Lib.CM.CustomerHelper.AlertHandler({
				pane: Controls.AlertPane,
				container: Controls.AlertsContainer__,
				onAlertsChange: function (nbAlerts)
				{
					NavMenuHelper._defaultRightPanes.AlertPane = nbAlerts;
				}
			});
		}

		return AlertsHelper._handler;
	},
	Init: function ()
	{
		AlertsHelper._enabled = Sys.AR.GetParameter("EnableCreditApplication");

		if (!AlertsHelper._enabled)
		{
			return;
		}

		var alerts = Data.GetValue("ShowAlerts__");
		if (alerts)
		{
			var handler = AlertsHelper.GetHandler();
			handler.AddAlert({
				id: "CreditAgenciesUpdatesAlertTitle",
				message: Language.Translate("_CreditAgenciesUpdatesAlertTitle"),
				dismissBtn: {
					title: Language.Translate("_Dismiss"),
					onClick: function ()
					{
						handler.RemoveAlert("CreditAgenciesUpdatesAlertTitle");
						ProcessInstance.UpdateRecord(function (lastErrorMessage)
						{
							if (!lastErrorMessage)
							{
								ProcessInstance.SetSilentChange(true);
								Data.SetValue("ShowAlerts__", 0);
								ProcessInstance.SetSilentChange(false);

								Lib.O2C.Audit.Add({
									eventId: Lib.O2C.Audit.Events.DismissCreditAgenciesAlerts,
									companyId: NavMenuHelper.Menus.invoices._customerId,
									oldValue: "1",
									newValue: "0",
									packageId: "CM"
								});
							}
						}, { ShowAlerts__: "0" });
					}
				},
				detailsBtn: {
					title: Language.Translate("_CheckUpdates"),
					onClick: function ()
					{
						Controls.NavMenu__.FireEvent("onChangeTab", { tabId: "creditScore" });
						handler.HideDetailsBtn("CreditAgenciesUpdatesAlertTitle");
					}
				}
			});
		}

		Lib.CM.CustomerHelper.CheckCreditLimitUtilization({
			customerId: NavMenuHelper.Menus.invoices._customerId,
			creditLimit: Data.GetValue("CreditLimit__"),
			callback: function (data)
			{
				if (data.error)
				{
					Controls.CreditLimitUtilization__.Hide(true);
					return;
				}
				var availableCredit = data.creditLimit - data.outstanding;
				Controls.CreditLimitUtilization__.FireEvent('onCreditLimitUtilizationInit', {
					progressValue: data.outstanding,
					totalValue: data.creditLimit,
					availableCredit: availableCredit >= 0 ? availableCredit : 0,
					culture: User.culture,
					currency: Data.GetValue("Currency__"),
					progressLegend: Language.Translate("_UsedCredit"),
					totalLegend: Language.Translate("_AvailableCredit")
				});

				if (data.outstanding > data.creditLimit && (Data.GetValue("OutstandingAlert__") || data.outstanding - data.creditLimit > Data.GetValue("OverCreditLimit__")))
				{
					ProcessInstance.UpdateRecord(function (lastErrorMessage)
					{
						if (!lastErrorMessage)
						{
							ProcessInstance.SetSilentChange(true);
							Data.SetValue("OutstandingAlert__", true);
							ProcessInstance.SetSilentChange(false);
						}
					},
						{
							OutstandingAlert__: true
						});

					var overcreditLimit = Lib.CM.CustomerHelper.CurrencyFormat(data.outstanding - data.creditLimit);
					var handler = AlertsHelper.GetHandler();
					handler.AddAlert({
						id: "CheckCreditLimitUtilization",
						message: Language.Translate("_CreditLimitAlertTitle", false, overcreditLimit),
						dismissBtn: {
							onClick: function ()
							{
								function onCommitDialog(dialog)
								{
									handler.RemoveAlert("CheckCreditLimitUtilization");
									var comments = dialog.GetControl("dissmissCreditLimitAlertComment").GetValue();
									ProcessInstance.UpdateRecord(function (lastErrorMessage)
									{
										if (!lastErrorMessage)
										{
											ProcessInstance.SetSilentChange(true);
											Data.SetValue("OutstandingAlert__", false);
											ProcessInstance.SetSilentChange(false);
											Lib.O2C.Audit.Add({
												eventId: Lib.O2C.Audit.Events.DismissOutstandingAlerts,
												companyId: NavMenuHelper.Menus.invoices._customerId,
												comments: comments,
												oldValue: "1",
												newValue: "0",
												packageId: "CM"
											});
										}
									}, { OutstandingAlert__: false });
								}

								function onHandleDialog(dialog)
								{
									dialog.RequireControl("dissmissCreditLimitAlertComment");
								}

								function onFillDialog(dialog)
								{
									dialog.AddMultilineText("dissmissCreditLimitAlertComment", Language.Translate("_Comment"));
								}
								Popup.Dialog(Language.Translate("_DismissCreditLimitAlertReason"), null, onFillDialog, onCommitDialog, null, onHandleDialog);
							},
							title: Language.Translate("_Dismiss")
						}
					});
				}
				if (data.outstanding != Data.GetValue("Outstanding__") || data.outstanding - data.creditLimit !== Data.GetValue("OverCreditLimit__"))
				{
					ProcessInstance.UpdateRecord(function (lastErrorMessage)
					{
						if (!lastErrorMessage)
						{
							ProcessInstance.SetSilentChange(true);
							Data.SetValue("OverCreditLimit__", data.outstanding - data.creditLimit);
							Data.SetValue("Outstanding__", data.outstanding);
							ProcessInstance.SetSilentChange(false);
						}
					},
						{
							OverCreditLimit__: data.outstanding - data.creditLimit,
							Outstanding__: data.outstanding
						});
				}
			}
		});
	},
	HandleDetailsBtnVisibility: function (args)
	{
		if (AlertsHelper._enabled)
		{
			AlertsHelper.GetHandler().HideDetailsBtn(args.alertId, args.hidden);
		}
	}
};

// Entry point
EDIHelper.Init();
CMHelper.Init();
ARHelper.Init();
TermSyncHelper.Init();
AlertsHelper.Init();
APPortalHelper.Init();
CustomerDetailsHelper.Init();
NavMenuHelper.Init();
ActionMenuHelper.Init();