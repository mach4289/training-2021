{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Company__": "LabelCompany__",
																	"LabelCompany__": "Company__",
																	"CompanyName__": "LabelCompanyName__",
																	"LabelCompanyName__": "CompanyName__",
																	"Country__": "LabelCountry__",
																	"LabelCountry__": "Country__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Company__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany__": {
																				"line": 1,
																				"column": 1
																			},
																			"CompanyName__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCompanyName__": {
																				"line": 2,
																				"column": 1
																			},
																			"Country__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCountry__": {
																				"line": 3,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany__": {
																			"type": "Label",
																			"data": [
																				"Company__"
																			],
																			"options": {
																				"label": "_Company"
																			},
																			"stamp": 14
																		},
																		"Company__": {
																			"type": "ShortText",
																			"data": [
																				"Company__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Company",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 15
																		},
																		"LabelCompanyName__": {
																			"type": "Label",
																			"data": [
																				"CompanyName__"
																			],
																			"options": {
																				"label": "_Company name"
																			},
																			"stamp": 16
																		},
																		"CompanyName__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Company name",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelCountry__": {
																			"type": "Label",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"label": "_Country"
																			},
																			"stamp": 18
																		},
																		"Country__": {
																			"type": "ShortText",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Country",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 19
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "_Currency"
																			},
																			"stamp": 20
																		},
																		"Currency__": {
																			"type": "ShortText",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Currency",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 21
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 21,
	"data": []
}