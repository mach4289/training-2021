///#GLOBALS Lib
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- Remittance Advice Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
function SendToApproval() {
    var submissionDate = Helpers.Date.DateToShortFormat(new Date());
    Data.SetValue("SubmissionDateTime__", submissionDate);
    Data.SetValue("Status__", "Approved");
    var ExtractedDataString = Variable.GetValueAsString("ExtractedData");
    var ExtractedData = Sys.Helpers.IsEmpty(ExtractedDataString) ? {} : JSON.parse(ExtractedDataString);
    for (var paymentId in ExtractedData) {
        if (Object.hasOwnProperty.call(ExtractedData, paymentId)) {
            var PaymentData = ExtractedData[paymentId].ExtractedData;
            var VendorInfo = GetVendorInfo(PaymentData.VendorId, PaymentData.VendorCompanyCode);
            var OwnerInformation = GetCompanyAddress(PaymentData.PayerCompanyCode);
            FormatOwnerAddress(OwnerInformation);
            var RemittanceAdvice = Process.CreateProcessInstanceAsProcessAdmin("Remittance Advice", true, false, false);
            if (VendorInfo.VendorLogin__) {
                var vendor = Lib.Purchasing.Vendor.QueryVendorContact(VendorInfo.VendorLogin__);
                if (vendor) {
                    var externalVars = RemittanceAdvice.GetExternalVars();
                    externalVars.AddValue_String("VendorLogin", vendor.GetVars().GetValue_String("login", 0), true);
                }
            }
            var extCMWVars = RemittanceAdvice.GetExternalVars();
            extCMWVars.AddValue_String("ExtractedData", JSON.stringify(ExtractedData[paymentId]), true);
            Log.Info(OwnerInformation.CompanyName__);
            var CMWVars = RemittanceAdvice.GetUninheritedVars();
            CMWVars.AddValue_String("TotalNetAmount__", PaymentData.TotalNetAmount, true);
            CMWVars.AddValue_String("Currency__", PaymentData.Currency, true);
            CMWVars.AddValue_String("PaymentMethodDescription__", PaymentData.PaymentMethodDescription, true);
            CMWVars.AddValue_String("PaymentMethodCode__", PaymentData.PaymentMethodCode, true);
            CMWVars.AddValue_String("PaymentReference__", PaymentData.PaymentId, true);
            CMWVars.AddValue_String("PayerAddress__", Data.GetValue("PayerAddress__"), true);
            CMWVars.AddValue_String("PayerCompany__", OwnerInformation.CompanyName__, true);
            CMWVars.AddValue_String("PayerCompanyCode__", PaymentData.PayerCompanyCode, true);
            CMWVars.AddValue_String("VendorAddress__", Data.GetValue("VendorAddress__"), true);
            CMWVars.AddValue_String("VendorName__", VendorInfo.VendorName__, true);
            CMWVars.AddValue_String("VendorNumber__", PaymentData.VendorId, true);
            CMWVars.AddValue_String("VendorCompanyCode__", PaymentData.VendorCompanyCode, true);
            RemittanceAdvice.Process();
            var ret = RemittanceAdvice.GetLastError();
            if (ret === 0) {
                Log.Info("RA process call OK");
            }
            else {
                Log.Error("RA process call returns with error message : " + RemittanceAdvice.GetLastErrorMessage());
            }
        }
    }
}
function GetVendorInfo(VendorNumber, CompanyCode) {
    var query = Process.CreateQueryAsProcessAdmin();
    query.Reset();
    query.SetSpecificTable("AP - Vendors__");
    query.SetAttributesList("CompanyCode__,Number__,Name__,Email__,Sub__,Street__,PostalCode__,Country__,Country__,Region__,City__,PostOfficeBox__");
    query.SetFilter("(&(Number__=" + VendorNumber + ")(CompanyCode__=" + CompanyCode + "))");
    query.MoveFirst();
    var record = query.MoveNextRecord();
    var vendor = {};
    if (record) {
        var companyCode = record.GetVars().GetValue_String("CompanyCode__", 0);
        var companyName = record.GetVars().GetValue_String("Name__", 0);
        var Sub__ = record.GetVars().GetValue_String("Sub__", 0);
        var Street__ = record.GetVars().GetValue_String("Street__", 0);
        var PostalCode__ = record.GetVars().GetValue_String("PostalCode__", 0);
        var Country__ = record.GetVars().GetValue_String("Country__", 0);
        var Region__ = record.GetVars().GetValue_String("Region__", 0);
        var City__ = record.GetVars().GetValue_String("City__", 0);
        var PostOfficeBox__ = record.GetVars().GetValue_String("PostOfficeBox__", 0);
        var address = {
            Sub__: Sub__, Street__: Street__, PostalCode__: PostalCode__, Country__: Country__, Region__: Region__, City__: City__, PostOfficeBox__: PostOfficeBox__
        };
        FormatVendorAddress(address);
        vendor = {
            "CompanyName__": companyName ? companyName : companyCode,
            "VendorNumber__": record.GetVars().GetValue_String("Number__", 0),
            "VendorName__": record.GetVars().GetValue_String("Name__", 0),
            "VendorLogin__": record.GetVars().GetValue_String("Email__", 0)
        };
        Log.Info("Vendor Info retrieved for company code = " + companyCode + " is : " + JSON.stringify(vendor));
    }
    return vendor;
}
function GetCompanyAddress(CompanyCode) {
    var attributes = {};
    Lib.P2P.CompanyCodesValue.QueryValues(CompanyCode).Then(function (CCValues) {
        if (Object.keys(CCValues).length > 0) {
            attributes = CCValues;
        }
    });
    return attributes;
}
function FormatVendorAddress(address) {
    var VendorAddress = {
        "ToName": "ToRemove",
        "ToSub": address.Sub__,
        "ToMail": address.Street__,
        "ToPostal": address.PostalCode__,
        "ToCountry": address.Country__,
        "ToCountryCode": address.Country__,
        "ToState": address.Region__,
        "ToCity": address.City__,
        "ToPOBox": address.PostOfficeBox__,
        "ForceCountry": "false"
    };
    Lib.Purchasing.Vendor.FillPostalAddress(VendorAddress);
}
function FormatOwnerAddress(options) {
    var OwnerAddress = {
        "ToName": options.CompanyName__,
        "ToSub": options.Sub__,
        "ToMail": options.Street__,
        "ToPostal": options.PostalCode__,
        "ToCountry": options.Country__,
        "ToCountryCode": options.Country__,
        "ToState": options.Region__,
        "ToCity": options.City__,
        "ToPOBox": options.PostOfficeBox__,
        "ForceCountry": "false"
    };
    var AddressOptions = {
        "isVariablesAddress": true,
        "address": OwnerAddress,
        "countryCode": options.Country__ // Get country code from contract ModProvider
    };
    Sys.GenericAPI.CheckPostalAddress(AddressOptions, function (address) {
        if (!Sys.Helpers.IsEmpty(address.LastErrorMessage)) {
            Data.SetWarning("OwnerAddress__", Language.Translate("_Owner address error:") + " " + address.LastErrorMessage);
        }
        else {
            Data.SetValue("PayerAddress__", address.FormattedBlockAddress.replace(/^[^\r\n]+(\r|\n)+/, ""), true);
        }
    });
}
if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
    switch (currentName) {
        case "Submit":
            SendToApproval();
            break;
        default:
    }
}
