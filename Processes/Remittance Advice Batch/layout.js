{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"backColor": "text-backgroundcolor-default",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 0,
													"width": 100,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 117,
													"*": {
														"Progress_Bar": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color1",
																"labelsAlignment": "center",
																"label": "Progress Bar",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "center",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 112,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ImportStepButton__": "LabelImportStepButton__",
																			"LabelImportStepButton__": "ImportStepButton__",
																			"HTML__": "Label_HTML",
																			"Label_HTML": "HTML__",
																			"ShortText__": "Label_ShortText",
																			"Label_ShortText": "ShortText__",
																			"Integer__": "Label_Integer",
																			"Label_Integer": "Integer__",
																			"SummaryStepButton__": "LabelSummaryStepButton__",
																			"LabelSummaryStepButton__": "SummaryStepButton__"
																		},
																		"version": 0
																	},
																	"stamp": 113,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ImportStepButton__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer4__": {
																						"line": 2,
																						"column": 1
																					},
																					"SummaryStepButton__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 115,
																			"*": {
																				"ImportStepButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ImportStepButton",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color8",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-cloud-upload fa-3x",
																						"style": 5,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 122
																				},
																				"Spacer4__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color6",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "___",
																						"version": 0
																					},
																					"stamp": 129
																				},
																				"SummaryStepButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SummaryStepButton",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color9",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-flag-checkered fa-3x",
																						"style": 5,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 131
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 84,
													"*": {
														"Information_panel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color8",
																"labelsAlignment": "right",
																"label": "Information panel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "300px"
															},
															"stamp": 65,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ShortText__": "Label_ShortText",
																			"Label_ShortText": "ShortText__",
																			"Sample_file__": "LabelSample_file__",
																			"LabelSample_file__": "Sample_file__",
																			"FileUploader__": "Label_FileUploader",
																			"Label_FileUploader": "FileUploader__",
																			"DownloadTemplate2__": "LabelDownloadTemplate2__",
																			"LabelDownloadTemplate2__": "DownloadTemplate2__",
																			"Status__": "LabelStatus__",
																			"LabelStatus__": "Status__",
																			"VendorAddress__": "LabelVendorAddress__",
																			"LabelVendorAddress__": "VendorAddress__",
																			"PayerAddress__": "LabelPayerAddress__",
																			"LabelPayerAddress__": "PayerAddress__"
																		},
																		"version": 0
																	},
																	"stamp": 66,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HelpDescription__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 3,
																						"column": 1
																					},
																					"Status__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 4,
																						"column": 1
																					},
																					"VendorAddress__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelVendorAddress__": {
																						"line": 5,
																						"column": 1
																					},
																					"PayerAddress__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelPayerAddress__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 67,
																			"*": {
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 98
																				},
																				"HelpDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "none",
																						"iconClass": "",
																						"label": "_HelpDescription",
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "40",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 106
																				},
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"label": "_Status",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 132
																				},
																				"Status__": {
																					"type": "ComboBox",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_Submitted"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "Submitted"
																						},
																						"label": "_Status",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 133
																				},
																				"LabelVendorAddress__": {
																					"type": "Label",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"label": "_VendorAddress",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 134
																				},
																				"VendorAddress__": {
																					"type": "LongText",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_VendorAddress",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 135
																				},
																				"LabelPayerAddress__": {
																					"type": "Label",
																					"data": [
																						"PayerAddress__"
																					],
																					"options": {
																						"label": "_PayerAddress",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 136
																				},
																				"PayerAddress__": {
																					"type": "LongText",
																					"data": [
																						"PayerAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_PayerAddress",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 137
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 57,
													"*": {
														"AttachmentsPane": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Documents",
																"hidden": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"culture": "A4",
																		"animate": true
																	},
																	"stamp": 10
																}
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 17,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": true
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 139,
							"data": []
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 26,
							"data": []
						},
						"Submit": {
							"type": "SubmitButton",
							"options": {
								"label": "Submit",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"style": 1
							},
							"stamp": 27,
							"data": []
						}
					},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 139,
	"data": []
}