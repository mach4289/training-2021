{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"PRNumber__": "LabelPRNumber__",
																	"LabelPRNumber__": "PRNumber__",
																	"CatalogReference__": "LabelCatalogReference__",
																	"LabelCatalogReference__": "CatalogReference__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"OrderedQuantity__": "LabelOrderedQuantity__",
																	"LabelOrderedQuantity__": "OrderedQuantity__",
																	"ReceivedQuantity__": "LabelReceivedQuantity__",
																	"LabelReceivedQuantity__": "ReceivedQuantity__",
																	"UnitPrice__": "LabelUnitPrice__",
																	"LabelUnitPrice__": "UnitPrice__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"NetAmount__": "LabelNetAmount__",
																	"LabelNetAmount__": "NetAmount__",
																	"VendorName__": "LabelVendorName__",
																	"LabelVendorName__": "VendorName__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"SupplyTypeName__": "LabelSupplyTypeName__",
																	"LabelSupplyTypeName__": "SupplyTypeName__",
																	"SupplyTypeId__": "LabelSupplyTypeId__",
																	"LabelSupplyTypeId__": "SupplyTypeId__",
																	"CompletelyDelivered__": "LabelCompletelyDelivered__",
																	"LabelCompletelyDelivered__": "CompletelyDelivered__",
																	"RequestedDeliveryDate__": "LabelRequestedDeliveryDate__",
																	"LabelRequestedDeliveryDate__": "RequestedDeliveryDate__",
																	"PRRUIDEX__": "LabelPRRUIDEX__",
																	"LabelPRRUIDEX__": "PRRUIDEX__",
																	"RecipientDN__": "LabelRecipientDN__",
																	"LabelRecipientDN__": "RecipientDN__",
																	"RequesterDN__": "LabelRequesterDN__",
																	"LabelRequesterDN__": "RequesterDN__",
																	"BuyerDN__": "LabelBuyerDN__",
																	"LabelBuyerDN__": "BuyerDN__",
																	"PONumber__": "LabelPONumber__",
																	"LabelPONumber__": "PONumber__",
																	"PRLineNumber__": "LabelPRLineNumber__",
																	"LabelPRLineNumber__": "PRLineNumber__",
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__",
																	"Status__": "LabelStatus__",
																	"LabelStatus__": "Status__",
																	"PORUIDEX__": "LabelPORUIDEX__",
																	"LabelPORUIDEX__": "PORUIDEX__",
																	"CostCenterName__": "LabelCostCenterName__",
																	"LabelCostCenterName__": "CostCenterName__",
																	"CostCenterId__": "LabelCostCenterId__",
																	"LabelCostCenterId__": "CostCenterId__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelGLAccount__": "GLAccount__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"RecipientLogin__": "LabelRecipientLogin__",
																	"LabelRecipientLogin__": "RecipientLogin__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"DeliveryDate__": "LabelDeliveryDate__",
																	"LabelDeliveryDate__": "DeliveryDate__",
																	"ExchangeRate__": "LabelExchangeRate__",
																	"LabelExchangeRate__": "ExchangeRate__",
																	"TaxRate__": "LabelTaxRate__",
																	"LabelTaxRate__": "TaxRate__",
																	"RequestedDeliveryDateInPast__": "LabelRequestedDeliveryDateInPast__",
																	"LabelRequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
																	"ItemUnit__": "LabelItemUnit__",
																	"LabelItemUnit__": "ItemUnit__",
																	"ItemUnitDescription__": "LabelItemUnitDescription__",
																	"LabelItemUnitDescription__": "ItemUnitDescription__",
																	"NoGoodsReceipt__": "LabelNoGoodsReceipt__",
																	"LabelNoGoodsReceipt__": "NoGoodsReceipt__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__",
																	"ItemType__": "LabelItemType__",
																	"LabelItemType__": "ItemType__",
																	"ItemRequestedAmount__": "LabelItemRequestedAmount__",
																	"LabelItemRequestedAmount__": "ItemRequestedAmount__",
																	"ProjectCode__": "LabelProjectCode__",
																	"LabelProjectCode__": "ProjectCode__",
																	"ProjectCodeDescription__": "LabelProjectCodeDescription__",
																	"LabelProjectCodeDescription__": "ProjectCodeDescription__",
																	"InternalOrder__": "LabelInternalOrder__",
																	"LabelInternalOrder__": "InternalOrder__",
																	"WBSElement__": "LabelWBSElement__",
																	"LabelWBSElement__": "WBSElement__",
																	"WBSElementID__": "LabelWBSElementID__",
																	"LabelWBSElementID__": "WBSElementID__",
																	"PRSubmissionDateTime__": "LabelPRSubmissionDateTime__",
																	"LabelPRSubmissionDateTime__": "PRSubmissionDateTime__",
																	"FreeDimension1__": "LabelFreeDimension1__",
																	"LabelFreeDimension1__": "FreeDimension1__",
																	"ShipToCompany__": "LabelShipToCompany__",
																	"LabelShipToCompany__": "ShipToCompany__",
																	"DeliveryAddressID__": "LabelDeliveryAddressID__",
																	"LabelDeliveryAddressID__": "DeliveryAddressID__",
																	"ShipToAddress__": "LabelShipToAddress__",
																	"LabelShipToAddress__": "ShipToAddress__",
																	"FreeDimension1ID__": "LabelFreeDimension1ID__",
																	"LabelFreeDimension1ID__": "FreeDimension1ID__",
																	"StartDate__": "LabelStartDate__",
																	"LabelStartDate__": "StartDate__",
																	"EndDate__": "LabelEndDate__",
																	"LabelEndDate__": "EndDate__",
																	"IsReplenishmentItem__": "LabelIsReplenishmentItem__",
																	"LabelIsReplenishmentItem__": "IsReplenishmentItem__",
																	"WarehouseID__": "LabelWarehouseID__",
																	"LabelWarehouseID__": "WarehouseID__",
																	"WarehouseName__": "LabelWarehouseName__",
																	"LabelWarehouseName__": "WarehouseName__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 58,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 7,
																				"column": 1
																			},
																			"PRNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelPRNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"CatalogReference__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCatalogReference__": {
																				"line": 8,
																				"column": 1
																			},
																			"Description__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 9,
																				"column": 1
																			},
																			"OrderedQuantity__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelOrderedQuantity__": {
																				"line": 19,
																				"column": 1
																			},
																			"ReceivedQuantity__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelReceivedQuantity__": {
																				"line": 20,
																				"column": 1
																			},
																			"UnitPrice__": {
																				"line": 26,
																				"column": 2
																			},
																			"LabelUnitPrice__": {
																				"line": 26,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 23,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 24,
																				"column": 1
																			},
																			"NetAmount__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelNetAmount__": {
																				"line": 27,
																				"column": 1
																			},
																			"VendorName__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelVendorName__": {
																				"line": 33,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 34,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 34,
																				"column": 1
																			},
																			"SupplyTypeName__": {
																				"line": 40,
																				"column": 2
																			},
																			"LabelSupplyTypeName__": {
																				"line": 40,
																				"column": 1
																			},
																			"SupplyTypeId__": {
																				"line": 44,
																				"column": 2
																			},
																			"LabelSupplyTypeId__": {
																				"line": 44,
																				"column": 1
																			},
																			"CompletelyDelivered__": {
																				"line": 45,
																				"column": 2
																			},
																			"LabelCompletelyDelivered__": {
																				"line": 45,
																				"column": 1
																			},
																			"RequestedDeliveryDate__": {
																				"line": 37,
																				"column": 2
																			},
																			"LabelRequestedDeliveryDate__": {
																				"line": 37,
																				"column": 1
																			},
																			"PRRUIDEX__": {
																				"line": 50,
																				"column": 2
																			},
																			"LabelPRRUIDEX__": {
																				"line": 50,
																				"column": 1
																			},
																			"RecipientDN__": {
																				"line": 48,
																				"column": 2
																			},
																			"LabelRecipientDN__": {
																				"line": 48,
																				"column": 1
																			},
																			"RequesterDN__": {
																				"line": 46,
																				"column": 2
																			},
																			"LabelRequesterDN__": {
																				"line": 46,
																				"column": 1
																			},
																			"BuyerDN__": {
																				"line": 47,
																				"column": 2
																			},
																			"LabelBuyerDN__": {
																				"line": 47,
																				"column": 1
																			},
																			"PONumber__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelPONumber__": {
																				"line": 1,
																				"column": 1
																			},
																			"PRLineNumber__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPRLineNumber__": {
																				"line": 4,
																				"column": 1
																			},
																			"LineNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"Status__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelStatus__": {
																				"line": 5,
																				"column": 1
																			},
																			"PORUIDEX__": {
																				"line": 52,
																				"column": 2
																			},
																			"LabelPORUIDEX__": {
																				"line": 52,
																				"column": 1
																			},
																			"CostCenterName__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelCostCenterName__": {
																				"line": 10,
																				"column": 1
																			},
																			"CostCenterId__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelCostCenterId__": {
																				"line": 11,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelGLAccount__": {
																				"line": 31,
																				"column": 1
																			},
																			"Group__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 32,
																				"column": 1
																			},
																			"RecipientLogin__": {
																				"line": 49,
																				"column": 2
																			},
																			"LabelRecipientLogin__": {
																				"line": 49,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 53,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 53,
																				"column": 1
																			},
																			"DeliveryDate__": {
																				"line": 39,
																				"column": 2
																			},
																			"LabelDeliveryDate__": {
																				"line": 39,
																				"column": 1
																			},
																			"ExchangeRate__": {
																				"line": 54,
																				"column": 2
																			},
																			"LabelExchangeRate__": {
																				"line": 54,
																				"column": 1
																			},
																			"TaxRate__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelTaxRate__": {
																				"line": 25,
																				"column": 1
																			},
																			"RequestedDeliveryDateInPast__": {
																				"line": 38,
																				"column": 2
																			},
																			"LabelRequestedDeliveryDateInPast__": {
																				"line": 38,
																				"column": 1
																			},
																			"ItemUnit__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelItemUnit__": {
																				"line": 21,
																				"column": 1
																			},
																			"ItemUnitDescription__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelItemUnitDescription__": {
																				"line": 22,
																				"column": 1
																			},
																			"NoGoodsReceipt__": {
																				"line": 55,
																				"column": 2
																			},
																			"LabelNoGoodsReceipt__": {
																				"line": 55,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 30,
																				"column": 1
																			},
																			"ItemType__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelItemType__": {
																				"line": 6,
																				"column": 1
																			},
																			"ItemReceivedAmount__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelItemReceivedAmount__": {
																				"line": 29,
																				"column": 1
																			},
																			"ItemRequestedAmount__": {
																				"line": 28,
																				"column": 2
																			},
																			"LabelItemRequestedAmount__": {
																				"line": 28,
																				"column": 1
																			},
																			"ProjectCode__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelProjectCode__": {
																				"line": 12,
																				"column": 1
																			},
																			"ProjectCodeDescription__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelProjectCodeDescription__": {
																				"line": 13,
																				"column": 1
																			},
																			"InternalOrder__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelInternalOrder__": {
																				"line": 14,
																				"column": 1
																			},
																			"WBSElement__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelWBSElement__": {
																				"line": 15,
																				"column": 1
																			},
																			"WBSElementID__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelWBSElementID__": {
																				"line": 16,
																				"column": 1
																			},
																			"FreeDimension1__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelFreeDimension1__": {
																				"line": 17,
																				"column": 1
																			},
																			"PRSubmissionDateTime__": {
																				"line": 51,
																				"column": 2
																			},
																			"LabelPRSubmissionDateTime__": {
																				"line": 51,
																				"column": 1
																			},
																			"ShipToCompany__": {
																				"line": 41,
																				"column": 2
																			},
																			"LabelShipToCompany__": {
																				"line": 41,
																				"column": 1
																			},
																			"DeliveryAddressID__": {
																				"line": 42,
																				"column": 2
																			},
																			"LabelDeliveryAddressID__": {
																				"line": 42,
																				"column": 1
																			},
																			"ShipToAddress__": {
																				"line": 43,
																				"column": 2
																			},
																			"LabelShipToAddress__": {
																				"line": 43,
																				"column": 1
																			},
																			"FreeDimension1ID__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelFreeDimension1ID__": {
																				"line": 18,
																				"column": 1
																			},
																			"StartDate__": {
																				"line": 35,
																				"column": 2
																			},
																			"LabelStartDate__": {
																				"line": 35,
																				"column": 1
																			},
																			"EndDate__": {
																				"line": 36,
																				"column": 2
																			},
																			"LabelEndDate__": {
																				"line": 36,
																				"column": 1
																			},
																			"IsReplenishmentItem__": {
																				"line": 56,
																				"column": 2
																			},
																			"LabelIsReplenishmentItem__": {
																				"line": 56,
																				"column": 1
																			},
																			"WarehouseID__": {
																				"line": 57,
																				"column": 2
																			},
																			"LabelWarehouseID__": {
																				"line": 57,
																				"column": 1
																			},
																			"WarehouseName__": {
																				"line": 58,
																				"column": 2
																			},
																			"LabelWarehouseName__": {
																				"line": 58,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelPONumber__": {
																			"type": "Label",
																			"data": [
																				"PONumber__"
																			],
																			"options": {
																				"label": "_PONumber",
																				"version": 0
																			},
																			"stamp": 96
																		},
																		"PONumber__": {
																			"type": "ShortText",
																			"data": [
																				"PONumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PONumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 97
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber",
																				"version": 0
																			},
																			"stamp": 98
																		},
																		"LineNumber__": {
																			"type": "ShortText",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_LineNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 99
																		},
																		"LabelPRNumber__": {
																			"type": "Label",
																			"data": [
																				"PRNumber__"
																			],
																			"options": {
																				"label": "_PRNumber",
																				"version": 0
																			},
																			"stamp": 82
																		},
																		"PRNumber__": {
																			"type": "ShortText",
																			"data": [
																				"PRNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PRNumber",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 83
																		},
																		"LabelPRLineNumber__": {
																			"type": "Label",
																			"data": [
																				"PRLineNumber__"
																			],
																			"options": {
																				"label": "_PRLineNumber",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"PRLineNumber__": {
																			"type": "Integer",
																			"data": [
																				"PRLineNumber__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_PRLineNumber",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": false,
																				"browsable": false
																			},
																			"stamp": 45
																		},
																		"LabelStatus__": {
																			"type": "Label",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"label": "_Status",
																				"version": 0
																			},
																			"stamp": 102
																		},
																		"Status__": {
																			"type": "ComboBox",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Order to order",
																					"1": "_Order to pay",
																					"2": "_Order waiting for deliver",
																					"3": "_Order auto deliver",
																					"4": "_Order delivered",
																					"5": "_Order canceled"
																				},
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "To order",
																					"1": "To pay",
																					"2": "To receive",
																					"3": "Auto receive",
																					"4": "Received",
																					"5": "Canceled"
																				},
																				"label": "_Status",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 1,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 103
																		},
																		"LabelItemType__": {
																			"type": "Label",
																			"data": [
																				"ComboBox__"
																			],
																			"options": {
																				"label": "_ItemType",
																				"version": 0
																			},
																			"stamp": 134
																		},
																		"ItemType__": {
																			"type": "ComboBox",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_QuantityBased",
																					"1": "_AmountBased"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "QuantityBased",
																					"1": "AmountBased"
																				},
																				"label": "_ItemType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 135
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 80
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 81
																		},
																		"LabelCatalogReference__": {
																			"type": "Label",
																			"data": [
																				"CatalogReference__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"CatalogReference__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CatalogReference__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 17
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_ItemDescription",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"Description__": {
																			"type": "LongText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ItemDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"minNbLines": 1,
																				"maxNbLines": 999
																			},
																			"stamp": 19
																		},
																		"LabelCostCenterName__": {
																			"type": "Label",
																			"data": [
																				"CostCenterName__"
																			],
																			"options": {
																				"label": "_CostCenterName",
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"CostCenterName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenterName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CostCenterName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 107
																		},
																		"LabelCostCenterId__": {
																			"type": "Label",
																			"data": [
																				"CostCenterId__"
																			],
																			"options": {
																				"label": "_CostCenterId",
																				"version": 0
																			},
																			"stamp": 108
																		},
																		"CostCenterId__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenterId__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CostCenterId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 109
																		},
																		"LabelProjectCode__": {
																			"type": "Label",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"label": "_ProjectCode",
																				"version": 0
																			},
																			"stamp": 140
																		},
																		"ProjectCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 141
																		},
																		"LabelProjectCodeDescription__": {
																			"type": "Label",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"label": "_ProjectCodeDescription",
																				"version": 0
																			},
																			"stamp": 142
																		},
																		"ProjectCodeDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCodeDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 143
																		},
																		"LabelInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"label": "_InternalOrder",
																				"version": 0
																			},
																			"stamp": 144
																		},
																		"InternalOrder__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_InternalOrder",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 145
																		},
																		"LabelWBSElement__": {
																			"type": "Label",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"label": "_WBSElement",
																				"version": 0
																			},
																			"stamp": 146
																		},
																		"WBSElement__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElement",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 147
																		},
																		"LabelWBSElementID__": {
																			"type": "Label",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"label": "_WBSElementID",
																				"version": 0
																			},
																			"stamp": 148
																		},
																		"WBSElementID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElementID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 149
																		},
																		"LabelFreeDimension1__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"label": "_FreeDimension1",
																				"version": 0
																			},
																			"stamp": 158
																		},
																		"FreeDimension1__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_FreeDimension1",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 500
																			},
																			"stamp": 159
																		},
																		"LabelFreeDimension1ID__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"label": "_FreeDimension1ID",
																				"version": 0
																			},
																			"stamp": 160
																		},
																		"FreeDimension1ID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_FreeDimension1ID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 500
																			},
																			"stamp": 161
																		},
																		"LabelOrderedQuantity__": {
																			"type": "Label",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"label": "_OrderedQuantity",
																				"version": 0
																			},
																			"stamp": 76
																		},
																		"OrderedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_OrderedQuantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 77
																		},
																		"LabelReceivedQuantity__": {
																			"type": "Label",
																			"data": [
																				"ReceivedQuantity__"
																			],
																			"options": {
																				"label": "_ReceivedQuantity",
																				"version": 0
																			},
																			"stamp": 78
																		},
																		"ReceivedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"ReceivedQuantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ReceivedQuantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 79
																		},
																		"LabelItemUnit__": {
																			"type": "Label",
																			"data": [
																				"S_lection_dans_une_table__"
																			],
																			"options": {
																				"label": "_ItemUnit",
																				"version": 0
																			},
																			"stamp": 126
																		},
																		"ItemUnit__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnit__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnit",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"length": 3,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 127
																		},
																		"LabelItemUnitDescription__": {
																			"type": "Label",
																			"data": [
																				"S_lection_dans_une_table__"
																			],
																			"options": {
																				"label": "_ItemUnitDescription",
																				"version": 0
																			},
																			"stamp": 128
																		},
																		"ItemUnitDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnitDescription__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnitDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 129
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "_Currency",
																				"version": 0
																			},
																			"stamp": 72
																		},
																		"Currency__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Currency",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 73
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"TaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"length": 600,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 29
																		},
																		"LabelTaxRate__": {
																			"type": "Label",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 122
																		},
																		"TaxRate__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 123
																		},
																		"LabelUnitPrice__": {
																			"type": "Label",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"label": "_UnitPrice",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"UnitPrice__": {
																			"type": "Decimal",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_UnitPrice",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 25
																		},
																		"LabelNetAmount__": {
																			"type": "Label",
																			"data": [
																				"NetAmount__"
																			],
																			"options": {
																				"label": "_NetAmount",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"NetAmount__": {
																			"type": "Decimal",
																			"data": [
																				"NetAmount__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_NetAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 27
																		},
																		"LabelItemRequestedAmount__": {
																			"type": "Label",
																			"data": [
																				"ItemRequestedAmount__"
																			],
																			"options": {
																				"label": "_ItemRequestedAmount",
																				"version": 0
																			},
																			"stamp": 138
																		},
																		"ItemRequestedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"ItemRequestedAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ItemRequestedAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false
																			},
																			"stamp": 139
																		},
																		"LabelItemReceivedAmount__": {
																			"type": "Label",
																			"data": [
																				"ItemReceivedAmount__"
																			],
																			"options": {
																				"label": "_ItemReceivedAmount",
																				"version": 0
																			},
																			"stamp": 136
																		},
																		"ItemReceivedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"ItemReceivedAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ItemReceivedAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"autocompletable": false
																			},
																			"stamp": 137
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 132
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 133
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_table__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"version": 0
																			},
																			"stamp": 110
																		},
																		"GLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_GLAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 111
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 112
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 113
																		},
																		"LabelVendorName__": {
																			"type": "Label",
																			"data": [
																				"VendorName__"
																			],
																			"options": {
																				"label": "_VendorName",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"VendorName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_VendorName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 57
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 86
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 87
																		},
																		"LabelStartDate__": {
																			"type": "Label",
																			"data": [
																				"StartDate__"
																			],
																			"options": {
																				"label": "_StartDate",
																				"version": 0
																			},
																			"stamp": 162
																		},
																		"StartDate__": {
																			"type": "DateTime",
																			"data": [
																				"StartDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_StartDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 163
																		},
																		"LabelEndDate__": {
																			"type": "Label",
																			"data": [
																				"EndDate__"
																			],
																			"options": {
																				"label": "_EndDate",
																				"version": 0
																			},
																			"stamp": 164
																		},
																		"EndDate__": {
																			"type": "DateTime",
																			"data": [
																				"EndDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_EndDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 165
																		},
																		"LabelRequestedDeliveryDate__": {
																			"type": "Label",
																			"data": [
																				"RequestedDeliveryDate__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDate",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"RequestedDeliveryDate__": {
																			"type": "DateTime",
																			"data": [
																				"RequestedDeliveryDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_RequestedDeliveryDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"LabelRequestedDeliveryDateInPast__": {
																			"type": "Label",
																			"data": [
																				"RequestedDeliveryDateInPast__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDateInPast",
																				"version": 0
																			},
																			"stamp": 124
																		},
																		"RequestedDeliveryDateInPast__": {
																			"type": "CheckBox",
																			"data": [
																				"RequestedDeliveryDateInPast__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDateInPast",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 125
																		},
																		"LabelDeliveryDate__": {
																			"type": "Label",
																			"data": [
																				"DeliveryDate__"
																			],
																			"options": {
																				"label": "_DeliveryDate",
																				"version": 0
																			},
																			"stamp": 118
																		},
																		"DeliveryDate__": {
																			"type": "DateTime",
																			"data": [
																				"DeliveryDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_DeliveryDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 119
																		},
																		"LabelSupplyTypeName__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeName__"
																			],
																			"options": {
																				"label": "_SupplyTypeName",
																				"version": 0
																			},
																			"stamp": 70
																		},
																		"SupplyTypeName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyTypeName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplyTypeName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 71
																		},
																		"LabelSupplyTypeId__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"label": "_SupplyTypeId",
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"SupplyTypeId__": {
																			"type": "ShortText",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SupplyTypeId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"readonly": true
																			},
																			"stamp": 55
																		},
																		"LabelCompletelyDelivered__": {
																			"type": "Label",
																			"data": [
																				"CompletelyDelivered__"
																			],
																			"options": {
																				"label": "_CompletelyDelivered",
																				"version": 0
																			},
																			"stamp": 74
																		},
																		"CompletelyDelivered__": {
																			"type": "CheckBox",
																			"data": [
																				"CompletelyDelivered__"
																			],
																			"options": {
																				"label": "_CompletelyDelivered",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 75
																		},
																		"LabelRequesterDN__": {
																			"type": "Label",
																			"data": [
																				"RequesterDN__"
																			],
																			"options": {
																				"label": "_RequesterDN",
																				"version": 0
																			},
																			"stamp": 90
																		},
																		"RequesterDN__": {
																			"type": "ShortText",
																			"data": [
																				"RequesterDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RequesterDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0,
																				"length": 500
																			},
																			"stamp": 91
																		},
																		"LabelBuyerDN__": {
																			"type": "Label",
																			"data": [
																				"BuyerDN__"
																			],
																			"options": {
																				"label": "_BuyerDN",
																				"version": 0
																			},
																			"stamp": 94
																		},
																		"BuyerDN__": {
																			"type": "ShortText",
																			"data": [
																				"BuyerDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BuyerDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 95
																		},
																		"LabelRecipientDN__": {
																			"type": "Label",
																			"data": [
																				"RecipientDN__"
																			],
																			"options": {
																				"label": "_RecipientDN",
																				"version": 0
																			},
																			"stamp": 88
																		},
																		"RecipientDN__": {
																			"type": "ShortText",
																			"data": [
																				"RecipientDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RecipientDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 89
																		},
																		"LabelRecipientLogin__": {
																			"type": "Label",
																			"data": [
																				"RecipientLogin__"
																			],
																			"options": {
																				"label": "_RecipientLogin",
																				"version": 0
																			},
																			"stamp": 114
																		},
																		"RecipientLogin__": {
																			"type": "ShortText",
																			"data": [
																				"RecipientLogin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RecipientLogin",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"length": 50,
																				"version": 0
																			},
																			"stamp": 115
																		},
																		"LabelPRRUIDEX__": {
																			"type": "Label",
																			"data": [
																				"PRRUIDEX__"
																			],
																			"options": {
																				"label": "_PRRUIDEX",
																				"version": 0
																			},
																			"stamp": 92
																		},
																		"PRRUIDEX__": {
																			"type": "ShortText",
																			"data": [
																				"PRRUIDEX__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PRRUIDEX",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 93
																		},
																		"LabelPRSubmissionDateTime__": {
																			"type": "Label",
																			"data": [
																				"PRSubmissionDateTime__"
																			],
																			"options": {
																				"label": "_PRSubmissionDateTime",
																				"version": 0
																			},
																			"stamp": 150
																		},
																		"PRSubmissionDateTime__": {
																			"type": "DateTime",
																			"data": [
																				"PRSubmissionDateTime__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_PRSubmissionDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"readonly": true,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 151
																		},
																		"LabelPORUIDEX__": {
																			"type": "Label",
																			"data": [
																				"PORUIDEX__"
																			],
																			"options": {
																				"label": "_PORUIDEX",
																				"version": 0
																			},
																			"stamp": 104
																		},
																		"PORUIDEX__": {
																			"type": "ShortText",
																			"data": [
																				"PORUIDEX__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PORUIDEX",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 105
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "_BudgetID",
																				"version": 0
																			},
																			"stamp": 116
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BudgetID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 117
																		},
																		"LabelExchangeRate__": {
																			"type": "Label",
																			"data": [
																				"ExchangeRate__"
																			],
																			"options": {
																				"label": "_ExchangeRate",
																				"version": 0
																			},
																			"stamp": 120
																		},
																		"ExchangeRate__": {
																			"type": "Decimal",
																			"data": [
																				"ExchangeRate__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ExchangeRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 121
																		},
																		"LabelNoGoodsReceipt__": {
																			"type": "Label",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"version": 0
																			},
																			"stamp": 130
																		},
																		"NoGoodsReceipt__": {
																			"type": "CheckBox",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 131
																		},
																		"ShipToCompany__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ShipToCompany",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 153
																		},
																		"LabelShipToCompany__": {
																			"type": "Label",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"label": "_ShipToCompany",
																				"version": 0
																			},
																			"stamp": 152
																		},
																		"DeliveryAddressID__": {
																			"type": "ShortText",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DeliveryAddressID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 157
																		},
																		"LabelDeliveryAddressID__": {
																			"type": "Label",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"label": "_DeliveryAddressID",
																				"version": 0
																			},
																			"stamp": 156
																		},
																		"ShipToAddress__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToAddress__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ShipToAddress",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 155
																		},
																		"LabelShipToAddress__": {
																			"type": "Label",
																			"data": [
																				"ShipToAddress__"
																			],
																			"options": {
																				"label": "_ShipToAddress",
																				"version": 0
																			},
																			"stamp": 154
																		},
																		"LabelIsReplenishmentItem__": {
																			"type": "Label",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem",
																				"hidden": false
																			},
																			"stamp": 166
																		},
																		"IsReplenishmentItem__": {
																			"type": "CheckBox",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"possibleValues": {},
																				"possibleKeys": {},
																				"hidden": false,
																				"readonly": true
																			},
																			"stamp": 167
																		},
																		"LabelWarehouseID__": {
																			"type": "Label",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"label": "_WarehouseID"
																			},
																			"stamp": 168
																		},
																		"WarehouseID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 169
																		},
																		"LabelWarehouseName__": {
																			"type": "Label",
																			"data": [
																				"WarehouseName__"
																			],
																			"options": {
																				"label": "_WarehouseName"
																			},
																			"stamp": 170
																		},
																		"WarehouseName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseName__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 171
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 171,
	"data": []
}