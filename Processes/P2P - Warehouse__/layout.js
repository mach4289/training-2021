{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 2,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 79,
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": "180px",
																"label": "_WarehouseInformation",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "left",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "FlexibleFormPanelLight",
																"sameHeightAsSiblings": false,
																"labelsPosition": "left"
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"WarehouseDescription__": "LabelWarehouseDescription__",
																			"LabelWarehouseDescription__": "WarehouseDescription__",
																			"LabelWarehouseName__": "WarehouseName__",
																			"WarehouseName__": "LabelWarehouseName__",
																			"WarehouseID__": "LabelWarehouseID__",
																			"LabelWarehouseID__": "WarehouseID__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 4,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"WarehouseDescription__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelWarehouseDescription__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelWarehouseName__": {
																						"line": 3,
																						"column": 1
																					},
																					"WarehouseName__": {
																						"line": 3,
																						"column": 2
																					},
																					"WarehouseID__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelWarehouseID__": {
																						"line": 2,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0
																					},
																					"stamp": 4
																				},
																				"LabelWarehouseName__": {
																					"type": "Label",
																					"data": [
																						"WarehouseName__"
																					],
																					"options": {
																						"label": "_WarehouseName",
																						"version": 0
																					},
																					"stamp": 5
																				},
																				"LabelWarehouseID__": {
																					"type": "Label",
																					"data": [
																						"WarehouseID__"
																					],
																					"options": {
																						"label": "_WarehouseID",
																						"version": 0
																					},
																					"stamp": 6
																				},
																				"WarehouseID__": {
																					"type": "ShortText",
																					"data": [
																						"WarehouseID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_WarehouseID",
																						"activable": true,
																						"width": "300",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 255,
																						"browsable": false,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 7
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"browsable": true,
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": "270",
																						"fTSmaxRecords": "20",
																						"version": 0,
																						"PreFillFTS": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 8
																				},
																				"WarehouseName__": {
																					"type": "ShortText",
																					"data": [
																						"WarehouseName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_WarehouseName",
																						"activable": true,
																						"width": "300",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 255,
																						"browsable": false,
																						"autocompletable": false,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 9
																				},
																				"LabelWarehouseDescription__": {
																					"type": "Label",
																					"data": [
																						"WarehouseDescription__"
																					],
																					"options": {
																						"label": "_WarehouseDescription",
																						"version": 0
																					},
																					"stamp": 10
																				},
																				"WarehouseDescription__": {
																					"type": "LongText",
																					"data": [
																						"WarehouseDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_WarehouseDescription",
																						"activable": true,
																						"width": "300",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 11
																				}
																			},
																			"stamp": 14
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 16,
															"data": []
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 21,
													"*": {
														"Action": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Action",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0,
																"labelsPosition": "left"
															},
															"stamp": 22,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 23,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"displayAsFlex": true,
																				"ctrlsPos": {
																					"Quit__": {
																						"line": 1,
																						"column": 1
																					},
																					"ReplenishmentOrderButton__": {
																						"line": 3,
																						"column": 1
																					},
																					"Save__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 94,
																			"*": {
																				"Save__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"label": "_Save",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"processName": {
																							"processName": "",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"buttonLabel": "_Save",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-save-circle fa-4x ",
																						"style": 5,
																						"width": "120px",
																						"action": "save",
																						"url": "",
																						"version": 0,
																						"shortCut": "S",
																						"nextprocess": {
																							"processName": "AP - Application Settings__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						}
																					},
																					"stamp": 73
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"label": "_Quit",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"buttonLabel": "_Quit",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x",
																						"style": 5,
																						"width": "120px",
																						"action": "cancel",
																						"url": "",
																						"version": 0,
																						"shortCut": "Q"
																					},
																					"stamp": 108
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "100%",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 77
																				},
																				"ReplenishmentOrderButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"label": "_ReplenishmentOrderButton",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"buttonLabel": "_ReplenishmentOrderButton",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-edit-document-circle fa-4x",
																						"style": 5,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 110
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 27,
													"*": {
														"StockInformationPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "180px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_StockDetails",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"panelStyle": "FlexibleFormPanelLight",
																"sameHeightAsSiblings": false,
																"labelsPosition": "left"
															},
															"stamp": 28,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 29,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line12__": {
																						"line": 1,
																						"column": 1
																					},
																					"StockTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 30,
																			"*": {
																				"Spacer_line12__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line12",
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"StockTable__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_StockTable",
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 32,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 33,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 34
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 35
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 36,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "80",
																										"version": 0
																									},
																									"stamp": 37,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemNumber",
																												"minwidth": "80",
																												"version": 0
																											},
																											"stamp": 38,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemDescription",
																												"type": "LongText",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"_LongText",
																												false,
																												false,
																												"LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "125",
																										"version": 0
																									},
																									"stamp": 41,
																									"data": [],
																									"*": {
																										"Stock__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Stock",
																												"minwidth": "125",
																												"type": "Decimal",
																												"version": 0
																											},
																											"stamp": 42,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "125",
																										"version": 0
																									},
																									"stamp": 43,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemUnitPrice",
																												"minwidth": "125",
																												"type": "Decimal",
																												"version": 0
																											},
																											"stamp": 44,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "125",
																										"version": 0
																									},
																									"stamp": 45,
																									"data": [],
																									"*": {
																										"MinimumThreshold__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_MinimumThreshold",
																												"minwidth": "125",
																												"type": "Decimal",
																												"version": 0
																											},
																											"stamp": 46,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "125",
																										"version": 0
																									},
																									"stamp": 47,
																									"data": [],
																									"*": {
																										"ExpectedStockLevel__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ExpectedStockLevel",
																												"minwidth": "125",
																												"type": "Decimal",
																												"version": 0
																											},
																											"stamp": 48,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 49,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 50,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 51,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 87,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"numberOfLines": 999
																											},
																											"stamp": 90,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54,
																									"data": [],
																									"*": {
																										"Stock__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Stock",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "125",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number"
																											},
																											"stamp": 55,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemUnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "125",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number"
																											},
																											"stamp": 57,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"MinimumThreshold__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_MinimumThreshold",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "125",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 59,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 60,
																									"data": [],
																									"*": {
																										"ExpectedStockLevel__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ExpectedStockLevel",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "125",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 61,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 62,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {},
											"stamp": 63,
											"data": []
										}
									},
									"stamp": 64,
									"data": []
								}
							},
							"stamp": 65,
							"data": []
						}
					},
					"stamp": 66,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {},
					"stamp": 70,
					"data": []
				}
			},
			"stamp": 71,
			"data": []
		}
	},
	"stamps": 90,
	"data": []
}