/* eslint-disable no-extra-boolean-cast */
/* eslint-disable dot-notation */
///#GLOBALS Lib Sys
function LoadStockTable() {
    var warehouseID = Controls.WarehouseID__.GetValue();
    var companyCode = Controls.CompanyCode__.GetValue();
    return Lib.P2P.Inventory.GetInventories([{ companyCode: companyCode, warehouseID: warehouseID }])
        .Then(function (inventoryStock) {
        var i = 0;
        Controls.StockTable__.SetItemCount(inventoryStock.length);
        inventoryStock.forEach(function (item) {
            var tableItem = Controls.StockTable__.GetItem(i);
            tableItem.SetValue("ItemNumber__", item.itemNumber);
            tableItem.SetValue("Stock__", parseFloat(item.stock));
            tableItem.SetValue("ItemDescription__", item.itemDescription);
            tableItem.SetValue("ItemUnitPrice__", item.itemUnitPrice);
            tableItem.SetValue("MinimumThreshold__", item.minimumThreshold);
            tableItem.SetValue("ExpectedStockLevel__", item.expectedStockLevel);
            i++;
        });
        return Sys.Helpers.Promise.Resolve();
    });
}
ProcessInstance.SetSilentChange(true);
ProcessInstance.SetFormWidth(1024);
function Init() {
    Controls.Save__.Hide(ProcessInstance.isReadOnly);
    Controls.Save__.SetReadOnly(ProcessInstance.isReadOnly);
    Controls.StockTable__.SetWidth("100%");
    Controls.StockTable__.SetExtendableColumn("ItemDescription__");
    Controls.ReplenishmentOrderButton__.OnClick = function () {
        var replenishmentData = {
            warehouseID: Data.GetValue("WarehouseID__"),
            warehouseName: Data.GetValue("WarehouseName__"),
            requestedItems: GetReplenishmentItems()
        };
        var storageKey = "ESKReplenishment" + Math.floor(Math.random() * 1000000000);
        Data.StorageSetValue(storageKey, JSON.stringify(replenishmentData));
        var url = "FlexibleForm.aspx?action=run&layout=_flexibleform&pName=Purchase requisition V2&replenishmentorder=true&replenishementkey=" + encodeURIComponent(storageKey);
        Process.OpenLink({ url: url, inCurrentTab: false });
    };
    Controls.StockTable__.OnRefreshRow = function (index) {
        var row = Controls.StockTable__.GetRow(index);
        if (row.GetItem().GetValue("Stock__") <= row.GetItem().GetValue("MinimumThreshold__")) {
            row.Stock__.AddStyle("text-highlight-warning");
        }
    };
    LoadStockTable().Then(function () {
        Controls.ReplenishmentOrderButton__.Hide(GetReplenishmentItems().length === 0);
        ProcessInstance.SetSilentChange(false);
    });
    GlobalLayout.Hide(false);
}
function GetReplenishmentItems() {
    var table = Controls.StockTable__;
    var requestedItems = [];
    for (var index = 0; index < table.GetItemCount(); index++) {
        var item = table.GetItem(index);
        var requestedQuantity = item.GetValue("ExpectedStockLevel__") - item.GetValue("Stock__");
        var itemNumber = item.GetValue("ItemNumber__");
        if (requestedQuantity > 0) {
            requestedItems.push({ itemNumber: itemNumber, requestedQuantity: requestedQuantity });
        }
    }
    return requestedItems;
}
/** ******************* **/
/** Form initialization **/
/** ******************* **/
var GlobalLayout = {
    panes: [
        "DataPanel",
        "Action",
        "StockInformationPanel"
    ].map(function (name) { return Controls[name]; }),
    HideWaitScreen: function (hide) {
        // async call just after boot
        setTimeout(function () {
            Controls.CompanyCode__.Wait(!hide);
        });
    },
    Hide: function (hide) {
        GlobalLayout.panes.forEach(function (pane) {
            pane.Hide(hide);
        });
        Log.TimeStamp("HideWaitScreen : " + !hide);
        GlobalLayout.HideWaitScreen(!hide);
    }
};
GlobalLayout.Hide(true);
function LoadLayout() {
    Lib.P2P.CompanyCodesValue.QueryValues(Data.GetValue("CompanyCode__")).Then(function (CCValues) {
        if (Object.keys(CCValues).length > 0) {
            Sys.Parameters.GetInstance("PAC").Reload(CCValues.DefaultConfiguration__);
        }
        else {
            Log.Error("The requested company code is not in the company code table.");
        }
        Sys.Parameters.GetInstance("PAC").IsReady(function () {
            GlobalLayout.Hide(false);
            Init();
        });
    });
}
LoadLayout();
