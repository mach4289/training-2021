{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"leftImageURL": ""
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"Template__": "LabelTemplate__",
																	"LabelTemplate__": "Template__",
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelGLAccount__": "GLAccount__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"AmountPercent__": "LabelAmountPercent__",
																	"LabelAmountPercent__": "AmountPercent__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 8,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Template__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelTemplate__": {
																				"line": 2,
																				"column": 1
																			},
																			"LineNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"Description__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 4,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelGLAccount__": {
																				"line": 5,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 7,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 6,
																				"column": 1
																			},
																			"AmountPercent__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelAmountPercent__": {
																				"line": 8,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"version": 0,
																				"length": 20
																			},
																			"stamp": 17
																		},
																		"LabelTemplate__": {
																			"type": "Label",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"label": "_Template",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"Template__": {
																			"type": "ShortText",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Template",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"LineNumber__": {
																			"type": "Integer",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_LineNumber",
																				"precision_internal": 0,
																				"activable": true,
																				"width": 500,
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_Description",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Description",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"GLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0,
																				"length": 30
																			},
																			"stamp": 25
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"CostCenter__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0,
																				"length": 20
																			},
																			"stamp": 29
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"TaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0,
																				"length": 10
																			},
																			"stamp": 27
																		},
																		"LabelAmountPercent__": {
																			"type": "Label",
																			"data": [
																				"AmountPercent__"
																			],
																			"options": {
																				"label": "_PercentageAmount",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"AmountPercent__": {
																			"type": "Decimal",
																			"data": [
																				"AmountPercent__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_PercentageAmount",
																				"precision_internal": 5,
																				"activable": true,
																				"width": 500
																			},
																			"stamp": 31
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 31,
	"data": []
}