{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1700,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 18
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning",
												"hideSeparator": true
											},
											"*": {
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"label": "_Banner",
																"hideTitleBar": true,
																"version": 0,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner",
																						"htmlContent": "Purchasing requisition",
																						"width": 1225,
																						"version": 0
																					},
																					"stamp": 14
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 15,
													"*": {
														"Requisition_information": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false,
																	"maximized": false
																},
																"version": 0,
																"labelLength": "230",
																"label": "_Requisition information",
																"iconURL": "PR_information.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"RequisitionNumber__": "LabelRequisition_number__",
																			"LabelRequisition_number__": "RequisitionNumber__",
																			"RequisitionInitiator__": "LabelRequisition_initiator__",
																			"LabelRequisition_initiator__": "RequisitionInitiator__",
																			"RequisitionStatus__": "LabelRequisition_status__",
																			"LabelRequisition_status__": "RequisitionStatus__",
																			"LabelRequester_name__": "RequesterName__",
																			"RequesterName__": "LabelRequester_name__",
																			"CompanyCode__": "LabelCompany_code__",
																			"LabelCompany_code__": "CompanyCode__",
																			"Currency__": "LabelCurrency__",
																			"LabelCurrency__": "Currency__",
																			"ExchangeRate__": "LabelExchange_rate__",
																			"LabelExchange_rate__": "ExchangeRate__",
																			"TotalNetLocalCurrency__": "LabelTotalNetLocalCurrency__",
																			"LabelTotalNetLocalCurrency__": "TotalNetLocalCurrency__",
																			"PurchasingOrganization__": "LabelPurchasingOrganization__",
																			"LabelPurchasingOrganization__": "PurchasingOrganization__",
																			"PurchasingGroup__": "LabelPurchasingGroup__",
																			"LabelPurchasingGroup__": "PurchasingGroup__",
																			"LabelTotal_net_amount__": "TotalNetAmount__",
																			"TotalNetAmount__": "LabelTotal_net_amount__",
																			"AtLeastOneRequestedDeliveryDateInPast__": "LabelAtLeastOneRequestedDeliveryDateInPast__",
																			"LabelAtLeastOneRequestedDeliveryDateInPast__": "AtLeastOneRequestedDeliveryDateInPast__",
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__",
																			"Reason__": "LabelReason__",
																			"LabelReason__": "Reason__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__",
																			"HasCapex__": "LabelHasCapex__",
																			"LabelHasCapex__": "HasCapex__",
																			"PRSubmissionDateTime__": "PRLabelSubmissionDateTime__",
																			"LabelPRSubmissionDateTime__": "PRSubmissionDateTime__",
																			"LocalCurrency__": "LabelLocalCurrency__",
																			"LabelLocalCurrency__": "LocalCurrency__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 20,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Spacer_line2__": {
																						"line": 12,
																						"column": 1
																					},
																					"RequisitionNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRequisition_number__": {
																						"line": 2,
																						"column": 1
																					},
																					"RequisitionInitiator__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRequisition_initiator__": {
																						"line": 4,
																						"column": 1
																					},
																					"RequisitionStatus__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelRequisition_status__": {
																						"line": 13,
																						"column": 1
																					},
																					"LabelRequester_name__": {
																						"line": 3,
																						"column": 1
																					},
																					"RequesterName__": {
																						"line": 3,
																						"column": 2
																					},
																					"Ligne_d_espacement2__": {
																						"line": 1,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCompany_code__": {
																						"line": 6,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 8,
																						"column": 1
																					},
																					"ExchangeRate__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelExchange_rate__": {
																						"line": 14,
																						"column": 1
																					},
																					"TotalNetLocalCurrency__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelTotalNetLocalCurrency__": {
																						"line": 10,
																						"column": 1
																					},
																					"PurchasingOrganization__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelPurchasingOrganization__": {
																						"line": 15,
																						"column": 1
																					},
																					"PurchasingGroup__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelPurchasingGroup__": {
																						"line": 16,
																						"column": 1
																					},
																					"LabelTotal_net_amount__": {
																						"line": 11,
																						"column": 1
																					},
																					"TotalNetAmount__": {
																						"line": 11,
																						"column": 2
																					},
																					"AtLeastOneRequestedDeliveryDateInPast__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelAtLeastOneRequestedDeliveryDateInPast__": {
																						"line": 17,
																						"column": 1
																					},
																					"ERP__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 7,
																						"column": 1
																					},
																					"Reason__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelReason__": {
																						"line": 5,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 18,
																						"column": 1
																					},
																					"HasCapex__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelHasCapex__": {
																						"line": 19,
																						"column": 1
																					},
																					"PRSubmissionDateTime__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelPRSubmissionDateTime__": {
																						"line": 20,
																						"column": 1
																					},
																					"LocalCurrency__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelLocalCurrency__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"Ligne_d_espacement2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement2",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 16
																				},
																				"LabelRequisition_number__": {
																					"type": "Label",
																					"data": [
																						"RequisitionNumber__"
																					],
																					"options": {
																						"label": "_Requisition number",
																						"version": 0
																					},
																					"stamp": 17
																				},
																				"LabelRequester_name__": {
																					"type": "Label",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"label": "_Requester name",
																						"version": 0
																					},
																					"stamp": 18
																				},
																				"LabelRequisition_initiator__": {
																					"type": "Label",
																					"data": [
																						"RequisitionInitiator__"
																					],
																					"options": {
																						"label": "_Requisition initiator",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 19
																				},
																				"RequesterName__": {
																					"type": "ShortText",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"label": "_Requester name",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"version": 0,
																						"browsable": false,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 20
																				},
																				"RequisitionNumber__": {
																					"type": "ShortText",
																					"data": [
																						"RequisitionNumber__"
																					],
																					"options": {
																						"label": "_Requisition number",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"browsable": false,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 21
																				},
																				"RequisitionInitiator__": {
																					"type": "ShortText",
																					"data": [
																						"RequisitionInitiator__"
																					],
																					"options": {
																						"label": "_Requisition initiator",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"browsable": false,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 22
																				},
																				"LabelReason__": {
																					"type": "Label",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"label": "_Reason",
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"Reason__": {
																					"type": "LongText",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Reason",
																						"activable": true,
																						"width": 275,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 3
																					},
																					"stamp": 24
																				},
																				"LabelCompany_code__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 25
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 1,
																						"activable": true,
																						"width": 275,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"hidden": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 26
																				},
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 27
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SAP",
																							"1": "_Generic",
																							"2": "_EBS",
																							"3": "_NAV",
																							"4": "_JDE"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SAP",
																							"1": "generic",
																							"2": "EBS",
																							"3": "NAV",
																							"4": "JDE"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 28
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"label": "_Currency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 29
																				},
																				"Currency__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"browsable": true,
																						"label": "_Currency",
																						"activable": true,
																						"width": "275",
																						"fTSmaxRecords": "20",
																						"PreFillFTS": true,
																						"version": 1,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"hidden": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 30
																				},
																				"LabelLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"label": "_LocalCurrency",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"LocalCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_LocalCurrency",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 32
																				},
																				"LabelTotalNetLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"label": "_TotalNetLocalCurrency",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"TotalNetLocalCurrency__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalNetLocalCurrency",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 34
																				},
																				"LabelTotal_net_amount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "_Total net amount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 35
																				},
																				"TotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Total net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2,
																						"dataSource": 1,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 36
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Spacer line2",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 37
																				},
																				"LabelRequisition_status__": {
																					"type": "Label",
																					"data": [
																						"RequisitionStatus__"
																					],
																					"options": {
																						"label": "_Requisition status",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 38
																				},
																				"RequisitionStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"RequisitionStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Requisition to complete",
																							"1": "_Requisition to review",
																							"2": "_Requisition to approve",
																							"3": "_Requisition to order",
																							"4": "_Requisition waiting for deliver",
																							"5": "_Requisition delivered",
																							"6": "_Requisition canceled",
																							"7": "_Requisition rejected"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "To complete",
																							"1": "To review",
																							"2": "To approve",
																							"3": "To order",
																							"4": "To receive",
																							"5": "Received",
																							"6": "Canceled",
																							"7": "Rejected"
																						},
																						"label": "_Requisition status",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"version": 1,
																						"readonlyIsText": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 39
																				},
																				"LabelExchange_rate__": {
																					"type": "Label",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"label": "_Exchange rate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 40
																				},
																				"ExchangeRate__": {
																					"type": "Decimal",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Exchange rate",
																						"precision_internal": 6,
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"precision_current": 6,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 41
																				},
																				"LabelPurchasingOrganization__": {
																					"type": "Label",
																					"data": [
																						"PurchasingOrganization__"
																					],
																					"options": {
																						"label": "_PurchasingOrganization",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 42
																				},
																				"PurchasingOrganization__": {
																					"type": "ShortText",
																					"data": [
																						"PurchasingOrganization__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_PurchasingOrganization",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 43
																				},
																				"LabelPurchasingGroup__": {
																					"type": "Label",
																					"data": [
																						"PurchasingGroup__"
																					],
																					"options": {
																						"label": "_PurchasingGroup",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 44
																				},
																				"PurchasingGroup__": {
																					"type": "ShortText",
																					"data": [
																						"PurchasingGroup__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_PurchasingGroup",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 45
																				},
																				"LabelAtLeastOneRequestedDeliveryDateInPast__": {
																					"type": "Label",
																					"data": [
																						"AtLeastOneRequestedDeliveryDateInPast__"
																					],
																					"options": {
																						"label": "_AtLeastOneRequestedDeliveryDateInPast",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 46
																				},
																				"AtLeastOneRequestedDeliveryDateInPast__": {
																					"type": "CheckBox",
																					"data": [
																						"AtLeastOneRequestedDeliveryDateInPast__"
																					],
																					"options": {
																						"label": "_AtLeastOneRequestedDeliveryDateInPast",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 47
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 5000,
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 49
																				},
																				"LabelHasCapex__": {
																					"type": "Label",
																					"data": [
																						"HasCapex__"
																					],
																					"options": {
																						"label": "_HasCapex",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"HasCapex__": {
																					"type": "CheckBox",
																					"data": [
																						"HasCapex__"
																					],
																					"options": {
																						"label": "_HasCapex",
																						"activable": true,
																						"width": "",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 51
																				},
																				"LabelPRSubmissionDateTime__": {
																					"type": "Label",
																					"data": [
																						"PRSubmissionDateTime__"
																					],
																					"options": {
																						"label": "_PRSubmissionDateTime",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"PRSubmissionDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"PRSubmissionDateTime__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_PRSubmissionDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 53
																				}
																			},
																			"stamp": 54
																		}
																	},
																	"stamp": 55,
																	"data": []
																}
															},
															"stamp": 56,
															"data": []
														},
														"Ship_to": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "170",
																"label": "_Ship to",
																"version": 0,
																"iconURL": "PR_reception_shipto.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 57,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ShipToContact__": "LabelShip_to_contact__",
																			"LabelShip_to_contact__": "ShipToContact__",
																			"ShipToCompany__": "LabelShip_to_company__",
																			"LabelShip_to_company__": "ShipToCompany__",
																			"ShipToAddress__": "LabelShip_to_address__",
																			"LabelShip_to_address__": "ShipToAddress__",
																			"DeliveryAddressID__": "LabelDeliveryAddressID__",
																			"LabelDeliveryAddressID__": "DeliveryAddressID__",
																			"ShipToPhone__": "LabelShipToPhone__",
																			"LabelShipToPhone__": "ShipToPhone__",
																			"ShipToEmail__": "LabelShipToEmail__",
																			"LabelShipToEmail__": "ShipToEmail__"
																		},
																		"version": 0
																	},
																	"stamp": 58,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ShipToContact__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelShip_to_contact__": {
																						"line": 4,
																						"column": 1
																					},
																					"ShipToCompany__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelShip_to_company__": {
																						"line": 3,
																						"column": 1
																					},
																					"ShipToAddress__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelShip_to_address__": {
																						"line": 7,
																						"column": 1
																					},
																					"Ligne_d_espacement6__": {
																						"line": 1,
																						"column": 1
																					},
																					"DeliveryAddressID__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDeliveryAddressID__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 8,
																						"column": 1
																					},
																					"ShipToPhone__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelShipToPhone__": {
																						"line": 5,
																						"column": 1
																					},
																					"ShipToEmail__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelShipToEmail__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 59,
																			"*": {
																				"Ligne_d_espacement6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement6",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 60
																				},
																				"LabelDeliveryAddressID__": {
																					"type": "Label",
																					"data": [
																						"DeliveryAddressID__"
																					],
																					"options": {
																						"label": "_DeliveryAddressID",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 61
																				},
																				"DeliveryAddressID__": {
																					"type": "ShortText",
																					"data": [
																						"DeliveryAddressID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_DeliveryAddressID",
																						"activable": true,
																						"width": "275",
																						"browsable": false,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 62
																				},
																				"LabelShip_to_company__": {
																					"type": "Label",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"label": "_Ship to company",
																						"version": 0
																					},
																					"stamp": 63
																				},
																				"ShipToCompany__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"label": "_Ship to company",
																						"activable": true,
																						"width": "275",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"version": 1,
																						"browsable": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 64
																				},
																				"LabelShip_to_contact__": {
																					"type": "Label",
																					"data": [
																						"ShipToContact__"
																					],
																					"options": {
																						"label": "_Ship to contact",
																						"version": 0
																					},
																					"stamp": 65
																				},
																				"ShipToContact__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToContact__"
																					],
																					"options": {
																						"label": "_Ship to contact",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"browsable": false,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 66
																				},
																				"LabelShipToPhone__": {
																					"type": "Label",
																					"data": [
																						"ShipToPhone__"
																					],
																					"options": {
																						"label": "_Ship to phone",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"ShipToPhone__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToPhone__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Ship to phone",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 68
																				},
																				"LabelShipToEmail__": {
																					"type": "Label",
																					"data": [
																						"ShipToEmail__"
																					],
																					"options": {
																						"label": "_Ship to email",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"ShipToEmail__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToEmail__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Ship to email",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 250,
																						"browsable": false
																					},
																					"stamp": 70
																				},
																				"LabelShip_to_address__": {
																					"type": "Label",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"label": "_Ship to address",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"ShipToAddress__": {
																					"type": "LongText",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"label": "_Ship to address",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"numberOfLines": 4,
																						"browsable": false,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"minNbLines": 4
																					},
																					"stamp": 72
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 73
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 74,
													"*": {
														"Line_items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "885",
																"label": "_Line items",
																"version": 0,
																"iconURL": "PR_items.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 75,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 76,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 77,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 78
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 67,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"width": "100%",
																						"subsection": null
																					},
																					"stamp": 79,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 80,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 81,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 82,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 83,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Label",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"label": "_Line item number",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 85
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"version": 0
																											},
																											"stamp": 87
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"version": 0
																											},
																											"stamp": 89
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 90,
																									"data": [],
																									"*": {
																										"ItemStatus__": {
																											"type": "Label",
																											"data": [
																												"ItemStatus__"
																											],
																											"options": {
																												"label": "_ItemStatus",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 91,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 92,
																									"data": [],
																									"*": {
																										"PONumber__": {
																											"type": "Label",
																											"data": [
																												"PONumber__"
																											],
																											"options": {
																												"label": "_PONumber",
																												"minwidth": "100",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 93,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "90",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 94,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": "90",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 95,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 96,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"label": "_Item quantity",
																												"version": 0
																											},
																											"stamp": 97
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 98,
																									"data": [],
																									"*": {
																										"QuantityTakenFromStock__": {
																											"type": "Label",
																											"data": [
																												"QuantityTakenFromStock__"
																											],
																											"options": {
																												"label": "_QuantityTakenFromStock",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 99,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 100,
																									"data": [],
																									"*": {
																										"CanceledQuantity__": {
																											"type": "Label",
																											"data": [
																												"CanceledQuantity__"
																											],
																											"options": {
																												"label": "_CanceledQuantity",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 101,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 102,
																									"data": [],
																									"*": {
																										"ItemOrderedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemOrderedQuantity__"
																											],
																											"options": {
																												"label": "_Ordered quantity",
																												"version": 0,
																												"hidden": true,
																												"minwidth": "60"
																											},
																											"stamp": 103,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 104,
																									"data": [],
																									"*": {
																										"ItemDeliveredQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveredQuantity__"
																											],
																											"options": {
																												"label": "_ItemTotalDeliveredQuantity",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 105,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 106,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "Label",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"label": "_ItemUnit",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 107,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 108,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"label": "_ItemUnitDescription",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 109,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 114,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemRequestedDeliveryDate",
																												"version": 0
																											},
																											"stamp": 115,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 110,
																									"data": [],
																									"*": {
																										"ItemStartDate__": {
																											"type": "Label",
																											"data": [
																												"ItemStartDate__"
																											],
																											"options": {
																												"label": "_ItemStartDate",
																												"minwidth": "80",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 111,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 112,
																									"data": [],
																									"*": {
																										"ItemEndDate__": {
																											"type": "Label",
																											"data": [
																												"ItemEndDate__"
																											],
																											"options": {
																												"label": "_ItemEndDate",
																												"minwidth": "80",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 113,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 116,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"label": "_Item unit price",
																												"version": 0
																											},
																											"stamp": 117
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 118,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"label": "_Item net amount",
																												"version": 0
																											},
																											"stamp": 119
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 120,
																									"data": [],
																									"*": {
																										"AmountTakenFromStock__": {
																											"type": "Label",
																											"data": [
																												"AmountTakenFromStock__"
																											],
																											"options": {
																												"label": "_AmountTakenFromStock",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 121,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 122,
																									"data": [],
																									"*": {
																										"CanceledAmount__": {
																											"type": "Label",
																											"data": [
																												"CanceledAmount__"
																											],
																											"options": {
																												"label": "_CanceledAmount",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 123,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 124,
																									"data": [],
																									"*": {
																										"ItemOrderedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemOrderedAmount__"
																											],
																											"options": {
																												"label": "_ItemOrderedAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 125,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 126,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"label": "_ItemReceivedAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 127,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 128,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "Label",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 129
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 130,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Label",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"label": "_ItemExchangeRate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 131,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 132,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"label": "_Supply type name",
																												"version": 0,
																												"minwidth": 140
																											},
																											"stamp": 133,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 134,
																									"data": [],
																									"*": {
																										"SupplyTypeID__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeID__"
																											],
																											"options": {
																												"label": "_Supply type ID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 135,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 136,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"label": "_SupplyTypeFullpath",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 137,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 144,
																									"data": [],
																									"*": {
																										"VendorName__": {
																											"type": "Label",
																											"data": [
																												"VendorName__"
																											],
																											"options": {
																												"label": "_VendorName",
																												"version": 0
																											},
																											"stamp": 145,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 146,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "Label",
																											"data": [
																												"VendorNumber__"
																											],
																											"options": {
																												"label": "_Vendor number",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 147,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 497,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "Label",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"label": "_WarehouseID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 498,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 501,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "Label",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"label": "_WarehouseName",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 502,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 138,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"label": "_ItemShipToCompany",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 139,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 140,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"label": "_ItemDeliveryAddressID",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 141,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 142,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"label": "_ItemShipToAddress",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 143,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 148,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"label": "_ItemCostCenterName",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 149,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 150,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"label": "_ItemCostCenterId",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 151,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 152,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 153
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 154,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "Label",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"label": "_ProjectCodeDescription",
																												"minwidth": 140,
																												"hidden": false,
																												"version": 0
																											},
																											"stamp": 155,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 156,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "Label",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"label": "_ProjectCode",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 157,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 158,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "Label",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"label": "_InternalOrder",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 159,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 160,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "Label",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"label": "_WBSElement",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 161,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 162,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "Label",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"label": "_WBSElementID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 163,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 164,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"label": "_FreeDimension1",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 165,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 166,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"label": "_FreeDimension1ID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 167,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 168,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 169,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 170,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "Label",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 171
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 172,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "Label",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"label": "_CostType",
																												"minwidth": 80,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 173,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 174,
																									"data": [],
																									"*": {
																										"ItemGroupDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemGroupDescription__"
																											],
																											"options": {
																												"label": "_ItemGroupDescription",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 175,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 176,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "Label",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"label": "_ItemGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 177
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 178,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "Label",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"label": "_BudgetID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 179,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 180,
																									"data": [],
																									"*": {
																										"ItemBudgetInitial__": {
																											"type": "Label",
																											"data": [
																												"ItemBudgetInitial__"
																											],
																											"options": {
																												"label": "_Item budget initial",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 181
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 182,
																									"data": [],
																									"*": {
																										"ItemBudgetRemaining__": {
																											"type": "Label",
																											"data": [
																												"ItemBudgetRemaining__"
																											],
																											"options": {
																												"label": "_Item budget remaining",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 183
																										}
																									}
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 184,
																									"data": [],
																									"*": {
																										"ItemBudgetCurrency__": {
																											"type": "Label",
																											"data": [
																												"ItemBudgetCurrency__"
																											],
																											"options": {
																												"label": "",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 185
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 186,
																									"data": [],
																									"*": {
																										"ItemPeriodCode__": {
																											"type": "Label",
																											"data": [
																												"ItemPeriodCode__"
																											],
																											"options": {
																												"label": "_Item period code",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 187
																										}
																									}
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 188,
																									"data": [],
																									"*": {
																										"ItemBudgetViewed__": {
																											"type": "Label",
																											"data": [
																												"ItemBudgetViewed__"
																											],
																											"options": {
																												"label": "_ItemBudgetViewed",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 189,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 190,
																									"data": [],
																									"*": {
																										"BuyerName__": {
																											"type": "Label",
																											"data": [
																												"BuyerName__"
																											],
																											"options": {
																												"label": "_Buyer name",
																												"version": 0
																											},
																											"stamp": 191,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 192,
																									"data": [],
																									"*": {
																										"BuyerLogin__": {
																											"type": "Label",
																											"data": [
																												"BuyerLogin__"
																											],
																											"options": {
																												"label": "_Buyer login",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 193,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 194,
																									"data": [],
																									"*": {
																										"RecipientName__": {
																											"type": "Label",
																											"data": [
																												"RecipientName__"
																											],
																											"options": {
																												"label": "_Recipient name",
																												"version": 0
																											},
																											"stamp": 195,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell59": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 196,
																									"data": [],
																									"*": {
																										"RecipientLogin__": {
																											"type": "Label",
																											"data": [
																												"RecipientLogin__"
																											],
																											"options": {
																												"label": "_Recipient login",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 197,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell60": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 198,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "Label",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 199,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell61": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 200,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "Label",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 201,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell62": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 202,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "Label",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"label": "ItemInCxml",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 203,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell63": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 204,
																									"data": [],
																									"*": {
																										"OutOfBudget__": {
																											"type": "Label",
																											"data": [
																												"OutOfBudget__"
																											],
																											"options": {
																												"label": "_OutOfBudget",
																												"version": 0,
																												"hidden": true,
																												"minwidth": 140
																											},
																											"stamp": 205,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell64": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 206,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "Label",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 207,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell65": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 208,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Label",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"label": "_LeadTime",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 209,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell66": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 210,
																									"data": [],
																									"*": {
																										"ItemOrigin__": {
																											"type": "Label",
																											"data": [
																												"ItemOrigin__"
																											],
																											"options": {
																												"label": "_ItemOrigin",
																												"minwidth": 140,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 211,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell67": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 505,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "Label",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 506,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 212,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 213,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Integer",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"integer": true,
																												"precision_internal": 0,
																												"label": "_Line item number",
																												"activable": true,
																												"width": "30",
																												"readonly": true,
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 214
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 215,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": "80",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 1,
																												"autocompletable": true,
																												"browsable": false,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 216
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 217,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"minNbLines": 1,
																												"maxNbLines": 999,
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": 250,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"version": 1,
																												"autocompletable": true,
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 218
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 219,
																									"data": [],
																									"*": {
																										"ItemStatus__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemStatus__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_Requisition to complete",
																													"1": "_Requisition to approve",
																													"2": "_Requisition to review",
																													"3": "_Requisition to order",
																													"4": "_Requisition waiting for deliver",
																													"5": "_Requisition delivered",
																													"6": "_Requisition canceled",
																													"7": "_Requisition rejected"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "To complete",
																													"1": "To review",
																													"2": "To approve",
																													"3": "To order",
																													"4": "To receive",
																													"5": "Received",
																													"6": "Canceled",
																													"7": "Rejected"
																												},
																												"label": "_ItemStatus",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 220,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 221,
																									"data": [],
																									"*": {
																										"PONumber__": {
																											"type": "LongText",
																											"data": [
																												"PONumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_PONumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 999,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 222,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 223,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 224,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 225,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item quantity",
																												"activable": true,
																												"width": "60",
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 226
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 227,
																									"data": [],
																									"*": {
																										"QuantityTakenFromStock__": {
																											"type": "Decimal",
																											"data": [
																												"QuantityTakenFromStock__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_QuantityTakenFromStock",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 228,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 229,
																									"data": [],
																									"*": {
																										"CanceledQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"CanceledQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CanceledQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 230,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 231,
																									"data": [],
																									"*": {
																										"ItemOrderedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemOrderedQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Ordered quantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 232,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 233,
																									"data": [],
																									"*": {
																										"ItemDeliveredQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemDeliveredQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTotalDeliveredQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 234,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnit",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"length": 3,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 236,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 237,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnitDescription",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 238,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 243,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemRequestedDeliveryDate",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 244,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 239,
																									"data": [],
																									"*": {
																										"ItemStartDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemStartDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemStartDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0,
																												"hidden": true,
																												"readonly": false
																											},
																											"stamp": 240,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 241,
																									"data": [],
																									"*": {
																										"ItemEndDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemEndDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemEndDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0,
																												"hidden": true,
																												"readonly": false
																											},
																											"stamp": 242,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 245,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item unit price",
																												"activable": true,
																												"width": "70",
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 246
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 247,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item net amount",
																												"activable": true,
																												"width": "90",
																												"version": 1,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2
																											},
																											"stamp": 248
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 249,
																									"data": [],
																									"*": {
																										"AmountTakenFromStock__": {
																											"type": "Decimal",
																											"data": [
																												"AmountTakenFromStock__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_AmountTakenFromStock",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 250,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 251,
																									"data": [],
																									"*": {
																										"CanceledAmount__": {
																											"type": "Decimal",
																											"data": [
																												"CanceledAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CanceledAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 252,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 253,
																									"data": [],
																									"*": {
																										"ItemOrderedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemOrderedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemOrderedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 254,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 255,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemReceivedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 256,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 257,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"browsable": true,
																												"label": "",
																												"activable": true,
																												"width": "40",
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"readonly": true,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 258
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 259,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemExchangeRate",
																												"precision_internal": 6,
																												"precision_current": 6,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"defaultValue": "1",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 260,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 261,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_Supply type name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 262,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 263,
																									"data": [],
																									"*": {
																										"SupplyTypeID__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeID__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Supply type ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"hidden": true
																											},
																											"stamp": 264,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 265,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_SupplyTypeFullpath",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 266,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 273,
																									"data": [],
																									"*": {
																										"VendorName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"VendorName__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_VendorName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"minNbLines": 1,
																												"maxNbLines": 1
																											},
																											"stamp": 274,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 275,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"VendorNumber__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_Vendor number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"browsable": true,
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 276,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 496,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"readonly": true
																											},
																											"stamp": 499,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 500,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"readonly": true
																											},
																											"stamp": 503,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 267,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ItemShipToCompany",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"hidden": true
																											},
																											"stamp": 268,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 269,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "ShortText",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemDeliveryAddressID",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 270,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 271,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "LongText",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemShipToAddress",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 4,
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 272,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 277,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"label": "_ItemCostCenterName",
																												"activable": true,
																												"width": 140,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"version": 1,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 278,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 279,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"label": "_ItemCostCenterId",
																												"activable": true,
																												"width": 140,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"version": 1,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 280,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 281,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"length": 600,
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"version": 1,
																												"browsable": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 282
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": false,
																										"version": 0
																									},
																									"stamp": 283,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCodeDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": false,
																												"browsable": true
																											},
																											"stamp": 284,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 285,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"RestrictSearch": true,
																												"hidden": true
																											},
																											"stamp": 286,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 287,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_InternalOrder",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 288,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 289,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElement",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 290,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 291,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElementID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true
																											},
																											"stamp": 292,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 293,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_FreeDimension1",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"length": 500
																											},
																											"stamp": 294,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 295,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_FreeDimension1ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"RestrictSearch": true
																											},
																											"stamp": 296,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 297,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 298,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 299,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"activable": true,
																												"width": "70",
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"autocompletable": true,
																												"version": 1,
																												"browsable": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains"
																											},
																											"stamp": 300
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 301,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "ComboBox",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "",
																													"1": "_OpEx",
																													"2": "_CapEx"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "",
																													"1": "OpEx",
																													"2": "CapEx"
																												},
																												"label": "_CostType",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"readonly": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 302,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 303,
																									"data": [],
																									"*": {
																										"ItemGroupDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemGroupDescription__"
																											],
																											"options": {
																												"label": "_ItemGroupDescription",
																												"activable": true,
																												"width": 140,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"readonly": true,
																												"version": 1,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 304,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 305,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "ShortText",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemGroup",
																												"activable": true,
																												"width": "150",
																												"length": 64,
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 306
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 307,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 308,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 309,
																									"data": [],
																									"*": {
																										"ItemBudgetInitial__": {
																											"type": "Decimal",
																											"data": [
																												"ItemBudgetInitial__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item budget initial",
																												"activable": true,
																												"width": "90",
																												"readonly": true,
																												"autocompletable": false,
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 310
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 311,
																									"data": [],
																									"*": {
																										"ItemBudgetRemaining__": {
																											"type": "Decimal",
																											"data": [
																												"ItemBudgetRemaining__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item budget remaining",
																												"activable": true,
																												"width": "90",
																												"readonly": true,
																												"autocompletable": false,
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 312
																										}
																									}
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 313,
																									"data": [],
																									"*": {
																										"ItemBudgetCurrency__": {
																											"type": "ShortText",
																											"data": [
																												"ItemBudgetCurrency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "",
																												"activable": true,
																												"width": "40",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 314
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 315,
																									"data": [],
																									"*": {
																										"ItemPeriodCode__": {
																											"type": "ShortText",
																											"data": [
																												"ItemPeriodCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"readonly": true,
																												"textColor": "default",
																												"label": "_Item period code",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 316
																										}
																									}
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 317,
																									"data": [],
																									"*": {
																										"ItemBudgetViewed__": {
																											"type": "CheckBox",
																											"data": [
																												"ItemBudgetViewed__"
																											],
																											"options": {
																												"label": "_ItemBudgetViewed",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 318,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 319,
																									"data": [],
																									"*": {
																										"BuyerName__": {
																											"type": "ShortText",
																											"data": [
																												"BuyerName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Buyer name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 320,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 321,
																									"data": [],
																									"*": {
																										"BuyerLogin__": {
																											"type": "ShortText",
																											"data": [
																												"BuyerLogin__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Buyer login",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 322,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 323,
																									"data": [],
																									"*": {
																										"RecipientName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"RecipientName__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_Recipient name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"PreFillFTS": true,
																												"readonly": false,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 324,
																											"position": [
																												"Select from a table"
																											]
																										}
																									}
																								},
																								"Cell59": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 325,
																									"data": [],
																									"*": {
																										"RecipientLogin__": {
																											"type": "ShortText",
																											"data": [
																												"RecipientLogin__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Recipient login",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 326,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell60": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 327,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "CheckBox",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 328,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell61": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 329,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "CheckBox",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 330,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell62": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 331,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "LongText",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "ItemInCxml",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"resizable": true,
																												"browsable": false,
																												"version": 0,
																												"minNbLines": 3,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 332,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell63": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 333,
																									"data": [],
																									"*": {
																										"OutOfBudget__": {
																											"type": "CheckBox",
																											"data": [
																												"OutOfBudget__"
																											],
																											"options": {
																												"label": "_OutOfBudget",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"hidden": true,
																												"readonly": true,
																												"helpIconPosition": "Right",
																												"possibleValues": {},
																												"possibleKeys": {}
																											},
																											"stamp": 334,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell64": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 335,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "CheckBox",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 336,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell65": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 337,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Integer",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_LeadTime",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"browsable": false,
																												"autocompletable": false,
																												"readonly": true
																											},
																											"stamp": 338,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell66": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 339,
																									"data": [],
																									"*": {
																										"ItemOrigin__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemOrigin__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_ExternalCatalog",
																													"1": "_FreeItem",
																													"2": "_InternalCatalog"
																												},
																												"maxNbLinesToDisplay": 10,
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "ExternalCatalog",
																													"1": "FreeItem",
																													"2": "InternalCatalog"
																												},
																												"label": "_ItemOrigin",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 340,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell67": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 504,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "CheckBox",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"possibleValues": {},
																												"possibleKeys": {},
																												"hidden": true,
																												"version": 0,
																												"readonly": true
																											},
																											"stamp": 507,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 493
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 341,
													"*": {
														"PunchoutButtons": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PunchoutButtons",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 342,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LoadFromClipboard__": "LabelLoadFromClipboard__",
																			"LabelLoadFromClipboard__": "LoadFromClipboard__"
																		},
																		"version": 0
																	},
																	"stamp": 343,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"PunchoutButton1__": {
																						"line": 5,
																						"column": 1
																					},
																					"PunchoutButton2__": {
																						"line": 6,
																						"column": 1
																					},
																					"PunchoutButton3__": {
																						"line": 7,
																						"column": 1
																					},
																					"PunchoutButton4__": {
																						"line": 8,
																						"column": 1
																					},
																					"PunchoutButton5__": {
																						"line": 9,
																						"column": 1
																					},
																					"PunchoutButton6__": {
																						"line": 10,
																						"column": 1
																					},
																					"PunchoutButton7__": {
																						"line": 11,
																						"column": 1
																					},
																					"PunchoutButton8__": {
																						"line": 12,
																						"column": 1
																					},
																					"PunchoutButton9__": {
																						"line": 13,
																						"column": 1
																					},
																					"PunchoutButton10__": {
																						"line": 14,
																						"column": 1
																					},
																					"Button__": {
																						"line": 1,
																						"column": 1
																					},
																					"AddFromCatalog2__": {
																						"line": 2,
																						"column": 1
																					},
																					"UploadQuote2__": {
																						"line": 3,
																						"column": 1
																					},
																					"LoadFromClipboard__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 14,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 344,
																			"*": {
																				"Button__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Add items from",
																						"label": "Button",
																						"textPosition": "text-left",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "PR_Punchout_Empty.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 345
																				},
																				"AddFromCatalog2__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Add from catalog",
																						"label": "Bouton",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"urlImage": "PR_Punchout_from_catalog.png",
																						"style": 4,
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png"
																					},
																					"stamp": 346
																				},
																				"UploadQuote2__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Upload Quote",
																						"label": "Bouton2",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"urlImage": "PR_Punchout_from_quotation.png",
																						"style": 4,
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png"
																					},
																					"stamp": 347
																				},
																				"LoadFromClipboard__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_LoadFromClipboard",
																						"label": "Bouton",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "PR_Punchout_from_clipboard.png",
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 348
																				},
																				"PunchoutButton1__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 349
																				},
																				"PunchoutButton2__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 350
																				},
																				"PunchoutButton3__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 351
																				},
																				"PunchoutButton4__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 352
																				},
																				"PunchoutButton5__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 353
																				},
																				"PunchoutButton6__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 354
																				},
																				"PunchoutButton7__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": " ",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 355
																				},
																				"PunchoutButton8__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": "Button8",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 356
																				},
																				"PunchoutButton9__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": " ",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 357
																				},
																				"PunchoutButton10__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": " ",
																						"label": " ",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "none",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "PR_Punchout_Circle.png",
																						"hidden": true
																					},
																					"stamp": 358
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 359,
													"*": {
														"InventoryMovements": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "PR_reception.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_InventoryMovements",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "200px"
															},
															"stamp": 360,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 361,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"InventoryMovements__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 362,
																			"*": {
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 363
																				},
																				"InventoryMovements__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_InventoryMovements",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration"
																					},
																					"stamp": 364,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 365,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 366
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 367
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 368,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 369,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemNumberInventory",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 370,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "250",
																										"version": 0
																									},
																									"stamp": 490,
																									"data": [],
																									"*": {
																										"ItemName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemNameInventory",
																												"minwidth": "250",
																												"version": 0
																											},
																											"stamp": 491,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "300",
																										"version": 0
																									},
																									"stamp": 373,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_WarehouseNameInventory",
																												"minwidth": "300",
																												"version": 0
																											},
																											"stamp": 374,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "100",
																										"version": 0
																									},
																									"stamp": 375,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ItemQuantityInventory",
																												"type": "Decimal",
																												"minwidth": "100",
																												"version": 0
																											},
																											"stamp": 376,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 377,
																									"data": [],
																									"*": {
																										"InventoryMovementDateTime__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_MovementDateTimeInventory",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 378,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 379,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 380,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemNumberInventory",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 381,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 489,
																									"data": [],
																									"*": {
																										"ItemName__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemNameInventory",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 999,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 492,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 384,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WarehouseNameInventory",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 385,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 386,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemQuantityInventory",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"notInDB": true,
																												"dataType": "Number"
																											},
																											"stamp": 387,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 388,
																									"data": [],
																									"*": {
																										"InventoryMovementDateTime__": {
																											"type": "RealDateTime",
																											"data": [],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_MovementDateTimeInventory",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"notInDB": true,
																												"dataType": "Date",
																												"version": 0
																											},
																											"stamp": 389,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 390
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 391,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Attachments",
																"iconURL": "PR_attachments.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 392
																}
															},
															"stamp": 393,
															"data": []
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 394,
													"*": {
														"ApprovalWorkflow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "_ApprovalWorkflow",
																"version": 0,
																"iconURL": "PR_workflow.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 395,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ApprovedDate__": "LabelApproved_date__",
																			"LabelApproved_date__": "ApprovedDate__",
																			"LabelLast_validator_user_ID__": "LastValidatorUserID__",
																			"LastValidatorUserID__": "LabelLast_validator_user_ID__",
																			"LabelLast_validator_name__": "LastValidatorName__",
																			"LastValidatorName__": "LabelLast_validator_name__",
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__"
																		},
																		"version": 0
																	},
																	"stamp": 396,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ApproversList__": {
																						"line": 4,
																						"column": 1
																					},
																					"ApprovedDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelApproved_date__": {
																						"line": 6,
																						"column": 1
																					},
																					"Ligne_d_espacement5__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelLast_validator_user_ID__": {
																						"line": 7,
																						"column": 1
																					},
																					"LastValidatorUserID__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelLast_validator_name__": {
																						"line": 8,
																						"column": 1
																					},
																					"LastValidatorName__": {
																						"line": 8,
																						"column": 2
																					},
																					"Ligne_d_espacement4__": {
																						"line": 3,
																						"column": 1
																					},
																					"Comments__": {
																						"line": 2,
																						"column": 1
																					},
																					"ComputingWorkflow__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelComputingWorkflow__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 397,
																			"*": {
																				"LastValidatorName__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_Last validator name",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 398
																				},
																				"Comments__": {
																					"type": "LongText",
																					"data": [
																						"Comments__"
																					],
																					"options": {
																						"label": "_Comments",
																						"activable": true,
																						"width": "100%",
																						"numberOfLines": 5,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"minNbLines": 5
																					},
																					"stamp": 399
																				},
																				"Ligne_d_espacement4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement4",
																						"version": 0
																					},
																					"stamp": 400
																				},
																				"LabelLast_validator_name__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_Last validator name",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 401
																				},
																				"LastValidatorUserID__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorUserID__"
																					],
																					"options": {
																						"label": "_Last validator user ID",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 402
																				},
																				"LabelLast_validator_user_ID__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorUserID__"
																					],
																					"options": {
																						"label": "_Last validator user ID",
																						"version": 0
																					},
																					"stamp": 403
																				},
																				"LabelApproved_date__": {
																					"type": "Label",
																					"data": [
																						"ApprovedDate__"
																					],
																					"options": {
																						"label": "_Approved date",
																						"version": 0
																					},
																					"stamp": 404
																				},
																				"Ligne_d_espacement5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement5",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 405
																				},
																				"ApprovedDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"ApprovedDate__"
																					],
																					"options": {
																						"readonly": true,
																						"label": "_Approved date",
																						"activable": true,
																						"width": 230,
																						"displayLongFormat": false,
																						"autocompletable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 406
																				},
																				"ApproversList__": {
																					"type": "Table",
																					"data": [
																						"ApproversList__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 11,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Approvers list",
																						"readonly": true,
																						"width": "100%"
																					},
																					"stamp": 407,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 408,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 409,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 410,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 411,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 412,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 413
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0
																									},
																									"stamp": 414,
																									"data": [],
																									"*": {
																										"WRKFParallelMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFParallelMarker__"
																											],
																											"options": {
																												"label": "",
																												"minwidth": "0",
																												"version": 0
																											},
																											"stamp": 415,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 416,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "Label",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"version": 0
																											},
																											"stamp": 417
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 418,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "Label",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"version": 0
																											},
																											"stamp": 419
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 420,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "Label",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"label": "_WRKFDate",
																												"version": 0
																											},
																											"stamp": 421
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 422,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "Label",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"version": 0
																											},
																											"stamp": 423
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 424,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "Label",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 425
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 426,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 427
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 428,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 429,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 430,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "Label",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"label": "_WRKFActualApprover",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 431,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 432,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "Label",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"label": "_WRKFRequestDateTime",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 433,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 434,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 435,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"autocompletable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 436
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 437,
																									"data": [],
																									"*": {
																										"WRKFParallelMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFParallelMarker__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 438,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 439,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"activable": true,
																												"width": "180",
																												"version": 0,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 440
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 441,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"activable": true,
																												"width": "140",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 442
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 443,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFDate",
																												"activable": true,
																												"width": "150",
																												"version": 0
																											},
																											"stamp": 444
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 445,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "LongText",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"activable": true,
																												"width": "300",
																												"version": 0,
																												"resizable": true,
																												"numberOfLines": 5,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"minNbLines": 1
																											},
																											"stamp": 446
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 447,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"activable": true,
																												"width": "140",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 448
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 449,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"activable": true,
																												"width": "90",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 450
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 451,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 452,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 453,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFActualApprover",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 454,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 455,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFRequestDateTime",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 456,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelComputingWorkflow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 457
																				},
																				"ComputingWorkflow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"><div>",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 458
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 459
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "PR_system_data.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 460,
															"data": []
														}
													},
													"stamp": 461,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"hideActionButtons": true,
																"TitleFontSize": "S",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 462,
															"data": []
														}
													},
													"stamp": 463,
													"data": []
												}
											},
											"stamp": 464,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"hidden": true,
																"iconURL": "PR_preview.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 465
																}
															},
															"stamp": 466,
															"data": []
														}
													},
													"stamp": 467,
													"data": []
												}
											},
											"stamp": 468,
											"data": []
										}
									},
									"stamp": 469,
									"data": []
								}
							},
							"stamp": 470,
							"data": []
						}
					},
					"stamp": 471,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0
							},
							"stamp": 472,
							"data": []
						},
						"Save_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Save_",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 473
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 474,
							"data": []
						},
						"Reject": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Reject",
								"action": "reject",
								"version": 0,
								"style": 3
							},
							"stamp": 475
						},
						"BackToAP": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "BackToAP",
								"action": "approve",
								"version": 0,
								"style": 2
							},
							"stamp": 476
						},
						"ModifyPR": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "ModifyPR",
								"action": "none",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"style": 2
							},
							"stamp": 477
						},
						"Request_for_information": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Request for information",
								"action": "approve",
								"style": 2,
								"version": 0
							},
							"stamp": 478
						},
						"Cancel_purchase_requisition": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Cancel purchase requisition",
								"action": "approve",
								"style": 2,
								"version": 0
							},
							"stamp": 479
						},
						"Submit_": {
							"type": "SubmitButton",
							"options": {
								"label": "_Submit purchase requisition",
								"action": "approve",
								"submit": true,
								"version": 0,
								"style": 1
							},
							"stamp": 480,
							"data": []
						},
						"Comment_Answer": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Comment & Answer",
								"action": "approve",
								"style": 1,
								"version": 0
							},
							"stamp": 481
						},
						"Generate_P_O_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "TEST : Generate P.O.",
								"action": "approve",
								"version": 0
							},
							"stamp": 482
						},
						"SynchronizeItems": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "SynchronizeItems",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 2,
								"url": "",
								"action": "approve",
								"version": 0
							},
							"stamp": 483
						},
						"CreatePO": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "CreatePO",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Purchase order V2",
									"attachmentsMode": "allasattachments",
									"willBeChild": false,
									"returnToOriginalUrl": true,
									"startWithoutProcessing": true
								},
								"url": "",
								"action": "openprocess",
								"version": 0
							},
							"stamp": 484
						},
						"Cancel_remaining_items": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Cancel remaining items",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"style": 2,
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 485
						},
						"ClonePR": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_ClonePR",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 486
						}
					},
					"stamp": 487,
					"data": []
				}
			},
			"stamp": 488,
			"data": []
		}
	},
	"stamps": 507,
	"data": []
}