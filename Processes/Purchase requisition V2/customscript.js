///#GLOBALS Lib Sys
Log.Time("CustomScript");
var fieldReseter = new Lib.Purchasing.FieldReseter();
var workflow = Lib.Purchasing.LayoutPR.WorkflowControllerInstance;
/** *************** **/
/** GENERIC HELPERS **/
/** *************** **/
// DEPRECATED !!! Use Lib.P2P.GetValidatorOrOwner()
function SetLastValidator() {
    Log.Info("[DEPRECATED] Set last validator to: " + User.loginId);
    Controls.LastValidatorName__.SetValue(User.fullName);
    Controls.LastValidatorUserID__.SetValue(User.loginId);
}
var ApprovalCommentCheck = {
    GetCommentField: function () {
        return Controls.Comments__;
    },
    // Fill dialog callback: Design and instantiation of the controls
    fill_CB: function (dialogTexts) {
        return function (dialog /*, tabId, event, control*/) {
            var commentValue = ApprovalCommentCheck.GetCommentField().GetValue();
            var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
            if (Sys.Helpers.IsEmpty(commentValue)) {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionRequired));
            }
            else {
                ctrl.SetText(Language.Translate(dialogTexts.descriptionConfirmation));
            }
            var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
            dialog.RequireControl(commentCtrl);
            commentCtrl.SetValue(commentValue);
        };
    },
    // Validate dialog callback: checks if required fields are set
    validate_CB: function (dialog /*, tabId, event, control*/) {
        if (!dialog.GetControl("ctrlComments").GetValue()) {
            dialog.GetControl("ctrlComments").SetError("This field is required!");
            return false;
        }
        return true;
    },
    // Commit dialog callback: updates the process form with dialog results
    commit_CB: function (anAction, onCommitted) {
        var action = anAction;
        return function (dialog /*, tabId, event, control*/) {
            var newValue = dialog.GetControl("ctrlComments").GetValue();
            ApprovalCommentCheck.GetCommentField().SetValue(newValue);
            ApprovalCommentCheck.GetCommentField().SetError("");
            SetLastValidator();
            Events.OnQuit();
            if (Data.GetValue("state") != 90) {
                if (action === "Submit_") {
                    submit();
                }
                else if (action === "ModifyPR") {
                    ProcessInstance.Approve(action);
                }
                else if (action === "CancelPR") {
                    OnConfirmCancel();
                }
                else {
                    ProcessInstance.ApproveAsynchronous(action);
                }
            }
            else {
                ProcessInstance.ResumeWithActionAsynchronous(action, { "Comments__": newValue, "LastValidatorName__": User.fullName, "LastValidatorUserID__": User.loginId });
            }
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted();
            }
        };
    },
    OnClick: function (action, dialogTexts, onCommitted) {
        var defaultDialogTexts = {
            titleConfirmation: "_Approval comments confirmation",
            titleRequired: "_Approval comments required",
            descriptionConfirmation: "_Please confirm your comment:",
            descriptionRequired: "_Please write your comment:"
        }, att;
        if (!dialogTexts) {
            dialogTexts = defaultDialogTexts;
        }
        else {
            for (att in defaultDialogTexts) {
                if (!(att in dialogTexts)) {
                    dialogTexts[att] = defaultDialogTexts[att];
                }
            }
        }
        var title = ApprovalCommentCheck.GetCommentField().GetValue() ? dialogTexts.titleConfirmation : dialogTexts.titleRequired;
        Popup.Dialog(title, null, ApprovalCommentCheck.fill_CB(dialogTexts), ApprovalCommentCheck.commit_CB(action, onCommitted), ApprovalCommentCheck.validate_CB);
        return false;
    }
};
var CompanyCodeManager = {
    previousCompanyCode: "",
    UpdateCompanyCodeDependencies: function () {
        CompanyCodeManager.previousCompanyCode = Data.GetValue("CompanyCode__");
        var newcc = Data.GetValue("CompanyCode__");
        var syncCallbacks = Sys.Helpers.Synchronizer.Create(function () {
            Events.InitCatalogBrowse();
            Lib.Purchasing.LayoutPR.DelayRebuildWorkflow();
            Lib.Purchasing.LayoutPR.GlobalLayout.HideWaitScreen(true);
        }, null, { progressDelay: 15000, OnProgress: Lib.P2P.OnSynchronizerProgress });
        Lib.Purchasing.LayoutPR.GlobalLayout.HideWaitScreen(false);
        Lib.P2P.CompanyCodesValue.QueryValues(newcc, true).Then(function (CCValues) {
            Lib.Purchasing.SetERPByCompanyCode(newcc)
                .Then(function () {
                Lib.Purchasing.ShipTo.Reset();
                Lib.Purchasing.PRLineItems.Reset();
                if (Object.keys(CCValues).length > 0) {
                    Data.SetValue("LocalCurrency__", CCValues.Currency__);
                    Data.SetValue("Currency__", CCValues.Currency__);
                    Controls.ExchangeRate__.SetValue(CCValues.currencies.GetRate(CCValues.Currency__));
                    Data.SetValue("PurchasingOrganization__", CCValues.PurchasingOrganization__);
                    Data.SetValue("PurchasingGroup__", CCValues.PurchasingGroup__);
                    Variable.SetValueAsString("companyCurrency", CCValues.Currency__);
                }
                else {
                    Data.SetError("CompanyCode__", "_This CompanyCode does not exist in the table.");
                }
                /** ************************** **/
                /** ERP Specific initialization */
                /** ************************** **/
                FixLayout();
                syncCallbacks.Register(Lib.Purchasing.ShipTo.FillFromCompanyCode());
                syncCallbacks.Register(Lib.Purchasing.CatalogHelper.SupplyTypesManager.Query());
                syncCallbacks.Start();
            });
        });
    },
    RevertCompanyCode: function () {
        Controls.CompanyCode__.SetValue(CompanyCodeManager.previousCompanyCode);
    },
    SetPreviousCompanyCode: function (companyCode) {
        CompanyCodeManager.previousCompanyCode = companyCode;
    }
};
function submit() {
    var customization = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.DialogAutoOrderCustomization");
    function SendPR() {
        SetLastValidator();
        if (Lib.Purchasing.LayoutPR.IsRequesterStep() || !Lib.Purchasing.WorkflowPR.HasAdditionalContributors()) {
            ProcessInstance.ApproveAsynchronous("Submit_");
        }
        else {
            ProcessInstance.ApproveAsynchronous("Approve_Forward");
        }
    }
    function fillDialogCallback(dialog /*, tabId, event, control*/) {
        var ctrlDesc = dialog.AddDescription("desc", null, 450);
        ctrlDesc.SetText(Language.Translate("_Auto order popup PR"));
        dialog.AddSeparator();
        var ctrl_autoPO = dialog.AddCheckBox("autoPO", "_Automatically send purchase order");
        var ctrl_SendTo = dialog.AddComboBox("cb_SendTo", "_Send notification to");
        var sendOptions = {
            "DefaultOrderTransmissionMethod": "_Default Order Transmission Method",
            "SendToVendor": "_Send to vendor",
            "PunchoutMode": "_PunchoutMode",
            "DontSend": "_Don't send"
        };
        var isSingleVendor = Lib.Purchasing.CheckPR.isSingleVendor(Data);
        var isSingleAndKnownVendor = isSingleVendor && Data.GetTable("LineItems__").GetItemCount() > 0 && Data.GetTable("LineItems__").GetItem(0).GetValue("VendorName__");
        var isPunchoutModeAllowed = Lib.Purchasing.HasOnlyPunchoutItems();
        if (!isPunchoutModeAllowed) {
            delete sendOptions.PunchoutMode;
        }
        if (isSingleVendor) {
            delete sendOptions.DefaultOrderTransmissionMethod;
        }
        else {
            ctrl_autoPO.SetValue(true);
        }
        var availableValues = Sys.Helpers.Array.Map(Object.keys(sendOptions), function (v) { return v + "=" + sendOptions[v]; }).join("\n");
        ctrl_SendTo.SetAvailableValues(availableValues);
        var asyncFill = false;
        if (isPunchoutModeAllowed) {
            ctrl_autoPO.SetValue(true);
            ctrl_SendTo.SetValue("PunchoutMode");
        }
        else if (isSingleAndKnownVendor) {
            asyncFill = true;
            Lib.Purchasing.Vendor.QueryPOTransmissionPreferences(Data.GetValue("CompanyCode__"), Data.GetTable("LineItems__").GetItem(0).GetValue("VendorNumber__"))
                .Then(function (preferences) {
                var POMethod = preferences === null || preferences === void 0 ? void 0 : preferences.POMETHOD__;
                if (!POMethod || POMethod === "PunchoutMode" || POMethod === "Prompt") {
                    ctrl_autoPO.SetValue(false);
                    ctrl_SendTo.SetValue("SendToVendor");
                }
                else {
                    ctrl_autoPO.SetValue(true);
                    ctrl_SendTo.SetValue(POMethod);
                }
            })
                .Catch(function (e) {
                Log.Error("Error querying preferred PO method: " + e);
            })
                .Finally(function () {
                if (asyncFill) {
                    if (customization) {
                        customization.FillAutoOrderPopup(dialog);
                    }
                    dialog.AddSeparator();
                }
            });
        }
        if (!asyncFill) {
            if (customization) {
                customization.FillAutoOrderPopup(dialog);
            }
            dialog.AddSeparator();
        }
    }
    function commitDialogCallback(dialog /*, tabId, event, control*/) {
        var autoSend = dialog.GetControl("autoPO").GetValue();
        var autoSendOrderParam = {};
        if (autoSend) {
            var emailNotifType = dialog.GetControl("cb_SendTo").GetValue();
            Variable.SetValueAsString("AutoCreateOrderEnabled", true);
            autoSendOrderParam = {
                "autoOrder": "true",
                "EmailNotificationOptions": emailNotifType
            };
            //Buyer is requester
            // Saves the buyer's full name as an external variable for the templates
            Variable.SetValueAsString("BuyerFullName__", User.fullName);
            Variable.SetValueAsString("BuyerEmail__", User.emailAddress);
            Variable.SetValueAsString("BuyerLogin__", User.loginId);
        }
        if (customization) {
            customization.CommitAutoOrderPopup(dialog, autoSendOrderParam);
        }
        Variable.SetValueAsString("AutoSendOrderParam", JSON.stringify(autoSendOrderParam));
        SendPR();
    }
    function handleDialogCallback(dialog, tabId, event, control, param1, param2) {
        if (customization) {
            customization.HandleAutoOrderPopup(dialog, tabId, event, control, param1, param2);
        }
    }
    function cancelDialogCallback( /*dialog, tabId, event, control*/) {
        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
        return false;
    }
    function rollbackDialogCallback(dialog /*, tabId, event, control*/) {
        if (customization) {
            customization.CancelAutoOrderPopup(dialog);
        }
    }
    function IsEligibleToAutoOrder() {
        var ret = null;
        if (customization) {
            ret = customization.IsEligibleToAutoOrder();
        }
        if (ret === null) {
            var table = Data.GetTable("LineItems__");
            var nbRow = table.GetItemCount();
            var buyerLogin = table.GetItem(0).GetValue("BuyerLogin__");
            var vendors = Object.create(null);
            var isMonoCurrency = true;
            var isMonoBuyer = true;
            var previousCurrency = void 0;
            for (var i = 0; i < nbRow; ++i) {
                var row = table.GetItem(i);
                if (!Sys.Helpers.IsEmpty(row.GetValue("VendorName__"))) {
                    previousCurrency = vendors[row.GetValue("VendorName__")];
                    if (previousCurrency && previousCurrency != row.GetValue("ItemCurrency__")) {
                        isMonoCurrency = false;
                        break;
                    }
                    vendors[row.GetValue("VendorName__")] = row.GetValue("ItemCurrency__");
                }
                var itemBuyer = row.GetValue("BuyerLogin__");
                if (!Sys.Helpers.IsEmpty(itemBuyer) && itemBuyer !== buyerLogin) {
                    isMonoBuyer = false;
                }
            }
            ret = Object.keys(vendors).length > 0 && isMonoCurrency && isMonoBuyer && (User.loginId == buyerLogin || User.IsMemberOf(buyerLogin));
        }
        return ret;
    }
    function ClearLeadTimeWarnings() {
        var table = Data.GetTable("LineItems__");
        var nbRow = table.GetItemCount();
        for (var i = 0; i < nbRow; ++i) {
            table.GetItem(i).SetWarning("ItemRequestedDeliveryDate__", "");
        }
    }
    // Check every items have been viewed when the next contributor is buyer. This check is done on server side too but avoid any async notification.
    var currentContributor = workflow.GetContributorAt(workflow.GetContributorIndex());
    var nextContributor = workflow.GetContributorAt(workflow.GetContributorIndex() + 1);
    if (nextContributor && nextContributor.role === Lib.Purchasing.roleBuyer && !Lib.Purchasing.PRBudget.IsBudgetViewedByEveryApprover()) {
        Sys.Helpers.SilentChange(function () {
            Lib.CommonDialog.NextAlert.Define("_Budget update error", "_NotAllItemsApproved");
            Lib.CommonDialog.NextAlert.Show();
        });
        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
    }
    else {
        ClearLeadTimeWarnings();
        // Only check if the PR is eligible to Auto order at the requester step
        if ((currentContributor.role === Lib.Purchasing.roleRequester) && IsEligibleToAutoOrder()) {
            Popup.Dialog("_Auto order popup", null, fillDialogCallback, commitDialogCallback, null, handleDialogCallback, cancelDialogCallback, rollbackDialogCallback);
        }
        else {
            SendPR();
        }
    }
}
function OnConfirmCancel() {
    SetLastValidator();
    Events.OnQuit();
    ProcessInstance.ApproveAsynchronous("Cancel_purchase_requisition");
}
//#region Events
var Events = (function () {
    function OnClickRequestForInformation() {
        var requisitionStatus = Controls.RequisitionStatus__.GetValue();
        // We only check error during approval workflow. In "To complete" status, the form may be partially filled...
        if (requisitionStatus === "To approve" && (!Lib.Purchasing.CheckPR.requiredFields.CheckAll(Lib.Purchasing.LayoutPR.GetCurrentLayout()) || Process.ShowFirstError() !== null)) {
            Process.ShowFirstError();
            Lib.Purchasing.PRLineItems.LeaveEmptyLine();
            return false;
        }
        var dialogTexts = {
            title: "_Add advisor",
            role: "_Advisor",
            description: "_Request for information description",
            description_size: 475,
            confirmation: "_Request for information confirm message",
            confirmation_size: 475,
            browseTitle: "_Browse advisor"
        };
        Lib.P2P.Browse.AddApprover(ApprovalCommentCheck.GetCommentField().GetValue(), function (contributor, comment) {
            if (contributor) {
                if (Lib.Purchasing.LayoutPR.IsRequesterStep()) {
                    //workflow will be recomputed, additionalApprovers will be lost
                    Lib.Purchasing.WorkflowPR.RemoveAllAdditionalContributors();
                }
                SetLastValidator();
                ApprovalCommentCheck.GetCommentField().SetValue(comment);
                Variable.SetValueAsString("AdvisorLogin", contributor.login);
                Variable.SetValueAsString("AdvisorName", contributor.displayName);
                Variable.SetValueAsString("AdvisorEmail", contributor.emailAddress);
                Events.OnQuit();
                ProcessInstance.ApproveAsynchronous("RequestForInformation");
            }
        }, false, dialogTexts);
        return false;
    }
    function OnClickPostButton() {
        if (!ProcessInstance.state) {
            ProcessInstance.DisableExtractionScript();
        }
        ApprovalCommentCheck.GetCommentField().SetError("");
        Lib.Purchasing.Items.NumberLineItems();
        Lib.Purchasing.CheckPR.requiredFields.CheckAll(Lib.Purchasing.LayoutPR.GetCurrentLayout())
            .Then(function (isValid) {
            if (isValid && Process.ShowFirstError() === null) {
                if ((Lib.Purchasing.PRLineItems.Budget.IsOut() || Lib.Purchasing.PRLineItems.Budget.IsUndefined()) && Lib.Purchasing.LayoutPR.IsApprover() && Lib.Purchasing.PRLineItems.Budget.HasVisibilityOnCurrentBudget()) {
                    if (Lib.Purchasing.PRLineItems.Budget.IsUndefined() && Lib.Purchasing.UndefinedBudgetBehavior.IsPrevented()) {
                        Popup.Alert("_One or more items have not been allocated a budget", true, null, "_Undefined budget");
                        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else if (Lib.Purchasing.PRLineItems.Budget.IsOut() && Lib.Purchasing.OutOfBudgetBehavior.IsPrevented()) {
                        Popup.Alert("_One or more items are out of budget", true, null, "_Out of budget");
                        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
                    }
                    else {
                        var dialogTexts = {};
                        if (Lib.Purchasing.PRLineItems.Budget.IsOut() && Lib.Purchasing.PRLineItems.Budget.IsUndefined()) {
                            dialogTexts =
                                {
                                    titleConfirmation: "_Approval comments confirmation - budget warning",
                                    titleRequired: "_Approval comments required - budget warning",
                                    descriptionConfirmation: "_Please confirm your comment - out and undefined budget:",
                                    descriptionRequired: "_Please write your comment - out and undefined budget:"
                                };
                        }
                        else if (Lib.Purchasing.PRLineItems.Budget.IsOut()) {
                            dialogTexts =
                                {
                                    titleConfirmation: "_Approval comments confirmation - budget warning",
                                    titleRequired: "_Approval comments required - budget warning",
                                    descriptionConfirmation: "_Please confirm your comment - out of budget:",
                                    descriptionRequired: "_Please write your comment - out of budget:"
                                };
                        }
                        else if (Lib.Purchasing.PRLineItems.Budget.IsUndefined()) {
                            dialogTexts =
                                {
                                    titleConfirmation: "_Approval comments confirmation - budget warning",
                                    titleRequired: "_Approval comments required - budget warning",
                                    descriptionConfirmation: "_Please confirm your comment - undefined budget:",
                                    descriptionRequired: "_Please write your comment - undefined budget:"
                                };
                        }
                        ApprovalCommentCheck.OnClick("Submit_", dialogTexts);
                    }
                }
                else {
                    submit();
                }
            }
            else {
                Process.ShowFirstError();
                Lib.Purchasing.PRLineItems.LeaveEmptyLine();
            }
        })
            .Catch(function (error) {
            Log.Error("Unhandled promise: " + error + ". Prevent posting.");
            Process.ShowFirstError();
            Lib.Purchasing.PRLineItems.LeaveEmptyLine();
        });
        return false;
    }
    function OnClickCancelRemainingItemsButton() {
        ApprovalCommentCheck.GetCommentField().SetError("");
        var dialogTexts = {
            titleConfirmation: "_Cancel remaining items comments confirmation",
            titleRequired: "_Cancel remaining items comments required",
            descriptionConfirmation: "_Please confirm your comment - cancel remaining items:",
            descriptionRequired: "_Please write your comment - cancel remaining items:"
        };
        ApprovalCommentCheck.OnClick("Cancel_remaining_items", dialogTexts);
    }
    function RemoveApprovalError() {
        ApprovalCommentCheck.GetCommentField().SetError("");
        return null;
    }
    function SaveOnClick() {
        RemoveApprovalError();
        Events.OnQuit();
        Lib.Purchasing.Items.NumberLineItems();
        ProcessInstance.Approve("Save_");
        return null;
    }
    function OnclickCancelPR() {
        Popup.Confirm("_Cancel_explanation", false, OnConfirmCancel, null, "_Cancel confirmation");
        SetLastValidator();
        return false;
    }
    /* Add item from catalog button */
    var customCallback = function (callback, result) {
        callback(result);
    };
    var addFromCatalogCustomFilter = "";
    var customQueryCallback = function (callback, Table, Attributes, LdapFilter, SortOrder, MaxRecords, config, option) {
        // create a new custom filter using both dialog's user inputs, only if current filter is not null.
        if (LdapFilter && LdapFilter != addFromCatalogCustomFilter) {
            addFromCatalogCustomFilter = Controls.LineItems__.ItemDescription__.GetCustomFilter(true);
            var ldapUtil = Sys.Helpers.LdapUtil;
            var userSearchInput1 = Sys.ERP.SAP.Browse.ExtractFieldValue(LdapFilter, "ItemDescription__");
            var userSearchInput2 = Sys.ERP.SAP.Browse.ExtractFieldValue(LdapFilter, "SupplyTypeID__.Name__");
            LdapFilter = "";
            if (userSearchInput1) {
                LdapFilter = ldapUtil.FilterEqual("ItemDescription__", userSearchInput1);
                LdapFilter = ldapUtil.FilterOr(LdapFilter, ldapUtil.FilterEqual("ItemNumber__", userSearchInput1));
            }
            if (userSearchInput2) {
                LdapFilter = ldapUtil.FilterAnd(LdapFilter, addFromCatalogCustomFilter + ldapUtil.FilterEqual("SupplyTypeID__.Name__", userSearchInput2));
            }
            else {
                LdapFilter = ldapUtil.FilterAnd(LdapFilter, addFromCatalogCustomFilter);
            }
            LdapFilter = LdapFilter.toString();
        }
        Query.DBQuery(function () {
            customCallback(callback, this.GetQueryValue());
        }, Table, Attributes, LdapFilter, SortOrder, MaxRecords, null, option);
    };
    var publicEvents = {
        OnQuit: function () {
            Lib.Purchasing.RemoveEmptyLineItem(Data.GetTable("LineItems__"));
            fieldReseter.Clean();
        },
        CompanyCodeChange: function () {
            setTimeout(function () {
                Popup.Confirm("_This action will delete company code related fields.", false, CompanyCodeManager.UpdateCompanyCodeDependencies, CompanyCodeManager.RevertCompanyCode, "_Warning");
            });
        },
        //init must be done after SupplyType are init
        InitCatalogBrowse: function () {
            Log.Time("Events.InitCatalogBrowse");
            addFromCatalogCustomFilter = Controls.LineItems__.ItemDescription__.GetCustomFilter(true);
            Controls.LineItems__.ItemDescription__.SetCustomQuery(customQueryCallback, null, null, null);
            Controls.LineItems__.ItemDescription__.SetSearchMode("contains");
            Log.TimeEnd("Events.InitCatalogBrowse");
        }
    };
    // declare callbacks...
    /* general informations */
    Controls.CompanyCode__.OnSelectItem = publicEvents.CompanyCodeChange;
    /* Line items */
    Controls.LineItems__.OnAddItem = Lib.Purchasing.PRLineItems.OnAddItem;
    Controls.LineItems__.OnDeleteItem = Lib.Purchasing.PRLineItems.OnDeleteItem;
    Controls.LineItems__.OnDuplicateItem = Lib.Purchasing.PRLineItems.OnDuplicateItem;
    Controls.LineItems__.OnRefreshRow = Lib.Purchasing.PRLineItems.OnRefreshRow;
    Controls.LineItems__.ItemType__.OnChange = Lib.Purchasing.PRLineItems.ItemType.OnChange;
    Controls.LineItems__.ItemNumber__.OnSelectItem = Lib.Purchasing.PRLineItems.NumberOrDescription.OnSelectItem;
    Controls.LineItems__.ItemNumber__.OnChange = Lib.Purchasing.PRLineItems.NumberOrDescription.OnChange;
    Controls.LineItems__.ItemDescription__.OnSelectItem = Lib.Purchasing.PRLineItems.NumberOrDescription.OnSelectItem;
    Controls.LineItems__.ItemDescription__.OnChange = Lib.Purchasing.PRLineItems.NumberOrDescription.OnChange;
    Controls.LineItems__.ItemDescription__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.CostType__.OnChange = Lib.Purchasing.PRLineItems.CostType.OnChange;
    Controls.LineItems__.CostType__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemUnitPrice__.OnChange = Lib.Purchasing.PRLineItems.UnitPrice.OnChange;
    Controls.LineItems__.ItemUnitPrice__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemNetAmount__.OnChange = Lib.Purchasing.PRLineItems.NetAmount.OnChange;
    Controls.LineItems__.ItemQuantity__.OnChange = Lib.Purchasing.PRLineItems.Quantity.OnChange;
    Controls.LineItems__.ItemQuantity__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemGLAccount__.OnSelectItem = Lib.Purchasing.PRLineItems.GLAccount.OnSelectItem;
    Controls.LineItems__.ItemGLAccount__.OnChange = Lib.Purchasing.PRLineItems.GLAccount.OnChange;
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ProjectCodeDescription__, Lib.Purchasing.PRLineItems.ProjectCodeDescription.OnSelectItem, Lib.Purchasing.PRLineItems.ProjectCodeDescription.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.ProjectCodeDescription.OnUnknownOrEmptyValue);
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ProjectCode__, Lib.Purchasing.PRLineItems.ProjectCode.OnSelectItem, Lib.Purchasing.PRLineItems.ProjectCode.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.ProjectCode.OnUnknownOrEmptyValue);
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.WBSElement__, Lib.Purchasing.PRLineItems.WBSElement.OnSelectItem, Lib.Purchasing.PRLineItems.WBSElement.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.WBSElement.OnUnknownOrEmptyValue);
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.WBSElementID__, Lib.Purchasing.PRLineItems.WBSElementID.OnSelectItem, Lib.Purchasing.PRLineItems.WBSElementID.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.WBSElementID.OnUnknownOrEmptyValue);
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1ID__", "Code__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"));
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1ID__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1__", "Description__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"));
    Controls.LineItems__.WBSElement__.SetAttributes("WBSElementID__");
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ItemGroupDescription__, Lib.Purchasing.PRLineItems.ExpenseCategory.OnSelectItem, Lib.Purchasing.PRLineItems.ExpenseCategory.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.ExpenseCategory.OnUnknownOrEmptyValue);
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ItemCostCenterName__, Lib.Purchasing.PRLineItems.CostCenterName.OnSelectItem, Lib.Purchasing.PRLineItems.CostCenterName.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.CostCenterName.OnUnknownOrEmptyValue);
    Controls.LineItems__.ItemCostCenterName__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ItemCostCenterId__, Lib.Purchasing.PRLineItems.CostCenterId.OnSelectItem, Lib.Purchasing.PRLineItems.CostCenterId.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.CostCenterId.OnUnknownOrEmptyValue);
    Controls.LineItems__.ItemCostCenterId__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.SupplyTypeName__.SetAttributes("SupplyID__");
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.SupplyTypeName__, Lib.Purchasing.PRLineItems.SupplyType.OnSelectItem, Lib.Purchasing.PRLineItems.SupplyType.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.SupplyType.OnUnknownOrEmptyValue);
    Controls.LineItems__.SupplyTypeName__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemStartDate__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemStartDate__.OnChange = Lib.Purchasing.PRLineItems.StartDate.OnChange;
    Controls.LineItems__.ItemEndDate__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemEndDate__.OnChange = Lib.Purchasing.PRLineItems.EndDate.OnChange;
    Controls.LineItems__.ItemRequestedDeliveryDate__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemRequestedDeliveryDate__.OnChange = Lib.Purchasing.PRLineItems.RequestedDeliveryDate.OnChange;
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.ItemTaxCode__, Lib.Purchasing.PRLineItems.TaxCode.OnSelectItem, Lib.Purchasing.PRLineItems.TaxCode.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.TaxCode.OnUnknownOrEmptyValue);
    Controls.LineItems__.ItemCurrency__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemCurrency__.OnSelectItem = Lib.Purchasing.PRLineItems.ItemCurrency.OnSelectItem;
    Controls.LineItems__.ItemUnit__.OnSelectItem = Lib.Purchasing.PRLineItems.ItemUnit.OnSelectItem;
    Controls.LineItems__.ItemUnit__.OnChange = Lib.Purchasing.PRLineItems.ItemUnit.OnChange;
    Controls.LineItems__.ItemUnit__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.ItemUnitDescription__.OnSelectItem = Lib.Purchasing.PRLineItems.ItemUnitDescription.OnSelectItem;
    Controls.LineItems__.ItemUnitDescription__.OnChange = Lib.Purchasing.PRLineItems.ItemUnitDescription.OnChange;
    Controls.LineItems__.ItemUnitDescription__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.PONumber__.OnClick = Lib.Purchasing.PRLineItems.PONumber.OnClick;
    Controls.LineItems__.RecipientName__.ClearSearchFields();
    Controls.LineItems__.RecipientName__.AddSearchField({ type: "Text", label: "_Name", id: "DisplayName" });
    Controls.LineItems__.RecipientName__.AddSearchField({ type: "Text", label: "_Login", id: "Login" });
    Controls.LineItems__.RecipientName__.SetSearchMode("contains");
    Controls.LineItems__.RecipientName__.ClearButtons();
    Controls.LineItems__.RecipientName__.AddButton("Me__", "_I am the recipient", { DISPLAYNAME: User.fullName, LOGIN: User.loginId, EMAILADDRESS: User.emailAddress });
    Controls.LineItems__.RecipientName__.SetImageColumn("DisplayName", {
        "IsGroup": [{ value: "1", imageUrl: Lib.P2P.GetP2PUserImage(true) }], "defaultImageUrl": Lib.P2P.GetP2PUserImage(false)
    }, 18);
    Controls.LineItems__.RecipientName__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    Controls.LineItems__.BuyerName__.OnBlur = Lib.Purchasing.PRLineItems.RequiredFields.OnBlur;
    /** @this DatabaseComboBox & IControlInTable<Purchase_Requisition_process_V2LineItemsTableRow>*/
    var recipientOnSelectItem = function (item) {
        var login = item.GetValue("Login");
        this.GetRow().RecipientLogin__.SetValue(login);
    };
    /** @this DatabaseComboBox & IControlInTable<Purchase_Requisition_process_V2LineItemsTableRow>*/
    var recipientOnUnknownOrEmptyValue = function () {
        this.GetRow().RecipientLogin__.SetValue("");
    };
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.RecipientName__, recipientOnSelectItem, recipientOnUnknownOrEmptyValue, recipientOnUnknownOrEmptyValue);
    Controls.AddFromCatalog2__.OnClick = function () {
        if (Lib.Purchasing.PRLineItems.Count() >= Controls.LineItems__.GetLineCount()) {
            var Options = {
                message: Language.Translate("You can not add more than {0} items", false, Controls.LineItems__.GetLineCount()),
                status: "warning"
            };
            Popup.Snackbar(Options);
        }
        else {
            var timestamp = new Date().getTime();
            var localStorageName = "cart" + timestamp;
            Data.OnStorageChange(localStorageName, Lib.Purchasing.CatalogCallback);
            var companyCode = Controls.CompanyCode__.GetValue();
            var url = "FlexibleForm.aspx?action=run&layout=_flexibleform&pName=PurchasingCart__&ls=cart" + timestamp + "&companycode=" + encodeURIComponent(companyCode);
            var filter = Controls.LineItems__.ItemDescription__.GetCustomFilter(true);
            if (filter) {
                url += "&filter=" + encodeURIComponent(filter);
            }
            var catalogs = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.GetCatalogs", companyCode, "PurchasingOrderedItems__");
            if (catalogs) {
                url += "&catalogs=" + encodeURIComponent(Sys.Helpers.IsArray(catalogs) ? catalogs.join(";") : catalogs);
            }
            Process.OpenLink({ url: url, inCurrentTab: false });
        }
    };
    Controls.ClonePR.OnClick = function () {
        ProcessInstance.OpenInProcess({
            processName: "Purchase requisition V2",
            attachmentsMode: "none",
            willBeChild: false,
            startWithoutProcessing: true,
            doNotCopyExternalVarsFromAncestor: true
        });
    };
    /* Ship to */
    Lib.Purchasing.ShipTo.InitControls();
    /* Vendor */
    Controls.LineItems__.VendorNumber__.SetAttributes("Email__|VATNumber__|FaxNumber__");
    Controls.LineItems__.VendorNumber__.OnSelectItem = Lib.Purchasing.PRLineItems.Vendor.OnSelectItem;
    Controls.LineItems__.VendorName__.SetAttributes("Email__|VATNumber__|FaxNumber__");
    Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.VendorName__, Lib.Purchasing.PRLineItems.Vendor.OnSelectItem, Lib.Purchasing.PRLineItems.Vendor.OnUnknownOrEmptyValue, Lib.Purchasing.PRLineItems.Vendor.OnUnknownOrEmptyValue);
    var vendorsExtraFilter = Sys.Helpers.TryCallFunction("Lib.P2P.Customization.Common.GetVendorsExtraFilter");
    if (vendorsExtraFilter && vendorsExtraFilter.length > 0) {
        var vendorFilter = Sys.Helpers.Browse.BuildLDAPFilter(vendorsExtraFilter);
        Log.Info("Extra filter defined for vendors: " + vendorFilter);
        var vendorsCustomFilter = Controls.LineItems__.VendorName__.GetCustomFilter(false);
        if (vendorsCustomFilter) {
            if (vendorFilter.charAt(0) !== '(') {
                vendorFilter = "(" + vendorFilter + ")";
            }
            if (vendorsCustomFilter.charAt(0) !== '(') {
                vendorsCustomFilter = "(" + vendorsCustomFilter + ")";
            }
            vendorFilter = "(&" + vendorFilter + vendorsCustomFilter + ")";
        }
        Log.Info("Setting filter for vendors: " + vendorFilter);
        Controls.LineItems__.VendorName__.SetFilter(vendorFilter);
    }
    /* buttons */
    Controls.Submit_.OnClick = OnClickPostButton;
    Controls.Request_for_information.OnClick = OnClickRequestForInformation;
    Controls.Cancel_purchase_requisition.OnClick = function () {
        var requisitionStatus = Controls.RequisitionStatus__.GetValue();
        if (Lib.Purchasing.LayoutPR.IsRequester() &&
            requisitionStatus !== "To approve" && requisitionStatus !== "To review") {
            OnclickCancelPR();
        }
        else {
            // If the PR is in the Reviewal/Approval workflow or being canceld by somebody else than the requester himself,
            // ask for the reason why it is being canceled
            var dialogTexts = {
                titleRequired: "_Cancel PR",
                descriptionRequired: "_Cancel PR warning message"
            };
            ApprovalCommentCheck.OnClick("CancelPR", dialogTexts);
        }
        return false;
    };
    Controls.Cancel_remaining_items.OnClick = OnClickCancelRemainingItemsButton;
    Controls.Reject.OnClick = function () {
        Events.OnQuit();
        ApprovalCommentCheck.OnClick("Reject");
        return false;
    };
    Controls.BackToAP.OnClick = function () {
        Events.OnQuit();
        ApprovalCommentCheck.OnClick("BackToAP");
        return false;
    };
    Controls.ModifyPR.OnClick = function () {
        Events.OnQuit();
        var dialogTexts = {
            titleRequired: "_Modify PR",
            descriptionRequired: "_Modify PR warning message"
        };
        ApprovalCommentCheck.OnClick("ModifyPR", dialogTexts);
        return false;
    };
    Controls.Comment_Answer.OnClick = function () {
        Events.OnQuit();
        Variable.SetValueAsString("AdvisorLogin", User.loginId);
        Variable.SetValueAsString("AdvisorName", User.fullName);
        Variable.SetValueAsString("AdvisorEmail", User.emailAddress);
        ApprovalCommentCheck.OnClick("Comment_Answer", null);
        return false;
    };
    Controls.Save_.OnClick = SaveOnClick;
    Controls.UploadQuote2__.OnClick = function () {
        Controls.DocumentsPanel.OpenUploadDocumentPopup();
    };
    Controls.LoadFromClipboard__.OnClick = function () {
        if (Lib.Purchasing.PRLineItems.Count() >= Controls.LineItems__.GetLineCount()) {
            var Options = {
                message: Language.Translate("You can not add more than {0} items", false, Controls.LineItems__.GetLineCount()),
                status: "warning"
            };
            Popup.Snackbar(Options);
        }
        else {
            Lib.Purchasing.Clipboard.LoadClipboard.ShowDialog();
        }
    };
    Controls.DocumentsPanel.OnDocumentDeleted = function () {
        Lib.Purchasing.LayoutPR.GlobalLayout.HideDocument(true);
        Lib.Purchasing.LayoutPR.UpdateButtonBar(); // Needed since all controls are shown after the document has been deleted
    };
    return publicEvents;
})();
//#endregion
/** *********** **/
/** Form Layout **/
/** *********** **/
//#region Layout
function CheckExtractionStatus() {
    var status = Variable.GetValueAsString("Extraction_status__");
    if (status) {
        if (status !== "SUCCESS") {
            Popup.Alert(status, true, null, "_Error");
        }
        // reset status: handle the extraction status only one time
        Variable.SetValueAsString("Extraction_status__", "");
    }
}
function FixLayoutBeforeStarting() {
    Log.Info("FixLayoutBeforeStarting");
    Lib.Purchasing.LayoutPR.InitRetroCompatibility();
    Process.ShowFirstErrorAfterBoot(false);
    Lib.Purchasing.LayoutPR.GlobalLayout.Hide(true);
}
function FixLayout() {
    Log.Time("FixLayout");
    Lib.Purchasing.LayoutPR.InitBanner();
    Lib.Purchasing.LayoutPR.GlobalLayout.Hide(false);
    Lib.Purchasing.LayoutPR.InitGeneralInformationPane();
    Lib.Purchasing.ShipTo.SetReadOnly(!Lib.Purchasing.LayoutPR.IsRequesterStep());
    Lib.Purchasing.LayoutPR.InitLineItemsPane();
    Lib.Purchasing.LayoutPR.InitInventoryMovementsPane();
    Lib.Purchasing.WorkflowPR.InitWorkflowPanel();
    Lib.Purchasing.LayoutPR.InitTopPaneWarning();
    Lib.Purchasing.Punchout.PR.UpdatePane();
    /** ************************** **/
    /** ERP Specific initialization */
    /** ************************** **/
    Lib.Purchasing.Browse.Init();
    Events.InitCatalogBrowse();
    Lib.Purchasing.LayoutPR.UpdateLayout();
    // make the controls visible only once all attributes have been set
    Process.ShowFirstError();
    Log.TimeEnd("FixLayout");
}
//#endregion
var FromAncestorManager = {
    ancestorDbItems: null,
    ancestorItemsToPR: null,
    LoadAncestorData: function () {
        var srcRuid = Process.GetURLParameter("srcruid");
        if (srcRuid) {
            var processId = Process.GetURLParameter("pid");
            var originalProcess = "";
            // After clicked on "Save", the URL will not contain "pid" but "id=CD#DMIIWM000IYP.620933803918795536"
            if (!processId) {
                processId = Process.GetURLParameter("id");
                if (processId) {
                    var exp = /(?:CD#)([A-Z0-9]*)(?:\.)/;
                    var res = exp.exec(processId);
                    processId = res ? res[1] : null;
                }
            }
            if (srcRuid.indexOf(processId) !== -1) {
                originalProcess = "PR";
                FromAncestorManager.ancestorItemsToPR = Lib.Purchasing.Items.PRItemsToPR;
            }
            else {
                originalProcess = "PO";
                FromAncestorManager.ancestorItemsToPR = Lib.Purchasing.Items.POItemsToPR;
            }
            return Lib.Purchasing.Items.PrepareFillPRFromAncestor(srcRuid, originalProcess)
                .Then(function (dbItems) {
                if (dbItems.length > 0) {
                    Data.SetValue("CompanyCode__", dbItems[0].GetValue("CompanyCode__"));
                    FromAncestorManager.ancestorDbItems = dbItems;
                }
            })
                .Catch(function (reason) {
                Log.Error("Failed to load ancestor data: " + reason);
                var title = "_Items synchronization error";
                var message = "_Items synchronization error message";
                var behaviorName = "syncItemsFromActionError";
                Lib.CommonDialog.NextAlert.Define(title, message, { isError: true, behaviorName: behaviorName });
            });
        }
        return Sys.Helpers.Promise.Resolve();
    },
    $FinalizeFillFromAncestor: function () {
        var attributesToBeUpdated = ["ItemNumber__",
            "ItemDescription__",
            "ItemUnitPrice__",
            "ItemCurrency__",
            "SupplyTypeID__",
            "CostType__",
            "ItemGLAccount__",
            "ItemTaxCode__",
            "VendorNumber__",
            "Locked__",
            "LeadTime__"];
        var queryAttributes = attributesToBeUpdated.concat(["ExpirationDate__", "ValidityDate__"]);
        var QueryPossiblyUpdatedPRItemsInformation = function (queryFilter) {
            var options = {
                table: "PurchasingOrderedItems__",
                filter: queryFilter,
                attributes: queryAttributes,
                additionalOptions: {
                    bigQuery: true
                }
            };
            return Sys.GenericAPI.PromisedQuery(options);
        };
        Variable.SetValueAsString("AncestorsRuid", "");
        Lib.CommonDialog.NextAlert.Reset();
        var table = Data.GetTable("LineItems__");
        var line;
        var filter = "&(|(ItemCompanyCode__=" + Data.GetValue("CompanyCode__") + ")(ItemCompanyCode__=)(ItemCompanyCode__!=*))(|";
        for (line = 0; line < Lib.Purchasing.PRLineItems.Count(); line++) {
            var item = table.GetItem(line);
            filter += "(ItemNumber__=" + item.GetValue("ItemNumber__") + ")";
        }
        filter += ")";
        QueryPossiblyUpdatedPRItemsInformation(filter)
            .Then(function (results) {
            var _loop_1 = function (lineIndex) {
                var item = table.GetItem(lineIndex);
                var result = results && Sys.Helpers.Array.Find(results, function (r) {
                    return r.ItemNumber__ == item.GetValue("ItemNumber__");
                });
                if (result) {
                    Log.Info("Item '" + item.GetValue("ItemNumber__") + "' still in the Catalog.");
                    attributesToBeUpdated.forEach(function (attribute) {
                        Log.Info("Retrieved value for '" + attribute + "' is : " + result[attribute]);
                        if (result[attribute]) {
                            item.SetValue(attribute, result[attribute]);
                        }
                    });
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    if (new Date(result.ValidityDate__) > today || new Date(result.ExpirationDate__) < today) {
                        item.SetError("ItemDescription__", "_Item not available");
                    }
                    Lib.Purchasing.PRLineItems.RequestedDeliveryDate.Set(item);
                }
                else {
                    Log.Info("Item '" + item.GetValue("ItemNumber__") + "' deleted from the Catalog.");
                }
                Lib.Purchasing.PRLineItems.UpdateLineItem(item, lineIndex);
            };
            for (var lineIndex = 0; lineIndex < Lib.Purchasing.PRLineItems.Count(); lineIndex++) {
                _loop_1(lineIndex);
            }
            Log.Info("$FinalizeFillFromAncestor -> ComputeAmounts");
            Lib.Purchasing.PRLineItems.ComputeAmounts();
            Lib.Purchasing.PRLineItems.UpdateHasCapex();
            Lib.Purchasing.PRLineItems.LeaveEmptyLine();
        });
    },
    FillPRFromAncestor: function () {
        var replishementKey = Process.GetURLParameter("replenishementkey");
        if (FromAncestorManager.ancestorDbItems) {
            Lib.Purchasing.Items.FillPRFromAncestor(FromAncestorManager.ancestorDbItems, FromAncestorManager.ancestorItemsToPR);
            FromAncestorManager.$FinalizeFillFromAncestor();
        }
        else if (replishementKey) {
            var replenishmentData = JSON.parse(Data.StorageGetValue(replishementKey));
            var items_1 = replenishmentData.requestedItems;
            Controls.LineItems__.SetItemCount(items_1.length);
            var _loop_2 = function (i) {
                var currentItem = Controls.LineItems__.GetItem(i);
                currentItem.SetValue("ItemNumber__", items_1[i].itemNumber);
                currentItem.SetValue("WarehouseID__", replenishmentData.warehouseID);
                currentItem.SetValue("WarehouseName__", replenishmentData.warehouseName);
                currentItem.SetValue("IsReplenishmentItem__", true);
                Lib.Purchasing.PRLineItems.NumberOrDescription.UpdateNumberOrDescription(currentItem)
                    .Then(function (dbItem) {
                    dbItem.ITEMQUANTITY__ = items_1[i].requestedQuantity;
                    Lib.Purchasing.PRLineItems.NumberOrDescription.$OnSelectItem(dbItem, currentItem, i);
                    Lib.Purchasing.PRLineItems.OnAddItem(currentItem, i);
                });
            };
            for (var i = 0; i < items_1.length; i++) {
                _loop_2(i);
            }
        }
    }
};
/** ***** **/
/** START **/
/** ***** **/
function Start() {
    Log.Info("Start");
    Log.Time("Start");
    Lib.Purchasing.InitTechnicalFields();
    Lib.Purchasing.CheckPR.requiredFields = new Lib.Purchasing.CheckPR.RequiredFields(Data);
    CheckExtractionStatus();
    workflow.UpdateParallelWorkflowCurrentUser();
    workflow.Define(Lib.Purchasing.WorkflowPR.Parameters);
    var cc = Data.GetValue("CompanyCode__");
    CompanyCodeManager.SetPreviousCompanyCode(cc);
    var CCValues = Lib.P2P.CompanyCodesValue.GetValues(cc);
    if (Object.keys(CCValues).length > 0) {
        if (Sys.Helpers.IsEmpty(Data.GetValue("PurchasingOrganization__"))) {
            Data.SetValue("PurchasingOrganization__", CCValues.PurchasingOrganization__);
        }
        if (Sys.Helpers.IsEmpty(Data.GetValue("PurchasingGroup__"))) {
            Data.SetValue("PurchasingGroup__", CCValues.PurchasingGroup__);
        }
        Data.SetValue("LocalCurrency__", CCValues.Currency__);
        if (Sys.Helpers.IsEmpty(Data.GetValue("Currency__"))) {
            Data.SetValue("Currency__", CCValues.Currency__);
            Data.SetValue("ExchangeRate__", CCValues.currencies.GetRate(CCValues.Currency__));
        }
        Variable.SetValueAsString("companyCurrency", CCValues.Currency__);
    }
    Controls.LineItems__.SupplyTypeName__.SetFilter("(|(CompanyCode__=%[CompanyCode__])(CompanyCode__=)(CompanyCode__!=*))");
    if (workflow.GetTableIndex() === 0) {
        // This is a new purchase request
        // Save initiator very soon so this P.R. can appear in "My P.R." view if we juste save it
        Lib.Purchasing.LayoutPR.SetCurrentLayout(Lib.Purchasing.roleRequester);
        if (Sys.Helpers.IsEmpty(Controls.RequisitionInitiator__.GetValue())) {
            Log.Info("Setting initiator to " + User.loginId);
            Controls.RequisitionInitiator__.SetValue(User.loginId);
            Controls.RequesterName__.SetValue(User.fullName);
        }
        Lib.Purchasing.WorkflowPR.UpdateRolesSequence();
        Lib.Purchasing.PRLineItems.ComputeAmounts();
        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
    }
    else {
        if (Controls.RequisitionStatus__.GetValue().toUpperCase() === "TO COMPLETE") {
            //This purchase Requisition need to be verify
            workflow.AllowRebuild(true);
            Lib.Purchasing.WorkflowPR.UpdateRolesSequence();
        }
        else if (Lib.Purchasing.LayoutPR.IsReviewer()) {
            workflow.AllowRebuild(true);
            workflow.Rebuild([Lib.Purchasing.sequenceRoleApprover, Lib.Purchasing.sequenceRoleBuyer]);
        }
        /*
         * if the current user has no action to take in the workflow
         * and if he has been set as an advisor, reset all errors in document
         */
        if (!Lib.Purchasing.LayoutPR.IsCurrentContributor() && Lib.Purchasing.LayoutPR.IsInAdvisorList()) {
            // No field in error when advisor opens document
            Lib.Purchasing.ResetAllFieldsInError();
        }
        Lib.Purchasing.LayoutPR.SetCurrentLayout(workflow.GetContributorAt(workflow.GetContributorIndex()).role);
        if (Lib.Purchasing.LayoutPR.GetCurrentLayout() === Lib.Purchasing.roleApprover || Lib.Purchasing.LayoutPR.GetCurrentLayout() === Lib.Purchasing.roleReviewer) {
            Lib.Purchasing.PRLineItems.Budget.Fill();
        }
    }
    if (!Lib.Purchasing.LayoutPR.IsReadOnly() && Lib.Purchasing.LayoutPR.IsRequesterStep()) {
        Lib.Purchasing.PRLineItems.ComputeAmounts();
        Lib.Purchasing.PRLineItems.LeaveEmptyLine();
    }
    else if (Lib.Purchasing.LayoutPR.IsReadOnly() && Lib.Purchasing.LayoutPR.GetCurrentLayout() !== Lib.Purchasing.roleApprover && Lib.Purchasing.LayoutPR.GetCurrentLayout() !== Lib.Purchasing.roleBuyer) {
        Lib.Purchasing.LayoutPR.SetCurrentLayout(Lib.Purchasing.roleApprover);
        var nbAttach = Attach.GetNbAttach();
        Controls.DocumentsPanel.SetNumberOfNonRemovableAttachments(nbAttach);
    }
    Log.Info("Current Layout load: " + Lib.Purchasing.LayoutPR.GetCurrentLayout());
    FixLayout();
    Lib.Purchasing.CheckPR.requiredFields.CheckUpdatedItems();
    Lib.CommonDialog.NextAlert.Show({
        // special behavior for Synchronize Items error
        "syncItemsFromActionError": {
            IsShowable: function () {
                // just show the button and Popup (need to persist the button state for the updateButtonBar function)
                Controls.SynchronizeItems.Hide(Lib.Purchasing.LayoutPR.hideSynchronizeItemsButton = false);
                return true;
            }
        },
        // special behavior for Invalid Recipient
        "onInvalidRecipient": {
            IsShowable: function () {
                // just show the button and Popup (need to persist the button state for the updateButtonBar function)
                Controls.SynchronizeItems.Hide(Lib.Purchasing.LayoutPR.hideSynchronizeItemsButton = false);
                // Eventually show recipient column...
                return true;
            }
        },
        // special behavior for Unexpected error
        "onUnexpectedError": {
            IsShowable: function () {
                // In PR workflow we just advise user to check logs and retry last action.
                // In post PR workflow we show the "SynchronizeItems" button (we change label) and advise user to check logs and retry
                // just show the button and Popup (need to persist the button state for the updateButtonBar function)
                var status = Data.GetValue("RequisitionStatus__");
                var inPRWorkflow = Lib.Purchasing.PRStatus.ForPRWorkflow.indexOf(status) !== -1;
                if (!inPRWorkflow) {
                    Controls.SynchronizeItems.SetText(Language.Translate("_Retry"));
                    Controls.SynchronizeItems.Hide(Lib.Purchasing.LayoutPR.hideSynchronizeItemsButton = false);
                }
                return true;
            }
        },
        "ClearSequence": {
            IsShowable: function () {
                Data.SetValue("RequisitionNumber__", "");
                return true;
            }
        }
    });
    fieldReseter.push({ "table": "LineItems__", "field": "ItemBudgetInitial__" });
    Lib.Purchasing.Punchout.PR.Init();
    PopulateInventoryMovementsTable();
    Lib.Purchasing.LayoutPR.GlobalLayout.HideWaitScreen(false);
    // Avoid refilling items on Save
    if (Lib.Purchasing.PRLineItems.Count() == 0) {
        Log.Info("FillPRFromAncestor");
        FromAncestorManager.FillPRFromAncestor();
    }
    Lib.Purchasing.LayoutPR.GlobalLayout.HideWaitScreen(true);
    var func = Sys.Helpers.TryGetFunction("Lib.PR.Customization.Client.CustomizeLayout");
    if (func) {
        func();
    }
    else {
        Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.CustomiseLayout");
    }
    if (Controls.RequisitionStatus__.GetValue().toUpperCase() === "TO COMPLETE") {
        Process.SetHelpId(5007);
    }
    Log.TimeEnd("Start");
}
function PopulateInventoryMovementsTable() {
    Sys.Parameters.GetInstance("P2P").IsReady(function () {
        if (Lib.P2P.Inventory.IsEnabled()) {
            Lib.P2P.Inventory.InventoryMovement.QueryMovementsForRuidex(Data.GetValue("RUIDEX"))
                .Then(function (inventoryMovements) {
                if (inventoryMovements.length > 0) {
                    AddInventoryMovementsEntriesToTable(inventoryMovements);
                    Lib.Purchasing.LayoutPR.UpdateInventoryMovementsPane();
                }
            })
                .Catch(function (err) {
                Log.Error("Error while filling Inventory Movements table: " + JSON.stringify(err));
            });
        }
        else {
            Lib.Purchasing.LayoutPR.UpdateInventoryMovementsPane();
        }
    });
}
function AddInventoryMovementsEntriesToTable(inventoryMovements) {
    Controls.InventoryMovements__.SetItemCount(0);
    inventoryMovements.forEach(function (inventoryMovement) {
        var inventoryMovementItem = Controls.InventoryMovements__.AddItem(true);
        inventoryMovementItem.SetValue("ItemNumber__", inventoryMovement.itemNumber);
        inventoryMovementItem.SetValue("ItemName__", inventoryMovement.itemName);
        inventoryMovementItem.SetValue("WarehouseName__", inventoryMovement.warehouseName);
        inventoryMovementItem.SetValue("ItemQuantity__", Math.abs(inventoryMovement.movementValue));
        inventoryMovementItem.SetValue("InventoryMovementDateTime__", inventoryMovement.movementDateTime);
    });
    Controls.InventoryMovements__.SetItemCount(inventoryMovements.length);
}
/** *************** **/
/** BEFORE STARTING **/
/** *************** **/
Sys.Helpers.EnableSmartSilentChange();
// START - ignore all changes on form during the initialization processing
ProcessInstance.SetSilentChange(true);
function LoadUserProperties(token) {
    Log.Info("LoadUserProperties");
    Lib.P2P.UserProperties.QueryValues(User.loginId)
        .Then(function (UserPropertiesValues) {
        var companyCode = Data.GetValue("CompanyCode__") || UserPropertiesValues.CompanyCode__;
        var allowedCompanyCodes = UserPropertiesValues.GetAllowedCompanyCodes() || UserPropertiesValues.CompanyCode__;
        if (allowedCompanyCodes.indexOf(companyCode) < 0) {
            allowedCompanyCodes = allowedCompanyCodes + "\n" + companyCode;
        }
        var LdapFilter;
        var ldapUtil = Sys.Helpers.LdapUtil;
        Sys.Helpers.Array.ForEach(allowedCompanyCodes.split("\n"), function (cc) {
            if (!LdapFilter) {
                LdapFilter = ldapUtil.FilterEqual("CompanyCode__", cc);
            }
            else {
                LdapFilter = ldapUtil.FilterOr(LdapFilter, ldapUtil.FilterEqual("CompanyCode__", cc));
            }
        });
        Controls.CompanyCode__.SetFilter(LdapFilter.toString());
        Data.SetValue("CompanyCode__", companyCode);
        var count = 2;
        function resolve() {
            if (--count == 0) {
                token.Use();
            }
        }
        Lib.Purchasing.CatalogHelper.SupplyTypesManager.Query();
        Lib.P2P.CompanyCodesValue.QueryValues(companyCode, false)
            .Then(function (CCValues) {
            if (Object.keys(CCValues).length <= 0) {
                Data.SetError("CompanyCode__", "_This CompanyCode does not exist in the table.");
                resolve(); //use one resolve because we won't be able to query the currencies rate
            }
            else {
                Data.SetValue("LocalCurrency__", CCValues.Currency__);
                CCValues.currencies.QueryRate().Then(resolve);
            }
            Lib.Purchasing.SetERPByCompanyCode(Data.GetValue("CompanyCode__"))
                .Then(function () {
                if (Lib.Purchasing.ShipTo.IsEmpty() && !Lib.Purchasing.ShipTo.IsSetByUser()) {
                    Lib.Purchasing.ShipTo.FillFromCompanyCode().Then(resolve);
                }
                else {
                    resolve();
                }
            });
        });
    });
}
var syncBeforeStarting = Sys.Helpers.Synchronizer.Create(function () {
    Start();
    // END - ignore all changes on form during the initialization processing
    setTimeout(function () {
        ProcessInstance.SetSilentChange(false);
    }, 1000);
}, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
if (!Lib.Purchasing.LayoutPR.IsReadOnly()) {
    // LoadUserProperties should be done after LoadAncestorData
    syncBeforeStarting.Register(function (token) {
        return FromAncestorManager.LoadAncestorData()
            .Then(function () { return LoadUserProperties(token); });
    });
}
var onLoadPromise = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Client.OnLoad");
if (onLoadPromise) {
    syncBeforeStarting.Register(onLoadPromise);
}
FixLayoutBeforeStarting();
Lib.Purchasing.LoadParameters().Then(function () {
    syncBeforeStarting.Start();
});
Log.TimeEnd("CustomScript");
