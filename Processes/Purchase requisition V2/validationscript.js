///#GLOBALS Lib Sys
var validationscript;
(function (validationscript) {
    var currentName = Data.GetActionName();
    var currentAction = Data.GetActionType();
    Log.Info("-- PR Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice() + "'");
    var workflow = Sys.WorkflowController.Create({ version: 2 });
    var requiredFields = new Lib.Purchasing.CheckPR.RequiredFields(Data);
    var g_newComment = Data.GetValue("Comments__");
    var g_sequenceStep = workflow.GetContributorIndex();
    var g_currentContributor = workflow.GetContributorAt(g_sequenceStep);
    var g_nextContributor = workflow.GetContributorAt(g_sequenceStep + 1);
    /** *************** **/
    /** CONTROL HELPERS **/
    /** *************** **/
    function IsFormInError() {
        var formInError;
        requiredFields.CheckUpdatedItems(true);
        requiredFields.CheckAll(g_currentContributor ? g_currentContributor.role : "")
            .Then(function (isValid) {
            if (!isValid) {
                Log.Info("Form in error.");
                // Check if it is the recipient field which is in error in order to handle it specifically (as it hidden)
                var recipientsInError_1 = [];
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                    if (!Sys.Helpers.IsEmpty(line.GetError("RecipientName__"))) {
                        recipientsInError_1.push(line.GetValue("RecipientLogin__"));
                        Log.Info("Invalid recipient: " + line.GetValue("RecipientLogin__"));
                    }
                });
                if (recipientsInError_1.length > 0) {
                    Lib.CommonDialog.NextAlert.Define("_Recipient error", "_Recipient {0} is invalid", { isError: true, behaviorName: "onInvalidRecipient" }, recipientsInError_1.join(", "));
                    SendEmailNotification(g_currentContributor, "_A purchase requisition is in error", "Purchasing_Email_NotifPRError.htm");
                }
                formInError = true;
            }
            formInError = false;
        })
            .Catch(function (error) {
            Log.Error("Unhandled promise: " + error + ". Prevent posting.");
        });
        return formInError;
    }
    function SendEmailNotifications(contributors, subject, template, backupUserAsCC, customTagsByContributor) {
        Sys.Helpers.Array.ForEach(contributors, function (contributor) {
            if (!contributor.additionalProperties || !contributor.additionalProperties.emailNotifSent) {
                var customTags = !!customTagsByContributor && (contributor.login in customTagsByContributor) ? customTagsByContributor[contributor.login] : null;
                SendEmailNotification(contributor, subject, template, backupUserAsCC, customTags);
                workflow.UpdateAdditionalContributorData(contributor.contributorId, { emailNotifSent: true });
            }
        });
    }
    function SendEmailNotification(contributor, subject, template, backupUserAsCC, customTags) {
        //if you change this structure, plz update the sample in lib_PR_Customization_Server
        var options = {
            userId: contributor.login,
            subject: subject,
            template: template,
            fromName: "Esker Purchase requisition",
            backupUserAsCC: !!backupUserAsCC,
            sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1",
            customTags: Sys.Helpers.Extend({ "DestinationFullName": contributor.name }, customTags)
        };
        var doSendNotif = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnSendEmailNotification", options);
        if (doSendNotif !== false) {
            Sys.EmailNotification.SendEmailNotification(options);
        }
    }
    function SetVendorNameStyle() {
        if (Sys.Helpers.IsEmpty(Variable.GetValueAsString("lastExtractedVendorName"))) {
            Variable.SetValueAsString("Vendor_name_START__", "<!--");
            Variable.SetValueAsString("Vendor_name_END__", "-->");
        }
        else {
            Variable.SetValueAsString("Vendor_name_START__", "");
            Variable.SetValueAsString("Vendor_name_END__", "");
        }
    }
    function GiveRightToRecipients(right) {
        Log.Info("GiveRightToRecipients");
        var ownerLogin = Lib.P2P.GetOwner().GetValue("login");
        // Get all unique recipients
        var allRecipientLogins = [];
        Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
            allRecipientLogins.push(line.GetValue("RecipientLogin__"));
        });
        allRecipientLogins = Sys.Helpers.Array.GetDistinctArray(allRecipientLogins);
        Sys.Helpers.Array.ForEach(allRecipientLogins, function (recipientLogin) {
            // Give right to recipient if different from buyer/treasurer
            if (ownerLogin !== recipientLogin) {
                Log.Info("Grant " + right + " right on purchase request to recipient: " + recipientLogin);
                Process.AddRight(recipientLogin, right);
            }
        });
    }
    function GiveRightToEverybody() {
        Log.Info("Reset rights");
        Process.ResetRights();
        var lastSaveOwnerID = Data.GetValue("LastSavedOwnerID");
        Log.Info("Grant read right to LastSavedOwnerID: " + lastSaveOwnerID);
        Process.AddRight(lastSaveOwnerID, "read");
        // always give the "read" right to the initiator
        if (workflow.GetNbContributors() > 0) {
            Log.Info("Grant write right to initiator: " + workflow.GetContributorAt(0).login);
            Process.AddRight(workflow.GetContributorAt(0).login, "write");
        }
        // Skips initiator : starts at 1
        for (var i = 1; i < workflow.GetNbContributors(); i++) {
            var step = workflow.GetContributorAt(i);
            Log.Info("Grant read right to approver: " + step.login);
            Process.AddRight(step.login, "read");
        }
        // Add right for supervisor
        Lib.Purchasing.SetRightForP2PSupervisor();
        Log.Info("Grant read right to top managers");
        Lib.Purchasing.PRValidation.GiveReadRightToTopManagers(workflow, parameters);
        GiveRightToRecipients("read");
    }
    function Forward() {
        var idx = workflow.GetContributorIndex();
        Log.Info("forward_callback idx :" + idx);
        var step = workflow.GetContributorAt(idx);
        // Need to give rights
        //   - to everybody on requester first step
        //   - to approvers on reviewer step because the workflow rebuilt due to CostType, GLAccount, CostCenter or TaxCode changes
        //   - to manually added reviewers/approvers
        // For the moment, no way to know the workflow changes, so, give rights in all cases
        GiveRightToEverybody();
        Process.Forward(step.login);
        Process.LeaveForm();
    }
    function ChangeOwner() {
        var idx = workflow.GetContributorIndex();
        Log.Info("change owner idx :" + idx);
        var step = workflow.GetContributorAt(idx);
        if (idx === 1) // Give rights only on first step
         {
            GiveRightToEverybody();
        }
        Process.ChangeOwner(step.login);
    }
    function ResetAdvisorList() {
        var list = Variable.GetValueAsString("AdvisorLoginList");
        if (list) {
            var logins = list.split("\n");
            for (var i = 0; i < logins.length; i++) {
                Log.Info("Grant read right to advisor: " + logins[i]);
                Process.SetRight(logins[i], "read");
            }
        }
        Variable.SetValueAsString("AdvisorLoginList", "");
    }
    function AddSharedWith(comment) {
        var prefix = Language.Translate("_Shared with", false, Variable.GetValueAsString("AdvisorName"));
        if (comment) {
            return prefix + ": " + comment;
        }
        return prefix;
    }
    var validationDate = null;
    function GetContributionData(sequenceStep, newAction, newComment, commentPrefixes, ignorePrevComment) {
        var currentContributor = workflow.GetContributorAt(sequenceStep);
        currentContributor.action = newAction;
        if (validationDate === null) {
            currentContributor.date = new Date();
            validationDate = currentContributor.date;
        }
        else {
            currentContributor.date = validationDate;
        }
        currentContributor.actualApprover = Sys.Helpers.String.ExtractLoginFromDN(Lib.P2P.GetValidatorOrOwnerLogin());
        if (commentPrefixes) {
            if (commentPrefixes.sharedWith) {
                newComment = AddSharedWith(newComment);
            }
            if (commentPrefixes.onBehalfOf) {
                newComment = Lib.P2P.AddOnBehalfOf(currentContributor, newComment);
            }
        }
        var previousComment = "";
        if (!ignorePrevComment) {
            if (currentContributor.comment && currentContributor.comment.length > 0) {
                previousComment = currentContributor.comment;
            }
        }
        var newCommentFormatted = !!newComment ? newComment : "";
        if (!!newComment && !!previousComment) {
            newCommentFormatted += "\n";
        }
        newCommentFormatted += previousComment;
        Data.SetValue("Comments__", "");
        currentContributor.comment = newCommentFormatted;
        return currentContributor;
    }
    function SetStatusAccordingToRole(role) {
        // intermediate state when all approvers approved
        if (role === Lib.Purchasing.roleBuyer) {
            Data.SetValue("RequisitionStatus__", "To order");
        }
        else if (role === Lib.Purchasing.roleReviewer) {
            Data.SetValue("RequisitionStatus__", "To review");
        }
        else {
            Data.SetValue("RequisitionStatus__", "To approve");
        }
    }
    function GoToNextStep(contributionData, forward, sendDefaultEmail) {
        var currentContributor = workflow.GetContributorAt(workflow.GetContributorIndex());
        workflow.NextContributor(contributionData);
        if (forward !== false) {
            Forward();
        }
        else {
            ChangeOwner();
        }
        var nextContributor = workflow.GetContributorAt(workflow.GetContributorIndex());
        var nextContributors = workflow.GetParallelContributorsOf(nextContributor, true);
        nextContributors.forEach(function (contributor) {
            Log.Info("Grant validate right to approver: " + contributor.login);
            Process.AddRight(contributor.login, "validate");
            if (!contributor.additionalProperties || !contributor.additionalProperties.startingDate) {
                workflow.UpdateAdditionalContributorData(contributor.contributorId, { startingDate: new Date(contributionData.date) });
            }
        });
        // intermediate state when all approvers approved
        if (nextContributor.role === Lib.Purchasing.roleBuyer) {
            if (!Lib.Purchasing.IsAutoCreateOrderEnabled()) {
                if (nextContributors.length > 0 && nextContributors[0].contributorId === nextContributor.contributorId) // check we are in the start of parallel step to avoid to send notif at each parallel approval
                 {
                    var customTagsByBuyers = GetCustomTagsByBuyer();
                    SendEmailNotifications(nextContributors, "_A purchase requisition is waiting for your order creation", "Purchasing_Email_NotifBuyer.htm", true, customTagsByBuyers);
                }
            }
            Data.SetValue("ApprovedDate__", new Date());
            ResetAdvisorList();
            var initiator = workflow.GetContributorAt(0);
            var approverIsRequester = currentContributor.login === initiator.login;
            var buyerIsRequester = nextContributor.login === initiator.login;
            // Don't send "Your purchase requisition has been approved" notification if the approver is the requester
            // And don't send it either if the buyer is the requester as he will receive "A purchase requisition is waiting for your order creation"
            if (!approverIsRequester && !buyerIsRequester) {
                SendEmailNotification(initiator, "_A purchase requisition has been approved", "Purchasing_Email_PRValidatedNotifRequester.htm");
            }
        }
        else if (sendDefaultEmail !== false) {
            var templateType_1 = "NotifNextApprover";
            if (nextContributor.role === Lib.Purchasing.roleReviewer) {
                templateType_1 = "NotifNextReviewer";
                SendEmailNotifications(nextContributors, "_A purchase requisition is waiting for your review", "Purchasing_Email_NotifNextReviewer.htm", true);
            }
            else {
                SendEmailNotifications(nextContributors, "_A purchase requisition is waiting for your action", "Purchasing_Email_NotifNextApprover.htm", true);
            }
            // Send push notifications if enabled
            if (Process.GetProcessDefinition().PushNotification === true) {
                var pushNotificationType_1 = (Sys.Parameters.GetInstance("PAC").GetParameter("PushNotificationType") || Sys.Parameters.GetInstance("P2P").GetParameter("PushNotificationType") || "").toLowerCase();
                Log.Info("Sending " + pushNotificationType_1 + " push notification");
                if (pushNotificationType_1 == "short" || pushNotificationType_1 == "full") {
                    nextContributors.forEach(function (contributor) {
                        if (!contributor.additionalProperties || !contributor.additionalProperties.pushNotifSent) {
                            Sys.PushNotification.SendNotifToUser({
                                user: Users.GetUser(contributor.login),
                                id: pushNotificationType_1 == "short" ? Process.GetProcessID() : Data.GetValue("ruidex"),
                                template: "Purchasing_PushNotif_" + templateType_1 + "_" + pushNotificationType_1 + ".txt",
                                sendToBackupUser: true
                            });
                            workflow.UpdateAdditionalContributorData(contributor.contributorId, { pushNotifSent: true });
                        }
                    });
                }
            }
        }
        return nextContributor;
    }
    function NotifyEndOfContributionOnMobile(contributor) {
        // On mobile, only approvers can approve the purchase requisition
        if (contributor.role === Lib.Purchasing.roleApprover) {
            // Send push notifications if enabled
            if (Process.GetProcessDefinition().PushNotification === true) {
                var user = Users.GetUser(contributor.login);
                if (user) {
                    Log.Info("Notifying the end of approval contribution step on mobile. A silent push notif will be sent to: " + contributor.login + " (and backup user if needed)");
                    Sys.PushNotification.SendNotifToUser({
                        user: user,
                        id: Data.GetValue("ruidex"),
                        sendToBackupUser: true,
                        silent: true
                    });
                }
            }
        }
    }
    function AutoCreateOrderIfNeeded() {
        var bok = true;
        if (Lib.Purchasing.IsAutoCreateOrderEnabled()) {
            if (!Lib.Purchasing.IsMonoBuyer(Data.GetTable("LineItems__"))) {
                Log.Warn("Auto create order is disabled because the buyer is not the same on each lines...");
            }
            if (Lib.Purchasing.IsTotalyTakenFromStock(Data.GetTable("LineItems__"))) {
                Log.Warn("Auto create order is disabled because all requested item have been taken from stock...");
            }
            else {
                bok = Lib.Purchasing.PRValidation.AutoCreateOrder();
            }
        }
        return bok;
    }
    function TakeFromStockIfPossible() {
        var bok = true;
        if (Lib.P2P.Inventory.IsEnabled()) {
            var ruidex_1 = Data.GetValue("RUIDEX");
            var companyCode_1 = Data.GetValue("CompanyCode__");
            var stockImpacted_1 = false;
            var inventoryFilter_1 = [];
            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                inventoryFilter_1.push({
                    companyCode: companyCode_1,
                    itemNumber: item.GetValue("ItemNumber__"),
                    warehouseID: item.GetValue("WarehouseID__")
                });
            });
            var alreadyTakenFromStockItems_1 = {};
            Lib.P2P.Inventory.InventoryMovement.QueryMovementsForRuidex(Data.GetValue("RUIDEX"))
                .Then(function (inventoryMovements) {
                inventoryMovements.forEach(function (inventoryMovement) {
                    alreadyTakenFromStockItems_1[inventoryMovement.lineNumber] = inventoryMovement;
                });
            })
                .Catch(function (err) {
                Log.Error("Error while filling Inventory Movements table: " + JSON.stringify(err));
            });
            Lib.P2P.Inventory.GetInventories(inventoryFilter_1, false, true)
                .Then(function (inventories) {
                var itemsInventory = {};
                inventories.forEach(function (inventory) {
                    itemsInventory[inventory.itemNumber + "###" + inventory.warehouseID] = inventory;
                });
                Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                    var warehouseID = item.GetValue("WarehouseID__");
                    var itemNumber = item.GetValue("ItemNumber__");
                    var inventory = itemsInventory[itemNumber + "###" + warehouseID];
                    var lineNumber = item.GetValue("LineItemNumber__");
                    if (inventory && !item.GetValue("IsReplenishmentItem__")) {
                        var quantityTakenFromStock = 0;
                        if (alreadyTakenFromStockItems_1[lineNumber]) {
                            quantityTakenFromStock = alreadyTakenFromStockItems_1[lineNumber].movementValue;
                        }
                        else {
                            quantityTakenFromStock = item.GetValue("ItemQuantity__") > inventory.stock ? inventory.stock : item.GetValue("ItemQuantity__");
                            inventory.stock -= quantityTakenFromStock;
                        }
                        item.SetValue("QuantityTakenFromStock__", quantityTakenFromStock);
                        item.SetValue("AmountTakenFromStock__", new Sys.Decimal(quantityTakenFromStock).mul(item.GetValue("ItemUnitPrice__")).toNumber());
                        var inventoryMovement = new Lib.P2P.Inventory.InventoryMovement();
                        inventoryMovement.companyCode = companyCode_1;
                        inventoryMovement.itemNumber = itemNumber;
                        inventoryMovement.lineNumber = lineNumber;
                        inventoryMovement.movementOrigin = ruidex_1;
                        inventoryMovement.movementType = "Withdraw";
                        inventoryMovement.movementValue = new Sys.Decimal(quantityTakenFromStock).mul(-1).toNumber();
                        inventoryMovement.unitOfMeasure = item.GetValue("ItemUnit__");
                        inventoryMovement.warehouseID = warehouseID;
                        bok = Lib.P2P.Inventory.CreateOrUpdateInventoryMovement(inventoryMovement) && bok;
                        stockImpacted_1 = true;
                    }
                });
                if (stockImpacted_1) {
                    Lib.P2P.Inventory.UpdateInventoryStock(inventories);
                }
            });
        }
        return bok;
    }
    function OnApproved(lastApprover) {
        var bok = true;
        if (lastApprover) {
            //check the budget with the last approver
            bok = Lib.Purchasing.PRBudget.AsToApprove();
        }
        bok = bok && Lib.Purchasing.PRBudget.AsCommitted();
        return bok;
    }
    function GoToNextStepIfApproved(contributionData) {
        GoToNextStep(contributionData, false, false);
        // Give the "write" right to all buyers to allow "Cancel remaining items" via ResumeWithActionAsync can set correct LastSavedOwnerID
        for (var i = workflow.GetContributorIndex(); i < workflow.GetNbContributors(); i++) {
            var step = workflow.GetContributorAt(i);
            Log.Info("Grant validate right to buyer: " + step.login);
            Process.AddRight(step.login, "validate");
        }
        var apClerkLogin = Lib.P2P.ResolveDemoLogin(Sys.Parameters.GetInstance("PAC").GetParameter("APClerkLogin"));
        if (apClerkLogin) {
            Log.Info("Grant read right to AP Clerk: " + apClerkLogin);
            Process.SetRight(apClerkLogin, "read");
        }
        var treasurerLogin = Lib.P2P.ResolveDemoLogin(Sys.Parameters.GetInstance("PAC").GetParameter("TreasurerLogin"));
        if (treasurerLogin) {
            Log.Info("Grant read right to treasurer: " + treasurerLogin);
            Process.SetRight(treasurerLogin, "read");
        }
        Process.WaitForUpdate();
        Process.LeaveForm();
    }
    function GetCanceledItems(itemsNumbers) {
        var canceledItems = [];
        Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
            if (itemsNumbers.indexOf(line.GetValue("LineItemNumber__")) >= 0) {
                var requestedQuantity = new Sys.Decimal(line.GetValue("ItemQuantity__"));
                var orderedQuantity = new Sys.Decimal(line.GetValue("ItemOrderedQuantity__") || 0);
                var alreadyCanceledQuantity = new Sys.Decimal(line.GetValue("CanceledQuantity__") || 0);
                var openQuantity = requestedQuantity.minus(alreadyCanceledQuantity).minus(orderedQuantity);
                // Still cancelable ? Not ordered at all...
                if (orderedQuantity.equals(0) || (Sys.Parameters.GetInstance("PAC").GetParameter("AllowSplitPRIntoMultiplePO", false) && openQuantity.greaterThan(0))) {
                    Log.Info("Canceling item [" + line.GetValue("LineItemNumber__") + " - " + line.GetValue("ItemDescription__") + "]");
                    canceledItems.push(line);
                }
                else {
                    Log.Warn("Can't cancel the already ordered item [" + line.GetValue("LineItemNumber__") + " - " + line.GetValue("ItemDescription__") + "]");
                }
            }
        });
        return canceledItems;
    }
    /**
     * Browse items, get global status for buyer items, and stash some for each buyer.
     *
     * @param itemsNumbersToStash optional list of item number to stash. When an item is stashed
     * the item is pushed to the stashedItems array and the status of the stashed items is not
     * merged with the global returned status.
     *
     * @param all optional true to take all items; not set or false to take only items specified in itemsNumbersToStash
     */
    function GetItemStatusByBuyer(itemsNumbersToStash, all) {
        if (itemsNumbersToStash === void 0) { itemsNumbersToStash = null; }
        if (all === void 0) { all = false; }
        var itemStatusByBuyer = {};
        Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
            var itemBuyer = line.GetValue("BuyerLogin__");
            var itemStatus = itemStatusByBuyer[itemBuyer];
            if (!itemStatus) {
                itemStatusByBuyer[itemBuyer] = itemStatus = {
                    hasToOrderItems: false,
                    hasOrderedItems: false,
                    stashedItems: []
                };
            }
            if (all || (itemsNumbersToStash && itemsNumbersToStash.indexOf(line.GetValue("LineItemNumber__")) >= 0)) {
                itemStatus.stashedItems.push(line);
            }
            if (line.GetValue("ItemStatus__") !== "Canceled") {
                var orderedQuantity = line.GetValue("ItemOrderedQuantity__");
                var requestedQuantity = line.GetValue("ItemQuantity__");
                var canceledQuantity = line.GetValue("CanceledQuantity__");
                if (line.GetValue("ItemOrderedQuantity__") !== 0) {
                    itemStatus.hasOrderedItems = true;
                }
                if (new Sys.Decimal(requestedQuantity || 0).minus(orderedQuantity || 0).minus(canceledQuantity || 0).greaterThan(0)) {
                    itemStatus.hasToOrderItems = true;
                }
            }
        });
        return itemStatusByBuyer;
    }
    function BuildCancellationDescription(canceledItems, fullDesc, firstItemsCount) {
        if (fullDesc === void 0) { fullDesc = false; }
        if (firstItemsCount === void 0) { firstItemsCount = 1; }
        var items = [];
        for (var i = 0; i < canceledItems.length && (fullDesc || i < firstItemsCount); i++) {
            var item = canceledItems[i];
            var ItemCanceledDescription = void 0;
            if (Sys.Parameters.GetInstance("PAC").GetParameter("AllowSplitPRIntoMultiplePO", false)) {
                var culture = Lib.P2P.GetValidatorOrOwner().GetValue("CULTURE");
                var itemCurrency = item.GetValue("ItemCurrency__");
                var canceledQuantity = item.GetValue("CanceledQuantity__");
                var itemQuantity = item.GetValue("ItemQuantity__");
                if (Lib.Purchasing.Items.IsAmountBasedItem(item)) {
                    canceledQuantity = canceledQuantity.toLocaleString(culture, { currency: itemCurrency, style: "currency" });
                    itemQuantity = itemQuantity.toLocaleString(culture, { currency: itemCurrency, style: "currency" });
                }
                var itemDescription = item.GetValue("ItemDescription__").replace(/,/g, " ");
                ItemCanceledDescription = "- " + Language.Translate("_{0} out of {1} requested ({2})", false, canceledQuantity, itemQuantity, itemDescription);
            }
            else {
                ItemCanceledDescription = item.GetValue("ItemDescription__").replace(/,/g, " ");
            }
            items.push(ItemCanceledDescription);
        }
        var itemsDesc = items.join("\n");
        if (!fullDesc && firstItemsCount < canceledItems.length) {
            itemsDesc += "...";
        }
        return "\n" + itemsDesc;
    }
    function CancelItems(itemsNumbers, actionName, cancelerUser, contributionDate, newComment) {
        var bok = true;
        if (itemsNumbers === null || !Sys.Helpers.IsArray(itemsNumbers)) {
            Log.Warn("Invalid items numbers parameter: an array is expected (" + itemsNumbers + ")");
            return false;
        }
        // Selecting the canceled items. This action can be done asynchronously. So we check before canceling
        // if the items can still be canceled.
        var canceledItems = GetCanceledItems(itemsNumbers);
        var canceledItemsNumbers = [];
        // Nothing canceled
        if (canceledItems.length === 0) {
            Process.WaitForUpdate();
            return true;
        }
        // Set as canceled the item status...
        canceledItems.forEach(function (item) {
            canceledItemsNumbers.push(item.GetValue("LineItemNumber__"));
            item.SetValue("CanceledQuantity__", new Sys.Decimal(item.GetValue("ItemQuantity__") || 0).minus(item.GetValue("ItemOrderedQuantity__") || 0).minus(item.GetValue("QuantityTakenFromStock__") || 0).toNumber());
            item.SetValue("CanceledAmount__", new Sys.Decimal(item.GetValue("ItemNetAmount__") || 0).minus(item.GetValue("ItemOrderedAmount__") || 0).minus(item.GetValue("AmountTakenFromStock__") || 0).toNumber());
        });
        // Retrieving item status for each buyer step.
        var itemStatusByBuyer = GetItemStatusByBuyer(canceledItemsNumbers);
        bok = Lib.Purchasing.PRBudget.AsCanceledItemFromCommitted();
        bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
            return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
        });
        if (bok) {
            // Send notification: never notify the requester when the canceler user is the requester
            var requester = workflow.GetContributorAt(0);
            var cancelerUserIsRequester = requester.login === cancelerUser.GetValue("Login");
            if (!cancelerUserIsRequester) {
                var customTags = {
                    CanceledItems: BuildCancellationDescription(canceledItems)
                };
                SendEmailNotification(requester, actionName === "remainingItemsCanceled" ? "_The remaining items have been canceled" : "_Items canceled", "Purchasing_Email_NotifPRItemCanceled.htm", false, customTags);
            }
            var status = Data.GetValue("RequisitionStatus__");
            // Update workflow for each buyer step
            Sys.Helpers.Object.ForEach(itemStatusByBuyer, function (itemStatus, buyer) {
                if (itemStatus.stashedItems.length > 0) {
                    var buyerUser = Users.GetUser(buyer);
                    if (buyerUser) {
                        Log.Info("Cancelling items for buyer [" + buyer + "]");
                        workflow.UpdateParallelWorkflowCurrentUser(buyerUser);
                        ChangeOwner(); // Need to change owner in order to be coherent with the current contributor
                        var sequenceStep = workflow.GetContributorIndex();
                        var currentContributor = workflow.GetContributorAt(sequenceStep);
                        if (currentContributor.login === buyer) {
                            var isLastStep = sequenceStep + 1 >= workflow.GetNbContributors();
                            var contributionData = void 0;
                            var cancelerContributor = Sys.Helpers.Extend({}, currentContributor);
                            cancelerContributor.action = actionName;
                            var canceledItemsFullDesc = BuildCancellationDescription(itemStatus.stashedItems, true);
                            var cancellationComment = Language.Translate("_The following items have been canceled {0}", false, canceledItemsFullDesc) + "\n" + newComment;
                            var cancellationSequenceStep = sequenceStep;
                            // Not all items have been cancelled (items have either been ordered or are still open),
                            // so add a new step for further operations (order or another cancel)
                            if (itemStatus.hasOrderedItems || itemStatus.hasToOrderItems) {
                                Log.Info("Adding a new step for the cancellation");
                                // Add cancel step (we must do it before calling NextContributor)
                                workflow.AddContributorAt(cancellationSequenceStep, cancelerContributor);
                                if (!itemStatus.hasToOrderItems) {
                                    Log.Info("All items of this buyer have either been ordered or cancelled, marking buyer step as done");
                                    contributionData = GetContributionData(cancellationSequenceStep, parameters.actions.orderCreated.GetName(), "", { onBehalfOf: true });
                                    contributionData.date = contributionDate;
                                    workflow.NextContributor(contributionData);
                                    ChangeOwner(); // Need to change owner in order to be consistent with the current contributor
                                }
                                contributionData = GetContributionData(cancellationSequenceStep, actionName, cancellationComment, { onBehalfOf: true }, true);
                                contributionData.date = contributionDate;
                            }
                            else {
                                // All items of this buyer have been cancelled
                                Log.Info("All items of this buyer have been cancelled, marking buyer step as done");
                                contributionData = GetContributionData(cancellationSequenceStep, actionName, cancellationComment, { onBehalfOf: true });
                                contributionData.date = contributionDate;
                            }
                            if (!isLastStep || itemStatus.hasToOrderItems) {
                                // There are other buyers waiting for ordering or items still to order, go to next step
                                workflow.NextContributor(contributionData);
                                ChangeOwner(); // Need to change owner in order to be consistent with the current contributor
                            }
                            else {
                                // End of workflow
                                workflow.EndWorkflow(contributionData);
                            }
                        }
                    }
                    else {
                        Log.Error("Unable to finalize the cancellation by buyer " + buyer + "' because this user does not exist");
                        bok = false;
                    }
                }
            });
            if (bok) {
                // All items have been canceled
                if (status === "Canceled") {
                    Data.SetValue("State", 300);
                }
                else if (status !== "Received") {
                    // Some items have not yet been received, wait for them
                    Process.WaitForUpdate();
                }
            }
        }
        return bok;
    }
    function OnCancelPO() {
        var itemBeforeSynch = GetItemStatusByBuyer();
        var bok = Lib.Purchasing.PRValidation.SynchronizeItems({ fromAction: true });
        if (bok) {
            //One of the purchase Order has been Canceled, at least one item returns to "To Order" status
            //We need to reOpen the Workflow in order to do the Order or Cancel Items action afterward
            Log.Info("Restarting the workflow following cancel order");
            //When the PO canceled was autocreated, we need to autorize the buyer to create a new the Purchase Order
            Variable.SetValueAsString("AutoCreateOrderEnabled", "");
            // pour chaque buyer get item status if avant toorder = false et ordered = true et après synch qui passe toorder
            // à true et false faut ouvrir
            var itemAfterSynch = GetItemStatusByBuyer();
            var date_1 = new Date();
            Sys.Helpers.Object.ForEach(itemAfterSynch, function (itemStatus, buyer) {
                // If no more items to order before the SynchronizeItems and after an item can be ordered, so open the PO
                if (itemBeforeSynch[buyer].hasToOrderItems === false && itemBeforeSynch[buyer].hasOrderedItems === true && itemStatus.hasToOrderItems === true) {
                    var currentSequenceStep = workflow.GetContributorIndex();
                    var contributionData = workflow.GetContributorAt(currentSequenceStep);
                    if (workflow.IsEnded()) {
                        workflow.Reopen();
                        currentSequenceStep = workflow.GetContributorIndex();
                    }
                    var buyerUser = Users.GetUser(buyer);
                    var buyerContributor = {
                        //mandatory fields
                        contributorId: buyer + Lib.Purchasing.roleBuyer,
                        role: Lib.Purchasing.roleBuyer,
                        //not mandatory fields
                        login: buyer,
                        name: buyerUser.GetValue("displayName"),
                        email: buyerUser.GetValue("emailAddress"),
                        isGroup: buyerUser.GetValue("isGroup") == "1",
                        action: parameters.actions.purchase.GetName(),
                        parallel: contributionData ? contributionData.parallel : undefined
                    };
                    workflow.UpdateParallelWorkflowCurrentUser(buyerUser);
                    workflow.AddContributorAt(currentSequenceStep, buyerContributor);
                    workflow.AddContributorAt(currentSequenceStep, buyerContributor);
                    contributionData = GetContributionData(currentSequenceStep, "orderCanceled", Language.Translate("_PO canceled, items back to 'To order' status"));
                    contributionData.date = date_1;
                    workflow.NextContributor(contributionData);
                }
            });
            Process.WaitForUpdate();
            Process.LeaveForm();
        }
        else {
            Process.PreventApproval();
        }
    }
    function OnEditOrder() {
        var bok = Lib.Purchasing.PRValidation.SynchronizeItems({ fromAction: true });
        if (bok) {
            //One of the purchase Order has been modified, at least one item returns to "To Order" status
            //We need to reOpen the Workflow in order to do the Order or Cancel Items action afterward
            Log.Info("Restarting the workflow following EditOrder");
            //When the PO modified was autocreated, we need to autorize the buyer to create a new the Purchase Order
            Variable.SetValueAsString("AutoCreateOrderEnabled", "");
            // pour chaque buyer get item status if avant toorder = false et ordered = true et après synch qui passe toorder
            // à true et false faut ouvrir
            var itemAfterSynch = GetItemStatusByBuyer();
            var date_2 = new Date();
            var everyLineClose_1 = true;
            Sys.Helpers.Object.ForEach(itemAfterSynch, function (itemStatus, buyer) {
                // If no more items to order before the SynchronizeItems and after an item can be ordered, so open the PO
                if (itemStatus.hasToOrderItems === true) {
                    everyLineClose_1 = false;
                    var currentSequenceStep = workflow.GetContributorIndex();
                    var contributionData = workflow.GetContributorAt(currentSequenceStep);
                    if (workflow.IsEnded()) {
                        workflow.Reopen();
                        currentSequenceStep = workflow.GetContributorIndex();
                    }
                    var buyerUser = Users.GetUser(buyer);
                    var buyerContributor = {
                        //mandatory fields
                        contributorId: buyer + Lib.Purchasing.roleBuyer,
                        role: Lib.Purchasing.roleBuyer,
                        //not mandatory fields
                        login: buyer,
                        name: buyerUser.GetValue("displayName"),
                        email: buyerUser.GetValue("emailAddress"),
                        isGroup: buyerUser.GetValue("isGroup") == "1",
                        action: parameters.actions.purchase.GetName(),
                        parallel: contributionData ? contributionData.parallel : undefined
                    };
                    workflow.UpdateParallelWorkflowCurrentUser(buyerUser);
                    workflow.AddContributorAt(currentSequenceStep, buyerContributor);
                    contributionData.date = date_2;
                    workflow.NextContributor(contributionData);
                }
            });
            if (everyLineClose_1) {
                //close workflow because all item has been ordered
                var currentSequenceStep = workflow.GetContributorIndex();
                var contributionData = workflow.GetContributorAt(currentSequenceStep);
                contributionData.action = parameters.actions.orderCreated.GetName();
                contributionData.date = date_2;
                workflow.EndWorkflow(contributionData);
            }
            Process.WaitForUpdate();
            Process.LeaveForm();
        }
        else {
            Process.PreventApproval();
        }
    }
    function GetWorkflow() {
        return workflow;
    }
    validationscript.GetWorkflow = GetWorkflow;
    function GetCustomTagsByBuyer() {
        var customTagsByBuyer = {};
        var itemsByBuyer = GetItemStatusByBuyer(null, true);
        for (var buyer in itemsByBuyer) {
            if (Sys.Helpers.IsPlainObject(itemsByBuyer[buyer])) {
                var totalAmountByBuyer = Lib.Purchasing.Items.ComputePRAmounts(itemsByBuyer[buyer].stashedItems);
                customTagsByBuyer[buyer] = {
                    "GoodsSummary": GetGoodsSummary(1, itemsByBuyer[buyer].stashedItems),
                    "TotalNetAmount": totalAmountByBuyer.TotalNetAmount,
                    "Currency": totalAmountByBuyer.Currency
                };
            }
        }
        return customTagsByBuyer;
    }
    validationscript.GetCustomTagsByBuyer = GetCustomTagsByBuyer;
    function GetGoodsSummary(firstItemsCount, forItems) {
        if (firstItemsCount === undefined) {
            firstItemsCount = 1;
        }
        if (forItems === undefined) {
            forItems = [];
            // No specific items specifed, take all items
            var table = Data.GetTable("LineItems__");
            var tableCount = table.GetItemCount();
            for (var j = 0; j < tableCount; j++) {
                forItems.push(table.GetItem(j));
            }
        }
        var count = forItems.length;
        var summary = count > 0 ? forItems[0].GetValue("SupplyTypeName__") : "";
        var noCategory = !summary;
        if (!noCategory) {
            summary += " (";
        }
        var i;
        for (i = 0; (i < firstItemsCount) && (i < count); i++) {
            var lineItem = forItems[i];
            summary += lineItem.GetValue("ItemDescription__").replace(/,/g, " ");
            if ((i + 1) < count) {
                summary += ", ";
            }
        }
        if (i < count) {
            summary += "...";
        }
        if (!noCategory) {
            summary += ")";
        }
        return summary;
    }
    validationscript.GetGoodsSummary = GetGoodsSummary;
    function SetVariablesForPostValidation() {
        Variable.SetValueAsString("GoodsSummary__", GetGoodsSummary());
    }
    function autoApproveIfNextIsSameUser(sequenceStep, nextContributor, contributionData) {
        var notMergeableRoles = [Lib.Purchasing.roleBuyer];
        var currentUser = Lib.P2P.GetValidatorOrOwner();
        var currentUserLogin = currentUser.GetValue("Login");
        var forwarded = false;
        while (nextContributor && currentUserLogin === nextContributor.login && notMergeableRoles.indexOf(nextContributor.role) === -1) {
            forwarded = true;
            Log.Info("Auto approve step " + sequenceStep + " for " + nextContributor.login);
            workflow.NextContributor(contributionData);
            ChangeOwner(); // Need to change owner in order to be coherent with the current contributor
            sequenceStep = workflow.GetContributorIndex();
            var actionName = null;
            var autoComment = null;
            switch (nextContributor.action) {
                case parameters.actions.reviewal.GetName():
                    actionName = parameters.actions.reviewed.GetName();
                    autoComment = Language.Translate("_Requisition auto reviewed");
                    break;
                case parameters.actions.approval.GetName():
                default:
                    actionName = parameters.actions.approved.GetName();
                    autoComment = Language.Translate("_Requisition auto approved");
                    break;
            }
            contributionData = GetContributionData(sequenceStep, actionName, autoComment, { onBehalfOf: true });
            nextContributor = workflow.GetContributorAt(sequenceStep + 1);
        }
        return {
            forwarded: forwarded,
            sequenceStep: sequenceStep,
            nextContributor: nextContributor,
            contributionData: contributionData
        };
    }
    function NextContributorExist() {
        var nextContributor = workflow.GetContributorAt(workflow.GetContributorIndex() + 1);
        if (nextContributor && Users.GetUser(nextContributor.login) == null) {
            Lib.CommonDialog.NextAlert.Define("_Workflow error", "_Invalid user '{0}' in worflow, please contact your administrator", null, nextContributor.login);
            SendEmailNotification(g_currentContributor, "_A purchase requisition is in error", "Purchasing_Email_NotifPRError.htm");
            return false;
        }
        return true;
    }
    function SubmitImpl(params) {
        var currentContributor = workflow.GetContributorAt(params.sequenceStep);
        var approved = false;
        var nextContributor = null;
        var contributionData = null;
        var bok = NextContributorExist();
        if (bok) {
            nextContributor = workflow.GetContributorAt(params.sequenceStep + 1);
            contributionData = GetContributionData(params.sequenceStep, params.actionName, g_newComment, { onBehalfOf: true });
            var autoApproveReturn = autoApproveIfNextIsSameUser(params.sequenceStep, nextContributor, contributionData);
            params.sequenceStep = autoApproveReturn.sequenceStep;
            nextContributor = autoApproveReturn.nextContributor;
            contributionData = autoApproveReturn.contributionData;
            if (nextContributor.role === Lib.Purchasing.roleBuyer) {
                if (!Lib.Purchasing.PRBudget.IsBudgetViewedByEveryApprover()) {
                    Lib.CommonDialog.NextAlert.Define("_Budget update error", "_NotAllItemsApproved");
                    bok = false;
                }
                else {
                    if (Lib.P2P.Inventory.IsEnabled()) {
                        bok = TakeFromStockIfPossible();
                    }
                    if (bok) {
                        var lastApprover = currentContributor.role === Lib.Purchasing.roleApprover;
                        bok = OnApproved(lastApprover);
                        approved = true;
                    }
                }
            }
            else {
                bok = params.updateBudgetFn();
            }
        }
        if (bok && params.beforeSynchronizeItems) {
            params.beforeSynchronizeItems();
        }
        bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
            SetStatusAccordingToRole(nextContributor.role);
            return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
        });
        if (bok && approved) {
            bok = AutoCreateOrderIfNeeded();
        }
        // We reset the advisor list only when the role workflow is over
        if (bok && currentContributor.role !== nextContributor.role) {
            ResetAdvisorList();
        }
        Variable.SetValueAsString("AdditionalContributors", "");
        if (bok && params.beforeChangingContributorFn) {
            bok = params.beforeChangingContributorFn();
        }
        if (bok) {
            if (approved) {
                GoToNextStepIfApproved(contributionData);
            }
            else {
                GoToNextStep(contributionData);
            }
            NotifyEndOfContributionOnMobile(currentContributor);
        }
        return bok;
    }
    function SentBack(sequenceStep, actionName) {
        var bok = true;
        var currentContributor = workflow.GetContributorAt(sequenceStep);
        // update budget according to the role
        if (currentContributor.role === Lib.Purchasing.roleBuyer) {
            bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromCommitted();
        }
        else {
            bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromToApprove();
        }
        bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
            Data.SetValue("ApprovedDate__", null);
            Data.SetValue("RequisitionStatus__", "To complete");
            return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
        });
        if (bok) {
            var contributionData = GetContributionData(sequenceStep, actionName, g_newComment, { onBehalfOf: true });
            ResetAdvisorList();
            workflow.Restart(contributionData);
            var nextContributor = workflow.GetContributorAt(0);
            SendEmailNotification(nextContributor, "_A Purchase requisition must be verified", "Purchasing_Email_NotifBack.htm");
            NotifyEndOfContributionOnMobile(currentContributor);
            Process.Forward(nextContributor.login);
            Process.LeaveForm();
        }
        else {
            Process.PreventApproval();
        }
        return bok;
    }
    //#region workflow parameters
    var parameters = {
        actions: {
            submission: {
                OnDone: function (sequenceStep) {
                    // PRSubmissionDatetime stores the submission datetime. When it's empty, we consider the requisition hasn't yet been submitted.
                    var isNewRequest = !Variable.GetValueAsString("PRSubmissionDatetime");
                    Lib.P2P.SetBillingInfo("PR002");
                    var bok = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnBilling");
                    bok = bok || bok === null;
                    if (!bok) {
                        // !!! OnBilling is responsible to manage error, include eventual calling to Process.PreventApproval()
                        return true;
                    }
                    bok = SubmitImpl({
                        sequenceStep: sequenceStep,
                        actionName: this.actions.submitted.GetName(),
                        updateBudgetFn: function () { return Lib.Purchasing.PRBudget.AsSubmitted(); },
                        beforeSynchronizeItems: function () {
                            Data.SetValue("PRSubmissionDateTime__", new Date());
                        },
                        beforeChangingContributorFn: function () {
                            Data.SetValue("SubmissionDate__", new Date());
                            if (isNewRequest) {
                                var currentContributor = workflow.GetContributorAt(sequenceStep);
                                SendEmailNotification(currentContributor, "_A purchase requisition has been created", "Purchasing_Email_FirstNotifRequester.htm");
                                var strNow = Sys.Helpers.Date.Date2DBDateTime(new Date());
                                Variable.SetValueAsString("PRSubmissionDatetime", strNow);
                            }
                            return true;
                        }
                    });
                    if (!bok) {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            submitted: {},
            reviewal: {
                OnDone: function (sequenceStep) {
                    var bok = SubmitImpl({
                        sequenceStep: sequenceStep,
                        actionName: this.actions.reviewed.GetName(),
                        updateBudgetFn: function () { return Lib.Purchasing.PRBudget.AsReviewed(); }
                    });
                    if (!bok) {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            reviewed: {},
            approval: {
                OnDone: function (sequenceStep) {
                    var bok = Sys.Helpers.String.ToBoolean(Variable.GetValueAsString("WorkflowImpacted")) ?
                        Sys.Helpers.TryCallFunction("Lib.Workflow.Customization.Common.OnWorkflowImpacted") : true;
                    bok = bok || bok === null;
                    bok = bok && SubmitImpl({
                        sequenceStep: sequenceStep,
                        actionName: this.actions.approved.GetName(),
                        updateBudgetFn: function () { return Lib.Purchasing.PRBudget.AsToApprove(); }
                    });
                    if (!bok) {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            approved: {},
            forward: {
                OnDone: function (sequenceStep) {
                    var bok = SubmitImpl({
                        sequenceStep: sequenceStep,
                        actionName: this.actions.forward.GetName(),
                        updateBudgetFn: function () { return Lib.Purchasing.PRBudget.AsToApprove(); }
                    });
                    if (!bok) {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            reviewAndForward: {
                OnDone: function (sequenceStep) {
                    var bok = SubmitImpl({
                        sequenceStep: sequenceStep,
                        actionName: this.actions.reviewAndForward.GetName(),
                        updateBudgetFn: function () { return Lib.Purchasing.PRBudget.AsReviewed(); }
                    });
                    if (!bok) {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            rejected: {
                OnDone: function (sequenceStep) {
                    var currentContributor = workflow.GetContributorAt(sequenceStep);
                    var bok = true;
                    // update budget according to the role
                    if (currentContributor.role === Lib.Purchasing.roleBuyer) {
                        bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromCommitted();
                    }
                    else {
                        bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromToApprove();
                    }
                    bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                        Data.SetValue("RequisitionStatus__", "Rejected");
                        return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
                    });
                    if (bok) {
                        var contributionData = GetContributionData(sequenceStep, this.actions.rejected.GetName(), g_newComment, { onBehalfOf: true });
                        ResetAdvisorList();
                        // Sends email notification to the user
                        SendEmailNotification(workflow.GetContributorAt(0), "_A purchase requisition has been rejected", "Purchasing_Email_NotifRejected.htm");
                        NotifyEndOfContributionOnMobile(currentContributor);
                        workflow.EndWorkflow(contributionData);
                        // Reject message
                        Data.SetValue("State", 400);
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            sentBackAsReviewer: {
                OnDone: function (sequenceStep) {
                    return SentBack(sequenceStep, this.actions.sentBackAsReviewer.GetName());
                }
            },
            sentBackAsApprover: {
                OnDone: function (sequenceStep) {
                    return SentBack(sequenceStep, this.actions.sentBackAsApprover.GetName());
                }
            },
            sentBackAsBuyer: {
                OnDone: function (sequenceStep) {
                    return SentBack(sequenceStep, this.actions.sentBackAsBuyer.GetName());
                }
            },
            modifyPR: {
                OnDone: function (sequenceStep) {
                    var bok = true;
                    var currentContributor = workflow.GetContributorAt(sequenceStep);
                    // update budget according to the role
                    bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromToApprove();
                    bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                        Data.SetValue("RequisitionStatus__", "To complete");
                        return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
                    });
                    if (bok) {
                        var contributionData = GetContributionData(0, this.actions.modifyPR.GetName());
                        contributionData.comment = Language.Translate("_Has modified PR", false) + "\n" + g_newComment;
                        Data.SetValue("Comments__", "");
                        ResetAdvisorList();
                        workflow.Restart(contributionData);
                        var nextContributor = workflow.GetContributorAt(0);
                        // mail to send went recall from requester
                        SendEmailNotification(currentContributor, "_A Purchase requisition has been recalled", "Purchasing_Email_NotifRecall.htm");
                        NotifyEndOfContributionOnMobile(currentContributor);
                        Process.Forward(nextContributor.login);
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            rfi: {
                OnDone: function (sequenceStep) {
                    var bok = Lib.Purchasing.PRValidation.SynchronizeItems();
                    if (bok) {
                        var advisor = {
                            login: Variable.GetValueAsString("AdvisorLogin"),
                            name: Variable.GetValueAsString("AdvisorName"),
                            email: Variable.GetValueAsString("AdvisorEmail")
                        };
                        // add the current advisor in advisor list
                        var list = Variable.GetValueAsString("AdvisorLoginList");
                        list = list ? advisor.login + "\n" + list : advisor.login;
                        Variable.SetValueAsString("AdvisorLoginList", list);
                        var contributionData = GetContributionData(sequenceStep, this.actions.rfi.GetName(), g_newComment, { sharedWith: true, onBehalfOf: true });
                        SendEmailNotification(advisor, "_Request for information mail title", "Purchasing_Email_NotifRFI.htm");
                        Log.Info("Grand all right to advisor: " + advisor.login);
                        Process.SetRight(advisor.login, "all"); // Advisor can modify the comment
                        // Add a new step for user shared the PR (requester or approver). Previously done in the custom script.
                        // We do it here now in order to reduce the crash window between the both calls to workfow (AddContributorAt and NextContributor).
                        workflow.AddContributorAt(sequenceStep + 1, g_currentContributor);
                        workflow.NextContributor(contributionData);
                        Process.DisableChecks();
                        Process.PreventApproval();
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            comment: {
                OnDone: function (sequenceStep) {
                    var requesterOfInformations = workflow.GetContributorAt(sequenceStep);
                    // Add the step of advisor here. Previously done in the custom script.
                    // We do it here now in order to reduce the crash window between the both calls to workfow (AddContributorAt and NextContributor).
                    var advisor = {
                        login: Variable.GetValueAsString("AdvisorLogin"),
                        name: Variable.GetValueAsString("AdvisorName"),
                        email: Variable.GetValueAsString("AdvisorEmail")
                    };
                    workflow.AddContributorAt(sequenceStep, {
                        //mandatory fields
                        contributorId: advisor.login + Lib.Purchasing.roleAdvisor,
                        role: Lib.Purchasing.roleAdvisor,
                        //not mandatory fields
                        login: advisor.login,
                        name: advisor.name,
                        email: advisor.email,
                        action: this.actions.comment.GetName()
                    });
                    var contributionData = GetContributionData(sequenceStep, this.actions.commentDone.GetName(), g_newComment);
                    SendEmailNotification(requesterOfInformations, "_Your RFI has been answered", "Purchasing_Email_NotifRFIAnswer.htm");
                    workflow.NextContributor(contributionData);
                    Process.DisableChecks();
                    Process.PreventApproval();
                    Process.LeaveForm();
                }
            },
            commentDone: {},
            // This action is only called from the PO when the buyer orders one or several items of this PR with SynchronizeItems action
            purchase: {
                OnDone: function (sequenceStep) {
                    var bok = Lib.Purchasing.PRValidation.SynchronizeItems({ fromAction: true });
                    if (bok) {
                        // Group items by buyer to compute items status by buyers
                        var itemsByBuyer_1 = {};
                        Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                            var itemBuyer = line.GetValue("BuyerLogin__");
                            if (!itemsByBuyer_1[itemBuyer]) {
                                itemsByBuyer_1[itemBuyer] = [];
                            }
                            itemsByBuyer_1[itemBuyer].push(line);
                        });
                        var changeOwner = false;
                        for (var buyer in itemsByBuyer_1) {
                            if (Sys.Helpers.IsArray(itemsByBuyer_1[buyer])) {
                                var items = itemsByBuyer_1[buyer];
                                var status = Lib.Purchasing.PRValidation.GetGlobalStatus(items);
                                // Once all items are ordered for this buyer (status -> Pending delivery...) we go to the next step in workflow (recipient step or other buyers steps)
                                if (Lib.Purchasing.PRStatus.ForDelivery.indexOf(status) !== -1) {
                                    var buyerUser = Users.GetUser(buyer);
                                    if (buyerUser) {
                                        workflow.UpdateParallelWorkflowCurrentUser(buyerUser);
                                        ChangeOwner(); // Need to change owner in order to be coherent with the current contributor
                                        var currentContributor = workflow.GetContributorAt(sequenceStep);
                                        if (currentContributor.login === buyer) {
                                            // Contributor has been found and is current contributor, do his step
                                            var contributionData = GetContributionData(sequenceStep, this.actions.orderCreated.GetName(), g_newComment, { onBehalfOf: true });
                                            if (sequenceStep + 1 < workflow.GetNbContributors()) {
                                                // There are other buyers waiting for ordering, go to next step
                                                workflow.NextContributor(contributionData);
                                                sequenceStep = workflow.GetContributorIndex();
                                            }
                                            else {
                                                // End of workflow
                                                workflow.EndWorkflow(contributionData);
                                            }
                                            changeOwner = true;
                                        }
                                    }
                                    else {
                                        Log.Error("Unable to finalize purchase by buyer " + buyer + "' because this user does not exist");
                                    }
                                }
                            }
                        }
                        if (changeOwner) {
                            ChangeOwner();
                        }
                        Process.WaitForUpdate();
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            orderCreated: {},
            orderCanceled: {},
            // This action is only called from the PO when the recipient receives one or several items of this PR with SynchronizeItems action
            reception: {
                OnDone: function ( /*sequenceStep*/) {
                    var bok = Lib.Purchasing.PRValidation.SynchronizeItems({ fromAction: true });
                    if (bok) {
                        // Once all items are received (status -> Received) we end workflow
                        if (Data.GetValue("RequisitionStatus__") !== "Received") {
                            Process.WaitForUpdate();
                            Process.LeaveForm();
                        }
                        else {
                            // we need the "all" right in order to be able to resubmit PR if recipient cancels a GR and reopens PR
                            GiveRightToRecipients("all");
                        }
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            received: {},
            canceled: {
                OnDone: function (sequenceStep) {
                    var bok = true;
                    var currentContributor = workflow.GetContributorAt(sequenceStep);
                    var comment = g_newComment;
                    var PRPendingCommitment = currentContributor.role === Lib.Purchasing.roleApprover ||
                        currentContributor.role === Lib.Purchasing.roleReviewer;
                    if (PRPendingCommitment) {
                        bok = Lib.Purchasing.PRBudget.AsBackToBudgetFromToApprove();
                        comment = Language.Translate("_PR waiting for approval by {0} canceled by requester", false, currentContributor.name) + "\n" + comment;
                    }
                    bok = bok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
                        Data.SetValue("RequisitionStatus__", "Canceled");
                        return Lib.Purchasing.PRValidation.SynchronizeItems() || rollbackFn();
                    });
                    if (bok) {
                        var contributionData = GetContributionData(0, this.actions.canceled.GetName(), comment, { onBehalfOf: true }, true);
                        Data.SetValue("Comments__", "");
                        ResetAdvisorList();
                        workflow.EndWorkflow(contributionData);
                        if (PRPendingCommitment) {
                            SendEmailNotification(currentContributor, "_A purchase requisition has been canceled", "Purchasing_Email_NotifPRCancel.htm");
                        }
                        NotifyEndOfContributionOnMobile(currentContributor);
                        // Cancel message
                        Data.SetValue("State", 300);
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            remainingItemsCanceled: {
                OnDone: function ( /*sequenceStep: number*/) {
                    var cancelerUser = Lib.P2P.GetValidator();
                    var selectOnlyBuyerItems = Lib.Purchasing.IsBuyerInWorkflow(workflow, cancelerUser);
                    var items = [];
                    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                        if (line.GetValue("ItemStatus__") !== "Canceled" &&
                            (!selectOnlyBuyerItems || Lib.P2P.CurrentUserMatchesLogin(line.GetValue("BuyerLogin__"), cancelerUser))) {
                            if (Sys.Parameters.GetInstance("PAC").GetParameter("AllowSplitPRIntoMultiplePO", false)) {
                                items.push(line.GetValue("LineItemNumber__"));
                            }
                            else if (line.GetValue("ItemOrderedQuantity__") == 0) {
                                items.push(line.GetValue("LineItemNumber__"));
                            }
                        }
                    });
                    var bok = CancelItems(items, this.actions.remainingItemsCanceled.GetName(), cancelerUser, new Date(), g_newComment);
                    if (bok) {
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            },
            itemsCanceled: {
                OnDone: function ( /*sequenceStep: number*/) {
                    var bok = true;
                    var cancellationData;
                    var cancelerUser;
                    try {
                        cancellationData = JSON.parse(Variable.GetValueAsString("ResumeWithActionData") || "");
                    }
                    catch (e) {
                        Log.Error("Failed to parse cancellation data: " + e);
                        bok = false;
                    }
                    bok = bok && !!cancellationData.user;
                    if (bok) {
                        cancelerUser = Users.GetUser(cancellationData.user.login);
                        if (!cancelerUser) {
                            Log.Error("Cannot find user with login: " + cancellationData.user.login);
                            bok = false;
                        }
                    }
                    bok = bok && CancelItems(cancellationData.lines, this.actions.itemsCanceled.GetName(), cancelerUser, cancellationData.date, cancellationData.comment);
                    if (bok) {
                        Process.LeaveForm();
                    }
                    else {
                        Process.PreventApproval();
                    }
                    return bok;
                }
            }
        },
        callbacks: {
            OnError: function (msg) {
                Log.Error(msg);
            }
        }
    };
    Sys.Helpers.Extend(true, parameters, Lib.Purchasing.WorkflowPR.Parameters);
    //#endregion
    workflow.AllowRebuild(false);
    workflow.Define(parameters);
    function SetRequisitionNumber() {
        if (Sys.Helpers.IsEmpty(Data.GetValue("RequisitionNumber__"))) {
            var ReqNumber = Lib.Purchasing.NextNumber("PR", "RequisitionNumber__");
            if (ReqNumber !== "") {
                Data.SetValue("RequisitionNumber__", "" + ReqNumber);
            }
        }
    }
    /** ******** **/
    /** RUN PART **/
    /** ******** **/
    Lib.Purchasing.InitTechnicalFields();
    // Index LineItems table to ES for better reporting
    Lib.P2P.SetTablesToIndex(["LineItems__", "ApproversList__"]);
    Lib.Purchasing.RemoveEmptyLineItem(Data.GetTable("LineItems__"));
    if (Lib.ERP.IsSAP()) {
        Sys.Helpers.Data.SetAllowTableValuesOnlyForColumns("LineItems__", ["ItemCostCenterName__", "ItemGLAccount__", "VendorName__", "VendorNumber__"], false);
    }
    Variable.SetValueAsString("Last_Comments__", g_newComment);
    // First validation after extracting
    if (!currentName && !currentAction) {
        if (Variable.GetValueAsString("NotifyQuoteByEmailRequester__") === "1") {
            Variable.SetValueAsString("NotifyQuoteByEmailRequester__", "Done"); // send it just one time
            SetVendorNameStyle();
            SetRequisitionNumber();
            Process.RecallScript("PostValidation_NotifQuote");
        }
    }
    else if (currentName === "save") {
        // first call of the validation script when a user clicks an action button.
        // nothing todo here because the XGF isn't saved after execution.
    }
    else if (currentName === "Save_") {
        Lib.Purchasing.PRValidation.OnSave();
        SetRequisitionNumber();
        Process.PreventApproval();
    }
    else if (currentName === "PostValidation_Submit") {
        workflow.DoAction(g_currentContributor.action);
    }
    else if (currentName === "PostValidation_RFI") {
        workflow.DoAction(parameters.actions.rfi.GetName());
    }
    else if (currentName === "PostValidation_SentBack") {
        var actionName = parameters.actions.sentBackAsApprover.GetName();
        if (g_currentContributor.role === Lib.Purchasing.roleReviewer) {
            actionName = parameters.actions.sentBackAsReviewer.GetName();
        }
        else if (g_currentContributor.role === Lib.Purchasing.roleBuyer) {
            actionName = parameters.actions.sentBackAsBuyer.GetName();
        }
        workflow.DoAction(actionName);
    }
    else if (currentName === "PostValidation_ModifyPR") {
        workflow.DoAction(parameters.actions.modifyPR.GetName());
    }
    else if (currentName === "PostValidation_Comment_Answer") {
        workflow.DoAction(parameters.actions.comment.GetName());
    }
    else if (currentName === "PostValidation_Cancel_purchase_requisition") {
        workflow.DoAction(parameters.actions.canceled.GetName());
    }
    else if (currentName === "PostValidation_Cancel_remaining_items") {
        workflow.DoAction(parameters.actions.remainingItemsCanceled.GetName());
    }
    else if (currentName === "PostValidation_Cancel_items") {
        workflow.DoAction(parameters.actions.itemsCanceled.GetName());
    }
    else if (currentName === "PostValidation_Reject") {
        workflow.DoAction(parameters.actions.rejected.GetName());
    }
    else if (currentName === "PostValidation_Forward") {
        workflow.DoAction(parameters.actions.forward.GetName());
    }
    else if (currentName === "PostValidation_ReviewAndForward") {
        workflow.DoAction(parameters.actions.reviewAndForward.GetName());
    }
    else if (currentName === "PostValidation_NotifQuote") {
        var requester = {
            login: Lib.P2P.GetOwner().GetValue("Login"),
            email: Lib.P2P.GetOwner().GetValue("EmailAddress"),
            name: Lib.P2P.GetOwner().GetValue("DisplayName")
        };
        SendEmailNotification(requester, "_New quotation - Complete your purchase requisition", "Purchasing_Email_NotifQuotationReceived.htm");
    }
    else if (currentName === "PostValidation_Receive") {
        parameters.actions.reception.OnDone();
    }
    else if (currentName !== "" && currentAction !== "") {
        /** Workflow logic - Warning: it loops on the workflow since there is no workflowFinished flag yet **/
        if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
            SetRequisitionNumber();
            Lib.CommonDialog.NextAlert.Reset();
            switch (currentName) {
                case "Cancel_purchase_requisition":
                    Process.RecallScript("PostValidation_Cancel_purchase_requisition");
                    break;
                case "Cancel_remaining_items":
                    if (Lib.Purchasing.PRValidation.CanResumeWithAction(currentName)) {
                        Process.RecallScript("PostValidation_Cancel_remaining_items");
                    }
                    break;
                case "ModifyPR":
                    if (g_sequenceStep > 0) {
                        if (currentAction !== "ResumeWithAction" || Lib.Purchasing.PRValidation.CanResumeWithAction(currentName)) {
                            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                                item.SetValue("ItemBudgetViewed__", false);
                                item.SetValue("OutOfBudget__", false);
                            });
                            Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
                            SetVariablesForPostValidation();
                            Process.RecallScript("PostValidation_ModifyPR");
                        }
                    }
                    else {
                        Data.SetError("ApproversList__", "_Failed to forward back to requester");
                    }
                    break;
                case "Cancel_items":
                    if (Lib.Purchasing.PRValidation.CanResumeWithAction(currentName)) {
                        var cancellationData = void 0;
                        try {
                            cancellationData = JSON.parse(Variable.GetValueAsString("ResumeWithActionData") || "");
                        }
                        catch (e) {
                            Log.Error("Failed to parse cancellation data: " + e);
                        }
                        if (cancellationData) {
                            Variable.SetValueAsString("Last_Comments__", cancellationData.comment);
                        }
                        Process.RecallScript("PostValidation_Cancel_items");
                    }
                    break;
                case "Reject":
                    if (currentAction !== "ResumeWithAction" || Lib.Purchasing.PRValidation.CanResumeWithAction(currentName)) {
                        SetVariablesForPostValidation();
                        Process.RecallScript("PostValidation_Reject");
                    }
                    break;
                case "Submit_":
                    if (!IsFormInError()) {
                        if (g_nextContributor.role !== Lib.Purchasing.roleReceiver) {
                            SetVariablesForPostValidation();
                        }
                        Process.RecallScript("PostValidation_Submit");
                    }
                    break;
                case "BackToAP":
                    if (g_sequenceStep > 0) {
                        if (currentAction !== "ResumeWithAction" || Lib.Purchasing.PRValidation.CanResumeWithAction(currentName)) {
                            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (item) {
                                item.SetValue("ItemBudgetViewed__", false);
                                item.SetValue("OutOfBudget__", false);
                            });
                            Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
                            SetVariablesForPostValidation();
                            Process.RecallScript("PostValidation_SentBack");
                        }
                    }
                    else {
                        Data.SetError("ApproversList__", "_Failed to forward back to requester");
                    }
                    break;
                case "RequestForInformation":
                    {
                        Process.DisableChecks();
                        var advisorLogin = Variable.GetValueAsString("AdvisorLogin");
                        if (advisorLogin) {
                            SetVariablesForPostValidation();
                            Process.RecallScript("PostValidation_RFI");
                        }
                        break;
                    }
                case "Comment_Answer":
                    Process.DisableChecks();
                    if (g_sequenceStep > 0) {
                        Data.SetValue("CurrentAttachmentFlag__", Attach.GetNbAttach());
                        SetVariablesForPostValidation();
                        Process.RecallScript("PostValidation_Comment_Answer");
                    }
                    else {
                        Data.SetError("ApproversList__", "_Failed to add your comment");
                    }
                    break;
                case "Approve_Forward":
                    if (!IsFormInError()) {
                        SetVariablesForPostValidation();
                        Process.RecallScript(g_currentContributor.role === Lib.Purchasing.roleReviewer ? "PostValidation_ReviewAndForward" : "PostValidation_Forward");
                    }
                    break;
                case "SynchronizeItems":
                    Lib.Purchasing.PRValidation.SynchronizeItemsFromAction();
                    break;
                case "FullBudgetRecovery":
                    Lib.Purchasing.PRBudget.DoFullRecovery();
                    break;
                case "Cancel_PO":
                    OnCancelPO();
                    break;
                case "EditOrder":
                    OnEditOrder();
                    break;
                default:
                    if (!IsFormInError()) {
                        Lib.Purchasing.PRValidation.OnUnknownAction(currentAction, currentName);
                    }
                    break;
            }
        }
        else if (currentAction !== "reprocess" && currentAction !== "reprocess_asynchronous") {
            Lib.Purchasing.PRValidation.OnUnknownAction(currentAction, currentName);
        }
    }
    else {
        Lib.Purchasing.PRValidation.OnUnknownAction(currentAction, currentName);
    }
    var isRecallScriptScheduled = Process.IsRecallScriptScheduled();
    Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnValidationScriptEnd", currentAction, currentName, isRecallScriptScheduled);
})(validationscript || (validationscript = {}));
