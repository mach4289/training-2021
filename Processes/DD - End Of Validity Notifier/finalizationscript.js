///#GLOBALS Sys Lib
var Notifier;
(function (Notifier)
{
	function sendEmail(dest, customTags)
	{
		var template = "DD_ValidityNotifier_XX.htm";
		var userID = Data.GetValue("OwnerId");
		var userAdmin = Users.GetUserAsProcessAdmin(userID);
		var destEmail = dest.GetValue("EmailAddress");
		customTags.AccountLogo__ = userAdmin.GetLogoPath();
		var email = Sys.EmailNotification.CreateEmailWithUser(userAdmin, destEmail, null, template, customTags, true);
		if (email)
		{
			var destLanguage = dest.GetVars().GetValue_String("Language", 0);
			Sys.EmailNotification.AddSender(email, "notification@eskerondemand.com", Language.TranslateInto("Esker Accounts payable", destLanguage, false));
			Sys.EmailNotification.SendEmail(email);
		}
		else
		{
			Log.Error("Cannot send email to '" + destEmail + "'");
		}
	}
	Notifier.sendEmail = sendEmail;

	function GetDocumentTypesToNotify()
	{
		var documentTypes = [];
		var ASQuery = Process.CreateQueryAsProcessAdmin();
		ASQuery.SetSpecificTable("DD - Application Settings__");
		ASQuery.SetAttributesList("Document_Type__");
		ASQuery.SetFilter("(EndOfValidityNotification__=true)");
		ASQuery.SetSearchInArchive(true);
		if (!ASQuery.MoveFirst())
		{
			Log.Warn("Query on 'DD - Application Settings__' failed");
			return documentTypes;
		}
		var transport = ASQuery.MoveNext();
		while (transport)
		{
			var docType = transport.GetUninheritedVars().GetValue_String("Document_Type__", 0);
			documentTypes.push(docType);
			transport = ASQuery.MoveNext();
		}
		return documentTypes;
	}
	Notifier.GetDocumentTypesToNotify = GetDocumentTypesToNotify;

	function SendNotificationIfNeeded(docType, horizonDate)
	{
		Log.Info("... Notifying for SDA configuration '" + docType + "' ...");
		var SFQuery = Process.CreateQueryAsProcessAdmin();
		SFQuery.SetSpecificTable("CDNAME#DD - SenderForm");
		SFQuery.SetAttributesList("NotificationRecipient__,EndOfValidity__,Subject,State");
		SFQuery.SetFilter("&(Document_Type__=" + docType + ")(EndOfValidity__<" + horizonDate + ")(State=100)");
		SFQuery.SetSortOrder("NotificationRecipient__ DESC, SubmitDateTime DESC");
		SFQuery.SetSearchInArchive(true);
		if (!SFQuery.MoveFirst())
		{
			Log.Warn("Query on 'DD - SenderForm' failed for type '" + docType + "'");
			return;
		}
		var currentRecipient;
		var expiredDocToNotifyCount = 0;
		var transport = SFQuery.MoveNext();
		while (transport)
		{
			var vars = transport.GetUninheritedVars();
			var recipientFullDN = vars.GetValue_String("NotificationRecipient__", 0);
			// We only notify for the most recent document of each type
			if (recipientFullDN !== currentRecipient)
			{
				var recipient = Users.GetUser(recipientFullDN);
				if (recipient)
				{
					var endOfValidityDate = vars.GetValue_Date("EndOfValidity__", 0);
					// Workaround timezone issues when formatting date in recipient timezone
					endOfValidityDate.setHours(13);
					var customTags = {
						endOfValidity: recipient.GetFormattedDate(endOfValidityDate,
						{
							dateFormat: "ShortDate",
							timeFormat: "None"
						}),
						documentName: vars.GetValue_String("Subject", 0),
						documentType: docType,
						portalURL: recipient.GetPortalURL()
					};
					sendEmail(recipient, customTags);
				}
				else
				{
					Log.Warn("User not found: " + recipientFullDN);
				}
				++expiredDocToNotifyCount;
				currentRecipient = recipientFullDN;
			}
			transport = SFQuery.MoveNext();
		}
		Log.Info("Found " + expiredDocToNotifyCount + " expired documents of type '" + docType + "'");
	}
	Notifier.SendNotificationIfNeeded = SendNotificationIfNeeded;
})(Notifier || (Notifier = {}));

function run()
{
	var horizonDate = new Date();
	horizonDate.setDate(horizonDate.getDate() + 15);
	var horizonDateDBString = Sys.Helpers.Date.Date2DBDate(horizonDate);
	var documentTypes = Notifier.GetDocumentTypesToNotify();
	if (documentTypes.length)
	{
		documentTypes.forEach(function (docType)
		{
			return Notifier.SendNotificationIfNeeded(docType, horizonDateDBString);
		});
	}
	else
	{
		Log.Info("No notifiable SDA configuration was found");
	}
}
run();