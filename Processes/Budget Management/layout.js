{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 36,
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Document data",
																"hidden": true
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 0,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {},
																				"colspans": []
																			},
																			"data": [
																				"fields"
																			],
																			"*": {},
																			"stamp": 16
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 14,
															"data": []
														},
														"WizardStepsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color1",
																"labelsAlignment": "right",
																"label": "_WizardStepsPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 32,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"WizardSteps__": "LabelWizardSteps__",
																			"LabelWizardSteps__": "WizardSteps__"
																		},
																		"version": 0
																	},
																	"stamp": 33,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line__": {
																						"line": 2,
																						"column": 1
																					},
																					"WizardSteps__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelWizardSteps__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 34,
																			"*": {
																				"LabelWizardSteps__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 51
																				},
																				"WizardSteps__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "120",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 53
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 17,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 47,
													"*": {
														"GeneralPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_GeneralPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 48,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"User__": "LabelUser__",
																			"LabelUser__": "User__",
																			"Reason__": "LabelReason__",
																			"LabelReason__": "Reason__",
																			"Operation__": "LabelOperation__",
																			"LabelOperation__": "Operation__",
																			"UserName__": "LabelUserName__",
																			"LabelUserName__": "UserName__"
																		},
																		"version": 0
																	},
																	"stamp": 49,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"User__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelUser__": {
																						"line": 2,
																						"column": 1
																					},
																					"Reason__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelReason__": {
																						"line": 4,
																						"column": 1
																					},
																					"Operation__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelOperation__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 1,
																						"column": 1
																					},
																					"UserName__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelUserName__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 50,
																			"*": {
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 98
																				},
																				"LabelUser__": {
																					"type": "Label",
																					"data": [
																						"User__"
																					],
																					"options": {
																						"label": "_User",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"User__": {
																					"type": "ShortText",
																					"data": [
																						"User__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_User",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 68
																				},
																				"LabelUserName__": {
																					"type": "Label",
																					"data": [
																						"UserName__"
																					],
																					"options": {
																						"label": "_UserName",
																						"version": 0
																					},
																					"stamp": 210
																				},
																				"UserName__": {
																					"type": "ShortText",
																					"data": [
																						"UserName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_UserName",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 211
																				},
																				"LabelReason__": {
																					"type": "Label",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"label": "_Reason",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"Reason__": {
																					"type": "LongText",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Reason",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 10,
																						"browsable": false
																					},
																					"stamp": 70
																				},
																				"LabelOperation__": {
																					"type": "Label",
																					"data": [
																						"Operation__"
																					],
																					"options": {
																						"label": "_Operation",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"Operation__": {
																					"type": "ComboBox",
																					"data": [
																						"Operation__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ImportBudgetData",
																							"1": "_ExportBudgetLines",
																							"2": "_ReviseBudget",
																							"3": "_CloseBudget",
																							"4": "_DeleteBudgetLines"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Import",
																							"1": "Export",
																							"2": "Revise",
																							"3": "Close",
																							"4": "Delete"
																						},
																						"label": "_Operation",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 72
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 73,
													"*": {
														"ImportPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "100",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ImportPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 74,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BrowseCSV__": "LabelBrowseCSV__",
																			"LabelBrowseCSV__": "BrowseCSV__"
																		},
																		"version": 0
																	},
																	"stamp": 75,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line3__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 3,
																						"column": 1
																					},
																					"Import_Description__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 76,
																			"*": {
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 84
																				},
																				"Import_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 50,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Import_Description",
																						"version": 0
																					},
																					"stamp": 83
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 91
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 77,
													"*": {
														"ResultPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ResultPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 78,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompletedTime__": "LabelCompletedTime__",
																			"LabelCompletedTime__": "CompletedTime__",
																			"StartedTime__": "LabelStartedTime__",
																			"LabelStartedTime__": "StartedTime__",
																			"Output__": "LabelOutput__",
																			"LabelOutput__": "Output__"
																		},
																		"version": 0
																	},
																	"stamp": 79,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompletedTime__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelCompletedTime__": {
																						"line": 2,
																						"column": 1
																					},
																					"StartedTime__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelStartedTime__": {
																						"line": 1,
																						"column": 1
																					},
																					"Output__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelOutput__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 80,
																			"*": {
																				"LabelStartedTime__": {
																					"type": "Label",
																					"data": [
																						"StartedTime__"
																					],
																					"options": {
																						"label": "_StartedTime",
																						"version": 0
																					},
																					"stamp": 103
																				},
																				"StartedTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"StartedTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_StartedTime",
																						"activable": true,
																						"width": "250",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 104
																				},
																				"LabelCompletedTime__": {
																					"type": "Label",
																					"data": [
																						"CompletedTime__"
																					],
																					"options": {
																						"label": "_CompletedTime",
																						"version": 0
																					},
																					"stamp": 105
																				},
																				"CompletedTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"CompletedTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_CompletedTime",
																						"activable": true,
																						"width": "250",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 106
																				},
																				"LabelOutput__": {
																					"type": "Label",
																					"data": [
																						"Output__"
																					],
																					"options": {
																						"label": "_Output",
																						"version": 0
																					},
																					"stamp": 111
																				},
																				"Output__": {
																					"type": "LongText",
																					"data": [
																						"Output__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Output",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 10,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 112
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 113
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 89,
													"*": {
														"AttachmentsPane": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_AttachmentsPane",
																"hidden": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true,
																		"previewButton": false
																	},
																	"stamp": 10
																}
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 117,
													"*": {
														"DeletePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_DeletePane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 118,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Delete_BudgetLinesImpacted__": "LabelDelete_BudgetLinesImpacted__",
																			"LabelDelete_BudgetLinesImpacted__": "Delete_BudgetLinesImpacted__",
																			"Delete_CompanyCode__": "LabelDelete_CompanyCode__",
																			"LabelDelete_CompanyCode__": "Delete_CompanyCode__",
																			"Delete_BudgetID__": "LabelDelete_BudgetID__",
																			"LabelDelete_BudgetID__": "Delete_BudgetID__",
																			"Delete_PeriodCode__": "LabelDelete_PeriodCode__",
																			"LabelDelete_PeriodCode__": "Delete_PeriodCode__"
																		},
																		"version": 0
																	},
																	"stamp": 119,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Delete_BudgetLinesImpacted__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelDelete_BudgetLinesImpacted__": {
																						"line": 13,
																						"column": 1
																					},
																					"Delete_CompanyCode__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelDelete_CompanyCode__": {
																						"line": 8,
																						"column": 1
																					},
																					"Delete_BudgetID__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelDelete_BudgetID__": {
																						"line": 10,
																						"column": 1
																					},
																					"Spacer_line9__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line10__": {
																						"line": 11,
																						"column": 1
																					},
																					"Delete_PeriodCode__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelDelete_PeriodCode__": {
																						"line": 9,
																						"column": 1
																					},
																					"Delete_ByBudgetID__": {
																						"line": 6,
																						"column": 1
																					},
																					"Delete_ByCriteria__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line11__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line12__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer_line17__": {
																						"line": 5,
																						"column": 1
																					},
																					"SpacerAfterBudgetID__": {
																						"line": 12,
																						"column": 1
																					},
																					"Delete_Description__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 120,
																			"*": {
																				"Spacer_line11__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line11",
																						"version": 0
																					},
																					"stamp": 153
																				},
																				"Delete_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 50,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Delete_Description",
																						"version": 0
																					},
																					"stamp": 133
																				},
																				"Spacer_line9__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line9",
																						"version": 0
																					},
																					"stamp": 134
																				},
																				"Delete_ByCriteria__": {
																					"type": "RadioButton",
																					"data": [
																						"Delete_ByCriteria__"
																					],
																					"options": {
																						"keys": [
																							"_ByCriteria"
																						],
																						"values": [
																							"_ByCriteria"
																						],
																						"label": "_Delete_ByCriteria",
																						"activable": true,
																						"width": "",
																						"marge": 50,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 152
																				},
																				"Spacer_line17__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line17",
																						"version": 0
																					},
																					"stamp": 207
																				},
																				"Delete_ByBudgetID__": {
																					"type": "RadioButton",
																					"data": [
																						"Delete_ByBudgetID__"
																					],
																					"options": {
																						"keys": [
																							"_ByBudgetID"
																						],
																						"values": [
																							"_ByBudgetID"
																						],
																						"label": "_Delete_ByBudgetID",
																						"activable": true,
																						"width": "",
																						"marge": 50,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 150
																				},
																				"Spacer_line12__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line12",
																						"version": 0
																					},
																					"stamp": 206
																				},
																				"LabelDelete_CompanyCode__": {
																					"type": "Label",
																					"data": [
																						"Delete_CompanyCode__"
																					],
																					"options": {
																						"label": "_Delete_CompanyCode",
																						"version": 0
																					},
																					"stamp": 127
																				},
																				"Delete_CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Delete_CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Delete_CompanyCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Delete_Help_Select a company code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 128
																				},
																				"LabelDelete_PeriodCode__": {
																					"type": "Label",
																					"data": [
																						"Delete_PeriodCode__"
																					],
																					"options": {
																						"label": "_Delete_PeriodCode",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"Delete_PeriodCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Delete_PeriodCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Delete_PeriodCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Delete_Help_Select a period code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true
																					},
																					"stamp": 122
																				},
																				"LabelDelete_BudgetID__": {
																					"type": "Label",
																					"data": [
																						"Delete_BudgetID__"
																					],
																					"options": {
																						"label": "_Delete_BudgetID",
																						"version": 0
																					},
																					"stamp": 129
																				},
																				"Delete_BudgetID__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Delete_BudgetID__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Delete_BudgetID",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Delete_Help_Select a budget ID",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 130
																				},
																				"Spacer_line10__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line10",
																						"version": 0
																					},
																					"stamp": 135
																				},
																				"SpacerAfterBudgetID__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "34",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line16",
																						"version": 0
																					},
																					"stamp": 205
																				},
																				"LabelDelete_BudgetLinesImpacted__": {
																					"type": "Label",
																					"data": [
																						"Delete_BudgetLinesImpacted__"
																					],
																					"options": {
																						"label": "_BudgetLinesImpacted",
																						"version": 0
																					},
																					"stamp": 131
																				},
																				"Delete_BudgetLinesImpacted__": {
																					"type": "Integer",
																					"data": [
																						"Delete_BudgetLinesImpacted__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_BudgetLinesImpacted",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"browsable": false,
																						"defaultValue": "0"
																					},
																					"stamp": 132
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 155,
													"*": {
														"ExportPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ExportPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 156,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Export_BudgetLinesImpacted__": "LabelExport_BudgetLinesImpacted__",
																			"LabelExport_BudgetLinesImpacted__": "Export_BudgetLinesImpacted__",
																			"Export_CompanyCode__": "LabelExport_CompanyCode__",
																			"LabelExport_CompanyCode__": "Export_CompanyCode__",
																			"Export_PeriodCode__": "LabelExport_PeriodCode__",
																			"LabelExport_PeriodCode__": "Export_PeriodCode__",
																			"Export_BudgetID__": "LabelExport_BudgetID__",
																			"LabelExport_BudgetID__": "Export_BudgetID__"
																		},
																		"version": 0
																	},
																	"stamp": 157,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line2__": {
																						"line": 1,
																						"column": 1
																					},
																					"Export_BudgetLinesImpacted__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelExport_BudgetLinesImpacted__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer_line14__": {
																						"line": 6,
																						"column": 1
																					},
																					"Export_CompanyCode__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelExport_CompanyCode__": {
																						"line": 4,
																						"column": 1
																					},
																					"Export_PeriodCode__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelExport_PeriodCode__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line6__": {
																						"line": 3,
																						"column": 1
																					},
																					"Export_Description__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 158,
																			"*": {
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"Export_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 50,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Export_Description",
																						"version": 0
																					},
																					"stamp": 226
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 227
																				},
																				"LabelExport_CompanyCode__": {
																					"type": "Label",
																					"data": [
																						"Export_CompanyCode__"
																					],
																					"options": {
																						"label": "_Export_CompanyCode",
																						"version": 0
																					},
																					"stamp": 161
																				},
																				"Export_CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Export_CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Export_CompanyCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Export_Help_Select a company code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true
																					},
																					"stamp": 162
																				},
																				"LabelExport_PeriodCode__": {
																					"type": "Label",
																					"data": [
																						"Export_PeriodCode__"
																					],
																					"options": {
																						"label": "_Export_PeriodCode",
																						"version": 0
																					},
																					"stamp": 163
																				},
																				"Export_PeriodCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Export_PeriodCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Export_PeriodCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Export_Help_Select a period code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true
																					},
																					"stamp": 164
																				},
																				"Spacer_line14__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line14",
																						"version": 0
																					},
																					"stamp": 173
																				},
																				"LabelExport_BudgetLinesImpacted__": {
																					"type": "Label",
																					"data": [
																						"Export_BudgetLinesImpacted__"
																					],
																					"options": {
																						"label": "_BudgetLinesSelected",
																						"version": 0
																					},
																					"stamp": 171
																				},
																				"Export_BudgetLinesImpacted__": {
																					"type": "Integer",
																					"data": [
																						"Export_BudgetLinesImpacted__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_BudgetLinesSelected",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"browsable": false,
																						"defaultValue": "0"
																					},
																					"stamp": 172
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 176,
													"*": {
														"RevisePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RevisePane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 177,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Revise_BudgetID__": "LabelRevise_BudgetID__",
																			"LabelRevise_BudgetID__": "Revise_BudgetID__",
																			"BudgetDescription__": "LabelBudgetDescription__",
																			"LabelBudgetDescription__": "BudgetDescription__",
																			"Budget__": "LabelBudget__",
																			"LabelBudget__": "Budget__",
																			"NewBudget__": "LabelNewBudget__",
																			"LabelNewBudget__": "NewBudget__",
																			"Revise_PeriodCode__": "LabelRevise_PeriodCode__",
																			"LabelRevise_PeriodCode__": "Revise_PeriodCode__",
																			"Revise_CompanyCode__": "LabelRevise_CompanyCode__",
																			"LabelRevise_CompanyCode__": "Revise_CompanyCode__",
																			"AvailableBeforeRevision__": "LabelAvailableBeforeRevision__",
																			"LabelAvailableBeforeRevision__": "AvailableBeforeRevision__",
																			"AvailableAfterRevision__": "LabelAvailableAfterRevision__",
																			"LabelAvailableAfterRevision__": "AvailableAfterRevision__"
																		},
																		"version": 0
																	},
																	"stamp": 178,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Revise_BudgetID__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRevise_BudgetID__": {
																						"line": 4,
																						"column": 1
																					},
																					"BudgetDescription__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelBudgetDescription__": {
																						"line": 5,
																						"column": 1
																					},
																					"Budget__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelBudget__": {
																						"line": 8,
																						"column": 1
																					},
																					"NewBudget__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelNewBudget__": {
																						"line": 9,
																						"column": 1
																					},
																					"Revise_PeriodCode__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelRevise_PeriodCode__": {
																						"line": 7,
																						"column": 1
																					},
																					"Revise_CompanyCode__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelRevise_CompanyCode__": {
																						"line": 6,
																						"column": 1
																					},
																					"AvailableBeforeRevision__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelAvailableBeforeRevision__": {
																						"line": 10,
																						"column": 1
																					},
																					"AvailableAfterRevision__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelAvailableAfterRevision__": {
																						"line": 11,
																						"column": 1
																					},
																					"Spacer_line15__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line16__": {
																						"line": 3,
																						"column": 1
																					},
																					"Revise_Description__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 11,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 179,
																			"*": {
																				"Spacer_line15__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line15",
																						"version": 0
																					},
																					"stamp": 204
																				},
																				"Revise_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 50,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Revise_Description",
																						"version": 0
																					},
																					"stamp": 209
																				},
																				"Spacer_line16__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line16",
																						"version": 0
																					},
																					"stamp": 208
																				},
																				"LabelRevise_BudgetID__": {
																					"type": "Label",
																					"data": [
																						"Revise_BudgetID__"
																					],
																					"options": {
																						"label": "_Revise_BudgetID",
																						"version": 0
																					},
																					"stamp": 180
																				},
																				"Revise_BudgetID__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Revise_BudgetID__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Revise_BudgetID",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 181
																				},
																				"LabelBudgetDescription__": {
																					"type": "Label",
																					"data": [
																						"BudgetDescription__"
																					],
																					"options": {
																						"label": "_BudgetDescription",
																						"version": 0
																					},
																					"stamp": 182
																				},
																				"BudgetDescription__": {
																					"type": "ShortText",
																					"data": [
																						"BudgetDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_BudgetDescription",
																						"activable": true,
																						"width": "400",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 120,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 183
																				},
																				"LabelRevise_CompanyCode__": {
																					"type": "Label",
																					"data": [
																						"Revise_CompanyCode__"
																					],
																					"options": {
																						"label": "_Revise_CompanyCode",
																						"version": 0
																					},
																					"stamp": 184
																				},
																				"Revise_CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"Revise_CompanyCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Revise_CompanyCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 185
																				},
																				"LabelRevise_PeriodCode__": {
																					"type": "Label",
																					"data": [
																						"Revise_PeriodCode__"
																					],
																					"options": {
																						"label": "_Revise_PeriodCode",
																						"version": 0
																					},
																					"stamp": 186
																				},
																				"Revise_PeriodCode__": {
																					"type": "ShortText",
																					"data": [
																						"Revise_PeriodCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Revise_PeriodCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 187
																				},
																				"LabelBudget__": {
																					"type": "Label",
																					"data": [
																						"Budget__"
																					],
																					"options": {
																						"label": "_Budget",
																						"version": 0
																					},
																					"stamp": 188
																				},
																				"Budget__": {
																					"type": "Decimal",
																					"data": [
																						"Budget__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Budget",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 189
																				},
																				"LabelNewBudget__": {
																					"type": "Label",
																					"data": [
																						"NewBudget__"
																					],
																					"options": {
																						"label": "_NewBudget",
																						"version": 0
																					},
																					"stamp": 190
																				},
																				"NewBudget__": {
																					"type": "Decimal",
																					"data": [
																						"NewBudget__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_NewBudget",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 191
																				},
																				"LabelAvailableBeforeRevision__": {
																					"type": "Label",
																					"data": [
																						"AvailableBeforeRevision__"
																					],
																					"options": {
																						"label": "_AvailableBeforeRevision",
																						"version": 0
																					},
																					"stamp": 200
																				},
																				"AvailableBeforeRevision__": {
																					"type": "Decimal",
																					"data": [
																						"AvailableBeforeRevision__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_AvailableBeforeRevision",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 201
																				},
																				"LabelAvailableAfterRevision__": {
																					"type": "Label",
																					"data": [
																						"AvailableAfterRevision__"
																					],
																					"options": {
																						"label": "_AvailableAfterRevision",
																						"version": 0
																					},
																					"stamp": 202
																				},
																				"AvailableAfterRevision__": {
																					"type": "Decimal",
																					"data": [
																						"AvailableAfterRevision__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_AvailableAfterRevision",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true
																					},
																					"stamp": 203
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 212,
													"*": {
														"ClosePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ClosePane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 213,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Close_CompanyCode__": "LabelClose_CompanyCode__",
																			"LabelClose_CompanyCode__": "Close_CompanyCode__",
																			"Close_PeriodCode__": "LabelClose_PeriodCode__",
																			"LabelClose_PeriodCode__": "Close_PeriodCode__",
																			"Close_BudgetLinesImpacted__": "LabelClose_BudgetLinesImpacted__",
																			"LabelClose_BudgetLinesImpacted__": "Close_BudgetLinesImpacted__",
																			"Close_ByCriteria__": "LabelClose_ByCriteria__",
																			"LabelClose_ByCriteria__": "Close_ByCriteria__",
																			"Close_ByBudgetID__": "LabelClose_ByBudgetID__",
																			"LabelClose_ByBudgetID__": "Close_ByBudgetID__",
																			"Select_from_a_combo_box__": "LabelSelect_from_a_combo_box",
																			"LabelSelect_from_a_combo_box": "Select_from_a_combo_box__",
																			"Close_BudgetID__": "LabelClose_BudgetID__",
																			"LabelClose_BudgetID__": "Close_BudgetID__"
																		},
																		"version": 0
																	},
																	"stamp": 214,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line13__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line18__": {
																						"line": 11,
																						"column": 1
																					},
																					"Spacer_line19__": {
																						"line": 3,
																						"column": 1
																					},
																					"Close_CompanyCode__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelClose_CompanyCode__": {
																						"line": 8,
																						"column": 1
																					},
																					"Close_PeriodCode__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelClose_PeriodCode__": {
																						"line": 9,
																						"column": 1
																					},
																					"Close_BudgetLinesImpacted__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelClose_BudgetLinesImpacted__": {
																						"line": 13,
																						"column": 1
																					},
																					"Close_Description__": {
																						"line": 2,
																						"column": 1
																					},
																					"Close_ByCriteria__": {
																						"line": 4,
																						"column": 1
																					},
																					"Close_ByBudgetID__": {
																						"line": 6,
																						"column": 1
																					},
																					"Close_BudgetID__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelClose_BudgetID__": {
																						"line": 10,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line20__": {
																						"line": 7,
																						"column": 1
																					},
																					"SpacerCloseAfterBudgetID__": {
																						"line": 12,
																						"column": 1
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 215,
																			"*": {
																				"Spacer_line13__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line13",
																						"version": 0
																					},
																					"stamp": 216
																				},
																				"Close_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 50,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_Close_Description",
																						"version": 0
																					},
																					"stamp": 225
																				},
																				"Spacer_line19__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "30",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line19",
																						"version": 0
																					},
																					"stamp": 224
																				},
																				"Close_ByCriteria__": {
																					"type": "RadioButton",
																					"data": [
																						"Close_ByCriteria__"
																					],
																					"options": {
																						"keys": [
																							"_Close_ByCriteria"
																						],
																						"values": [
																							"_Close_ByCriteria"
																						],
																						"label": "_Close_ByCriteria",
																						"activable": true,
																						"width": "",
																						"marge": 50,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 230
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7"
																					},
																					"stamp": 238
																				},
																				"Close_ByBudgetID__": {
																					"type": "RadioButton",
																					"data": [
																						"Close_ByBudgetID__"
																					],
																					"options": {
																						"keys": [
																							"_Close_ByBudgetID"
																						],
																						"values": [
																							"_Close_ByBudgetID"
																						],
																						"label": "_Close_ByBudgetID",
																						"activable": true,
																						"width": "",
																						"marge": 50,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 232
																				},
																				"Spacer_line20__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line20"
																					},
																					"stamp": 239
																				},
																				"LabelClose_CompanyCode__": {
																					"type": "Label",
																					"data": [
																						"Close_CompanyCode__"
																					],
																					"options": {
																						"label": "_Close_CompanyCode",
																						"version": 0
																					},
																					"stamp": 217
																				},
																				"Close_CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Close_CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Close_CompanyCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Close_Help_Select a company code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true
																					},
																					"stamp": 218
																				},
																				"LabelClose_PeriodCode__": {
																					"type": "Label",
																					"data": [
																						"Close_PeriodCode__"
																					],
																					"options": {
																						"label": "_Close_PeriodCode",
																						"version": 0
																					},
																					"stamp": 219
																				},
																				"Close_PeriodCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Close_PeriodCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Close_PeriodCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Close_Help_Select a period code",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true
																					},
																					"stamp": 220
																				},
																				"LabelClose_BudgetID__": {
																					"type": "Label",
																					"data": [
																						"Close_BudgetID__"
																					],
																					"options": {
																						"label": "_Close_BudgetID"
																					},
																					"stamp": 235
																				},
																				"Close_BudgetID__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"Close_BudgetID__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Close_BudgetID",
																						"activable": true,
																						"width": "250",
																						"helpText": "_Close_Help_Select a budget ID",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 236
																				},
																				"Spacer_line18__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line18",
																						"version": 0
																					},
																					"stamp": 221
																				},
																				"SpacerCloseAfterBudgetID__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "34",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line21"
																					},
																					"stamp": 240
																				},
																				"LabelClose_BudgetLinesImpacted__": {
																					"type": "Label",
																					"data": [
																						"Close_BudgetLinesImpacted__"
																					],
																					"options": {
																						"label": "_BudgetLinesClosed",
																						"version": 0
																					},
																					"stamp": 222
																				},
																				"Close_BudgetLinesImpacted__": {
																					"type": "Integer",
																					"data": [
																						"Close_BudgetLinesImpacted__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_BudgetLinesClosed",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 223
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 99
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 30,
							"data": []
						},
						"Previous": {
							"type": "SubmitButton",
							"options": {
								"label": "_Previous",
								"action": "none",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 26,
							"data": []
						},
						"Next": {
							"type": "SubmitButton",
							"options": {
								"label": "_Next",
								"action": "none",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"style": 1
							},
							"stamp": 27,
							"data": []
						},
						"Run": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Run",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 114
						}
					},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 240,
	"data": []
}