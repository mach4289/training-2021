var CustomScript;
(function (CustomScript) {
    ///#GLOBALS Lib
    /* Expense HTML page script */
    /** **************** **/
    /** Global variables **/
    /** **************** **/
    var OriginalOwner;
    var layout = {
        panes: ["Banner", "ExpenseInformation", "DocumentsPanel", "PreviewPanel", "TopPaneWarning"].map(function (name) { return Controls[name]; }),
        actionButtons: ["Save", "DeleteExpense", "Close"].map(function (name) { return Controls[name]; }),
        taxFields: [
            {
                taxCode: Controls["TaxCode1__"],
                taxRate: Controls["TaxRate1__"],
                taxAmount: Controls["TaxAmount1__"]
            },
            {
                taxCode: Controls["TaxCode2__"],
                taxRate: Controls["TaxRate2__"],
                taxAmount: Controls["TaxAmount2__"]
            },
            {
                taxCode: Controls["TaxCode3__"],
                taxRate: Controls["TaxRate3__"],
                taxAmount: Controls["TaxAmount3__"]
            },
            {
                taxCode: Controls["TaxCode4__"],
                taxRate: Controls["TaxRate4__"],
                taxAmount: Controls["TaxAmount4__"]
            },
            {
                taxCode: Controls["TaxCode5__"],
                taxRate: Controls["TaxRate5__"],
                taxAmount: Controls["TaxAmount5__"]
            }
        ],
        HideWaitScreen: function (hide) {
            // async call just after boot
            setTimeout(function () {
                Controls.Date__.Wait(!hide);
            });
        }
    };
    /** ********* **/
    /** FUNCTIONS **/
    /** ********* **/
    function DispayTaxes() {
        layout.taxFields.forEach(function (fields) {
            if (Controls.InputTaxRate__.GetValue() && !Sys.Helpers.IsEmpty(fields.taxCode.GetValue()) && fields.taxRate.GetValue() != 0) {
                fields.taxAmount.Hide(false);
                fields.taxAmount.SetLabel(Language.Translate("_TaxAmount", false, fields.taxRate.GetValue()));
            }
            else {
                fields.taxAmount.Hide(true);
            }
        });
    }
    function CheckValidityTotalAmount(itemChanged) {
        var fieldsClearError = [Controls.TaxAmount1__, Controls.TaxAmount2__, Controls.TaxAmount3__, Controls.TaxAmount4__, Controls.TaxAmount5__];
        // Reset all errors
        fieldsClearError.forEach(function (field) {
            field.SetError("");
        });
        if (!Sys.Helpers.IsEmpty(Controls.TotalAmount__.GetValue()) && Data.GetValue("InputTaxRate__") === true) {
            var taxLines = [];
            for (var i = 1; i <= 5; i++) {
                if (!Sys.Helpers.IsEmpty(Data.GetValue("TaxCode" + i + "__"))) {
                    var taxAmount = Data.GetValue("TaxAmount" + i + "__");
                    var taxRate = Data.GetValue("TaxRate" + i + "__");
                    taxLines.push({
                        taxAmount: taxAmount ? taxAmount : 0,
                        taxCode: Data.GetValue("TaxCode" + i + "__"),
                        taxRate: taxRate ? taxRate : 0
                    });
                }
            }
            Controls.TotalAmount__.SetError("");
            var computerError = Lib.Expense.Report.TaxesComputations.ComputeNetAmounts(taxLines, (Controls.TotalAmount__.GetValue() || 0));
            if (computerError === Lib.Expense.Report.TaxesComputations.BalanceNetAmountsErrorCode.TaxAmountTooBig) {
                if (itemChanged && !Sys.Helpers.IsEmpty(itemChanged.GetValue())) {
                    itemChanged.SetError("_Error invalid tax amount");
                }
                else {
                    Controls.TotalAmount__.SetError("_Error invalid tax amount");
                }
            }
        }
    }
    function SetErrorExchangeRate() {
        if (Controls.ExchangeRate__.GetValue() <= 0) {
            Controls.ExchangeRate__.SetError("_This field should be greater than zero");
            return true;
        }
        return false;
    }
    function UpdateDisplayFieldsForMileage() {
        if (Controls.ExpenseType__.GetValue() === "Mileage") {
            Controls.ExchangeRate__.SetValue(1);
            Controls.TotalAmountCurrency__.SetValue(Controls.LocalCurrency__.GetValue());
            Lib.Expense.UpdateControl(["ExchangeRate__"]);
        }
    }
    function InitLayoutBeforeStarting() {
        Log.Info("InitLayoutBeforeStarting");
        Process.ShowFirstErrorAfterBoot(false);
        layout.panes.forEach(function (pane) {
            pane.Hide(true);
        });
        layout.actionButtons.forEach(function (button) {
            button.Hide(true);
        });
        Controls.TechnicalFieldsPanel.Hide(true);
        layout.HideWaitScreen(false);
    }
    function InitLayout() {
        Log.Info("InitLayout");
        layout.panes.forEach(function (pane) {
            pane.Hide(false);
        });
        layout.actionButtons.forEach(function (button) {
            button.Hide(false);
        });
        layout.HideWaitScreen(true);
        InitBanner();
        InitInformationPanel();
        InitButtonsBar();
        InitWarningBanner();
        var func = Sys.Helpers.TryGetFunction("Lib.Expense.Customization.Client.CustomizeLayout");
        if (func) {
            func();
        }
        else {
            Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Client.CustomiseLayout");
        }
        TemplateManager.OnInitLayout();
        MileageManager.OnInitLayout();
        Lib.Expense.LayoutManager.SetRequiredFields(true);
        Lib.Expense.ControlsWatcher.CheckAll();
        Process.SetHelpId("5012");
        Process.ShowFirstError();
        var initCheckValidityTotalAmount = !Sys.Helpers.IsEmpty(Controls.TotalAmount__.GetValue());
        for (var i = 1; i <= 5; i++) {
            initCheckValidityTotalAmount = !Sys.Helpers.IsEmpty(Data.GetValue("TaxAmount" + i + "__")) || initCheckValidityTotalAmount;
        }
        if (initCheckValidityTotalAmount) {
            CheckValidityTotalAmount(null);
        }
    }
    function InitWarningBanner() {
        var topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
        Log.Info("InitWarningBanner");
        Lib.P2P.DisplayOnBehalfWarning(User.loginId, function (nameToDisplay, isCreatedBy) {
            topMessageWarning.Clear();
            // Special message for CreateFor in draft status
            if (Data.GetValue("ExpenseStatus__") === "Draft" && !isCreatedBy) {
                topMessageWarning.Add(Language.Translate("_ExpenseCreateFor", true, nameToDisplay));
            }
            else if (isCreatedBy) {
                topMessageWarning.Add(Language.Translate("_ExpenseCreatedBy", true, nameToDisplay));
            }
            else {
                topMessageWarning.Add(Language.Translate("_ExpenseCreatedFor", true, nameToDisplay));
            }
        });
    }
    function InitBanner() {
        Log.Info("InitBanner");
        Sys.Helpers.Banner.SetStatusCombo(Controls.ExpenseStatus__);
        Sys.Helpers.Banner.SetHTMLBanner(Controls.HTMLBanner__);
        var status = Data.GetValue("ExpenseStatus__");
        Sys.Helpers.Banner.SetMainTitle("_Expense");
        Sys.Helpers.Banner.SetSubTitle("_Banner expense " + status.toLowerCase());
    }
    function InitInformationPanel() {
        Log.Info("InitInformationPanel");
        var status = Data.GetValue("ExpenseStatus__");
        if (Lib.Expense.IsReadOnly() || status === "To approve" || status === "Pending delete") {
            Controls.ExpenseInformation.SetReadOnly(true);
        }
        Controls.ExpenseNumber__.Hide(!Data.GetValue("ExpenseNumber__"));
        Controls.CompanyCode__.Hide(!Controls.CompanyCode__.GetError());
        Controls.ReimbursableLocalAmount__.Hide(true);
        Controls.ReimbursableLocalAmount__.SetReadOnly(true);
        Controls.NonReimbursableLocalAmount__.Hide(true);
        Controls.NonReimbursableLocalAmount__.SetReadOnly(true);
        CheckCurrencyCompanyCode(Controls.CompanyCode__.GetValue(), Sys.Helpers.IsEmpty(Controls.ExchangeRate__.GetValue()));
        SetErrorExchangeRate();
        if (!Data.GetValue("Date__")) {
            Data.SetValue("Date__", new Date());
        }
        if (!Data.GetValue("OwnerName__")) {
            Data.SetValue("OwnerName__", User.fullName);
            OriginalOwnerManager.SetPreviousOriginalOwner(User.fullName);
        }
        var isAllowedRes = Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Common.IsAllowedToCreateExpenseOnBehalfOf");
        Sys.Helpers.Promise.Resolve(isAllowedRes).Then(function (res) {
            if (res) {
                Sys.Helpers.Controls.HideForever(Controls.OwnerName__, false);
            }
        });
        if (Data.GetValue("State")) {
            Controls.OwnerName__.SetReadOnly(true);
        }
        DispayTaxes();
    }
    function InitButtonsBar() {
        function OnConfirmDelete() {
            Lib.Expense.LayoutManager.SetRequiredFields(false);
            if (Process.GetURLParameter("onquit") == "Back") {
                ProcessInstance.Approve("DeleteExpense");
            }
            else {
                ProcessInstance.ApproveAsynchronous("DeleteExpense");
            }
        }
        Log.Info("InitButtonsBar");
        if (!ProcessInstance.state) {
            // Not yet saved, no need to show the delete button
            Controls.DeleteExpense.Hide(true);
        }
        var status = Data.GetValue("ExpenseStatus__");
        if (Lib.Expense.IsReadOnly()) {
            Controls.Save.Hide(true);
            Controls.DeleteExpense.Hide(true);
        }
        else if (status === "Draft" || status === "To submit") {
            Controls.Save.OnClick = function () {
                if (Data.GetValue("state") == 90) {
                    ProcessInstance.ResumeWithActionAsynchronous("Save");
                    return false;
                }
                if (!Data.GetValue("ExpenseNumber__")) {
                    Lib.Expense.LayoutManager.SetRequiredFields(false);
                    ProcessInstance.ApproveAsynchronous("Save");
                    return false;
                }
                Lib.Expense.LayoutManager.CleanHiddenFields();
                Lib.Expense.LayoutManager.FillEmptyFields();
                var toSubmit = Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Common.IsToSubmitExpense");
                if (!Sys.Helpers.IsBoolean(toSubmit)) {
                    toSubmit = Lib.Expense.LayoutManager.CheckAllLayoutRequiredFields() && Lib.Expense.LayoutManager.CheckFieldsValidity();
                }
                if (toSubmit && !Process.ShowFirstError()) {
                    Data.SetValue("ExpenseStatus__", "To submit");
                }
                else {
                    Data.SetValue("ExpenseStatus__", "Draft");
                }
                return null;
            };
            Controls.DeleteExpense.OnClick = function () {
                Popup.Confirm("_Delete_Expense_explanation", false, OnConfirmDelete, null, "_Delete expense confirmation");
                return false;
            };
        }
        else {
            Controls.Save.Hide(true);
            Controls.DeleteExpense.Hide(true);
        }
    }
    var TemplateManager = (function () {
        var currentLayout = null;
        var ignoredFields = {};
        var fieldHistoryState = {};
        function IgnoreField(fieldName) {
            ignoredFields[fieldName] = true;
        }
        function IsIgnoredField(fieldName) {
            return fieldName in ignoredFields;
        }
        function RefreshLayout() {
            var setDefaultValueNeeded = false;
            if (currentLayout) {
                currentLayout.fields.forEach(function (field) {
                    if (!IsIgnoredField(field.name)) {
                        var ctrl = Controls[field.name];
                        if (field.documentsPanel) {
                            ctrl.Hide(true);
                            Lib.Expense.LayoutManager.SetProcessedDocumentRequired(field.name, false);
                            Controls.PreviewPanel.Hide(true);
                        }
                        else {
                            ctrl.SetRequired(false);
                            ctrl.SetReadOnly(false);
                            // store previous state
                            if (ctrl.IsVisible()) {
                                fieldHistoryState[field.name] = { value: ctrl.GetValue() };
                            }
                            ctrl.Hide(true);
                            ctrl.SetInfo("");
                            ctrl.SetWarning("");
                            ctrl.SetError("");
                        }
                    }
                });
                currentLayout = null;
                setDefaultValueNeeded = true;
            }
            currentLayout = Lib.Expense.LayoutManager.GetLayout();
            currentLayout.fields.forEach(function (field) {
                if (!IsIgnoredField(field.name)) {
                    var ctrl = Controls[field.name];
                    var hide = field.hidden || (!!field.visibilityCondition && !field.visibilityCondition());
                    ctrl.Hide(hide);
                    if (field.documentsPanel) {
                        Lib.Expense.LayoutManager.SetProcessedDocumentRequired(field.name, field.required);
                        Controls.PreviewPanel.Hide(hide);
                    }
                    else {
                        ctrl.SetRequired(field.required);
                        ctrl.SetReadOnly(field.readonly);
                        if (ctrl.IsVisible() && (field.name in fieldHistoryState)) {
                            // restore value before we hide this control
                            ctrl.SetValue(fieldHistoryState[field.name].value);
                            delete fieldHistoryState[field.name];
                        }
                        if (Sys.Helpers.IsDefined(field.defaultValue) && setDefaultValueNeeded) {
                            ctrl.SetValue(field.defaultValue);
                        }
                    }
                }
            });
        }
        function OnInitLayout() {
            Log.Info("TemplateManager.OnInitLayout");
            if (Controls.CompanyCode__.GetError()) {
                IgnoreField("CompanyCode__");
                // Show company code when this field is in error
                Controls.CompanyCode__.SetReadOnly(false);
                Controls.CompanyCode__.Hide(false);
            }
            Controls.ExpenseType__.OnChange = Sys.Helpers.Wrap(Controls.ExpenseType__.OnChange, function (originalFn) {
                originalFn.apply(this, Array.prototype.slice.call(arguments, 1));
                RefreshLayout();
                CheckValidityTotalAmount(null);
                UpdateDisplayFieldsForMileage();
                Lib.Expense.UpdateControl(["ExpenseType__"]);
            });
            // Hide all fields before except if error
            Lib.Expense.LayoutManager.GetFieldNames().forEach(function (fieldName) {
                var ctrl = Controls[fieldName];
                ctrl.Hide(!ctrl.GetError());
            });
            RefreshLayout();
        }
        return {
            OnInitLayout: OnInitLayout,
            IgnoreField: IgnoreField,
            IsIgnoredField: IsIgnoredField
        };
    })();
    var OriginalOwnerManager = {
        previousOriginalOwner: "",
        UpdateOriginalOwnerDependencies: function () {
            ProcessInstance.CreateOnBehalfOf(OriginalOwner);
            Object.keys(Controls).forEach(function (control) {
                if (control.endsWith("__") && control != "OwnerName__" && control != "ExpenseStatus__") {
                    Controls[control].SetValue("");
                }
            });
            InitForm();
            OriginalOwnerManager.SetPreviousOriginalOwner(Controls.OwnerName__.GetValue());
            InitWarningBanner();
        },
        RevertOriginalOwner: function () {
            Controls.OwnerName__.SetValue(OriginalOwnerManager.previousOriginalOwner);
            InitWarningBanner();
        },
        SetPreviousOriginalOwner: function (OriginalOwnerId) {
            OriginalOwnerManager.previousOriginalOwner = OriginalOwnerId;
        }
    };
    var MileageManager = (function () {
        function ComputeMileage() {
            if (Controls.Template__.GetValue() === "Distance") {
                var distance = Controls.Distance__.GetValue();
                var rate = Controls.MileageRate1__.GetValue();
                if (distance && rate) {
                    var total = (new Sys.Decimal(distance)).mul(rate);
                    total = Sys.Helpers.Round(total, 2);
                    Controls.TotalAmount__.SetValue(total.toNumber());
                }
                else {
                    Controls.TotalAmount__.SetValue(0);
                }
            }
            Lib.Expense.UpdateControl(["TotalAmount__"]);
        }
        // Called when VehicleType, CompanyCode and ExpenseDate change
        function UpdateMileageFields(noVehicleType) {
            if (Controls.Template__.GetValue() === "Distance") {
                var vehicleType = noVehicleType ? "" : Controls.VehicleTypeName__.GetValue();
                var companyCode = Controls.CompanyCode__.GetValue();
                var expenseDate = Controls.Date__.GetValue();
                // Reset depending fields when missing info
                if (expenseDate && vehicleType && companyCode) {
                    Lib.Expense.SaveButtonDisabler.SetDisabled(true);
                    Lib.Expense.GetMileageInfo(vehicleType, companyCode, expenseDate)
                        .Then(function (info) {
                        Controls.VehicleTypeName__.SetError("");
                        Lib.Expense.SaveButtonDisabler.SetDisabled(false);
                        Controls.VehicleTypeID__.SetValue(info["ID__"]);
                        Controls.MileageRate1__.SetValue(info["REIMBURSMENTRATE1__"]);
                        ComputeMileage();
                    })
                        .Catch(function (error) {
                        Log.Error("UpdateMileageFields failed: " + error);
                        Controls.VehicleTypeName__.SetError("_No Mileage record found");
                        Lib.Expense.SaveButtonDisabler.SetDisabled(false);
                        Controls.MileageRate1__.SetValue(0);
                        Controls.VehicleTypeID__.SetValue("");
                        ComputeMileage();
                    });
                }
                else {
                    Controls.MileageRate1__.SetValue(0);
                    Controls.VehicleTypeID__.SetValue("");
                    ComputeMileage();
                }
            }
        }
        function DoQuery(callback, Table, Attributes, LdapFilter, SortOrder, MaxRecords, option) {
            Query.DBQuery(function () {
                callback(this.GetQueryValue());
            }, Table, Attributes, LdapFilter, SortOrder, "NO_LIMIT", null, "distinct=1");
        }
        function OnInitLayout() {
            Log.Info("MileageManager.OnInitLayout");
            Controls.Date__.OnChange = Sys.Helpers.Wrap(Controls.Date__.OnChange, function (originalFn) {
                originalFn.apply(this, Array.prototype.slice.call(arguments, 1));
                UpdateMileageFields();
                // For date-dependent custom exchange rate
                if (Controls.Date__.GetValue() && Controls.LocalCurrency__.GetValue() !== Controls.TotalAmountCurrency__.GetValue()) {
                    Lib.Expense.CheckCustomExchangeRate();
                }
            });
            Controls.Distance__.OnChange = Sys.Helpers.Wrap(Controls.Distance__.OnChange, function (originalFn) {
                originalFn.apply(this, Array.prototype.slice.call(arguments, 1));
                ComputeMileage();
            });
            Controls.CompanyCode__.OnChange = Sys.Helpers.Wrap(Controls.CompanyCode__.OnChange, function (originalFn) {
                originalFn.apply(this, Array.prototype.slice.call(arguments, 1));
                // Reset vehicleType because is invalid for the new company code
                Controls.VehicleTypeName__.SetValue("");
                UpdateMileageFields();
            });
            Controls.VehicleTypeName__.SetCustomQuery(DoQuery, null, DoQuery, null);
            Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.VehicleTypeName__, function () { return UpdateMileageFields(); }, function () { return UpdateMileageFields(true); }, function () { return UpdateMileageFields(true); });
        }
        return {
            OnInitLayout: OnInitLayout
        };
    })();
    function OriginalOwnerChange() {
        setTimeout(function () {
            Popup.Confirm("_This action will delete completed fields", false, OriginalOwnerManager.UpdateOriginalOwnerDependencies, OriginalOwnerManager.RevertOriginalOwner, "_Warning");
        });
    }
    /** ***** **/
    /** EVENT **/
    /** ***** **/
    function CheckCurrencyCompanyCode(companyCode, refreshCurrency) {
        if (refreshCurrency === void 0) { refreshCurrency = false; }
        Lib.P2P.CompanyCodesValue.QueryValues(companyCode, true)
            .Then(function (CCValues) {
            if (!Sys.Helpers.IsNumeric(CCValues.currencies.GetRate(Controls.TotalAmountCurrency__.GetValue()))) {
                Controls.TotalAmountCurrency__.SetError("_This currency is not defined for the company code '{0}'", Controls.CompanyCode__.GetValue());
                TemplateManager.IgnoreField("TotalAmountCurrency__");
                Controls.TotalAmountCurrency__.SetReadOnly(false);
                Controls.TotalAmountCurrency__.Hide(false);
            }
            else {
                // START - ignore all changes
                ProcessInstance.SetSilentChange(true);
                Controls.TotalAmountCurrency__.SetError("");
                Controls.LocalCurrency__.SetValue(CCValues.Currency__);
                // END - ignore all changes
                ProcessInstance.SetSilentChange(false);
            }
            if (refreshCurrency) {
                if (CCValues.Currency__ !== Controls.TotalAmountCurrency__.GetValue()) {
                    Controls.ExchangeRate__.Hide(false);
                    if (!Lib.Expense.CheckCustomExchangeRate()) {
                        Controls.ExchangeRate__.SetValue(CCValues.currencies.GetRate(Controls.TotalAmountCurrency__.GetValue()));
                        Controls.ExchangeRate__.SetError("");
                    }
                    Controls.LocalAmount__.Hide(false);
                }
                else {
                    // START - ignore all changes
                    ProcessInstance.SetSilentChange(true);
                    Controls.ExchangeRate__.Hide(true);
                    Controls.ExchangeRate__.SetValue(1);
                    Controls.LocalAmount__.Hide(true);
                    // END - ignore all changes
                    ProcessInstance.SetSilentChange(false);
                }
                Lib.Expense.UpdateControl(["ExchangeRate__"]);
            }
            var currency = CCValues.Currency__;
            var localAmountLabel = Language.Translate("_ExpenseLocalAmount"), reimbursableLocalAmountLabel = Language.Translate("_ReimbursableLocalAmount"), nonReimbursableLocalAmountLabel = Language.Translate("_NonReimbursableLocalAmount");
            if (currency) {
                currency = " (" + currency + ")";
                localAmountLabel += currency;
                reimbursableLocalAmountLabel += currency;
                nonReimbursableLocalAmountLabel += currency;
            }
            Controls.LocalAmount__.SetLabel(localAmountLabel);
            Controls.ReimbursableLocalAmount__.SetLabel(reimbursableLocalAmountLabel);
            Controls.NonReimbursableLocalAmount__.SetLabel(nonReimbursableLocalAmountLabel);
        });
    }
    Controls.CompanyCode__.OnSelectItem = function (item) {
        CheckCurrencyCompanyCode(item.GetValue("CompanyCode__"));
    };
    Controls.TotalAmountCurrency__.OnSelectItem = function (item) {
        CheckCurrencyCompanyCode(Controls.CompanyCode__.GetValue(), true);
    };
    Controls.ExpenseType__.SetAttributes("DefaultGLAccount__|ReceiptBehaviour__|VendorFieldBehaviour__|CostCenterFieldBehaviour__|BillableFieldBehaviour__|Template__|InputTaxRate__|TaxCode1__|TaxRate1__|TaxCode2__|TaxRate2__|TaxCode3__|TaxRate3__|TaxCode4__|TaxRate4__|TaxCode5__|TaxRate5__|CommentTemplate__");
    Controls.ExpenseType__.OnSelectItem = function (item) {
        Controls.GLAccount__.SetValue(item.GetValue("DefaultGLAccount__"));
        Controls.InputTaxRate__.SetValue(item.GetValue("InputTaxRate__"));
        Controls.TaxCode1__.SetValue(item.GetValue("TaxCode1__"));
        Controls.TaxRate1__.SetValue(item.GetValue("TaxRate1__"));
        Controls.TaxCode2__.SetValue(item.GetValue("TaxCode2__"));
        Controls.TaxRate2__.SetValue(item.GetValue("TaxRate2__"));
        Controls.TaxCode3__.SetValue(item.GetValue("TaxCode3__"));
        Controls.TaxRate3__.SetValue(item.GetValue("TaxRate3__"));
        Controls.TaxCode4__.SetValue(item.GetValue("TaxCode4__"));
        Controls.TaxRate4__.SetValue(item.GetValue("TaxRate4__"));
        Controls.TaxCode5__.SetValue(item.GetValue("TaxCode5__"));
        Controls.TaxRate5__.SetValue(item.GetValue("TaxRate5__"));
        Controls.Description__.SetValue(item.GetValue("CommentTemplate__"));
        DispayTaxes();
        Controls.ReceiptBehaviour__.SetValue(item.GetValue("ReceiptBehaviour__"));
        Controls.VendorFieldBehaviour__.SetValue(item.GetValue("VendorFieldBehaviour__"));
        Controls.CostCenterFieldBehaviour__.SetValue(item.GetValue("CostCenterFieldBehaviour__"));
        Controls.BillableFieldBehaviour__.SetValue(item.GetValue("BillableFieldBehaviour__"));
        Controls.Template__.SetValue(item.GetValue("Template__"));
        Lib.Expense.UpdateControl(["ExpenseType__"]);
    };
    Controls.TotalAmount__.OnChange = function (item) {
        Lib.Expense.UpdateControl(["TotalAmount__"]);
        CheckValidityTotalAmount(this);
    };
    Controls.TaxAmount1__.OnChange = function (item) {
        CheckValidityTotalAmount(this);
    };
    Controls.TaxAmount2__.OnChange = function (item) {
        CheckValidityTotalAmount(this);
    };
    Controls.TaxAmount3__.OnChange = function (item) {
        CheckValidityTotalAmount(this);
    };
    Controls.TaxAmount4__.OnChange = function (item) {
        CheckValidityTotalAmount(this);
    };
    Controls.TaxAmount5__.OnChange = function (item) {
        CheckValidityTotalAmount(this);
    };
    Controls.ExchangeRate__.OnChange = function () {
        if (!SetErrorExchangeRate()) {
            Lib.Expense.UpdateControl(["ExchangeRate__"]);
        }
    };
    Controls.LocalAmount__.OnChange = function () {
        if (!Sys.Helpers.IsEmpty(Controls.TotalAmount__.GetValue())) {
            Controls.ExchangeRate__.SetValue(new Sys.Decimal(this.GetValue() || 0).div(Controls.TotalAmount__.GetValue() || 0).toNumber());
            Lib.Expense.UpdateControl(["LocalAmount__"]);
        }
    };
    Controls.Refundable__.OnChange = function () {
        Lib.Expense.UpdateControl(["Refundable__"]);
    };
    Controls.CostCenterName__.OnSelectItem = function (item) {
        Controls.CostCenterId__.SetValue(item.GetValue("CostCenter__"));
    };
    Controls.CostCenterId__.OnSelectItem = function (item) {
        Controls.CostCenterName__.SetValue(item.GetValue("Description__"));
    };
    Controls.OwnerName__.OnSelectItem = function (item) {
        OriginalOwner = item.GetValue("login");
        OriginalOwnerChange();
    };
    Controls.OwnerName__.OnChange = function () {
        if (Sys.Helpers.IsEmpty(Controls.OwnerName__.GetValue())) {
            OriginalOwner = "";
            OriginalOwnerChange();
        }
    };
    /** ******** **/
    /** RUN PART **/
    /** ******** **/
    function Start() {
        InitLayout();
        Lib.CommonDialog.NextAlert.Show({});
    }
    /** *************** **/
    /** BEFORE STARTING **/
    /** *************** **/
    Sys.Helpers.EnableSmartSilentChange();
    var syncBeforeStarting = Sys.Helpers.Synchronizer.Create(function () {
        Start();
        // END - ignore all changes on form during the initialization processing
        ProcessInstance.SetSilentChange(false);
    }, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
    var onLoadPromise = Sys.Helpers.TryCallFunction("Lib.Expense.Customization.Client.OnLoad");
    // !! Register the callbacks only once !!
    syncBeforeStarting.Register(InitLayoutBeforeStarting, null, { async: false });
    // Here we can't register the function returned by LoadUserProperties because
    // we need to change the userLogin dynamically (for ex. with the 'OnBehalf' case).
    syncBeforeStarting.Register(function (token) { return Lib.Expense.LoadUserProperties(OriginalOwner || Data.GetValue("OwnerID") || User.loginId)(token); });
    if (onLoadPromise) {
        syncBeforeStarting.Register(onLoadPromise);
    }
    // this function just restarts the synchronizer. No need to re-register callbacks
    // (to avoid repeating the same stuff twice)
    function InitForm() {
        syncBeforeStarting.Start();
    }
    // START - ignore all changes on form during the initialization processing
    ProcessInstance.SetSilentChange(true);
    InitForm();
})(CustomScript || (CustomScript = {}));
