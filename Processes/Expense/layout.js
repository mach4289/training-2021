{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1700,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"height": "25%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "55%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning"
											},
											"*": {
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 5,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 140,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 129,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ShortText__": "Label_ShortText",
																			"Label_ShortText": "ShortText__"
																		},
																		"version": 0
																	},
																	"stamp": 130,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 131,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"iconClass": "",
																						"label": "_TopMessageWarning",
																						"version": 0
																					},
																					"stamp": 149
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 7,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 8,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 9,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 10,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner__",
																						"width": "100%",
																						"version": 0,
																						"htmlContent": "Expense"
																					},
																					"stamp": 11
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 12,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "Exp_system_data.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 13,
															"data": []
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"ExpenseInformation": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"iconURL": "Exp_information.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ExpenseInformation",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"labelLength": 0,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"Date__": "LabelDate",
																			"LabelDate": "Date__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"LabelTotalAmount__": "TotalAmount__",
																			"TotalAmount__": "LabelTotalAmount__",
																			"LabelTotalAmountCurrency__": "TotalAmountCurrency__",
																			"TotalAmountCurrency__": "LabelTotalAmountCurrency__",
																			"ExpenseNumber__": "LabelExpenseNumber__",
																			"LabelExpenseNumber__": "ExpenseNumber__",
																			"Description__": "LabelDescription__",
																			"LabelDescription__": "Description__",
																			"Vendor__": "LabelVendor__",
																			"LabelVendor__": "Vendor__",
																			"ExpenseReportMsnEx__": "LabelExpenseReportMsnEx__",
																			"LabelExpenseReportMsnEx__": "ExpenseReportMsnEx__",
																			"ExpenseType__": "LabelExpenseType__",
																			"LabelExpenseType__": "ExpenseType__",
																			"GLAccount__": "LabelGLAccount__",
																			"LabelGLAccount__": "GLAccount__",
																			"TaxCode1__": "LabelTaxCode1__",
																			"LabelTaxCode1__": "TaxCode1__",
																			"TaxRate1__": "LabelTaxRate1__",
																			"LabelTaxRate1__": "TaxRate1__",
																			"TaxAmount1__": "LabelTaxAmount1__",
																			"LabelTaxAmount1__": "TaxAmount1__",
																			"TaxCode2__": "LabelTaxCode2__",
																			"LabelTaxCode2__": "TaxCode2__",
																			"TaxRate2__": "LabelTaxRate2__",
																			"LabelTaxRate2__": "TaxRate2__",
																			"TaxAmount2__": "LabelTaxAmount2__",
																			"LabelTaxAmount2__": "TaxAmount2__",
																			"TaxCode3__": "LabelTaxCode3__",
																			"LabelTaxCode3__": "TaxCode3__",
																			"TaxRate3__": "LabelTaxRate3__",
																			"LabelTaxRate3__": "TaxRate3__",
																			"TaxAmount3__": "LabelTaxAmount3__",
																			"LabelTaxAmount3__": "TaxAmount3__",
																			"TaxCode4__": "LabelTaxCode4__",
																			"LabelTaxCode4__": "TaxCode4__",
																			"TaxRate4__": "LabelTaxRate4__",
																			"LabelTaxRate4__": "TaxRate4__",
																			"TaxAmount4__": "LabelTaxAmount4__",
																			"LabelTaxAmount4__": "TaxAmount4__",
																			"TaxCode5__": "LabelTaxCode5__",
																			"LabelTaxCode5__": "TaxCode5__",
																			"TaxAmount5__": "LabelTaxAmount5__",
																			"LabelTaxAmount5__": "TaxAmount5__",
																			"TaxRate5__": "LabelTaxRate5__",
																			"LabelTaxRate5__": "TaxRate5__",
																			"InputTaxRate__": "LabelInputTaxRate__",
																			"LabelInputTaxRate__": "InputTaxRate__",
																			"Refundable__": "LabelRefundable__",
																			"LabelRefundable__": "Refundable__",
																			"VendorFieldBehaviour__": "LabelVendorFieldBehaviour__",
																			"LabelVendorFieldBehaviour__": "VendorFieldBehaviour__",
																			"Distance__": "LabelDistance__",
																			"LabelDistance__": "Distance__",
																			"Template__": "LabelTemplate__",
																			"LabelTemplate__": "Template__",
																			"VehicleTypeID__": "LabelVehicleTypeID__",
																			"LabelVehicleTypeID__": "VehicleTypeID__",
																			"MileageRate1__": "LabelMileageRate1__",
																			"LabelMileageRate1__": "MileageRate1__",
																			"VehicleTypeName__": "LabelVehicleTypeName__",
																			"LabelVehicleTypeName__": "VehicleTypeName__",
																			"From__": "LabelFrom__",
																			"LabelFrom__": "From__",
																			"To__": "LabelTo__",
																			"LabelTo__": "To__",
																			"Billable__": "LabelBillable__",
																			"LabelBillable__": "Billable__",
																			"CostCenterId__": "LabelCostCenterId__",
																			"LabelCostCenterId__": "CostCenterId__",
																			"CostCenterName__": "LabelCostCenterName__",
																			"LabelCostCenterName__": "CostCenterName__",
																			"ExchangeRate__": "LabelExchangeRate__",
																			"LabelExchangeRate__": "ExchangeRate__",
																			"LocalAmount__": "LabelLocalAmount__",
																			"LabelLocalAmount__": "LocalAmount__",
																			"OwnerName__": "LabelOwnerName__",
																			"LabelOwnerName__": "OwnerName__",
																			"ReimbursableLocalAmount__": "LabelReimbursableLocalAmount__",
																			"LabelReimbursableLocalAmount__": "ReimbursableLocalAmount__",
																			"NonReimbursableLocalAmount__": "LabelNonReimbursableLocalAmount__",
																			"LabelNonReimbursableLocalAmount__": "NonReimbursableLocalAmount__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 42,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"LabelDate": {
																						"line": 5,
																						"column": 1
																					},
																					"Date__": {
																						"line": 5,
																						"column": 2
																					},
																					"CompanyCode__": {
																						"line": 37,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 37,
																						"column": 1
																					},
																					"LabelTotalAmount__": {
																						"line": 12,
																						"column": 1
																					},
																					"TotalAmount__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelTotalAmountCurrency__": {
																						"line": 13,
																						"column": 1
																					},
																					"TotalAmountCurrency__": {
																						"line": 13,
																						"column": 2
																					},
																					"ExpenseNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelExpenseNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"Description__": {
																						"line": 41,
																						"column": 2
																					},
																					"LabelDescription__": {
																						"line": 41,
																						"column": 1
																					},
																					"Vendor__": {
																						"line": 32,
																						"column": 2
																					},
																					"LabelVendor__": {
																						"line": 32,
																						"column": 1
																					},
																					"ExpenseReportMsnEx__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelExpenseReportMsnEx__": {
																						"line": 2,
																						"column": 1
																					},
																					"ExpenseType__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelExpenseType__": {
																						"line": 4,
																						"column": 1
																					},
																					"GLAccount__": {
																						"line": 40,
																						"column": 2
																					},
																					"LabelGLAccount__": {
																						"line": 40,
																						"column": 1
																					},
																					"TaxCode1__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelTaxCode1__": {
																						"line": 17,
																						"column": 1
																					},
																					"TaxRate1__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelTaxRate1__": {
																						"line": 18,
																						"column": 1
																					},
																					"TaxAmount1__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelTaxAmount1__": {
																						"line": 19,
																						"column": 1
																					},
																					"TaxCode2__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelTaxCode2__": {
																						"line": 20,
																						"column": 1
																					},
																					"TaxRate2__": {
																						"line": 21,
																						"column": 2
																					},
																					"LabelTaxRate2__": {
																						"line": 21,
																						"column": 1
																					},
																					"TaxAmount2__": {
																						"line": 22,
																						"column": 2
																					},
																					"LabelTaxAmount2__": {
																						"line": 22,
																						"column": 1
																					},
																					"TaxCode3__": {
																						"line": 23,
																						"column": 2
																					},
																					"LabelTaxCode3__": {
																						"line": 23,
																						"column": 1
																					},
																					"TaxRate3__": {
																						"line": 24,
																						"column": 2
																					},
																					"LabelTaxRate3__": {
																						"line": 24,
																						"column": 1
																					},
																					"TaxAmount3__": {
																						"line": 25,
																						"column": 2
																					},
																					"LabelTaxAmount3__": {
																						"line": 25,
																						"column": 1
																					},
																					"TaxCode4__": {
																						"line": 26,
																						"column": 2
																					},
																					"LabelTaxCode4__": {
																						"line": 26,
																						"column": 1
																					},
																					"TaxRate4__": {
																						"line": 27,
																						"column": 2
																					},
																					"LabelTaxRate4__": {
																						"line": 27,
																						"column": 1
																					},
																					"TaxAmount4__": {
																						"line": 28,
																						"column": 2
																					},
																					"LabelTaxAmount4__": {
																						"line": 28,
																						"column": 1
																					},
																					"TaxCode5__": {
																						"line": 29,
																						"column": 2
																					},
																					"LabelTaxCode5__": {
																						"line": 29,
																						"column": 1
																					},
																					"TaxAmount5__": {
																						"line": 31,
																						"column": 2
																					},
																					"LabelTaxAmount5__": {
																						"line": 31,
																						"column": 1
																					},
																					"TaxRate5__": {
																						"line": 30,
																						"column": 2
																					},
																					"LabelTaxRate5__": {
																						"line": 30,
																						"column": 1
																					},
																					"InputTaxRate__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelInputTaxRate__": {
																						"line": 16,
																						"column": 1
																					},
																					"Refundable__": {
																						"line": 33,
																						"column": 2
																					},
																					"LabelRefundable__": {
																						"line": 33,
																						"column": 1
																					},
																					"Distance__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelDistance__": {
																						"line": 8,
																						"column": 1
																					},
																					"VehicleTypeID__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelVehicleTypeID__": {
																						"line": 10,
																						"column": 1
																					},
																					"VehicleTypeName__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelVehicleTypeName__": {
																						"line": 9,
																						"column": 1
																					},
																					"MileageRate1__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelMileageRate1__": {
																						"line": 11,
																						"column": 1
																					},
																					"From__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelFrom__": {
																						"line": 6,
																						"column": 1
																					},
																					"To__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelTo__": {
																						"line": 7,
																						"column": 1
																					},
																					"Billable__": {
																						"line": 36,
																						"column": 2
																					},
																					"LabelBillable__": {
																						"line": 36,
																						"column": 1
																					},
																					"CostCenterId__": {
																						"line": 39,
																						"column": 2
																					},
																					"LabelCostCenterId__": {
																						"line": 39,
																						"column": 1
																					},
																					"CostCenterName__": {
																						"line": 38,
																						"column": 2
																					},
																					"LabelCostCenterName__": {
																						"line": 38,
																						"column": 1
																					},
																					"ExchangeRate__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelExchangeRate__": {
																						"line": 14,
																						"column": 1
																					},
																					"LocalAmount__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelLocalAmount__": {
																						"line": 15,
																						"column": 1
																					},
																					"OwnerName__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelOwnerName__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 42,
																						"column": 1
																					},
																					"ReimbursableLocalAmount__": {
																						"line": 34,
																						"column": 2
																					},
																					"LabelReimbursableLocalAmount__": {
																						"line": 34,
																						"column": 1
																					},
																					"NonReimbursableLocalAmount__": {
																						"line": 35,
																						"column": 2
																					},
																					"LabelNonReimbursableLocalAmount__": {
																						"line": 35,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelExpenseReportMsnEx__": {
																					"type": "Label",
																					"data": [
																						"ExpenseReportMsnEx__"
																					],
																					"options": {
																						"label": "_ExpenseReportMsnEx",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 15
																				},
																				"ExpenseReportMsnEx__": {
																					"type": "ShortText",
																					"data": [
																						"ExpenseReportMsnEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseReportMsnEx",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 16
																				},
																				"LabelExpenseType__": {
																					"type": "Label",
																					"data": [
																						"ExpenseType__"
																					],
																					"options": {
																						"label": "_ExpenseType",
																						"version": 0
																					},
																					"stamp": 17
																				},
																				"ExpenseType__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ExpenseType__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_ExpenseType",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 18
																				},
																				"LabelOwnerName__": {
																					"type": "Label",
																					"data": [
																						"OwnerName__"
																					],
																					"options": {
																						"label": "_OwnerName",
																						"version": 0
																					},
																					"stamp": 19
																				},
																				"OwnerName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"OwnerName__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"label": "_OwnerName",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"hidden": true
																					},
																					"stamp": 20
																				},
																				"LabelTotalAmountCurrency__": {
																					"type": "Label",
																					"data": [
																						"TotalAmountCurrency__"
																					],
																					"options": {
																						"label": "_TotalAmountCurrency",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"TotalAmountCurrency__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"TotalAmountCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_TotalAmountCurrency",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 22
																				},
																				"LabelExpenseNumber__": {
																					"type": "Label",
																					"data": [
																						"ExpenseNumber__"
																					],
																					"options": {
																						"label": "_ExpenseNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 23
																				},
																				"ExpenseNumber__": {
																					"type": "ShortText",
																					"data": [
																						"ExpenseNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 24
																				},
																				"LabelFrom__": {
																					"type": "Label",
																					"data": [
																						"From__"
																					],
																					"options": {
																						"label": "_From",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 25
																				},
																				"From__": {
																					"type": "ShortText",
																					"data": [
																						"From__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_From",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": true,
																						"hidden": true
																					},
																					"stamp": 26
																				},
																				"LabelTo__": {
																					"type": "Label",
																					"data": [
																						"To__"
																					],
																					"options": {
																						"label": "_To",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 27
																				},
																				"To__": {
																					"type": "ShortText",
																					"data": [
																						"To__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_To",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": true,
																						"hidden": true
																					},
																					"stamp": 28
																				},
																				"LabelDistance__": {
																					"type": "Label",
																					"data": [
																						"Distance__"
																					],
																					"options": {
																						"label": "_Distance",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 29
																				},
																				"Distance__": {
																					"type": "Decimal",
																					"data": [
																						"Distance__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Distance",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 30
																				},
																				"LabelVehicleTypeName__": {
																					"type": "Label",
																					"data": [
																						"VehicleTypeName__"
																					],
																					"options": {
																						"label": "_VehicleTypeName",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 31
																				},
																				"VehicleTypeName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VehicleTypeName__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_VehicleTypeName",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 32
																				},
																				"LabelVehicleTypeID__": {
																					"type": "Label",
																					"data": [
																						"VehicleTypeID__"
																					],
																					"options": {
																						"label": "_VehicleTypeID",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 33
																				},
																				"VehicleTypeID__": {
																					"type": "ShortText",
																					"data": [
																						"VehicleTypeID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VehicleTypeID",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 34
																				},
																				"LabelMileageRate1__": {
																					"type": "Label",
																					"data": [
																						"MileageRate1__"
																					],
																					"options": {
																						"label": "_MileageRate1",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 35
																				},
																				"MileageRate1__": {
																					"type": "Decimal",
																					"data": [
																						"MileageRate1__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_MileageRate1",
																						"precision_internal": 3,
																						"precision_current": 3,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 36
																				},
																				"LabelDate": {
																					"type": "Label",
																					"data": [
																						"Date__"
																					],
																					"options": {
																						"label": "_Date",
																						"version": 0
																					},
																					"stamp": 37
																				},
																				"Date__": {
																					"type": "DateTime",
																					"data": [
																						"Date__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Date",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"LabelExchangeRate__": {
																					"type": "Label",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"label": "_ExpenseExchangeRate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 39
																				},
																				"ExchangeRate__": {
																					"type": "Decimal",
																					"data": [
																						"ExchangeRate__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_ExpenseExchangeRate",
																						"precision_internal": 5,
																						"precision_current": 5,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 40
																				},
																				"LabelLocalAmount__": {
																					"type": "Label",
																					"data": [
																						"LocalAmount__"
																					],
																					"options": {
																						"label": "_ExpenseLocalAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 41
																				},
																				"LocalAmount__": {
																					"type": "Decimal",
																					"data": [
																						"LocalAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_ExpenseLocalAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 42
																				},
																				"LabelInputTaxRate__": {
																					"type": "Label",
																					"data": [
																						"InputTaxRate__"
																					],
																					"options": {
																						"label": "_InputTaxRate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 43
																				},
																				"InputTaxRate__": {
																					"type": "CheckBox",
																					"data": [
																						"InputTaxRate__"
																					],
																					"options": {
																						"label": "_InputTaxRate",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 44
																				},
																				"LabelTaxCode1__": {
																					"type": "Label",
																					"data": [
																						"TaxCode1__"
																					],
																					"options": {
																						"label": "_TaxCode",
																						"version": 0
																					},
																					"stamp": 45
																				},
																				"TaxCode1__": {
																					"type": "ShortText",
																					"data": [
																						"TaxCode1__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 46
																				},
																				"LabelTaxRate1__": {
																					"type": "Label",
																					"data": [
																						"TaxRate1__"
																					],
																					"options": {
																						"label": "_TaxRate",
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"TaxRate1__": {
																					"type": "Decimal",
																					"data": [
																						"TaxRate1__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxRate",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 48
																				},
																				"LabelTaxAmount1__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount1__"
																					],
																					"options": {
																						"label": "_TaxAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 49
																				},
																				"TaxAmount1__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount1__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": false,
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 50
																				},
																				"LabelTaxCode2__": {
																					"type": "Label",
																					"data": [
																						"TaxCode2__"
																					],
																					"options": {
																						"label": "_TaxCode",
																						"version": 0
																					},
																					"stamp": 51
																				},
																				"TaxCode2__": {
																					"type": "ShortText",
																					"data": [
																						"TaxCode2__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 52
																				},
																				"LabelTaxRate2__": {
																					"type": "Label",
																					"data": [
																						"TaxRate2__"
																					],
																					"options": {
																						"label": "_TaxRate",
																						"version": 0
																					},
																					"stamp": 53
																				},
																				"TaxRate2__": {
																					"type": "Decimal",
																					"data": [
																						"TaxRate2__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxRate",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 54
																				},
																				"LabelTaxAmount2__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount2__"
																					],
																					"options": {
																						"label": "_TaxAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 55
																				},
																				"TaxAmount2__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount2__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 56
																				},
																				"LabelTaxCode3__": {
																					"type": "Label",
																					"data": [
																						"TaxCode3__"
																					],
																					"options": {
																						"label": "_TaxCode",
																						"version": 0
																					},
																					"stamp": 57
																				},
																				"TaxCode3__": {
																					"type": "ShortText",
																					"data": [
																						"TaxCode3__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 58
																				},
																				"LabelTaxRate3__": {
																					"type": "Label",
																					"data": [
																						"TaxRate3__"
																					],
																					"options": {
																						"label": "_TaxRate",
																						"version": 0
																					},
																					"stamp": 59
																				},
																				"TaxRate3__": {
																					"type": "Decimal",
																					"data": [
																						"TaxRate3__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxRate",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 60
																				},
																				"LabelTaxAmount3__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount3__"
																					],
																					"options": {
																						"label": "_TaxAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 61
																				},
																				"TaxAmount3__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount3__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 62
																				},
																				"LabelTaxCode4__": {
																					"type": "Label",
																					"data": [
																						"TaxCode4__"
																					],
																					"options": {
																						"label": "_TaxCode",
																						"version": 0
																					},
																					"stamp": 63
																				},
																				"TaxCode4__": {
																					"type": "ShortText",
																					"data": [
																						"TaxCode4__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 64
																				},
																				"LabelTaxRate4__": {
																					"type": "Label",
																					"data": [
																						"TaxRate4__"
																					],
																					"options": {
																						"label": "_TaxRate",
																						"version": 0
																					},
																					"stamp": 65
																				},
																				"TaxRate4__": {
																					"type": "Decimal",
																					"data": [
																						"TaxRate4__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxRate",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 66
																				},
																				"LabelTaxAmount4__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount4__"
																					],
																					"options": {
																						"label": "_TaxAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 67
																				},
																				"TaxAmount4__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount4__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 68
																				},
																				"LabelTaxCode5__": {
																					"type": "Label",
																					"data": [
																						"TaxCode5__"
																					],
																					"options": {
																						"label": "_TaxCode",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"TaxCode5__": {
																					"type": "ShortText",
																					"data": [
																						"TaxCode5__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 70
																				},
																				"LabelTaxRate5__": {
																					"type": "Label",
																					"data": [
																						"TaxRate5__"
																					],
																					"options": {
																						"label": "_TaxRate",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"TaxRate5__": {
																					"type": "Decimal",
																					"data": [
																						"TaxRate5__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxRate",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 72
																				},
																				"LabelVendor__": {
																					"type": "Label",
																					"data": [
																						"Vendor__"
																					],
																					"options": {
																						"label": "_Vendor",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 73
																				},
																				"Vendor__": {
																					"type": "ShortText",
																					"data": [
																						"Vendor__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Vendor",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 74
																				},
																				"LabelTaxAmount5__": {
																					"type": "Label",
																					"data": [
																						"TaxAmount5__"
																					],
																					"options": {
																						"label": "_TaxAmount",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 75
																				},
																				"TaxAmount5__": {
																					"type": "Decimal",
																					"data": [
																						"TaxAmount5__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TaxAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 76
																				},
																				"Refundable__": {
																					"type": "CheckBox",
																					"data": [
																						"Refundable__"
																					],
																					"options": {
																						"label": "_Refundable",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 77
																				},
																				"LabelTotalAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalAmount__"
																					],
																					"options": {
																						"label": "_TotalAmount",
																						"version": 0
																					},
																					"stamp": 78
																				},
																				"TotalAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 79
																				},
																				"LabelBillable__": {
																					"type": "Label",
																					"data": [
																						"Billable__"
																					],
																					"options": {
																						"label": "_Billable",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 80
																				},
																				"LabelReimbursableLocalAmount__": {
																					"type": "Label",
																					"data": [
																						"ReimbursableLocalAmount__"
																					],
																					"options": {
																						"label": "_ReimbursableLocalAmount",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 153
																				},
																				"ReimbursableLocalAmount__": {
																					"type": "Decimal",
																					"data": [
																						"ReimbursableLocalAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_ReimbursableLocalAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"autocompletable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 154
																				},
																				"LabelNonReimbursableLocalAmount__": {
																					"type": "Label",
																					"data": [
																						"NonReimbursableLocalAmount__"
																					],
																					"options": {
																						"label": "_NonReimbursableLocalAmount",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 155
																				},
																				"NonReimbursableLocalAmount__": {
																					"type": "Decimal",
																					"data": [
																						"NonReimbursableLocalAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_NonReimbursableLocalAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"autocompletable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 156
																				},
																				"Billable__": {
																					"type": "CheckBox",
																					"data": [
																						"Billable__"
																					],
																					"options": {
																						"label": "_Billable",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 81
																				},
																				"LabelRefundable__": {
																					"type": "Label",
																					"data": [
																						"Refundable__"
																					],
																					"options": {
																						"label": "_Refundable",
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 83
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 84
																				},
																				"LabelCostCenterName__": {
																					"type": "Label",
																					"data": [
																						"CostCenterName__"
																					],
																					"options": {
																						"label": "_CostCenterName",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 85
																				},
																				"CostCenterName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CostCenterName__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_CostCenterName",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains"
																					},
																					"stamp": 86
																				},
																				"LabelCostCenterId__": {
																					"type": "Label",
																					"data": [
																						"CostCenterId__"
																					],
																					"options": {
																						"label": "_CostCenterId",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 87
																				},
																				"CostCenterId__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CostCenterId__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_CostCenterId",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"hidden": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "contains",
																						"readonly": true
																					},
																					"stamp": 88
																				},
																				"LabelGLAccount__": {
																					"type": "Label",
																					"data": [
																						"GLAccount__"
																					],
																					"options": {
																						"label": "_GL Account",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 89
																				},
																				"GLAccount__": {
																					"type": "ShortText",
																					"data": [
																						"GLAccount__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_GL Account",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 30,
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 90
																				},
																				"LabelDescription__": {
																					"type": "Label",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"label": "_ExpenseDescription",
																						"version": 0
																					},
																					"stamp": 91
																				},
																				"Description__": {
																					"type": "LongText",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ExpenseDescription",
																						"activable": true,
																						"width": "300",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 92
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 150
																				}
																			},
																			"stamp": 93
																		}
																	},
																	"stamp": 94,
																	"data": []
																}
															},
															"stamp": 95,
															"data": []
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 96,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "Exp_attachments.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Attachments",
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"downloadButton": true,
																		"downloadConvertedFile": true
																	},
																	"stamp": 97
																}
															},
															"stamp": 98,
															"data": []
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 99,
													"*": {
														"TechnicalFieldsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Technical fields",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 100,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelExpenseStatus__": "ExpenseStatus__",
																			"ExpenseStatus__": "LabelExpenseStatus__",
																			"BillableFieldBehaviour__": "LabelBillableFieldBehaviour__",
																			"LabelBillableFieldBehaviour__": "BillableFieldBehaviour__",
																			"LabelVendorFieldBehaviour__": "VendorFieldBehaviour__",
																			"VendorFieldBehaviour__": "LabelVendorFieldBehaviour__",
																			"LabelTemplate__": "Template__",
																			"Template__": "LabelTemplate__",
																			"CostCenterFieldBehaviour__": "LabelCostCenterFieldBehaviour__",
																			"LabelCostCenterFieldBehaviour__": "CostCenterFieldBehaviour__",
																			"LocalCurrency__": "LabelLocalCurrency__",
																			"LabelLocalCurrency__": "LocalCurrency__",
																			"ReceiptBehaviour__": "LabelReceiptBehaviour__",
																			"LabelReceiptBehaviour__": "ReceiptBehaviour__",
																			"Manager__": "LabelManager__",
																			"LabelManager__": "Manager__",
																			"ManagerDN__": "LabelManagerDN__",
																			"LabelManagerDN__": "ManagerDN__"
																		},
																		"version": 0
																	},
																	"stamp": 101,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelExpenseStatus__": {
																						"line": 1,
																						"column": 1
																					},
																					"ExpenseStatus__": {
																						"line": 1,
																						"column": 2
																					},
																					"BillableFieldBehaviour__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelBillableFieldBehaviour__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelVendorFieldBehaviour__": {
																						"line": 3,
																						"column": 1
																					},
																					"VendorFieldBehaviour__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelTemplate__": {
																						"line": 5,
																						"column": 1
																					},
																					"Template__": {
																						"line": 5,
																						"column": 2
																					},
																					"CostCenterFieldBehaviour__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCostCenterFieldBehaviour__": {
																						"line": 6,
																						"column": 1
																					},
																					"LocalCurrency__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelLocalCurrency__": {
																						"line": 7,
																						"column": 1
																					},
																					"ReceiptBehaviour__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelReceiptBehaviour__": {
																						"line": 2,
																						"column": 1
																					},
																					"Manager__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelManager__": {
																						"line": 9,
																						"column": 1
																					},
																					"ManagerDN__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelManagerDN__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 102,
																			"*": {
																				"Template__": {
																					"type": "ShortText",
																					"data": [
																						"Template__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Template",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"hidden": true
																					},
																					"stamp": 103
																				},
																				"LabelTemplate__": {
																					"type": "Label",
																					"data": [
																						"Template__"
																					],
																					"options": {
																						"label": "_Template",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 104
																				},
																				"LabelReceiptBehaviour__": {
																					"type": "Label",
																					"data": [
																						"ReceiptBehaviour__"
																					],
																					"options": {
																						"label": "_ReceiptBehaviour",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 151
																				},
																				"ReceiptBehaviour__": {
																					"type": "ShortText",
																					"data": [
																						"ReceiptBehaviour__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ReceiptBehaviour",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 152
																				},
																				"VendorFieldBehaviour__": {
																					"type": "ShortText",
																					"data": [
																						"VendorFieldBehaviour__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorFieldBehaviour",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 105
																				},
																				"LabelVendorFieldBehaviour__": {
																					"type": "Label",
																					"data": [
																						"VendorFieldBehaviour__"
																					],
																					"options": {
																						"label": "_VendorFieldBehaviour",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 106
																				},
																				"ExpenseStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"ExpenseStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_To submit",
																							"2": "_To approve",
																							"3": "_To control",
																							"4": "_Validated",
																							"5": "_Deleted",
																							"6": "_Pending delete"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "To submit",
																							"2": "To approve",
																							"3": "To control",
																							"4": "Validated",
																							"5": "Deleted",
																							"6": "Pending delete"
																						},
																						"label": "_ExpenseStatus",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 107
																				},
																				"LabelExpenseStatus__": {
																					"type": "Label",
																					"data": [
																						"ExpenseStatus__"
																					],
																					"options": {
																						"label": "_ExpenseStatus",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 108
																				},
																				"LabelBillableFieldBehaviour__": {
																					"type": "Label",
																					"data": [
																						"BillableFieldBehaviour__"
																					],
																					"options": {
																						"label": "_BillableFieldBehaviour",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 109
																				},
																				"BillableFieldBehaviour__": {
																					"type": "ShortText",
																					"data": [
																						"BillableFieldBehaviour__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_BillableFieldBehaviour",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 110
																				},
																				"LabelCostCenterFieldBehaviour__": {
																					"type": "Label",
																					"data": [
																						"CostCenterFieldBehaviour__"
																					],
																					"options": {
																						"label": "_CostCenterFieldBehaviour",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 111
																				},
																				"CostCenterFieldBehaviour__": {
																					"type": "ShortText",
																					"data": [
																						"CostCenterFieldBehaviour__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_CostCenterFieldBehaviour",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 112
																				},
																				"LabelLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"label": "_ExpenseLocalCurrency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 113
																				},
																				"LocalCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ExpenseLocalCurrency",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 114
																				},
																				"LabelManagerDN__": {
																					"type": "Label",
																					"data": [
																						"ManagerDN__"
																					],
																					"options": {
																						"label": "_ManagerDN",
																						"hidden": true
																					},
																					"stamp": 161
																				},
																				"ManagerDN__": {
																					"type": "ShortText",
																					"data": [
																						"ManagerDN__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ManagerDN",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"length": 500,
																						"autocompletable": false
																					},
																					"stamp": 162
																				},
																				"LabelManager__": {
																					"type": "Label",
																					"data": [
																						"Manager__"
																					],
																					"options": {
																						"label": "_Manager",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 159
																				},
																				"Manager__": {
																					"type": "ShortText",
																					"data": [
																						"Manager__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Manager",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true,
																						"readonly": true,
																						"length": 80,
																						"autocompletable": false
																					},
																					"stamp": 160
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 115,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 55,
													"width": 45,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "Exp_preview.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"label": "_Document Preview",
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0,
																		"initialZoom": "height"
																	},
																	"stamp": 116
																}
															},
															"stamp": 117,
															"data": []
														}
													},
													"stamp": 118,
													"data": []
												}
											},
											"stamp": 119,
											"data": []
										}
									},
									"stamp": 120,
									"data": []
								}
							},
							"stamp": 121,
							"data": []
						}
					},
					"stamp": 122,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 123,
							"data": []
						},
						"DeleteExpense": {
							"type": "SubmitButton",
							"options": {
								"label": "_DeleteExpense",
								"action": "none",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Expense",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"style": 2,
								"url": ""
							},
							"stamp": 124,
							"data": []
						},
						"Save": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Saveandquit",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"url": "",
								"action": "savequit",
								"version": 0,
								"urlImageOverlay": "",
								"style": 1,
								"shortCut": "S"
							},
							"stamp": 125
						}
					},
					"stamp": 126,
					"data": []
				}
			},
			"stamp": 127,
			"data": []
		}
	},
	"stamps": 162,
	"data": []
}