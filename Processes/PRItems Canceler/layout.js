{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1700
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning"
											},
											"*": {
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Spacer": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Spacer",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {},
																				"lines": 0,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 15,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 16,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 17,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner__",
																						"width": 3802,
																						"htmlContent": "Goods Receipt",
																						"version": 0
																					},
																					"stamp": 18
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 19,
															"data": []
														}
													},
													"stamp": 20,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 21,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 23,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "PR_attachments.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Attachments",
																"hidden": true
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 24
																}
															},
															"stamp": 25,
															"data": []
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 26,
													"*": {
														"LineItems": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "GR_items.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_LineItems",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 27,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 28,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LineItems__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 29,
																			"*": {
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line"
																					},
																					"stamp": 30
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 13,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_LineItems",
																						"subsection": null
																					},
																					"stamp": 31,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 32,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 33
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 34
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 35,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 36,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "Label",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"label": "_PRRUIDEX",
																												"version": 0
																											},
																											"stamp": 37,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 38,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "Label",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"label": "_PRNumber",
																												"version": 0
																											},
																											"stamp": 39,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 40,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Label",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"label": "_PRLineNumber",
																												"version": 0
																											},
																											"stamp": 41,
																											"position": [
																												"Integer"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 42,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"version": 0
																											},
																											"stamp": 43,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 44,
																									"data": [],
																									"*": {
																										"CancelableQuantity__": {
																											"type": "Label",
																											"data": [
																												"CancelableQuantity__"
																											],
																											"options": {
																												"label": "_CancelableQuantity",
																												"minwidth": "90",
																												"hidden": true
																											},
																											"stamp": 45,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 46,
																									"data": [],
																									"*": {
																										"CancelableAmount__": {
																											"type": "Label",
																											"data": [
																												"CancelableAmount__"
																											],
																											"options": {
																												"label": "_CancelableAmount",
																												"minwidth": "90",
																												"hidden": true
																											},
																											"stamp": 47,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 48,
																									"data": [],
																									"*": {
																										"RequestedQuantity__": {
																											"type": "Label",
																											"data": [
																												"RequestedQuantity__"
																											],
																											"options": {
																												"label": "_RequestedQuantity",
																												"version": 0
																											},
																											"stamp": 49,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 50,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 51,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 52,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Label",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"label": "_NetAmount",
																												"version": 0
																											},
																											"stamp": 53,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"RequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDate",
																												"version": 0
																											},
																											"stamp": 55,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "Label",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"label": "_RequesterDN",
																												"version": 0
																											},
																											"stamp": 57,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"RequesterName__": {
																											"type": "Label",
																											"data": [
																												"RequesterName__"
																											],
																											"options": {
																												"label": "_RequesterName",
																												"version": 0
																											},
																											"stamp": 59,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 114,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 115,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 62,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "ShortText",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRRUIDEX",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 64,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 65,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "ShortText",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRNumber",
																												"activable": true,
																												"width": "90",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 66,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 67,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Integer",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_PRLineNumber",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 68,
																											"position": [
																												"Integer"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 69,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "ShortText",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": "200",
																												"length": 200,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 70,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 71,
																									"data": [],
																									"*": {
																										"CancelableQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"CancelableQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CancelableQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 72,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 73,
																									"data": [],
																									"*": {
																										"CancelableAmount__": {
																											"type": "Decimal",
																											"data": [
																												"CancelableAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CancelableAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 74,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75,
																									"data": [],
																									"*": {
																										"RequestedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"RequestedQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_RequestedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "50",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 76,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 77,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 78,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 79,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_NetAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 80,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 81,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"RequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_RequestedDeliveryDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 82,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 83,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 84,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 85,
																									"data": [],
																									"*": {
																										"RequesterName__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 86,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 113,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"maxNbLinesToDisplay": 10,
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 116,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "50",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 89
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 90,
													"*": {
														"Comment": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": "170",
																"iconURL": "GR_information.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Comment",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 2,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Comment__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2"
																					},
																					"stamp": 91
																				},
																				"Comment__": {
																					"type": "LongText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Comment",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 92
																				}
																			},
																			"stamp": 93
																		}
																	},
																	"stamp": 94,
																	"data": []
																}
															},
															"stamp": 95,
															"data": []
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 96,
													"*": {
														"Actions": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Actions",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 97,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 98,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Quit__": {
																						"line": 3,
																						"column": 1
																					},
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConfirmCancelItems__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 99,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "60",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement",
																						"version": 0
																					},
																					"stamp": 100
																				},
																				"ConfirmCancelItems__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ConfirmCancelItems",
																						"label": "",
																						"urlImage": "",
																						"style": 5,
																						"textPosition": "text-below",
																						"textSize": "S",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "BudgetIntegrityValidation__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"width": "180",
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-cancel-goods-circle fa-4x ",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "",
																						"textStyle": "default"
																					},
																					"stamp": 101
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "_Quit",
																						"urlImage": "",
																						"style": 5,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "BudgetIntegrityValidation__",
																							"attachmentsMode": "all",
																							"willBeChild": true
																						},
																						"iconColor": "color1",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x",
																						"action": "cancel",
																						"url": "",
																						"width": "",
																						"version": 0
																					},
																					"stamp": 102
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 103,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": true
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 104
																}
															},
															"stamp": 105,
															"data": []
														}
													},
													"stamp": 106,
													"data": []
												}
											},
											"stamp": 107,
											"data": []
										}
									},
									"stamp": 108,
									"data": []
								}
							},
							"stamp": 109,
							"data": []
						}
					},
					"stamp": 110,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1700
					},
					"*": {},
					"stamp": 111,
					"data": []
				}
			},
			"stamp": 112,
			"data": []
		}
	},
	"stamps": 116,
	"data": []
}