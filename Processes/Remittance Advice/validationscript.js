///#GLOBALS Lib
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
if (!currentName && !currentAction) {
    var vendorLogin = Variable.GetValueAsString("vendorLogin");
    if (vendorLogin) {
        Process.ChangeOwner(vendorLogin);
    }
    var message = void 0;
    if (!vendorLogin) {
        message = Language.Translate("_No vendor login");
    }
    if (!Data.GetValue("PayerAddress__")) {
        message = Language.Translate("_No Payer address");
    }
    if (message) {
        Lib.CommonDialog.NextAlert.Define("_Error", message);
        Data.SetValue("State", 200);
        Data.SetValue("StatusCode", 1);
        Data.SetValue("ShortStatusTranslated", message);
        Data.SetValue("ShortStatus", message);
        Data.SetValue("LongStatus", message);
        Data.SetValue("CompletionDateTime", new Date());
    }
}
