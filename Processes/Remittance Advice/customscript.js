///#GLOBALS Lib
Lib.CommonDialog.NextAlert.Show({});
Controls.Line_items.Hide(true);
Controls.form_content_top.Hide(true);
Controls.DownloadCrystalReportsDataFile.OnClick = function () {
    var templateInfos = Lib.Purchasing.RA.Export.GetTemplateInfos();
    if (templateInfos.fileFormat === "RPT") {
        Process.OpenPreview({
            "conversionType": "crystal",
            "templateName": templateInfos.template,
            "language": templateInfos.escapedCompanyCode,
            "outputFormat": "mdb",
            "data": function (fnDataBuildDone) {
                Lib.Purchasing.RA.Export.CreateJsonString(templateInfos, function (jsonString) {
                    fnDataBuildDone(jsonString);
                });
            }
        });
    }
    return false;
};
Controls.PayerCompany__.OnSelectItem = function (item) {
    Controls.PayerCompanyCode__.SetValue(item.GetValue("CompanyCode__"));
    // @TODO recompute the payer address
};
Controls.VendorName__.OnSelectItem = function (item) {
    Controls.VendorNumber__.SetValue(item.GetValue("Number__"));
    // @TODO recompute the vendor address
};
