///#GLOBALS Lib Sys
if (Sys.Helpers.IsEmpty(Variable.GetValueAsString("vendorLinksInfos"))) {
    Lib.Purchasing.CM.InitCompaniesAndVendorInfos();
}
Lib.Purchasing.CM.PrepareImportJson()
    .Then(function (extractedData) {
    Lib.Purchasing.CM.SetExternalData(extractedData);
});
if (Lib.Purchasing.CM.IsCMSubmitable()) {
    Variable.SetValueAsString("IsCMSubmitable", "true");
}
else {
    Variable.SetValueAsString("IsCMSubmitable", "false");
}
