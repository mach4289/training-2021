///#GLOBALS Lib Sys
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- Catalog Management Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
var g_companyCode = Data.GetValue("CompanyCode__");
var g_vendorNumber = Data.GetValue("VendorNumber__");
function SendToApproval() {
    var submissionDate = new Date();
    Data.SetValue("SubmissionDateTime__", submissionDate);
    Data.SetValue("Status__", "ToApprove");
    // @TODO Create an instance of the process as Admin
    var catalogManagementWorkflow = Process.CreateProcessInstanceAsProcessAdmin("Catalog management workflow", false, false, false);
    var extCMWVars = catalogManagementWorkflow.GetExternalVars();
    extCMWVars.AddValue_String("ExtractedData", Variable.GetValueAsString("ExtractedData"), true);
    extCMWVars.AddValue_String("CMRUIDEX", Data.GetValue("RUIDEX"), true);
    extCMWVars.AddValue_String("SubmissionTime", submissionDate.getTime(), true);
    extCMWVars.AddValue_String("CatalogManagmentType", "external", true);
    var CMWVars = catalogManagementWorkflow.GetUninheritedVars();
    CMWVars.AddValue_String("Status__", Data.GetValue("Status__"), true);
    CMWVars.AddValue_String("CompanyCode__", Data.GetValue("CompanyCode__"), true);
    CMWVars.AddValue_String("VendorNumber__", Data.GetValue("VendorNumber__"), true);
    CMWVars.AddValue_String("VendorName__", Data.GetValue("VendorName__"), true);
    CMWVars.AddValue_String("RequesterLogin__", Data.GetValue("RequesterLogin__"), true);
    CMWVars.AddValue_String("RequesterName__", Data.GetValue("RequesterName__"), true);
    CMWVars.AddValue_String("ImportDescription__", Data.GetValue("ImportDescription__"), true);
    // The Imported CSV file is the first document attached to the CM process
    var csvFile = Attach.GetAttach(0);
    catalogManagementWorkflow.AddAttachEx(csvFile);
    catalogManagementWorkflow.Process();
    var ret = catalogManagementWorkflow.GetLastError();
    if (ret === 0) {
        Log.Info("CMW process call OK");
        // Set next alert with no parameters in particular
        Lib.CommonDialog.NextAlert.Define("_Update request will be submitted", "_Your catalog update request will be sent for approval", { isError: false, behaviorName: "CMCreationInfo" });
        Data.SetValue("KeepOpenAfterApproval", "WaitForApproval");
    }
    else {
        Log.Error("CMW process call returns with error message : " + catalogManagementWorkflow.GetLastErrorMessage());
    }
    Process.WaitForUpdate();
}
function CSVApproved() {
    Data.SetValue("ValidationDateTime__", new Date());
    Data.SetValue("Status__", "Approved");
    //Notif Succes
}
function CSVRejected() {
    var data = JSON.parse(Variable.GetValueAsString("resumeWithActionData") || "{}");
    Data.SetValue("Reason__", data.rejectionReason);
    Data.SetValue("ValidationDateTime__", new Date());
    Data.SetValue("Status__", "Rejected");
    Data.SetValue("State", 400);
}
function HandleApprovalWorkflowErrors() {
    var data = JSON.parse(Variable.GetValueAsString("resumeWithActionData") || "{}");
    Log.Error("[Catalog management workflow Error] " + data.errorMsg);
    Variable.SetValueAsString("MissingWorkflowRuleError", "true");
    Data.SetValue("Status__", "Failed");
    Data.SetValue("State", 200);
}
if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
    switch (currentName) {
        case "Submit":
            SendToApproval();
            break;
        case "CSVApproved":
            CSVApproved();
            break;
        case "CSVRejected":
            CSVRejected();
            break;
        case "MissingWorkflowRules":
            HandleApprovalWorkflowErrors();
            break;
        default:
            Process.PreventApproval();
            break;
    }
}
