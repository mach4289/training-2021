{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ConfigurationName__": "LabelConfigurationName__",
																	"LabelConfigurationName__": "ConfigurationName__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"LastModifiedDateTime__": "LabelLastModifiedDateTime__",
																	"LabelLastModifiedDateTime__": "LastModifiedDateTime__",
																	"LastModifiedBy__": "LabelLastModifiedBy__",
																	"LabelLastModifiedBy__": "LastModifiedBy__",
																	"ERP__": "LabelERP__",
																	"LabelERP__": "ERP__",
																	"OrderNumberPatterns__": "LabelOrderNumberPatterns__",
																	"LabelOrderNumberPatterns__": "OrderNumberPatterns__",
																	"GoodIssueNumberPatterns__": "LabelGoodIssueNumberPatterns__",
																	"LabelGoodIssueNumberPatterns__": "GoodIssueNumberPatterns__",
																	"SAPConfiguration__": "LabelSAPConfiguration__",
																	"LabelSAPConfiguration__": "SAPConfiguration__",
																	"SAPDocumentTypeFIInvoice__": "LabelSAPDocumentTypeFIInvoice__",
																	"LabelSAPDocumentTypeFIInvoice__": "SAPDocumentTypeFIInvoice__",
																	"SAPDocumentTypeFICreditNote__": "LabelSAPDocumentTypeFICreditNote__",
																	"LabelSAPDocumentTypeFICreditNote__": "SAPDocumentTypeFICreditNote__",
																	"SAPDocumentTypeFIConsignmentStock__": "LabelSAPDocumentTypeFIConsignmentStock__",
																	"LabelSAPDocumentTypeFIConsignmentStock__": "SAPDocumentTypeFIConsignmentStock__",
																	"SAPDocumentTypeMMInvoice__": "LabelSAPDocumentTypeMMInvoice__",
																	"LabelSAPDocumentTypeMMInvoice__": "SAPDocumentTypeMMInvoice__",
																	"SAPDocumentTypeMMCreditNote__": "LabelSAPDocumentTypeMMCreditNote__",
																	"LabelSAPDocumentTypeMMCreditNote__": "SAPDocumentTypeMMCreditNote__",
																	"CodingEnableCostCenter__": "LabelCodingEnableCostCenter__",
																	"LabelCodingEnableCostCenter__": "CodingEnableCostCenter__",
																	"CodingEnableGLAccount__": "LabelCodingEnableGLAccount__",
																	"LabelCodingEnableGLAccount__": "CodingEnableGLAccount__",
																	"CodingEnableWBSElement__": "LabelCodingEnableWBSElement__",
																	"LabelCodingEnableWBSElement__": "CodingEnableWBSElement__",
																	"CodingEnableBusinessArea__": "LabelCodingEnableBusinessArea__",
																	"LabelCodingEnableBusinessArea__": "CodingEnableBusinessArea__",
																	"CodingEnableInternalOrder__": "LabelCodingEnableInternalOrder__",
																	"LabelCodingEnableInternalOrder__": "CodingEnableInternalOrder__",
																	"CodingEnableAssignments__": "LabelCodingEnableAssignments__",
																	"LabelCodingEnableAssignments__": "CodingEnableAssignments__",
																	"VerificationCheckAttachmentSignature__": "LabelVerificationCheckAttachmentSignature__",
																	"LabelVerificationCheckAttachmentSignature__": "VerificationCheckAttachmentSignature__",
																	"WorkflowReviewersCanModifyCostCenter__": "LabelWorkflowReviewersCanModifyCostCenter__",
																	"LabelWorkflowReviewersCanModifyCostCenter__": "WorkflowReviewersCanModifyCostCenter__",
																	"WorkflowReviewersCanModifyGLAccount__": "LabelWorkflowReviewersCanModifyGLAccount__",
																	"LabelWorkflowReviewersCanModifyGLAccount__": "WorkflowReviewersCanModifyGLAccount__",
																	"WorkflowReviewersCanModifyWBSElement__": "LabelWorkflowReviewersCanModifyWBSElement__",
																	"LabelWorkflowReviewersCanModifyWBSElement__": "WorkflowReviewersCanModifyWBSElement__",
																	"WorkflowReviewersCanModifyBusinessArea__": "LabelWorkflowReviewersCanModifyBusinessArea__",
																	"LabelWorkflowReviewersCanModifyBusinessArea__": "WorkflowReviewersCanModifyBusinessArea__",
																	"WorkflowReviewersCanModifyAssignments__": "LabelWorkflowReviewersCanModifyAssignments__",
																	"LabelWorkflowReviewersCanModifyAssignments__": "WorkflowReviewersCanModifyAssignments__",
																	"WorkflowReviewersCanModifyInternalOrder__": "LabelWorkflowReviewersCanModifyInternalOrder__",
																	"LabelWorkflowReviewersCanModifyInternalOrder__": "WorkflowReviewersCanModifyInternalOrder__",
																	"WorkflowAutoPostAfterReview__": "LabelWorkflowAutoPostAfterReview__",
																	"LabelWorkflowAutoPostAfterReview__": "WorkflowAutoPostAfterReview__",
																	"CodingEnableTradingPartner__": "LabelCodingEnableTradingPartner__",
																	"LabelCodingEnableTradingPartner__": "CodingEnableTradingPartner__",
																	"WorkflowReviewersCanModifyTradingPartner__": "LabelWorkflowReviewersCanModifyTradingPartner__",
																	"LabelWorkflowReviewersCanModifyTradingPartner__": "WorkflowReviewersCanModifyTradingPartner__",
																	"VerificationPOMatchingMode__": "LabelVerificationPOMatchingMode__",
																	"LabelVerificationPOMatchingMode__": "VerificationPOMatchingMode__",
																	"SignInvoicesForArchiving__": "LabelSignInvoicesForArchiving__",
																	"LabelSignInvoicesForArchiving__": "SignInvoicesForArchiving__",
																	"AutolearningOnPOLines__": "LabelAutolearningOnPOLines__",
																	"LabelAutolearningOnPOLines__": "AutolearningOnPOLines__",
																	"ERPExportMethod__": "LabelERPExportMethod__",
																	"LabelERPExportMethod__": "ERPExportMethod__",
																	"ERPNotifierProcessName__": "LabelERPNotifierProcessName__",
																	"LabelERPNotifierProcessName__": "ERPNotifierProcessName__",
																	"ExportInvoiceImageFormat__": "LabelExportInvoiceImageFormat__",
																	"LabelExportInvoiceImageFormat__": "ExportInvoiceImageFormat__",
																	"WorkflowReviewersCanAddLineItems__": "LabelWorkflowReviewersCanAddLineItems__",
																	"LabelWorkflowReviewersCanAddLineItems__": "WorkflowReviewersCanAddLineItems__",
																	"ArchiveDurationVendorPortalInMonth__": "LabelArchiveDurationVendorPortalInMonth__",
																	"LabelArchiveDurationVendorPortalInMonth__": "ArchiveDurationVendorPortalInMonth__",
																	"TaxesWithholdingTax__": "LabelTaxesWithholdingTax__",
																	"LabelTaxesWithholdingTax__": "TaxesWithholdingTax__",
																	"LegalArchiving__": "LabelLegalArchiving__",
																	"LabelLegalArchiving__": "LegalArchiving__",
																	"WorkflowEnableNewInvoiceNotifications__": "LabelWorkflowEnableNewInvoiceNotifications__",
																	"LabelWorkflowEnableNewInvoiceNotifications__": "WorkflowEnableNewInvoiceNotifications__",
																	"ArchiveDurationInMonths__": "LabelArchiveDurationInMonths__",
																	"LabelArchiveDurationInMonths__": "ArchiveDurationInMonths__",
																	"ArchiveDataSettings__": "LabelArchiveDataSettings__",
																	"LabelArchiveDataSettings__": "ArchiveDataSettings__",
																	"AvailableDocumentCultures__": "LabelAvailableDocumentCultures__",
																	"LabelAvailableDocumentCultures__": "AvailableDocumentCultures__",
																	"AutomaticallyForwardNonPoInvoiceToReviewer__": "LabelAutomaticallyForwardNonPoInvoiceToReviewer__",
																	"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": "AutomaticallyForwardNonPoInvoiceToReviewer__",
																	"CodingEnableCompanyCode__": "LabelCodingEnableCompanyCode__",
																	"LabelCodingEnableCompanyCode__": "CodingEnableCompanyCode__",
																	"WorkflowReviewersCanModifyCompanyCode__": "LabelWorkflowReviewersCanModifyCompanyCode__",
																	"LabelWorkflowReviewersCanModifyCompanyCode__": "WorkflowReviewersCanModifyCompanyCode__",
																	"DefaultAPClerk__": "LabelDefaultAPClerk__",
																	"LabelDefaultAPClerk__": "DefaultAPClerk__",
																	"DefaultAPClerkEnd__": "LabelDefaultAPClerkEnd__",
																	"LabelDefaultAPClerkEnd__": "DefaultAPClerkEnd__",
																	"SAPDuplicateCheck__": "LabelSAPDuplicateCheck__",
																	"LabelSAPDuplicateCheck__": "SAPDuplicateCheck__",
																	"DisplayUnitOfMeasure__": "LabelDisplayUnitOfMeasure__",
																	"LabelDisplayUnitOfMeasure__": "DisplayUnitOfMeasure__",
																	"DisplayTaxCode__": "LabelDisplayTaxCode__",
																	"LabelDisplayTaxCode__": "DisplayTaxCode__",
																	"DefaultUnitOfMeasure__": "LabelDefaultUnitOfMeasure__",
																	"LabelDefaultUnitOfMeasure__": "DefaultUnitOfMeasure__",
																	"EnableConsignmentStock__": "LabelEnableConsignmentStock__",
																	"LabelEnableConsignmentStock__": "EnableConsignmentStock__",
																	"WebserviceURL__": "LabelWebserviceURL__",
																	"LabelWebserviceURL__": "WebserviceURL__",
																	"WebserviceUser__": "LabelWebserviceUser__",
																	"LabelWebserviceUser__": "WebserviceUser__",
																	"WebservicePassword__": "LabelWebservicePassword__",
																	"LabelWebservicePassword__": "WebservicePassword__",
																	"WebserviceURLDetails__": "LabelWebserviceURLDetails__",
																	"LabelWebserviceURLDetails__": "WebserviceURLDetails__",
																	"DisplayCostType__": "LabelDisplayCostType__",
																	"LabelDisplayCostType__": "DisplayCostType__",
																	"PlannedPricingConditions__": "LabelPlannedPricingConditions__",
																	"LabelPlannedPricingConditions__": "PlannedPricingConditions__",
																	"ReconciliationType__": "LabelReconciliationType__",
																	"LabelReconciliationType__": "ReconciliationType__",
																	"HeaderFooterThreshold__": "LabelHeaderFooterThreshold__",
																	"LabelHeaderFooterThreshold__": "HeaderFooterThreshold__",
																	"CodingEnableProjectCode__": "LabelCodingEnableProjectCode__",
																	"LabelCodingEnableProjectCode__": "CodingEnableProjectCode__",
																	"WorkflowReviewersCanModifyProjectCode__": "LabelWorkflowReviewersCanModifyProjectCode__",
																	"LabelWorkflowReviewersCanModifyProjectCode__": "WorkflowReviewersCanModifyProjectCode__",
																	"DisableTeaching__": "LabelDisableTeaching__",
																	"LabelDisableTeaching__": "DisableTeaching__",
																	"EnableTouchlessForNonPoInvoice__": "LabelEnableTouchlessForNonPoInvoice__",
																	"LabelEnableTouchlessForNonPoInvoice__": "EnableTouchlessForNonPoInvoice__",
																	"FTRVendorOnIdentifiersOnly__": "LabelFTRVendorOnIdentifiersOnly__",
																	"LabelFTRVendorOnIdentifiersOnly__": "FTRVendorOnIdentifiersOnly__",
																	"WorkflowDisableRules__": "LabelWorkflowDisableRules__",
																	"LabelWorkflowDisableRules__": "WorkflowDisableRules__",
																	"AutomatedDeterminationCompanyCode__": "LabelAutomatedDeterminationCompanyCode__",
																	"LabelAutomatedDeterminationCompanyCode__": "AutomatedDeterminationCompanyCode__",
																	"OCRJsonOverrideSettings__": "LabelOCRJsonOverrideSettings__",
																	"LabelOCRJsonOverrideSettings__": "OCRJsonOverrideSettings__",
																	"MultiTaxesOnALineItem__": "LabelMultiTaxesOnALineItem__",
																	"LabelMultiTaxesOnALineItem__": "MultiTaxesOnALineItem__",
																	"ContractViewer__": "LabelContractViewer__",
																	"LabelContractViewer__": "ContractViewer__",
																	"EnablePortalAccountCreation__": "LabelEnablePortalAccountCreation__",
																	"LabelEnablePortalAccountCreation__": "EnablePortalAccountCreation__",
																	"EnableOFACVerification__": "LabelEnableOFACVerification__",
																	"LabelEnableOFACVerification__": "EnableOFACVerification__",
																	"GLAccountDeterminationFrench__": "LabelGLAccountDeterminationFrench__",
																	"LabelGLAccountDeterminationFrench__": "GLAccountDeterminationFrench__",
																	"EnableSiSid__": "LabelEnableSiSid__",
																	"LabelEnableSiSid__": "EnableSiSid__",
																	"SiSidLogin__": "LabelSiSidLogin__",
																	"LabelSiSidLogin__": "SiSidLogin__",
																	"Autolearning_Folder__": "LabelAutolearning_Folder__",
																	"LabelAutolearning_Folder__": "Autolearning_Folder__",
																	"Autolearning_FallbackOnCommonFolder__": "LabelAutolearning_FallbackOnCommonFolder__",
																	"LabelAutolearning_FallbackOnCommonFolder__": "Autolearning_FallbackOnCommonFolder__",
																	"SiSidPassword__": "LabelSiSidPassword__",
																	"LabelSiSidPassword__": "SiSidPassword__",
																	"EnableItemRating__": "LabelEnableItemRating__",
																	"LabelEnableItemRating__": "EnableItemRating__",
																	"AnomalyDetectionForAP__": "LabelAnomalyDetectionForAP__",
																	"LabelAnomalyDetectionForAP__": "AnomalyDetectionForAP__",
																	"EnableVendorModifyPO__": "LabelEnableVendorModifyPO__",
																	"LabelEnableVendorModifyPO__": "EnableVendorModifyPO__",
																	"VendorPortalDynamicDiscounting__": "LabelVendorPortalDynamicDiscounting__",
																	"LabelVendorPortalDynamicDiscounting__": "VendorPortalDynamicDiscounting__",
																	"RecurringInvoiceDetection__": "LabelRecurringInvoiceDetection__",
																	"LabelRecurringInvoiceDetection__": "RecurringInvoiceDetection__",
																	"APCodingsPrediction__": "LabelAPCodingsPrediction__",
																	"LabelAPCodingsPrediction__": "APCodingsPrediction__",
																	"EnableAdvancedShippingNotice__": "LabelEnableAdvancedShippingNotice__",
																	"LabelEnableAdvancedShippingNotice__": "EnableAdvancedShippingNotice__",
																	"ContractNumberPatterns__": "LabelContractNumberPatterns__",
																	"LabelContractNumberPatterns__": "ContractNumberPatterns__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 87,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ConfigurationName__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelConfigurationName__": {
																				"line": 1,
																				"column": 1
																			},
																			"LastModifiedDateTime__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelLastModifiedDateTime__": {
																				"line": 2,
																				"column": 1
																			},
																			"LastModifiedBy__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelLastModifiedBy__": {
																				"line": 3,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"OrderNumberPatterns__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelOrderNumberPatterns__": {
																				"line": 5,
																				"column": 1
																			},
																			"GoodIssueNumberPatterns__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelGoodIssueNumberPatterns__": {
																				"line": 6,
																				"column": 1
																			},
																			"ERP__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelERP__": {
																				"line": 7,
																				"column": 1
																			},
																			"ERPExportMethod__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelERPExportMethod__": {
																				"line": 8,
																				"column": 1
																			},
																			"ERPNotifierProcessName__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelERPNotifierProcessName__": {
																				"line": 9,
																				"column": 1
																			},
																			"ExportInvoiceImageFormat__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelExportInvoiceImageFormat__": {
																				"line": 10,
																				"column": 1
																			},
																			"SAPConfiguration__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelSAPConfiguration__": {
																				"line": 11,
																				"column": 1
																			},
																			"SAPDocumentTypeFIInvoice__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelSAPDocumentTypeFIInvoice__": {
																				"line": 12,
																				"column": 1
																			},
																			"SAPDocumentTypeFICreditNote__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelSAPDocumentTypeFICreditNote__": {
																				"line": 13,
																				"column": 1
																			},
																			"SAPDocumentTypeFIConsignmentStock__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelSAPDocumentTypeFIConsignmentStock__": {
																				"line": 14,
																				"column": 1
																			},
																			"SAPDocumentTypeMMInvoice__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelSAPDocumentTypeMMInvoice__": {
																				"line": 15,
																				"column": 1
																			},
																			"SAPDocumentTypeMMCreditNote__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelSAPDocumentTypeMMCreditNote__": {
																				"line": 16,
																				"column": 1
																			},
																			"VerificationCheckAttachmentSignature__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelVerificationCheckAttachmentSignature__": {
																				"line": 17,
																				"column": 1
																			},
																			"SignInvoicesForArchiving__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelSignInvoicesForArchiving__": {
																				"line": 18,
																				"column": 1
																			},
																			"SAPDuplicateCheck__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelSAPDuplicateCheck__": {
																				"line": 19,
																				"column": 1
																			},
																			"CodingEnableCostCenter__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelCodingEnableCostCenter__": {
																				"line": 20,
																				"column": 1
																			},
																			"CodingEnableGLAccount__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelCodingEnableGLAccount__": {
																				"line": 21,
																				"column": 1
																			},
																			"CodingEnableCompanyCode__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelCodingEnableCompanyCode__": {
																				"line": 22,
																				"column": 1
																			},
																			"CodingEnableWBSElement__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelCodingEnableWBSElement__": {
																				"line": 23,
																				"column": 1
																			},
																			"CodingEnableBusinessArea__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelCodingEnableBusinessArea__": {
																				"line": 24,
																				"column": 1
																			},
																			"CodingEnableAssignments__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelCodingEnableAssignments__": {
																				"line": 25,
																				"column": 1
																			},
																			"CodingEnableInternalOrder__": {
																				"line": 26,
																				"column": 2
																			},
																			"LabelCodingEnableInternalOrder__": {
																				"line": 26,
																				"column": 1
																			},
																			"CodingEnableTradingPartner__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelCodingEnableTradingPartner__": {
																				"line": 27,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyCostCenter__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyCostCenter__": {
																				"line": 29,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyGLAccount__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyGLAccount__": {
																				"line": 30,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyCompanyCode__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyCompanyCode__": {
																				"line": 31,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyWBSElement__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyWBSElement__": {
																				"line": 32,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyBusinessArea__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyBusinessArea__": {
																				"line": 33,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyAssignments__": {
																				"line": 34,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyAssignments__": {
																				"line": 34,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyInternalOrder__": {
																				"line": 35,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyInternalOrder__": {
																				"line": 35,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyTradingPartner__": {
																				"line": 36,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyTradingPartner__": {
																				"line": 36,
																				"column": 1
																			},
																			"WorkflowReviewersCanModifyProjectCode__": {
																				"line": 37,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanModifyProjectCode__": {
																				"line": 37,
																				"column": 1
																			},
																			"WorkflowAutoPostAfterReview__": {
																				"line": 38,
																				"column": 2
																			},
																			"LabelWorkflowAutoPostAfterReview__": {
																				"line": 38,
																				"column": 1
																			},
																			"VerificationPOMatchingMode__": {
																				"line": 39,
																				"column": 2
																			},
																			"LabelVerificationPOMatchingMode__": {
																				"line": 39,
																				"column": 1
																			},
																			"AutolearningOnPOLines__": {
																				"line": 40,
																				"column": 2
																			},
																			"LabelAutolearningOnPOLines__": {
																				"line": 40,
																				"column": 1
																			},
																			"WorkflowReviewersCanAddLineItems__": {
																				"line": 43,
																				"column": 2
																			},
																			"LabelWorkflowReviewersCanAddLineItems__": {
																				"line": 43,
																				"column": 1
																			},
																			"ArchiveDurationVendorPortalInMonth__": {
																				"line": 44,
																				"column": 2
																			},
																			"LabelArchiveDurationVendorPortalInMonth__": {
																				"line": 44,
																				"column": 1
																			},
																			"TaxesWithholdingTax__": {
																				"line": 45,
																				"column": 2
																			},
																			"LabelTaxesWithholdingTax__": {
																				"line": 45,
																				"column": 1
																			},
																			"PlannedPricingConditions__": {
																				"line": 46,
																				"column": 2
																			},
																			"LabelPlannedPricingConditions__": {
																				"line": 46,
																				"column": 1
																			},
																			"LabelLegalArchiving__": {
																				"line": 47,
																				"column": 1
																			},
																			"LegalArchiving__": {
																				"line": 47,
																				"column": 2
																			},
																			"WorkflowEnableNewInvoiceNotifications__": {
																				"line": 48,
																				"column": 2
																			},
																			"LabelWorkflowEnableNewInvoiceNotifications__": {
																				"line": 48,
																				"column": 1
																			},
																			"LabelArchiveDurationInMonths__": {
																				"line": 49,
																				"column": 1
																			},
																			"ArchiveDurationInMonths__": {
																				"line": 49,
																				"column": 2
																			},
																			"ArchiveDataSettings__": {
																				"line": 50,
																				"column": 2
																			},
																			"LabelArchiveDataSettings__": {
																				"line": 50,
																				"column": 1
																			},
																			"AvailableDocumentCultures__": {
																				"line": 51,
																				"column": 2
																			},
																			"LabelAvailableDocumentCultures__": {
																				"line": 51,
																				"column": 1
																			},
																			"AutomaticallyForwardNonPoInvoiceToReviewer__": {
																				"line": 52,
																				"column": 2
																			},
																			"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": {
																				"line": 52,
																				"column": 1
																			},
																			"DefaultAPClerk__": {
																				"line": 53,
																				"column": 2
																			},
																			"LabelDefaultAPClerk__": {
																				"line": 53,
																				"column": 1
																			},
																			"DefaultAPClerkEnd__": {
																				"line": 54,
																				"column": 2
																			},
																			"LabelDefaultAPClerkEnd__": {
																				"line": 54,
																				"column": 1
																			},
																			"DisplayUnitOfMeasure__": {
																				"line": 55,
																				"column": 2
																			},
																			"LabelDisplayUnitOfMeasure__": {
																				"line": 55,
																				"column": 1
																			},
																			"DisplayTaxCode__": {
																				"line": 56,
																				"column": 2
																			},
																			"LabelDisplayTaxCode__": {
																				"line": 56,
																				"column": 1
																			},
																			"DefaultUnitOfMeasure__": {
																				"line": 57,
																				"column": 2
																			},
																			"LabelDefaultUnitOfMeasure__": {
																				"line": 57,
																				"column": 1
																			},
																			"EnableConsignmentStock__": {
																				"line": 58,
																				"column": 2
																			},
																			"LabelEnableConsignmentStock__": {
																				"line": 58,
																				"column": 1
																			},
																			"WebserviceURL__": {
																				"line": 59,
																				"column": 2
																			},
																			"LabelWebserviceURL__": {
																				"line": 59,
																				"column": 1
																			},
																			"WebserviceUser__": {
																				"line": 60,
																				"column": 2
																			},
																			"LabelWebserviceUser__": {
																				"line": 60,
																				"column": 1
																			},
																			"WebservicePassword__": {
																				"line": 61,
																				"column": 2
																			},
																			"LabelWebservicePassword__": {
																				"line": 61,
																				"column": 1
																			},
																			"WebserviceURLDetails__": {
																				"line": 62,
																				"column": 2
																			},
																			"LabelWebserviceURLDetails__": {
																				"line": 62,
																				"column": 1
																			},
																			"ReconciliationType__": {
																				"line": 63,
																				"column": 2
																			},
																			"LabelReconciliationType__": {
																				"line": 63,
																				"column": 1
																			},
																			"HeaderFooterThreshold__": {
																				"line": 64,
																				"column": 2
																			},
																			"LabelHeaderFooterThreshold__": {
																				"line": 64,
																				"column": 1
																			},
																			"DisplayCostType__": {
																				"line": 65,
																				"column": 2
																			},
																			"LabelDisplayCostType__": {
																				"line": 65,
																				"column": 1
																			},
																			"DisableTeaching__": {
																				"line": 66,
																				"column": 2
																			},
																			"LabelDisableTeaching__": {
																				"line": 66,
																				"column": 1
																			},
																			"EnableTouchlessForNonPoInvoice__": {
																				"line": 67,
																				"column": 2
																			},
																			"LabelEnableTouchlessForNonPoInvoice__": {
																				"line": 67,
																				"column": 1
																			},
																			"LabelCodingEnableProjectCode__": {
																				"line": 28,
																				"column": 1
																			},
																			"CodingEnableProjectCode__": {
																				"line": 28,
																				"column": 2
																			},
																			"FTRVendorOnIdentifiersOnly__": {
																				"line": 68,
																				"column": 2
																			},
																			"LabelFTRVendorOnIdentifiersOnly__": {
																				"line": 68,
																				"column": 1
																			},
																			"WorkflowDisableRules__": {
																				"line": 69,
																				"column": 2
																			},
																			"LabelWorkflowDisableRules__": {
																				"line": 69,
																				"column": 1
																			},
																			"AutomatedDeterminationCompanyCode__": {
																				"line": 70,
																				"column": 2
																			},
																			"LabelAutomatedDeterminationCompanyCode__": {
																				"line": 70,
																				"column": 1
																			},
																			"OCRJsonOverrideSettings__": {
																				"line": 71,
																				"column": 2
																			},
																			"LabelOCRJsonOverrideSettings__": {
																				"line": 71,
																				"column": 1
																			},
																			"MultiTaxesOnALineItem__": {
																				"line": 72,
																				"column": 2
																			},
																			"LabelMultiTaxesOnALineItem__": {
																				"line": 72,
																				"column": 1
																			},
																			"ContractViewer__": {
																				"line": 73,
																				"column": 2
																			},
																			"LabelContractViewer__": {
																				"line": 73,
																				"column": 1
																			},
																			"EnablePortalAccountCreation__": {
																				"line": 74,
																				"column": 2
																			},
																			"LabelEnablePortalAccountCreation__": {
																				"line": 74,
																				"column": 1
																			},
																			"GLAccountDeterminationFrench__": {
																				"line": 75,
																				"column": 2
																			},
																			"LabelGLAccountDeterminationFrench__": {
																				"line": 75,
																				"column": 1
																			},
																			"EnableOFACVerification__": {
																				"line": 76,
																				"column": 2
																			},
																			"LabelEnableOFACVerification__": {
																				"line": 76,
																				"column": 1
																			},
																			"Autolearning_Folder__": {
																				"line": 41,
																				"column": 2
																			},
																			"LabelAutolearning_Folder__": {
																				"line": 41,
																				"column": 1
																			},
																			"Autolearning_FallbackOnCommonFolder__": {
																				"line": 42,
																				"column": 2
																			},
																			"LabelAutolearning_FallbackOnCommonFolder__": {
																				"line": 42,
																				"column": 1
																			},
																			"EnableSiSid__": {
																				"line": 77,
																				"column": 2
																			},
																			"LabelEnableSiSid__": {
																				"line": 77,
																				"column": 1
																			},
																			"SiSidLogin__": {
																				"line": 78,
																				"column": 2
																			},
																			"LabelSiSidLogin__": {
																				"line": 78,
																				"column": 1
																			},
																			"SiSidPassword__": {
																				"line": 79,
																				"column": 2
																			},
																			"LabelSiSidPassword__": {
																				"line": 79,
																				"column": 1
																			},
																			"EnableItemRating__": {
																				"line": 80,
																				"column": 2
																			},
																			"LabelEnableItemRating__": {
																				"line": 80,
																				"column": 1
																			},
																			"VendorPortalDynamicDiscounting__": {
																				"line": 81,
																				"column": 2
																			},
																			"LabelVendorPortalDynamicDiscounting__": {
																				"line": 81,
																				"column": 1
																			},
																			"AnomalyDetectionForAP__": {
																				"line": 82,
																				"column": 2
																			},
																			"LabelAnomalyDetectionForAP__": {
																				"line": 82,
																				"column": 1
																			},
																			"RecurringInvoiceDetection__": {
																				"line": 83,
																				"column": 2
																			},
																			"LabelRecurringInvoiceDetection__": {
																				"line": 83,
																				"column": 1
																			},
																			"APCodingsPrediction__": {
																				"line": 84,
																				"column": 2
																			},
																			"LabelAPCodingsPrediction__": {
																				"line": 84,
																				"column": 1
																			},
																			"EnableVendorModifyPO__": {
																				"line": 85,
																				"column": 2
																			},
																			"LabelEnableVendorModifyPO__": {
																				"line": 85,
																				"column": 1
																			},
																			"EnableAdvancedShippingNotice__": {
																				"line": 86,
																				"column": 2
																			},
																			"LabelEnableAdvancedShippingNotice__": {
																				"line": 86,
																				"column": 1
																			},
																			"ContractNumberPatterns__": {
																				"line": 87,
																				"column": 2
																			},
																			"LabelContractNumberPatterns__": {
																				"line": 87,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelConfigurationName__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"label": "_Configuration name",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"ConfigurationName__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Configuration name",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 4
																		},
																		"LabelLastModifiedDateTime__": {
																			"type": "Label",
																			"data": [
																				"LastModifiedDateTime__"
																			],
																			"options": {
																				"label": "_Last modified datetime",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"LastModifiedDateTime__": {
																			"type": "RealDateTime",
																			"data": [
																				"LastModifiedDateTime__"
																			],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_Last modified datetime",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"LabelLastModifiedBy__": {
																			"type": "Label",
																			"data": [
																				"LastModifiedBy__"
																			],
																			"options": {
																				"label": "_Last modified by",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"LastModifiedBy__": {
																			"type": "ShortText",
																			"data": [
																				"LastModifiedBy__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Last modified by",
																				"readonly": true,
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 10
																		},
																		"LabelDefaultAPClerk__": {
																			"type": "Label",
																			"data": [
																				"DefaultAPClerk__"
																			],
																			"options": {
																				"label": "_DefaultAPClerk",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"DefaultAPClerk__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"DefaultAPClerk__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_DefaultAPClerk",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 12
																		},
																		"LabelOrderNumberPatterns__": {
																			"type": "Label",
																			"data": [
																				"OrderNumberPatterns__"
																			],
																			"options": {
																				"label": "_OrderNumberPatterns",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"OrderNumberPatterns__": {
																			"type": "ShortText",
																			"data": [
																				"OrderNumberPatterns__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_OrderNumberPatterns",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"LabelGoodIssueNumberPatterns__": {
																			"type": "Label",
																			"data": [
																				"GoodIssueNumberPatterns__"
																			],
																			"options": {
																				"label": "_GoodIssueNumberPatterns",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"GoodIssueNumberPatterns__": {
																			"type": "ShortText",
																			"data": [
																				"GoodIssueNumberPatterns__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_GoodIssueNumberPatterns",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"LabelERP__": {
																			"type": "Label",
																			"data": [
																				"ERP__"
																			],
																			"options": {
																				"label": "_ERP",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"ERP__": {
																			"type": "ComboBox",
																			"data": [
																				"ERP__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_SAP",
																					"2": "_Generic",
																					"3": "_EBS",
																					"4": "_NAV",
																					"5": "_JDE",
																					"6": "_SAPS4CLOUD"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "SAP",
																					"2": "generic",
																					"3": "EBS",
																					"4": "NAV",
																					"5": "JDE",
																					"6": "SAPS4CLOUD"
																				},
																				"label": "_ERP",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 18
																		},
																		"LabelERPExportMethod__": {
																			"type": "Label",
																			"data": [
																				"ERPExportMethod__"
																			],
																			"options": {
																				"label": "_ERPExportMethod",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"ERPExportMethod__": {
																			"type": "ComboBox",
																			"data": [
																				"ERPExportMethod__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_SFTP",
																					"1": "_PROCESS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "SFTP",
																					"1": "PROCESS"
																				},
																				"label": "_ERPExportMethod",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 20
																		},
																		"LabelERPNotifierProcessName__": {
																			"type": "Label",
																			"data": [
																				"ERPNotifierProcessName__"
																			],
																			"options": {
																				"label": "_ERPNotifierProcessName",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"ERPNotifierProcessName__": {
																			"type": "ShortText",
																			"data": [
																				"ERPNotifierProcessName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ERPNotifierProcessName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"LabelExportInvoiceImageFormat__": {
																			"type": "Label",
																			"data": [
																				"ExportInvoiceImageFormat__"
																			],
																			"options": {
																				"label": "_ExportInvoiceImageFormat",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"ExportInvoiceImageFormat__": {
																			"type": "ComboBox",
																			"data": [
																				"ExportInvoiceImageFormat__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_No",
																					"1": "_Yes as TIFF",
																					"2": "_Yes as PDF"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "tif",
																					"2": "pdf"
																				},
																				"label": "_ExportInvoiceImageFormat",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 24
																		},
																		"LabelSAPConfiguration__": {
																			"type": "Label",
																			"data": [
																				"SAPConfiguration__"
																			],
																			"options": {
																				"label": "_SAPConfiguration",
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"SAPConfiguration__": {
																			"type": "ShortText",
																			"data": [
																				"SAPConfiguration__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPConfiguration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"LabelSAPDocumentTypeFIInvoice__": {
																			"type": "Label",
																			"data": [
																				"SAPDocumentTypeFIInvoice__"
																			],
																			"options": {
																				"label": "_SAPDocumentTypeFIInvoice",
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"SAPDocumentTypeFIInvoice__": {
																			"type": "ShortText",
																			"data": [
																				"SAPDocumentTypeFIInvoice__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPDocumentTypeFIInvoice",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"LabelSAPDocumentTypeFICreditNote__": {
																			"type": "Label",
																			"data": [
																				"SAPDocumentTypeFICreditNote__"
																			],
																			"options": {
																				"label": "_SAPDocumentTypeFICreditNote",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"SAPDocumentTypeFICreditNote__": {
																			"type": "ShortText",
																			"data": [
																				"SAPDocumentTypeFICreditNote__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPDocumentTypeFICreditNote",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 30
																		},
																		"LabelSAPDocumentTypeFIConsignmentStock__": {
																			"type": "Label",
																			"data": [
																				"SAPDocumentTypeFIConsignmentStock__"
																			],
																			"options": {
																				"label": "_SAPDocumentTypeFIConsignmentStock",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"SAPDocumentTypeFIConsignmentStock__": {
																			"type": "ShortText",
																			"data": [
																				"SAPDocumentTypeFIConsignmentStock__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPDocumentTypeFIConsignmentStock",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 32
																		},
																		"LabelSAPDocumentTypeMMInvoice__": {
																			"type": "Label",
																			"data": [
																				"SAPDocumentTypeMMInvoice__"
																			],
																			"options": {
																				"label": "_SAPDocumentTypeMMInvoice",
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"SAPDocumentTypeMMInvoice__": {
																			"type": "ShortText",
																			"data": [
																				"SAPDocumentTypeMMInvoice__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPDocumentTypeMMInvoice",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 34
																		},
																		"LabelSAPDocumentTypeMMCreditNote__": {
																			"type": "Label",
																			"data": [
																				"SAPDocumentTypeMMCreditNote__"
																			],
																			"options": {
																				"label": "_SAPDocumentTypeMMCreditNote",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"SAPDocumentTypeMMCreditNote__": {
																			"type": "ShortText",
																			"data": [
																				"SAPDocumentTypeMMCreditNote__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SAPDocumentTypeMMCreditNote",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 36
																		},
																		"LabelVerificationCheckAttachmentSignature__": {
																			"type": "Label",
																			"data": [
																				"VerificationCheckAttachmentSignature__"
																			],
																			"options": {
																				"label": "_VerificationCheckAttachmentSignature",
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"VerificationCheckAttachmentSignature__": {
																			"type": "CheckBox",
																			"data": [
																				"VerificationCheckAttachmentSignature__"
																			],
																			"options": {
																				"label": "_VerificationCheckAttachmentSignature",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"LabelSAPDuplicateCheck__": {
																			"type": "Label",
																			"data": [
																				"SAPDuplicateCheck__"
																			],
																			"options": {
																				"label": "_SAPDuplicateCheck",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"SAPDuplicateCheck__": {
																			"type": "CheckBox",
																			"data": [
																				"SAPDuplicateCheck__"
																			],
																			"options": {
																				"label": "_SAPDuplicateCheck",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"LabelSignInvoicesForArchiving__": {
																			"type": "Label",
																			"data": [
																				"SignInvoicesForArchiving__"
																			],
																			"options": {
																				"label": "_ArchivingSignInvoicesForArchiving",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"SignInvoicesForArchiving__": {
																			"type": "CheckBox",
																			"data": [
																				"SignInvoicesForArchiving__"
																			],
																			"options": {
																				"label": "_ArchivingSignInvoicesForArchiving",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 42
																		},
																		"LabelCodingEnableCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableCostCenter__"
																			],
																			"options": {
																				"label": "_CodingEnableCostCenter",
																				"version": 0
																			},
																			"stamp": 43
																		},
																		"CodingEnableCostCenter__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableCostCenter__"
																			],
																			"options": {
																				"label": "_CodingEnableCostCenter",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"LabelCodingEnableGLAccount__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableGLAccount__"
																			],
																			"options": {
																				"label": "_CodingEnableGLAccount",
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"CodingEnableGLAccount__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableGLAccount__"
																			],
																			"options": {
																				"label": "_CodingEnableGLAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"LabelCodingEnableCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableCompanyCode__"
																			],
																			"options": {
																				"label": "_CodingEnableCompanyCode",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"CodingEnableCompanyCode__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableCompanyCode__"
																			],
																			"options": {
																				"label": "_CodingEnableCompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"LabelCodingEnableWBSElement__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableWBSElement__"
																			],
																			"options": {
																				"label": "_CodingEnableWBSElement",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"CodingEnableWBSElement__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableWBSElement__"
																			],
																			"options": {
																				"label": "_CodingEnableWBSElement",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 50
																		},
																		"LabelCodingEnableBusinessArea__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableBusinessArea__"
																			],
																			"options": {
																				"label": "_CodingEnableBusinessArea",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"CodingEnableBusinessArea__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableBusinessArea__"
																			],
																			"options": {
																				"label": "_CodingEnableBusinessArea",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 52
																		},
																		"LabelCodingEnableAssignments__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableAssignments__"
																			],
																			"options": {
																				"label": "_CodingEnableAssignments",
																				"version": 0
																			},
																			"stamp": 53
																		},
																		"CodingEnableAssignments__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableAssignments__"
																			],
																			"options": {
																				"label": "_CodingEnableAssignments",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"LabelCodingEnableInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableInternalOrder__"
																			],
																			"options": {
																				"label": "_CodingEnableInternalOrder",
																				"version": 0
																			},
																			"stamp": 55
																		},
																		"CodingEnableInternalOrder__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableInternalOrder__"
																			],
																			"options": {
																				"label": "_CodingEnableInternalOrder",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"LabelCodingEnableProjectCode__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableProjectCode__"
																			],
																			"options": {
																				"label": "_CodingEnableProjectCode",
																				"version": 0
																			},
																			"stamp": 57
																		},
																		"CodingEnableProjectCode__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableProjectCode__"
																			],
																			"options": {
																				"label": "_CodingEnableProjectCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 58
																		},
																		"LabelCodingEnableTradingPartner__": {
																			"type": "Label",
																			"data": [
																				"CodingEnableTradingPartner__"
																			],
																			"options": {
																				"label": "_CodingEnableTradingPartner",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"CodingEnableTradingPartner__": {
																			"type": "CheckBox",
																			"data": [
																				"CodingEnableTradingPartner__"
																			],
																			"options": {
																				"label": "_CodingEnableTradingPartner",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 60
																		},
																		"LabelWorkflowReviewersCanModifyCostCenter__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyCostCenter__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyCostCenter",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"WorkflowReviewersCanModifyCostCenter__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyCostCenter__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyCostCenter",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 62
																		},
																		"LabelWorkflowReviewersCanModifyGLAccount__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyGLAccount__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyGLAccount",
																				"version": 0
																			},
																			"stamp": 63
																		},
																		"WorkflowReviewersCanModifyGLAccount__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyGLAccount__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyGLAccount",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 64
																		},
																		"LabelWorkflowReviewersCanModifyCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyCompanyCode__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyCompanyCode",
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"WorkflowReviewersCanModifyCompanyCode__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyCompanyCode__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyCompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 66
																		},
																		"LabelWorkflowReviewersCanModifyWBSElement__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyWBSElement__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyWBSElement",
																				"version": 0
																			},
																			"stamp": 67
																		},
																		"WorkflowReviewersCanModifyWBSElement__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyWBSElement__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyWBSElement",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 68
																		},
																		"LabelWorkflowReviewersCanModifyBusinessArea__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyBusinessArea__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyBusinessArea",
																				"version": 0
																			},
																			"stamp": 69
																		},
																		"WorkflowReviewersCanModifyBusinessArea__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyBusinessArea__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyBusinessArea",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 70
																		},
																		"LabelWorkflowReviewersCanModifyAssignments__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyAssignments__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyAssignments",
																				"version": 0
																			},
																			"stamp": 71
																		},
																		"WorkflowReviewersCanModifyAssignments__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyAssignments__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyAssignments",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 72
																		},
																		"LabelWorkflowReviewersCanModifyInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyInternalOrder__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyInternalOrder",
																				"version": 0
																			},
																			"stamp": 73
																		},
																		"WorkflowReviewersCanModifyInternalOrder__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyInternalOrder__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyInternalOrder",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": ""
																			},
																			"stamp": 74
																		},
																		"LabelWorkflowReviewersCanModifyProjectCode__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyProjectCode__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyProjectCode",
																				"version": 0
																			},
																			"stamp": 75
																		},
																		"WorkflowReviewersCanModifyProjectCode__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyProjectCode__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyProjectCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 76
																		},
																		"LabelWorkflowReviewersCanModifyTradingPartner__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanModifyTradingPartner__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyTradingPartner",
																				"version": 0
																			},
																			"stamp": 77
																		},
																		"WorkflowReviewersCanModifyTradingPartner__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanModifyTradingPartner__"
																			],
																			"options": {
																				"label": "_WorkflowReviewersCanModifyTradingPartner",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 78
																		},
																		"LabelWorkflowAutoPostAfterReview__": {
																			"type": "Label",
																			"data": [
																				"WorkflowAutoPostAfterReview__"
																			],
																			"options": {
																				"label": "_Auto post after review",
																				"version": 0
																			},
																			"stamp": 79
																		},
																		"WorkflowAutoPostAfterReview__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowAutoPostAfterReview__"
																			],
																			"options": {
																				"label": "_Auto post after review",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 80
																		},
																		"LabelVerificationPOMatchingMode__": {
																			"type": "Label",
																			"data": [
																				"VerificationPOMatchingMode__"
																			],
																			"options": {
																				"label": "_VerificationPOMatchingMode",
																				"version": 0
																			},
																			"stamp": 81
																		},
																		"VerificationPOMatchingMode__": {
																			"type": "ShortText",
																			"data": [
																				"VerificationPOMatchingMode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_VerificationPOMatchingMode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 82
																		},
																		"LabelAutolearning_Folder__": {
																			"type": "Label",
																			"data": [
																				"Autolearning_Folder__"
																			],
																			"options": {
																				"label": "_Autolearning_Folder",
																				"version": 0
																			},
																			"stamp": 83
																		},
																		"Autolearning_Folder__": {
																			"type": "ShortText",
																			"data": [
																				"Autolearning_Folder__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Autolearning_Folder",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 84
																		},
																		"LabelAutolearning_FallbackOnCommonFolder__": {
																			"type": "Label",
																			"data": [
																				"Autolearning_FallbackOnCommonFolder__"
																			],
																			"options": {
																				"label": "_Autolearning_FallbackOnCommonFolder",
																				"version": 0
																			},
																			"stamp": 85
																		},
																		"Autolearning_FallbackOnCommonFolder__": {
																			"type": "CheckBox",
																			"data": [
																				"Autolearning_FallbackOnCommonFolder__"
																			],
																			"options": {
																				"label": "_Autolearning_FallbackOnCommonFolder",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 86
																		},
																		"LabelAutolearningOnPOLines__": {
																			"type": "Label",
																			"data": [
																				"AutolearningOnPOLines__"
																			],
																			"options": {
																				"label": "_AutolearningOnPOLines",
																				"version": 0
																			},
																			"stamp": 87
																		},
																		"AutolearningOnPOLines__": {
																			"type": "CheckBox",
																			"data": [
																				"AutolearningOnPOLines__"
																			],
																			"options": {
																				"label": "_AutolearningOnPOLines",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 88
																		},
																		"LabelWorkflowReviewersCanAddLineItems__": {
																			"type": "Label",
																			"data": [
																				"WorkflowReviewersCanAddLineItems__"
																			],
																			"options": {
																				"label": "_Reviewers can also add/remove line items",
																				"version": 0
																			},
																			"stamp": 89
																		},
																		"WorkflowReviewersCanAddLineItems__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowReviewersCanAddLineItems__"
																			],
																			"options": {
																				"label": "_Reviewers can also add/remove line items",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 90
																		},
																		"LabelArchiveDurationVendorPortalInMonth__": {
																			"type": "Label",
																			"data": [
																				"ArchiveDurationVendorPortalInMonth__"
																			],
																			"options": {
																				"label": "_ArchiveDurationVendorPortalInMonth",
																				"version": 0
																			},
																			"stamp": 91
																		},
																		"ArchiveDurationVendorPortalInMonth__": {
																			"type": "ComboBox",
																			"data": [
																				"ArchiveDurationVendorPortalInMonth__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_TwoMonths",
																					"1": "_OneYear",
																					"2": "_TwoYears",
																					"3": "_ThreeYears",
																					"4": "_FourYears",
																					"5": "_FiveYears",
																					"6": "_SixYears",
																					"7": "_SevenYears",
																					"8": "_EightYears",
																					"9": "_NineYears",
																					"10": "_TenYears",
																					"11": "_ElevenYears",
																					"12": "_TwelveYears",
																					"13": "_ThirteenYears",
																					"14": "_FourteenYears",
																					"15": "_FifteenYears"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132",
																					"12": "144",
																					"13": "156",
																					"14": "168",
																					"15": "180"
																				},
																				"label": "_ArchiveDurationVendorPortalInMonth",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 92
																		},
																		"LabelPlannedPricingConditions__": {
																			"type": "Label",
																			"data": [
																				"PlannedPricingConditions__"
																			],
																			"options": {
																				"label": "_PlannedPricingConditions",
																				"version": 0
																			},
																			"stamp": 93
																		},
																		"PlannedPricingConditions__": {
																			"type": "LongText",
																			"data": [
																				"PlannedPricingConditions__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_PlannedPricingConditions",
																				"activable": true,
																				"width": 250,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 94
																		},
																		"LabelTaxesWithholdingTax__": {
																			"type": "Label",
																			"data": [
																				"TaxesWithholdingTax__"
																			],
																			"options": {
																				"label": "_TaxesWithholdingTax",
																				"version": 0
																			},
																			"stamp": 95
																		},
																		"TaxesWithholdingTax__": {
																			"type": "ShortText",
																			"data": [
																				"TaxesWithholdingTax__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_TaxesWithholdingTax",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 96
																		},
																		"LabelLegalArchiving__": {
																			"type": "Label",
																			"data": [
																				"LegalArchiving__"
																			],
																			"options": {
																				"label": "_LegalArchiving",
																				"version": 0
																			},
																			"stamp": 97
																		},
																		"LegalArchiving__": {
																			"type": "CheckBox",
																			"data": [
																				"LegalArchiving__"
																			],
																			"options": {
																				"label": "_LegalArchiving",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 98
																		},
																		"LabelWorkflowEnableNewInvoiceNotifications__": {
																			"type": "Label",
																			"data": [
																				"WorkflowEnableNewInvoiceNotifications__"
																			],
																			"options": {
																				"label": "_WorkflowEnableNewInvoiceNotifications",
																				"version": 0
																			},
																			"stamp": 99
																		},
																		"WorkflowEnableNewInvoiceNotifications__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowEnableNewInvoiceNotifications__"
																			],
																			"options": {
																				"label": "_WorkflowEnableNewInvoiceNotifications",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 100
																		},
																		"LabelArchiveDurationInMonths__": {
																			"type": "Label",
																			"data": [
																				"ArchiveDurationInMonths__"
																			],
																			"options": {
																				"label": "_ArchiveDurationInMonths",
																				"version": 0
																			},
																			"stamp": 101
																		},
																		"ArchiveDurationInMonths__": {
																			"type": "ComboBox",
																			"data": [
																				"ArchiveDurationInMonths__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_TwoMonths",
																					"1": "_OneYear",
																					"2": "_TwoYears",
																					"3": "_ThreeYears",
																					"4": "_FourYears",
																					"5": "_FiveYears",
																					"6": "_SixYears",
																					"7": "_SevenYears",
																					"8": "_EightYears",
																					"9": "_NineYears",
																					"10": "_TenYears",
																					"11": "_ElevenYears",
																					"12": "_TwelveYears",
																					"13": "_ThirteenYears",
																					"14": "_FourteenYears",
																					"15": "_FifteenYears"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132",
																					"12": "144",
																					"13": "156",
																					"14": "168",
																					"15": "180"
																				},
																				"label": "_ArchiveDurationInMonths",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 102
																		},
																		"LabelArchiveDataSettings__": {
																			"type": "Label",
																			"data": [
																				"ArchiveDataSettings__"
																			],
																			"options": {
																				"label": "_ArchiveDataSettings",
																				"version": 0
																			},
																			"stamp": 103
																		},
																		"ArchiveDataSettings__": {
																			"type": "RadioButton",
																			"data": [
																				"ArchiveDataSettings__"
																			],
																			"options": {
																				"keys": [
																					"_ArchiveAll",
																					"_ArchiveDataOnly"
																				],
																				"values": [
																					"_ArchiveAll",
																					"_ArchiveDataOnly"
																				],
																				"label": "_ArchiveDataSettings",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 104
																		},
																		"LabelAvailableDocumentCultures__": {
																			"type": "Label",
																			"data": [
																				"AvailableDocumentCultures__"
																			],
																			"options": {
																				"label": "_AvailableDocumentCultures",
																				"version": 0
																			},
																			"stamp": 105
																		},
																		"AvailableDocumentCultures__": {
																			"type": "ShortText",
																			"data": [
																				"AvailableDocumentCultures__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_AvailableDocumentCultures",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 100,
																				"defaultValue": "en-US,en-GB,fr-FR",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": {
																			"type": "Label",
																			"data": [
																				"AutomaticallyForwardNonPoInvoiceToReviewer__"
																			],
																			"options": {
																				"label": "_AutomaticallyForwardNonPoInvoiceToReviewer",
																				"version": 0
																			},
																			"stamp": 107
																		},
																		"AutomaticallyForwardNonPoInvoiceToReviewer__": {
																			"type": "CheckBox",
																			"data": [
																				"AutomaticallyForwardNonPoInvoiceToReviewer__"
																			],
																			"options": {
																				"label": "_AutomaticallyForwardNonPoInvoiceToReviewer",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 108
																		},
																		"LabelDefaultAPClerkEnd__": {
																			"type": "Label",
																			"data": [
																				"DefaultAPClerkEnd__"
																			],
																			"options": {
																				"label": "_DefaultAPClerkEnd",
																				"version": 0
																			},
																			"stamp": 109
																		},
																		"DefaultAPClerkEnd__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"DefaultAPClerkEnd__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_DefaultAPClerkEnd",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 110
																		},
																		"LabelDisplayCostType__": {
																			"type": "Label",
																			"data": [
																				"DisplayCostType__"
																			],
																			"options": {
																				"label": "_Display Cost Type",
																				"version": 0
																			},
																			"stamp": 111
																		},
																		"DisplayCostType__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplayCostType__"
																			],
																			"options": {
																				"label": "_Display Cost Type",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 112
																		},
																		"LabelDisplayUnitOfMeasure__": {
																			"type": "Label",
																			"data": [
																				"DisplayUnitOfMeasure__"
																			],
																			"options": {
																				"label": "_Display Unit Of Measure",
																				"version": 0
																			},
																			"stamp": 113
																		},
																		"DisplayUnitOfMeasure__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplayUnitOfMeasure__"
																			],
																			"options": {
																				"label": "_Display Unit Of Measure",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 114
																		},
																		"LabelDisplayTaxCode__": {
																			"type": "Label",
																			"data": [
																				"DisplayTaxCode__"
																			],
																			"options": {
																				"label": "_Display Tax Code",
																				"version": 0
																			},
																			"stamp": 115
																		},
																		"DisplayTaxCode__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplayTaxCode__"
																			],
																			"options": {
																				"label": "_Display Tax Code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 116
																		},
																		"LabelDefaultUnitOfMeasure__": {
																			"type": "Label",
																			"data": [
																				"DefaultUnitOfMeasure__"
																			],
																			"options": {
																				"label": "_Default Unit Of Measure",
																				"version": 0
																			},
																			"stamp": 117
																		},
																		"DefaultUnitOfMeasure__": {
																			"type": "ShortText",
																			"data": [
																				"DefaultUnitOfMeasure__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Default Unit Of Measure",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 118
																		},
																		"LabelEnableConsignmentStock__": {
																			"type": "Label",
																			"data": [
																				"EnableConsignmentStock__"
																			],
																			"options": {
																				"label": "_EnableConsignmentStock",
																				"version": 0
																			},
																			"stamp": 119
																		},
																		"EnableConsignmentStock__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableConsignmentStock__"
																			],
																			"options": {
																				"label": "_EnableConsignmentStock",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 120
																		},
																		"LabelWebserviceURL__": {
																			"type": "Label",
																			"data": [
																				"WebserviceURL__"
																			],
																			"options": {
																				"label": "_WebserviceURL",
																				"version": 0
																			},
																			"stamp": 121
																		},
																		"WebserviceURL__": {
																			"type": "ShortText",
																			"data": [
																				"WebserviceURL__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_WebserviceURL",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 122
																		},
																		"LabelWebserviceUser__": {
																			"type": "Label",
																			"data": [
																				"WebserviceUser__"
																			],
																			"options": {
																				"label": "_WebserviceUser",
																				"version": 0
																			},
																			"stamp": 123
																		},
																		"WebserviceUser__": {
																			"type": "ShortText",
																			"data": [
																				"WebserviceUser__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_WebserviceUser",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 124
																		},
																		"LabelWebservicePassword__": {
																			"type": "Label",
																			"data": [
																				"WebservicePassword__"
																			],
																			"options": {
																				"label": "_WebservicePassword",
																				"version": 0
																			},
																			"stamp": 125
																		},
																		"WebservicePassword__": {
																			"type": "Password",
																			"data": [
																				"WebservicePassword__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_WebservicePassword",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 126
																		},
																		"LabelWebserviceURLDetails__": {
																			"type": "Label",
																			"data": [
																				"WebserviceURLDetails__"
																			],
																			"options": {
																				"label": "_WebserviceURLDetails",
																				"version": 0
																			},
																			"stamp": 127
																		},
																		"WebserviceURLDetails__": {
																			"type": "LongText",
																			"data": [
																				"WebserviceURLDetails__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_WebserviceURLDetails",
																				"activable": true,
																				"width": "100%",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"numberOfLines": 6
																			},
																			"stamp": 128
																		},
																		"LabelReconciliationType__": {
																			"type": "Label",
																			"data": [
																				"ReconciliationType__"
																			],
																			"options": {
																				"label": "_ReconciliationType",
																				"version": 0
																			},
																			"stamp": 129
																		},
																		"ReconciliationType__": {
																			"type": "ShortText",
																			"data": [
																				"ReconciliationType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ReconciliationType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 130
																		},
																		"LabelHeaderFooterThreshold__": {
																			"type": "Label",
																			"data": [
																				"HeaderFooterThreshold__"
																			],
																			"options": {
																				"label": "_HeaderFooterThreshold",
																				"version": 0
																			},
																			"stamp": 131
																		},
																		"HeaderFooterThreshold__": {
																			"type": "ShortText",
																			"data": [
																				"HeaderFooterThreshold__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_HeaderFooterThreshold",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 132
																		},
																		"LabelDisableTeaching__": {
																			"type": "Label",
																			"data": [
																				"DisableTeaching__"
																			],
																			"options": {
																				"label": "_Disable Teaching",
																				"version": 0
																			},
																			"stamp": 133
																		},
																		"DisableTeaching__": {
																			"type": "CheckBox",
																			"data": [
																				"DisableTeaching__"
																			],
																			"options": {
																				"label": "_Disable Teaching",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 134
																		},
																		"LabelEnableTouchlessForNonPoInvoice__": {
																			"type": "Label",
																			"data": [
																				"EnableTouchlessForNonPoInvoice__"
																			],
																			"options": {
																				"label": "_Enable touchless for Non PO Invoices",
																				"version": 0
																			},
																			"stamp": 135
																		},
																		"EnableTouchlessForNonPoInvoice__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableTouchlessForNonPoInvoice__"
																			],
																			"options": {
																				"label": "_Enable touchless for Non PO Invoices",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 136
																		},
																		"LabelFTRVendorOnIdentifiersOnly__": {
																			"type": "Label",
																			"data": [
																				"FTRVendorOnIdentifiersOnly__"
																			],
																			"options": {
																				"label": "_FTR Vendor on identifiers only",
																				"version": 0
																			},
																			"stamp": 137
																		},
																		"FTRVendorOnIdentifiersOnly__": {
																			"type": "CheckBox",
																			"data": [
																				"FTRVendorOnIdentifiersOnly__"
																			],
																			"options": {
																				"label": "_FTR Vendor on identifiers only",
																				"activable": true,
																				"width": 230,
																				"helpText": "_If enabled, only uses Unique vendor identifiers for the recognition algorithm (such as IBAN, VAT, Phone number), in other words the recognition by address bloc is deactivated.",
																				"helpURL": "2007",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 138
																		},
																		"LabelWorkflowDisableRules__": {
																			"type": "Label",
																			"data": [
																				"WorkflowDisableRules__"
																			],
																			"options": {
																				"label": "_WorkflowDisableRules",
																				"version": 0
																			},
																			"stamp": 139
																		},
																		"WorkflowDisableRules__": {
																			"type": "CheckBox",
																			"data": [
																				"WorkflowDisableRules__"
																			],
																			"options": {
																				"label": "_WorkflowDisableRules",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 140
																		},
																		"LabelAutomatedDeterminationCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"AutomatedDeterminationCompanyCode__"
																			],
																			"options": {
																				"label": "_AutomatedDeterminationCompanyCode",
																				"version": 0
																			},
																			"stamp": 141
																		},
																		"AutomatedDeterminationCompanyCode__": {
																			"type": "CheckBox",
																			"data": [
																				"AutomatedDeterminationCompanyCode__"
																			],
																			"options": {
																				"label": "_AutomatedDeterminationCompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 142
																		},
																		"LabelOCRJsonOverrideSettings__": {
																			"type": "Label",
																			"data": [
																				"OCRJsonOverrideSettings__"
																			],
																			"options": {
																				"label": "_OCRJsonOverrideSettings",
																				"version": 0
																			},
																			"stamp": 143
																		},
																		"OCRJsonOverrideSettings__": {
																			"type": "ShortText",
																			"data": [
																				"OCRJsonOverrideSettings__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_OCRJsonOverrideSettings",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 2048,
																				"browsable": false
																			},
																			"stamp": 144
																		},
																		"LabelMultiTaxesOnALineItem__": {
																			"type": "Label",
																			"data": [
																				"MultiTaxesOnALineItem__"
																			],
																			"options": {
																				"label": "_MultiTaxesOnALineItem",
																				"version": 0
																			},
																			"stamp": 145
																		},
																		"MultiTaxesOnALineItem__": {
																			"type": "CheckBox",
																			"data": [
																				"MultiTaxesOnALineItem__"
																			],
																			"options": {
																				"label": "_MultiTaxesOnALineItem",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 146
																		},
																		"LabelContractViewer__": {
																			"type": "Label",
																			"data": [
																				"ContractViewer__"
																			],
																			"options": {
																				"label": "_ContractViewer",
																				"version": 0
																			},
																			"stamp": 147
																		},
																		"ContractViewer__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ContractViewer__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ContractViewer",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 148
																		},
																		"LabelEnablePortalAccountCreation__": {
																			"type": "Label",
																			"data": [
																				"EnablePortalAccountCreation__"
																			],
																			"options": {
																				"label": "_EnablePortalAccountCreation",
																				"version": 0
																			},
																			"stamp": 149
																		},
																		"EnablePortalAccountCreation__": {
																			"type": "CheckBox",
																			"data": [
																				"EnablePortalAccountCreation__"
																			],
																			"options": {
																				"label": "_EnablePortalAccountCreation",
																				"activable": true,
																				"width": 230,
																				"helpText": "_HelpPortalAccountCreation",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 150
																		},
																		"LabelGLAccountDeterminationFrench__": {
																			"type": "Label",
																			"data": [
																				"GLAccountDeterminationFrench__"
																			],
																			"options": {
																				"label": "_GLAccountDeterminationFrench",
																				"version": 0
																			},
																			"stamp": 151
																		},
																		"GLAccountDeterminationFrench__": {
																			"type": "CheckBox",
																			"data": [
																				"GLAccountDeterminationFrench__"
																			],
																			"options": {
																				"label": "_GLAccountDeterminationFrench",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 152
																		},
																		"LabelEnableOFACVerification__": {
																			"type": "Label",
																			"data": [
																				"EnableOFACVerification__"
																			],
																			"options": {
																				"label": "_EnableOFACVerification",
																				"version": 0
																			},
																			"stamp": 153
																		},
																		"EnableOFACVerification__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableOFACVerification__"
																			],
																			"options": {
																				"label": "_EnableOFACVerification",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 154
																		},
																		"LabelEnableSiSid__": {
																			"type": "Label",
																			"data": [
																				"EnableSiSid__"
																			],
																			"options": {
																				"label": "_Enable IBAN verification using 3rd party service SiS id",
																				"version": 0
																			},
																			"stamp": 155
																		},
																		"EnableSiSid__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableSiSid__"
																			],
																			"options": {
																				"label": "_Enable IBAN verification using 3rd party service SiS id",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 156
																		},
																		"LabelSiSidLogin__": {
																			"type": "Label",
																			"data": [
																				"SiSidLogin__"
																			],
																			"options": {
																				"label": "_SiS id login",
																				"version": 0
																			},
																			"stamp": 157
																		},
																		"SiSidLogin__": {
																			"type": "ShortText",
																			"data": [
																				"SiSidLogin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SiS id login",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 158
																		},
																		"LabelSiSidPassword__": {
																			"type": "Label",
																			"data": [
																				"SiSidPassword__"
																			],
																			"options": {
																				"label": "_SiS id password",
																				"version": 0
																			},
																			"stamp": 159
																		},
																		"SiSidPassword__": {
																			"type": "Password",
																			"data": [
																				"SiSidPassword__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SiS id password",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 160
																		},
																		"LabelEnableItemRating__": {
																			"type": "Label",
																			"data": [
																				"EnableItemRating__"
																			],
																			"options": {
																				"label": "_EnableItemRating",
																				"version": 0
																			},
																			"stamp": 161
																		},
																		"EnableItemRating__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableItemRating__"
																			],
																			"options": {
																				"label": "_EnableItemRating",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 162
																		},
																		"LabelAnomalyDetectionForAP__": {
																			"type": "Label",
																			"data": [
																				"AnomalyDetectionForAP__"
																			],
																			"options": {
																				"label": "_AnomalyDetectionForAP",
																				"version": 0
																			},
																			"stamp": 163
																		},
																		"AnomalyDetectionForAP__": {
																			"type": "CheckBox",
																			"data": [
																				"AnomalyDetectionForAP__"
																			],
																			"options": {
																				"label": "_AnomalyDetectionForAP",
																				"activable": true,
																				"width": 230,
																				"helpText": "_AnomalyDetectionForAP_Help",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 164
																		},
																		"LabelEnableVendorModifyPO__": {
																			"type": "Label",
																			"data": [
																				"EnableVendorModifyPO__"
																			],
																			"options": {
																				"label": "_EnableVendorModifyPO",
																				"version": 0
																			},
																			"stamp": 165
																		},
																		"EnableVendorModifyPO__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableVendorModifyPO__"
																			],
																			"options": {
																				"label": "_EnableVendorModifyPO",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 166
																		},
																		"LabelVendorPortalDynamicDiscounting__": {
																			"type": "Label",
																			"data": [
																				"VendorPortalDynamicDiscounting__"
																			],
																			"options": {
																				"label": "_VendorPortalDynamicDiscounting",
																				"version": 0
																			},
																			"stamp": 167
																		},
																		"VendorPortalDynamicDiscounting__": {
																			"type": "CheckBox",
																			"data": [
																				"VendorPortalDynamicDiscounting__"
																			],
																			"options": {
																				"label": "_VendorPortalDynamicDiscounting",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 168
																		},
																		"LabelRecurringInvoiceDetection__": {
																			"type": "Label",
																			"data": [
																				"RecurringInvoiceDetection__"
																			],
																			"options": {
																				"label": "_RecurringInvoiceDetection"
																			},
																			"stamp": 184
																		},
																		"RecurringInvoiceDetection__": {
																			"type": "CheckBox",
																			"data": [
																				"RecurringInvoiceDetection__"
																			],
																			"options": {
																				"label": "_RecurringInvoiceDetection",
																				"activable": true,
																				"width": 230,
																				"helpText": "_RecurringInvoiceDetection_Help",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 185
																		},
																		"APCodingsPrediction__": {
																			"type": "CheckBox",
																			"data": [
																				"APCodingsPrediction__"
																			],
																			"options": {
																				"label": "_APCodingsPrediction",
																				"activable": true,
																				"width": 230,
																				"helpText": "_APCodingsPrediction_Help",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 171
																		},
																		"LabelAPCodingsPrediction__": {
																			"type": "Label",
																			"data": [
																				"APCodingsPrediction__"
																			],
																			"options": {
																				"label": "_APCodingsPrediction"
																			},
																			"stamp": 172
																		},
																		"LabelEnableAdvancedShippingNotice__": {
																			"type": "Label",
																			"data": [
																				"EnableAdvancedShippingNotice__"
																			],
																			"options": {
																				"label": "_EnableAdvancedShippingNotice"
																			},
																			"stamp": 186
																		},
																		"EnableAdvancedShippingNotice__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableAdvancedShippingNotice__"
																			],
																			"options": {
																				"label": "_EnableAdvancedShippingNotice",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"possibleValues": {},
																				"possibleKeys": {}
																			},
																			"stamp": 187
																		},
																		"LabelContractNumberPatterns__": {
																			"type": "Label",
																			"data": [
																				"ContractNumberPatterns__"
																			],
																			"options": {
																				"label": "_ContractNumberPatterns",
																				"version": 0
																			},
																			"stamp": 188
																		},
																		"ContractNumberPatterns__": {
																			"type": "ShortText",
																			"data": [
																				"ContractNumberPatterns__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ContractNumberPatterns",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 189
																		}
																	},
																	"stamp": 173
																}
															},
															"stamp": 174,
															"data": []
														}
													},
													"stamp": 175,
													"data": []
												}
											}
										}
									},
									"stamp": 176,
									"data": []
								}
							},
							"stamp": 177,
							"data": []
						}
					},
					"stamp": 178,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 179,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 180,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 181,
							"data": []
						}
					},
					"stamp": 182,
					"data": []
				}
			},
			"stamp": 183,
			"data": []
		}
	},
	"stamps": 189,
	"data": []
}