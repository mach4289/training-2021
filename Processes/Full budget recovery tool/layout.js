{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 0
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents",
														"hidden": true
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 10
														}
													},
													"stamp": 9,
													"data": []
												}
											},
											"stamp": 8,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 175,
													"*": {
														"TMPStepPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "center",
																"label": "TMPStepPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 150,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CurrentStep__": "LabelCurrentStep__",
																			"LabelCurrentStep__": "CurrentStep__"
																		},
																		"version": 0
																	},
																	"stamp": 151,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CurrentStepDesc__": {
																						"line": 1,
																						"column": 1
																					},
																					"CurrentStep__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelCurrentStep__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 152,
																			"*": {
																				"CurrentStepDesc__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_CurrentStepDesc",
																						"version": 0
																					},
																					"stamp": 181
																				},
																				"LabelCurrentStep__": {
																					"type": "Label",
																					"data": [
																						"CurrentStep__"
																					],
																					"options": {
																						"label": "_CurrentStep",
																						"version": 0
																					},
																					"stamp": 269
																				},
																				"CurrentStep__": {
																					"type": "ShortText",
																					"data": [
																						"CurrentStep__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_CurrentStep",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 270
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 74,
													"*": {
														"UserActionHistoryPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_UserActionHistoryPanel",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 66,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"UserActionHistory__": "LabelUserActionHistory__",
																			"LabelUserActionHistory__": "UserActionHistory__"
																		},
																		"version": 0
																	},
																	"stamp": 67,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line4__": {
																						"line": 2,
																						"column": 1
																					},
																					"UserActionHistory__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelUserActionHistory__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 68,
																			"*": {
																				"LabelUserActionHistory__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_UserActionHistory",
																						"version": 0
																					},
																					"stamp": 621
																				},
																				"UserActionHistory__": {
																					"type": "ComboBox",
																					"data": [],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_UserActionHistory",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"notInDB": true,
																						"dataType": "String"
																					},
																					"stamp": 622
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 72
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SelectionMethodPanel": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_SelectionMethodPanel",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"SelectionMethod__": "LabelSelectionMethod__",
																			"LabelSelectionMethod__": "SelectionMethod__",
																			"Filter__": "LabelFilter__",
																			"LabelFilter__": "Filter__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 3,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Spacer_line__": {
																						"line": 3,
																						"column": 1
																					},
																					"SelectionMethod__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSelectionMethod__": {
																						"line": 1,
																						"column": 1
																					},
																					"Filter__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelFilter__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelSelectionMethod__": {
																					"type": "Label",
																					"data": [
																						"SelectionMethod__"
																					],
																					"options": {
																						"label": "_SelectionMethod",
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"SelectionMethod__": {
																					"type": "RadioButton",
																					"data": [
																						"SelectionMethod__"
																					],
																					"options": {
																						"keys": [
																							"_Doc",
																							"_DocItems"
																						],
																						"values": [
																							"_Doc",
																							"_DocItems"
																						],
																						"label": "_SelectionMethod",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 32
																				},
																				"LabelFilter__": {
																					"type": "Label",
																					"data": [
																						"Filter__"
																					],
																					"options": {
																						"label": "_Filter",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"Filter__": {
																					"type": "ShortText",
																					"data": [
																						"Filter__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Filter",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 200,
																						"defaultValue": "MsnEx=*",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 34
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 35
																				}
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 14,
															"data": []
														}
													},
													"stamp": 13,
													"data": []
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 392,
													"*": {
														"CheckBudgetResultSummaryPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_CheckBudgetResultSummaryPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 363,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TotalCheckCount__": "LabelTotalCheckCount__",
																			"LabelTotalCheckCount__": "TotalCheckCount__",
																			"CheckCountWarning__": "LabelCheckCountWarning__",
																			"LabelCheckCountWarning__": "CheckCountWarning__",
																			"CheckCountError__": "LabelCheckCountError__",
																			"LabelCheckCountError__": "CheckCountError__",
																			"CheckDocCountInError__": "LabelCheckDocCountInError__",
																			"LabelCheckDocCountInError__": "CheckDocCountInError__",
																			"CheckDocCountInWarning__": "LabelCheckDocCountInWarning__",
																			"LabelCheckDocCountInWarning__": "CheckDocCountInWarning__"
																		},
																		"version": 0
																	},
																	"stamp": 364,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CheckSummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 7,
																						"column": 1
																					},
																					"TotalCheckCount__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelTotalCheckCount__": {
																						"line": 2,
																						"column": 1
																					},
																					"CheckCountWarning__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCheckCountWarning__": {
																						"line": 6,
																						"column": 1
																					},
																					"CheckCountError__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelCheckCountError__": {
																						"line": 5,
																						"column": 1
																					},
																					"CheckDocCountInError__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelCheckDocCountInError__": {
																						"line": 3,
																						"column": 1
																					},
																					"CheckDocCountInWarning__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelCheckDocCountInWarning__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 365,
																			"*": {
																				"LabelTotalCheckCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_TotalCheckCount",
																						"version": 0
																					},
																					"stamp": 623
																				},
																				"TotalCheckCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_TotalCheckCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 624
																				},
																				"LabelCheckCountWarning__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_CheckCountWarning",
																						"version": 0
																					},
																					"stamp": 625
																				},
																				"LabelCheckDocCountInError__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_CheckDocCountInError",
																						"version": 0
																					},
																					"stamp": 884
																				},
																				"CheckDocCountInError__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_CheckDocCountInError",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 885
																				},
																				"LabelCheckDocCountInWarning__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_CheckDocCountInWarning",
																						"version": 0
																					},
																					"stamp": 886
																				},
																				"CheckDocCountInWarning__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_CheckDocCountInWarning",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 887
																				},
																				"LabelCheckCountError__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_CheckCountError",
																						"version": 0
																					},
																					"stamp": 627
																				},
																				"CheckCountError__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_CheckCountError",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 628
																				},
																				"CheckCountWarning__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_CheckCountWarning",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 626
																				},
																				"CheckSummary__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_CheckSummary",
																						"version": 0
																					},
																					"stamp": 80
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 411
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 38,
													"*": {
														"CheckBudgetResultDetailsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_CheckBudgetResultDetailsPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 39,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 40,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line3__": {
																						"line": 3,
																						"column": 1
																					},
																					"CheckDetailsTitle__": {
																						"line": 1,
																						"column": 1
																					},
																					"CheckTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 41,
																			"*": {
																				"CheckDetailsTitle__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_CheckDetailsTitle",
																						"version": 0
																					},
																					"stamp": 81
																				},
																				"CheckTable__": {
																					"type": "Table",
																					"data": [
																						"CheckTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_CheckTable",
																						"readonly": true
																					},
																					"stamp": 806,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 807,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 808
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 809
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 810,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 813,
																									"data": [],
																									"*": {
																										"CheckDocRuidEx__": {
																											"type": "Label",
																											"data": [
																												"CheckDocRuidEx__"
																											],
																											"options": {
																												"label": "_CheckDocRuidEx",
																												"version": 0
																											},
																											"stamp": 814,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 817,
																									"data": [],
																									"*": {
																										"CheckType__": {
																											"type": "Label",
																											"data": [
																												"CheckType__"
																											],
																											"options": {
																												"label": "_CheckType",
																												"version": 0
																											},
																											"stamp": 818,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 821,
																									"data": [],
																									"*": {
																										"CheckDetails__": {
																											"type": "Label",
																											"data": [
																												"CheckDetails__"
																											],
																											"options": {
																												"label": "_CheckDetails",
																												"version": 0
																											},
																											"stamp": 822,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 825,
																									"data": [],
																									"*": {
																										"CheckLevel__": {
																											"type": "Label",
																											"data": [
																												"CheckLevel__"
																											],
																											"options": {
																												"label": "_CheckLevel",
																												"version": 0
																											},
																											"stamp": 826,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 811,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 812,
																									"data": [],
																									"*": {
																										"CheckDocRuidEx__": {
																											"type": "ShortText",
																											"data": [
																												"CheckDocRuidEx__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CheckDocRuidEx",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 815,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 816,
																									"data": [],
																									"*": {
																										"CheckType__": {
																											"type": "ShortText",
																											"data": [
																												"CheckType__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CheckType",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 819,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 820,
																									"data": [],
																									"*": {
																										"CheckDetails__": {
																											"type": "LongText",
																											"data": [
																												"CheckDetails__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_CheckDetails",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 823,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 824,
																									"data": [],
																									"*": {
																										"CheckLevel__": {
																											"type": "ShortText",
																											"data": [
																												"CheckLevel__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CheckLevel",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 827,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 104
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 422,
													"*": {
														"RecoveryRequestResultSummaryPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecoveryRequestResultSummaryPanel",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 417,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"RecoveryRequestCount__": "LabelRecoveryRequestCount__",
																			"LabelRecoveryRequestCount__": "RecoveryRequestCount__",
																			"RecoveryAbortedRequestCount__": "LabelRecoveryAbortedRequestCount__",
																			"LabelRecoveryAbortedRequestCount__": "RecoveryAbortedRequestCount__"
																		},
																		"version": 0
																	},
																	"stamp": 418,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RecoveryRequestSummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line10__": {
																						"line": 4,
																						"column": 1
																					},
																					"RecoveryRequestCount__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRecoveryRequestCount__": {
																						"line": 2,
																						"column": 1
																					},
																					"RecoveryAbortedRequestCount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRecoveryAbortedRequestCount__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 419,
																			"*": {
																				"LabelRecoveryRequestCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_RecoveryRequestCount",
																						"version": 0
																					},
																					"stamp": 661
																				},
																				"RecoveryRequestCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RecoveryRequestCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 662
																				},
																				"RecoveryRequestSummary__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecoveryRequestSummary",
																						"version": 0
																					},
																					"stamp": 116
																				},
																				"LabelRecoveryAbortedRequestCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_RecoveryAbortedRequestCount",
																						"version": 0
																					},
																					"stamp": 663
																				},
																				"RecoveryAbortedRequestCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RecoveryAbortedRequestCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 664
																				},
																				"Spacer_line10__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line10",
																						"version": 0
																					},
																					"stamp": 420
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 432,
													"*": {
														"RecoveryRequestResultDetailsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecoveryRequestResultDetailsPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 426,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 427,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RecoveryRequestErrors__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line11__": {
																						"line": 3,
																						"column": 1
																					},
																					"RecoveryRequestErrorsTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 428,
																			"*": {
																				"RecoveryRequestErrors__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecoveryRequestErrors",
																						"version": 0
																					},
																					"stamp": 118
																				},
																				"RecoveryRequestErrorsTable__": {
																					"type": "Table",
																					"data": [
																						"RecoveryRequestErrorsTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 2,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RecoveryRequestErrorsTable",
																						"readonly": true
																					},
																					"stamp": 828,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 829,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 830
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 831
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 832,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 835,
																									"data": [],
																									"*": {
																										"RecoveryRequestErrorDocRuidEx__": {
																											"type": "Label",
																											"data": [
																												"RecoveryRequestErrorDocRuidEx__"
																											],
																											"options": {
																												"label": "_RecoveryRequestErrorDocRuidEx",
																												"version": 0
																											},
																											"stamp": 836,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 839,
																									"data": [],
																									"*": {
																										"RecoveryRequestErrorDetails__": {
																											"type": "Label",
																											"data": [
																												"RecoveryRequestErrorDetails__"
																											],
																											"options": {
																												"label": "_RecoveryRequestErrorDetails",
																												"version": 0
																											},
																											"stamp": 840,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 833,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 834,
																									"data": [],
																									"*": {
																										"RecoveryRequestErrorDocRuidEx__": {
																											"type": "ShortText",
																											"data": [
																												"RecoveryRequestErrorDocRuidEx__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_RecoveryRequestErrorDocRuidEx",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 837,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 838,
																									"data": [],
																									"*": {
																										"RecoveryRequestErrorDetails__": {
																											"type": "LongText",
																											"data": [
																												"RecoveryRequestErrorDetails__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_RecoveryRequestErrorDetails",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 841,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line11__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line11",
																						"version": 0
																					},
																					"stamp": 429
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-21": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 478,
													"*": {
														"RecoverBudgetSummaryResultPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecoverBudgetSummaryResultPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 436,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"PendingRecoveryCount__": "LabelPendingRecoveryCount__",
																			"LabelPendingRecoveryCount__": "PendingRecoveryCount__",
																			"RecoveryInSuccessCount__": "LabelRecoveryInSuccessCount__",
																			"LabelRecoveryInSuccessCount__": "RecoveryInSuccessCount__",
																			"RecoveryInErrorCount__": "LabelRecoveryInErrorCount__",
																			"LabelRecoveryInErrorCount__": "RecoveryInErrorCount__"
																		},
																		"version": 0
																	},
																	"stamp": 437,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RecoverySummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line12__": {
																						"line": 5,
																						"column": 1
																					},
																					"PendingRecoveryCount__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPendingRecoveryCount__": {
																						"line": 2,
																						"column": 1
																					},
																					"RecoveryInSuccessCount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRecoveryInSuccessCount__": {
																						"line": 3,
																						"column": 1
																					},
																					"RecoveryInErrorCount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRecoveryInErrorCount__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 438,
																			"*": {
																				"RecoverySummary__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecoverySummary",
																						"version": 0
																					},
																					"stamp": 107
																				},
																				"LabelPendingRecoveryCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_PendingRecoveryCount",
																						"version": 0
																					},
																					"stamp": 747
																				},
																				"PendingRecoveryCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_PendingRecoveryCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 748
																				},
																				"LabelRecoveryInSuccessCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_RecoveryInSuccessCount",
																						"version": 0
																					},
																					"stamp": 749
																				},
																				"RecoveryInSuccessCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RecoveryInSuccessCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"autocompletable": false
																					},
																					"stamp": 750
																				},
																				"LabelRecoveryInErrorCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_RecoveryInErrorCount",
																						"version": 0
																					},
																					"stamp": 751
																				},
																				"RecoveryInErrorCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RecoveryInErrorCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 752
																				},
																				"Spacer_line12__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line12",
																						"version": 0
																					},
																					"stamp": 439
																				}
																			}
																		}
																	}
																}
															}
														},
														"PendingRecoveryBudgetPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PendingRecoveryBudgetPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 499,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 500,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"PendingRecoveries__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 3,
																						"column": 1
																					},
																					"PendingRecoveriesTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 501,
																			"*": {
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 470
																				},
																				"PendingRecoveriesTable__": {
																					"type": "Table",
																					"data": [
																						"PendingRecoveriesTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 5,
																						"columns": 1,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_PendingRecoveriesTable",
																						"readonly": true
																					},
																					"stamp": 856,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 857,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 858
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 859
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 860,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 863,
																									"data": [],
																									"*": {
																										"PendingRecoveryDocRuidEx__": {
																											"type": "Label",
																											"data": [
																												"PendingRecoveryDocRuidEx__"
																											],
																											"options": {
																												"label": "_PendingRecoveryDocRuidEx",
																												"version": 0
																											},
																											"stamp": 864,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 861,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 862,
																									"data": [],
																									"*": {
																										"PendingRecoveryDocRuidEx__": {
																											"type": "ShortText",
																											"data": [
																												"PendingRecoveryDocRuidEx__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_PendingRecoveryDocRuidEx",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 865,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"PendingRecoveries__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_PendingRecoveries",
																						"version": 0
																					},
																					"stamp": 449
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 492,
													"*": {
														"RecoverBudgetDetailsResultPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecoverBudgetDetailsResultPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 54,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 55,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RecoveryErrors__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 3,
																						"column": 1
																					},
																					"RecoveryErrorsTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 56,
																			"*": {
																				"RecoveryErrors__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecoveryErrors",
																						"version": 0
																					},
																					"stamp": 134
																				},
																				"RecoveryErrorsTable__": {
																					"type": "Table",
																					"data": [
																						"RecoveryErrorsTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 2,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RecoveryErrorsTable",
																						"readonly": true
																					},
																					"stamp": 842,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 843,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 844
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 845
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 846,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 849,
																									"data": [],
																									"*": {
																										"RecoveryErrorDocRuidEx__": {
																											"type": "Label",
																											"data": [
																												"RecoveryErrorDocRuidEx__"
																											],
																											"options": {
																												"label": "_RecoveryErrorDocRuidEx",
																												"version": 0
																											},
																											"stamp": 850,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 853,
																									"data": [],
																									"*": {
																										"RecoveryErrorDetails__": {
																											"type": "Label",
																											"data": [
																												"RecoveryErrorDetails__"
																											],
																											"options": {
																												"label": "_RecoveryErrorDetails",
																												"version": 0
																											},
																											"stamp": 854,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 847,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 848,
																									"data": [],
																									"*": {
																										"RecoveryErrorDocRuidEx__": {
																											"type": "ShortText",
																											"data": [
																												"RecoveryErrorDocRuidEx__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_RecoveryErrorDocRuidEx",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 851,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 852,
																									"data": [],
																									"*": {
																										"RecoveryErrorDetails__": {
																											"type": "LongText",
																											"data": [
																												"RecoveryErrorDetails__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_RecoveryErrorDetails",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 855,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 440
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 289,
													"*": {
														"BudgetIDListPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_BudgetIDListPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 290,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 291,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"BudgetIDs__": {
																						"line": 2,
																						"column": 1
																					},
																					"ImpactedBudgetIDList__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 292,
																			"*": {
																				"ImpactedBudgetIDList__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_ImpactedBudgetIDList",
																						"version": 0
																					},
																					"stamp": 296
																				},
																				"BudgetIDs__": {
																					"type": "LongText",
																					"data": [
																						"BudgetIDs__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_BudgetIDs",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 10,
																						"browsable": false
																					},
																					"stamp": 294
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 497
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 302,
													"*": {
														"RecomputeBudgetResultSummaryPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecomputeBudgetResultSummaryPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 303,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TotalRecomputeCount__": "LabelTotalRecomputeCount__",
																			"LabelTotalRecomputeCount__": "TotalRecomputeCount__",
																			"RecomputeCountError__": "LabelRecomputeCountError__",
																			"LabelRecomputeCountError__": "RecomputeCountError__"
																		},
																		"version": 0
																	},
																	"stamp": 304,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RecomputeSummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line6__": {
																						"line": 4,
																						"column": 1
																					},
																					"TotalRecomputeCount__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelTotalRecomputeCount__": {
																						"line": 2,
																						"column": 1
																					},
																					"RecomputeCountError__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRecomputeCountError__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 305,
																			"*": {
																				"RecomputeSummary__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecomputeSummary",
																						"version": 0
																					},
																					"stamp": 306
																				},
																				"LabelTotalRecomputeCount__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_TotalRecomputeCount",
																						"version": 0
																					},
																					"stamp": 784
																				},
																				"TotalRecomputeCount__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_TotalRecomputeCount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 785
																				},
																				"LabelRecomputeCountError__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_RecomputeCountError",
																						"version": 0
																					},
																					"stamp": 786
																				},
																				"RecomputeCountError__": {
																					"type": "Integer",
																					"data": [],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RecomputeCountError",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "70",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "0",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Number",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 787
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 311
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 312,
													"*": {
														"RecomputeBudgetResultDetailsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecomputeBudgetResultDetailsPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 313,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 314,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line9__": {
																						"line": 3,
																						"column": 1
																					},
																					"RecomputeErrors__": {
																						"line": 1,
																						"column": 1
																					},
																					"RecomputeBudgetTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 315,
																			"*": {
																				"RecomputeErrors__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RecomputeErrors",
																						"version": 0
																					},
																					"stamp": 326
																				},
																				"RecomputeBudgetTable__": {
																					"type": "Table",
																					"data": [
																						"RecomputeBudgetTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 3,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RecomputeBudgetTable",
																						"readonly": true
																					},
																					"stamp": 866,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 867,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 868
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 869
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 870,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 873,
																									"data": [],
																									"*": {
																										"RecomputeBudgetID__": {
																											"type": "Label",
																											"data": [
																												"RecomputeBudgetID__"
																											],
																											"options": {
																												"label": "_RecomputeBudgetID",
																												"version": 0
																											},
																											"stamp": 874,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 877,
																									"data": [],
																									"*": {
																										"RecomputeErrorType__": {
																											"type": "Label",
																											"data": [
																												"RecomputeErrorType__"
																											],
																											"options": {
																												"label": "_RecomputeErrorType",
																												"version": 0
																											},
																											"stamp": 878,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 881,
																									"data": [],
																									"*": {
																										"RecomputeErrorDetails__": {
																											"type": "Label",
																											"data": [
																												"RecomputeErrorDetails__"
																											],
																											"options": {
																												"label": "_RecomputeErrorDetails",
																												"version": 0
																											},
																											"stamp": 882,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 871,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 872,
																									"data": [],
																									"*": {
																										"RecomputeBudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"RecomputeBudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_RecomputeBudgetID",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 875,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 876,
																									"data": [],
																									"*": {
																										"RecomputeErrorType__": {
																											"type": "ShortText",
																											"data": [
																												"RecomputeErrorType__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_RecomputeErrorType",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 879,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 880,
																									"data": [],
																									"*": {
																										"RecomputeErrorDetails__": {
																											"type": "LongText",
																											"data": [
																												"RecomputeErrorDetails__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_RecomputeErrorDetails",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 883,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line9__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line9",
																						"version": 0
																					},
																					"stamp": 327
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-28": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 618,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 18,
															"data": []
														}
													}
												},
												"form-content-left-30": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 551,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 20,
															"data": []
														}
													}
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 100,
													"width": 0,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": true
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Previous": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_PreviousStep",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 62
						},
						"Next": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_NextStep",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 63
						},
						"CheckBudgetButton": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_CheckBudgetButton",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 61
						},
						"RecoverBudgetButton": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_RecoverBudgetButton",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 64
						},
						"RecomputeBudgetButton": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_RecomputeBudgetButton",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 297
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 30,
							"data": []
						},
						"TerminateButton": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_TerminateButton",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 3,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 341
						}
					},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 905,
	"data": []
}