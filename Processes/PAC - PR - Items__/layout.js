{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"PRNumber__": "LabelPRNumber__",
																	"LabelPRNumber__": "PRNumber__",
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__",
																	"CatalogReference__": "LabelCatalogReference__",
																	"LabelCatalogReference__": "CatalogReference__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"CostCenterId__": "LabelCostCenterId__",
																	"LabelCostCenterId__": "CostCenterId__",
																	"Quantity__": "LabelQuantity__",
																	"LabelQuantity__": "Quantity__",
																	"OrderedQuantity__": "LabelOrderedQuantity__",
																	"LabelOrderedQuantity__": "OrderedQuantity__",
																	"ReceivedQuantity__": "LabelReceivedQuantity__",
																	"LabelReceivedQuantity__": "ReceivedQuantity__",
																	"UnitPrice__": "LabelUnitPrice__",
																	"LabelUnitPrice__": "UnitPrice__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"NetAmount__": "LabelNetAmount__",
																	"LabelNetAmount__": "NetAmount__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelGLAccount__": "GLAccount__",
																	"VendorName__": "LabelVendorName__",
																	"LabelVendorName__": "VendorName__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"SupplyTypeName__": "LabelSupplyTypeName__",
																	"LabelSupplyTypeName__": "SupplyTypeName__",
																	"SupplyTypeId__": "LabelSupplyTypeId__",
																	"LabelSupplyTypeId__": "SupplyTypeId__",
																	"CompletelyOrdered__": "LabelCompletelyOrdered__",
																	"LabelCompletelyOrdered__": "CompletelyOrdered__",
																	"CostCenterName__": "LabelCostCenterName__",
																	"LabelCostCenterName__": "CostCenterName__",
																	"RequestedDeliveryDate__": "LabelRequestedDeliveryDate__",
																	"LabelRequestedDeliveryDate__": "RequestedDeliveryDate__",
																	"PRRUIDEX__": "LabelPRRUIDEX__",
																	"LabelPRRUIDEX__": "PRRUIDEX__",
																	"RecipientDN__": "LabelRecipientDN__",
																	"LabelRecipientDN__": "RecipientDN__",
																	"RequesterDN__": "LabelRequesterDN__",
																	"LabelRequesterDN__": "RequesterDN__",
																	"BuyerDN__": "LabelBuyerDN__",
																	"LabelBuyerDN__": "BuyerDN__",
																	"BuyerLogin__": "LabelBuyerLogin__",
																	"LabelBuyerLogin__": "BuyerLogin__",
																	"Status__": "LabelStatus__",
																	"LabelStatus__": "Status__",
																	"DeliveryAddressID__": "LabelDeliveryAddressID__",
																	"LabelDeliveryAddressID__": "DeliveryAddressID__",
																	"ExchangeRate__": "LabelExchangeRate__",
																	"LabelExchangeRate__": "ExchangeRate__",
																	"PurchasingGroup__": "LabelPurchasingGroup__",
																	"LabelPurchasingGroup__": "PurchasingGroup__",
																	"PurchasingOrganization__": "LabelPurchasingOrganization__",
																	"LabelPurchasingOrganization__": "PurchasingOrganization__",
																	"ShipToAddress__": "LabelShipToAddress__",
																	"LabelShipToAddress__": "ShipToAddress__",
																	"ShipToCompany__": "LabelShipToCompany__",
																	"LabelShipToCompany__": "ShipToCompany__",
																	"ShipToContact__": "LabelShipToContact__",
																	"LabelShipToContact__": "ShipToContact__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"TaxRate__": "LabelTaxRate__",
																	"LabelTaxRate__": "TaxRate__",
																	"RequestedDeliveryDateInPast__": "LabelRequestedDeliveryDateInPast__",
																	"LabelRequestedDeliveryDateInPast__": "RequestedDeliveryDateInPast__",
																	"ItemInCxml__": "LabelItemInCxml__",
																	"LabelItemInCxml__": "ItemInCxml__",
																	"Reason__": "LabelReason__",
																	"LabelReason__": "Reason__",
																	"ShipToEmail__": "LabelShipToEmail__",
																	"LabelShipToEmail__": "ShipToEmail__",
																	"ShipToPhone__": "LabelShipToPhone__",
																	"LabelShipToPhone__": "ShipToPhone__",
																	"ItemUnit__": "LabelItemUnit__",
																	"LabelItemUnit__": "ItemUnit__",
																	"ItemUnitDescription__": "LabelItemUnitDescription__",
																	"LabelItemUnitDescription__": "ItemUnitDescription__",
																	"NoGoodsReceipt__": "LabelNoGoodsReceipt__",
																	"LabelNoGoodsReceipt__": "NoGoodsReceipt__",
																	"Locked__": "LabelLocked__",
																	"LabelLocked__": "Locked__",
																	"LeadTime__": "LabelLeadTime__",
																	"LabelLeadTime__": "LeadTime__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__",
																	"TechnicalData__": "LabelTechnicalData__",
																	"LabelTechnicalData__": "TechnicalData__",
																	"ItemType__": "LabelItemType__",
																	"LabelItemType__": "ItemType__",
																	"ItemReceivedAmount__": "LabelItemReceivedAmount__",
																	"LabelItemReceivedAmount__": "ItemReceivedAmount__",
																	"ItemOrderedAmount__": "LabelItemOrderedAmount__",
																	"LabelItemOrderedAmount__": "ItemOrderedAmount__",
																	"ProjectCode__": "LabelProjectCode__",
																	"LabelProjectCode__": "ProjectCode__",
																	"ProjectCodeDescription__": "LabelProjectCodeDescription__",
																	"LabelProjectCodeDescription__": "ProjectCodeDescription__",
																	"WBSElement__": "LabelWBSElement__",
																	"LabelWBSElement__": "WBSElement__",
																	"InternalOrder__": "LabelInternalOrder__",
																	"LabelInternalOrder__": "InternalOrder__",
																	"WBSElementID__": "LabelWBSElementID__",
																	"LabelWBSElementID__": "WBSElementID__",
																	"CanceledQuantity__": "LabelCanceledQuantity__",
																	"LabelCanceledQuantity__": "CanceledQuantity__",
																	"CanceledAmount__": "LabelCanceledAmount__",
																	"LabelCanceledAmount__": "CanceledAmount__",
																	"PRSubmissionDateTime__": "LabelPRSubmissionDateTime__",
																	"LabelPRSubmissionDateTime__": "PRSubmissionDateTime__",
																	"SupplyTypeFullpath__": "LabelSupplyTypeFullpath__",
																	"LabelSupplyTypeFullpath__": "SupplyTypeFullpath__",
																	"FreeDimension1__": "LabelFreeDimension1__",
																	"LabelFreeDimension1__": "FreeDimension1__",
																	"FreeDimension1ID__": "LabelFreeDimension1ID__",
																	"LabelFreeDimension1ID__": "FreeDimension1ID__",
																	"StartDate__": "LabelStartDate__",
																	"LabelStartDate__": "StartDate__",
																	"EndDate__": "LabelEndDate__",
																	"LabelEndDate__": "EndDate__",
																	"QuantityTakenFromStock__": "LabelQuantityTakenFromStock__",
																	"LabelQuantityTakenFromStock__": "QuantityTakenFromStock__",
																	"AmountTakenFromStock__": "LabelAmountTakenFromStock__",
																	"LabelAmountTakenFromStock__": "AmountTakenFromStock__",
																	"IsReplenishmentItem__": "LabelIsReplenishmentItem__",
																	"LabelIsReplenishmentItem__": "IsReplenishmentItem__",
																	"WarehouseID__": "LabelWarehouseID__",
																	"LabelWarehouseID__": "WarehouseID__",
																	"WarehouseName__": "LabelWarehouseName__",
																	"LabelWarehouseName__": "WarehouseName__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 70,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"PRNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelPRNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"LineNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"Status__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelStatus__": {
																				"line": 4,
																				"column": 1
																			},
																			"ItemType__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelItemType__": {
																				"line": 5,
																				"column": 1
																			},
																			"CatalogReference__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCatalogReference__": {
																				"line": 6,
																				"column": 1
																			},
																			"Description__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 7,
																				"column": 1
																			},
																			"CostCenterName__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCostCenterName__": {
																				"line": 8,
																				"column": 1
																			},
																			"CostCenterId__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelCostCenterId__": {
																				"line": 9,
																				"column": 1
																			},
																			"ProjectCode__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelProjectCode__": {
																				"line": 10,
																				"column": 1
																			},
																			"ProjectCodeDescription__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelProjectCodeDescription__": {
																				"line": 11,
																				"column": 1
																			},
																			"InternalOrder__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelInternalOrder__": {
																				"line": 12,
																				"column": 1
																			},
																			"WBSElement__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelWBSElement__": {
																				"line": 13,
																				"column": 1
																			},
																			"WBSElementID__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelWBSElementID__": {
																				"line": 14,
																				"column": 1
																			},
																			"FreeDimension1__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelFreeDimension1__": {
																				"line": 15,
																				"column": 1
																			},
																			"Quantity__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelQuantity__": {
																				"line": 17,
																				"column": 1
																			},
																			"OrderedQuantity__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelOrderedQuantity__": {
																				"line": 20,
																				"column": 1
																			},
																			"ReceivedQuantity__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelReceivedQuantity__": {
																				"line": 21,
																				"column": 1
																			},
																			"ItemUnit__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelItemUnit__": {
																				"line": 22,
																				"column": 1
																			},
																			"ItemUnitDescription__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelItemUnitDescription__": {
																				"line": 23,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 24,
																				"column": 1
																			},
																			"ExchangeRate__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelExchangeRate__": {
																				"line": 25,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 26,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 26,
																				"column": 1
																			},
																			"TaxRate__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelTaxRate__": {
																				"line": 27,
																				"column": 1
																			},
																			"UnitPrice__": {
																				"line": 28,
																				"column": 2
																			},
																			"LabelUnitPrice__": {
																				"line": 28,
																				"column": 1
																			},
																			"NetAmount__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelNetAmount__": {
																				"line": 29,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 35,
																				"column": 2
																			},
																			"ItemOrderedAmount__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelItemOrderedAmount__": {
																				"line": 32,
																				"column": 1
																			},
																			"ItemReceivedAmount__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelItemReceivedAmount__": {
																				"line": 33,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 34,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 34,
																				"column": 1
																			},
																			"LabelGLAccount__": {
																				"line": 35,
																				"column": 1
																			},
																			"Group__": {
																				"line": 36,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 36,
																				"column": 1
																			},
																			"VendorName__": {
																				"line": 37,
																				"column": 2
																			},
																			"LabelVendorName__": {
																				"line": 37,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 38,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 38,
																				"column": 1
																			},
																			"RequestedDeliveryDate__": {
																				"line": 41,
																				"column": 2
																			},
																			"LabelRequestedDeliveryDate__": {
																				"line": 41,
																				"column": 1
																			},
																			"RequestedDeliveryDateInPast__": {
																				"line": 43,
																				"column": 2
																			},
																			"LabelRequestedDeliveryDateInPast__": {
																				"line": 43,
																				"column": 1
																			},
																			"SupplyTypeName__": {
																				"line": 44,
																				"column": 2
																			},
																			"LabelSupplyTypeName__": {
																				"line": 44,
																				"column": 1
																			},
																			"SupplyTypeId__": {
																				"line": 45,
																				"column": 2
																			},
																			"LabelSupplyTypeId__": {
																				"line": 45,
																				"column": 1
																			},
																			"PurchasingGroup__": {
																				"line": 47,
																				"column": 2
																			},
																			"LabelPurchasingGroup__": {
																				"line": 47,
																				"column": 1
																			},
																			"PurchasingOrganization__": {
																				"line": 48,
																				"column": 2
																			},
																			"LabelPurchasingOrganization__": {
																				"line": 48,
																				"column": 1
																			},
																			"DeliveryAddressID__": {
																				"line": 49,
																				"column": 2
																			},
																			"LabelDeliveryAddressID__": {
																				"line": 49,
																				"column": 1
																			},
																			"ShipToAddress__": {
																				"line": 50,
																				"column": 2
																			},
																			"LabelShipToAddress__": {
																				"line": 50,
																				"column": 1
																			},
																			"ShipToPhone__": {
																				"line": 51,
																				"column": 2
																			},
																			"LabelShipToPhone__": {
																				"line": 51,
																				"column": 1
																			},
																			"ShipToEmail__": {
																				"line": 52,
																				"column": 2
																			},
																			"LabelShipToEmail__": {
																				"line": 52,
																				"column": 1
																			},
																			"ShipToCompany__": {
																				"line": 53,
																				"column": 2
																			},
																			"LabelShipToCompany__": {
																				"line": 53,
																				"column": 1
																			},
																			"ShipToContact__": {
																				"line": 54,
																				"column": 2
																			},
																			"LabelShipToContact__": {
																				"line": 54,
																				"column": 1
																			},
																			"CompletelyOrdered__": {
																				"line": 55,
																				"column": 2
																			},
																			"LabelCompletelyOrdered__": {
																				"line": 55,
																				"column": 1
																			},
																			"RequesterDN__": {
																				"line": 56,
																				"column": 2
																			},
																			"LabelRequesterDN__": {
																				"line": 56,
																				"column": 1
																			},
																			"Reason__": {
																				"line": 57,
																				"column": 2
																			},
																			"LabelReason__": {
																				"line": 57,
																				"column": 1
																			},
																			"BuyerDN__": {
																				"line": 58,
																				"column": 2
																			},
																			"LabelBuyerDN__": {
																				"line": 58,
																				"column": 1
																			},
																			"BuyerLogin__": {
																				"line": 59,
																				"column": 2
																			},
																			"LabelBuyerLogin__": {
																				"line": 59,
																				"column": 1
																			},
																			"RecipientDN__": {
																				"line": 60,
																				"column": 2
																			},
																			"LabelRecipientDN__": {
																				"line": 60,
																				"column": 1
																			},
																			"PRRUIDEX__": {
																				"line": 61,
																				"column": 2
																			},
																			"LabelPRRUIDEX__": {
																				"line": 61,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 62,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 62,
																				"column": 1
																			},
																			"ItemInCxml__": {
																				"line": 63,
																				"column": 2
																			},
																			"LabelItemInCxml__": {
																				"line": 63,
																				"column": 1
																			},
																			"NoGoodsReceipt__": {
																				"line": 64,
																				"column": 2
																			},
																			"LabelNoGoodsReceipt__": {
																				"line": 64,
																				"column": 1
																			},
																			"Locked__": {
																				"line": 65,
																				"column": 2
																			},
																			"LabelLocked__": {
																				"line": 65,
																				"column": 1
																			},
																			"LeadTime__": {
																				"line": 66,
																				"column": 2
																			},
																			"LabelLeadTime__": {
																				"line": 66,
																				"column": 1
																			},
																			"TechnicalData__": {
																				"line": 67,
																				"column": 2
																			},
																			"LabelTechnicalData__": {
																				"line": 67,
																				"column": 1
																			},
																			"CanceledQuantity__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelCanceledQuantity__": {
																				"line": 19,
																				"column": 1
																			},
																			"CanceledAmount__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelCanceledAmount__": {
																				"line": 31,
																				"column": 1
																			},
																			"PRSubmissionDateTime__": {
																				"line": 42,
																				"column": 2
																			},
																			"LabelPRSubmissionDateTime__": {
																				"line": 42,
																				"column": 1
																			},
																			"SupplyTypeFullpath__": {
																				"line": 46,
																				"column": 2
																			},
																			"LabelSupplyTypeFullpath__": {
																				"line": 46,
																				"column": 1
																			},
																			"FreeDimension1ID__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelFreeDimension1ID__": {
																				"line": 16,
																				"column": 1
																			},
																			"StartDate__": {
																				"line": 39,
																				"column": 2
																			},
																			"LabelStartDate__": {
																				"line": 39,
																				"column": 1
																			},
																			"EndDate__": {
																				"line": 40,
																				"column": 2
																			},
																			"LabelEndDate__": {
																				"line": 40,
																				"column": 1
																			},
																			"QuantityTakenFromStock__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelQuantityTakenFromStock__": {
																				"line": 18,
																				"column": 1
																			},
																			"AmountTakenFromStock__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelAmountTakenFromStock__": {
																				"line": 30,
																				"column": 1
																			},
																			"IsReplenishmentItem__": {
																				"line": 68,
																				"column": 2
																			},
																			"LabelIsReplenishmentItem__": {
																				"line": 68,
																				"column": 1
																			},
																			"WarehouseID__": {
																				"line": 69,
																				"column": 2
																			},
																			"LabelWarehouseID__": {
																				"line": 69,
																				"column": 1
																			},
																			"WarehouseName__": {
																				"line": 70,
																				"column": 2
																			},
																			"LabelWarehouseName__": {
																				"line": 70,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 4
																		},
																		"LabelPRNumber__": {
																			"type": "Label",
																			"data": [
																				"PRNumber__"
																			],
																			"options": {
																				"label": "_PRNumber",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"PRNumber__": {
																			"type": "ShortText",
																			"data": [
																				"PRNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PRNumber",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 6
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"LineNumber__": {
																			"type": "Integer",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_LineNumber",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": false,
																				"browsable": false
																			},
																			"stamp": 8
																		},
																		"LabelStatus__": {
																			"type": "Label",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"label": "_Status",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Status__": {
																			"type": "ComboBox",
																			"data": [
																				"Status__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Requisition to complete",
																					"1": "_Requisition to review",
																					"2": "_Requisition to approve",
																					"3": "_Requisition to order",
																					"4": "_Requisition waiting for deliver",
																					"5": "_Requisition delivered",
																					"6": "_Requisition canceled",
																					"7": "_Requisition rejected"
																				},
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "To complete",
																					"1": "To review",
																					"2": "To approve",
																					"3": "To order",
																					"4": "To receive",
																					"5": "Received",
																					"6": "Canceled",
																					"7": "Rejected"
																				},
																				"label": "_Status",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 1,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 10
																		},
																		"LabelItemType__": {
																			"type": "Label",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"label": "_ItemType",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"ItemType__": {
																			"type": "ComboBox",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_QuantityBased",
																					"1": "_AmountBased"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "QuantityBased",
																					"1": "AmountBased"
																				},
																				"label": "_ItemType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 12
																		},
																		"LabelCatalogReference__": {
																			"type": "Label",
																			"data": [
																				"CatalogReference__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"CatalogReference__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CatalogReference__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 14
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_ItemDescription",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"Description__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"version": 1,
																				"minNbLines": 1,
																				"maxNbLines": 999,
																				"label": "_ItemDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 16
																		},
																		"LabelCostCenterName__": {
																			"type": "Label",
																			"data": [
																				"CostCenterName__"
																			],
																			"options": {
																				"label": "_CostCenterName",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"CostCenterName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenterName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CostCenterName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 18
																		},
																		"LabelCostCenterId__": {
																			"type": "Label",
																			"data": [
																				"CostCenterId__"
																			],
																			"options": {
																				"label": "_CostCenterId",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"CostCenterId__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenterId__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CostCenterId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 20
																		},
																		"LabelProjectCode__": {
																			"type": "Label",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"label": "_ProjectCode",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"ProjectCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 22
																		},
																		"LabelProjectCodeDescription__": {
																			"type": "Label",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"label": "_ProjectCodeDescription",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"ProjectCodeDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ProjectCodeDescription__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ProjectCodeDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 24
																		},
																		"LabelInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"label": "_InternalOrder",
																				"hidden": false,
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"InternalOrder__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_InternalOrder",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"hidden": false,
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 26
																		},
																		"LabelWBSElement__": {
																			"type": "Label",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"label": "_WBSElement",
																				"hidden": false,
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"WBSElement__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElement",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"hidden": false,
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 28
																		},
																		"LabelWBSElementID__": {
																			"type": "Label",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"label": "_WBSElementID",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"WBSElementID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElementID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 30
																		},
																		"LabelFreeDimension1__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"label": "_FreeDimension1",
																				"version": 0
																			},
																			"stamp": 136
																		},
																		"FreeDimension1__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_FreeDimension1",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 500
																			},
																			"stamp": 137
																		},
																		"LabelFreeDimension1ID__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"label": "_FreeDimension1ID",
																				"version": 0
																			},
																			"stamp": 138
																		},
																		"FreeDimension1ID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_FreeDimension1ID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 500
																			},
																			"stamp": 139
																		},
																		"LabelQuantity__": {
																			"type": "Label",
																			"data": [
																				"Quantity__"
																			],
																			"options": {
																				"label": "_Quantity",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"Quantity__": {
																			"type": "Decimal",
																			"data": [
																				"Quantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Quantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 32
																		},
																		"LabelQuantityTakenFromStock__": {
																			"type": "Label",
																			"data": [
																				"QuantityTakenFromStock__"
																			],
																			"options": {
																				"label": "_QuantityTakenFromStock",
																				"version": 0
																			},
																			"stamp": 144
																		},
																		"QuantityTakenFromStock__": {
																			"type": "Decimal",
																			"data": [
																				"QuantityTakenFromStock__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_QuantityTakenFromStock",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 145
																		},
																		"LabelCanceledQuantity__": {
																			"type": "Label",
																			"data": [
																				"CanceledQuantity__"
																			],
																			"options": {
																				"label": "_CanceledQuantity",
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"CanceledQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"CanceledQuantity__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_CanceledQuantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 34
																		},
																		"LabelOrderedQuantity__": {
																			"type": "Label",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"label": "_OrderedQuantity",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"OrderedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_OrderedQuantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 36
																		},
																		"LabelReceivedQuantity__": {
																			"type": "Label",
																			"data": [
																				"ReceivedQuantity__"
																			],
																			"options": {
																				"label": "_ReceivedQuantity",
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"ReceivedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"ReceivedQuantity__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ReceivedQuantity",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 38
																		},
																		"LabelItemUnit__": {
																			"type": "Label",
																			"data": [
																				"ItemUnit__"
																			],
																			"options": {
																				"label": "_ItemUnit",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"ItemUnit__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnit__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnit",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"length": 3,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 40
																		},
																		"LabelItemUnitDescription__": {
																			"type": "Label",
																			"data": [
																				"ItemUnitDescription__"
																			],
																			"options": {
																				"label": "_ItemUnitDescription",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"ItemUnitDescription__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemUnitDescription__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_ItemUnitDescription",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 42
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "_Currency",
																				"version": 0
																			},
																			"stamp": 43
																		},
																		"Currency__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Currency",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 44
																		},
																		"LabelExchangeRate__": {
																			"type": "Label",
																			"data": [
																				"ExchangeRate__"
																			],
																			"options": {
																				"label": "_ExchangeRate",
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"ExchangeRate__": {
																			"type": "Decimal",
																			"data": [
																				"ExchangeRate__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ExchangeRate",
																				"precision_internal": 6,
																				"precision_current": 6,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 46
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"TaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"length": 600,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 48
																		},
																		"LabelTaxRate__": {
																			"type": "Label",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"TaxRate__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 50
																		},
																		"LabelUnitPrice__": {
																			"type": "Label",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"label": "_UnitPrice",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"UnitPrice__": {
																			"type": "Decimal",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_UnitPrice",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 52
																		},
																		"LabelNetAmount__": {
																			"type": "Label",
																			"data": [
																				"NetAmount__"
																			],
																			"options": {
																				"label": "_NetAmount",
																				"version": 0
																			},
																			"stamp": 53
																		},
																		"NetAmount__": {
																			"type": "Decimal",
																			"data": [
																				"NetAmount__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_NetAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 54
																		},
																		"LabelAmountTakenFromStock__": {
																			"type": "Label",
																			"data": [
																				"AmountTakenFromStock__"
																			],
																			"options": {
																				"label": "_AmountTakenFromStock",
																				"version": 0
																			},
																			"stamp": 146
																		},
																		"AmountTakenFromStock__": {
																			"type": "Decimal",
																			"data": [
																				"AmountTakenFromStock__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_AmountTakenFromStock",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 147
																		},
																		"LabelCanceledAmount__": {
																			"type": "Label",
																			"data": [
																				"CanceledAmount__"
																			],
																			"options": {
																				"label": "_CanceledAmount",
																				"version": 0
																			},
																			"stamp": 55
																		},
																		"CanceledAmount__": {
																			"type": "Decimal",
																			"data": [
																				"CanceledAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_CanceledAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 56
																		},
																		"LabelItemOrderedAmount__": {
																			"type": "Label",
																			"data": [
																				"ItemOrderedAmount__"
																			],
																			"options": {
																				"label": "_ItemOrderedAmount",
																				"version": 0
																			},
																			"stamp": 57
																		},
																		"ItemOrderedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"ItemOrderedAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ItemOrderedAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"autocompletable": false
																			},
																			"stamp": 58
																		},
																		"LabelItemReceivedAmount__": {
																			"type": "Label",
																			"data": [
																				"ItemReceivedAmount__"
																			],
																			"options": {
																				"label": "_ItemReceivedAmount",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"ItemReceivedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"ItemReceivedAmount__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ItemReceivedAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"autocompletable": false
																			},
																			"stamp": 60
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 62
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"version": 0
																			},
																			"stamp": 63
																		},
																		"GLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_GLAccount",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 64
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 66
																		},
																		"LabelVendorName__": {
																			"type": "Label",
																			"data": [
																				"VendorName__"
																			],
																			"options": {
																				"label": "_VendorName",
																				"version": 0
																			},
																			"stamp": 67
																		},
																		"VendorName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_VendorName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 68
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 69
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 70
																		},
																		"LabelStartDate__": {
																			"type": "Label",
																			"data": [
																				"StartDate__"
																			],
																			"options": {
																				"label": "_StartDate",
																				"version": 0
																			},
																			"stamp": 140
																		},
																		"StartDate__": {
																			"type": "DateTime",
																			"data": [
																				"StartDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_StartDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 141
																		},
																		"LabelEndDate__": {
																			"type": "Label",
																			"data": [
																				"EndDate__"
																			],
																			"options": {
																				"label": "_EndDate",
																				"version": 0
																			},
																			"stamp": 142
																		},
																		"EndDate__": {
																			"type": "DateTime",
																			"data": [
																				"EndDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_EndDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 143
																		},
																		"LabelRequestedDeliveryDate__": {
																			"type": "Label",
																			"data": [
																				"RequestedDeliveryDate__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDate",
																				"version": 0
																			},
																			"stamp": 71
																		},
																		"RequestedDeliveryDate__": {
																			"type": "DateTime",
																			"data": [
																				"RequestedDeliveryDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_RequestedDeliveryDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 72
																		},
																		"LabelPRSubmissionDateTime__": {
																			"type": "Label",
																			"data": [
																				"PRSubmissionDateTime__"
																			],
																			"options": {
																				"label": "_PRSubmissionDateTime",
																				"hidden": false,
																				"version": 0
																			},
																			"stamp": 73
																		},
																		"PRSubmissionDateTime__": {
																			"type": "DateTime",
																			"data": [
																				"PRSubmissionDateTime__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_PRSubmissionDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"hidden": false,
																				"readonly": true,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 74
																		},
																		"LabelRequestedDeliveryDateInPast__": {
																			"type": "Label",
																			"data": [
																				"RequestedDeliveryDateInPast__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDateInPast",
																				"version": 0
																			},
																			"stamp": 75
																		},
																		"RequestedDeliveryDateInPast__": {
																			"type": "CheckBox",
																			"data": [
																				"RequestedDeliveryDateInPast__"
																			],
																			"options": {
																				"label": "_RequestedDeliveryDateInPast",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 76
																		},
																		"LabelSupplyTypeName__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeName__"
																			],
																			"options": {
																				"label": "_SupplyTypeName",
																				"version": 0
																			},
																			"stamp": 77
																		},
																		"SupplyTypeName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyTypeName__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_SupplyTypeName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 78
																		},
																		"LabelSupplyTypeId__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"label": "_SupplyTypeId",
																				"version": 0
																			},
																			"stamp": 79
																		},
																		"SupplyTypeId__": {
																			"type": "ShortText",
																			"data": [
																				"SupplyTypeId__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_SupplyTypeId",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"readonly": true
																			},
																			"stamp": 80
																		},
																		"LabelSupplyTypeFullpath__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeFullpath__"
																			],
																			"options": {
																				"label": "_SupplyTypeFullpath",
																				"version": 0
																			},
																			"stamp": 81
																		},
																		"SupplyTypeFullpath__": {
																			"type": "ShortText",
																			"data": [
																				"SupplyTypeFullpath__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SupplyTypeFullpath",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"length": 500,
																				"browsable": false
																			},
																			"stamp": 82
																		},
																		"LabelPurchasingGroup__": {
																			"type": "Label",
																			"data": [
																				"PurchasingGroup__"
																			],
																			"options": {
																				"label": "_PurchasingGroup",
																				"version": 0
																			},
																			"stamp": 83
																		},
																		"PurchasingGroup__": {
																			"type": "ShortText",
																			"data": [
																				"PurchasingGroup__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PurchasingGroup",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 84
																		},
																		"LabelPurchasingOrganization__": {
																			"type": "Label",
																			"data": [
																				"PurchasingOrganization__"
																			],
																			"options": {
																				"label": "_PurchasingOrganization",
																				"version": 0
																			},
																			"stamp": 85
																		},
																		"PurchasingOrganization__": {
																			"type": "ShortText",
																			"data": [
																				"PurchasingOrganization__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PurchasingOrganization",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 86
																		},
																		"LabelDeliveryAddressID__": {
																			"type": "Label",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"label": "_DeliveryAddressID",
																				"version": 0
																			},
																			"stamp": 87
																		},
																		"DeliveryAddressID__": {
																			"type": "ShortText",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_DeliveryAddressID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 88
																		},
																		"LabelShipToAddress__": {
																			"type": "Label",
																			"data": [
																				"ShipToAddress__"
																			],
																			"options": {
																				"label": "_ShipToAddress",
																				"version": 0
																			},
																			"stamp": 89
																		},
																		"ShipToAddress__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToAddress__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ShipToAddress",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 90
																		},
																		"LabelShipToPhone__": {
																			"type": "Label",
																			"data": [
																				"ShipToPhone__"
																			],
																			"options": {
																				"label": "_Ship to phone",
																				"version": 0
																			},
																			"stamp": 91
																		},
																		"ShipToPhone__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToPhone__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Ship to phone",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 92
																		},
																		"LabelShipToEmail__": {
																			"type": "Label",
																			"data": [
																				"ShipToEmail__"
																			],
																			"options": {
																				"label": "_Ship to email",
																				"version": 0
																			},
																			"stamp": 93
																		},
																		"ShipToEmail__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToEmail__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Ship to email",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 250
																			},
																			"stamp": 94
																		},
																		"LabelShipToCompany__": {
																			"type": "Label",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"label": "_ShipToCompany",
																				"version": 0
																			},
																			"stamp": 95
																		},
																		"ShipToCompany__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ShipToCompany",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 96
																		},
																		"LabelShipToContact__": {
																			"type": "Label",
																			"data": [
																				"ShipToContact__"
																			],
																			"options": {
																				"label": "_ShipToContact",
																				"version": 0
																			},
																			"stamp": 97
																		},
																		"ShipToContact__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToContact__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ShipToContact",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 98
																		},
																		"LabelCompletelyOrdered__": {
																			"type": "Label",
																			"data": [
																				"CompletelyOrdered__"
																			],
																			"options": {
																				"label": "_CompletelyOrdered",
																				"version": 0
																			},
																			"stamp": 99
																		},
																		"CompletelyOrdered__": {
																			"type": "CheckBox",
																			"data": [
																				"CompletelyOrdered__"
																			],
																			"options": {
																				"label": "_CompletelyOrdered",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 100
																		},
																		"LabelRequesterDN__": {
																			"type": "Label",
																			"data": [
																				"RequesterDN__"
																			],
																			"options": {
																				"label": "_RequesterDN",
																				"version": 0
																			},
																			"stamp": 101
																		},
																		"RequesterDN__": {
																			"type": "ShortText",
																			"data": [
																				"RequesterDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RequesterDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0,
																				"length": 500
																			},
																			"stamp": 102
																		},
																		"LabelReason__": {
																			"type": "Label",
																			"data": [
																				"Reason__"
																			],
																			"options": {
																				"label": "_Reason",
																				"version": 0
																			},
																			"stamp": 103
																		},
																		"Reason__": {
																			"type": "LongText",
																			"data": [
																				"Reason__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Reason",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0,
																				"minNbLines": 3
																			},
																			"stamp": 104
																		},
																		"LabelBuyerDN__": {
																			"type": "Label",
																			"data": [
																				"BuyerDN__"
																			],
																			"options": {
																				"label": "_BuyerDN",
																				"version": 0
																			},
																			"stamp": 105
																		},
																		"BuyerDN__": {
																			"type": "ShortText",
																			"data": [
																				"BuyerDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BuyerDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"LabelBuyerLogin__": {
																			"type": "Label",
																			"data": [
																				"BuyerLogin__"
																			],
																			"options": {
																				"label": "_BuyerLogin",
																				"version": 0
																			},
																			"stamp": 107
																		},
																		"BuyerLogin__": {
																			"type": "ShortText",
																			"data": [
																				"BuyerLogin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BuyerLogin",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 108
																		},
																		"LabelRecipientDN__": {
																			"type": "Label",
																			"data": [
																				"RecipientDN__"
																			],
																			"options": {
																				"label": "_RecipientDN",
																				"version": 0
																			},
																			"stamp": 109
																		},
																		"RecipientDN__": {
																			"type": "ShortText",
																			"data": [
																				"RecipientDN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_RecipientDN",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 110
																		},
																		"LabelPRRUIDEX__": {
																			"type": "Label",
																			"data": [
																				"PRRUIDEX__"
																			],
																			"options": {
																				"label": "_PRRUIDEX",
																				"version": 0
																			},
																			"stamp": 111
																		},
																		"PRRUIDEX__": {
																			"type": "ShortText",
																			"data": [
																				"PRRUIDEX__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PRRUIDEX",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0,
																				"readonly": true
																			},
																			"stamp": 112
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "_BudgetID",
																				"version": 0
																			},
																			"stamp": 113
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BudgetID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 114
																		},
																		"LabelItemInCxml__": {
																			"type": "Label",
																			"data": [
																				"ItemInCxml__"
																			],
																			"options": {
																				"label": "ItemInCxml",
																				"version": 0
																			},
																			"stamp": 115
																		},
																		"ItemInCxml__": {
																			"type": "LongText",
																			"data": [
																				"ItemInCxml__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ItemInCxml",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"resizable": true,
																				"browsable": false,
																				"version": 0,
																				"minNbLines": 1
																			},
																			"stamp": 116
																		},
																		"LabelNoGoodsReceipt__": {
																			"type": "Label",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"version": 0
																			},
																			"stamp": 117
																		},
																		"NoGoodsReceipt__": {
																			"type": "CheckBox",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 118
																		},
																		"LabelLocked__": {
																			"type": "Label",
																			"data": [
																				"Locked__"
																			],
																			"options": {
																				"label": "_Locked",
																				"version": 0
																			},
																			"stamp": 119
																		},
																		"Locked__": {
																			"type": "CheckBox",
																			"data": [
																				"Locked__"
																			],
																			"options": {
																				"label": "_Locked",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 120
																		},
																		"LabelLeadTime__": {
																			"type": "Label",
																			"data": [
																				"LeadTime__"
																			],
																			"options": {
																				"label": "_LeadTime",
																				"version": 0
																			},
																			"stamp": 121
																		},
																		"LeadTime__": {
																			"type": "Integer",
																			"data": [
																				"LeadTime__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_LeadTime",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 122
																		},
																		"TechnicalData__": {
																			"type": "ShortText",
																			"data": [
																				"TechnicalData__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TechnicalData",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 5000,
																				"defaultValue": "{}",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 123
																		},
																		"LabelTechnicalData__": {
																			"type": "Label",
																			"data": [
																				"TechnicalData__"
																			],
																			"options": {
																				"label": "_TechnicalData",
																				"version": 0
																			},
																			"stamp": 124
																		},
																		"LabelIsReplenishmentItem__": {
																			"type": "Label",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem"
																			},
																			"stamp": 148
																		},
																		"IsReplenishmentItem__": {
																			"type": "CheckBox",
																			"data": [
																				"IsReplenishmentItem__"
																			],
																			"options": {
																				"label": "_IsReplenishmentItem",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"possibleValues": {},
																				"possibleKeys": {},
																				"readonly": true
																			},
																			"stamp": 149
																		},
																		"LabelWarehouseID__": {
																			"type": "Label",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"label": "_WarehouseID",
																				"version": 0
																			},
																			"stamp": 152
																		},
																		"WarehouseID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 153
																		},
																		"LabelWarehouseName__": {
																			"type": "Label",
																			"data": [
																				"WarehouseName__"
																			],
																			"options": {
																				"label": "_WarehouseName",
																				"version": 0
																			},
																			"stamp": 156
																		},
																		"WarehouseName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseName__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 157
																		}
																	},
																	"stamp": 125
																}
															},
															"stamp": 126,
															"data": []
														}
													},
													"stamp": 127,
													"data": []
												}
											}
										}
									},
									"stamp": 128,
									"data": []
								}
							},
							"stamp": 129,
							"data": []
						}
					},
					"stamp": 130,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 131,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 132,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 133,
							"data": []
						}
					},
					"stamp": 134,
					"data": []
				}
			},
			"stamp": 135,
			"data": []
		}
	},
	"stamps": 149,
	"data": []
}