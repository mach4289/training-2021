{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1500,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1500
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1500
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning",
												"hideSeparator": true
											},
											"*": {
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"label": "_Banner",
																"version": 0,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner",
																						"version": 0,
																						"htmlContent": "Purchase order",
																						"width": 700
																					},
																					"stamp": 14
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 15
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 820,
													"*": {
														"progressBarPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "PO_order_progress.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_progressBarPanel",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"stamp": 821,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 822,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ProgressBar__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 823,
																			"*": {
																				"ProgressBar__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_progressBar",
																						"htmlContent": "<input id=\"onLoadProgressBar\" type=\"hidden\" style=\"display:none;\" onclick=\"SetProgressBar(event);\">\n<div class=\"progressBar-customControlContainer\">\n\t<div class=\"progressBar-count progressBar-text\"><span class=\"progressBar-title\">Amount delivered </span><span class=\"progressBar-done\"></span> of <span class=\"progressBar-total\"></span> <span class=\"progressBar-currency\"></span></div>\n\t<div class=\"progressBar-container progressbar\">\n\t\t<div class=\"progressBar-bar progressbar\"></div><div class=\"progressBar-percentage progressBar-text\"></div>\n\t</div>\n</div>\n\n<script type=\"text/javascript\">\nwindow.postMessage({ eventName: \"OnLoadProgressBar\", control: \"ProgressBar__\" }, document.location.origin);\nfunction SetProgressBar(evt){\n\tvar done = document.getElementsByClassName(\"progressBar-done\");\n\tvar total = document.getElementsByClassName(\"progressBar-total\");\n\tvar bar = document.getElementsByClassName(\"progressBar-bar\");\n\tvar percentage = document.getElementsByClassName(\"progressBar-percentage\");\n\tvar currency = document.getElementsByClassName(\"progressBar-currency\");\n\tvar nbPercentage = 0;\n\tif (!evt.total || evt.done >= evt.total)\n\t{\n\t\tnbPercentage = 100;\n\t}\n\telse\n\t{\n\t\t// set the max percentage to 99 when the done has not reach the total for UX purpose\n\t\tnbPercentage = Math.min(evt.done / evt.total * 100, 99);\n\t}\n\tif (isNaN(nbPercentage))\n\t{\n\t\tnbPercentage = 0;\n\t}\n\tvar strPercentage = nbPercentage.toFixed(0) + \"%\";\n\tif (done && done.length)\n\t{\n\t\tdone[0].innerHTML = evt.done;\n\t}\n\telse\n\t{\n\t\tdone[0].innerHTML = \"?\";\n\t}\n\tif (total && total.length)\n\t{\n\t\ttotal[0].innerHTML = evt.total;\n\t}\n\telse\n\t{\n\t\ttotal.innerHTML = \"?\";\n\t}\n\tif (bar && bar.length)\n\t{\n\t\tbar[0].style.width = strPercentage;\n\t}\n\tif (percentage && percentage.length)\n\t{\n\t\tpercentage[0].innerHTML = strPercentage;\n\t}\n\tif (currency && currency.length)\n\t{\n\t\tcurrency[0].innerHTML = evt.currency;\n\t}\n\telse\n\t{\n\t\tcurrency.innerHTML = \"?\";\n\t}\n}\n</script>",
																						"css": ".progressBar-container{\n\tbackground-color:#ebeff0;\n}\n.progressBar-bar{\n\tbackground-color:#008996;\n\tposition: absolute;\n}\n.progressbar{\n\theight: 30px;\n}\n.progressBar-customControlContainer{\n\twidth: 50%;\n\tposition: relative;\n}\n.progressBar-text{\n\tcolor:grey;\n\tpadding-top: 10px;\n\tpadding-bottom: 5px;\n}\n.progressBar-count{\n\ttext-align:left;\n}\n.progressBar-percentage{\n\tcolor: black;\n\tleft: 50%;\n\tposition: absolute;\n\tfont-weight: bold;\n\tfont-size: larger;\n\tpadding-top: 6px;\n}\n.progressBar-title{\n\tcolor:#008996;\n}\n.progressBar-done{\n\tcolor:black;\n\tfont-weight: bold;\n}\n.progressBar-total{\n\tcolor:black;\n\tfont-weight: bold;\n}",
																						"version": 0,
																						"width": "100%"
																					},
																					"stamp": 824
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer"
																					},
																					"stamp": 825
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"Requisition_information": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false,
																	"maximized": false
																},
																"version": 0,
																"labelLength": "200",
																"label": "_Requisition information",
																"iconURL": "PO_information.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"removeMargins": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"OrderNumber__": "LabelOrder_number__",
																			"LabelOrder_number__": "OrderNumber__",
																			"OrderStatus__": "LabelOrder_status__",
																			"LabelOrder_status__": "OrderStatus__",
																			"LabelDelivered_date__": "DeliveredDate__",
																			"DeliveredDate__": "LabelDelivered_date__",
																			"BuyerName__": "LabelBuyer_name__",
																			"LabelBuyer_name__": "BuyerName__",
																			"BuyerLogin__": "LabelBuyer_login__",
																			"LabelBuyer_login__": "BuyerLogin__",
																			"LabelCompany_code__": "CompanyCode__",
																			"CompanyCode__": "LabelCompany_code__",
																			"ConfirmationDatetime__": "LabelConfirmation_datetime__",
																			"LabelConfirmation_datetime__": "ConfirmationDatetime__",
																			"TotalNetLocalCurrency__": "LabelTotalNetLocalCurrency__",
																			"LabelTotalNetLocalCurrency__": "TotalNetLocalCurrency__",
																			"OpenOrderLocalCurrency__": "LabelOpenOrderLocalCurrency__",
																			"LabelOpenOrderLocalCurrency__": "OpenOrderLocalCurrency__",
																			"PurchasingOrganization__": "LabelPurchasingOrganization__",
																			"LabelPurchasingOrganization__": "PurchasingOrganization__",
																			"PurchasingGroup__": "LabelPurchasingGroup__",
																			"LabelPurchasingGroup__": "PurchasingGroup__",
																			"OrderDate__": "LabelOrderDate__",
																			"LabelOrderDate__": "OrderDate__",
																			"AtLeastOneRequestedDeliveryDateInPast__": "LabelAtLeastOneRequestedDeliveryDateInPast__",
																			"LabelAtLeastOneRequestedDeliveryDateInPast__": "AtLeastOneRequestedDeliveryDateInPast__",
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__",
																			"RevisionDateTime__": "LabelRevisionDateTime__",
																			"LabelRevisionDateTime__": "RevisionDateTime__",
																			"InfoTotalNetAmount__": "LabelInfoTotalNetAmount__",
																			"LabelInfoTotalNetAmount__": "InfoTotalNetAmount__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__",
																			"OrderDateTime__": "LabelOrderDateTime__",
																			"LabelOrderDateTime__": "OrderDateTime__",
																			"NumberOfLines__": "LabelNumberOfLines__",
																			"LabelNumberOfLines__": "NumberOfLines__",
																			"OrderRevised__": "LabelOrderRevised__",
																			"LabelOrderRevised__": "OrderRevised__",
																			"LocalCurrency__": "LabelLocalCurrency__",
																			"LabelLocalCurrency__": "LocalCurrency__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 23,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"OrderNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelOrder_number__": {
																						"line": 2,
																						"column": 1
																					},
																					"OrderStatus__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelOrder_status__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelDelivered_date__": {
																						"line": 17,
																						"column": 1
																					},
																					"DeliveredDate__": {
																						"line": 17,
																						"column": 2
																					},
																					"BuyerName__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelBuyer_name__": {
																						"line": 6,
																						"column": 1
																					},
																					"BuyerLogin__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelBuyer_login__": {
																						"line": 7,
																						"column": 1
																					},
																					"Ligne_d_espacement4__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelCompany_code__": {
																						"line": 3,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 3,
																						"column": 2
																					},
																					"ConfirmationDatetime__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelConfirmation_datetime__": {
																						"line": 16,
																						"column": 1
																					},
																					"TotalNetLocalCurrency__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelTotalNetLocalCurrency__": {
																						"line": 8,
																						"column": 1
																					},
																					"OpenOrderLocalCurrency__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelOpenOrderLocalCurrency__": {
																						"line": 9,
																						"column": 1
																					},
																					"PurchasingOrganization__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelPurchasingOrganization__": {
																						"line": 11,
																						"column": 1
																					},
																					"PurchasingGroup__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelPurchasingGroup__": {
																						"line": 12,
																						"column": 1
																					},
																					"OrderDate__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelOrderDate__": {
																						"line": 13,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 21,
																						"column": 1
																					},
																					"AtLeastOneRequestedDeliveryDateInPast__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelAtLeastOneRequestedDeliveryDateInPast__": {
																						"line": 18,
																						"column": 1
																					},
																					"ERP__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 4,
																						"column": 1
																					},
																					"RevisionDateTime__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelRevisionDateTime__": {
																						"line": 15,
																						"column": 1
																					},
																					"InfoTotalNetAmount__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelInfoTotalNetAmount__": {
																						"line": 20,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 22,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 22,
																						"column": 1
																					},
																					"OrderDateTime__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelOrderDateTime__": {
																						"line": 14,
																						"column": 1
																					},
																					"NumberOfLines__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelNumberOfLines__": {
																						"line": 19,
																						"column": 1
																					},
																					"OrderRevised__": {
																						"line": 23,
																						"column": 2
																					},
																					"LabelOrderRevised__": {
																						"line": 23,
																						"column": 1
																					},
																					"LocalCurrency__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelLocalCurrency__": {
																						"line": 10,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"Ligne_d_espacement4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement4",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 17
																				},
																				"LabelOrder_number__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 18
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 19
																				},
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 20
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SAP",
																							"1": "_Generic",
																							"2": "_EBS",
																							"3": "_NAV",
																							"4": "_JDE"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SAP",
																							"1": "generic",
																							"2": "EBS",
																							"3": "NAV",
																							"4": "JDE"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 21
																				},
																				"LabelCompany_code__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 23
																				},
																				"LabelOrder_status__": {
																					"type": "Label",
																					"data": [
																						"OrderStatus__"
																					],
																					"options": {
																						"label": "_Order status",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 24
																				},
																				"OrderStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"OrderStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Order to order",
																							"1": "_Order to pay",
																							"2": "_Order waiting for deliver",
																							"3": "_Order auto deliver",
																							"4": "_Order delivered",
																							"5": "_Order canceled"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "To order",
																							"1": "To pay",
																							"2": "To receive",
																							"3": "Auto receive",
																							"4": "Received",
																							"5": "Canceled"
																						},
																						"label": "_Order status",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"version": 1,
																						"readonlyIsText": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 25
																				},
																				"LabelBuyer_name__": {
																					"type": "Label",
																					"data": [
																						"BuyerName__"
																					],
																					"options": {
																						"label": "_Buyer name",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"BuyerName__": {
																					"type": "ShortText",
																					"data": [
																						"BuyerName__"
																					],
																					"options": {
																						"label": "_Buyer name",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 27
																				},
																				"LabelBuyer_login__": {
																					"type": "Label",
																					"data": [
																						"BuyerLogin__"
																					],
																					"options": {
																						"label": "_Buyer login",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 28
																				},
																				"BuyerLogin__": {
																					"type": "ShortText",
																					"data": [
																						"BuyerLogin__"
																					],
																					"options": {
																						"label": "_Buyer login",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 29
																				},
																				"LabelTotalNetLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"label": "_TotalNetLocalCurrency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 30
																				},
																				"TotalNetLocalCurrency__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalNetLocalCurrency",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 31
																				},
																				"LabelOpenOrderLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"OpenOrderLocalCurrency__"
																					],
																					"options": {
																						"label": "_OpenOrderLocalCurrency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 32
																				},
																				"OpenOrderLocalCurrency__": {
																					"type": "Decimal",
																					"data": [
																						"OpenOrderLocalCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_OpenOrderLocalCurrency",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 33
																				},
																				"LabelLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"label": "_LocalCurrency",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 812
																				},
																				"LocalCurrency__": {
																					"type": "ShortText",
																					"data": [
																						"LocalCurrency__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_LocalCurrency",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 813
																				},
																				"LabelPurchasingOrganization__": {
																					"type": "Label",
																					"data": [
																						"PurchasingOrganization__"
																					],
																					"options": {
																						"label": "_PurchasingOrganization",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 34
																				},
																				"PurchasingOrganization__": {
																					"type": "ShortText",
																					"data": [
																						"PurchasingOrganization__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_PurchasingOrganization",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 35
																				},
																				"LabelPurchasingGroup__": {
																					"type": "Label",
																					"data": [
																						"PurchasingGroup__"
																					],
																					"options": {
																						"label": "_PurchasingGroup",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 36
																				},
																				"PurchasingGroup__": {
																					"type": "ShortText",
																					"data": [
																						"PurchasingGroup__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_PurchasingGroup",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 37
																				},
																				"LabelOrderDate__": {
																					"type": "Label",
																					"data": [
																						"OrderDate__"
																					],
																					"options": {
																						"label": "_OrderDate",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"OrderDate__": {
																					"type": "DateTime",
																					"data": [
																						"OrderDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_OrderDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"LabelOrderDateTime__": {
																					"type": "Label",
																					"data": [
																						"OrderDateTime__"
																					],
																					"options": {
																						"label": "_OrderDateTime",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 620
																				},
																				"OrderDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"OrderDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_OrderDateTime",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true
																					},
																					"stamp": 621
																				},
																				"LabelRevisionDateTime__": {
																					"type": "Label",
																					"data": [
																						"RevisionDateTime__"
																					],
																					"options": {
																						"label": "_RevisionDateTime",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 40
																				},
																				"RevisionDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"RevisionDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_RevisionDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 41
																				},
																				"LabelConfirmation_datetime__": {
																					"type": "Label",
																					"data": [
																						"ConfirmationDatetime__"
																					],
																					"options": {
																						"label": "_Confirmation datetime",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 42
																				},
																				"ConfirmationDatetime__": {
																					"type": "RealDateTime",
																					"data": [
																						"ConfirmationDatetime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_Confirmation datetime",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 43
																				},
																				"LabelDelivered_date__": {
																					"type": "Label",
																					"data": [
																						"DeliveredDate__"
																					],
																					"options": {
																						"label": "_Delivered date",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 44
																				},
																				"DeliveredDate__": {
																					"type": "DateTime",
																					"data": [
																						"DeliveredDate__"
																					],
																					"options": {
																						"label": "_Delivered date",
																						"activable": true,
																						"width": "180",
																						"displayLongFormat": false,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 45
																				},
																				"LabelAtLeastOneRequestedDeliveryDateInPast__": {
																					"type": "Label",
																					"data": [
																						"AtLeastOneRequestedDeliveryDateInPast__"
																					],
																					"options": {
																						"label": "_AtLeastOneRequestedDeliveryDateInPast",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 46
																				},
																				"AtLeastOneRequestedDeliveryDateInPast__": {
																					"type": "CheckBox",
																					"data": [
																						"AtLeastOneRequestedDeliveryDateInPast__"
																					],
																					"options": {
																						"label": "_AtLeastOneRequestedDeliveryDateInPast",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 47
																				},
																				"LabelNumberOfLines__": {
																					"type": "Label",
																					"data": [
																						"NumberOfLines__"
																					],
																					"options": {
																						"label": "_NumberOfLines",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 798
																				},
																				"NumberOfLines__": {
																					"type": "Integer",
																					"data": [
																						"NumberOfLines__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfLines",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": "0",
																						"enablePlusMinus": false,
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 799
																				},
																				"LabelInfoTotalNetAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "_Info total net amount",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"InfoTotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Info total net amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"dataSource": 1,
																						"notInDB": true,
																						"dataType": "Number",
																						"autocompletable": false
																					},
																					"stamp": 49
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 51
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 5000,
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 52
																				},
																				"LabelOrderRevised__": {
																					"type": "Label",
																					"data": [
																						"OrderRevised__"
																					],
																					"options": {
																						"label": "_OrderRevised",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 810
																				},
																				"OrderRevised__": {
																					"type": "CheckBox",
																					"data": [
																						"OrderRevised__"
																					],
																					"options": {
																						"label": "_OrderRevised",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"readonly": true,
																						"version": 0,
																						"possibleValues": {
																							"0": "_OrderRevised_No",
																							"1": "_OrderRevised_Yes"
																						},
																						"possibleKeys": {
																							"0": "0",
																							"1": "1"
																						}
																					},
																					"stamp": 811
																				}
																			},
																			"stamp": 53
																		}
																	},
																	"stamp": 54,
																	"data": []
																}
															},
															"stamp": 55,
															"data": []
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 56,
													"*": {
														"Vendor_details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"maximized": false,
																	"collapsed": false
																},
																"labelLength": "200",
																"label": "_Vendor details",
																"hidden": true,
																"version": 0,
																"iconURL": "PO_vendor.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 57,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"VendorAddress__": "LabelVendor_address__",
																			"LabelVendor_address__": "VendorAddress__",
																			"VendorVatNumber__": "LabelVendor_vat_number__",
																			"LabelVendor_vat_number__": "VendorVatNumber__",
																			"VendorFaxNumber__": "LabelVendor_fax_number__",
																			"LabelVendor_fax_number__": "VendorFaxNumber__",
																			"LabelVendor_name__": "VendorName__",
																			"VendorName__": "LabelVendor_name__",
																			"NewVendorRequest__": "LabelNewVendorRequest__",
																			"LabelNewVendorRequest__": "NewVendorRequest__"
																		},
																		"version": 0
																	},
																	"stamp": 58,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VendorNumber__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 3,
																						"column": 1
																					},
																					"VendorAddress__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelVendor_address__": {
																						"line": 6,
																						"column": 1
																					},
																					"Ligne_d_espacement5__": {
																						"line": 1,
																						"column": 1
																					},
																					"VendorVatNumber__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelVendor_vat_number__": {
																						"line": 4,
																						"column": 1
																					},
																					"VendorFaxNumber__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelVendor_fax_number__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelVendor_name__": {
																						"line": 2,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 2,
																						"column": 2
																					},
																					"NewVendorRequest__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelNewVendorRequest__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 59,
																			"*": {
																				"LabelVendor_name__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Vendor name",
																						"version": 0
																					},
																					"stamp": 60
																				},
																				"Ligne_d_espacement5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement5",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 61
																				},
																				"VendorName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Vendor name",
																						"activable": true,
																						"width": "275",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"version": 1,
																						"readonly": true,
																						"browsable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"minNbLines": 1,
																						"maxNbLines": 1
																					},
																					"stamp": 62
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"version": 0
																					},
																					"stamp": 63
																				},
																				"VendorNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"activable": true,
																						"width": "275",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"version": 1,
																						"readonly": true,
																						"browsable": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 64
																				},
																				"LabelVendor_vat_number__": {
																					"type": "Label",
																					"data": [
																						"VendorVatNumber__"
																					],
																					"options": {
																						"label": "_Vendor vat number",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 65
																				},
																				"VendorVatNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorVatNumber__"
																					],
																					"options": {
																						"label": "_Vendor vat number",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 66
																				},
																				"LabelVendor_fax_number__": {
																					"type": "Label",
																					"data": [
																						"VendorFaxNumber__"
																					],
																					"options": {
																						"label": "_Vendor fax number",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 67
																				},
																				"VendorFaxNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorFaxNumber__"
																					],
																					"options": {
																						"label": "_Vendor fax number",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 68
																				},
																				"LabelVendor_address__": {
																					"type": "Label",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"label": "_Vendor address",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"VendorAddress__": {
																					"type": "LongText",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"label": "_Vendor address",
																						"activable": true,
																						"width": "275",
																						"numberOfLines": 4,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"minNbLines": 4
																					},
																					"stamp": 70
																				},
																				"LabelNewVendorRequest__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"NewVendorRequest__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_New vendor request",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "BudgetIntegrityValidation__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 73
																				}
																			}
																		}
																	}
																}
															}
														},
														"Ship_to": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "_Ship to",
																"version": 0,
																"iconURL": "PO_reception_shipto.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 74,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ShipToCompany__": "LabelShip_to_company__",
																			"LabelShip_to_company__": "ShipToCompany__",
																			"ShipToContact__": "LabelShip_to_contact__",
																			"LabelShip_to_contact__": "ShipToContact__",
																			"ShipToAddress__": "LabelShip_to_address__",
																			"LabelShip_to_address__": "ShipToAddress__",
																			"DeliveryAddressID__": "LabelDeliveryAddressID__",
																			"LabelDeliveryAddressID__": "DeliveryAddressID__",
																			"ShipToPhone__": "LabelShipToPhone__",
																			"LabelShipToPhone__": "ShipToPhone__",
																			"ShipToEmail__": "LabelShipToEmail__",
																			"LabelShipToEmail__": "ShipToEmail__"
																		},
																		"version": 0
																	},
																	"stamp": 75,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ShipToCompany__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelShip_to_company__": {
																						"line": 3,
																						"column": 1
																					},
																					"ShipToContact__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelShip_to_contact__": {
																						"line": 4,
																						"column": 1
																					},
																					"ShipToAddress__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelShip_to_address__": {
																						"line": 7,
																						"column": 1
																					},
																					"Ligne_d_espacement7__": {
																						"line": 1,
																						"column": 1
																					},
																					"DeliveryAddressID__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDeliveryAddressID__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line9__": {
																						"line": 8,
																						"column": 1
																					},
																					"ShipToPhone__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelShipToPhone__": {
																						"line": 5,
																						"column": 1
																					},
																					"ShipToEmail__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelShipToEmail__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 76,
																			"*": {
																				"Ligne_d_espacement7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement7",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 77
																				},
																				"LabelDeliveryAddressID__": {
																					"type": "Label",
																					"data": [
																						"DeliveryAddressID__"
																					],
																					"options": {
																						"label": "_DeliveryAddressID",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 78
																				},
																				"DeliveryAddressID__": {
																					"type": "ShortText",
																					"data": [
																						"DeliveryAddressID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_DeliveryAddressID",
																						"activable": true,
																						"width": "275",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 79
																				},
																				"LabelShip_to_company__": {
																					"type": "Label",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"label": "_Ship to company",
																						"version": 0
																					},
																					"stamp": 80
																				},
																				"ShipToCompany__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Ship to company",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 81
																				},
																				"LabelShip_to_contact__": {
																					"type": "Label",
																					"data": [
																						"ShipToContact__"
																					],
																					"options": {
																						"label": "_Ship to contact",
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"ShipToContact__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToContact__"
																					],
																					"options": {
																						"label": "_Ship to contact",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 83
																				},
																				"LabelShipToPhone__": {
																					"type": "Label",
																					"data": [
																						"ShipToPhone__"
																					],
																					"options": {
																						"label": "_Ship to phone",
																						"version": 0
																					},
																					"stamp": 84
																				},
																				"ShipToPhone__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToPhone__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Ship to phone",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 85
																				},
																				"LabelShipToEmail__": {
																					"type": "Label",
																					"data": [
																						"ShipToEmail__"
																					],
																					"options": {
																						"label": "_Ship to email",
																						"version": 0
																					},
																					"stamp": 86
																				},
																				"ShipToEmail__": {
																					"type": "ShortText",
																					"data": [
																						"ShipToEmail__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Ship to email",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 87
																				},
																				"LabelShip_to_address__": {
																					"type": "Label",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"label": "_Ship to address",
																						"version": 0
																					},
																					"stamp": 88
																				},
																				"ShipToAddress__": {
																					"type": "LongText",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"label": "_Ship to address",
																						"activable": true,
																						"width": "275",
																						"numberOfLines": 4,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"minNbLines": 4
																					},
																					"stamp": 89
																				},
																				"Spacer_line9__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line9",
																						"version": 0
																					},
																					"stamp": 90
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 91,
													"*": {
														"Vendor_notification_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "PO_information.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Vendor notification",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 92,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelBuyer_comment__": "BuyerComment__",
																			"BuyerComment__": "LabelBuyer_comment__",
																			"VendorEmail__": "LabelVendor_email__",
																			"LabelVendor_email__": "VendorEmail__",
																			"EmailNotificationOptions__": "LabelEmailNotificationOptions__",
																			"LabelEmailNotificationOptions__": "EmailNotificationOptions__",
																			"LabelEmail_carbon_copy__": "EmailCarbonCopy__",
																			"EmailCarbonCopy__": "LabelEmail_carbon_copy__",
																			"AutomaticOrder__": "LabelAutomaticOrder__",
																			"LabelAutomaticOrder__": "AutomaticOrder__"
																		},
																		"version": 0
																	},
																	"stamp": 93,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement3__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelBuyer_comment__": {
																						"line": 3,
																						"column": 1
																					},
																					"BuyerComment__": {
																						"line": 3,
																						"column": 2
																					},
																					"VendorEmail__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelVendor_email__": {
																						"line": 4,
																						"column": 1
																					},
																					"EmailNotificationOptions__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEmailNotificationOptions__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelEmail_carbon_copy__": {
																						"line": 5,
																						"column": 1
																					},
																					"EmailCarbonCopy__": {
																						"line": 5,
																						"column": 2
																					},
																					"Spacer_line6__": {
																						"line": 6,
																						"column": 1
																					},
																					"AutomaticOrder__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelAutomaticOrder__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 94,
																			"*": {
																				"EmailCarbonCopy__": {
																					"type": "LongText",
																					"data": [
																						"EmailCarbonCopy__"
																					],
																					"options": {
																						"label": "_Email carbon copy",
																						"activable": true,
																						"width": "275",
																						"numberOfLines": 3,
																						"browsable": false,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"resizable": true,
																						"length": 250,
																						"minNbLines": 3,
																						"hidden": true
																					},
																					"stamp": 95
																				},
																				"LabelEmail_carbon_copy__": {
																					"type": "Label",
																					"data": [
																						"EmailCarbonCopy__"
																					],
																					"options": {
																						"label": "_Email carbon copy",
																						"version": 0
																					},
																					"stamp": 96
																				},
																				"Ligne_d_espacement3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement3",
																						"version": 0
																					},
																					"stamp": 97
																				},
																				"LabelBuyer_comment__": {
																					"type": "Label",
																					"data": [
																						"BuyerComment__"
																					],
																					"options": {
																						"label": "_Additionnal message",
																						"version": 0
																					},
																					"stamp": 98
																				},
																				"BuyerComment__": {
																					"type": "LongText",
																					"data": [
																						"BuyerComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Additionnal message",
																						"activable": true,
																						"width": "275",
																						"browsable": false,
																						"maxNbLines": 4,
																						"version": 1,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 4
																					},
																					"stamp": 99
																				},
																				"LabelEmailNotificationOptions__": {
																					"type": "Label",
																					"data": [
																						"EmailNotificationOptions__"
																					],
																					"options": {
																						"label": "_EmailNotificationOptions",
																						"version": 0
																					},
																					"stamp": 100
																				},
																				"EmailNotificationOptions__": {
																					"type": "ComboBox",
																					"data": [
																						"EmailNotificationOptions__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SendToVendor",
																							"1": "_PunchoutMode",
																							"2": "_DontSend"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SendToVendor",
																							"1": "PunchoutMode",
																							"2": "DontSend"
																						},
																						"label": "_EmailNotificationOptions",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 101
																				},
																				"LabelVendor_email__": {
																					"type": "Label",
																					"data": [
																						"VendorEmail__"
																					],
																					"options": {
																						"label": "_Vendor email",
																						"version": 0
																					},
																					"stamp": 102
																				},
																				"VendorEmail__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorEmail__"
																					],
																					"options": {
																						"label": "_Vendor email",
																						"activable": true,
																						"width": "275",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"version": 1,
																						"autocompletable": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 250,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 103
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 104
																				},
																				"LabelAutomaticOrder__": {
																					"type": "Label",
																					"data": [
																						"AutomaticOrder__"
																					],
																					"options": {
																						"label": "_AutomaticOrder",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 818
																				},
																				"AutomaticOrder__": {
																					"type": "CheckBox",
																					"data": [
																						"AutomaticOrder__"
																					],
																					"options": {
																						"label": "_AutomaticOrder",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"possibleValues": {
																							"0": "_TransmittedManually",
																							"1": "_AutomaticOrder"
																						},
																						"possibleKeys": {
																							"0": "0",
																							"1": "1"
																						},
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 819
																				}
																			}
																		}
																	}
																}
															}
														},
														"Down_payment_request": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "_Down payment request",
																"version": 0,
																"iconURL": "PO_downpayment.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 105,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TotalNetAmount__": "LabelTotal_net_amount__",
																			"LabelTotal_net_amount__": "TotalNetAmount__",
																			"PaymentPercent__": "LabelPayment_percent__",
																			"LabelPayment_percent__": "PaymentPercent__",
																			"LabelCurrency__": "Currency__",
																			"Currency__": "LabelCurrency__",
																			"LabelPayment_amount__": "PaymentAmount__",
																			"PaymentAmount__": "LabelPayment_amount__",
																			"LabelPayment_type__": "PaymentType__",
																			"PaymentType__": "LabelPayment_type__",
																			"LabelPayment_date__": "PaymentDate__",
																			"PaymentDate__": "LabelPayment_date__",
																			"LabelPayment_reference__": "PaymentReference__",
																			"PaymentReference__": "LabelPayment_reference__",
																			"TotalAmountIncludingVAT__": "LabelTotalAmountIncludingVAT__",
																			"LabelTotalAmountIncludingVAT__": "TotalAmountIncludingVAT__",
																			"PaymentTermCode__": "LabelPaymentTermCode__",
																			"LabelPaymentTermCode__": "PaymentTermCode__",
																			"PaymentTermDescription__": "LabelPaymentTermDescription__",
																			"LabelPaymentTermDescription__": "PaymentTermDescription__",
																			"PaymentMethodCode__": "LabelPaymentMethodCode__",
																			"LabelPaymentMethodCode__": "PaymentMethodCode__",
																			"PaymentMethodDescription__": "LabelPaymentMethodDescription__",
																			"LabelPaymentMethodDescription__": "PaymentMethodDescription__"
																		},
																		"version": 0
																	},
																	"stamp": 106,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TotalNetAmount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelTotal_net_amount__": {
																						"line": 3,
																						"column": 1
																					},
																					"Ligne_d_espacement6__": {
																						"line": 1,
																						"column": 1
																					},
																					"PaymentPercent__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelPayment_percent__": {
																						"line": 9,
																						"column": 1
																					},
																					"LabelCurrency__": {
																						"line": 2,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPayment_amount__": {
																						"line": 10,
																						"column": 1
																					},
																					"PaymentAmount__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelPayment_type__": {
																						"line": 11,
																						"column": 1
																					},
																					"PaymentType__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelPayment_date__": {
																						"line": 12,
																						"column": 1
																					},
																					"PaymentDate__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelPayment_reference__": {
																						"line": 13,
																						"column": 1
																					},
																					"PaymentReference__": {
																						"line": 13,
																						"column": 2
																					},
																					"TotalAmountIncludingVAT__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelTotalAmountIncludingVAT__": {
																						"line": 4,
																						"column": 1
																					},
																					"PaymentTermCode__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelPaymentTermCode__": {
																						"line": 5,
																						"column": 1
																					},
																					"PaymentTermDescription__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelPaymentTermDescription__": {
																						"line": 6,
																						"column": 1
																					},
																					"PaymentMethodCode__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelPaymentMethodCode__": {
																						"line": 7,
																						"column": 1
																					},
																					"PaymentMethodDescription__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelPaymentMethodDescription__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 107,
																			"*": {
																				"PaymentType__": {
																					"type": "ComboBox",
																					"data": [
																						"PaymentType__"
																					],
																					"options": {
																						"readonlyIsText": false,
																						"possibleValues": {
																							"0": "",
																							"1": "_Credit card",
																							"2": "_Check",
																							"3": "_Bank transfer",
																							"4": "_Cash payment",
																							"5": "_Direct debits"
																						},
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_Payment type",
																						"activable": true,
																						"width": "180",
																						"version": 1,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 108
																				},
																				"LabelPayment_type__": {
																					"type": "Label",
																					"data": [
																						"PaymentType__"
																					],
																					"options": {
																						"label": "_Payment type",
																						"version": 0
																					},
																					"stamp": 109
																				},
																				"PaymentAmount__": {
																					"type": "Decimal",
																					"data": [
																						"PaymentAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Payment amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "275",
																						"version": 1,
																						"textSize": "S",
																						"autocompletable": false,
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2
																					},
																					"stamp": 110
																				},
																				"LabelTotal_net_amount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "_Total net amount",
																						"version": 0
																					},
																					"stamp": 111
																				},
																				"TotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Total net amount",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "275",
																						"version": 1,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2
																					},
																					"stamp": 112
																				},
																				"LabelPayment_amount__": {
																					"type": "Label",
																					"data": [
																						"PaymentAmount__"
																					],
																					"options": {
																						"label": "_Payment amount",
																						"version": 0
																					},
																					"stamp": 113
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"label": "_Currency",
																						"version": 0
																					},
																					"stamp": 114
																				},
																				"LabelTotalAmountIncludingVAT__": {
																					"type": "Label",
																					"data": [
																						"TotalAmountIncludingVAT__"
																					],
																					"options": {
																						"label": "_TotalAmountIncludingVAT",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 115
																				},
																				"TotalAmountIncludingVAT__": {
																					"type": "Decimal",
																					"data": [
																						"TotalAmountIncludingVAT__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalAmountIncludingVAT",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 116
																				},
																				"LabelPaymentTermCode__": {
																					"type": "Label",
																					"data": [
																						"PaymentTermCode__"
																					],
																					"options": {
																						"label": "_Payment terms",
																						"version": 0
																					},
																					"stamp": 117
																				},
																				"PaymentTermCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"PaymentTermCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Payment terms",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"readonly": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 118
																				},
																				"LabelPaymentTermDescription__": {
																					"type": "Label",
																					"data": [
																						"PaymentTermDescription__"
																					],
																					"options": {
																						"label": "_Payment terms description",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 119
																				},
																				"PaymentTermDescription__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentTermDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Payment terms description",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 400,
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 120
																				},
																				"LabelPaymentMethodCode__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethodCode__"
																					],
																					"options": {
																						"label": "_PaymentMethodCode",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"PaymentMethodCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"PaymentMethodCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_PaymentMethodCode",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"readonly": true,
																						"browsable": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 122
																				},
																				"LabelPaymentMethodDescription__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethodDescription__"
																					],
																					"options": {
																						"label": "_PaymentMethodDescription",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 123
																				},
																				"PaymentMethodDescription__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentMethodDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_PaymentMethodDescription",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 124
																				},
																				"Ligne_d_espacement6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement6",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 125
																				},
																				"Currency__": {
																					"type": "ComboBox",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Euro",
																							"1": "USD"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "EUR",
																							"1": "USD"
																						},
																						"label": "_Currency",
																						"activable": true,
																						"width": "180",
																						"version": 1,
																						"readonly": true,
																						"readonlyIsText": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 126
																				},
																				"LabelPayment_date__": {
																					"type": "Label",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"label": "_Payment date",
																						"version": 0
																					},
																					"stamp": 127
																				},
																				"LabelPayment_reference__": {
																					"type": "Label",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"label": "_Payment reference",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 128
																				},
																				"PaymentDate__": {
																					"type": "DateTime",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Payment date",
																						"activable": true,
																						"width": "180",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 129
																				},
																				"PaymentReference__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"label": "_Payment reference",
																						"activable": true,
																						"width": "180",
																						"length": 250,
																						"version": 0,
																						"autocompletable": false,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 130
																				},
																				"LabelPayment_percent__": {
																					"type": "Label",
																					"data": [
																						"PaymentPercent__"
																					],
																					"options": {
																						"label": "_Payment percent",
																						"version": 0
																					},
																					"stamp": 131
																				},
																				"PaymentPercent__": {
																					"type": "Decimal",
																					"data": [
																						"PaymentPercent__"
																					],
																					"options": {
																						"integer": false,
																						"label": "_Payment percent",
																						"precision_internal": 2,
																						"activable": true,
																						"width": "275",
																						"autocompletable": false,
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 2
																					},
																					"stamp": 132
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 133,
													"*": {
														"AdditionalDataPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "PO_additional_data.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_AdditionalDataPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 134,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ValidityStart__": "LabelValidityStart__",
																			"LabelValidityStart__": "ValidityStart__",
																			"ValidityEnd__": "LabelValidityEnd__",
																			"LabelValidityEnd__": "ValidityEnd__"
																		},
																		"version": 0
																	},
																	"stamp": 135,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ValidityStart__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelValidityStart__": {
																						"line": 1,
																						"column": 1
																					},
																					"ValidityEnd__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelValidityEnd__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 136,
																			"*": {
																				"LabelValidityStart__": {
																					"type": "Label",
																					"data": [
																						"ValidityStart__"
																					],
																					"options": {
																						"label": "_ValidityStart",
																						"version": 0
																					},
																					"stamp": 137
																				},
																				"ValidityStart__": {
																					"type": "DateTime",
																					"data": [
																						"ValidityStart__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ValidityStart",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 138
																				},
																				"LabelValidityEnd__": {
																					"type": "Label",
																					"data": [
																						"ValidityEnd__"
																					],
																					"options": {
																						"label": "_ValidityEnd",
																						"version": 0
																					},
																					"stamp": 139
																				},
																				"ValidityEnd__": {
																					"type": "DateTime",
																					"data": [
																						"ValidityEnd__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ValidityEnd",
																						"activable": true,
																						"width": "275",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 140
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 141,
													"*": {
														"LocalActionPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_LocalActionPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "center",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 142,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DisplayGoodsReceipt__": {
																						"line": 2,
																						"column": 1
																					},
																					"DisplayInvoices__": {
																						"line": 4,
																						"column": 1
																					},
																					"DisplayAdvancedShippingNotices__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 144,
																			"*": {
																				"DisplayAdvancedShippingNotices__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_DisplayAdvancedShippingNotices",
																						"label": "_DisplayAdvancedShippingNotices",
																						"textPosition": "text-right",
																						"textSize": "M",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "Advanced Shipping Notice",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"hidden": true,
																						"awesomeClasses": "esk-ifont-adv-shipping-notice-circle fa-3x",
																						"style": 5,
																						"version": 0,
																						"urlImage": ""
																					},
																					"stamp": 974
																				},
																				"DisplayInvoices__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_DisplayInvoices",
																						"label": "_DisplayInvoices",
																						"textPosition": "text-right",
																						"textSize": "M",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "",
																						"action": "none",
																						"url": "",
																						"awesomeClasses": "esk-ifont-display-good-receipts-circle fa-3x",
																						"style": 5,
																						"version": 0,
																						"hidden": true,
																						"urlImage": "",
																						"iconColor": "color1"
																					},
																					"stamp": 146
																				},
																				"DisplayGoodsReceipt__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_DisplayGoodsReceipt",
																						"label": "_DisplayGoodsReceipt",
																						"textPosition": "text-right",
																						"textSize": "M",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-display-invoices-circle fa-3x",
																						"style": 5,
																						"width": "",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"hidden": true,
																						"urlImage": "",
																						"iconColor": "color1"
																					},
																					"stamp": 145
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1055,
													"*": {
														"RelatedShippingNoticePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"iconURL": "icon_related_shipping_notice.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RelatedShippingNoticePane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 925,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 926,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RelatedShippingNoticeTable__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 927,
																			"*": {
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 1047
																				},
																				"RelatedShippingNoticeTable__": {
																					"type": "Table",
																					"data": [
																						"RelatedShippingNoticeTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 15,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RelatedShippingNoticeTable",
																						"subsection": null,
																						"readonly": true
																					},
																					"stamp": 939,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 940,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 941
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 942
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 943,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 966,
																									"data": [],
																									"*": {
																										"ASNNumber__": {
																											"type": "Label",
																											"data": [
																												"ASNNumber__"
																											],
																											"options": {
																												"label": "_ASNNumber",
																												"type": "LongText",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 967,
																											"position": [
																												"_LongText",
																												false,
																												false,
																												"LongText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 950,
																									"data": [],
																									"*": {
																										"ASNShippingDate__": {
																											"type": "Label",
																											"data": [
																												"ASNShippingDate__"
																											],
																											"options": {
																												"label": "_ASNShippingDate",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 951,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 958,
																									"data": [],
																									"*": {
																										"ASNExpectedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ASNExpectedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ASNExpectedDeliveryDate",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 959,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 962,
																									"data": [],
																									"*": {
																										"ASNCarrier__": {
																											"type": "Label",
																											"data": [
																												"ASNCarrier__"
																											],
																											"options": {
																												"label": "_ASNCarrier",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 963,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 970,
																									"data": [],
																									"*": {
																										"ASNStatus__": {
																											"type": "Label",
																											"data": [
																												"ASNStatus__"
																											],
																											"options": {
																												"label": "_ASNStatus",
																												"type": "LongText",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 971,
																											"position": [
																												"_LongText",
																												false,
																												false,
																												"LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 944,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 965,
																									"data": [],
																									"*": {
																										"ASNNumber__": {
																											"type": "LongText",
																											"data": [
																												"ASNNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ASNNumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 1,
																												"browsable": false
																											},
																											"stamp": 968,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 949,
																									"data": [],
																									"*": {
																										"ASNShippingDate__": {
																											"type": "DateTime",
																											"data": [
																												"ASNShippingDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ASNShippingDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 952,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 957,
																									"data": [],
																									"*": {
																										"ASNExpectedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ASNExpectedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ASNExpectedDeliveryDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 960,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 961,
																									"data": [],
																									"*": {
																										"ASNCarrier__": {
																											"type": "ShortText",
																											"data": [
																												"ASNCarrier__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ASNCarrier",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 964,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 969,
																									"data": [],
																									"*": {
																										"ASNStatus__": {
																											"type": "LongText",
																											"data": [
																												"ASNStatus__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ASNStatus",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 1,
																												"browsable": false
																											},
																											"stamp": 972,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 1048
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-24": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1068,
													"*": {
														"Delivery_history": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_Delivery history",
																"version": 0,
																"hidden": false,
																"iconURL": "PO_history.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 148,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 149,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement8__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line16__": {
																						"line": 3,
																						"column": 1
																					},
																					"RelatedGoodsReceiptTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 150,
																			"*": {
																				"Ligne_d_espacement8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement8",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 151
																				},
																				"RelatedGoodsReceiptTable__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 15,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RelatedGoodsReceiptTable",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 152,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 153,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 154
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 155
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 156,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"GoodsReceiptNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptNumber",
																												"version": 0
																											},
																											"stamp": 158,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 159,
																									"data": [],
																									"*": {
																										"GoodsReceiptOwner__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptOwner",
																												"version": 0
																											},
																											"stamp": 160,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"GoodsReceiptDate__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptDate",
																												"version": 0
																											},
																											"stamp": 162,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 163,
																									"data": [],
																									"*": {
																										"GoodsReceiptNote__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptNote",
																												"version": 0
																											},
																											"stamp": 164,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 165,
																									"data": [],
																									"*": {
																										"GoodsReceiptStatus__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptStatus",
																												"version": 0
																											},
																											"stamp": 166,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"GoodsReceiptValidationURL__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_GoodsReceiptValidationURL",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 168,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 169,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 170,
																									"data": [],
																									"*": {
																										"GoodsReceiptNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_GoodsReceiptNumber",
																												"activable": true,
																												"width": "140",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 171,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 172,
																									"data": [],
																									"*": {
																										"GoodsReceiptOwner__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_GoodsReceiptOwner",
																												"activable": true,
																												"width": "240",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 173,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 174,
																									"data": [],
																									"*": {
																										"GoodsReceiptDate__": {
																											"type": "DateTime",
																											"data": [],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_GoodsReceiptDate",
																												"activable": true,
																												"width": "100",
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Date",
																												"autocompletable": false
																											},
																											"stamp": 175,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 176,
																									"data": [],
																									"*": {
																										"GoodsReceiptNote__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_GoodsReceiptNote",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 5,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 177,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 178,
																									"data": [],
																									"*": {
																										"GoodsReceiptStatus__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_GoodsReceiptStatus",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 179,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 180,
																									"data": [],
																									"*": {
																										"GoodsReceiptValidationURL__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_GoodsReceiptValidationURL",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 181,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line16__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line16",
																						"version": 0
																					},
																					"stamp": 182
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-23": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1041,
													"*": {
														"RelatedInvoicesPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "PO_Invoice_pane.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RelatedInvoicesPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 184,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 185,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line3__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line15__": {
																						"line": 3,
																						"column": 1
																					},
																					"RelatedInvoicesTable__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 186,
																			"*": {
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 187
																				},
																				"RelatedInvoicesTable__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 15,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_RelatedInvoicesTable",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 188,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 189,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 190
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 191
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 192,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 193,
																									"data": [],
																									"*": {
																										"InvoiceNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceNumber",
																												"version": 0
																											},
																											"stamp": 194,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 195,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceDate",
																												"version": 0
																											},
																											"stamp": 196,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 197,
																									"data": [],
																									"*": {
																										"InvoiceAmount__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceAmount",
																												"version": 0
																											},
																											"stamp": 198,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 199,
																									"data": [],
																									"*": {
																										"InvoiceCurrency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceCurrency",
																												"version": 0
																											},
																											"stamp": 200,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 201,
																									"data": [],
																									"*": {
																										"InvoiceStatus__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceStatus",
																												"version": 0
																											},
																											"stamp": 202,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 203,
																									"data": [],
																									"*": {
																										"InvoiceValidationURL__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_InvoiceValidationURL",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 204,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 205,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 206,
																									"data": [],
																									"*": {
																										"InvoiceNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_InvoiceNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 207,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 208,
																									"data": [],
																									"*": {
																										"InvoiceDate__": {
																											"type": "DateTime",
																											"data": [],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_InvoiceDate",
																												"activable": true,
																												"width": "100",
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Date",
																												"autocompletable": false
																											},
																											"stamp": 209,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 210,
																									"data": [],
																									"*": {
																										"InvoiceAmount__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_InvoiceAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "150",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 211,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 212,
																									"data": [],
																									"*": {
																										"InvoiceCurrency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_InvoiceCurrency",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 213,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 214,
																									"data": [],
																									"*": {
																										"InvoiceStatus__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_InvoiceStatus",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 215,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 216,
																									"data": [],
																									"*": {
																										"InvoiceValidationURL__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_InvoiceValidationURL",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 217,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line15__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line15",
																						"version": 0
																					},
																					"stamp": 218
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 219,
													"*": {
														"Line_items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_Line items",
																"version": 0,
																"iconURL": "PO_items.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hidden": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 220,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 221,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line10__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 222,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 223
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 69,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"readonly": true,
																						"subsection": null
																					},
																					"stamp": 224,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 225,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 226,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 227,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 228,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 229,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Label",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"label": "_Line item number",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 230
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 231,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": 90,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 232,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 233,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"version": 0
																											},
																											"stamp": 234
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"version": 0
																											},
																											"stamp": 236
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 237,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Label",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"label": "_PRLineNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 238,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 239,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "Label",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"label": "_PRNumber",
																												"version": 0
																											},
																											"stamp": 240,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 241,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "Label",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"label": "_PRRUIDEX",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 242,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 243,
																									"data": [],
																									"*": {
																										"PRSubmissionDateTime__": {
																											"type": "Label",
																											"data": [
																												"PRSubmissionDateTime__"
																											],
																											"options": {
																												"label": "_PRSubmissionDateTime",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 622,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 815,
																									"data": [],
																									"*": {
																										"ItemInitialRequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemInitialRequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemInitialRequestedDeliveryDate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 826,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 819,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemRequestedDeliveryDate",
																												"version": 0
																											},
																											"stamp": 827,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 245,
																									"data": [],
																									"*": {
																										"ItemStartDate__": {
																											"type": "Label",
																											"data": [
																												"ItemStartDate__"
																											],
																											"options": {
																												"label": "_ItemStartDate",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 828,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 247,
																									"data": [],
																									"*": {
																										"ItemEndDate__": {
																											"type": "Label",
																											"data": [
																												"ItemEndDate__"
																											],
																											"options": {
																												"label": "_ItemEndDate",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 829,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 613,
																									"data": [],
																									"*": {
																										"ItemRequestedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedQuantity__"
																											],
																											"options": {
																												"label": "_ItemRequestedQuantity",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 625,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 253,
																									"data": [],
																									"*": {
																										"OrderableQuantity__": {
																											"type": "Label",
																											"data": [
																												"OrderableQuantity__"
																											],
																											"options": {
																												"label": "_OrderableQuantity",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 626,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 257,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"label": "_Ordered quantity",
																												"version": 0
																											},
																											"stamp": 627
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 255,
																									"data": [],
																									"*": {
																										"ItemUndeliveredQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemUndeliveredQuantity__"
																											],
																											"options": {
																												"label": "_Item undelivered quantity",
																												"version": 0
																											},
																											"stamp": 628
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 249,
																									"data": [],
																									"*": {
																										"ItemTotalDeliveredQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemTotalDeliveredQuantity__"
																											],
																											"options": {
																												"label": "_ItemTotalDeliveredQuantity",
																												"version": 0
																											},
																											"stamp": 629,
																											"position": [
																												"Nombre d�cimal"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 259,
																									"data": [],
																									"*": {
																										"ItemRequestedUnitPrice__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedUnitPrice__"
																											],
																											"options": {
																												"label": "_ItemRequestedUnitPrice",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 630,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 251,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"label": "_Item unit price",
																												"version": 0
																											},
																											"stamp": 631
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 617,
																									"data": [],
																									"*": {
																										"ItemRequestedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedAmount__"
																											],
																											"options": {
																												"label": "_ItemRequestedAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 632,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 261,
																									"data": [],
																									"*": {
																										"OrderableAmount__": {
																											"type": "Label",
																											"data": [
																												"OrderableAmount__"
																											],
																											"options": {
																												"label": "_OrderableAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 633,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "90"
																									},
																									"stamp": 265,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"label": "_ItemOrderedAmount",
																												"version": 0,
																												"minwidth": "90"
																											},
																											"stamp": 634
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "90",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 263,
																									"data": [],
																									"*": {
																										"ItemOpenAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemOpenAmount__"
																											],
																											"options": {
																												"label": "_ItemOpenAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 635,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "90",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 267,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"label": "_ItemReceivedAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 636,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 269,
																									"data": [],
																									"*": {
																										"ItemDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemDeliveryDate",
																												"minwidth": "180",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 637,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 271,
																									"data": [],
																									"*": {
																										"ItemDeliveryComplete__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryComplete__"
																											],
																											"options": {
																												"label": "_ItemDeliveryComplete",
																												"version": 0,
																												"hidden": true,
																												"minwidth": 140
																											},
																											"stamp": 638,
																											"position": [
																												"Case � cocher"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 273,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "Label",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"label": "_ItemUnit",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 639,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 275,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"label": "_ItemUnitDescription",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 640,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 277,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "Label",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"label": "_ItemCurrency",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 641,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 727,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "Label",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"label": "_WarehouseID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 830,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 822,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "Label",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"label": "_WarehouseName",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 831,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 823,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"label": "_ItemShipToCompany",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 832,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 278,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"label": "_ItemDeliveryAddressID",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 833,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 731,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"label": "_ItemShipToAddress",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 834,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 735,
																									"data": [],
																									"*": {
																										"ItemCompanyCode__": {
																											"type": "Label",
																											"data": [
																												"ItemCompanyCode__"
																											],
																											"options": {
																												"label": "_ItemCompanyCode",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 835,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 279,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"label": "_ItemCostCenterId",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 836,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 281,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"label": "_ItemCostCenterName",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 837,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 283,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "Label",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"label": "_ProjectCode",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 838,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 285,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "Label",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"label": "_ProjectCodeDescription",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 839,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 287,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "Label",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"label": "_InternalOrder",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 840,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 803,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "Label",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"label": "_WBSElement",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 841,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 807,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "Label",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"label": "_WBSElementID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 842,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 289,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"label": "_FreeDimension1",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 843,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 824,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"label": "_FreeDimension1ID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 844,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 291,
																									"data": [],
																									"*": {
																										"SupplyTypeId__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeId__"
																											],
																											"options": {
																												"label": "_Supply type ID",
																												"version": 0,
																												"hidden": true,
																												"minwidth": "90"
																											},
																											"stamp": 845,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 293,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"label": "_SupplyTypeFullpath",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 846,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 295,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"label": "_Supply type name",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 847,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 297,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"version": 0
																											},
																											"stamp": 848
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 299,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 849,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 301,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"label": "_ItemTaxAmount",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 850,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 303,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Label",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"label": "_LeadTime",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 851,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 305,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "Label",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"version": 0
																											},
																											"stamp": 852,
																											"position": [
																												"Select from a table"
																											]
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 307,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "Label",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"label": "_CostType",
																												"minwidth": 80,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 853,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 309,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "Label",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"label": "_ItemGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 854
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 311,
																									"data": [],
																									"*": {
																										"RequestedBudgetID__": {
																											"type": "Label",
																											"data": [
																												"RequestedBudgetID__"
																											],
																											"options": {
																												"label": "_RequestedBudgetID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 855,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 313,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "Label",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"label": "_BudgetID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 856,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 315,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "Label",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"label": "_RequesterDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 857,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 317,
																									"data": [],
																									"*": {
																										"ItemRequester__": {
																											"type": "Label",
																											"data": [
																												"ItemRequester__"
																											],
																											"options": {
																												"label": "_ItemRequester",
																												"version": 0
																											},
																											"stamp": 858,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell59": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 319,
																									"data": [],
																									"*": {
																										"BuyerDN__": {
																											"type": "Label",
																											"data": [
																												"BuyerDN__"
																											],
																											"options": {
																												"label": "_BuyerDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 859,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell60": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 321,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "Label",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"label": "_ItemInCxml",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 860,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell61": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 323,
																									"data": [],
																									"*": {
																										"RequestedVendor__": {
																											"type": "Label",
																											"data": [
																												"RequestedVendor__"
																											],
																											"options": {
																												"label": "_RequestedVendor",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 861,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell62": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 325,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "Label",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"label": "_RecipientDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 862,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell63": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 327,
																									"data": [],
																									"*": {
																										"ItemRecipient__": {
																											"type": "Label",
																											"data": [
																												"ItemRecipient__"
																											],
																											"options": {
																												"label": "_ItemRecipient",
																												"version": 0
																											},
																											"stamp": 863,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell64": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 329,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Label",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"label": "_ItemExchangeRate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 864,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell65": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 331,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "Label",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 865,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell66": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 815,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "Label",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 866,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell67": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 867,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "Label",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 868,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell68": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 869,
																									"data": [],
																									"*": {
																										"ItemNetAmountLocalCurrency__": {
																											"type": "Label",
																											"data": [
																												"ItemNetAmountLocalCurrency__"
																											],
																											"options": {
																												"label": "_ItemNetAmountLocalCurrency",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 870,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell69": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 871,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "Label",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 872,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 335,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 336,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Integer",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"integer": true,
																												"precision_internal": 0,
																												"label": "_Line item number",
																												"activable": true,
																												"width": "25",
																												"readonly": true,
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 337
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 338,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"readonly": true,
																												"hidden": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 339,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 340,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": "100",
																												"version": 0,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 341
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 342,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "LongText",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": "260",
																												"resizable": true,
																												"minNbLines": 1,
																												"numberOfLines": 999,
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 343
																										}
																									},
																									"position": [
																										"Multiline text"
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 344,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Integer",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_PRLineNumber",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 345,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 346,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "ShortText",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 347,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 348,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "ShortText",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRRUIDEX",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 100,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 349,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 350,
																									"data": [],
																									"*": {
																										"PRSubmissionDateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"PRSubmissionDateTime__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_PRSubmissionDateTime",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 674,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 814,
																									"data": [],
																									"*": {
																										"ItemInitialRequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemInitialRequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemInitialRequestedDeliveryDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 873,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 818,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemRequestedDeliveryDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 874,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 352,
																									"data": [],
																									"*": {
																										"ItemStartDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemStartDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemStartDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 875,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 354,
																									"data": [],
																									"*": {
																										"ItemEndDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemEndDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemEndDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 876,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 612,
																									"data": [],
																									"*": {
																										"ItemRequestedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemRequestedQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemRequestedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 677,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 360,
																									"data": [],
																									"*": {
																										"OrderableQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"OrderableQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_OrderableQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 678,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 364,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Ordered quantity",
																												"activable": true,
																												"width": "60",
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false
																											},
																											"stamp": 679
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 362,
																									"data": [],
																									"*": {
																										"ItemUndeliveredQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemUndeliveredQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item undelivered quantity",
																												"activable": true,
																												"width": "60",
																												"autocompletable": false,
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 680
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 356,
																									"data": [],
																									"*": {
																										"ItemTotalDeliveredQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTotalDeliveredQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTotalDeliveredQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true
																											},
																											"stamp": 681,
																											"position": [
																												"Nombre d�cimal"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 366,
																									"data": [],
																									"*": {
																										"ItemRequestedUnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"ItemRequestedUnitPrice__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemRequestedUnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 682,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 358,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item unit price",
																												"activable": true,
																												"width": "70",
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false
																											},
																											"stamp": 683
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 616,
																									"data": [],
																									"*": {
																										"ItemRequestedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemRequestedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemRequestedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 684,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 368,
																									"data": [],
																									"*": {
																										"OrderableAmount__": {
																											"type": "Decimal",
																											"data": [
																												"OrderableAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_OrderableAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 685,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 372,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_ItemOrderedAmount",
																												"activable": true,
																												"width": "90",
																												"version": 1,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false
																											},
																											"stamp": 686
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 370,
																									"data": [],
																									"*": {
																										"ItemOpenAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemOpenAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemOpenAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 687,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 374,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemReceivedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 688,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 376,
																									"data": [],
																									"*": {
																										"ItemDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemDeliveryDate",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 689,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 378,
																									"data": [],
																									"*": {
																										"ItemDeliveryComplete__": {
																											"type": "CheckBox",
																											"data": [
																												"ItemDeliveryComplete__"
																											],
																											"options": {
																												"label": "_ItemDeliveryComplete",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 690,
																											"position": [
																												"Case � cocher"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 380,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnit",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"length": 3,
																												"PreFillFTS": true,
																												"readonly": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 691,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 382,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnitDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"readonly": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 692,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 384,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCurrency",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 693,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 825,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true
																											},
																											"stamp": 877,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 826,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 878,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 827,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ItemShipToCompany",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true
																											},
																											"stamp": 879,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 726,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "ShortText",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemDeliveryAddressID",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 880,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 730,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "LongText",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemShipToAddress",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 4,
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 881,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 734,
																									"data": [],
																									"*": {
																										"ItemCompanyCode__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCompanyCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCompanyCode",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 882,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 386,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCostCenterId",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 883,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 388,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCostCenterName",
																												"activable": true,
																												"width": 170,
																												"browsable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 884,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 390,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true,
																												"RestrictSearch": true
																											},
																											"stamp": 885,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 392,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCodeDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 886,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 394,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_InternalOrder",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 887,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 802,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElement",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 888,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 806,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElementID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 889,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 396,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_FreeDimension1",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"length": 500
																											},
																											"stamp": 890,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 828,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_FreeDimension1ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"length": 500,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 891,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 398,
																									"data": [],
																									"*": {
																										"SupplyTypeId__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeId__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Supply type ID",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 892,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 400,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_SupplyTypeFullpath",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 500,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 893,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 402,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Supply type name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 894,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 404,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"length": 600,
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"version": 1,
																												"browsable": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 895
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 406,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 896,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 408,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTaxAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 897,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 410,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Integer",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_LeadTime",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 898,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 412,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"version": 0,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains"
																											},
																											"stamp": 899,
																											"position": [
																												"Select from a table"
																											]
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 414,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "ComboBox",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "",
																													"1": "_OpEx",
																													"2": "_CapEx"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "",
																													"1": "OpEx",
																													"2": "CapEx"
																												},
																												"label": "_CostType",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"readonly": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 900,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 416,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "ShortText",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemGroup",
																												"activable": true,
																												"width": "200",
																												"length": 64,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 901
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 418,
																									"data": [],
																									"*": {
																										"RequestedBudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"RequestedBudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequestedBudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 902,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 420,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 903,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 422,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 904,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 424,
																									"data": [],
																									"*": {
																										"ItemRequester__": {
																											"type": "ShortText",
																											"data": [
																												"ItemRequester__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemRequester",
																												"activable": true,
																												"width": "120",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 905,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell59": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 426,
																									"data": [],
																									"*": {
																										"BuyerDN__": {
																											"type": "ShortText",
																											"data": [
																												"BuyerDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BuyerDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 906,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell60": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 428,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "LongText",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemInCxml",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"resizable": true,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"minNbLines": 3,
																												"hidden": true
																											},
																											"stamp": 907,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell61": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 430,
																									"data": [],
																									"*": {
																										"RequestedVendor__": {
																											"type": "ShortText",
																											"data": [
																												"RequestedVendor__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequestedVendor",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 908,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell62": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 432,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "ShortText",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RecipientDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 909,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell63": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 434,
																									"data": [],
																									"*": {
																										"ItemRecipient__": {
																											"type": "ShortText",
																											"data": [
																												"ItemRecipient__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemRecipient",
																												"activable": true,
																												"width": "120",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 910,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell64": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 436,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemExchangeRate",
																												"precision_internal": 6,
																												"precision_current": 6,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 911,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell65": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 438,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "CheckBox",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 912,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell66": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 814,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "CheckBox",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 913,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell67": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 914,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "CheckBox",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 915,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell68": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 916,
																									"data": [],
																									"*": {
																										"ItemNetAmountLocalCurrency__": {
																											"type": "Decimal",
																											"data": [
																												"ItemNetAmountLocalCurrency__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemNetAmountLocalCurrency",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 917,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell69": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 918,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "CheckBox",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"possibleValues": {},
																												"possibleKeys": {},
																												"hidden": true
																											},
																											"stamp": 919,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 442
																				},
																				"Spacer_line10__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line10",
																						"version": 0
																					},
																					"stamp": 443
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-18": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 444,
													"*": {
														"TaxSummaryPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_TaxSummaryPane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "right",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 445,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TotalTaxAmount__": "LabelTotalTaxAmount__",
																			"LabelTotalTaxAmount__": "TotalTaxAmount__"
																		},
																		"version": 0
																	},
																	"stamp": 446,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TaxSummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line11__": {
																						"line": 2,
																						"column": 1
																					},
																					"TotalTaxAmount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelTotalTaxAmount__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 447,
																			"*": {
																				"TaxSummary__": {
																					"type": "Table",
																					"data": [
																						"TaxSummary__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 20,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_TaxSummary",
																						"readonly": true
																					},
																					"stamp": 448,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 449,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 450
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 451
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 452,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 453,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "Label",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"version": 0
																											},
																											"stamp": 454,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 455,
																									"data": [],
																									"*": {
																										"TaxDescription__": {
																											"type": "Label",
																											"data": [
																												"TaxDescription__"
																											],
																											"options": {
																												"label": "_TaxDescription",
																												"version": 0
																											},
																											"stamp": 456,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 457,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Label",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"version": 0
																											},
																											"stamp": 458,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 459,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Label",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"label": "_Item net amount",
																												"version": 0
																											},
																											"stamp": 460,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 461,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Label",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"label": "_Item tax amount",
																												"version": 0
																											},
																											"stamp": 462,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 463,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 464,
																									"data": [],
																									"*": {
																										"TaxCode__": {
																											"type": "ShortText",
																											"data": [
																												"TaxCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"length": 600
																											},
																											"stamp": 465,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 466,
																									"data": [],
																									"*": {
																										"TaxDescription__": {
																											"type": "ShortText",
																											"data": [
																												"TaxDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_TaxDescription",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0
																											},
																											"stamp": 467,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 468,
																									"data": [],
																									"*": {
																										"TaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"TaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 469,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 470,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item net amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 471,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 472,
																									"data": [],
																									"*": {
																										"TaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"TaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax amount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 473,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line11__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line11",
																						"version": 0
																					},
																					"stamp": 474
																				},
																				"LabelTotalTaxAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalTaxAmount__"
																					],
																					"options": {
																						"label": "_Total tax amount",
																						"version": 0
																					},
																					"stamp": 475
																				},
																				"TotalTaxAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalTaxAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Total tax amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "80",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 476
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 477,
													"*": {
														"AdditionalFees_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "PO_AdditionalFees.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_AdditionalFees_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 478,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BrowseAddFees__": "LabelBrowseAddFees__",
																			"LabelBrowseAddFees__": "BrowseAddFees__"
																		},
																		"version": 0
																	},
																	"stamp": 479,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AdditionalFees__": {
																						"line": 2,
																						"column": 1
																					},
																					"BrowseAddFees__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelBrowseAddFees__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line13__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 480,
																			"*": {
																				"Spacer_line13__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line13",
																						"version": 0
																					},
																					"stamp": 481
																				},
																				"AdditionalFees__": {
																					"type": "Table",
																					"data": [
																						"AdditionalFees__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_AdditionalFees",
																						"subsection": null
																					},
																					"stamp": 482,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 483,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 484
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 485
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 486,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 487,
																									"data": [],
																									"*": {
																										"AdditionalFeeID__": {
																											"type": "Label",
																											"data": [
																												"AdditionalFeeID__"
																											],
																											"options": {
																												"label": "_AdditionalFeeID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 488,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 489,
																									"data": [],
																									"*": {
																										"AdditionalFeeDescription__": {
																											"type": "Label",
																											"data": [
																												"AdditionalFeeDescription__"
																											],
																											"options": {
																												"label": "_AdditionalFeeDescription",
																												"version": 0
																											},
																											"stamp": 490,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 491,
																									"data": [],
																									"*": {
																										"Price__": {
																											"type": "Label",
																											"data": [
																												"Price__"
																											],
																											"options": {
																												"label": "_Price",
																												"version": 0
																											},
																											"stamp": 492,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 493,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 494,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 495,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 496,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 497,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"label": "_ItemTaxAmount",
																												"minwidth": "70",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 498,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 499,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 500,
																									"data": [],
																									"*": {
																										"AdditionalFeeID__": {
																											"type": "ShortText",
																											"data": [
																												"AdditionalFeeID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_AdditionalFeeID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 501,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 502,
																									"data": [],
																									"*": {
																										"AdditionalFeeDescription__": {
																											"type": "ShortText",
																											"data": [
																												"AdditionalFeeDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_AdditionalFeeDescription",
																												"activable": true,
																												"width": "350",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 503,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 504,
																									"data": [],
																									"*": {
																										"Price__": {
																											"type": "Decimal",
																											"data": [
																												"Price__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Price",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 505,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 506,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"length": 600
																											},
																											"stamp": 507,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 508,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 509,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 510,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTaxAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 511,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 512
																				},
																				"LabelBrowseAddFees__": {
																					"type": "Label",
																					"data": [
																						"BrowseAddFees__"
																					],
																					"options": {
																						"label": "_BrowseAddFees",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 513
																				},
																				"BrowseAddFees__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"BrowseAddFees__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_BrowseAddFees",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"hidden": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 514
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-19": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 515,
													"*": {
														"ItemsButtons": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ItemsButtons",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 516,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BrowsePRItems__": "LabelBrowsePRItems__",
																			"LabelBrowsePRItems__": "BrowsePRItems__"
																		},
																		"version": 0
																	},
																	"stamp": 517,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AddItemsToOrderButton__": {
																						"line": 1,
																						"column": 1
																					},
																					"AddFee__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line14__": {
																						"line": 3,
																						"column": 1
																					},
																					"BrowsePRItems__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelBrowsePRItems__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 518,
																			"*": {
																				"AddItemsToOrderButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Add items to order",
																						"label": "",
																						"version": 0,
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "PO_add_item_button.png",
																						"urlImageOverlay": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 519
																				},
																				"AddFee__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_AddFee",
																						"label": "_AddFee",
																						"version": 0,
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "PO_add_fee_button.png",
																						"urlImageOverlay": "",
																						"style": 4,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"hidden": true
																					},
																					"stamp": 520
																				},
																				"Spacer_line14__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line14",
																						"version": 0
																					},
																					"stamp": 521
																				},
																				"LabelBrowsePRItems__": {
																					"type": "Label",
																					"data": [
																						"BrowsePRItems__"
																					],
																					"options": {
																						"label": "_Browse items to order title",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 522
																				},
																				"BrowsePRItems__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"BrowsePRItems__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"label": "_Browse items to order title",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"hidden": true,
																						"MultipleSelection": true
																					},
																					"stamp": 523
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 524,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Attachments",
																"iconURL": "PO_attachments.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 525
																}
															},
															"stamp": 526,
															"data": []
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 527,
													"*": {
														"ApprovalWorkflow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_ApprovalWorkflow",
																"version": 0,
																"hidden": true,
																"iconURL": "PO_workflow.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 528,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__"
																		},
																		"version": 0
																	},
																	"stamp": 529,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement9__": {
																						"line": 1,
																						"column": 1
																					},
																					"Comments__": {
																						"line": 2,
																						"column": 1
																					},
																					"POWorkflow__": {
																						"line": 4,
																						"column": 1
																					},
																					"ComputingWorkflow__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelComputingWorkflow__": {
																						"line": 5,
																						"column": 1
																					},
																					"Ligne_d_espacement10__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 530,
																			"*": {
																				"Ligne_d_espacement9__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement9",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 531
																				},
																				"Comments__": {
																					"type": "LongText",
																					"data": [
																						"Comments__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Comments",
																						"activable": true,
																						"width": "100%",
																						"numberOfLines": 5,
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 5
																					},
																					"stamp": 532
																				},
																				"Ligne_d_espacement10__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement10",
																						"version": 0
																					},
																					"stamp": 533
																				},
																				"POWorkflow__": {
																					"type": "Table",
																					"data": [
																						"POWorkflow__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 8,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_POWorkflow",
																						"readonly": true
																					},
																					"stamp": 534,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 535,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 536,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 537,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 538,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 539,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 540
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 541,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "Label",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"version": 0
																											},
																											"stamp": 542
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 543,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "Label",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"version": 0
																											},
																											"stamp": 544
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 545,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "Label",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"label": "_WRKFDate",
																												"version": 0
																											},
																											"stamp": 546
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 547,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "Label",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"version": 0
																											},
																											"stamp": 548
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 549,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "Label",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 550
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 551,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 552
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 553,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 554,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 555,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 556,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 557
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 558,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"activable": true,
																												"width": "180",
																												"version": 0,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 559
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 560,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"activable": true,
																												"width": "140",
																												"version": 0,
																												"readonly": false,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 561
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 562,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFDate",
																												"activable": true,
																												"width": 150,
																												"version": 0
																											},
																											"stamp": 563
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 564,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "LongText",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFComment",
																												"activable": true,
																												"width": "300",
																												"resizable": true,
																												"numberOfLines": 5,
																												"browsable": false,
																												"version": 0,
																												"minNbLines": 1
																											},
																											"stamp": 565
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 566,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFAction",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 567
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 568,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFIndex",
																												"activable": true,
																												"width": "90",
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 569
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 570,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 571,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelComputingWorkflow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 572
																				},
																				"ComputingWorkflow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"><div>",
																						"version": 0,
																						"width": "10",
																						"hidden": true
																					},
																					"stamp": 573
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 574
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-20": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 575,
													"*": {
														"Event_history": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "PR_comments.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Events",
																"leftImageURL": "",
																"version": 0,
																"hidden": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 576,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 577,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConversationUI__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 578,
																			"*": {
																				"ConversationUI__": {
																					"type": "ConversationUI",
																					"data": false,
																					"options": {
																						"label": "_ConversationUI",
																						"activable": true,
																						"tableName": "Conversation__",
																						"volatile": true,
																						"width": "100%",
																						"version": 0,
																						"conversationOptions": {
																							"ignoreIfExists": false,
																							"notifyByEmail": true,
																							"emailTemplate": "Conversation_MissedPurchasingItem.htm",
																							"emailCustomTags": {
																								"OrderNumber__": null
																							}
																						}
																					},
																					"stamp": 579
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-27": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 580,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "PO_system_data.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 581,
															"data": []
														}
													}
												},
												"form-content-left-30": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 582,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"hideActionButtons": true,
																"TitleFontSize": "S",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 583,
															"data": []
														}
													}
												}
											},
											"stamp": 584,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "56%",
													"width": "44%",
													"height": null
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"hidden": true,
																"iconURL": "PO_preview.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 585
																}
															},
															"stamp": 586,
															"data": []
														}
													},
													"stamp": 587,
													"data": []
												}
											},
											"stamp": 588,
											"data": []
										}
									},
									"stamp": 589,
									"data": []
								}
							},
							"stamp": 590,
							"data": []
						}
					},
					"stamp": 591,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1500
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0
							},
							"stamp": 592,
							"data": []
						},
						"RollbackEditionAndQuit": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_RollbackEditionAndQuit",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 593
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 594,
							"data": []
						},
						"DownloadCrystalReportsDataFile": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_DownloadCrystalReportsDataFile",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Customer Order",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"url": "",
								"action": "none"
							},
							"stamp": 595
						},
						"Cancel_purchase_order": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Cancel purchase order",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"style": 2,
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 596
						},
						"Submit_": {
							"type": "SubmitButton",
							"options": {
								"label": "_Validate and send email",
								"action": "approve",
								"submit": true,
								"version": 0,
								"style": 1,
								"nextprocess": {
									"processName": {
										"processName": "",
										"attachmentsMode": "all",
										"willBeChild": true
									},
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1"
							},
							"stamp": 597,
							"data": []
						},
						"NewGR2": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_NewGR",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Goods receipt V2",
									"attachmentsMode": "none",
									"willBeChild": true,
									"returnToOriginalUrl": false,
									"startWithoutProcessing": true
								},
								"style": 1,
								"url": "",
								"action": "openprocess",
								"version": 0,
								"textStyle": "default"
							},
							"stamp": 598
						},
						"RequestPayment": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_RequestPayment",
								"nextprocess": {
									"processName": "Purchase order V2",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"action": "approve",
								"style": 1,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1"
							},
							"stamp": 599
						},
						"Generate_Invoice": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Generate invoice",
								"style": 2,
								"action": "approve",
								"version": 0,
								"nextprocess": {
									"processName": {
										"processName": "",
										"attachmentsMode": "all",
										"willBeChild": true
									},
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1"
							},
							"stamp": 600
						},
						"ConfirmPayment": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_ConfirmPayment",
								"nextprocess": {
									"processName": "Purchase order V2",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"action": "approve",
								"style": 1,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"textStyle": "default"
							},
							"stamp": 601
						},
						"Generate_CO": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Generate customer order",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"action": "approve",
								"style": 2,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1"
							},
							"stamp": 602
						},
						"Finalize_PO": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Finalize purchase order",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 2,
								"url": "",
								"action": "approve",
								"version": 0
							},
							"stamp": 603
						},
						"Preview_purchase_order": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Preview purchase order",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 604
						},
						"SynchronizeItems": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "SynchronizeItems",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "BudgetIntegrityValidation__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 2,
								"url": "",
								"action": "approve",
								"version": 0
							},
							"stamp": 605
						},
						"Edit_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Edit",
								"style": 2,
								"action": "unseal",
								"version": 0,
								"disabled": true
							},
							"stamp": 606
						},
						"SaveEditing_": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"style": 2,
								"action": "reseal",
								"version": 0,
								"disabled": true
							},
							"stamp": 607
						},
						"SaveEditOrder": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Save_",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"style": 2,
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 608
						},
						"NewPR": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_NewPR",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 609
						}
					},
					"stamp": 610,
					"data": []
				}
			},
			"stamp": 611,
			"data": []
		}
	},
	"stamps": 1074,
	"data": []
}