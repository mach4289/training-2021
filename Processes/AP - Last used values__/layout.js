{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": true,
														"leftImageURL": ""
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__",
																	"AmountPercent__": "LabelAmountPercent__",
																	"LabelAmountPercent__": "AmountPercent__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelGLAccount__": "GLAccount__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 7,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"LineNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"AmountPercent__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelAmountPercent__": {
																				"line": 7,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelGLAccount__": {
																				"line": 4,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 5,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 6,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"length": 20,
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"LineNumber__": {
																			"type": "Integer",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_LineNumber",
																				"precision_internal": 0,
																				"activable": true,
																				"width": "500",
																				"browsable": false
																			},
																			"stamp": 8
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"GLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "_GLAccount",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"length": 30,
																				"browsable": true,
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"CostCenter__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 20,
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"TaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 10,
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"LabelAmountPercent__": {
																			"type": "Label",
																			"data": [
																				"AmountPercent__"
																			],
																			"options": {
																				"label": "_PercentageAmount",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"AmountPercent__": {
																			"type": "Decimal",
																			"data": [
																				"AmountPercent__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_PercentageAmount",
																				"precision_internal": 5,
																				"activable": true,
																				"width": "500"
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										}
									},
									"stamp": 20,
									"data": []
								}
							},
							"stamp": 21,
							"data": []
						}
					},
					"stamp": 22,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 23,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 24,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 25,
							"data": []
						}
					},
					"stamp": 26,
					"data": []
				}
			},
			"stamp": 27,
			"data": []
		}
	},
	"stamps": 41,
	"data": []
}