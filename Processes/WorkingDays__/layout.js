{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"maxwidth": 1024,
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"maxwidth": 1024,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"maxwidth": 1024,
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"width": "400",
												"version": 0
											},
											"stamp": 41,
											"*": {
												"PanelGeneral": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_PanelGeneral",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": "100px"
													},
													"stamp": 21,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"ConfigName__": "LabelConfigName__",
																	"LabelConfigName__": "ConfigName__",
																	"ConfigDescription__": "LabelConfigDescription__",
																	"LabelConfigDescription__": "ConfigDescription__"
																},
																"version": 0
															},
															"stamp": 22,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ConfigName__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelConfigName__": {
																				"line": 1,
																				"column": 1
																			},
																			"ConfigDescription__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelConfigDescription__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 23,
																	"*": {
																		"LabelConfigName__": {
																			"type": "Label",
																			"data": [
																				"ConfigName__"
																			],
																			"options": {
																				"label": "_ConfigName",
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"ConfigName__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ConfigName",
																				"activable": true,
																				"width": "350",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"length": 100
																			},
																			"stamp": 38
																		},
																		"LabelConfigDescription__": {
																			"type": "Label",
																			"data": [
																				"ConfigDescription__"
																			],
																			"options": {
																				"label": "_ConfigDescription",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"ConfigDescription__": {
																			"type": "LongText",
																			"data": [
																				"ConfigDescription__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_ConfigDescription",
																				"activable": true,
																				"width": "350",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 40
																		}
																	}
																}
															}
														}
													}
												},
												"PanelWorkingDays": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_PanelWorkingDays",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": "100px"
													},
													"stamp": 62,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Weekday1__": "LabelWeekday1__",
																	"LabelWeekday1__": "Weekday1__",
																	"Weekday2__": "LabelWeekday2__",
																	"LabelWeekday2__": "Weekday2__",
																	"Weekday3__": "LabelWeekday3__",
																	"LabelWeekday3__": "Weekday3__",
																	"Weekday4__": "LabelWeekday4__",
																	"LabelWeekday4__": "Weekday4__",
																	"Weekday6__": "LabelWeekday6__",
																	"LabelWeekday6__": "Weekday6__",
																	"Weekday5__": "LabelWeekday5__",
																	"LabelWeekday5__": "Weekday5__",
																	"Weekday0__": "LabelWeekday0__",
																	"LabelWeekday0__": "Weekday0__"
																},
																"version": 0
															},
															"stamp": 63,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Weekday1__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelWeekday1__": {
																				"line": 1,
																				"column": 1
																			},
																			"Weekday2__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelWeekday2__": {
																				"line": 2,
																				"column": 1
																			},
																			"Weekday3__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelWeekday3__": {
																				"line": 3,
																				"column": 1
																			},
																			"Weekday4__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelWeekday4__": {
																				"line": 4,
																				"column": 1
																			},
																			"Weekday6__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelWeekday6__": {
																				"line": 6,
																				"column": 1
																			},
																			"Weekday5__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelWeekday5__": {
																				"line": 5,
																				"column": 1
																			},
																			"Weekday0__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelWeekday0__": {
																				"line": 7,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 64,
																	"*": {
																		"LabelWeekday1__": {
																			"type": "Label",
																			"data": [
																				"Weekday1__"
																			],
																			"options": {
																				"label": "_Monday",
																				"version": 0
																			},
																			"stamp": 70
																		},
																		"Weekday1__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday1__"
																			],
																			"options": {
																				"label": "_Monday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"defaultValue": true
																			},
																			"stamp": 71
																		},
																		"LabelWeekday2__": {
																			"type": "Label",
																			"data": [
																				"Weekday2__"
																			],
																			"options": {
																				"label": "_Tuesday",
																				"version": 0
																			},
																			"stamp": 72
																		},
																		"Weekday2__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday2__"
																			],
																			"options": {
																				"label": "_Tuesday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"defaultValue": true
																			},
																			"stamp": 73
																		},
																		"LabelWeekday3__": {
																			"type": "Label",
																			"data": [
																				"Weekday3__"
																			],
																			"options": {
																				"label": "_Wednesday",
																				"version": 0
																			},
																			"stamp": 74
																		},
																		"Weekday3__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday3__"
																			],
																			"options": {
																				"label": "_Wednesday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"defaultValue": true
																			},
																			"stamp": 75
																		},
																		"LabelWeekday4__": {
																			"type": "Label",
																			"data": [
																				"Weekday4__"
																			],
																			"options": {
																				"label": "_Thursday",
																				"version": 0
																			},
																			"stamp": 76
																		},
																		"Weekday4__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday4__"
																			],
																			"options": {
																				"label": "_Thursday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"defaultValue": true
																			},
																			"stamp": 77
																		},
																		"LabelWeekday5__": {
																			"type": "Label",
																			"data": [
																				"Weekday5__"
																			],
																			"options": {
																				"label": "_Friday",
																				"version": 0
																			},
																			"stamp": 78
																		},
																		"Weekday5__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday5__"
																			],
																			"options": {
																				"label": "_Friday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"defaultValue": true
																			},
																			"stamp": 79
																		},
																		"LabelWeekday6__": {
																			"type": "Label",
																			"data": [
																				"Weekday6__"
																			],
																			"options": {
																				"label": "_Saturday",
																				"version": 0
																			},
																			"stamp": 80
																		},
																		"Weekday6__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday6__"
																			],
																			"options": {
																				"label": "_Saturday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 81
																		},
																		"LabelWeekday0__": {
																			"type": "Label",
																			"data": [
																				"Weekday0__"
																			],
																			"options": {
																				"label": "_Sunday",
																				"version": 0
																			},
																			"stamp": 68
																		},
																		"Weekday0__": {
																			"type": "CheckBox",
																			"data": [
																				"Weekday0__"
																			],
																			"options": {
																				"label": "_Sunday",
																				"activable": true,
																				"width": "",
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 69
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 84,
											"*": {
												"PanelDaysOff": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_PanelDaysOff",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": "100px"
													},
													"stamp": 85,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabeljsonDaysOff__": "jsonDaysOff__",
																	"jsonDaysOff__": "LabeljsonDaysOff__"
																},
																"version": 0
															},
															"stamp": 86,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabeljsonDaysOff__": {
																				"line": 1,
																				"column": 1
																			},
																			"jsonDaysOff__": {
																				"line": 1,
																				"column": 2
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 87,
																	"*": {
																		"jsonDaysOff__": {
																			"type": "LongText",
																			"data": [
																				"jsonDaysOff__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 5,
																				"label": "_jsonDaysOff",
																				"activable": true,
																				"width": "800",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"numberOfLines": 20
																			},
																			"stamp": 83
																		},
																		"LabeljsonDaysOff__": {
																			"type": "Label",
																			"data": [
																				"jsonDaysOff__"
																			],
																			"options": {
																				"label": "_jsonDaysOff",
																				"version": 0
																			},
																			"stamp": 82
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"maxwidth": 1024,
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": {
										"processName": "",
										"attachmentsMode": "all",
										"willBeChild": true,
										"returnToOriginalUrl": false
									},
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								}
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 90,
	"data": []
}