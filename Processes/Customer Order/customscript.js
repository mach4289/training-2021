///#GLOBALS Lib Sys
function main() {
    var banner = Sys.Helpers.Banner;
    banner.SetMainTitle("_Purchase Order");
    banner.SetSubTitleAligned(true);
    banner.SetCentered(false);
    banner.SetHTMLBanner(Controls.HTMLBanner__);
    banner.SetSubTitle("_To Confirm");
    Process.SetHelpId(5026);
    Controls.InvoiceThisOrder__.Hide(true);
    Controls.InvoiceThisOrder__.SetDisabled(true);
    Controls.ServiceEntrySheet__.Hide(true);
    Controls.ServiceEntrySheet__.SetDisabled(true);
    Controls.CreateAdvancedShippingNotice__.Hide(true);
    if (ProcessInstance.state === 100 || ProcessInstance.isReadOnly) {
        if (ProcessInstance.state === 100) {
            if (!Data.GetValue("CanceledDatetime__")) {
                banner.SetSubTitle("_Confirmed");
                Sys.Parameters.GetInstance("P2P").IsReady(function () {
                    var flipPO = Sys.Parameters.GetInstance("P2P").GetParameter("EnableFlipPOGlobalSetting", false) === "1";
                    if (flipPO) {
                        Controls.InvoiceThisOrder__.Hide(false);
                        Controls.InvoiceThisOrder__.SetDisabled(false);
                        Controls.InvoiceThisOrder__.OnClick = function () {
                            Controls.InvoiceThisOrder__.Wait(true);
                            InvoiceThisOrder()
                                .Then(function (ruid) {
                                Process.OpenMessage(ruid, true);
                                Controls.InvoiceThisOrder__.Wait(false);
                            })
                                .Catch(function (error) {
                                Log.Error("CreateProcessInstance FlipPO", error);
                                Popup.Alert(["_ErrorCannotInvoiceThisOrder"], true, null, "Error");
                                Controls.InvoiceThisOrder__.Wait(false);
                            });
                        };
                    }
                });
            }
            else {
                banner.SetSubTitle("_Canceled");
            }
        }
        else if (ProcessInstance.state === 400) {
            banner.SetSubTitle("_Rejected");
        }
        Controls.Confirm__.Hide(true);
        // This gets overwritten later, but keep the two lines for the time we get the feature out of Esker Labs
        Controls.Change__.Hide(true);
        Controls.Reject__.Hide(true);
    }
    else {
        Controls.Confirm__.OnClick = function () {
            ProcessInstance.Approve("Confirm");
        };
        Controls.Reject__.OnClick = function () {
            ProcessInstance.Approve("Reject");
        };
    }
    Controls.DocumentsPanel.Hide(Attach.GetNbAttach() <= 1);
    Controls.CanceledDatetime__.Hide(!Boolean(Data.GetValue("CanceledDatetime__")));
    Controls.RevisionDateTime__.Hide(!Boolean(Data.GetValue("RevisionDateTime__")));
    Sys.Parameters.GetInstance("PAC").IsReady(function () {
        var EnableEditPORequest = Sys.Parameters.GetInstance("PAC").GetParameter("EnableVendorModifyPO", false);
        var hideButtons = !EnableEditPORequest || ProcessInstance.state === 100 || ProcessInstance.isReadOnly;
        Controls.Change__.Hide(hideButtons);
        Controls.Reject__.Hide(hideButtons);
        var isAdvancedShippingNoticeEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("EnableAdvancedShippingNotice", false);
        if (isAdvancedShippingNoticeEnabled) {
            Lib.Purchasing.ASN.InitRelatedASNPane(Controls.RelatedShippingNoticeTable__, Controls.RelatedShippingNoticePane);
            var containsQuantityBasedItem = Variable.GetValueAsString("containsQuantityBasedItem") === "1";
            Controls.CreateAdvancedShippingNotice__.Hide(false);
            var hideASNCreation = ProcessInstance.state !== 100 || !ProcessInstance.isReadOnly || !containsQuantityBasedItem;
            Controls.CreateAdvancedShippingNotice__.Hide(hideASNCreation);
        }
    });
    Controls.CompanyCode__.Hide();
    Controls.VendorNumber__.Hide();
    Controls.Invoice_RuidEx__.Hide();
    Controls.ConfirmationDatetime__.Hide(true);
    Controls.Share__.SetDisabled(true);
    Controls.Quit__.OnClick = function () {
        ProcessInstance.Quit("Quit");
        return false;
    };
    // Add customer name into Event history panel title
    var title = Language.Translate("_Event_history", false, Variable.GetValueAsString("CustomerCompany"));
    Controls.Event_history.SetText(title);
    // Viewed event
    var options = {
        ignoreIfExists: false,
        notifyByEmail: true,
        emailTemplate: "Conversation_MissedPurchasingItem.htm",
        emailCustomTags: {
            OrderNumber__: Controls.Sales_Order_Number__.GetValue()
        }
    };
    Controls.ConversationUI__.SetOptions(options);
    Controls.ConversationUI__.AddItem({ Message: Language.Translate("_CustomerOrderViewed"), Type: Lib.Purchasing.ConversationTypes.Viewed }, { ignoreIfExists: true, emailTemplate: "Event_MissedPurchasingItem.htm" });
    Lib.CommonDialog.NextAlert.Show();
    Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.HTMLScripts.OnCustomerOrderHTMLScriptEnd");
}
function InvoiceThisOrder() {
    return Sys.Helpers.Promise.Create(function (resolve, reject) {
        var processInfo = {
            companyCode: Data.GetValue("CompanyCode__"),
            orderNumber: Data.GetValue("OrderNumber__"),
            vendorNumber: Data.GetValue("VendorNumber__")
        };
        var isSelfServiceWorkEntryEnabled = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.HTMLScripts.IsSelfServiceWorkEntryEnabled");
        if (isSelfServiceWorkEntryEnabled) {
            processInfo["additionnalData"] = {
                customerOrderIdentifier: Data.GetValue("RuiDex")
            };
        }
        // Create Flip PO Instance
        Process.CreateProcessInstance("Flip PO", {}, {
            OrderToConvert: JSON.stringify(processInfo),
            OrderRuidEx: Data.GetValue("RuidEx")
        }, {
            callback: function (data) {
                if (data.error) {
                    reject(data.errorMessage);
                }
                else {
                    Log.Info("Process FlipPO created successfully");
                    // Retrieve CustomerInvoice process
                    var filter_1 = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CustomerInvoiceStatus__", "Draft"), Sys.Helpers.LdapUtil.FilterEqual("Source_RuidEx__", Data.GetValue("RuidEx")), Sys.Helpers.LdapUtil.FilterEqual("FlipPO_RuidEx__", data.ruid)).toString();
                    var retrieveCustomerInvoiceProcess_1 = function (maxTries) {
                        setTimeout(function () {
                            Log.Info("Trying to retrieve CustomerInvoice process... (" + maxTries + ")");
                            Query.DBQuery(function () {
                                var err = this.GetQueryError();
                                if (err) {
                                    reject(err);
                                }
                                else if (this.GetRecordsCount() > 0 && this.GetQueryValue("PORTALRUIDEX__", 0).length > 0) {
                                    // Process found
                                    Controls.InvoiceThisOrder__.SetDisabled(false);
                                    var previousRuidEx = Variable.GetValueAsString("GeneratedInvoiceRuidEx");
                                    var ruid = previousRuidEx ? previousRuidEx + "," + this.GetQueryValue("PORTALRUIDEX__", 0) : this.GetQueryValue("PORTALRUIDEX__", 0);
                                    Variable.SetValueAsString("GeneratedInvoiceRuidEx", ruid);
                                    resolve(ruid);
                                }
                                else if (maxTries > 0) {
                                    // Process not created yet
                                    retrieveCustomerInvoiceProcess_1(maxTries - 1);
                                }
                                else {
                                    // Max tries reached
                                    reject("reached max tries count");
                                }
                            }, "CDNAME#Customer Invoice", "PortalRuidEx__", filter_1, "SubmitDateTime DESC");
                        }, 1000);
                    };
                    retrieveCustomerInvoiceProcess_1(60);
                }
            }
        });
    });
}
main();
