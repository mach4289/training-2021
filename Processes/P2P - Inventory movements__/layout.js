{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"WarehouseID__": "LabelWarehouseID__",
																	"LabelWarehouseID__": "WarehouseID__",
																	"MovementValue__": "LabelMovementValue__",
																	"LabelMovementValue__": "MovementValue__",
																	"MovementDateTime__": "LabelMovementDateTime__",
																	"LabelMovementDateTime__": "MovementDateTime__",
																	"UnitOfMeasure__": "LabelUnitOfMeasure__",
																	"LabelUnitOfMeasure__": "UnitOfMeasure__",
																	"MovementOrigin__": "LabelMovementOrigin__",
																	"LabelMovementOrigin__": "MovementOrigin__",
																	"MovementType__": "LabelMovementType__",
																	"LabelMovementType__": "MovementType__",
																	"ItemNumber__": "LabelItemNumber__",
																	"LabelItemNumber__": "ItemNumber__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"LineNumber__": "LabelLineNumber__",
																	"LabelLineNumber__": "LineNumber__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 9,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"WarehouseID__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelWarehouseID__": {
																				"line": 3,
																				"column": 1
																			},
																			"MovementValue__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelMovementValue__": {
																				"line": 4,
																				"column": 1
																			},
																			"MovementDateTime__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelMovementDateTime__": {
																				"line": 5,
																				"column": 1
																			},
																			"UnitOfMeasure__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelUnitOfMeasure__": {
																				"line": 6,
																				"column": 1
																			},
																			"MovementOrigin__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelMovementOrigin__": {
																				"line": 7,
																				"column": 1
																			},
																			"MovementType__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelMovementType__": {
																				"line": 8,
																				"column": 1
																			},
																			"ItemNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelItemNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"LineNumber__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelLineNumber__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"DatabaseComboBox__"
																			],
																			"options": {
																				"label": "_CompanyCode"
																			},
																			"stamp": 28
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 29
																		},
																		"LabelItemNumber__": {
																			"type": "Label",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"ItemNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ItemNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 4
																		},
																		"LabelWarehouseID__": {
																			"type": "Label",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"label": "_WarehouseID",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"WarehouseID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 6
																		},
																		"LabelMovementValue__": {
																			"type": "Label",
																			"data": [
																				"MovementValue__"
																			],
																			"options": {
																				"label": "_MovementValue",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"MovementValue__": {
																			"type": "Decimal",
																			"data": [
																				"MovementValue__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_MovementValue",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 8
																		},
																		"LabelMovementDateTime__": {
																			"type": "Label",
																			"data": [
																				"MovementDateTime__"
																			],
																			"options": {
																				"label": "_MovementDateTime",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"MovementDateTime__": {
																			"type": "RealDateTime",
																			"data": [
																				"MovementDateTime__"
																			],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_MovementDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelUnitOfMeasure__": {
																			"type": "Label",
																			"data": [
																				"DatabaseComboBox__"
																			],
																			"options": {
																				"label": "_UnitOfMeasure",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"UnitOfMeasure__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"UnitOfMeasure__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_UnitOfMeasure",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 12
																		},
																		"LabelMovementOrigin__": {
																			"type": "Label",
																			"data": [
																				"MovementOrigin__"
																			],
																			"options": {
																				"label": "_MovementOrigin",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"MovementOrigin__": {
																			"type": "ShortText",
																			"data": [
																				"MovementOrigin__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_MovementOrigin",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 14
																		},
																		"LabelMovementType__": {
																			"type": "Label",
																			"data": [
																				"MovementType__"
																			],
																			"options": {
																				"label": "_MovementType",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"MovementType__": {
																			"type": "ComboBox",
																			"data": [
																				"MovementType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_Withdraw",
																					"2": "_Stocktaking"
																				},
																				"maxNbLinesToDisplay": 10,
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": 0,
																					"1": "Withdraw",
																					"2": "Stocktaking"
																				},
																				"label": "_MovementType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 16
																		},
																		"LabelLineNumber__": {
																			"type": "Label",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"label": "_LineNumber"
																			},
																			"stamp": 30
																		},
																		"LineNumber__": {
																			"type": "ShortText",
																			"data": [
																				"LineNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_LineNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 31
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										}
									},
									"stamp": 20,
									"data": []
								}
							},
							"stamp": 21,
							"data": []
						}
					},
					"stamp": 22,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 23,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 24,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 25,
							"data": []
						}
					},
					"stamp": 26,
					"data": []
				}
			},
			"stamp": 27,
			"data": []
		}
	},
	"stamps": 31,
	"data": []
}