///#GLOBALS Lib Sys

// days before process is in error in case of the grouping would fail
var g_groupingEmailValidityDurationDays = 2;

var documentData = {
	type: Data.GetValue("Document_Type__"),
	ID: Data.GetValue("Document_ID__")
};

var sender = {
	company: Data.GetValue("Sender_company__"),
	name: Data.GetValue("Sender_name__"),
	address: Data.GetValue("Sender_address__"),
	country: Data.GetValue("Sender_country__"),
	email: Data.GetValue("Sender_email__"),
	faxNumber: Data.GetValue("Sender_fax__"),
	phoneNumber: Data.GetValue("Sender_phone__"),
	skin: Data.GetValue("Sender_skin__"),
	ownerID: Variable.GetValueAsString("Sender_ownerID__"),
	modProvider: Variable.GetValueAsString("Sender_modProvider__"),
	language: Variable.GetValueAsString("Sender_language__"),
	accountID: Variable.GetValueAsString("Sender_accountID__"),
	internalUser: Users.GetUserAsProcessAdmin(this.ownerID),
	getEnvironment: function ()
	{
		if (!this.internalUser)
		{
			this.internalUser = Users.GetUserAsProcessAdmin(this.ownerID);
		}
		return this.internalUser.GetValue("Environment");
	},
	computeEmailTemplateContent: function (templateParameters)
	{
		if (!this.internalUser)
		{
			this.internalUser = Users.GetUserAsProcessAdmin(this.ownerID);
		}
		if (this.internalUser)
		{
			return this.internalUser.GetTemplateContent(templateParameters).content;
		}
		Log.Warn("sender.computeEmailTemplateContent return empty content");
		return "";
	}
};

var recipient = {
	internalUser: null,
	accountID: "",
	recipientID: Data.GetValue("Recipient_ID__"),
	login: Variable.GetValueAsString("Recipient_login__"),
	displayName: Data.GetValue("Recipient_name__"),
	exists: false,
	language: Variable.GetValueAsString("Recipient_language__"),
	culture: Variable.GetValueAsString("Recipient_culture__"),
	timeZoneIdentifier: Variable.GetValueAsString("Recipient_timezone__"),
	ownerID: Variable.GetValueAsString("Recipient_ownerId__"),
	company: "",
	deliveryMethod: Data.GetValue("Delivery_method__"),
	address: Data.GetValue("Recipient_address__"),
	emailAddress: Sys.DD.ComputeRedirectEmailAddress("Redirect_recipient_notifications", Data.GetValue("Recipient_email__")),
	phoneNumber: Data.GetValue("Recipient_phone__"),
	faxNumber: Data.GetValue("Recipient_fax__"),
	deliveryCopyEmail: Sys.DD.ComputeRedirectEmailAddress("Redirect_recipient_notifications", Data.GetValue("Recipient_copyemail__")),
	country: Data.GetValue("Recipient_country__"),
	allowedDeliveryMethods: "",
	lastConnectionDateTime: "",
	alreadySentWelcomeEmail: "",
	lastSentWelcomeEmailDateTime: "",
	alreadySentWelcomeLetter: "",
	noPasswordAllowed: "",
	passwordDefined: "",
	passwordUrl: Variable.GetValueAsString("Recipient_PasswordUrl__"),
	portalUrl: Variable.GetValueAsString("Recipient_portalUrl__"),
	shouldSendWelcomeEmail: Variable.GetValueAsString("Recipient_sendWelcomeEmail__") ? Variable.GetValueAsString("Recipient_sendWelcomeEmail__") === "1" : false,
	shouldSendWelcomeLetter: Variable.GetValueAsString("Recipient_sendWelcomeLetter__") ? Variable.GetValueAsString("Recipient_sendWelcomeLetter__") === "1" : false,
	parsedOptions: "",
	isCreatedOnTheFly: false,
	firstPassword: Variable.GetValueAsString("Recipient_firstPassword__"),

	load: function (accountID, recipientID)
	{
		this.accountID = accountID;
		this.recipientID = recipientID;
		this.login = this.login || this.accountID + "$" + this.recipientID;

		this.internalUser = Users.GetUserAsProcessAdmin(this.login);

		if (!this.internalUser && Sys.DD.GetParameter("AutoCreateRecipients"))
		{
			var that = this;
			if (!Process.PreventConcurrentAccess("DD_UpdateRecipient-" + this.login, function ()
			{
				that.internalUser = Users.GetUserAsProcessAdmin(that.login);
				that.internalLoad.call(that, true);
			}))
			{
				throw "Could not get critical section on recipient. Retrying.";
			}
		}
		else
		{
			this.internalLoad();
		}

		return this.exists;
	},

	internalLoad: function (isLocked)
	{
		if (!this.internalUser && Sys.DD.GetParameter("AutoCreateRecipients"))
		{
			this.internalUser = CreateRecipientOnTheFly(this.accountID, this.recipientID);
		}

		if (this.internalUser)
		{
			var recipientVars = this.internalUser.GetVars();

			this.exists = true;
			this.language = this.language || recipientVars.GetValue_String("Language", 0);
			this.displayName = recipientVars.GetValue_String("DisplayName", 0);
			this.login = this.login || recipientVars.GetValue_String("Login", 0);
			this.ownerID = this.ownerID || ("cn=" + this.login + "," + recipientVars.GetValue_String("OwnerID", 0));
			this.company = recipientVars.GetValue_String("Company", 0);
			this.phoneNumber = recipientVars.GetValue_String("PhoneNumber", 0);
			this.faxNumber = this.faxNumber || recipientVars.GetValue_String("FaxNumber", 0);
			this.country = recipientVars.GetValue_String("Country", 0);
			this.allowedDeliveryMethods = recipientVars.GetValue_String("AllowedDeliveryMethods", 0);
			this.lastConnectionDateTime = recipientVars.GetValue_String("LastConnectionDateTime", 0);
			this.alreadySentWelcomeEmail = recipientVars.GetValue_String("AlreadySentWelcomeEmail", 0);
			this.lastSentWelcomeEmailDateTime = recipientVars.GetValue_String("LastSentWelcomeEmailDateTime", 0);
			this.alreadySentWelcomeLetter = recipientVars.GetValue_String("AlreadySentWelcomeLetter", 0);
			this.noPasswordAllowed = this.internalUser.NoPasswordAllowed();
			this.passwordDefined = recipientVars.GetValue_String("Password", 0);

			this.portalUrl = this.portalUrl || this.internalUser.GetPortalURL(true);
			if (!Variable.GetValueAsString("Recipient_sendWelcomeEmail__") || Variable.GetValueAsString("Recipient_sendWelcomeEmail__") === "0")
			{
				this.shouldSendWelcomeEmail = this.internalShouldSendWelcomeEmail();
			}

			if (!Variable.GetValueAsString("Recipient_sendWelcomeLetter__") || Variable.GetValueAsString("Recipient_sendWelcomeLetter__") === "0")
			{
				this.shouldSendWelcomeLetter = this.internalShouldSendWelcomeLetter();
			}

			if (this.shouldSendWelcomeLetter || this.shouldSendWelcomeEmail)
			{
				if (isLocked)
				{
					this.processShouldSendWelcomeInfos(isLocked);
				}
				else
				{
					var that = this;
					if (!Process.PreventConcurrentAccess("DD_UpdateRecipient-" + this.recipientLogin, function ()
					{
						that.processShouldSendWelcomeInfos.apply(that);
					}))
					{
						throw "Could not get critical section on recipient. Retrying.";
					}
				}
			}

			this.parsedOptions = Lib.DD.GetUserOptions(recipientVars.GetValue_String("Options", 0));

			// Add or override parsedOptions values with extended properties custom table fields
			var fieldNames = [
				"Request_validation_for_all_invoices__",
				"NbAdditionalModCopies__"
			];
			this.addExtendedProperties(recipientVars, fieldNames);
			this.nbAdditionalModCopies = this.getAdvancedOption("NbAdditionalModCopies");

			if (!this.deliveryMethod)
			{
				Log.Warn("Switching to default configuration delivery because recipient's preferred delivery has not been set.");
				this.deliveryMethod = Sys.DD.GetParameter("DefaultDelivery__");
			}

			this.requestValidationForAllDocuments = this.getAdvancedOption("Request_validation_for_all_invoices") === "1";
		}
	},

	internalShouldSendWelcomeEmail: function ()
	{
		var whenSendWelcomeEmail = Sys.DD.GetParameter("WhenSendWelcomeEmail");
		var IsWelcomeEmailSent = whenSendWelcomeEmail === "ONNEVERLOGGED" || (whenSendWelcomeEmail === "ONFIRSTDOCUMENT" && this.alreadySentWelcomeEmail !== "1");
		if (this.exists && !this.lastConnectionDateTime && IsWelcomeEmailSent)
		{
			// Do not send WelcomeEmail more than twice a day
			if (this.lastSentWelcomeEmailDateTime)
			{
				var lastSentWelcomeEmail = Sys.Helpers.Date.ISOSTringToDate(this.lastSentWelcomeEmailDateTime);
				var now = new Date();
				if (lastSentWelcomeEmail.getTime() > (now.getTime() - 86400000))
				{
					Log.Info("Welcome email ignored because already sent in last 24hours");
					return false;
				}
			}

			return true;
		}
		return false;
	},
	internalShouldSendWelcomeLetter: function ()
	{
		var whenSendWelcomeLetter = Sys.DD.GetParameter("WhenSendWelcomeLetter");
		var IsWelcomeLetterSent = whenSendWelcomeLetter === "ONNEVERLOGGED" || (whenSendWelcomeLetter === "ONFIRSTDOCUMENT" && this.alreadySentWelcomeLetter !== "1");
		return this.exists && !this.lastConnectionDateTime && IsWelcomeLetterSent;
	},
	processSendWelcomeEmail: function ()
	{
		this.internalUser.UpdateWelcomeInfoDate("Email");

		// Generate password url only if noPasswordAllowed is disabled or if user has defined a password
		if (!this.noPasswordAllowed || this.passwordDefined)
		{
			this.passwordUrl = this.passwordUrl || this.internalUser.GeneratePasswordURL();
		}

		return true;
	},
	processShouldSendWelcomeLetter: function ()
	{
		this.internalUser.UpdateWelcomeInfoDate("Letter");
		this.firstPassword = this.firstPassword || this.internalUser.GenerateFirstPassword();
		return !!this.firstPassword;
	},
	processShouldSendWelcomeInfos: function (isLocked)
	{
		if (!isLocked)
		{
			this.internalUser = Users.GetUserAsProcessAdmin(this.login);
			var recipientVars = this.internalUser.GetVars();
			this.lastConnectionDateTime = recipientVars.GetValue_String("LastConnectionDateTime", 0);
			this.alreadySentWelcomeEmail = recipientVars.GetValue_String("AlreadySentWelcomeEmail", 0);
			this.lastSentWelcomeEmailDateTime = recipientVars.GetValue_String("LastSentWelcomeEmailDateTime", 0);
			this.alreadySentWelcomeLetter = recipientVars.GetValue_String("AlreadySentWelcomeLetter", 0);
			this.shouldSendWelcomeEmail = this.internalShouldSendWelcomeEmail();
			this.shouldSendWelcomeLetter = this.internalShouldSendWelcomeLetter();
		}

		if (this.shouldSendWelcomeEmail)
		{
			this.shouldSendWelcomeEmail = this.processSendWelcomeEmail();
		}

		if (this.shouldSendWelcomeLetter)
		{
			this.shouldSendWelcomeLetter = this.processShouldSendWelcomeLetter();
		}
	},
	compareOption: function (optionName, compareWith)
	{
		return optionName
			&& optionName.toLowerCase() in this.parsedOptions
			&& this.parsedOptions[optionName.toLowerCase()] === compareWith;
	},
	addExtendedProperties: function (recipientVars, fieldNames)
	{
		if (!recipientVars || !Array.isArray(fieldNames))
		{
			return;
		}

		for (var i = 0; i < fieldNames.length; i++)
		{
			var fieldName = fieldNames[i];
			if (fieldName)
			{
				var equalIndex = fieldName.indexOf("__");
				if (equalIndex !== -1)
				{
					var parsedOptionName = fieldName.substring(0, equalIndex);
					parsedOptionName = parsedOptionName.toLowerCase();
					var value = recipientVars.GetValue_String(fieldName, 0);
					if (value)
					{
						this.parsedOptions[parsedOptionName] = value;
					}
				}
			}
		}
	},
	getAdvancedOption: function (optionName)
	{
		var externalVarOptions;
		var recipientOptions = Variable.GetValueAsString("Recipient_options__");
		if (recipientOptions)
		{
			try
			{
				externalVarOptions = JSON.parse(recipientOptions);
			}
			catch (ex)
			{
				Log.Warn(ex);
			}
		}
		var opt;
		if (externalVarOptions)
		{
			opt = externalVarOptions[optionName.toLowerCase()];
		}
		if (!opt)
		{
			opt = this.parsedOptions ? this.parsedOptions[optionName.toLowerCase()] : "";
		}

		return opt || "";
	},
	computeEmailTemplateContent: function (templateParameters)
	{
		if (!this.internalUser)
		{
			this.internalUser = Users.GetUserAsProcessAdmin(this.ownerID);
		}
		if (this.internalUser)
		{
			return this.internalUser.GetTemplateContent(templateParameters).content;
		}
		Log.Warn("recipient.computeEmailTemplateContent return empty content");
		return "";
	}
};

Sys.Helpers.TryCallFunction(
	"Lib.DD.Customization.Deliveries.PrepareDeliveriesGeneration",
	Lib.DD.CreateCallbackParametersUsers(sender, recipient)
);

var customTags;

// ------------------------------
// Entry point, main algorithm
// ------------------------------

function Main()
{
	Variable.SetValueAsString("doPostFeedBackGuessingType", "true");

	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsFinalization.OnFinalizationScriptBegin");

	var canBeLinkedToAnotherDocument = Sys.DD.GetParameter("LinkToAnotherDocument");
	var neverLinkedToAnotherDocument = Variable.GetValueAsString("MasterDocumentLinked") !== "1";
	var routingSuccess = Variable.GetValueAsString("RoutingSuccess") === "true";
	var behaviorOnExpiration = Sys.DD.GetParameter("BehaviorOnExpiration");
	var hasExpired = Process.AutoValidatingOnExpiration();
	var generateTransports = !(routingSuccess || canBeLinkedToAnotherDocument) || (hasExpired && behaviorOnExpiration === "SEND");
	var setProcessInError = hasExpired && behaviorOnExpiration === "ERROR";
		
	if (documentData.type)
	{
		if (!Data.GetValue("ConfigurationName__"))
		{
			var configurationVars = Sys.Helpers.Database.GetFirstRecordResult(Sys.DD.settingTableName, "(document_type__=" + documentData.type + ")");
			if (configurationVars)
			{
				Variable.SetValueAsString("ConfigurationSetUserValidation__", "true");
			}
			else
			{
				var deliveryMethod = Data.GetValue("Delivery_method__");
				configurationVars = Sys.DD.Configuration.CreateNewConfiguration(documentData.type, null, deliveryMethod);

				Variable.SetValueAsString("RUIDExNewConfiguration", configurationVars.GetValue_String("RuidEx", 0));
			}
			var configurationParameters = Sys.Helpers.Database.RecordVarsToJSON(configurationVars);
			Sys.DD.UpdateParameters(configurationParameters, true);
			Data.SetValue("ConfigurationName__", Sys.DD.GetParameter("ConfigurationName__"));
		}
		else if (Sys.DD.GetParameter("ForceValidation__") === "1" && Data.GetValue("Enable_touchless__"))
		{
			Sys.DD.Configuration.EnableTouchless(documentData.type);
		}
	}
	TryLoadRecipient();

	if (setProcessInError)
	{
		var errorMessage = Language.Translate("_The related document hasn't been found");
		Data.SetValue("State", 200);
		Data.SetValue("StatusCode", 200);
		Data.SetValue("ShortStatusTranslated", errorMessage);
		Data.SetValue("ShortStatus", errorMessage);
		Data.SetValue("LongStatus", errorMessage);
		Data.SetValue("CompletionDateTime", new Date());
	}
	else if (generateTransports)
	{
		GenerateTransports();
	}

	if (generateTransports || setProcessInError)
	{
		Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeSenderFormGeneration");

		// Clean External Variables in case of Resubmit
		if (Variable.GetValueAsString("GeneratedPdfIndex"))
		{
			Variable.SetValueAsString("GeneratedPdfIndex", "");
		}
	}

	if (!generateTransports && !setProcessInError && neverLinkedToAnotherDocument && !routingSuccess)
	{
		Log.Info("This document wait to be linked to another document.");
		// This process wait to be wake up by the master document when it will be submitted.
		// If this process expires, this script will be called again to execute the selected behavior on expiration.
		Process.WaitForUpdate();
		// Set the validity date time
		var validityDateTime = new Date(Data.GetValue("SubmitDateTime"));
		var masterDocumentExpirationTime = Sys.DD.GetParameter("MasterDocumentExpirationTime");
		validityDateTime.setMinutes(validityDateTime.getMinutes() + (masterDocumentExpirationTime * 60));
		Data.SetValue("ValidityDateTime", validityDateTime);
	}

	if(Variable.GetValueAsString("doPostFeedBackGuessingType") !== "false")
	{
		PostFeedBackGuessingType();
	}

	if (hasExpired)
	{
		Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeSenderFormGenerationExpiration");
	}

	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsFinalization.OnFinalizationScriptEnd");
}

var transportSubject;
var transportSubjectCopy;
var callback = Lib.DD.CreateCallbackParametersUsers(sender, recipient);

function GenerateTransports()
{
	// If recipient doesn't exists, we'll need to get sender object for culture and language
	var destinationLanguage = recipient.exists ? recipient.language : sender.language;
	transportSubject = Language.TranslateInto("_Subject text", destinationLanguage, true, documentData.ID, sender.company);
	transportSubjectCopy = Language.TranslateInto("_Subject text copy", destinationLanguage, true, documentData.ID, sender.company);

	var maindocumentAttachIndex = Sys.DD.Document.GetMainAttachmentIndex(documentData.ID);
	var emailsProperties = Lib.DD.Emails.GetEmailsProperties("SenderForm");
	if (emailsProperties)
	{
		Variable.SetValueAsString("From_Name__", emailsProperties.fromName);
		Variable.SetValueAsString("From_Email_Address__", emailsProperties.fromEmailAddress);
	}

	var anyDeliveryAndRecipient = recipient.deliveryMethod !== "NONE" && recipient.exists;
	var deliverySuccess = true;
	var routingSuccess = true;

	var inboundMode = Sys.DD.GetParameter("Routing") === "Routing message";
	if (recipient.deliveryMethod === "SM" && inboundMode)
	{
		deliverySuccess = RerouteEmail(Data.GetValue("Recipient_email__"));
	}
	else if (recipient.deliveryMethod === "SM" && !recipient.exists)
	{
		deliverySuccess = SendEmail(recipient.emailAddress, maindocumentAttachIndex, emailsProperties);
	}
	else if (recipient.deliveryMethod === "PORTAL" && !recipient.exists)
	{
		Variable.SetValueAsString("RoutingErrorMessage", Language.Translate("An error occurred when trying to send by PORTAL to a non existing recipient"));
		deliverySuccess = false;
	}
	else if (recipient.deliveryMethod === "FGFAXOUT")
	{
		deliverySuccess = SendFax(recipient.faxNumber);
	}
	else if (recipient.deliveryMethod === "COP")
	{
		routingSuccess = false;
		deliverySuccess = SendSOP();
	}
	else if (recipient.deliveryMethod === "CONVERSATION")
	{
		deliverySuccess = SendConversation();
	}
	else if (recipient.deliveryMethod === "MOD")
	{
		deliverySuccess = SendMOD(recipient.address, sender.address);
	}
	else if (recipient.deliveryMethod === "APPLICATION_PROCESS")
	{
		routingSuccess = false;
		deliverySuccess = SendToApplicationProcess();
	}
	else if (recipient.deliveryMethod !== "PORTAL" && recipient.deliveryMethod !== "SM")
	{
		Log.Warn("No transport sent in 'SenderForm' process");
	}

	var emailCopyAdress = recipient.deliveryCopyEmail;
	if (!recipient.exists && emailCopyAdress.indexOf("@") !== -1)
	{
		deliverySuccess = SendCopyEmail(emailCopyAdress, maindocumentAttachIndex, emailsProperties);
	}

	if (anyDeliveryAndRecipient && deliverySuccess)
	{
		Data.SetValue("StartProcessNameOnView", "Sys_Update Records");
		var processOnViewParams = "Update_Type__=1;Target_Ruid__=%[data:RUIDEX];View_Date__=%[NOW];First_viewed_date_field__=FirstViewedDateTime__;";
		processOnViewParams += "Last_viewed_date_field__=LastViewedDateTime__;NextTryDateTime=NOW()";
		Data.SetValue("StartProcessOnViewParams", processOnViewParams);

		Process.SetRight(recipient.login, "read");
		SendRecipientEmails(maindocumentAttachIndex, emailsProperties);
	}
	if (!deliverySuccess)
	{
		routingSuccess = false;
		Variable.SetValueAsString("RoutingSuccess", routingSuccess ? "true" : "false");
		Process.Exit(70, Variable.GetValueAsString("RoutingErrorMessage"));
	}
	else
	{
		Variable.SetValueAsString("RoutingSuccess", routingSuccess ? "true" : "false");
	}
}

function SendRecipientEmails(maindocumentAttachIndex, emailsProperties)
{
	var shouldGroupEmails = ShouldGroupEmails();

	if (recipient.deliveryMethod === "SM")
	{
		if (!shouldGroupEmails)
		{
			SendEmail(recipient.emailAddress, maindocumentAttachIndex, emailsProperties);
		}
		else
		{
			Data.SetValue("Email_grouping_state__", "GROUPABLE_WAITING");

			ComputeAttachmentsForGrouping(maindocumentAttachIndex);

			// As SM is the main delivery method, the invoice don't have to be in
			// success before the invoice has effectively by sent to the recipient.
			// So, contrary to PORTAL delivery method, we block the process in
			// state 90 for a short period until the AR Email Grouping is executed.
			Process.WaitForUpdate();
			Sys.DD.Helpers.SetValidityDateTime(g_groupingEmailValidityDurationDays);
		}
	}
	else if (IsNotificationRequested())
	{
		if (!shouldGroupEmails)
		{
			SendNotificationEmail(emailsProperties);
		}
		else
		{
			Data.SetValue("Email_grouping_state__", "GROUPABLE_WAITING");
		}
	}

	if (recipient.exists && recipient.passwordUrl && Lib.DD.Emails.ShouldSendWelcomeEmailSeparately(recipient.deliveryMethod))
	{
		SendWelcomeEmail(recipient.emailAddress, emailsProperties);
	}

	// /!\
	// The copy email must stay at the end of the process
	// So that, in case of retry, it is not sent multiple times
	// /!\

	// Send invoice copy by email
	var emailCopyAdress = recipient.deliveryCopyEmail;
	if (emailCopyAdress.indexOf("@") !== -1)
	{
		SendCopyEmail(emailCopyAdress, maindocumentAttachIndex, emailsProperties);
	}
}

function ShouldGroupEmails()
{
	var isDocumentToGroup = false;
	var doNotGroupEmailsForThisCustomer = Variable.GetValueAsString("CustomerOptionDoNotGroupEmails") === "1";

	if (recipient.deliveryMethod === "SM" && Sys.DD.GetParameter("GroupEmailWithAttach"))
	{
		if (!doNotGroupEmailsForThisCustomer)
		{
			isDocumentToGroup = true;
		}
		else
		{
			Log.Info("SM email grouping disabled because DoNotGroupEmails__ set on the recipient's advanced properties");
		}
	}
	else if (IsNotificationRequested() && Sys.DD.GetParameter("GroupEmailNotifications"))
	{
		if (!doNotGroupEmailsForThisCustomer)
		{
			isDocumentToGroup = true;
		}
		else
		{
			Log.Info("PORTAL email grouping disabled because DoNotGroupEmails__ set on the recipient's advanced properties");
		}
	}

	// If defined, ActivateEmailGrouping user exit overrides both configuration setting and recipient settings
	var emailGroupingUserExitName = "Lib.DD.Customization.Deliveries.ActivateEmailGrouping";
	var callbackParameters = Lib.DD.CreateCallbackParametersUsers(sender, recipient);
	var activateEmailGroupingUserExitResult = Sys.Helpers.TryCallFunction(emailGroupingUserExitName, recipient.deliveryMethod, isDocumentToGroup, callbackParameters);
	if (typeof activateEmailGroupingUserExitResult === "boolean")
	{
		Log.Info("Email grouping is overriden by the user exit Lib.DD.Customization.Deliveries.ActivateEmailGrouping, result is: " + activateEmailGroupingUserExitResult);
		isDocumentToGroup = activateEmailGroupingUserExitResult;
	}

	return isDocumentToGroup;
}

function IsNotificationRequested()
{
	var isDMPortal = recipient.deliveryMethod === "PORTAL";
	var isDMEmail = recipient.deliveryMethod === "SM";
	var paramAlwaysSendNotif = Sys.DD.GetParameter("AlwaysSendNotification");
	var recipientHasEmail = recipient.email !== "";

	return isDMPortal || (!isDMEmail && paramAlwaysSendNotif && recipientHasEmail);
}

function ComputeAttachmentsForGrouping(maindocumentAttachIndex)
{
	var attachOutputName = "";
	var outputFilenamePattern = Sys.DD.GetParameter("OutputFilenamePattern");
	if (outputFilenamePattern)
	{
		attachOutputName = Sys.DD.Helpers.ComputeAttachmentName(outputFilenamePattern);
	}
	else
	{
		attachOutputName = Attach.GetName(maindocumentAttachIndex);
	}

	var attachments = [];
	attachments.push({
		fileName: Attach.GetAttachConvertedPath(maindocumentAttachIndex) || Attach.GetAttach(maindocumentAttachIndex).GetAttachFile(),
		outputName: attachOutputName,
		outputFormat: Attach.GetExtension(maindocumentAttachIndex),
		size: Attach.GetSize(maindocumentAttachIndex)
	});

	//If the checkbox is not set in the wizard, we only add the current process document
	if (Sys.DD.GetParameter("AddAttachmentsWithEmail"))
	{
		var nbAttach = Attach.GetNbAttach();
		for (var i = 0; i < nbAttach; i++)
		{
			if (i !== maindocumentAttachIndex)
			{
				// Get converted file in priority
				attachments.push({
					fileName: Attach.GetAttachConvertedPath(i) || Attach.GetAttach(i).GetAttachFile(),
					outputName: Attach.GetName(i),
					outputFormat: Attach.GetExtension(i),
					size: Attach.GetSize(i)
				});
			}
		}

		var documentsSummaryJSON = Variable.GetValueAsString("DocumentsSummaryJSON");
		if (documentsSummaryJSON)
		{
			var documentsSummary = JSON.parse(documentsSummaryJSON);
			var relatedDocumentsInfo = Sys.Outbound.Documents.GetRelatedDocumentsGroupingInfo(documentsSummary);
			attachments = attachments.concat(relatedDocumentsInfo);
		}
	}

	Variable.SetValueAsString("AttachmentFiles__", JSON.stringify(attachments));
}

// Send the document to recipient by email
function SendCopyEmail(deliveryCopyEmail, mainAttachIndex, emailsProperties)
{
	var config = {
		logText: "Sending copy e-mail to: ",
		destAddress: deliveryCopyEmail,
		isChildTransport: false,
		PDFCommands: true,
		subject: transportSubjectCopy,
		typeEmail: "EMAIL",
		transportType: "SM-COPY",
		transportIsOriginal: false,
		libName: "Finalize" + (recipient.exists ? "" : "Non") + "ExistingRecipientCopyEmailTransport",
		templateName: "DD_EmailCopy.htm",
		// Sender archive duration
		archiveDuration: Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")),
		mainAttachIndex: mainAttachIndex,
		emailsProperties: emailsProperties,
		documentData: documentData,
		recipient: recipient,
		sender: sender,
		callback: callback,
		billing: true,
		customTags: customTags
	};
	return Lib.DD.Emails.emailProcess(config);
}

// Create the welcome email
function SendWelcomeEmail(emailAddress, emailsProperties)
{
	var config = {
		logText: "Send welcome email to ",
		destAddress: emailAddress,
		isChildTransport: recipient.deliveryMethod === "PORTAL",
		subject: Language.TranslateInto("_Welcome email subject", recipient.language, true, documentData.ID, sender.company),
		typeEmail: "WELCOME_EMAIL",
		transportType: "SM-WELCOME",
		transportIsOriginal: false,
		libName: "FinalizeWelcomeEmailTransport",
		notif: true,
		templateName: "DD_EmailWelcome.htm",
		// Sender archive duration
		archiveDuration: Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")),
		welcomeInformations: Lib.DD.Emails.GetWelcomeEmailLoginInfo(recipient),
		emailsProperties: emailsProperties,
		documentData: documentData,
		recipient: recipient,
		sender: sender,
		callback: callback,
		billing: true,
		customTags: customTags
	};
	return Lib.DD.Emails.emailProcess(config);
}


// Send the document to recipient by email
function SendNotificationEmail(emailsProperties)
{
	var config = {
		logText: "Adding an e-mail notification for publication addressed to: ",
		destAddress: recipient.emailAddress,
		isChildTransport: false,
		subject: Language.TranslateInto("_Subject text", recipient.language, true, documentData.ID, sender.company),
		typeEmail: "NOTIF_EMAIL",
		transportType: "SM-PORTAL",
		transportIsOriginal: false,
		libName: "FinalizeRecipientNotifEmailTransport",
		notif: true,
		templateName: "DD_EmailDelivery" + (recipient.exists ? "" : "NoRecipient") + ".htm",
		// Sender archive duration
		archiveDuration: Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")),
		welcomeInformations: !Lib.DD.Emails.ShouldSendWelcomeEmailSeparately(recipient.deliveryMethod) ? Lib.DD.Emails.GetWelcomeEmailLoginInfo(recipient) : "",
		emailsProperties: emailsProperties,
		documentData: documentData,
		recipient: recipient,
		sender: sender,
		callback: callback,
		customTags: customTags
	};
	return Lib.DD.Emails.emailProcess(config);
}


function ExtractEmailBodyAndDisplayName()
{
	var messageEx = Data.GetValue("MessageEx");
	if (messageEx)
	{
		var parts = messageEx.split("\n");
		var emailBody = "";
		var displayName = "";
		var subject = "";

		if (parts.length >= 3)
		{
			displayName = parts[0];
			subject = parts[1];
			// email body can contain multiple \n
			emailBody = parts.slice(2).join("\n");
		}
		else
		{
			emailBody = parts[0];
		}

		return {
			emailBody: emailBody,
			displayName: displayName,
			subject: subject
		};
	}
	return null;
}

function SendConversation()
{

	var formMsnex = Data.GetValue("FormRelatedToConversationMsnex__");
	if (formMsnex == null)
	{
		Log.Error("Missing MSNEX, routing to conversation needs the msnex of the related document");
		return false;
	}

	var additionalSettings = null;
	try
	{
		additionalSettings = JSON.parse(Sys.DD.GetParameter("AdditionalSettings__"));
	}
	catch (e)
	{
		Log.Error("Conversation settings not found or invalid for document type " + Data.GetValue("Document_Type__"));
		return false;
	}
	if (!additionalSettings.conversations)
	{
		Log.Error("Settings for document type " + Data.GetValue("Document_Type__") + " do not have 'conversations' member");
		return false;
	}
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Configuration.ModifyConversationSettings", additionalSettings.conversations);

	var emailAddress = Data.GetValue("Email_From__");
	Log.Info("Creating Conversation for " + emailAddress);

	var result = ExtractEmailBodyAndDisplayName();
	var emailSubject = result ? result.subject : null;
	var emailBodyText = result ? result.emailBody : null;
	if (emailBodyText == null)
	{
		Log.Error("Failed extracting email body ");
		return false;
	}

	var externalContributors = {};
	externalContributors[emailAddress] =
	{
		"emailAddress": emailAddress,
		"displayName": result.displayName
	};

	return Lib.Conversations.CreateConversation(
		additionalSettings.conversations,
		formMsnex,
		emailBodyText,
		emailSubject,
		emailAddress,
		result.displayName,
		"SDACONV__",
		"custom",
		{
			notifyByEmail: true,
			allowAttachments: true,
			externalContributors : externalContributors
		});

}

function SendSOP()
{
	try
	{
		Log.Info("Sending SOP");
		var sop = Process.CreateProcessInstance("Customer Order Processing", false);
		var vars = sop.GetUninheritedVars();

		vars.AddValue_String("Identifier", documentData.ID, true);
		// This is the ref. doc so we set attachToProcess to true
		Sys.DD.Document.AttachDocument(sop, 0, true, false, null);
		Sys.DD.Document.AttachOriginalDocuments(sop, 1);

		// Add custom informations about the transport
		vars.AddValue_String("ToUser1", JSON.stringify(
			{
				"DD-transportType": "SOP-ORIGINAL",
				"DD-transportIsOriginal": true
			}
		), true);

		var configuration = Data.GetValue("ProcessRelatedConfiguration__");
		var extVars = sop.GetExternalVars();
		extVars.AddValue_String("Configuration", configuration, true);
		extVars.AddValue_String("SDADocTypeSource", Data.GetValue("Document_Type__"), true);
		extVars.AddValue_String("SDAIdentifier", Data.GetValue("RUIDEx"), true);
		extVars.AddValue_String("SDADocumentFamily", Variable.GetValueAsString("SDADocumentFamily"), true);

		Sys.Helpers.TryCallFunction(
			"Lib.DD.Customization.Deliveries.FinalizeApplicationTransport",
			sop,
			"COP",
			configuration, Lib.DD.CreateCallbackParametersUsers(sender, recipient)
		);
		sop.Process();
		if (sop.GetLastError() !== 0)
		{
			Variable.SetValueAsString("RoutingErrorMessage", sop.GetLastErrorMessage());
			return false;
		}
		Process.WaitForUpdate();
		return true;
	}
	catch (error)
	{
		Variable.SetValueAsString("RoutingErrorMessage", error.message);
		return false;
	}
}

function SendToApplicationProcess()
{
	try
	{
		var processName = Sys.DD.GetParameter("ProcessSelected__");
		Log.Info("Sending to application process: " + processName);
		var processToCreate = Process.CreateProcessInstance(processName, false);
		var vars = processToCreate.GetUninheritedVars();

		vars.AddValue_String("Identifier", documentData.ID, true);
		// This is the ref. doc so we set attachToProcess to true
		Sys.DD.Document.AttachDocument(processToCreate, 0, true, false, null);
		Sys.DD.Document.AttachOriginalDocuments(processToCreate, 1);

		var configuration = Data.GetValue("ProcessRelatedConfiguration__");
		var extVars = processToCreate.GetExternalVars();
		extVars.AddValue_String("Configuration", configuration, true);
		extVars.AddValue_String("SDADocTypeSource", Data.GetValue("Document_Type__"), true);
		extVars.AddValue_String("SDAIdentifier", Data.GetValue("RUIDEx"), true);

		Sys.Helpers.TryCallFunction(
			"Lib.DD.Customization.Deliveries.FinalizeApplicationTransport",
			processToCreate,
			"APPLICATION_PROCESS",
			configuration, Lib.DD.CreateCallbackParametersUsers(sender, recipient)
		);
		processToCreate.Process();
		if (processToCreate.GetLastError() !== 0)
		{
			Variable.SetValueAsString("RoutingErrorMessage", processToCreate.GetLastErrorMessage());
			return false;
		}

		var query = Process.CreateQuery();
		query.SetSpecificTable("EDD_PROCESS");
		query.SetFilter("ID=" + Process.GetProcessID(processName));

		if (!query.MoveFirst())
		{
			Variable.SetValueAsString("RoutingErrorMessage", "Failed to find process " + processName + "in DB");
			return false;
		}

		var record = query.MoveNextRecord();
		if (!record)
		{
			Variable.SetValueAsString("RoutingErrorMessage", "Failed to find process " + processName + "in DB");
			return false;
		}

		var eddProcessVars = record.GetVars();
		var processNode = eddProcessVars.FindSubNode("process");
		var notifySDAOnComplete = processNode.GetValue("NotifySDAOnComplete", 0);

		if (notifySDAOnComplete === "1")
		{
			Variable.SetValueAsString("doPostFeedBackGuessingType", "false");
			Process.WaitForUpdate();
		}

		return true;
	}
	catch (error)
	{
		Variable.SetValueAsString("RoutingErrorMessage", error.message);
		return false;
	}

}

function SetFieldIfEmpty(fieldName, fieldValue)
{
	if (Data.IsEmpty(fieldName))
	{
		Data.SetValue(fieldName, fieldValue);
	}
}

// ------------------------------
// Recipient load/creation
// ------------------------------

function TryLoadRecipient()
{
	if (recipient.load(sender.accountID, Data.GetValue("Recipient_ID__")))
	{
		SetFieldIfEmpty("Recipient_name__", recipient.company);
		SetFieldIfEmpty("Recipient_country__", recipient.country);
		SetFieldIfEmpty("Recipient_email__", recipient.emailAddress);
		SetFieldIfEmpty("Recipient_phone__", recipient.phoneNumber);
		SetFieldIfEmpty("Recipient_fax__", recipient.faxNumber);
		SetFieldIfEmpty("Recipient_copyemail__", recipient.deliveryCopyEmail);
	}

	customTags = { portalUrl: recipient.portalUrl };
}

function CreateRecipientOnTheFly(accountID, recipientID)
{
	//These parameters will be used to determine which delivery methods are authorized only for recipients created on the fly
	var allowedDeliveryMethods = [];

	if (Sys.DD.GetParameter("AllowMethodMOD"))
	{
		allowedDeliveryMethods.push('MOD');
	}
	if (Sys.DD.GetParameter("AllowMethodSM"))
	{
		allowedDeliveryMethods.push('SM');
	}
	if (Sys.DD.GetParameter("AllowMethodPORTAL"))
	{
		allowedDeliveryMethods.push('PORTAL');
	}
	if (Sys.DD.GetParameter("AllowMethodFax"))
	{
		allowedDeliveryMethods.push('FGFAXOUT');
	}

	var recipientToDuplicate = Users.GetUserAsProcessAdmin(accountID + "$" + "recipientusertemplate");
	var recipientName = Data.GetValue("Recipient_name__");
	if (!recipientName)
	{
		recipientName = recipientID;
	}

	var address = Data.ExtractPostalAddressUsingMailProvider(Data.GetValue("Recipient_address__"), sender.modProvider, false);
	var recipientOverrides = {
		"Company": recipientName,
		"DisplayName": recipientName,
		"CreationDateTime": "NOW()",
		"AlreadySentWelcomeLetter": 0,
		"LastSentWelcomeLetterDateTime": null,
		"EAgreement": 0,
		"EAgreementDateTime": null,
		"ERevokeDateTime": null,
		"PwdLastChangeDateTime": "NOW()",
		"AccountLocked": 0,
		"AlreadySentWelcomeEmail": 0,
		"LastSentWelcomeEmailDateTime": null,
		"Password": "",
		"AccountLockDateTime": null,
		"Locked": 0,
		"LastConnectionDateTime": null,
		"LastUpdateDateTime": "NOW()",
		"PreferredDeliveryMethod": Data.GetValue("Delivery_method__"),
		"AllowedDeliveryMethods": allowedDeliveryMethods.join('|'),
		"EmailAddress": Data.GetValue("Recipient_email__"),
		"PhoneNumber": Data.GetValue("Recipient_phone__"),
		"FaxNumber": Data.GetValue("Recipient_fax__"),
		"Street": address ? address.street : null,
		"ZipCode": address ? address.zipcode : null,
		"City": address ? address.city : null,
		"Country": address ? address.country : null,
		"Description": "Created from document id " + Data.GetValue("Document_ID__"),
		"BusinessPartnerID": Data.GetValue("Recipient_ID__"),
		"AllowNoPassword": Sys.DD.GetParameter("NewRecipientAllowNoPwd")
	};

	Sys.Helpers.TryCallFunction("Lib.DD.Customization.User.CustomizeAutomaticRecipientCreation", recipientOverrides);

	// Forward AllowNoPassword to real column in database (AllowNoPassword is just an helper for CustomizeAutomaticRecipientCreation callback)
	if (recipientOverrides.AllowNoPassword && recipientToDuplicate)
	{
		recipientOverrides.LoginType = recipientToDuplicate.GetVars().GetValue_Long("LoginType", 0) | 0x00000010;
	}
	// This column does not exists on User table so remove it from recipientOverrides
	delete recipientOverrides.AllowNoPassword;

	var internalRecipient = null;
	if (recipientToDuplicate)
	{
		internalRecipient = recipientToDuplicate.Clone(recipientID, recipientOverrides);
		recipient.isCreatedOnTheFly = true;
	}

	if (!internalRecipient)
	{
		Log.Info("Error during recipient creation. Verify that the recipient 'recipientusertemplate' exists.");
	}

	return internalRecipient;
}

// ------------------------------
// Email generation
// ------------------------------

// Send the document to recipient by email
function SendEmail(recipientEmail, mainAttachIndex, emailsProperties)
{
	if (recipientEmail.indexOf("@") < 0)
	{
		Variable.SetValueAsString("RoutingErrorMessage", "Invalid email address: " + recipientEmail);
		return false;
	}
	var config = {
		logText: "Sending e-mail to: ",
		destAddress: recipientEmail,
		isChildTransport: true,
		subject: transportSubject,
		typeEmail: "EMAIL",
		transportType: "SM-ORIGINAL",
		transportIsOriginal: true,
		libName: "Finalize" + (recipient.exists ? "" : "Non") + "ExistingRecipientEmailTransport",
		templateName: "DD_EmailDelivery" + (recipient.exists ? "" : "NoRecipient") + ".htm",
		archiveDuration: Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")),
		mainAttachIndex: mainAttachIndex,
		welcomeInformations: !Lib.DD.Emails.ShouldSendWelcomeEmailSeparately(recipient.deliveryMethod) ? Lib.DD.Emails.GetWelcomeEmailLoginInfo(recipient) : "",
		emailsProperties: emailsProperties,
		documentData: documentData,
		recipient: recipient,
		sender: sender,
		callback: callback,
		billing: true,
		customTags: customTags
	};
	return Lib.DD.Emails.emailProcess(config);
}

// Reroute email to recipient as is
function RerouteEmail(recipientEmail)
{
	if (recipientEmail && recipientEmail.indexOf("@") >= 0)
	{
		var routingSuccess = Lib.DD.Emails.rerouteEmailProcess(recipientEmail);
		if (!routingSuccess)
		{
			//Fallback in case rerouting didn't work
			var maindocumentAttachIndex = Sys.DD.Document.GetMainAttachmentIndex(documentData.ID);
			var emailsProperties = Lib.DD.Emails.GetEmailsProperties("SenderForm");
			routingSuccess = SendEmail(recipient.emailAddress, maindocumentAttachIndex, emailsProperties);
		}
		return routingSuccess;
	}
	Variable.SetValueAsString("RoutingErrorMessage", "Invalid email address: " + recipientEmail);
	return false;
}


// ------------------------------
// Fax generation
// ------------------------------

// Send the document to recipient by fax
function SendFax(recipientFaxNumber)
{
	Log.Info("Sending fax to: " + recipientFaxNumber);

	var fax = Process.CreateTransport("fax", true);

	// Get the fax parameters from the external variables set in the HTML page script
	var vars = fax.GetUninheritedVars();
	vars.AddValue_String("FaxNumber", recipientFaxNumber, true);

	var archiveDuration = Data.GetValue("ArchiveDuration");
	Sys.DD.Document.AddCommonFields(vars, transportSubject, "FAX", true, documentData, recipient, archiveDuration);
	Sys.DD.Document.AttachCurrentDocuments(fax);

	// Add custom informations about the transport
	vars.AddValue_String("ToUser1", JSON.stringify(
		{
			"DD-transportType": "FAX-ORIGINAL",
			"DD-transportIsOriginal": true
		}
	), true);

	Sys.DD.CopyBillingInfoTo(fax);
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeFaxTransport", fax, Lib.DD.CreateCallbackParametersUsers(sender, recipient));

	fax.Process();
	if (fax.GetLastError() !== 0)
	{
		Variable.SetValueAsString("RoutingErrorMessage", fax.GetLastErrorMessage());
		return false;
	}
	return true;
}

// ------------------------------
// MOD generation
// ------------------------------

// Send the document to recipient by mail
function SendMOD(recipientAddress, senderAddress)
{
	try
	{
		Log.Info("Sending MOD to: " + recipientAddress);

		var mod = Process.CreateTransport("MODEsker", true);
		var vars = mod.GetUninheritedVars();

		vars.AddValue_String("ToBlockAddress", recipientAddress, true);
		if (senderAddress)
		{
			vars.AddValue_String("FromBlockAddress", senderAddress, true);
		}

		var archiveDuration = Data.GetValue("ArchiveDuration");
		Sys.DD.Document.AddCommonFields(vars, transportSubject, "MOD", true, documentData, recipient, archiveDuration);

		// MOD options set in configuration
		vars.AddValue_String("Color", Sys.DD.GetParameter("MOD_Option_Color"), true);
		vars.AddValue_String("Bothsided", Sys.DD.GetParameter("MOD_Option_Bothsided"), true);
		vars.AddValue_String("Cover", Sys.DD.GetParameter("MOD_Option_Cover"), true);
		vars.AddValue_String("CoverOnC4", Sys.DD.GetParameter("MOD_Option_CoverOnC4"), true);
		vars.AddValue_String("FirstPageOnOddPage", Sys.DD.GetParameter("MOD_Option_FirstPageOnOddPage"), true);

		if (Sys.DD.GetParameter("MOD_Option_MailService") === "ECONOMY_IF_APPLICABLE")
		{
			var modProvider = Variable.GetValueAsString("Sender_modProvider__");
			var mfpStampTypes = Sys.DD.GetMFPStampTypes();

			if (mfpStampTypes[modProvider] && mfpStampTypes[modProvider].economy)
			{
				Log.Info("MOD mail service is \"ECONOMY_IF_APPLICABLE\" for MODProvider=" + modProvider + ", MOD variable \"StampType\" set to: " + mfpStampTypes[modProvider].economy);
				vars.AddValue_String("StampType", mfpStampTypes[modProvider].economy, true);
			}
			else
			{
				Log.Info("MOD mail service is \"ECONOMY_IF_APPLICABLE\" but couldn't find an economy stamping for MODProvider=" + modProvider);
			}
		}

		// allow to resize document to avoid overwriting crucial data on document
		if (Sys.DD.GetParameter("MOD_Option_Resize"))
		{
			var scale = Sys.DD.GetParameter("MOD_Option_Scale", 0);
			var offsetX = Sys.DD.GetParameter("MOD_Option_OffsetX", 0);
			var offsetY = Sys.DD.GetParameter("MOD_Option_OffsetY", 0);

			// scale is from 0 to 1
			var shouldScale = scale && scale > 0 && scale <= 1;

			// offset are in mm in configuration
			var shouldTranslate = (offsetX && offsetX > 0) || (offsetY && offsetY > 0);

			// anchor is only topleft for now : need to add a new parameter
			var cmd = "-modify %infile[1]% ALL -tl";
			if (shouldScale && shouldTranslate)
			{
				cmd += "ST " + scale + " " + offsetX + " " + offsetY;
			}
			else if (shouldScale)
			{
				cmd += "S " + scale;
			}
			else if (shouldTranslate)
			{
				cmd += "T " + offsetX + " " + offsetY;
			}

			Sys.DD.AddPdfCommand(vars, cmd);
		}

		var customPdfCommand = Sys.DD.GetParameter("MOD_Option_PDFCommand__");
		if (customPdfCommand)
		{
			Sys.DD.AddPdfCommand(vars, customPdfCommand.replace(/\\r\\n/g, "\n"));
		}

		HandleModAdditionalCopies(mod, vars);

		// For demo purpose the following feature will resize the document if the destination MFP requires it
		if (Sys.DD.GetParameter("Configuration_type__") === "DEMO")
		{
			var documentCulture = Variable.GetValueAsString("DocumentCulture");

			var hasToBeResized = Lib.DD.DocumentHasToBeResized(sender.modProvider,
				Variable.GetValueAsString("DocumentWidth"),
				Variable.GetValueAsString("DocumentHeigth"),
				Variable.GetValueAsString("DocumentResX"),
				Variable.GetValueAsString("DocumentResY"));

			if (hasToBeResized)
			{
				// Warning: This function will add or concatenate a PDF command
				Lib.DD.ResizeDocument(vars, sender.modProvider);
			}

			if (hasToBeResized || Lib.DD.DocumentNeedsACoverPage(documentCulture, sender.modProvider))
			{
				vars.AddValue_String("Cover", "Y", true);
			}
		}

		if (recipient.shouldSendWelcomeLetter)
		{
			AttachWelcomeLetter(mod, recipient.firstPassword, recipientAddress);
		}
		Sys.DD.Document.AttachCurrentDocuments(mod);

		// Add custom informations about the transport
		vars.AddValue_String("ToUser1", JSON.stringify(
			{
				"DD-transportType": "MOD-ORIGINAL",
				"DD-transportIsOriginal": true
			}
		), true);

		// Add grouping options
		var groupingMOD = Sys.DD.GetParameter("MOD_Option_Grouping__");
		if (groupingMOD === "1")
		{
			var serverDate = new Date();
			var deferredDateTime = GetDeferredDateTime(serverDate);

			// Set variables for grouping
			vars.AddValue_String("DocumentGrouping", "1", true);
			vars.AddValue_Date("DeferredDateTime", deferredDateTime, true);

			var DeferredAfterValidation = Sys.DD.GetParameter("DeferredTimeAfterValidation__");
			if (DeferredAfterValidation === "1")
			{
				var deferredTimeAfterValidation = Sys.Helpers.Date.Format(deferredDateTime, "HH:MM:ss");
				vars.AddValue_String("DeferredTimeAfterValidation", deferredTimeAfterValidation, true);
			}
		}

		Sys.DD.CopyBillingInfoTo(mod);
		Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeMODTransport", mod, Lib.DD.CreateCallbackParametersUsers(sender, recipient));

		mod.Process();
		if (mod.GetLastError() !== 0)
		{
			Variable.SetValueAsString("RoutingErrorMessage", mod.GetLastErrorMessage());
			return false;
		}
		return true;
	}
	catch (error)
	{
		Variable.SetValueAsString("RoutingErrorMessage", error.message);
		return false;
	}
}

function GetDeferredDateTime(serverDate)
{
	var userDate = Sys.DD.GetDateInUserTimezone(serverDate);

	var deferredDateTime = new Date(userDate);
	deferredDateTime.setHours(8);
	deferredDateTime.setMinutes(0);
	deferredDateTime.setSeconds(0);

	if (userDate >= deferredDateTime)
	{
		deferredDateTime.setDate(userDate.getDate() + 1);
	}

	return Sys.DD.GetDateInServerTimezone(deferredDateTime);
}

function HandleModAdditionalCopies(mod, modVars)
{
	var nbAdditionalCopies = 0;
	var nbAdditionalCopiesOverride = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.GetModAdditionalCopies", Lib.DD.CreateCallbackParametersUsers(sender, recipient));
	if (typeof nbAdditionalCopiesOverride === "number" && isFinite(nbAdditionalCopiesOverride) && (nbAdditionalCopiesOverride >= 0))
	{
		nbAdditionalCopies = nbAdditionalCopiesOverride;
	}
	else
	{
		var recipientNbAdditionalModCopies = recipient.getAdvancedOption("NbAdditionalModCopies");
		if (recipientNbAdditionalModCopies)
		{
			recipientNbAdditionalModCopies = parseInt(recipientNbAdditionalModCopies, 10);
			if (!isNaN(recipientNbAdditionalModCopies))
			{
				nbAdditionalCopies = recipientNbAdditionalModCopies;
			}
		}
	}

	if (nbAdditionalCopies > 0)
	{
		// To make sure that each additional copy is on a odd page
		modVars.AddValue_String("FirstPageOnOddPage", "1", true);

		var nbAttach = Attach.GetNbAttach();
		if (nbAttach > 0)
		{
			var copyStampLanguage = recipient.exists ? recipient.language : sender.language;

			for (var i = 0; i < nbAdditionalCopies; i++)
			{
				Sys.DD.Document.AttachCurrentDocuments(mod);

				var copyIndex = 1 + (nbAttach * (i + 1));
				for (var j = 0; j < nbAttach; j++)
				{
					var copyStampPdfCmd = Sys.DD.ComputeCopyStampPdfCommand(copyIndex + j, copyStampLanguage);
					Sys.DD.AddPdfCommand(modVars, copyStampPdfCmd);
				}
			}
		}
	}
}

// Attach a welcome letter to a postal mail
//	mod -> tansport variable
//	pwd -> recipient Password
//  address -> address to stamp on the welcome letter
function AttachWelcomeLetter(mod, pwd, address)
{
	// Word template
	var docTemplate = "DD_WelcomeCustomer_%lg%.docx";
	docTemplate = docTemplate.replace("%lg%", recipient.language);

	var attachWelcomeLetter = mod.AddAttach();
	var attWLVars = attachWelcomeLetter.GetVars();
	attWLVars.AddValue_String("AttachIsResource", "1", true);
	attWLVars.AddValue_String("AttachOutputName", "WelcomeLetterTemplate", true);
	attachWelcomeLetter.SetAttachFile("%templates%\\" + docTemplate);

	// CSV file
	var attachCsv = mod.AddAttach();
	var attachCsvVars = attachCsv.GetVars();

	var portalUrl = Sys.DD.GetParameter("AliasPortalUrl__");
	if (!portalUrl)
	{
		portalUrl = recipient.portalUrl;
	}

	var content = "\"CustomerEmail\";\"CustPwd\";\"ShortURL\";\"CustID\";\"ToBlockAddress\"";
	content += "\r\n\"";
	content += recipient.emailAddress + "\";\"";
	content += pwd + "\";\"";
	content += portalUrl + "\";\"";
	content += recipient.recipientID + "\";\"";
	content += address + "\"";
	attachCsvVars.AddValue_String("AttachContent", content, true);
	attachCsvVars.AddValue_String("AttachFlyerId", "WelcomeLetter", true);
	attachCsvVars.AddValue_String("AttachConvertOptions", "?mailmerge=%[Resources(AttachOutputName=WelcomeLetterTemplate)]?CsvSep=;?Csv_Quotes=\"", true);
	attachCsvVars.AddValue_String("AttachType", "inline", true);
	attachCsvVars.AddValue_String("AttachEncoding", "UTF-8", true);
	attachCsvVars.AddValue_String("AttachOutputName", "Welcome Letter", true);
	attachCsvVars.AddValue_String("AttachInputFormat", ".txt_mailmerge", true);
	attachCsvVars.AddValue_String("AttachOutputFormat", ".pdf", true);
}

function GetAttachmentAndEml()
{
	var eml = null;
	var attachment = null;
	var nbAttach = Attach.GetNbAttach();
	if (nbAttach > 0)
	{
		for(var index = nbAttach -1; index >= 0; index--)
		{
			if (Attach.GetAttach(index).GetInputFile().GetExtension() === ".eml")
			{
				eml = Attach.GetAttach(index).GetInputFile().GetContent();
				break;
			}
		}
		if (nbAttach > 1 || (nbAttach === 1 && !eml))
		{
			attachment = Document.GetArea().toString();
		}
	}
	return {
		attachment: attachment,
		eml: eml
	};
}

function PostFeedBackGuessingType()
{
	var documentType = Data.GetValue("Document_Type__");
	if (!Sys.DD.GetParameter("Activate_Document_type_guessing") || !documentType)
	{
		return;
	}

	var documentTypeGuessing = Sys.DD.GetParameter("Document_type_guessing");
	var customClientId = Variable.GetValueAsString("customClientId");
	var documents = GetAttachmentAndEml();
	var attachId = Attach.GetAttach(0).GetVars().GetValue_String("AttachID", 0);
	var classifier = Sys.DD.GetParameter("Document_type_guessing_classifier");
	if (documentTypeGuessing === "DOCUMENT")
	{
		if (documents.attachment)
		{
			var resultLearnDocument = Sys.DocumentClassification.PostDocumentFeedBack(attachId, documents.attachment, customClientId, documentType, classifier);
			if (resultLearnDocument.status === 200)
			{
				Log.Info("PostDocumentFeedBack : " + JSON.stringify(resultLearnDocument));
			}
			else
			{
				Log.Warn("Error when calling AI : " + resultLearnDocument.lastErrorMessage);
			}
		}
		else
		{
			Log.Info("PostDocumentFeedBack : No attachment to document post feed back");
		}
	}
	else if (documentTypeGuessing === "EMAIL")
	{
		if (documents.eml || documents.attachment)
		{
			var resultLearnEmail = Sys.DocumentClassification.PostEmailFeedBack(attachId, documents.eml, customClientId, documentType, documents.attachment, classifier);
			if (resultLearnEmail.status === 200)
			{
				Log.Info("PostEmailFeedBack : " + JSON.stringify(resultLearnEmail));
			}
			else
			{
				Log.Warn("Error when calling AI : " + resultLearnEmail.lastErrorMessage);
			}
		}
		else
		{
			Log.Info("PostEmailFeedBack : No attachment to email post feed back");
		}
	}
}

Main();
