///#GLOBALS Lib Sys

// Extraction from database
var sender = {
	internalUser: null,
	ownerID: "",
	accountID: "",
	logoPath: "",
	company: "",
	country: "",
	language: "",
	culture: "",
	address: "",
	modProvider: "",
	documentUrl: "",
	environment: "",
	skin: "",
	street: "",
	city: "",
	emailAddress: "",
	zipcode: "",
	displayName: "",
	timeZoneIdentifier: "Romance Standard Time",
	phoneNumber: "",
	faxNumber: "",
	stateRegion: "",
	load: function (ownerId)
	{
		this.ownerID = ownerId;
		this.internalUser = Users.GetUserAsProcessAdmin(ownerId);
		if (!this.internalUser)
		{
			return false;
		}

		this.logoPath = this.internalUser.GetLogoPath();
		this.logoPath = this.logoPath ? "<img src=\"" + this.logoPath + "\" >" : "";

		var senderVars = this.internalUser.GetVars();

		this.accountID = senderVars.GetValue_String("AccountID", 0);
		this.company = senderVars.GetValue_String("Company", 0);
		this.displayName = senderVars.GetValue_String("DisplayName", 0);
		this.timeZoneIdentifier = senderVars.GetValue_String("TimeZoneIndex", 0);
		this.country = senderVars.GetValue_String("Country", 0);
		if (this.country)
		{
			this.country = this.country.toUpperCase();
		}
		this.language = senderVars.GetValue_String("Language", 0);
		this.culture = senderVars.GetValue_String("Culture", 0);
		this.environment = senderVars.GetValue_String("Environment", 0);
		this.skin = senderVars.GetValue_String("Skin", 0);
		this.emailAddress = senderVars.GetValue_String("EmailAddress", 0);
		this.phoneNumber = senderVars.GetValue_String("PhoneNumber", 0);

		// Postal address
		this.street = senderVars.GetValue_String("Street", 0);
		this.city = senderVars.GetValue_String("City", 0);
		this.stateRegion = senderVars.GetValue_String("MailState", 0);
		this.zipcode = senderVars.GetValue_String("ZipCode", 0);
		this.computeAddress(senderVars.GetValue_Long("CompanyFirstInAddress", 0));

		// Get MOD provider and Country code
		this.modProvider = this.loadContractInfos(senderVars);

		// Save document url
		this.documentUrl = this.internalUser.GetProcessURL(Data.GetValue("RUIDEx"));

		return true;
	},
	computeAddress: function (companyFirstInAddress)
	{
		this.address = companyFirstInAddress ? this.company + "\n" : "";
		this.address += this.street + "\n";
		this.address += this.zipcode ? this.zipcode + " " : "";
		this.address += this.city + "\n";
		this.address += this.country ? this.country + "\n" : "";
	},
	loadContractInfos: function (senderVars)
	{
		var profileId = senderVars.GetValue_String("ProfileID", 0);

		var profileVars = null;
		if (profileId)
		{
			profileVars = QueryDB("ODPROFILE", "msn=" + profileId);
		}

		if (profileVars)
		{
			var contractId = profileVars.GetValue_String("ContractID", 0);

			var contractVars = null;
			if (contractId)
			{
				contractVars = QueryDB("ODCONTRACT", "msn=" + contractId);
			}

			if (contractVars)
			{
				return contractVars.GetValue_String("ModProvider", 0);
			}
		}
		return "";
	}
};

function GetAttachmentAndEml()
{
	var eml = null;
	var attachment = null;
	var nbAttach = Attach.GetNbAttach();
	Log.Info("nbAttach: " + nbAttach);

	if (nbAttach > 0)
	{
		for(var index = nbAttach -1; index >= 0; index--)
		{
			if (Attach.GetAttach(index).GetInputFile().GetExtension() === ".eml")
			{
				eml = Attach.GetAttach(index).GetInputFile().GetContent();
				break;
			}
		}
		if (nbAttach > 1 || (nbAttach === 1 && !eml))
		{
			attachment = Document.GetArea().toString();
		}
	}

	Log.Info("eml: " + !!eml);
	Log.Info("attachment: " + !!attachment);

	return {
		attachment: attachment,
		eml: eml
	};
}

// We cannot use GetParameter here because it will cache only the global parameters into the variable
function GetClassifierFromGlobalSettings()
{
	var parameters = Sys.DD.GetGlobalSettings();
	return Sys.DD.Modifiers.Document_type_guessing_classifier(parameters.Document_type_guessing_classifier__);
}

function GetTypeGuessingDocument(documents, clientId)
{
	try
	{
		if (documents.attachment)
		{
			var docId = Attach.GetAttach(0).GetVars().GetValue_String("AttachID", 0);
			var classifier = GetClassifierFromGlobalSettings();
			var resultAttachment = Sys.DocumentClassification.ComputeDocumentClassification(docId, documents.attachment, clientId, classifier);
			if (resultAttachment.isSuccess())
			{
				return resultAttachment.getBestPrediction();
			}
			Log.Info("No prediction for attachment");
		}
		else
		{
			Log.Info("No attachment to document type predict");
		}
	}
	catch (error)
	{
		Log.Warn("GetTypeGuessingDocument: " + error);
	}
	return null;
}

function GetTypeGuessingEmail(documents, clientId)
{
	var emlPrediction = null;
	if (documents.eml)
	{
		try
		{
			var emailId = Attach.GetAttach(Attach.GetNbAttach() - 1).GetVars().GetValue_String("AttachID", 0);
			var classifier = GetClassifierFromGlobalSettings();
			var emailPrediction = Sys.DocumentClassification.ComputeEmailClassification(emailId, documents.eml, documents.attachment, clientId, classifier);
			if (emailPrediction.isSuccess())
			{
				emlPrediction = emailPrediction.getBestPrediction();
			}
			else
			{
				Log.Info("No prediction for email");
			}
		}
		catch (error)
		{
			Log.Warn("GetTypeGuessingEmail: " + error);
		}
	}
	else
	{
		Log.Info("No email to document type predict");
	}

	return emlPrediction;
}

function SetCustomClientId()
{
	var customClientId = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Common.SetClientId");
	if (customClientId === null || customClientId === "")
	{
		//Sprints prior to sprint 205 don't have SetClientId user exit in Lib_DD_Customization_Common library, but in Lib_DD_Customization_Configuration library
		//To ensure backward compatibility, try to call SetClientId in Lib_DD_Customization_Configuration if Lib_DD_Customization_Common library doesn't exist
		customClientId = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Configuration.SetClientId");
	}
	if (customClientId !== null)
	{
		Variable.SetValueAsString("customClientId", customClientId);
	}
}

function GetDocumentPrediction()
{
	if (Sys.DD.GetParameter("ConfigurationName") || !Sys.DD.GetParameter("Activate_Document_type_guessing"))
	{
		return false;
	}
	var documentTypeGuessing = Sys.DD.GetParameter("Document_type_guessing");
	Log.Info("documentTypeGuessing: " + documentTypeGuessing);
	if (documentTypeGuessing !== "DOCUMENT" && documentTypeGuessing !== "EMAIL")
	{
		return false;
	}
	Sys.DD.ClearConfigurationCache(true);

	var documents = GetAttachmentAndEml();
	var documentTypePredicted = null;

	SetCustomClientId();
	var customClientId = Variable.GetValueAsString("customClientId");

	if (documentTypeGuessing === "DOCUMENT")
	{
		documentTypePredicted = GetTypeGuessingDocument(documents, customClientId);
	}
	else if (documentTypeGuessing === "EMAIL")
	{
		documentTypePredicted = GetTypeGuessingEmail(documents, customClientId);
	}

	if (!documentTypePredicted)
	{
		Log.Info("No document type predicted");
		return false;
	}

	Variable.SetValueAsString("documentTypePredicted", documentTypePredicted);

	var filter = "(Document_Type__=" + documentTypePredicted + ")";
	var restrictDocumentFamily = Variable.GetValueAsString("SDADocumentFamily");
	if (restrictDocumentFamily)
	{
		filter = "(&" + filter + "(Family__=" + Sys.Helpers.String.EscapeValueForLdapFilter(restrictDocumentFamily) + "))";
	}

	Sys.DD.LoadConfigurationByFilter(filter);

	if (!restrictDocumentFamily || Sys.DD.GetParameter("Family__") === restrictDocumentFamily)
	{
		Data.SetValue("Document_Type__", documentTypePredicted);
		Data.SetValue("Family__", Sys.DD.GetParameter("Family__"));
	}
	if (Sys.DD.GetParameter("Document_Type__") === documentTypePredicted)
	{
		return true;
	}
	return false;
}

function ApplyAlDocumentType()
{
	var alDocumentType = Data.GetValue("Document_type__");
	if (alDocumentType)
	{
		Sys.DD.ApplyConfiguration("(Document_type__=" + alDocumentType + ")");
	}
	else
	{
		// old way to use AL to choose configuration
		var templateId = Data.GetValue("AutoLearningTemplateInstance");
		if (templateId)
		{
			Sys.DD.ApplyConfiguration("(templateidlist__=*" + templateId + "*)");
		}
	}
}

function DisableAutolearningIfEmailBody()
{
	if (Sys.DD.GetGlobalSettings() && Sys.DD.GetGlobalSettings().ActivateAutolearnForEmailBody__ === "1")
	{
		return false;
	}

	var nbAttachs = Attach.GetNbAttach();
	for (var i = 0; i < nbAttachs; i++)
	{
		var attachExtension = Attach.GetExtension(i);
		if (attachExtension.toLowerCase() !== ".eml")
		{
			return false;
		}
	}
	Data.SetValue("Disableautolearning", "true");
	Data.SetValue("Document_Type__", "");
	Data.SetValue("Family__", "");
	return true;
}

function Main()
{
	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsExtraction.OnExtractionScriptBegin");

	var restrictDocumentFamily = Variable.GetValueAsString("SDADocumentFamily");
	if (restrictDocumentFamily)
	{
		Log.Info("Forced document family (by external variable SDADocumentFamily): " + restrictDocumentFamily);
		Data.SetValue("Family__", restrictDocumentFamily);
	}

	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.InitializeSenderFormExtraction");

	if (Data.GetValue("State") === 200)
	{
		return;
	}

	if (!DisableAutolearningIfEmailBody())
	{
		ApplyAlDocumentType();
	}

	if (!GetDocumentPrediction())
	{
		var configurationName = Sys.DD.GetParameter("ConfigurationName");
		Data.SetValue("ConfigurationName__", configurationName);
		if (configurationName)
		{
			var documentType = Sys.DD.GetParameter("Document_Type");
			Data.SetValue("Document_Type__", documentType);
			Data.SetValue("Family__", Sys.DD.GetParameter("Family__"));
		}
	}
	var ident = Data.GetValue("Document_ID__");
	if (!ident)
	{
		ident = Data.GetValue("MsnEx");
		Data.SetValue("Document_ID__", ident);
	}
	Data.SetValue("Identifier", ident);

	var senderOwnerId = Data.GetValue("AlternateBillingOwner") || Data.GetValue("OwnerId");
	if (sender.load(senderOwnerId))
	{
		Variable.SetValueAsString("Account_logo__", sender.logoPath);
		Variable.SetValueAsString("Sender_ownerID__", sender.ownerID);
		Variable.SetValueAsString("Sender_accountID__", sender.accountID);
		Variable.SetValueAsString("Sender_modProvider__", sender.modProvider);
		Variable.SetValueAsString("Sender_language__", sender.language);
		Variable.SetValueAsString("Sender_culture__", sender.culture);
		Variable.SetValueAsString("Sender_timeZoneIdentifier__", sender.timeZoneIdentifier);
		Variable.SetValueAsString("Sender_environment__", sender.environment);
		Variable.SetValueAsString("Sender_skin__", sender.skin);
		Variable.SetValueAsString("Sender_phone__", sender.phoneNumber);
		Variable.SetValueAsString("Sender_document_url__", sender.documentUrl);

		SetVariableIfEmpty("Sender_displayName__", sender.displayName);
		SetVariableIfEmpty("Sender_street__", sender.street);
		SetVariableIfEmpty("Sender_zipCode__", sender.zipcode);
		SetVariableIfEmpty("Sender_city__", sender.city);
		SetVariableIfEmpty("Sender_stateRegion__", sender.stateRegion);

		SetFieldIfEmpty("Sender_company__", sender.company);
		SetFieldIfEmpty("Sender_name_", sender.name);
		SetFieldIfEmpty("Sender_address__", sender.address);
		SetFieldIfEmpty("Sender_country__", sender.country);
		SetFieldIfEmpty("Sender_phone__", sender.phoneNumber);
		SetFieldIfEmpty("Sender_fax__", sender.faxNumber);
		SetFieldIfEmpty("Sender_email__", sender.emailAddress);
	}
	else
	{
		Log.Error("Could not find sender with OwnerID: '" + senderOwnerId + "'");
	}

	var extractionType = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.GetExtractionType") ||
		Sys.DD.Extraction.GetExtractionTypeFromConfig();

	Log.Info("Extraction made using extraction type : '" + extractionType + "'");
	Sys.DD.Extraction.TryExtractDataFromDocument(extractionType);

	// The method below contains the extraction logic defined by the PS.
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.ExtractDataFromDocument", extractionType);

	Variable.SetValueAsString("DocumentWidth", Document.GetPageWidth(0));
	Variable.SetValueAsString("DocumentHeigth", Document.GetPageHeight(0));
	Variable.SetValueAsString("DocumentResX", Document.GetPageResolutionX(0));
	Variable.SetValueAsString("DocumentResY", Document.GetPageResolutionY(0));

	var recipientValidationRequested = false;
	var recipientLogin = Variable.GetValueAsString("Sender_accountID__") + "$" + Data.GetValue("Recipient_ID__");
	var recipientLoaded = Users.GetUserAsProcessAdmin(recipientLogin);

	if (recipientLoaded)
	{
		var recipientVars = recipientLoaded.GetVars();
		var recipientOptions = recipientVars.GetValue_String("Options", 0);

		if (recipientOptions)
		{
			var recipientOptionsJSON = null;
			try
			{
				recipientOptionsJSON = JSON.parse(recipientOptions);
			}
			catch (ex)
			{
				Log.Warn(ex);
			}

			if (recipientOptionsJSON)
			{
				var requestValidationOption = recipientOptionsJSON["Request_validation_for_all_invoices".toLowerCase()] || "";
				recipientValidationRequested = requestValidationOption === "1";
			}
		}

		Data.SetValue("Delivery_method__", recipientVars.GetValue_String("PreferredDeliveryMethod", 0) || Sys.DD.GetParameter("DefaultDelivery__"));
		SetFieldIfEmpty("Recipient_name__", recipientVars.GetValue_String("Company", 0));
		SetFieldIfEmpty("Recipient_country__", recipientVars.GetValue_String("Country", 0));
		SetFieldIfEmpty("Recipient_email__", recipientVars.GetValue_String("EmailAddress", 0));
		SetFieldIfEmpty("Recipient_phone__", recipientVars.GetValue_String("PhoneNumber", 0));
		SetFieldIfEmpty("Recipient_fax__", recipientVars.GetValue_String("FaxNumber", 0));
		SetFieldIfEmpty("Recipient_copyemail__", recipientVars.GetValue_String("DeliveryCopyEmail", 0));

		Variable.SetValueAsString("CustomerOptionDoNotGroupEmails", recipientVars.GetValue_Long("DoNotGroupEmails__", 0) !== 0 ? 1 : 0);
	}

	if (!recipientLoaded || Sys.DD.GetParameter("DeliveryMethodToUse__") === "DeliveryMethodToUse_DEFAULT")
	{
		Data.SetValue("Delivery_method__", Sys.DD.GetParameter("DefaultDelivery__"));
	}

	var defaultDeliveryEmail = Sys.DD.GetParameter("DefaultDeliveryEmail__");
	if (!recipientLoaded && Data.GetValue("Delivery_method__") === "SM" && Sys.DD.GetParameter("IsEmailAllowed__") === "1" && defaultDeliveryEmail)
	{
		SetFieldIfEmpty("Recipient_email__", defaultDeliveryEmail);
	}

	var forceDelivery = Variable.GetValueAsString("ForceDelivery__");
	if (forceDelivery)
	{
		Log.Info("Delivery method has been forced by external variable");
		Data.SetValue("Delivery_method__", forceDelivery);
	}
	Data.SetValue("ProcessRelatedConfiguration__", Sys.DD.GetParameter("ProcessRelatedConfiguration__"));

	ApplyValidationOptions(recipientValidationRequested);

	ApplyBillingInfos();

	if (Sys.DD.GetParameter("Routing") === "Deliver message")
	{
		var nbAttach = Attach.GetNbAttach();
		if (nbAttach)
		{
			var outputFilenamePattern = Sys.DD.GetParameter("OutputFilenamePattern");
			var attachmentName = Sys.DD.Helpers.ComputeAttachmentName(outputFilenamePattern);
			if (attachmentName)
			{
				Attach.SetValue(0, "AttachOutputName", attachmentName);
			}
		}
	}

	SetDocumentSourceInfo();

	// This callback is used by the PS to customize the process based on extracted data.
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.FinalizeSenderFormExtraction");

	ApplyTransformations();

	Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsExtraction.OnExtractionScriptEnd");
}

function ApplyValidationOptions(recipientValidationRequested)
{
	// Force validation if needed
	var bNeedValidation = false;
	var bNeedValidationUserExit = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.ForceValidation", Data.GetValue("Document_Type__"));
	if (bNeedValidationUserExit !== undefined && bNeedValidationUserExit !== null)
	{
		bNeedValidation = bNeedValidationUserExit;
	}
	else if (Sys.DD.GetParameter("ForceValidation"))
	{
		bNeedValidation = true;
	}
	else if (recipientValidationRequested)
	{
		bNeedValidation = true;
		Variable.SetValueAsString("Validation_requested_for_recipient", "1");
	}

	if (bNeedValidation)
	{
		Data.SetValue("NeedValidation", 1);
	}
	else if (Sys.DD.GetParameter("ConfigurationName"))
	{
		Data.SetValue("Enable_touchless__", true);
	}
}


function ApplyBillingInfos()
{
	var extractedBillingAccount = Data.GetValue("Billing_Info_Billing_Account__");
	if (extractedBillingAccount)
	{
		// If the billing account has been extracted from the document, we include it in the FromAccount system field
		Process.SetBillingInfo(
			{
				billingAccount: extractedBillingAccount
			}
		);
	}
	else
	{
		Data.SetValue("Billing_Info_Billing_Account__", Sys.DD.GetParameter("Billing_Info_Billing_Account"));
	}

	var extractedCostCenter = Data.GetValue("Billing_Info_Cost_Center__");
	var costCenter = extractedCostCenter || Data.GetValue("CostCenter");
	if (costCenter)
	{
		//Setting the CostCenter and the displayed Value on SUPI
		Data.SetValue("CostCenter", costCenter);
		Data.SetValue("Billing_Info_Cost_Center__", costCenter);

		//This variable is used in case of resend.
		Variable.SetValueAsString("CostCenter", costCenter);
	}
}

function ApplyTransformations()
{
	var backgrounds = {
		enabled: Sys.DD.GetParameter("Add_backgrounds"),
		first: {
			enabled: Sys.DD.GetParameter("Use_a_specific_background_for_the_first_page"),
			file: Sys.DD.GetParameter("FirstPage__"),
			options: "-jpegres"
		},
		front: {
			file: Sys.DD.GetParameter("FrontPage__"),
			options: "-jpegres"
		},
		back: {
			enabled: Sys.DD.GetParameter("Alternate_background_on_front_and_back_pages"),
			file: Sys.DD.GetParameter("BackPage__"),
			options: "-jpegres"
		}
	};
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Configuration.GetBackgrounds", backgrounds);

	var terms = {
		enabled: Sys.DD.GetParameter("Add_T_C"),
		file: Sys.DD.GetParameter("Terms___conditions_file__"),
		position: Sys.DD.GetParameter("Terms___conditions_position__")
	};
	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Configuration.GetTermsAndConditions", terms);

	// Fonction with multiple parameters to ease unit tests
	ApplyPDFCommands({
		nbPages: Document.GetPageCount(),
		backgrounds: backgrounds,
		terms: terms,
		additionalCommands: Sys.DD.GetParameter("Document_Option_PDFCommand__")
	});
}

// Search for a record in a given database table
//   - tableName [string]: database table to explore
//   - filter [string]: Ldap filter
//   - attributes [string]: comma-operated list of attributes to be retrieved from database (if null, all the available attributes are retrieved)
// Returns: null if no object matches filter, or the xVars object corresponding to the matching record
function QueryDB(tableName, filter, attributes)
{
	var query = Process.CreateQueryAsProcessAdmin();
	query.SetSpecificTable(tableName);
	query.SetFilter(filter);
	query.SetAttributesList(attributes ? attributes : "*");
	var result = null;
	if (query.MoveFirst())
	{
		var record = query.MoveNext();
		if (record)
		{
			result = record.GetUninheritedVars();
		}
	}
	return result;
}

function SetFieldIfEmpty(fieldName, fieldValue)
{
	if (Data.IsEmpty(fieldName))
	{
		Data.SetValue(fieldName, fieldValue);
	}
}

function SetVariableIfEmpty(fieldName, fieldValue)
{
	if (!Variable.GetValueAsString(fieldName))
	{
		Variable.SetValueAsString(fieldName, fieldValue);
	}
}

function CheckExtension(filename, extension)
{
	return filename.split('.').pop().toLowerCase() === extension;
}

function CheckBackgroundParameters(backgrounds, nbPages)
{
	if (!backgrounds.front.file)
	{
		Log.Warn("Background option is set but no background have been specified. No background will be added!");
		return false;
	}
	if (!backgrounds.first.file && backgrounds.first.enabled)
	{
		Log.Warn("First page background option is set but no first page background have been specified. No background will be added!");
		return false;
	}
	if (!backgrounds.back.file && backgrounds.back.enabled)
	{
		Log.Warn("Alternate page background option is set but no alternate background have been specified. No background will be added!");
		return false;
	}
	if (!CheckExtension(backgrounds.front.file, "jpg") && !CheckExtension(backgrounds.front.file, "jpeg"))
	{
		Log.Warn("First page background option is set but the file format of the main background file is not supported: " + backgrounds.front.file + ". No background will be added!");
		return false;
	}
	if (backgrounds.first.file && backgrounds.first.enabled && !CheckExtension(backgrounds.first.file, "jpg") && !CheckExtension(backgrounds.first.file, "jpeg"))
	{
		Log.Warn("Background option is set but the file format of the first background file is not supported: " + backgrounds.first.file + ". No background will be added!");
		return false;
	}
	if (backgrounds.back.file && backgrounds.back.enabled && !CheckExtension(backgrounds.back.file, "jpg") && !CheckExtension(backgrounds.back.file, "jpeg"))
	{
		Log.Warn("Alternate page background option is set but the file format of the back background file is not supported: " + backgrounds.back.file + ". No background will be added!");
		return false;
	}
	if (nbPages <= 0)
	{
		Log.Warn("Page count is invalid (" + nbPages + "). No background file will be added!");
		return false;
	}
	return true;
}

function GetBackgroundsCommand(backgrounds, nbPages, pdfCommands)
{
	if (!CheckBackgroundParameters(backgrounds, nbPages))
	{
		return;
	}

	if (!pdfCommands)
	{
		Log.Warn("Return object containing pdfCommands is null. No backgrounds will be added!");
		return;
	}

	var pageRange = "ALL";
	if (backgrounds.first.enabled)
	{
		pdfCommands.push("-insimg %infile% -tli \"" + backgrounds.first.file + "\" " + backgrounds.first.options + " 1");
		if (nbPages > 1)
		{
			// set main background start on page 2
			pageRange = "(2-END)";
		}
		else if (nbPages === 1)
		{
			return;
		}
	}
	if (backgrounds.back.enabled)
	{
		pdfCommands.push("-insimg %infile% -tli \"" + backgrounds.front.file + "\" " + backgrounds.front.options + " ODD");
		if (nbPages > 1)
		{
			pdfCommands.push("-insimg %infile% -tli \"" + backgrounds.back.file + "\" " + backgrounds.back.options + " EVEN");
		}
	}
	else
	{
		pdfCommands.push("-insimg %infile% -tli \"" + backgrounds.front.file + "\" " + backgrounds.front.options + " " + pageRange);
	}
}

function GetTermsAndConditionsCommand(terms, nbPages, pdfCommands, ignoredPages)
{
	if (!terms.file)
	{
		Log.Warn("Add terms and conditions option is set but no file have been specified. No T&C file will be added!");
		return;
	}

	if (!CheckExtension(terms.file, "pdf"))
	{
		Log.Warn("Add terms and conditions option is set but the file format is not supported: " + terms.file + ". No T&C file will be added!");
		return;
	}

	if (nbPages <= 0)
	{
		Log.Warn("Page count is invalid (" + nbPages + "). No T&C file will be added!");
		return;
	}

	if (!pdfCommands)
	{
		Log.Warn("Return object containing pdfCommands is null. No T&C file will be added!");
		return;
	}

	var termsFile = Process.GetResourceFile(terms.file);
	if (!termsFile)
	{
		Log.Warn("Add terms and conditions option is set but no file is available in resource files. No T&C file will be added!");
		return;
	}

	var nbAttach = Attach.GetNbAttach();
	var processDocIndex = -1;
	var index;
	for (index = 0; index < nbAttach && processDocIndex === -1; index++)
	{
		processDocIndex = Attach.IsProcessedDocument(index) ? index + 1 : -1;
	}
	var termsNPages = termsFile.GetNbPages();
	var cmd = "";
	switch (terms.position)
	{
		case "FIRST":
		{
			cmd = "-merge %infile[" + processDocIndex + "]% 1 \"" + terms.file + "\"";
			if (nbPages > 1)
			{
				cmd += " %infile[" + processDocIndex + "]% (2-END)";
			}

			for (index = 1; index <= termsNPages; index++)
			{
				ignoredPages.push(index + 1);
			}

			pdfCommands.push(cmd);
			break;
		}
		case "LAST":
		{
			for (index = 1; index <= termsNPages; index++)
			{
				ignoredPages.push(nbPages + index);
			}

			pdfCommands.push("-merge %infile[" + processDocIndex + "]% \"" + terms.file + "\"");
			break;
		}
		case "EACH":
		{
			cmd = "-merge";
			for (var i = 0; i < nbPages; i++)
			{
				cmd += " %infile[" + processDocIndex + "]% " + (i + 1) + " \"" + terms.file + "\"";
				for (index = 1; index <= termsNPages; index++)
				{
					ignoredPages.push(((1 + i + termsNPages) * i) + 1 + index);
				}
			}
			pdfCommands.push(cmd);
			break;
		}
		default:
			Log.Warn("Terms and condition position is invalid: '" + terms.position + "'. No T&C file will be added!");
			break;
	}
}

function ApplyPDFCommands(config)
{
	var pdfCommands = [];
	var ignoredPagesForExtraction = [];
	if (config.backgrounds.enabled)
	{
		GetBackgroundsCommand(config.backgrounds, config.nbPages, pdfCommands);
	}
	if (config.terms.enabled)
	{
		GetTermsAndConditionsCommand(config.terms, config.nbPages, pdfCommands, ignoredPagesForExtraction);
	}
	if (config.additionalCommands)
	{
		pdfCommands.push(config.additionalCommands);
	}
	if (pdfCommands.length > 0)
	{
		// Store old pdfcommands to trace them
		if (Data.GetValue("PDFCommands"))
		{
			var previousPDFCommands = Variable.GetValueAsString("PreviousPDFCommands");
			if (previousPDFCommands)
			{
				previousPDFCommands += "\r\n";
			}
			Variable.SetValueAsString("PreviousPDFCommands", previousPDFCommands + Data.GetValue("PDFCommands"));
		}

		if (!Attach.PDFCommands(pdfCommands))
		{
			Log.Error("Error with pdf command(s): '" + pdfCommands + "'.");
		}
		else
		{
			Data.SetValue("IgnoredPagesForExtraction", ignoredPagesForExtraction.join(","));
		}
	}

	return pdfCommands;
}

function SetDocumentSourceInfo()
{
	var originalJobID = Data.GetValue("OriginalJobID");
	if (originalJobID)
	{
		var filter = "(&(JobID=" + originalJobID + "))";
		var tableName = "ISM";
		var attributes = "SendType,SrcRuid,FromAddress,Subject";
		var processQuery = Process.CreateQueryAsProcessAdmin();
		processQuery.Reset();
		processQuery.SetSpecificTable(tableName);
		processQuery.SetFilter(filter);
		processQuery.SetAttributesList(attributes);
		if (processQuery.MoveFirst())
		{
			var rec = processQuery.MoveNext();
			var sourceFound = false;
			while (rec && !sourceFound)
			{
				var vars = rec.GetUninheritedVars();
				if (vars)
				{
					var sendType = vars.GetValue_String("SendType", 0);
					var srcRuid = vars.GetValue_String("SrcRuid", 0);
					if (sendType && !srcRuid)
					{
						//srcRuid null implies that it is an origin of the workflow
						sourceFound = true;
						if (sendType.indexOf("ISM") !== -1)
						{
							var emailFrom = vars.GetValue_String("FromAddress", 0);
							var emailSubject = vars.GetValue_String("Subject", 0);

							Data.SetValue("Email_From__", emailFrom);
							Data.SetValue("Email_Subject__", emailSubject);

							Log.Info("Coming from email '" + emailFrom + "' with subject: " + emailSubject);
						}
					}
				}
				if (!sourceFound)
				{
					rec = processQuery.MoveNext();
				}
			}
		}
	}
}

Main();
