Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsPostProcessing.OnPostProcessingScriptBegin");

// Synchronize the ValidattionMessage and Validation_comment__ contents
// (depending on whether the message has been entered in the form (Validation_comment__) or in a view (ValidationMessage)
var validationMessage = Data.GetValue("ValidationMessage");
var validationComment = Data.GetValue("Validation_comment__");
if (!validationComment && validationMessage)
{
	Data.SetValue("Validation_comment__", validationMessage);
}
else if (validationComment)
{
	Data.SetValue("ValidationMessage", validationComment);
}

var RUIDExNewConfiguration = Variable.GetValueAsString("RUIDExNewConfiguration");
var templateId = Data.GetValue("AutoLearningTemplateInstance");

if (templateId && templateId !== 0)
{
	if (RUIDExNewConfiguration)
	{
		var valuesToUpdate = {
			"IsAutolearningConf__": true,
			"ForceValidation__": !Data.GetValue("Enable_touchless__"),
			"MatchDocumentLayout__": true
		};
		Sys.DD.Configuration.SetValuesToConfiguration(RUIDExNewConfiguration, valuesToUpdate);
	}
	else if (Variable.GetValueAsString("ConfigurationSetUserValidation__") === "true")
	{
		var configurationName = Data.GetValue("ConfigurationName__");
		var processQuery = Process.CreateQuery();
		processQuery.SetFilter("(&(configurationname__=" + configurationName + ")(ConfigurationSelection_Enable__=1))");
		processQuery.SetSpecificTable(Sys.DD.settingTableName);
		processQuery.AddAttribute("RUIDEx");
		processQuery.AddAttribute("IsAutolearningConf__");

		processQuery.MoveFirst();
		var configRecord = processQuery.MoveNext();
		if (configRecord)
		{
			var configRuidEx = configRecord.GetUninheritedVars().GetValue_String("RUIDEx", 0);
			var isAutolearningConf = configRecord.GetUninheritedVars().GetValue_String("IsAutolearningConf__", 0);
			var matchDocumentLayout = configRecord.GetUninheritedVars().GetValue_String("MatchDocumentLayout__", 0);
			if (isAutolearningConf !== "1" || matchDocumentLayout !== "1")
			{
				Sys.DD.Configuration.SetValuesToConfiguration(configRuidEx,
					{	"IsAutolearningConf__": true,
						"MatchDocumentLayout__": true });
			}
		}
	}
}

Sys.Helpers.TryCallFunction("Lib.DD.FeatureSetsPostProcessing.OnPostProcessingScriptEnd");
