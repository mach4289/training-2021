{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 860,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 860,
						"hideDesignButton": true
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 860
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "Actions_pane"
											},
											"*": {
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1060,
													"*": {
														"Demonstration_Info": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"label": "_Demonstration Mode",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 9,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 10,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Demonstration_Info__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 11,
																			"*": {
																				"Demonstration_Info__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "Placeholder process",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 12
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1040,
													"*": {
														"Actions_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_Actions",
																"leftImageURL": "",
																"version": 0,
																"removeMargins": true,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 121,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 122,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line6__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 123,
																			"*": {
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"hidden": true,
																					"options": {
																						"height": "6",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 124
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1977,
													"*": {
														"ChooseDocument": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Choose a document",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1764,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Arrow__": "LabelArrow__",
																			"LabelArrow__": "Arrow__"
																		},
																		"version": 0
																	},
																	"stamp": 1765,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Use_the_button__Select_a_document_to_process...__": {
																						"line": 2,
																						"column": 1
																					},
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"Arrow__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelArrow__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1766,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement",
																						"version": 0
																					},
																					"stamp": 1779
																				},
																				"Use_the_button__Select_a_document_to_process...__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"image_url": "",
																						"label": "_StartProcessingLabel",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 1767
																				},
																				"LabelArrow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 1791
																				},
																				"Arrow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"css": "@ img {\npadding-left:360px;\n}@ ",
																						"version": 0
																					},
																					"stamp": 1792
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1969,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Documents",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"downloadButton": true
																	},
																	"stamp": 6
																}
															},
															"stamp": 7,
															"data": []
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 13,
													"*": {
														"Header_title_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Header_title_pane",
																"leftImageURL": "",
																"version": 0,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 14,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 15,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HeaderTitle__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 16,
																			"*": {
																				"HeaderTitle__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<div class=\"Text Title header\">\n<div class=\"title text-normal text-size-XL text-color-default text-backgroundcolor-default text-disabled\">%TITLE%<div class=\"company\"> - %COMPANY%</div></div>\n</div>\n<div class=\"topright\">\n<div class=\"image\" style=\"background-image: url('%IMG%')\">%LABEL%</div>\n</div>",
																						"css": "@ .header {\n}@ \n.title {\n    width: 75%;\n    text-overflow: ellipsis;\n    overflow: hidden;\n}@ \n.title > .company {\ndisplay: inline;\nfont-size: 18px;\n}@ \n\n.topright {\n                position: absolute;\n                top: 12px;\n                right: 0px;\n}@ \n\n.image {\n    height: 18px;\n    background-repeat: no-repeat;\n    background-position-x: left;\n    text-transform: uppercase;\n    font-weight: bold;\n    padding-left: 25px;\n    padding-right: 0px;\n    padding-bottom: 24px;\n    text-align: right;\n    font-size: 12px;\n}@ \n.small {\n    background-position-y: 1px;\n    text-align: left;\n    background-size: 14px;\n    font-weight: normal;\n    text-transform: none;\n    white-space: normal;\n    margin-top: 7px;\n    padding-bottom: 7px;\n}@ \n.notvisible {\n    display: none;\n}@ ",
																						"width": "",
																						"version": 0
																					},
																					"stamp": 17
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 18,
													"*": {
														"Being_process_pane": {
															"type": "PanelData",
															"data": [],
															"hidden": true,
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Being_process_pane",
																"leftImageURL": "being_process.png",
																"version": 0,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 19,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 20,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"BeingProcessLabel__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 21,
																			"*": {
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "11",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"BeingProcessLabel__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"label": "BeingProcessLabel__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 23
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 24
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-29": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 25,
													"*": {
														"Error_pane": {
															"type": "PanelData",
															"data": [],
															"hidden": true,
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Error_pane",
																"leftImageURL": "",
																"version": 0,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 26,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 27,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ErrorLabelReason__": {
																						"line": 1,
																						"column": 1
																					},
																					"WarningLabel__": {
																						"line": 3,
																						"column": 1
																					},
																					"Error_spacer__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 28,
																			"*": {
																				"WarningLabel__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color3",
																						"backgroundcolor": "default",
																						"label": "Placeholder for the warning message by script.",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 29
																				},
																				"ErrorLabelReason__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "color2",
																						"backgroundcolor": "default",
																						"label": "Placeholder for the error message or rejection comment.",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 30
																				},
																				"Error_spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line9",
																						"version": 0
																					},
																					"stamp": 31
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 32,
													"*": {
														"Main_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Main_pane",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": true,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 33,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Document_ID__": "LabelDocument_ID__",
																			"LabelDocument_ID__": "Document_ID__",
																			"Delivery_method__": "LabelDelivery_method__",
																			"LabelDelivery_method__": "Delivery_method__",
																			"Recipient_ID_link__": "LabelRecipient_ID_link__",
																			"LabelRecipient_ID_link__": "Recipient_ID_link__",
																			"LabelComment__": "Comment__",
																			"Comment__": "LabelComment__",
																			"SetAsideReason__": "LabelSetAsideReason__",
																			"LabelSetAsideReason__": "SetAsideReason__",
																			"SetAsideComment__": "LabelSetAsideComment__",
																			"LabelSetAsideComment__": "SetAsideComment__",
																			"Document_type__": "LabelDocument_type__",
																			"LabelDocument_type__": "Document_type__",
																			"Document_type_autolearn__": "LabelDocument_type_autolearn__",
																			"LabelDocument_type_autolearn__": "Document_type_autolearn__",
																			"LabelRecipient_address__": "Recipient_address__",
																			"Recipient_address__": "LabelRecipient_address__",
																			"LabelRecipient_ID__": "Recipient_ID__",
																			"Recipient_ID__": "LabelRecipient_ID__",
																			"LabelRecipient_email__": "Recipient_email__",
																			"Recipient_email__": "LabelRecipient_email__",
																			"LabelRecipient_fax__": "Recipient_fax__",
																			"Recipient_fax__": "LabelRecipient_fax__",
																			"ProcessRelatedConfiguration__": "LabelProcessRelatedConfiguration__",
																			"LabelProcessRelatedConfiguration__": "ProcessRelatedConfiguration__",
																			"Enable_touchless__": "LabelEnable_touchless__",
																			"LabelEnable_touchless__": "Enable_touchless__",
																			"Family__": "LabelFamily__",
																			"LabelFamily__": "Family__",
																			"Routing__": "LabelRouting__",
																			"LabelRouting__": "Routing__",
																			"Email_From__": "LabelEmail_From__",
																			"LabelEmail_From__": "Email_From__",
																			"Email_Subject__": "LabelEmail_Subject__",
																			"LabelEmail_Subject__": "Email_Subject__",
																			"FormRelatedToConversationMsnex__": "LabelFormRelatedToConversationMsnex__",
																			"LabelFormRelatedToConversationMsnex__": "FormRelatedToConversationMsnex__",
																			"CF_1711725718e_Company_code__": "LabelCF_1711725718e_Company_code__",
																			"LabelCF_1711725718e_Company_code__": "CF_1711725718e_Company_code__",
																			"CF_17117258411_Vendor_number__": "LabelCF_17117258411_Vendor_number__",
																			"LabelCF_17117258411_Vendor_number__": "CF_17117258411_Vendor_number__"
																		},
																		"version": 0
																	},
																	"stamp": 34,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Document_ID__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelDocument_ID__": {
																						"line": 5,
																						"column": 1
																					},
																					"Delivery_method__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelDelivery_method__": {
																						"line": 7,
																						"column": 1
																					},
																					"Recipient_ID_link__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelRecipient_ID_link__": {
																						"line": 13,
																						"column": 1
																					},
																					"LabelComment__": {
																						"line": 14,
																						"column": 1
																					},
																					"Comment__": {
																						"line": 14,
																						"column": 2
																					},
																					"Document_type__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelDocument_type__": {
																						"line": 6,
																						"column": 1
																					},
																					"Document_type_autolearn__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelDocument_type_autolearn__": {
																						"line": 15,
																						"column": 1
																					},
																					"LabelRecipient_address__": {
																						"line": 10,
																						"column": 1
																					},
																					"Recipient_address__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelRecipient_ID__": {
																						"line": 12,
																						"column": 1
																					},
																					"Recipient_ID__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelRecipient_email__": {
																						"line": 9,
																						"column": 1
																					},
																					"Recipient_email__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelRecipient_fax__": {
																						"line": 11,
																						"column": 1
																					},
																					"Recipient_fax__": {
																						"line": 11,
																						"column": 2
																					},
																					"ProcessRelatedConfiguration__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelProcessRelatedConfiguration__": {
																						"line": 8,
																						"column": 1
																					},
																					"Enable_touchless__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelEnable_touchless__": {
																						"line": 16,
																						"column": 1
																					},
																					"Family__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelFamily__": {
																						"line": 17,
																						"column": 1
																					},
																					"Routing__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelRouting__": {
																						"line": 18,
																						"column": 1
																					},
																					"Email_From__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEmail_From__": {
																						"line": 2,
																						"column": 1
																					},
																					"Email_Subject__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelEmail_Subject__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_Source_Document__": {
																						"line": 4,
																						"column": 1
																					},
																					"FormRelatedToConversationMsnex__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelFormRelatedToConversationMsnex__": {
																						"line": 19,
																						"column": 1
																					},
																					"SetAsideReason__": {
																						"line": 20,
																						"column": 2
																					},
																					"LabelSetAsideReason__": {
																						"line": 20,
																						"column": 1
																					},
																					"SetAsideComment__": {
																						"line": 21,
																						"column": 2
																					},
																					"LabelSetAsideComment__": {
																						"line": 21,
																						"column": 1
																					},
																					"CF_1711725718e_Company_code__": {
																						"line": 22,
																						"column": 2
																					},
																					"LabelCF_1711725718e_Company_code__": {
																						"line": 22,
																						"column": 1
																					},
																					"CF_17117258411_Vendor_number__": {
																						"line": 23,
																						"column": 2
																					},
																					"LabelCF_17117258411_Vendor_number__": {
																						"line": 23,
																						"column": 1
																					}
																				},
																				"lines": 23,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 35,
																			"*": {
																				"LabelEmail_From__": {
																					"type": "Label",
																					"data": [
																						"Email_From__"
																					],
																					"options": {
																						"label": "_Email From",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 1980
																				},
																				"Email_From__": {
																					"type": "ShortText",
																					"data": [
																						"Email_From__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Email From",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 1981
																				},
																				"LabelEmail_Subject__": {
																					"type": "Label",
																					"data": [
																						"Email_Subject__"
																					],
																					"options": {
																						"label": "_Email Subject",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 1982
																				},
																				"Email_Subject__": {
																					"type": "ShortText",
																					"data": [
																						"Email_Subject__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Email Subject",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 1983
																				},
																				"Spacer_Source_Document__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 1984
																				},
																				"LabelRouting__": {
																					"type": "Label",
																					"data": [
																						"Routing__"
																					],
																					"options": {
																						"label": "Routing",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1962
																				},
																				"Routing__": {
																					"type": "ComboBox",
																					"data": [
																						"Routing__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Deliver message",
																							"1": "Routing message"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Deliver message",
																							"1": "Routing message"
																						},
																						"label": "Routing",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1963
																				},
																				"Comment__": {
																					"type": "LongText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Comment",
																						"activable": true,
																						"width": "300",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 5,
																						"browsable": false,
																						"minNbLines": 5
																					},
																					"stamp": 1268
																				},
																				"LabelComment__": {
																					"type": "Label",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"label": "_Comment",
																						"version": 0
																					},
																					"stamp": 1267
																				},
																				"LabelSetAsideReason__": {
																					"type": "Label",
																					"data": [
																						"SetAsideReason__"
																					],
																					"options": {
																						"label": "_SetAsideReason",
																						"version": 0
																					},
																					"stamp": 2008
																				},
																				"SetAsideReason__": {
																					"type": "ComboBox",
																					"data": [
																						"SetAsideReason__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "_SetAsideReason",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 2009
																				},
																				"LabelSetAsideComment__": {
																					"type": "Label",
																					"data": [
																						"SetAsideComment__"
																					],
																					"options": {
																						"label": "_SetAsideComment",
																						"version": 0
																					},
																					"stamp": 2010
																				},
																				"SetAsideComment__": {
																					"type": "LongText",
																					"data": [
																						"SetAsideComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SetAsideComment",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"version": 0,
																						"minNbLines": 3
																					},
																					"stamp": 2011
																				},
																				"LabelDocument_ID__": {
																					"type": "Label",
																					"data": [
																						"Document_ID__"
																					],
																					"options": {
																						"label": "Document_ID__",
																						"version": 0
																					},
																					"stamp": 1161
																				},
																				"Document_ID__": {
																					"type": "ShortText",
																					"data": [
																						"Document_ID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Document_ID__",
																						"activable": true,
																						"width": 300,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1162
																				},
																				"Recipient_ID_link__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "http://www.esker.com",
																						"label": "",
																						"width": "100%",
																						"openInCurrentWindow": false,
																						"text": "_ViewRecipient"
																					},
																					"stamp": 37
																				},
																				"LabelFamily__": {
																					"type": "Label",
																					"data": [
																						"Family__"
																					],
																					"options": {
																						"label": "Family",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1960
																				},
																				"Family__": {
																					"type": "ShortText",
																					"data": [
																						"Family__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Family",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 1961
																				},
																				"LabelDocument_type__": {
																					"type": "Label",
																					"data": [
																						"Document_type__"
																					],
																					"options": {
																						"label": "Document_Type__",
																						"version": 0
																					},
																					"stamp": 1896
																				},
																				"Document_type__": {
																					"type": "ComboBox",
																					"data": [
																						"Document_type__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "Document_type__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1897
																				},
																				"LabelProcessRelatedConfiguration__": {
																					"type": "Label",
																					"data": [
																						"Select_from_a_combo_box__"
																					],
																					"options": {
																						"label": "ProcessRelatedConfiguration__",
																						"version": 0
																					},
																					"stamp": 1910
																				},
																				"ProcessRelatedConfiguration__": {
																					"type": "ComboBox",
																					"data": [
																						"ProcessRelatedConfiguration__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "ProcessRelatedConfiguration__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1911
																				},
																				"LabelRecipient_email__": {
																					"type": "Label",
																					"data": [
																						"Recipient_email__"
																					],
																					"options": {
																						"label": "Recipient_email__",
																						"version": 0
																					},
																					"stamp": 357
																				},
																				"Recipient_email__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_email__"
																					],
																					"options": {
																						"label": "Recipient_email__",
																						"activable": true,
																						"width": "300",
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"length": 255,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 358
																				},
																				"LabelRecipient_address__": {
																					"type": "Label",
																					"data": [
																						"Recipient_address__"
																					],
																					"options": {
																						"label": "Recipient_address__",
																						"version": 0
																					},
																					"stamp": 345
																				},
																				"Recipient_address__": {
																					"type": "LongText",
																					"data": [
																						"Recipient_address__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Recipient_address__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"version": 0,
																						"numberOfLines": 6,
																						"readonly": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 6
																					},
																					"stamp": 349
																				},
																				"LabelRecipient_fax__": {
																					"type": "Label",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"label": "Recipient_fax__",
																						"version": 0
																					},
																					"stamp": 355
																				},
																				"Recipient_fax__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"label": "Recipient_fax__",
																						"activable": true,
																						"width": 300,
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 356
																				},
																				"LabelRecipient_ID__": {
																					"type": "Label",
																					"data": [
																						"Recipient_ID__"
																					],
																					"options": {
																						"label": "Recipient_ID__",
																						"version": 0
																					},
																					"stamp": 351
																				},
																				"Recipient_ID__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_ID__"
																					],
																					"options": {
																						"label": "Recipient_ID__",
																						"activable": true,
																						"width": 300,
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"helpText": "Recipient_ID__Tooltip",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 352
																				},
																				"LabelDelivery_method__": {
																					"type": "Label",
																					"data": [
																						"Delivery_method__"
																					],
																					"options": {
																						"label": "Delivery_method__",
																						"version": 0
																					},
																					"stamp": 210
																				},
																				"Delivery_method__": {
																					"type": "ComboBox",
																					"data": [
																						"Delivery_method__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "_DM Mail on demand",
																							"2": "_DM Portal",
																							"3": "_DM Email",
																							"4": "_DM Fax",
																							"5": "_DM Other",
																							"6": "_DM None",
																							"7": "_DM COP",
																							"8": "_DM Conversation",
																							"9": "_DM Application process"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "EMPTY",
																							"1": "MOD",
																							"2": "PORTAL",
																							"3": "SM",
																							"4": "FGFAXOUT",
																							"5": "OTHER",
																							"6": "NONE",
																							"7": "COP",
																							"8": "CONVERSATION",
																							"9": "APPLICATION_PROCESS"
																						},
																						"label": "Delivery_method__",
																						"activable": true,
																						"width": 300,
																						"version": 1,
																						"readonly": false,
																						"readonlyIsText": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 211
																				},
																				"LabelRecipient_ID_link__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"LabelDocument_type_autolearn__": {
																					"type": "Label",
																					"data": [
																						"Document_type_autolearn__"
																					],
																					"options": {
																						"label": "Document_type_autolearn__",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1898
																				},
																				"Document_type_autolearn__": {
																					"type": "ShortText",
																					"data": [
																						"Document_type_autolearn__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Document_type_autolearn__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 1899
																				},
																				"LabelEnable_touchless__": {
																					"type": "Label",
																					"data": [
																						"Enable_touchless__"
																					],
																					"options": {
																						"label": "Enable_touchless__",
																						"version": 0
																					},
																					"stamp": 1914
																				},
																				"Enable_touchless__": {
																					"type": "CheckBox",
																					"data": [
																						"Enable_touchless__"
																					],
																					"options": {
																						"label": "Enable_touchless__",
																						"activable": true,
																						"width": 300,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 1915
																				},
																				"LabelFormRelatedToConversationMsnex__": {
																					"type": "Label",
																					"data": [
																						"FormRelatedToConversationMsnex__"
																					],
																					"options": {
																						"label": "FormRelatedToConversationMsnex",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1986
																				},
																				"FormRelatedToConversationMsnex__": {
																					"type": "ShortText",
																					"data": [
																						"FormRelatedToConversationMsnex__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "FormRelatedToConversationMsnex",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 256,
																						"hidden": false,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 1987
																				},
																				"LabelCF_1711725718e_Company_code__": {
																					"type": "Label",
																					"data": [
																						"CF_1711725718e_Company_code__"
																					],
																					"options": {
																						"label": "CF_1711725718e_Company_code__"
																					},
																					"stamp": 2012
																				},
																				"CF_1711725718e_Company_code__": {
																					"type": "ShortText",
																					"data": [
																						"CF_1711725718e_Company_code__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "CF_1711725718e_Company_code__",
																						"activable": true,
																						"width": 230,
																						"browsable": false
																					},
																					"stamp": 1965
																				},
																				"LabelCF_17117258411_Vendor_number__": {
																					"type": "Label",
																					"data": [
																						"CF_17117258411_Vendor_number__"
																					],
																					"options": {
																						"label": "CF_17117258411_Vendor_number__"
																					},
																					"stamp": 2014
																				},
																				"CF_17117258411_Vendor_number__": {
																					"type": "ShortText",
																					"data": [
																						"CF_17117258411_Vendor_number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "CF_17117258411_Vendor_number__",
																						"activable": true,
																						"width": 230,
																						"browsable": false
																					},
																					"stamp": 1967
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 151,
													"*": {
														"Generated_transports_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "Generated_transports_pane",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hidden": true,
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 152,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 153,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DeliveryTable__": {
																						"line": 1,
																						"column": 1
																					},
																					"DeliveryTable_Loader__": {
																						"line": 3,
																						"column": 1
																					},
																					"There_are_no_items_to_display__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 154,
																			"*": {
																				"DeliveryTable__": {
																					"type": "Table",
																					"data": [
																						"DeliveryTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "DeliveryTable__",
																						"readonly": true
																					},
																					"stamp": 155,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 156,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 158,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 159,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 160,
																									"data": [],
																									"*": {
																										"DeliveryTable_Type__": {
																											"type": "Label",
																											"data": [
																												"DeliveryTable_Type__"
																											],
																											"options": {
																												"label": "DeliveryTable_Type__",
																												"version": 0
																											},
																											"stamp": 161
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 162,
																									"data": [],
																									"*": {
																										"DeliveryTable_Icon__": {
																											"type": "Label",
																											"data": [
																												"DeliveryTable_Icon__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 163
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 164,
																									"data": [],
																									"*": {
																										"DeliveryTable_Details__": {
																											"type": "Label",
																											"data": [
																												"DeliveryTable_Details__"
																											],
																											"options": {
																												"label": "DeliveryTable_Details__",
																												"version": 0
																											},
																											"stamp": 165
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 166,
																									"data": [],
																									"*": {
																										"DeliveryTable_DateTime__": {
																											"type": "Label",
																											"data": [
																												"DeliveryTable_DateTime__"
																											],
																											"options": {
																												"label": "DeliveryTable_DateTime__",
																												"version": 0
																											},
																											"stamp": 167
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 168,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"DeliveryTable_Type__": {
																											"type": "LongText",
																											"data": [
																												"DeliveryTable_Type__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "DeliveryTable_Type__",
																												"activable": true,
																												"width": "180",
																												"numberOfLines": 8,
																												"browsable": false,
																												"version": 0,
																												"readonly": false,
																												"resizable": true,
																												"minNbLines": 1
																											},
																											"stamp": 170
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"DeliveryTable_Icon__": {
																											"type": "ShortText",
																											"data": [
																												"DeliveryTable_Icon__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"browsable": false,
																												"version": 0,
																												"length": 10,
																												"autocompletable": false
																											},
																											"stamp": 172
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"DeliveryTable_Details__": {
																											"type": "LongText",
																											"data": [
																												"DeliveryTable_Details__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "DeliveryTable_Details__",
																												"activable": true,
																												"width": "",
																												"numberOfLines": 4,
																												"browsable": false,
																												"version": 0,
																												"resizable": true,
																												"minNbLines": 1
																											},
																											"stamp": 174
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 175,
																									"data": [],
																									"*": {
																										"DeliveryTable_DateTime__": {
																											"type": "LongText",
																											"data": [
																												"DeliveryTable_DateTime__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "DeliveryTable_DateTime__",
																												"activable": true,
																												"width": "",
																												"numberOfLines": 4,
																												"browsable": false,
																												"version": 0,
																												"resizable": true,
																												"minNbLines": 1
																											},
																											"stamp": 176
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"There_are_no_items_to_display__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "color7",
																						"label": "There_are_no_items_to_display__",
																						"version": 0,
																						"image_url": "",
																						"iconClass": ""
																					},
																					"stamp": 177
																				},
																				"DeliveryTable_Loader__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<div class=\"centered\"><div class=\"WaitingIcon-Small\">&nbsp;</div></div>",
																						"css": "@ .centered\n{\n\ttext-align:center;\n}@ ",
																						"width": "",
																						"version": 0
																					},
																					"stamp": 178
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "20",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 179
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-30": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1880,
													"*": {
														"Toggle_button_top_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Toggle_button_top_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "center",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1802,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Button_Show_More_Less__": "LabelButton_Show_More_Less__",
																			"LabelButton_Show_More_Less__": "Button_Show_More_Less__"
																		},
																		"version": 0
																	},
																	"stamp": 1803,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Button_Show_More_Less__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelButton_Show_More_Less__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1805,
																			"*": {
																				"LabelButton_Show_More_Less__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 1890
																				},
																				"Button_Show_More_Less__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Show more details",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"action": "none",
																						"url": "",
																						"urlImage": "show_more.png",
																						"style": 4,
																						"version": 0
																					},
																					"stamp": 1891
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-27": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1753,
													"*": {
														"History_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "History_pane",
																"leftImageURL": "",
																"version": 0,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 187,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 188,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HistoryTable__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 189,
																			"*": {
																				"HistoryTable__": {
																					"type": "Table",
																					"data": [
																						"HistoryTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 2,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "HistoryTable__",
																						"readonly": true
																					},
																					"stamp": 190,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 191,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 192,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 193,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 194,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 195,
																									"data": [],
																									"*": {
																										"HistoryTable_DateTime__": {
																											"type": "Label",
																											"data": [
																												"HistoryTable_DateTime__"
																											],
																											"options": {
																												"label": "HistoryTable_DateTime__",
																												"version": 0
																											},
																											"stamp": 196
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 197,
																									"data": [],
																									"*": {
																										"HistoryTable_Details__": {
																											"type": "Label",
																											"data": [
																												"HistoryTable_Details__"
																											],
																											"options": {
																												"label": "HistoryTable_Details__",
																												"version": 0
																											},
																											"stamp": 198
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 199,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 200,
																									"data": [],
																									"*": {
																										"HistoryTable_DateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"HistoryTable_DateTime__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "HistoryTable_DateTime__",
																												"activable": true,
																												"width": "140",
																												"version": 0
																											},
																											"stamp": 201
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 202,
																									"data": [],
																									"*": {
																										"HistoryTable_Details__": {
																											"type": "LongText",
																											"data": [
																												"HistoryTable_Details__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "HistoryTable_Details__",
																												"activable": true,
																												"width": 140,
																												"numberOfLines": 8,
																												"resizable": true,
																												"browsable": false,
																												"version": 0,
																												"readonly": false,
																												"minNbLines": 1
																											},
																											"stamp": 203
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-20": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1126,
													"*": {
														"Delivery_details_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "200",
																"label": "Delivery_details_pane",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"hidden": true,
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 205,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Portal_published__": "LabelPortal_published__",
																			"LabelPortal_published__": "Portal_published__",
																			"LabelFirstViewedDateTime__": "FirstViewedDateTime__",
																			"FirstViewedDateTime__": "LabelFirstViewedDateTime__",
																			"LabelLastViewedDateTime__": "LastViewedDateTime__",
																			"LastViewedDateTime__": "LabelLastViewedDateTime__",
																			"Short_Status__": "LabelShort_Status__",
																			"LabelShort_Status__": "Short_Status__",
																			"Delivery_date__": "LabelDelivery_date__",
																			"LabelDelivery_date__": "Delivery_date__",
																			"Delivery_status__": "LabelDelivery_status__",
																			"LabelDelivery_status__": "Delivery_status__",
																			"ConfigurationName__": "LabelConfigurationName__",
																			"LabelConfigurationName__": "ConfigurationName__",
																			"Billing_Info_Billing_Account__": "LabelBilling_Info_Billing_Account__",
																			"LabelBilling_Info_Billing_Account__": "Billing_Info_Billing_Account__",
																			"Billing_Info_Cost_Center__": "LabelBilling_Info_Cost_Center__",
																			"LabelBilling_Info_Cost_Center__": "Billing_Info_Cost_Center__"
																		},
																		"version": 0
																	},
																	"stamp": 206,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Portal_published__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelPortal_published__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelFirstViewedDateTime__": {
																						"line": 6,
																						"column": 1
																					},
																					"FirstViewedDateTime__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelLastViewedDateTime__": {
																						"line": 7,
																						"column": 1
																					},
																					"LastViewedDateTime__": {
																						"line": 7,
																						"column": 2
																					},
																					"Short_Status__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelShort_Status__": {
																						"line": 3,
																						"column": 1
																					},
																					"Delivery_date__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDelivery_date__": {
																						"line": 4,
																						"column": 1
																					},
																					"Delivery_status__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDelivery_status__": {
																						"line": 2,
																						"column": 1
																					},
																					"ConfigurationName__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelConfigurationName__": {
																						"line": 1,
																						"column": 1
																					},
																					"Billing_Info_Billing_Account__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelBilling_Info_Billing_Account__": {
																						"line": 8,
																						"column": 1
																					},
																					"Billing_Info_Cost_Center__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelBilling_Info_Cost_Center__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 207,
																			"*": {
																				"LabelConfigurationName__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"label": "ConfigurationName__",
																						"version": 0
																					},
																					"stamp": 208
																				},
																				"ConfigurationName__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationName__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 209
																				},
																				"LabelDelivery_status__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Delivery_status__",
																						"version": 0
																					},
																					"stamp": 214
																				},
																				"Delivery_status__": {
																					"type": "ComboBox",
																					"data": [],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "Delivery_status__",
																						"activable": true,
																						"width": "100%",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 215
																				},
																				"LastViewedDateTime__": {
																					"type": "DateTime",
																					"data": [
																						"LastViewedDateTime__"
																					],
																					"options": {
																						"label": "LastViewedDateTime__",
																						"activable": true,
																						"width": "100%",
																						"displayLongFormat": false,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 216
																				},
																				"LabelLastViewedDateTime__": {
																					"type": "Label",
																					"data": [
																						"LastViewedDateTime__"
																					],
																					"options": {
																						"label": "LastViewedDateTime__",
																						"version": 0
																					},
																					"stamp": 217
																				},
																				"LabelShort_Status__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Short_Status__",
																						"version": 0
																					},
																					"stamp": 234
																				},
																				"Short_Status__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Short_Status__",
																						"activable": true,
																						"width": "100%",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true
																					},
																					"stamp": 235
																				},
																				"FirstViewedDateTime__": {
																					"type": "DateTime",
																					"data": [
																						"FirstViewedDateTime__"
																					],
																					"options": {
																						"label": "FirstViewedDateTime__",
																						"activable": true,
																						"width": "100%",
																						"displayLongFormat": false,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"autocompletable": false
																					},
																					"stamp": 240
																				},
																				"LabelFirstViewedDateTime__": {
																					"type": "Label",
																					"data": [
																						"FirstViewedDateTime__"
																					],
																					"options": {
																						"label": "FirstViewedDateTime__",
																						"version": 0
																					},
																					"stamp": 242
																				},
																				"LabelDelivery_date__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "Delivery_date__",
																						"version": 0
																					},
																					"stamp": 244
																				},
																				"Delivery_date__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"label": "Delivery_date__",
																						"text": "",
																						"width": "100%"
																					},
																					"stamp": 245
																				},
																				"LabelPortal_published__": {
																					"type": "Label",
																					"data": [
																						"Portal_published__"
																					],
																					"options": {
																						"label": "Portal_published__",
																						"version": 0
																					},
																					"stamp": 246
																				},
																				"Portal_published__": {
																					"type": "CheckBox",
																					"data": [
																						"Portal_published__"
																					],
																					"options": {
																						"label": "Portal_published__",
																						"activable": true,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 247
																				},
																				"LabelBilling_Info_Billing_Account__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Billing_Account__"
																					],
																					"options": {
																						"label": "Billing_Info_Billing_Account__",
																						"version": 0
																					},
																					"stamp": 1179
																				},
																				"Billing_Info_Billing_Account__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Billing_Account__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Billing_Account__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 200,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 1180
																				},
																				"LabelBilling_Info_Cost_Center__": {
																					"type": "Label",
																					"data": [
																						"Billing_Info_Cost_Center__"
																					],
																					"options": {
																						"label": "Billing_Info_Cost_Center__",
																						"version": 0
																					},
																					"stamp": 1181
																				},
																				"Billing_Info_Cost_Center__": {
																					"type": "ShortText",
																					"data": [
																						"Billing_Info_Cost_Center__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Billing_Info_Cost_Center__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 200,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 1182
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-22": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 302,
													"*": {
														"Archiving_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Archiving_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 303,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Archive_duration_preview__": "LabelArchive_duration_preview__",
																			"LabelArchive_duration_preview__": "Archive_duration_preview__",
																			"Archive_expiration_date_preview__": "LabelArchive_expiration_date_preview__",
																			"LabelArchive_expiration_date_preview__": "Archive_expiration_date_preview__"
																		},
																		"version": 0
																	},
																	"stamp": 304,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Archive_duration_preview__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelArchive_duration_preview__": {
																						"line": 1,
																						"column": 1
																					},
																					"Archive_expiration_date_preview__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelArchive_expiration_date_preview__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 305,
																			"*": {
																				"LabelArchive_duration_preview__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Archive_duration_preview__",
																						"version": 0
																					},
																					"stamp": 1163
																				},
																				"Archive_duration_preview__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Archive_duration_preview__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 1164
																				},
																				"LabelArchive_expiration_date_preview__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Archive_expiration_date_preview__",
																						"version": 0
																					},
																					"stamp": 1165
																				},
																				"Archive_expiration_date_preview__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Archive_expiration_date_preview__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 1166
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-32": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1729,
													"*": {
														"Email_grouping_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Email grouping pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1670,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Email_grouping_state__": "LabelEmail_grouping_state__",
																			"LabelEmail_grouping_state__": "Email_grouping_state__",
																			"Email_grouping_datetime__": "LabelEmail_grouping_datetime__",
																			"LabelEmail_grouping_datetime__": "Email_grouping_datetime__",
																			"Email_groupment_identifier__": "LabelEmail_groupment_identifier__",
																			"LabelEmail_groupment_identifier__": "Email_groupment_identifier__"
																		},
																		"version": 0
																	},
																	"stamp": 1671,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Email_grouping_state__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEmail_grouping_state__": {
																						"line": 1,
																						"column": 1
																					},
																					"Email_grouping_datetime__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEmail_grouping_datetime__": {
																						"line": 2,
																						"column": 1
																					},
																					"Email_groupment_identifier__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelEmail_groupment_identifier__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1672,
																			"*": {
																				"LabelEmail_grouping_state__": {
																					"type": "Label",
																					"data": [
																						"Email_grouping_state__"
																					],
																					"options": {
																						"label": "_Email grouping state",
																						"version": 0
																					},
																					"stamp": 1736
																				},
																				"Email_grouping_state__": {
																					"type": "ComboBox",
																					"data": [
																						"Email_grouping_state__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Non groupable",
																							"1": "_Groupable not grouped",
																							"2": "_Grouped",
																							"3": "_Waiting to be grouped"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "",
																							"1": "GROUPABLE_NOTGROUPED",
																							"2": "GROUPABLE_GROUPED",
																							"3": "GROUPABLE_WAITING"
																						},
																						"label": "_Email grouping state",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 1737
																				},
																				"LabelEmail_grouping_datetime__": {
																					"type": "Label",
																					"data": [
																						"Email_grouping_datetime__"
																					],
																					"options": {
																						"label": "_Email grouping datetime",
																						"version": 0
																					},
																					"stamp": 1738
																				},
																				"Email_grouping_datetime__": {
																					"type": "RealDateTime",
																					"data": [
																						"Email_grouping_datetime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_Email grouping datetime",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 1739
																				},
																				"LabelEmail_groupment_identifier__": {
																					"type": "Label",
																					"data": [
																						"Email_groupment_identifier__"
																					],
																					"options": {
																						"label": "_Email groupment identifier",
																						"version": 0
																					},
																					"stamp": 1740
																				},
																				"Email_groupment_identifier__": {
																					"type": "ShortText",
																					"data": [
																						"Email_groupment_identifier__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Email groupment identifier",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1741
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-21": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 319,
													"*": {
														"Validation_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "Validation_pane",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"hidden": true,
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 320,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Validation_comment__": "LabelValidation_comment__",
																			"LabelValidation_comment__": "Validation_comment__",
																			"Validation_user_in_charge__": "LabelValidation_user_in_charge__",
																			"LabelValidation_user_in_charge__": "Validation_user_in_charge__",
																			"Validation_user_in_charge_name__": "LabelValidation_user_in_charge_name__",
																			"LabelValidation_user_in_charge_name__": "Validation_user_in_charge_name__",
																			"Validation_status__": "LabelValidation_status__",
																			"LabelValidation_status__": "Validation_status__",
																			"Validation_user_name__": "LabelValidation_user_name__",
																			"LabelValidation_user_name__": "Validation_user_name__"
																		},
																		"version": 0
																	},
																	"stamp": 321,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Validation_comment__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelValidation_comment__": {
																						"line": 5,
																						"column": 1
																					},
																					"Validation_user_in_charge__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelValidation_user_in_charge__": {
																						"line": 1,
																						"column": 1
																					},
																					"Validation_user_in_charge_name__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelValidation_user_in_charge_name__": {
																						"line": 2,
																						"column": 1
																					},
																					"Validation_status__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelValidation_status__": {
																						"line": 3,
																						"column": 1
																					},
																					"Validation_user_name__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelValidation_user_name__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 322,
																			"*": {
																				"LabelValidation_user_in_charge__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Validation_user_in_charge__",
																						"version": 0
																					},
																					"stamp": 1167
																				},
																				"Validation_user_in_charge__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Validation_user_in_charge__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 1168
																				},
																				"LabelValidation_user_in_charge_name__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Validation_user_in_charge_name__",
																						"version": 0
																					},
																					"stamp": 1169
																				},
																				"Validation_user_in_charge_name__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Validation_user_in_charge_name__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 1170
																				},
																				"LabelValidation_status__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Validation_status__",
																						"version": 0
																					},
																					"stamp": 1171
																				},
																				"Validation_status__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Validation_status__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 1172
																				},
																				"LabelValidation_user_name__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Validation_user_name__",
																						"version": 0
																					},
																					"stamp": 1175
																				},
																				"Validation_user_name__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Validation_user_name__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 1176
																				},
																				"LabelValidation_comment__": {
																					"type": "Label",
																					"data": [
																						"Validation_comment__"
																					],
																					"options": {
																						"label": "Validation_comment__",
																						"version": 0
																					},
																					"stamp": 333
																				},
																				"Validation_comment__": {
																					"type": "LongText",
																					"data": [
																						"Validation_comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Validation_comment__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"numberOfLines": 6,
																						"resizable": true,
																						"version": 0,
																						"readonly": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1
																					},
																					"stamp": 334
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 335,
													"*": {
														"Recipient_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "Recipient_pane",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 336,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Recipient_name__": "LabelRecipient_name__",
																			"LabelRecipient_name__": "Recipient_name__",
																			"Recipient_phone__": "LabelRecipient_phone__",
																			"LabelRecipient_phone__": "Recipient_phone__",
																			"Recipient_exist__": "LabelRecipient_exist__",
																			"LabelRecipient_exist__": "Recipient_exist__",
																			"Recipient_copyemail__": "LabelRecipient_copyemail__",
																			"LabelRecipient_copyemail__": "Recipient_copyemail__",
																			"Recipient_country__": "LabelRecipient_country__",
																			"LabelRecipient_country__": "Recipient_country__",
																			"LabelRecipient_address_Common__": "Recipient_address_Common__",
																			"Recipient_address_Common__": "LabelRecipient_address_Common__",
																			"LabelRecipient_ID_Common__": "Recipient_ID_Common__",
																			"Recipient_ID_Common__": "LabelRecipient_ID_Common__",
																			"LabelRecipient_email_Common__": "Recipient_email_Common__",
																			"Recipient_email_Common__": "LabelRecipient_email_Common__",
																			"Recipient_fax_Common__": "LabelRecipient_fax_Common__",
																			"LabelRecipient_fax_Common__": "Recipient_fax_Common__"
																		},
																		"version": 0
																	},
																	"stamp": 337,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Recipient_name__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRecipient_name__": {
																						"line": 2,
																						"column": 1
																					},
																					"Recipient_phone__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelRecipient_phone__": {
																						"line": 6,
																						"column": 1
																					},
																					"Recipient_exist__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelRecipient_exist__": {
																						"line": 3,
																						"column": 1
																					},
																					"Recipient_copyemail__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelRecipient_copyemail__": {
																						"line": 9,
																						"column": 1
																					},
																					"Recipient_country__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelRecipient_country__": {
																						"line": 5,
																						"column": 1
																					},
																					"LabelRecipient_address_Common__": {
																						"line": 4,
																						"column": 1
																					},
																					"Recipient_address_Common__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRecipient_ID_Common__": {
																						"line": 1,
																						"column": 1
																					},
																					"Recipient_ID_Common__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelRecipient_email_Common__": {
																						"line": 8,
																						"column": 1
																					},
																					"Recipient_email_Common__": {
																						"line": 8,
																						"column": 2
																					},
																					"Recipient_fax_Common__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelRecipient_fax_Common__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 338,
																			"*": {
																				"LabelRecipient_ID_Common__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Recipient_ID_Common__",
																						"version": 0
																					},
																					"stamp": 1906
																				},
																				"Recipient_ID_Common__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_ID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Recipient_ID_Common__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"notInDB": true,
																						"dataSource": 1,
																						"readonly": false
																					},
																					"stamp": 1907
																				},
																				"Recipient_copyemail__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_copyemail__"
																					],
																					"options": {
																						"label": "Recipient_copyemail__",
																						"activable": true,
																						"width": "100%",
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"length": 255,
																						"browsable": false,
																						"autocompletable": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 339
																				},
																				"LabelRecipient_copyemail__": {
																					"type": "Label",
																					"data": [
																						"Recipient_copyemail__"
																					],
																					"options": {
																						"label": "Recipient_copyemail__",
																						"version": 0
																					},
																					"stamp": 340
																				},
																				"LabelRecipient_name__": {
																					"type": "Label",
																					"data": [
																						"Recipient_name__"
																					],
																					"options": {
																						"label": "Recipient_name__",
																						"version": 0
																					},
																					"stamp": 341
																				},
																				"Recipient_name__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_name__"
																					],
																					"options": {
																						"label": "Recipient_name__",
																						"activable": true,
																						"width": "100%",
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 342
																				},
																				"LabelRecipient_exist__": {
																					"type": "Label",
																					"data": [
																						"Recipient_exist__"
																					],
																					"options": {
																						"label": "Recipient_exist__",
																						"version": 0
																					},
																					"stamp": 343
																				},
																				"Recipient_exist__": {
																					"type": "CheckBox",
																					"data": [
																						"Recipient_exist__"
																					],
																					"options": {
																						"label": "Recipient_exist__",
																						"activable": true,
																						"version": 0,
																						"readonly": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 344
																				},
																				"LabelRecipient_address_Common__": {
																					"type": "Label",
																					"data": [
																						"Recipient_address_Common__"
																					],
																					"options": {
																						"label": "Recipient_address__",
																						"version": 0
																					},
																					"stamp": 1902
																				},
																				"Recipient_address_Common__": {
																					"type": "LongText",
																					"data": [
																						"Recipient_address__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 6,
																						"label": "Recipient_address__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"browsable": false,
																						"numberOfLines": 6
																					},
																					"stamp": 1903
																				},
																				"LabelRecipient_country__": {
																					"type": "Label",
																					"data": [
																						"Recipient_country__"
																					],
																					"options": {
																						"label": "Recipient_country__",
																						"version": 0
																					},
																					"stamp": 1892
																				},
																				"Recipient_country__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_country__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Recipient_country__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 100
																					},
																					"stamp": 1893
																				},
																				"LabelRecipient_fax_Common__": {
																					"type": "Label",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"label": "Recipient_fax__",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 1908
																				},
																				"Recipient_fax_Common__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_fax__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Recipient_fax__",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 1909
																				},
																				"LabelRecipient_phone__": {
																					"type": "Label",
																					"data": [
																						"Recipient_phone__"
																					],
																					"options": {
																						"label": "Recipient_phone__",
																						"version": 0
																					},
																					"stamp": 353
																				},
																				"LabelRecipient_email_Common__": {
																					"type": "Label",
																					"data": [
																						"Recipient_email_Common__"
																					],
																					"options": {
																						"label": "Recipient_email__",
																						"version": 0
																					},
																					"stamp": 1900
																				},
																				"Recipient_phone__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_phone__"
																					],
																					"options": {
																						"label": "Recipient_phone__",
																						"activable": true,
																						"width": 300,
																						"length": 30,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 354
																				},
																				"Recipient_email_Common__": {
																					"type": "ShortText",
																					"data": [
																						"Recipient_email__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Recipient_email__",
																						"activable": true,
																						"width": "300",
																						"length": 255,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"dataSource": 1,
																						"browsable": false
																					},
																					"stamp": 1901
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-39": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 973,
													"*": {
														"Sender_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "200",
																"label": "Sender_pane",
																"version": 0,
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"removeMargins": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 364,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Sender_address__": "LabelSender_address__",
																			"LabelSender_address__": "Sender_address__",
																			"Sender_phone__": "LabelSender_phone__",
																			"LabelSender_phone__": "Sender_phone__",
																			"Sender_fax__": "LabelSender_fax__",
																			"LabelSender_fax__": "Sender_fax__",
																			"Sender_email__": "LabelSender_email__",
																			"LabelSender_email__": "Sender_email__",
																			"Sender_company__": "LabelSender_company__",
																			"LabelSender_company__": "Sender_company__",
																			"Sender_name__": "LabelSender_name__",
																			"LabelSender_name__": "Sender_name__",
																			"Sender_country__": "LabelSender_country__",
																			"LabelSender_country__": "Sender_country__"
																		},
																		"version": 0
																	},
																	"stamp": 365,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Sender_address__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSender_address__": {
																						"line": 2,
																						"column": 1
																					},
																					"Sender_phone__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSender_phone__": {
																						"line": 5,
																						"column": 1
																					},
																					"Sender_fax__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelSender_fax__": {
																						"line": 6,
																						"column": 1
																					},
																					"Sender_email__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelSender_email__": {
																						"line": 7,
																						"column": 1
																					},
																					"Sender_company__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSender_company__": {
																						"line": 1,
																						"column": 1
																					},
																					"Sender_name__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSender_name__": {
																						"line": 4,
																						"column": 1
																					},
																					"Sender_country__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSender_country__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 366,
																			"*": {
																				"Sender_address__": {
																					"type": "LongText",
																					"data": [
																						"Sender_address__"
																					],
																					"options": {
																						"label": "Sender_address__",
																						"activable": true,
																						"width": "100%",
																						"version": 0,
																						"numberOfLines": 6,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"resizable": true,
																						"browsable": false,
																						"readonly": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1
																					},
																					"stamp": 367
																				},
																				"LabelSender_address__": {
																					"type": "Label",
																					"data": [
																						"Sender_address__"
																					],
																					"options": {
																						"label": "Sender_address__",
																						"version": 0
																					},
																					"stamp": 368
																				},
																				"LabelSender_company__": {
																					"type": "Label",
																					"data": [
																						"Sender_company__"
																					],
																					"options": {
																						"label": "Sender_company__",
																						"version": 0
																					},
																					"stamp": 373
																				},
																				"Sender_company__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_company__"
																					],
																					"options": {
																						"label": "Sender_company__",
																						"activable": true,
																						"width": "100%",
																						"version": 0,
																						"textAlignment": "left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"length": 100,
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": false,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 374
																				},
																				"LabelSender_country__": {
																					"type": "Label",
																					"data": [
																						"Sender_country__"
																					],
																					"options": {
																						"label": "Sender_country__",
																						"version": 0
																					},
																					"stamp": 1894
																				},
																				"Sender_country__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_country__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Sender_country__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 100
																					},
																					"stamp": 1895
																				},
																				"LabelSender_name__": {
																					"type": "Label",
																					"data": [
																						"Sender_name__"
																					],
																					"options": {
																						"label": "Sender_name__",
																						"version": 0
																					},
																					"stamp": 377
																				},
																				"Sender_name__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_name__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_name__",
																						"activable": true,
																						"width": "100%",
																						"length": 100,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 378
																				},
																				"LabelSender_phone__": {
																					"type": "Label",
																					"data": [
																						"Sender_phone__"
																					],
																					"options": {
																						"label": "Sender_phone__",
																						"version": 0
																					},
																					"stamp": 379
																				},
																				"Sender_phone__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_phone__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_phone__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 380
																				},
																				"LabelSender_fax__": {
																					"type": "Label",
																					"data": [
																						"Sender_fax__"
																					],
																					"options": {
																						"label": "Sender_fax__",
																						"version": 0
																					},
																					"stamp": 381
																				},
																				"Sender_fax__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_fax__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_fax__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 382
																				},
																				"LabelSender_email__": {
																					"type": "Label",
																					"data": [
																						"Sender_email__"
																					],
																					"options": {
																						"label": "Sender_email__",
																						"version": 0
																					},
																					"stamp": 383
																				},
																				"Sender_email__": {
																					"type": "ShortText",
																					"data": [
																						"Sender_email__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Sender_email__",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 384
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 765,
															"data": []
														}
													},
													"stamp": 766,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 767,
															"data": []
														}
													},
													"stamp": 768,
													"data": []
												},
												"form-content-left-42": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1950,
													"*": {
														"Notification_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Notification pane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 1925,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"EndOfValidity__": "LabelEndOfValidity__",
																			"LabelEndOfValidity__": "EndOfValidity__",
																			"NotificationRecipient__": "LabelNotificationRecipient__",
																			"LabelNotificationRecipient__": "NotificationRecipient__",
																			"Notification_recipient_name__": "LabelNotification_recipient_name__",
																			"LabelNotification_recipient_name__": "Notification_recipient_name__"
																		},
																		"version": 0
																	},
																	"stamp": 1926,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"EndOfValidity__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEndOfValidity__": {
																						"line": 1,
																						"column": 1
																					},
																					"NotificationRecipient__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelNotificationRecipient__": {
																						"line": 2,
																						"column": 1
																					},
																					"Notification_recipient_name__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelNotification_recipient_name__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1927,
																			"*": {
																				"LabelEndOfValidity__": {
																					"type": "Label",
																					"data": [
																						"EndOfValidity__"
																					],
																					"options": {
																						"label": "_EndOfValidity",
																						"hidden": false,
																						"version": 0
																					},
																					"stamp": 1928
																				},
																				"EndOfValidity__": {
																					"type": "DateTime",
																					"data": [
																						"EndOfValidity__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_EndOfValidity",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": false,
																						"version": 0,
																						"autocompletable": false
																					},
																					"stamp": 1929
																				},
																				"LabelNotificationRecipient__": {
																					"type": "Label",
																					"data": [
																						"NotificationRecipient__"
																					],
																					"options": {
																						"label": "_NotificationRecipient",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 1954
																				},
																				"NotificationRecipient__": {
																					"type": "ShortText",
																					"data": [
																						"NotificationRecipient__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_NotificationRecipient",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 500,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 1955
																				},
																				"LabelNotification_recipient_name__": {
																					"type": "Label",
																					"data": [
																						"Notification_recipient_name__"
																					],
																					"options": {
																						"label": "_Notification recipient name",
																						"version": 0
																					},
																					"stamp": 1958
																				},
																				"Notification_recipient_name__": {
																					"type": "ShortText",
																					"data": [
																						"Notification_recipient_name__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Notification recipient name",
																						"activable": true,
																						"width": 300,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 150,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 1959
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 1793,
													"*": {
														"Toggle_button_bottom_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Toggle_button_bottom_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "center",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 1794,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Button_Show_More_Less_bottom__": "LabelButton_Show_More_Less_bottom__",
																			"LabelButton_Show_More_Less_bottom__": "Button_Show_More_Less_bottom__"
																		},
																		"version": 0
																	},
																	"stamp": 1795,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line7__": {
																						"line": 1,
																						"column": 1
																					},
																					"Button_Show_More_Less_bottom__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelButton_Show_More_Less_bottom__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1797,
																			"*": {
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "",
																						"version": 0
																					},
																					"stamp": 1798
																				},
																				"LabelButton_Show_More_Less_bottom__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 1799
																				},
																				"Button_Show_More_Less_bottom__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Expand/Reduce",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"urlImageOverlay": "",
																						"action": "none",
																						"url": "",
																						"urlImage": "",
																						"style": 4,
																						"version": 0
																					},
																					"stamp": 1800
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 1990,
													"*": {
														"Conversation": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Conversation",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": "1px"
															},
															"stamp": 1991,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ConversationPopupButton__": "LabelConversationPopupButton__",
																			"LabelConversationPopupButton__": "ConversationPopupButton__",
																			"ConversationListUI__": "LabelConversationListUI__",
																			"LabelConversationListUI__": "ConversationListUI__"
																		},
																		"version": 0
																	},
																	"stamp": 1992,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConversationListUI__": {
																						"line": 3,
																						"column": 2
																					},
																					"NewConversationMessageWarning__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConversationPopupButton__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelConversationPopupButton__": {
																						"line": 2,
																						"column": 1
																					},
																					"LabelConversationListUI__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 1993,
																			"*": {
																				"NewConversationMessageWarning__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0,
																						"htmlContent": "<div class=\"Conversation_Warning_Text\">\n    <span id=\"setMessage\" onclick=\"setMessage(event)\">\n    </span>\n    <a class=\"Button FormButton notSelectable Button_Enabled Resized Conversation_Warning_Button\" type=\"button\">\n        <span>OK</span>\n    </a>\n</div>\n<script>\ndocument.querySelector(\".Conversation_Warning_Button\").addEventListener(\"click\",function(){\n    window.postMessage({eventName: \"onClick\", control: \"NewConversationMessageWarning__\"}, document.location.origin);\n});\n\nfunction setMessage(evt) {\n    if(evt.warningConversationMessage!= null && evt.warningConversationMessage!=\"\") {\n        document.querySelector(\".Conversation_Warning_Text > span\").textContent = evt.warningConversationMessage;\n    }\n}\n</script>\n",
																						"css": "@ .Conversation_Warning_Text\n{\n    background: #F6B221;\n    margin-bottom: 10px;\n    height: 25px;\n    display: flex;\n    align-items: center;\n    justify-content: center;\n}@ \n.Conversation_Warning_Button\n{\n    padding-left: 4px;\n    padding-right: 4px;\n    margin: 7px !important;\n    margin-bottom: 7px !important;\n    height: 20px;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n}@ \n.Conversation_Warning_Button > span\n{\n    height: 25px;\n    padding : inherit;\n    font-size: 11px;\n}@ ",
																						"width": "100%"
																					},
																					"stamp": 2002
																				},
																				"ConversationPopupButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Conversation send message",
																						"label": "",
																						"version": 0,
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "Customer order - portal",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"style": 1,
																						"width": "150",
																						"action": "none",
																						"url": ""
																					},
																					"stamp": 2004
																				},
																				"ConversationListUI__": {
																					"type": "ConversationListUI",
																					"data": false,
																					"options": {
																						"label": "",
																						"width": "100%",
																						"version": 0,
																						"tableName": "ConversationDD__",
																						"pageSize": 10,
																						"hidden": false
																					},
																					"stamp": 1989
																				},
																				"LabelConversationPopupButton__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": ""
																					},
																					"stamp": 2003
																				},
																				"LabelConversationListUI__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": ""
																					},
																					"stamp": 2005
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 840,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "50%",
													"width": "50%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"iconURL": "DD_PreviewPanel.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 841
																}
															},
															"stamp": 842,
															"data": []
														}
													},
													"stamp": 843,
													"data": []
												}
											},
											"stamp": 844,
											"data": []
										}
									},
									"stamp": 845,
									"data": []
								}
							},
							"stamp": 846,
							"data": []
						}
					},
					"stamp": 847,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 860
					},
					"*": {
						"Action_Approve": {
							"type": "SubmitButton",
							"data": false,
							"hidden": true,
							"options": {
								"label": "_Action_Approve",
								"style": 1,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"hidden": true,
								"version": 0
							},
							"stamp": 1965
						},
						"Action_Reject": {
							"type": "SubmitButton",
							"data": false,
							"hidden": true,
							"options": {
								"label": "_Action_Reject",
								"style": 3,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"hidden": true,
								"version": 0
							},
							"stamp": 1966
						},
						"Action_SetAside": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Action_SetAside",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"hidden": true,
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"style": 2
							},
							"stamp": 2007
						},
						"Action_Save": {
							"type": "SubmitButton",
							"data": false,
							"hidden": true,
							"options": {
								"label": "_Action_Save",
								"action": "save",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"hidden": true,
								"textStyle": "default",
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 856
						},
						"Action_Resubmit": {
							"type": "SubmitButton",
							"data": false,
							"hidden": true,
							"options": {
								"label": "_Action_Resubmit",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "reprocess",
								"hidden": true,
								"version": 0
							},
							"stamp": 1967
						},
						"Action_Edit": {
							"type": "SubmitButton",
							"data": false,
							"hidden": true,
							"options": {
								"label": "_Action_Edit",
								"action": "unseal",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"disabled": true,
								"textStyle": "default",
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"hidden": true
							},
							"stamp": 855
						},
						"Action_Close": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Action_Close",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "cancel",
								"version": 0
							},
							"stamp": 1964
						},
						"Action_SaveAndQuit": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Action_SaveAndQuit",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"hidden": true,
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "savequit"
							},
							"stamp": 1985
						},
						"Reply": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reply",
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "Customer order - portal",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "none",
								"hidden": true
							},
							"stamp": 2006
						}
					},
					"stamp": 857,
					"data": []
				}
			},
			"stamp": 858,
			"data": []
		}
	},
	"stamps": 2014,
	"data": []
}