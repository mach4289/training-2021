///#GLOBALS Lib Sys
var DayInMs = 86400000;
var g_PRLocalStorageIndex = Process.GetURLParameter("ls");
var g_UserCartLocalStorageUniqueCounter = "ESKCart" + Lib.Purchasing.HashLoginID(User.loginId) + "UniqueCounter";
var g_UserCartLocalStorageIndex = "ESKCart" + Lib.Purchasing.HashLoginID(User.loginId);
var g_UserToastLocalStorageIndex = "ESKToast" + Lib.Purchasing.HashLoginID(User.loginId);
var g_UserDisplayLayoutLocalStorageIndex = "ESKCartDisplayLayout" + Lib.Purchasing.HashLoginID(User.loginId);
var g_uniqueCounter = parseInt(Data.StorageGetValue(g_UserCartLocalStorageUniqueCounter) || "1", 10);
var g_companyCode = Process.GetURLParameter("companycode");
var g_filter = Process.GetURLParameter("filter");
var catalogs = Process.GetURLParameter("catalogs");
var g_catalogs = catalogs ? catalogs.split(";") : ["PurchasingOrderedItems__"];
var g_vendorCompanyExtendedPropertiesQueryCache = {};
var g_warehouses;
var timeOut = null;
var lastSearchCache;
var lastSearchCacheVendors;
var g_totalQueries = 0;
var g_totalQueriesVendors = 0;
var g_totalQueriesCategorie = 0;
var g_vendorsSelected = [];
var g_inventoryStock = [];
var categorySelected = "*";
var treeview_options = {
    GetChildren: function (parentID) {
        var children = Lib.Purchasing.CatalogHelper.SupplyTypesManager.GetChildren(parentID);
        return children;
    },
    DisplayParent: "all",
    AllLabel: "_AllPurchasingSupply",
    ChildrenFilter: null
};
function InitForm() {
    ProcessInstance.SetSilentChange(true);
    ProcessInstance.SetFormWidth(1980);
    Controls.Close.Hide(true);
    Controls.Save.Hide(true);
    Data.SetValue("CompanyCode__", g_companyCode);
    Data.SetValue("Currency__", "");
    Data.SetValue("ExchangeRate__", 1);
    Process.SetHelpId(5008);
    var promises = [];
    promises.push(Lib.P2P.UserProperties.QueryValues(User.loginId));
    promises.push(Lib.P2P.CompanyCodesValue.QueryValues(g_companyCode, true)
        .Then(function (CCValues) {
        return Sys.Helpers.Promise.Create(function (resolve) {
            if (Object.keys(CCValues).length > 0) {
                Sys.Parameters.GetInstance("PAC").Reload(CCValues.DefaultConfiguration__);
                Sys.Parameters.GetInstance("PAC").IsReady(function () {
                    Data.SetValue("Currency__", CCValues.Currency__);
                    Data.SetValue("ExchangeRate__", CCValues.currencies.GetRate(CCValues.Currency__));
                    if (Lib.P2P.Inventory.IsEnabled()) {
                        Lib.P2P.Inventory.GetWarehouse(CCValues.CompanyCode__)
                            .Then(function (warehouses) {
                            g_warehouses = warehouses;
                        })
                            .Finally(resolve);
                    }
                    else {
                        Controls.WarehouseFilter.Hide(true);
                        resolve();
                    }
                });
            }
            else {
                Log.Error("The requested company code is not in the company code table.");
                resolve();
            }
        });
    }));
    promises.push(Lib.Purchasing.CatalogHelper.SupplyTypesManager.Query()
        .Then(function () {
        Controls.VerticalTreeview__.Init(treeview_options, false);
        Controls.HorizontalTreeview__.Init(treeview_options, true);
    }));
    Sys.Helpers.Synchronizer.All(promises)
        .Then(function () {
        UpdateWarehouseList();
        SearchInCatalog(null, true, false);
        UpdateCartSummary();
    });
    Controls.Quit__.OnClick = function () {
        Process.CloseTab();
    };
}
function UpdateVendorList(searchValue) {
    var filter = GetFilter(searchValue, true);
    if (filter != lastSearchCacheVendors) {
        lastSearchCacheVendors = filter;
        var options_1 = {
            table: "PurchasingOrderedItems__",
            filter: filter,
            attributes: ["VENDORNUMBER__", "VENDORNUMBER__.NAME__", "count(*) 'NBITEMS'"],
            sortOrder: "NBITEMS DESC",
            maxRecords: "100",
            additionalOptions: "EnableJoin=1",
            groupBy: ["VendorNumber__", "VendorNumber__.Name__"]
        };
        var currentQuery_1 = ++g_totalQueriesVendors;
        var promises = g_catalogs.map(function (table) {
            options_1.table = table;
            return Sys.GenericAPI.PromisedQuery(options_1)
                .Catch(function ( /*error*/) { return null; });
        });
        Sys.Helpers.Synchronizer.All(promises)
            .Then(function (queryResults) {
            if (currentQuery_1 === g_totalQueriesVendors) {
                var vendorList = [];
                if (queryResults) {
                    vendorList = vendorList.concat.apply([], queryResults);
                }
                vendorList.sort(function (a, b) {
                    return b.NBITEMS - a.NBITEMS;
                });
                var vendorsArray_1 = [];
                var vendorsNumberArray_1 = [];
                vendorList.forEach(function (r) {
                    vendorsArray_1.push(r.VENDORNUMBER__ + "=" + r["VENDORNUMBER__.NAME__"] + " (" + r.NBITEMS + ")");
                    vendorsNumberArray_1.push(r.VENDORNUMBER__);
                });
                Controls.VendorList__.SetAvailableValues(vendorsArray_1);
                if (vendorsNumberArray_1.length === 1) {
                    Controls.VendorList__.SelectOptions(vendorsNumberArray_1);
                    Controls.VendorList__.SetReadOnly(true);
                }
                else {
                    Controls.VendorList__.SelectOptions(g_vendorsSelected);
                    Controls.VendorList__.SetReadOnly(false);
                }
                Controls.VendorFilter.Hide(vendorsNumberArray_1.length === 0);
            }
        });
    }
}
function UpdateWarehouseList() {
    var warehouseArray = [];
    for (var warehouseID in g_warehouses) {
        if (Object.prototype.hasOwnProperty.call(g_warehouses, warehouseID)) {
            warehouseArray.push(warehouseID.toString() + "=" + g_warehouses[warehouseID].Name);
        }
    }
    Controls.WarehouseList__.SetAvailableValues(warehouseArray);
    if (warehouseArray.length === 1) {
        var optionKey = warehouseArray[0].split("=")[0];
        Controls.WarehouseList__.SelectOptions(optionKey);
        Controls.WarehouseList__.SetReadOnly(true);
    }
    else if (warehouseArray.length > 1) {
        var defaultWarehouse = Lib.P2P.UserProperties.GetValues(User.loginId).DefaultWarehouse__;
        if (defaultWarehouse) {
            Controls.WarehouseList__.SelectOptions([defaultWarehouse]);
        }
        Controls.WarehouseList__.SetReadOnly(false);
    }
    Controls.WarehouseFilter.Hide(warehouseArray.length === 0);
}
function UpdateCategoriesList(searchValue, autoSelectCategorie) {
    var filter = GetFilter(searchValue, false, true);
    var options = {
        table: "PurchasingOrderedItems__",
        filter: filter,
        attributes: ["SUPPLYTYPEID__", "SUPPLYTYPEID__.NAME__"],
        sortOrder: "SUPPLYTYPEID__.NAME__ ASC",
        maxRecords: "100",
        additionalOptions: "EnableJoin=1",
        groupBy: ["SUPPLYTYPEID__", "SUPPLYTYPEID__.NAME__"]
    };
    var currentQuery = ++g_totalQueriesCategorie;
    var promises = g_catalogs.map(function (table) {
        options.table = table;
        return Sys.GenericAPI.PromisedQuery(options)
            .Catch(function ( /*error*/) { return null; });
    });
    Sys.Helpers.Synchronizer.All(promises)
        .Then(function (queryResults) {
        if (currentQuery === g_totalQueriesCategorie) {
            var categorieList_1 = {};
            if (queryResults) {
                queryResults.forEach(function (res) {
                    if (res) {
                        res.forEach(function (r) {
                            categorieList_1[r.SUPPLYTYPEID__] = r["SUPPLYTYPEID__.NAME__"];
                        });
                    }
                });
            }
            treeview_options.ChildrenFilter = Object.keys(categorieList_1);
            Controls.VerticalTreeview__.UpdateOptions(treeview_options, autoSelectCategorie, categorySelected === "*" ? null : categorySelected);
        }
    });
}
function GetFilter(searchValue, getVendors, getCategories) {
    var ldapUtil = Sys.Helpers.LdapUtil;
    var searchFilterDescription = (searchValue === null ? Controls.SearchField__.GetValue() : searchValue) || "*";
    var allFilters = [];
    if (searchFilterDescription != "*") {
        searchFilterDescription = "*" + searchFilterDescription + "*";
    }
    allFilters.push(ldapUtil.FilterOr(ldapUtil.FilterEqual("ItemDescription__", searchFilterDescription), ldapUtil.FilterEqual("ItemLongDescription__", searchFilterDescription), ldapUtil.FilterEqual("ItemNumber__", searchFilterDescription), ldapUtil.FilterEqual("VendorNumber__.Name__", searchFilterDescription)));
    if (!getCategories) {
        var supplyTypeFilter = void 0;
        var searchFilterItemCategories_1 = [];
        searchFilterItemCategories_1.push(categorySelected);
        if (searchFilterItemCategories_1[0] != "*") {
            var children = Lib.Purchasing.CatalogHelper.SupplyTypesManager.GetChildrenTree(searchFilterItemCategories_1[0]);
            Sys.Helpers.Array.ForEach(children, function (child) {
                searchFilterItemCategories_1.push(child.id);
            });
        }
        if (searchFilterItemCategories_1.length >= 1 && searchFilterItemCategories_1[0] != "*") {
            supplyTypeFilter = ldapUtil.FilterIn("SupplyTypeID__", searchFilterItemCategories_1);
        }
        else {
            supplyTypeFilter = ldapUtil.FilterEqual("SupplyTypeID__", searchFilterItemCategories_1[0]);
            supplyTypeFilter = ldapUtil.FilterOr(supplyTypeFilter, ldapUtil.FilterEqual("SupplyTypeID__", ""));
        }
        allFilters.push(supplyTypeFilter);
    }
    if (!getVendors && g_vendorsSelected.length > 0) {
        var vendorFilter = ldapUtil.FilterIn("VendorNumber__", g_vendorsSelected);
        allFilters.push(vendorFilter);
    }
    if (g_filter) {
        allFilters.push(g_filter);
    }
    else if (g_companyCode) {
        var companyCodeFilter = ldapUtil.FilterEqual("ItemCompanyCode__", g_companyCode);
        companyCodeFilter = ldapUtil.FilterOr(companyCodeFilter, ldapUtil.FilterEqual("ItemCompanyCode__", ""));
        var validityDateFilter = ldapUtil.FilterLesserOrEqual("ValidityDate__", "DATE(NOW())");
        validityDateFilter = ldapUtil.FilterOr(validityDateFilter, ldapUtil.FilterNotEqual("ValidityDate__", "*"));
        var expirationDateFilter = ldapUtil.FilterGreaterOrEqual("ExpirationDate__", "DATE(NOW())");
        expirationDateFilter = ldapUtil.FilterOr(expirationDateFilter, ldapUtil.FilterNotEqual("ExpirationDate__", "*"));
        allFilters.push(companyCodeFilter);
        allFilters.push(validityDateFilter);
        allFilters.push(expirationDateFilter);
    }
    return ldapUtil.FilterAnd.apply(ldapUtil, allFilters).toString();
}
function SearchInCatalog(searchValue, refreshVendorList, autoSelectCategorie) {
    function formatQueryResult(result) {
        var formattedResult = {
            "ruidex": result.RUIDEX,
            "productName": result.ITEMDESCRIPTION__,
            "itemType": result.ITEMTYPE__,
            "unitPrice": result.ITEMUNITPRICE__,
            "vendorName": result["VENDORNUMBER__.NAME__"],
            "vendorNumber": result.VENDORNUMBER__,
            "currency": result.ITEMCURRENCY__,
            "imgUrl": Lib.Purchasing.GetCatalogImageUrl(result.IMAGE__ || "ItemNoImage.png"),
            "punchout": !!result.PUNCHOUTSITENAME__,
            "rawData": result
        };
        if (Sys.Parameters.GetInstance("PAC").GetParameter("DisplayVendorInformationOnCard", false)) {
            formattedResult.vendorStreet = result["VENDORNUMBER__.STREET__"];
            formattedResult.vendorCity = result["VENDORNUMBER__.CITY__"];
            formattedResult.vendorPostalCode = result["VENDORNUMBER__.POSTALCODE__"];
            formattedResult.vendorRegion = result["VENDORNUMBER__.REGION__"];
            formattedResult.vendorCountry = Sys.Locale.Country.GetCountryName(result["VENDORNUMBER__.COUNTRY__"]);
        }
        if (Sys.Parameters.GetInstance("PAC").GetParameter("EnableItemRating", false)) {
            formattedResult.grade = result.GRADE__;
            formattedResult.gradeNumber = result.GRADE_NUMBER__;
            formattedResult.itemTags = result.ITEMTAGS__;
        }
        return formattedResult;
    }
    function GetItemCardCompanyStructureAddon(itemsCards) {
        var itemsCardsAddon = [];
        itemsCards.forEach(function (item) {
            if (g_vendorCompanyExtendedPropertiesQueryCache[item.vendorNumber]) {
                itemsCardsAddon.push({
                    ruidex: item.ruidex,
                    companyStructure: g_vendorCompanyExtendedPropertiesQueryCache[item.vendorNumber]
                });
            }
        });
        return itemsCardsAddon;
    }
    function CompleteVendorCompanyExtendedPropertiesCache(itemsCards) {
        var _a;
        var vendorLdapArray = [];
        var vendorToRequestArray = [];
        itemsCards.forEach(function (item) {
            if (!g_vendorCompanyExtendedPropertiesQueryCache[item.vendorNumber] && vendorToRequestArray.indexOf(item.vendorNumber) === -1) {
                vendorToRequestArray.push(item.vendorNumber);
                vendorLdapArray.push(Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", item.vendorNumber));
            }
        });
        var table = "Vendor_company_extended_properties__";
        var requestFilter = Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("CompanyCode__", g_companyCode), (_a = Sys.Helpers.LdapUtil).FilterOr.apply(_a, vendorLdapArray));
        var fields = ["VendorNumber__", "CompanyCode__", "CompanyStructure__"];
        var options = {
            table: table,
            filter: requestFilter.toString(),
            attributes: fields,
            maxRecords: 100
        };
        return Sys.GenericAPI.PromisedQuery(options)
            .Then(function (queryResults) {
            if (queryResults.length > 0) {
                queryResults.forEach(function (res) {
                    if (res) {
                        g_vendorCompanyExtendedPropertiesQueryCache[res.VendorNumber__] = res.CompanyStructure__;
                    }
                });
            }
            return GetItemCardCompanyStructureAddon(itemsCards);
        });
    }
    clearTimeout(timeOut);
    var filter = GetFilter(searchValue);
    if (filter != lastSearchCache) {
        lastSearchCache = filter;
        var maxRecords = 30;
        var options_2 = {
            table: "PurchasingOrderedItems__",
            filter: filter,
            attributes: Lib.Purchasing.CatalogHelper.CatalogItem.Attributes,
            sortOrder: "ITEMDESCRIPTION__ ASC",
            maxRecords: maxRecords,
            additionalOptions: "EnableJoin=1"
        };
        Controls.ProductCardsList__.ShowLoadingGifs();
        var currentQuery_2 = ++g_totalQueries;
        var promises = g_catalogs.map(function (table) {
            options_2.table = table;
            return Sys.GenericAPI.PromisedQuery(options_2)
                .Catch(function ( /*error*/) { return null; });
        });
        promises.unshift(Sys.Parameters.IsReady());
        Sys.Helpers.Synchronizer.All(promises)
            .Then(function (queryResults) {
            if (currentQuery_2 === g_totalQueries) {
                var punchout2_1 = Sys.Parameters.GetInstance("P2P").GetParameter("EnablePunchoutV2") === "1";
                var itemsCards_1 = [];
                var inventoriesFilter_1 = [];
                if (queryResults) {
                    queryResults.forEach(function (res) {
                        if (res) {
                            res.forEach(function (r) {
                                if (punchout2_1 || !r.PUNCHOUTSITENAME__) {
                                    itemsCards_1.push(formatQueryResult(r));
                                    //Build Itemruidex => Itemnumber
                                    if (r.ITEMNUMBER__) {
                                        for (var warehouseID in g_warehouses) {
                                            if (Object.prototype.hasOwnProperty.call(g_warehouses, warehouseID)) {
                                                inventoriesFilter_1.push({ itemNumber: r.ITEMNUMBER__, warehouseID: warehouseID, companyCode: g_companyCode });
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
                Controls.ProductCardsList__.SetCardsData(itemsCards_1, Data.GetValue("Currency__"));
                if (Sys.Parameters.GetInstance("PAC").GetParameter("DisplayVendorInformationOnCard", false)) {
                    CompleteVendorCompanyExtendedPropertiesCache(itemsCards_1).Then(function (_itemsCardsAddon) {
                        Controls.ProductCardsList__.UpdateCardsData(_itemsCardsAddon, Data.GetValue("Currency__"));
                    });
                }
                if (Lib.P2P.Inventory.IsEnabled()) {
                    Lib.P2P.Inventory.GetInventories(inventoriesFilter_1, true)
                        .Then(function (inventoryStock) {
                        g_inventoryStock = inventoryStock;
                        UpdateStockFromWarehouseList();
                    });
                }
            }
        });
        if (refreshVendorList) {
            UpdateVendorList(searchValue);
        }
        UpdateCategoriesList(searchValue, autoSelectCategorie);
    }
}
function InitCartSummary() {
    Controls.CartSummary__.SetWidth("100%");
    Controls.CartSummary__.SetExtendableColumn("ItemDescription__");
    Controls.CartSummary__.OnDeleteItem = function (item /*, tableIndex: number*/) {
        var currentUserCart = JSON.parse(Data.StorageGetValue(g_UserCartLocalStorageIndex)) || {};
        delete currentUserCart[item.GetValue("CartItemNumber__")];
        Data.StorageSetValue(g_UserCartLocalStorageIndex, JSON.stringify(currentUserCart));
        setTimeout(function () {
            UpdateCartSummary();
        }, 30);
    };
    Controls.CartSummary__.CartItemQuantity__.OnChange = function () {
        var currentRow = this.GetRow();
        var keyedQuantity = currentRow.CartItemQuantity__.GetValue();
        var currentUserCart = JSON.parse(Data.StorageGetValue(g_UserCartLocalStorageIndex)) || {};
        var currentCartItem = currentUserCart[currentRow.CartItemNumber__.GetValue()];
        if (currentCartItem) {
            if (keyedQuantity == 0) {
                delete currentUserCart[currentRow.CartItemNumber__.GetValue()];
                currentRow.GetItem().Remove();
            }
            else if (keyedQuantity < 0) {
                currentCartItem.ITEMQUANTITY__ = 1;
            }
            else {
                currentCartItem.ITEMQUANTITY__ = keyedQuantity;
            }
            Data.StorageSetValue(g_UserCartLocalStorageIndex, JSON.stringify(currentUserCart));
        }
    };
    Controls.CartSummary__.OnBlurRow = function (index) {
        var ruidex = Controls.CartSummary__.GetRow(index).CartItemNumber__.GetValue();
        if (ruidex) {
            Controls.ProductCardsList__.SelectCard(ruidex, false);
        }
    };
    Controls.CartSummary__.OnFocusRow = function (index) {
        var ruidex = Controls.CartSummary__.GetRow(index).CartItemNumber__.GetValue();
        if (ruidex) {
            Controls.ProductCardsList__.SelectCard(ruidex, true);
        }
    };
    Controls.SendToPurchaseRequisition__.OnClick = function () {
        function CartSuccessfullySent() {
            // Success
            Data.StorageSetValue(g_UserCartLocalStorageIndex, "{}");
            Data.StorageSetValue(g_UserCartLocalStorageUniqueCounter, 1);
            Process.CloseTab();
        }
        var currentUserCart = JSON.parse(Data.StorageGetValue(g_UserCartLocalStorageIndex)) || {};
        var itemIDs = Object.keys(currentUserCart);
        var items = [];
        itemIDs.forEach(function (itemID) {
            items.push(currentUserCart[itemID]);
        });
        var statusCode = Data.StorageSetValue(g_PRLocalStorageIndex, JSON.stringify(items));
        if (statusCode === 0) {
            CartSuccessfullySent();
        }
        else if (statusCode === -1) {
            Data.CleanLocalStorage("cart([0-9])*", true, function (key) {
                var timestamp = new Date().getTime();
                // Change me if you change the pattern :)
                var existenceTime = timestamp - parseInt(key.substring(4), 10);
                // Clean only if the key is in the local storage for more than 1 day
                if (existenceTime / DayInMs > 1) {
                    return true;
                }
                return false;
            });
            // Try again after cleaning the storage
            statusCode = Data.StorageSetValue(g_PRLocalStorageIndex, JSON.stringify(items));
            if (statusCode === -1) {
                // Cleaning not sufficient
                Popup.Alert("_LocalStorage is full", true, null, "_Error localStoage full");
            }
            else {
                CartSuccessfullySent();
            }
        }
        else if (statusCode === -2) {
            // LocalStorage is not supported
            Popup.Alert("_LocalStorage not supported by your browser", true, null, "_Error localStoage probably not supported");
        }
    };
}
function UpdateCartSummary() {
    var currentUserCart = JSON.parse(Data.StorageGetValue(g_UserCartLocalStorageIndex)) || {};
    var table = Controls.CartSummary__;
    var itemIDs = Object.keys(currentUserCart);
    var items = [];
    itemIDs.forEach(function (itemID) {
        items.push(currentUserCart[itemID]);
    });
    table.SetItemCount(0);
    var lineCount = table.GetItemCount();
    var rowItem;
    var totalQuantity = 0;
    var totalPrice = 0;
    function SetRowStyle(index, lastLine, amountBased) {
        if (amountBased === void 0) { amountBased = false; }
        setTimeout(function () {
            table.HideTableRowDeleteForItem(index, lastLine);
            var row = table.GetRow(index);
            row.CartItemQuantity__.Hide(amountBased);
            row.CartItemQuantity__.SetReadOnly(lastLine || amountBased);
            if (lastLine) {
                row.AddStyle("highlight-grayed-2");
            }
            else {
                row.RemoveStyle("highlight-grayed-2");
            }
        }, 30);
    }
    table.HideTableRowDelete(items.length === 0);
    var i = 0;
    for (i; i < items.length; i++) {
        if (i >= lineCount) {
            rowItem = table.AddItem();
        }
        else {
            rowItem = table.GetRow(i).GetItem();
        }
        SetRowStyle(i, false, items[i].ITEMTYPE__ === Lib.P2P.ItemType.AMOUNT_BASED);
        rowItem.SetValue("CartItemNumber__", items[i].UNIQUEITEMID);
        rowItem.SetValue("CartItemDescription__", items[i].ITEMDESCRIPTION__);
        rowItem.SetValue("CartItemQuantity__", items[i].ITEMQUANTITY__);
        var itemTotalPrice = items[i].ITEMUNITPRICE__ ? Language.FormatNumber(items[i].ITEMQUANTITY__ * items[i].ITEMUNITPRICE__, false) + " " + (items[i].ITEMCURRENCY__ || Data.GetValue("Currency__") || "") : Language.Translate("_CardItem_EmptyValue");
        rowItem.SetValue("CartItemPrice__", itemTotalPrice);
        totalQuantity += items[i].ITEMQUANTITY__;
        var itemExchangeRate = Lib.P2P.CompanyCodesValue.GetValues(g_companyCode).currencies.GetRate(items[i].ITEMCURRENCY__);
        totalPrice += items[i].ITEMQUANTITY__ * items[i].ITEMUNITPRICE__ * (itemExchangeRate || 1);
    }
    if (i >= lineCount) {
        rowItem = table.AddItem();
    }
    else {
        rowItem = table.GetRow(i).GetItem();
    }
    SetRowStyle(i, true);
    rowItem.SetValue("CartItemDescription__", Language.Translate("_CartSummary:Total"));
    rowItem.SetValue("CartItemQuantity__", totalQuantity);
    var totalPriceFormatted = Language.FormatNumber(totalPrice, false) + " " + (Data.GetValue("Currency__") || "");
    rowItem.SetValue("CartItemPrice__", totalPriceFormatted);
    Controls.TotalPrice__.SetValue(totalPriceFormatted);
    if (totalQuantity > 1) {
        Controls.TotalDescription__.SetValue(Language.Translate("_SummaryPane:Total {0} articles", true, totalQuantity));
    }
    else {
        Controls.TotalDescription__.SetValue(Language.Translate("_SummaryPane:Total {0} article", true, totalQuantity));
    }
    Controls.SendToPurchaseRequisition__.SetDisabled(items.length === 0);
}
function UpdateStockFromWarehouseList() {
    var itemsMap = {};
    var selectedWarehouse = Controls.WarehouseList__.GetSelectedOptions();
    g_inventoryStock.forEach(function (item) {
        if (!itemsMap[item.itemRuidex]) {
            itemsMap[item.itemRuidex] = {
                ruidex: item.itemRuidex,
                stock: 0,
                itemNumber: item.itemNumber
            };
        }
        if (selectedWarehouse.length === 0 || selectedWarehouse.indexOf(item.warehouseID) > -1) {
            itemsMap[item.itemRuidex].stock += parseFloat(item.stock);
        }
    });
    var values = Object.keys(itemsMap).map(function (key) { return itemsMap[key]; });
    Controls.ProductCardsList__.UpdateCardsData(values, Data.GetValue("Currency__"));
}
Data.OnStorageChange(g_UserCartLocalStorageIndex, function (storageValue) {
    if (storageValue.newValue) {
        UpdateCartSummary();
    }
});
Data.OnStorageChange(g_UserToastLocalStorageIndex, function (storageValue) {
    if (storageValue.newValue) {
        Popup.Snackbar(JSON.parse(storageValue.newValue));
        Data.StorageSetValue(g_UserToastLocalStorageIndex, "");
    }
});
function InitSearchPane() {
    Controls.SearchField__.SetPlaceholder(Language.Translate("_Search"));
    Controls.SearchField__.OnEnter = function () {
        SearchInCatalog(null, true, true);
    };
    Controls.SearchField__.OnTextChange = function (newSearch) {
        clearTimeout(timeOut);
        timeOut = setTimeout(function () {
            SearchInCatalog(newSearch, true, true);
        }, 300);
    };
    Controls.SearchButton__.OnClick = function () {
        SearchInCatalog(null, false, false);
    };
    Controls.VerticalTreeview__.OnSelectItem = function (id /*, value: string*/) {
        categorySelected = id ? id : "*";
        Controls.HorizontalTreeview__.SetValue(id);
        SearchInCatalog(null, true, false);
    };
    Controls.HorizontalTreeview__.OnSelectItem = function (id /*, value: string*/) {
        categorySelected = id ? id : "*";
        Controls.VerticalTreeview__.SetValue(id);
        SearchInCatalog(null, true, false);
    };
    Controls.VendorList__.OnChange = function (selectedVendors) {
        g_vendorsSelected = selectedVendors;
        SearchInCatalog(null, false, true);
    };
    Controls.WarehouseList__.OnChange = function ( /*selectedWarehouse: string[]*/) {
        UpdateStockFromWarehouseList();
    };
}
Controls.ProductCardsList__.OnItemsAddedToCart = function (quantity, item) {
    var itemID = item.ITEMTYPE__ == Lib.P2P.ItemType.AMOUNT_BASED ? item.RUIDEX + ++g_uniqueCounter : item.RUIDEX;
    Data.StorageSetValue(g_UserCartLocalStorageUniqueCounter, g_uniqueCounter);
    var currentUserCart = JSON.parse(Data.StorageGetValue(g_UserCartLocalStorageIndex)) || {};
    if (currentUserCart[itemID]) {
        currentUserCart[itemID].ITEMQUANTITY__ += quantity;
    }
    else {
        item.UNIQUEITEMID = itemID;
        item.ITEMQUANTITY__ = quantity;
        currentUserCart[itemID] = item;
    }
    Data.StorageSetValue(g_UserCartLocalStorageIndex, JSON.stringify(currentUserCart));
};
Controls.ProductCardsList__.OnClickCart = function (item, quantity) {
    if (item) {
        if (item.PUNCHOUTSITENAME__) {
            Lib.Purchasing.Punchout.LoadConfigs()
                .Then(function (configs) {
                var config = Sys.Helpers.Array.Find(configs, function (config) {
                    return config.ConfigurationName__ === item.PUNCHOUTSITENAME__;
                });
                if (config) {
                    var items = [
                        {
                            punchoutConfig: config,
                            selectedItem: item
                        }
                    ];
                    Data.StorageSetValue(g_PRLocalStorageIndex, JSON.stringify(items));
                    // Keep selected local items in storage ?
                    // Data.StorageSetValue(g_UserCartLocalStorageIndex, "{}");
                    // Data.StorageSetValue(g_UserCartLocalStorageUniqueCounter, 1);
                    Process.CloseTab();
                }
            })
                .Catch(function (error) {
            });
        }
        else if (item.RUIDEX) {
            Process.OpenLink({
                "url": "FlexibleForm.aspx?action=run&layout=_flexibleform&Id=" + encodeURIComponent(item.RUIDEX) + "&fc=" + quantity + "&OnQuit=Close",
                "inCurrentTab": false
            });
        }
    }
};
Controls.SwitchLayout__.BindEvent("OnSwitchButton", function (evt) {
    Controls.ProductCardsList__.SetLayout(evt.args);
    Data.StorageSetValue(g_UserDisplayLayoutLocalStorageIndex, evt.args);
});
Controls.SwitchLayout__.BindEvent("OnLoad", function () {
    var defaultLayout = Data.StorageGetValue(g_UserDisplayLayoutLocalStorageIndex) || Sys.Parameters.GetInstance("PAC").GetParameter("DefaultCatalogLayout", "Lines");
    Controls.SwitchLayout__.FireEvent("onLoad", { layout: defaultLayout });
    Controls.ProductCardsList__.SetLayout(defaultLayout);
});
InitForm();
InitSearchPane();
InitCartSummary();
