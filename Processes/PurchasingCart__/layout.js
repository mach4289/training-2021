{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "complex-custom",
				"splitterOptions": {
					"sizeable": false
				},
				"version": 0,
				"maxwidth": 1980,
				"customSplitter": {
					"cssClass": "form-content",
					"splitter": {
						"knownPosition": true,
						"direction": "v",
						"columns": [
							{
								"id": "form-content-left",
								"left": "0px",
								"top": "0px",
								"bottom": "0px",
								"width": "calc(100% - 450px)",
								"unit": "",
								"direction": "h",
								"sizable": false,
								"fixedSlider": true,
								"columns": [
									{
										"id": "form-content-left-top",
										"droppable": true,
										"height": 103,
										"unit": "px",
										"scroll": {
											"y": false
										},
										"ctrlAlign": "v",
										"sizable": false
									},
									{
										"id": "form-content-left-bottom",
										"height": 900,
										"unit": "px",
										"left": "0px",
										"top": "0px",
										"bottom": "0px",
										"direction": "v",
										"sizable": false,
										"fixedSlider": true,
										"columns": [
											{
												"id": "form-content-left-bottom-left",
												"droppable": true,
												"width": 210,
												"unit": "px",
												"scroll": {
													"perfectY": true
												},
												"ctrlAlign": "v",
												"sizable": false
											},
											{
												"id": "form-content-left-bottom-right",
												"droppable": true,
												"width": 900,
												"unit": "px",
												"scroll": {
													"perfectY": true
												},
												"ctrlAlign": "v",
												"sizable": false
											}
										]
									}
								]
							},
							{
								"id": "form-content-right",
								"left": "calc(100% - 450px)",
								"top": "0px",
								"bottom": "0px",
								"width": "450px",
								"unit": "",
								"direction": "h",
								"sizable": false,
								"fixedSlider": true,
								"columns": [
									{
										"id": "form-content-right-top",
										"droppable": true,
										"height": 205,
										"unit": "px",
										"scroll": {
											"y": true
										},
										"ctrlAlign": "v",
										"sizable": false
									},
									{
										"id": "form-content-right-bottom",
										"droppable": true,
										"height": 900,
										"unit": "px",
										"scroll": {
											"perfectY": true
										},
										"ctrlAlign": "v",
										"sizable": false
									}
								]
							}
						]
					}
				},
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"version": 0,
						"maxwidth": 1980
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1980
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-left": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0px",
											"left": "0px",
											"width": "calc(100% - 450px)"
										},
										"hidden": false
									},
									"*": {
										"form-content-left-top": {
											"type": "FlexibleFormSplitter",
											"options": {
												"autosize": true,
												"version": 1,
												"box": {
													"top": "0px",
													"left": "0px",
													"width": null,
													"height": "70px"
												},
												"hidden": false
											},
											"*": {
												"form-content-left-top-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 2,
													"*": {
														"SearchPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "none",
																"labelsAlignment": "right",
																"label": "_SearchPane",
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "left",
																"version": 0,
																"panelStyle": "FlexibleFormPanelLight",
																"sameHeightAsSiblings": false
															},
															"stamp": 3,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 4,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"displayAsFlex": true,
																				"ctrlsPos": {
																					"SearchField__": {
																						"line": 1,
																						"column": 1
																					},
																					"SearchButton__": {
																						"line": 2,
																						"column": 1
																					},
																					"SpacerTop2__": {
																						"line": 3,
																						"column": 1
																					},
																					"SpacerTop1__": {
																						"line": 4,
																						"column": 1
																					},
																					"HorizontalTreeview__": {
																						"line": 5,
																						"column": 1
																					},
																					"SwitchLayout__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						[
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							]
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0,
																				"style": "flex"
																			},
																			"stamp": 5,
																			"*": {
																				"SpacerTop1__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "_SpacerTop1",
																						"version": 0
																					},
																					"stamp": 6
																				},
																				"SearchField__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "MS",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SearchField",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false
																					},
																					"stamp": 8
																				},
																				"SpacerTop2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "7",
																						"width": "",
																						"color": "default",
																						"label": "_SpacerTop2",
																						"version": 0
																					},
																					"stamp": 9
																				},
																				"SearchButton__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Search",
																						"label": "_Search",
																						"textPosition": "text-tooltip",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "PurchasingCart__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "25px",
																						"action": "none",
																						"url": "",
																						"style": 5,
																						"version": 0,
																						"awesomeClasses": "fa fa-search fa-2x"
																					},
																					"stamp": 10
																				},
																				"HorizontalTreeview__": {
																					"type": "Treeview",
																					"data": false,
																					"options": {
																						"version": 1,
																						"label": "_HorizontalTreeview",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"horizontal": true
																					},
																					"stamp": 158
																				},
																				"SwitchLayout__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "View",
																						"htmlContent": "<div class=\"layout-button\" onclick=\"Slide()\">\n\t<input id=\"onLoad\" type=\"hidden\" style=\"display:none;\" onclick=\"SetStyle(event);\">\n\t<div id=\"SwitchLayoutButtonSlider\" class=\"slider text-backgroundcolor-color1\"></div>\n\t<div class=\"button-lines\">\n\t\t<div class=\"line-container\">\n\t\t\t<div class=\"thumbnail\"></div>\n\t\t\t<div class=line></div>\n\t\t</div>\n\t\t<div class=\"line-container\">\n\t\t\t<div class=\"thumbnail\"></div>\n\t\t\t<div class=line></div>\n\t\t</div>\n\t\t<div class=\"line-container\">\n\t\t\t<div class=\"thumbnail\"></div>\n\t\t\t<div class=line></div>\n\t\t</div>\n\t</div>\n\t<div class=\"button-grid\">\n\t\t<div class=\"card\"></div>\n\t\t<div class=\"card\"></div>\n\t\t<div class=\"card\"></div>\n\t\t<div class=\"card\"></div>\n\t\t<div class=\"card\"></div>\n\t\t<div class=\"card\"></div>\n\t</div>\n</div>\n<script type=\"text/javascript\">\n\twindow.postMessage({ eventName: \"OnLoad\", control: \"SwitchLayout__\" }, document.location.origin);\n\tvar switchLayoutButtonSlider = document.getElementById(\"SwitchLayoutButtonSlider\");\n\n\tfunction SetStyle(evt)\n\t{\n\t\tvar style = evt.layout;\n\t\tif(style == \"Lines\")\n\t\t{\n\t\t\tif (switchLayoutButtonSlider.classList.contains(\"left\"))\n\t\t\t{\n\t\t\t\tswitchLayoutButtonSlider.classList.remove(\"left\");\n\t\t\t}\n\t\t}\n\t\telse\n\t\t{\n\t\t\tif (!switchLayoutButtonSlider.classList.contains(\"left\"))\n\t\t\t{\n\t\t\t\tswitchLayoutButtonSlider.classList.add(\"left\");\n\t\t\t}\n\t\t}\n\t}\n\n\tfunction Slide()\n\t{\n\t\tif (switchLayoutButtonSlider.classList.contains(\"left\"))\n\t\t{\n\t\t\tswitchLayoutButtonSlider.classList.remove(\"left\");\n\t\t\twindow.postMessage({ eventName: \"OnSwitchButton\", control: \"SwitchLayout__\", args: \"Lines\" }, document.location.origin);\n\t\t}\n\t\telse\n\t\t{\n\t\t\tswitchLayoutButtonSlider.classList.add(\"left\");\n\t\t\twindow.postMessage({ eventName: \"OnSwitchButton\", control: \"SwitchLayout__\", args: \"Cards\" }, document.location.origin);\n\t\t}\n\t}\n</script>",
																						"css": ".layout-button {\nbackground: #a5aeb6;\nfloat: right;\nwidth: 71px;\nheight: 29px;\noverflow: hidden;\nbox-shadow: none;\nposition: relative;\nborder-radius: 8px;\nz-index: 1;\ndisplay: flex;\nflex-direction: row;\ntop: 0px;\nright: 50px;\ncursor: pointer;\nwhite-space: normal;\n}\n.button-lines {\ndisplay: flex;\nflex-direction: column;\njustify-content: space-evenly;\nalign-items: center;\nmargin: 3px;\nheight: calc(100% - 6px);\nwidth: calc(100% - 6px);\n}\n.button-lines .line-container {\n\tdisplay: flex;\n\tflex-direction: row;\n\tjustify-content: space-evenly;\n\talign-items: center;\n\theight: 5px;\n\twidth: 75%;\n\tz-index: 3;\n\tflex: 1 1 auto;\n}\n.button-lines .line-container .thumbnail {\n\tbackground: white;\n\tborder-radius: 2px;\n\theight: 5px;\n\twidth: 5px;\n}\n.button-lines .line-container .line {\n\tbackground: white;\n\tborder-radius: 2px;\n\theight: 2px;\n\twidth: 65%;\n}\n.slider {\n\tposition: absolute;\n\tcursor: pointer;\n\theight: 100%;\n\twidth: 50%;\n\tleft: 0;\n\t-webkit-transition: 0.4s;\n\ttransition: 0.4s;\n\tborder-radius: 8px;\n\tz-index: 2;\n}\n.slider.left {\n\tleft: 50%;\n}\n.button-grid {\n\tdisplay: flex;\n\tmargin: 3px;\n\tmargin-right: 6px;\n\theight: calc(100% - 6px);\n\twidth: calc(100% - 9px);\n\tjustify-content: space-between;\n\talign-items: center;\n\talign-content: space-between;\n\tflex-wrap: wrap;\n\tz-index: 3;\n}\n.button-grid .card {\n\tborder-radius: 2px;\n\tbackground: white;\n\theight: 8px;\n\twidth: 6px;\n\tmargin: 1px;\n}\n",
																						"version": 0,
																						"width": "100%"
																					},
																					"stamp": 161
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 11,
											"data": []
										},
										"form-content-left-bottom": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "70px",
													"left": "0px",
													"height": "calc(100% - 70px)"
												},
												"hidden": false
											},
											"*": {
												"form-content-left-bottom-left": {
													"type": "FlexibleFormSplitter",
													"options": {
														"autosize": true,
														"version": 1,
														"box": {
															"top": "0px",
															"left": "0px",
															"width": "82px"
														},
														"hidden": false
													},
													"*": {
														"form-content-left-bottom-left-1": {
															"type": "FlexibleFormLine",
															"data": [],
															"options": {
																"version": 0
															},
															"stamp": 150,
															"*": {
																"VerticalTreeViewPane": {
																	"type": "PanelData",
																	"data": [],
																	"options": {
																		"border": {
																			"collapsed": false
																		},
																		"labelLength": 0,
																		"iconURL": "",
																		"TitleFontSize": "S",
																		"hideActionButtons": true,
																		"hideTitleBar": true,
																		"hideTitle": false,
																		"backgroundcolor": "default",
																		"labelsAlignment": "right",
																		"label": "_VerticalTreeViewPane",
																		"hidden": false,
																		"leftImageURL": "",
																		"removeMargins": false,
																		"elementsAlignment": "left",
																		"version": 0,
																		"panelStyle": "FlexibleFormPanelLight",
																		"sameHeightAsSiblings": false
																	},
																	"stamp": 13,
																	"*": {
																		"FieldsManager": {
																			"type": "FieldsManager",
																			"data": [],
																			"options": {
																				"controlsAssociation": {},
																				"version": 0
																			},
																			"stamp": 14,
																			"*": {
																				"Grid": {
																					"type": "GridLayout",
																					"data": [
																						"fields"
																					],
																					"options": {
																						"ctrlsPos": {
																							"VerticalTreeview__": {
																								"line": 1,
																								"column": 1
																							}
																						},
																						"lines": 1,
																						"columns": 2,
																						"colspans": [
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							]
																						],
																						"version": 0
																					},
																					"stamp": 15,
																					"*": {
																						"VerticalTreeview__": {
																							"type": "Treeview",
																							"data": false,
																							"options": {
																								"version": 1,
																								"label": "_VerticalTreeview",
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"width": "170px",
																								"horizontal": false,
																								"NbLines": 10,
																								"helpIconPosition": "Right",
																								"MaxNbLines": null
																							},
																							"stamp": 128
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														},
														"form-content-left-bottom-left-3": {
															"type": "FlexibleFormLine",
															"data": [],
															"options": {},
															"stamp": 170,
															"*": {
																"VendorFilter": {
																	"type": "PanelData",
																	"data": [],
																	"options": {
																		"border": {},
																		"iconURL": "",
																		"TitleFontSize": "M",
																		"hideActionButtons": true,
																		"hideTitleBar": false,
																		"hideTitle": false,
																		"backgroundcolor": "default",
																		"labelsAlignment": "right",
																		"label": "_VendorFilter",
																		"hidden": false,
																		"leftImageURL": "",
																		"removeMargins": false,
																		"elementsAlignment": "left",
																		"panelStyle": "FlexibleFormPanelLight",
																		"version": 0,
																		"labelLength": 0,
																		"sameHeightAsSiblings": false
																	},
																	"stamp": 164,
																	"*": {
																		"FieldsManager": {
																			"type": "FieldsManager",
																			"data": [],
																			"options": {
																				"controlsAssociation": {}
																			},
																			"stamp": 165,
																			"*": {
																				"Grid": {
																					"type": "GridLayout",
																					"data": [
																						"fields"
																					],
																					"options": {
																						"ctrlsPos": {
																							"VendorList__": {
																								"line": 1,
																								"column": 1
																							}
																						},
																						"lines": 1,
																						"columns": 2,
																						"colspans": [
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							]
																						]
																					},
																					"stamp": 166,
																					"*": {
																						"VendorList__": {
																							"type": "ComboBox",
																							"data": null,
																							"options": {
																								"possibleValues": {},
																								"version": 1,
																								"keyValueMode": false,
																								"possibleKeys": {},
																								"label": "VendorList__",
																								"activable": true,
																								"width": "100%",
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"helpIconPosition": "Right",
																								"DataSourceOrigin": "Standalone",
																								"DisplayedColumns": "",
																								"notInDB": true,
																								"dataType": "String",
																								"multiple": true,
																								"displayCheckboxes": true,
																								"maxNbLinesToDisplay": 999,
																								"nbLinesToDisplay": 5
																							},
																							"stamp": 178
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														},
														"form-content-left-bottom-left-4": {
															"type": "FlexibleFormLine",
															"data": [],
															"options": {},
															"stamp": 199,
															"*": {
																"WarehouseFilter": {
																	"type": "PanelData",
																	"data": [],
																	"options": {
																		"border": {},
																		"iconURL": "",
																		"TitleFontSize": "M",
																		"hideActionButtons": true,
																		"hideTitleBar": false,
																		"hideTitle": false,
																		"sameHeightAsSiblings": false,
																		"backgroundcolor": "default",
																		"labelsAlignment": "right",
																		"label": "_WarehouseFilter",
																		"hidden": false,
																		"leftImageURL": "",
																		"removeMargins": false,
																		"elementsAlignment": "left",
																		"panelStyle": "FlexibleFormPanelLight",
																		"version": 0,
																		"labelLength": 0
																	},
																	"stamp": 180,
																	"*": {
																		"FieldsManager": {
																			"type": "FieldsManager",
																			"data": [],
																			"options": {
																				"controlsAssociation": {},
																				"version": 0
																			},
																			"stamp": 181,
																			"*": {
																				"Grid": {
																					"type": "GridLayout",
																					"data": [
																						"fields"
																					],
																					"options": {
																						"ctrlsPos": {
																							"WarehouseList__": {
																								"line": 1,
																								"column": 1
																							}
																						},
																						"lines": 1,
																						"columns": 2,
																						"colspans": [
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							]
																						],
																						"version": 0
																					},
																					"stamp": 182,
																					"*": {
																						"WarehouseList__": {
																							"type": "ComboBox",
																							"data": [],
																							"options": {
																								"possibleValues": {},
																								"maxNbLinesToDisplay": 10,
																								"version": 1,
																								"keyValueMode": false,
																								"possibleKeys": {},
																								"label": "_WarehouseList",
																								"activable": true,
																								"width": "100%",
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"helpIconPosition": "Right",
																								"DataSourceOrigin": "Standalone",
																								"DisplayedColumns": "",
																								"notInDB": true,
																								"dataType": "String",
																								"displayCheckboxes": true,
																								"multiple": true,
																								"nbLinesToDisplay": 5
																							},
																							"stamp": 195
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													},
													"stamp": 16,
													"data": []
												},
												"form-content-left-bottom-right": {
													"type": "FlexibleFormSplitter",
													"options": {
														"version": 1,
														"box": {
															"top": "70px",
															"left": "0px",
															"width": null
														},
														"hidden": false
													},
													"*": {
														"form-content-left-bottom-right1": {
															"type": "FlexibleFormLine",
															"data": [],
															"options": {
																"version": 0
															},
															"stamp": 17,
															"*": {
																"ResultPane": {
																	"type": "PanelData",
																	"data": [],
																	"options": {
																		"border": {},
																		"labelLength": 0,
																		"iconURL": "",
																		"TitleFontSize": "S",
																		"hideActionButtons": true,
																		"hideTitleBar": true,
																		"hideTitle": true,
																		"backgroundcolor": "none",
																		"labelsAlignment": "right",
																		"label": "_ResultPane",
																		"leftImageURL": "",
																		"removeMargins": false,
																		"elementsAlignment": "left",
																		"version": 0,
																		"sameHeightAsSiblings": false
																	},
																	"stamp": 18,
																	"*": {
																		"FieldsManager": {
																			"type": "FieldsManager",
																			"data": [],
																			"options": {
																				"controlsAssociation": {
																					"CompanyCode__": "LabelCompanyCode__",
																					"LabelCompanyCode__": "CompanyCode__",
																					"ExchangeRate__": "LabelExchangeRate__",
																					"LabelExchangeRate__": "ExchangeRate__",
																					"Currency__": "LabelCurrency__",
																					"LabelCurrency__": "Currency__"
																				},
																				"version": 0
																			},
																			"stamp": 19,
																			"*": {
																				"Grid": {
																					"type": "GridLayout",
																					"data": [
																						"fields"
																					],
																					"options": {
																						"ctrlsPos": {
																							"ProductCardsList__": {
																								"line": 2,
																								"column": 1
																							},
																							"CompanyCode__": {
																								"line": 3,
																								"column": 2
																							},
																							"LabelCompanyCode__": {
																								"line": 3,
																								"column": 1
																							},
																							"ExchangeRate__": {
																								"line": 4,
																								"column": 2
																							},
																							"LabelExchangeRate__": {
																								"line": 4,
																								"column": 1
																							},
																							"Currency__": {
																								"line": 5,
																								"column": 2
																							},
																							"LabelCurrency__": {
																								"line": 5,
																								"column": 1
																							}
																						},
																						"lines": 5,
																						"columns": 2,
																						"colspans": [
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							],
																							[
																								{
																									"index": 0,
																									"length": 2
																								}
																							],
																							[],
																							[],
																							[]
																						],
																						"version": 0
																					},
																					"stamp": 20,
																					"*": {
																						"ProductCardsList__": {
																							"type": "CardsList",
																							"data": false,
																							"options": {
																								"version": 1,
																								"label": "_ProductCardsList",
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"width": "100%"
																							},
																							"stamp": 21
																						},
																						"LabelCompanyCode__": {
																							"type": "Label",
																							"data": [
																								"CompanyCode__"
																							],
																							"options": {
																								"label": "_CompanyCode",
																								"version": 0,
																								"hidden": true
																							},
																							"stamp": 22
																						},
																						"CompanyCode__": {
																							"type": "DatabaseComboBox",
																							"data": [
																								"CompanyCode__"
																							],
																							"options": {
																								"version": 1,
																								"label": "_CompanyCode",
																								"activable": true,
																								"width": 230,
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"minNbLines": 1,
																								"maxNbLines": 1,
																								"fTSmaxRecords": "20",
																								"readonly": true,
																								"browsable": true,
																								"hidden": true,
																								"autoCompleteSearchMode": "startswith",
																								"searchMode": "matches"
																							},
																							"stamp": 23
																						},
																						"LabelExchangeRate__": {
																							"type": "Label",
																							"data": [],
																							"options": {
																								"label": "_ExchangeRate",
																								"version": 0,
																								"hidden": true
																							},
																							"stamp": 24
																						},
																						"ExchangeRate__": {
																							"type": "Decimal",
																							"data": [],
																							"options": {
																								"version": 2,
																								"textSize": "S",
																								"textAlignment": "right",
																								"textStyle": "default",
																								"textColor": "default",
																								"integer": false,
																								"label": "_ExchangeRate",
																								"precision_internal": 2,
																								"precision_current": 2,
																								"activable": true,
																								"width": 230,
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"readonly": true,
																								"notInDB": true,
																								"dataType": "Number",
																								"enablePlusMinus": false,
																								"hidden": true
																							},
																							"stamp": 25
																						},
																						"LabelCurrency__": {
																							"type": "Label",
																							"data": [
																								"Currency__"
																							],
																							"options": {
																								"label": "_Currency",
																								"version": 0,
																								"hidden": true
																							},
																							"stamp": 26
																						},
																						"Currency__": {
																							"type": "ShortText",
																							"data": [
																								"Currency__"
																							],
																							"options": {
																								"textSize": "S",
																								"textAlignment": "left",
																								"textStyle": "default",
																								"textColor": "default",
																								"version": 1,
																								"label": "_Currency",
																								"activable": true,
																								"width": 230,
																								"helpText": "",
																								"helpURL": "",
																								"helpFormat": "HTML Format",
																								"readonly": true,
																								"browsable": false,
																								"hidden": true
																							},
																							"stamp": 27
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													},
													"stamp": 28,
													"data": []
												}
											},
											"stamp": 29,
											"data": []
										}
									},
									"stamp": 30,
									"data": []
								},
								"form-content-right": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0px",
											"left": "calc(100% - 450px)",
											"width": "450px"
										},
										"hidden": false
									},
									"*": {
										"form-content-right-top": {
											"type": "FlexibleFormSplitter",
											"options": {
												"autosize": true,
												"version": 1,
												"box": {
													"top": "0px",
													"left": "0px",
													"width": null,
													"height": "180px"
												},
												"hidden": false
											},
											"*": {
												"form-content-right-top1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 31,
													"*": {
														"SummaryPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "0",
																"iconURL": "",
																"TitleFontSize": "XL",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_SummaryPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "left",
																"version": 0,
																"panelStyle": "FlexibleFormPanelLight",
																"sameHeightAsSiblings": false
															},
															"stamp": 32,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 33,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"displayAsFlex": true,
																				"ctrlsPos": {
																					"Spacer1__": {
																						"line": 1,
																						"column": 1
																					},
																					"SendToPurchaseRequisition__": {
																						"line": 5,
																						"column": 1
																					},
																					"Quit__": {
																						"line": 6,
																						"column": 1
																					},
																					"TotalDescription__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer4__": {
																						"line": 7,
																						"column": 1
																					},
																					"TotalPrice__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"colspans": [
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 34,
																			"*": {
																				"Spacer1__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer1",
																						"version": 0
																					},
																					"stamp": 35
																				},
																				"TotalDescription__": {
																					"type": "ShortText",
																					"data": [
																						"TotalDescription__"
																					],
																					"options": {
																						"textSize": "L",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "color5",
																						"version": 1,
																						"label": "_TotalDescription",
																						"activable": true,
																						"width": "380px",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"length": 500
																					},
																					"stamp": 36
																				},
																				"SendToPurchaseRequisition__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SendToPurchaseRequisition",
																						"label": "_SendToPurchaseRequisition",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color3",
																						"nextprocess": {
																							"processName": "AP - Application Settings__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"awesomeClasses": "fa fa-cart-arrow-down fa-3x",
																						"style": 1
																					},
																					"stamp": 37
																				},
																				"TotalPrice__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"version": 1,
																						"label": "_TotalPrice",
																						"activable": true,
																						"width": "380px",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 500,
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false
																					},
																					"stamp": 38
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Close",
																						"label": "Bouton",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color2",
																						"nextprocess": {
																							"processName": "AP - Application Settings__",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"width": "130px",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"awesomeClasses": "fa fa-times fa-3x",
																						"style": 2
																					},
																					"stamp": 40
																				},
																				"Spacer4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer4",
																						"version": 0
																					},
																					"stamp": 41
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 42,
											"data": []
										},
										"form-content-right-bottom": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "180px",
													"left": "0px",
													"height": "calc(100% - 180px)"
												},
												"hidden": false
											},
											"*": {
												"form-content-right-bottom1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 43,
													"*": {
														"CartSummaryPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "XL",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_CartSummaryPane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"panelStyle": "FlexibleFormPanelLight",
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 44,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 45,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CartSummary__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 46,
																			"*": {
																				"CartSummary__": {
																					"type": "Table",
																					"data": [
																						"CartSummary__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_CartSummary",
																						"subsection": null,
																						"readonly": false
																					},
																					"stamp": 47,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 48,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 49
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 50
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 51,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 52,
																									"data": [],
																									"*": {
																										"CartItemDescription__": {
																											"type": "Label",
																											"data": [
																												"CartItemDescription__"
																											],
																											"options": {
																												"label": "_CartItemDescription",
																												"version": 0
																											},
																											"stamp": 53,
																											"position": [
																												"Texte multiligne"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54,
																									"data": [],
																									"*": {
																										"CartItemQuantity__": {
																											"type": "Label",
																											"data": [
																												"CartItemQuantity__"
																											],
																											"options": {
																												"label": "_CartItemQuantity",
																												"version": 0
																											},
																											"stamp": 55,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"CartItemNumber__": {
																											"type": "Label",
																											"data": [
																												"CartItemNumber__"
																											],
																											"options": {
																												"label": "_CartItemNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 57,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"CartItemPrice__": {
																											"type": "Label",
																											"data": [
																												"CartItemPrice__"
																											],
																											"options": {
																												"label": "_CartItemPrice",
																												"version": 0
																											},
																											"stamp": 59,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 60,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 61,
																									"data": [],
																									"*": {
																										"CartItemDescription__": {
																											"type": "LongText",
																											"data": [
																												"CartItemDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "bold",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_CartItemDescription",
																												"activable": true,
																												"width": "100%",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 559,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 62,
																											"position": [
																												"Texte multiligne"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"CartItemQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"CartItemQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_CartItemQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "55",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 64,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 65,
																									"data": [],
																									"*": {
																										"CartItemNumber__": {
																											"type": "ShortText",
																											"data": [
																												"CartItemNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CartItemNumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 66,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 67,
																									"data": [],
																									"*": {
																										"CartItemPrice__": {
																											"type": "ShortText",
																											"data": [
																												"CartItemPrice__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "bold",
																												"textColor": "color1",
																												"version": 1,
																												"label": "_CartItemPrice",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 100,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 68,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer1",
																						"version": 0
																					},
																					"stamp": 69
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 70,
											"data": []
										}
									},
									"stamp": 71,
									"data": []
								}
							},
							"stamp": 72,
							"data": []
						}
					},
					"stamp": 73,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1980
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 74,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 75,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 76,
							"data": []
						}
					},
					"stamp": 77,
					"data": []
				}
			},
			"stamp": 78,
			"data": []
		}
	},
	"stamps": 207,
	"data": []
}