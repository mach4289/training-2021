{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"GeneralInfo": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_GeneralInfo",
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ShipToCompany__": "LabelShip_to_company__",
																	"LabelShip_to_company__": "ShipToCompany__",
																	"ShipToContact__": "LabelShip_to_contact__",
																	"LabelShip_to_contact__": "ShipToContact__",
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"ID__": "LabelID__",
																	"LabelID__": "ID__",
																	"ShipToPhone__": "LabelShipToPhone__",
																	"LabelShipToPhone__": "ShipToPhone__",
																	"ShipToEmail__": "LabelShipToEmail__",
																	"LabelShipToEmail__": "ShipToEmail__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 6,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ShipToCompany__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelShip_to_company__": {
																				"line": 3,
																				"column": 1
																			},
																			"ShipToContact__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelShip_to_contact__": {
																				"line": 4,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"ID__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelID__": {
																				"line": 2,
																				"column": 1
																			},
																			"ShipToPhone__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelShipToPhone__": {
																				"line": 5,
																				"column": 1
																			},
																			"ShipToEmail__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelShipToEmail__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 29
																		},
																		"LabelID__": {
																			"type": "Label",
																			"data": [
																				"ID__"
																			],
																			"options": {
																				"label": "_ID",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"ID__": {
																			"type": "ShortText",
																			"data": [
																				"ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_ID",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 31
																		},
																		"LabelShip_to_company__": {
																			"type": "Label",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"label": "_Ship to company",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"ShipToCompany__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToCompany__"
																			],
																			"options": {
																				"label": "_Ship to company",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 15
																		},
																		"LabelShip_to_contact__": {
																			"type": "Label",
																			"data": [
																				"ShipToContact__"
																			],
																			"options": {
																				"label": "_Ship to contact",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"ShipToContact__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToContact__"
																			],
																			"options": {
																				"label": "_Ship to contact",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 17
																		},
																		"LabelShipToPhone__": {
																			"type": "Label",
																			"data": [
																				"ShipToPhone__"
																			],
																			"options": {
																				"label": "_Ship to phone"
																			},
																			"stamp": 34
																		},
																		"ShipToPhone__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToPhone__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Ship to phone",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 35
																		},
																		"LabelShipToEmail__": {
																			"type": "Label",
																			"data": [
																				"ShipToEmail__"
																			],
																			"options": {
																				"label": "_Ship to email",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"ShipToEmail__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToEmail__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Ship to email",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 37
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												},
												"AddressInfo": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_AddressInfo",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left"
													},
													"stamp": 39,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelShipToSub__": "ShipToSub__",
																	"ShipToSub__": "LabelShipToSub__",
																	"LabelShip_to_street__": "ShipToStreet__",
																	"ShipToStreet__": "LabelShip_to_street__",
																	"LabelShip_to_city__": "ShipToCity__",
																	"ShipToCity__": "LabelShip_to_city__",
																	"LabelShip_to_zip_code__": "ShipToZipCode__",
																	"ShipToZipCode__": "LabelShip_to_zip_code__",
																	"LabelShip_to_region__": "ShipToRegion__",
																	"ShipToRegion__": "LabelShip_to_region__",
																	"LabelShip_to_country__": "ShipToCountry__",
																	"ShipToCountry__": "LabelShip_to_country__",
																	"DeliveryAddress__": "LabelDeliveryAddress__",
																	"LabelDeliveryAddress__": "DeliveryAddress__"
																}
															},
															"stamp": 40,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelShipToSub__": {
																				"line": 1,
																				"column": 1
																			},
																			"ShipToSub__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelShip_to_street__": {
																				"line": 2,
																				"column": 1
																			},
																			"ShipToStreet__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelShip_to_city__": {
																				"line": 3,
																				"column": 1
																			},
																			"ShipToCity__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelShip_to_zip_code__": {
																				"line": 4,
																				"column": 1
																			},
																			"ShipToZipCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelShip_to_region__": {
																				"line": 5,
																				"column": 1
																			},
																			"ShipToRegion__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelShip_to_country__": {
																				"line": 6,
																				"column": 1
																			},
																			"ShipToCountry__": {
																				"line": 6,
																				"column": 2
																			},
																			"DeliveryAddress__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelDeliveryAddress__": {
																				"line": 7,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"stamp": 41,
																	"*": {
																		"ShipToCountry__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToCountry__"
																			],
																			"options": {
																				"label": "_Ship to country",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 27
																		},
																		"LabelShip_to_country__": {
																			"type": "Label",
																			"data": [
																				"ShipToCountry__"
																			],
																			"options": {
																				"label": "_Ship to country",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"ShipToRegion__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToRegion__"
																			],
																			"options": {
																				"label": "_Ship to region",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 25
																		},
																		"LabelShip_to_region__": {
																			"type": "Label",
																			"data": [
																				"ShipToRegion__"
																			],
																			"options": {
																				"label": "_Ship to region",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"ShipToZipCode__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToZipCode__"
																			],
																			"options": {
																				"label": "_Ship to zip code",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 23
																		},
																		"LabelShip_to_zip_code__": {
																			"type": "Label",
																			"data": [
																				"ShipToZipCode__"
																			],
																			"options": {
																				"label": "_Ship to zip code",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"ShipToCity__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToCity__"
																			],
																			"options": {
																				"label": "_Ship to city",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 21
																		},
																		"LabelShip_to_city__": {
																			"type": "Label",
																			"data": [
																				"ShipToCity__"
																			],
																			"options": {
																				"label": "_Ship to city",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"ShipToStreet__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToStreet__"
																			],
																			"options": {
																				"label": "_Ship to street",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 19
																		},
																		"LabelShip_to_street__": {
																			"type": "Label",
																			"data": [
																				"ShipToStreet__"
																			],
																			"options": {
																				"label": "_Ship to street",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"ShipToSub__": {
																			"type": "ShortText",
																			"data": [
																				"ShipToSub__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Ship to sub",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 33
																		},
																		"LabelShipToSub__": {
																			"type": "Label",
																			"data": [
																				"ShipToSub__"
																			],
																			"options": {
																				"label": "_Ship to sub",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"LabelDeliveryAddress__": {
																			"type": "Label",
																			"data": null,
																			"options": {
																				"label": "_DeliveryAddressFormated"
																			},
																			"stamp": 44
																		},
																		"DeliveryAddress__": {
																			"type": "LongText",
																			"data": null,
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 5,
																				"label": "_DeliveryAddressFormated",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 5,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 45
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 45,
	"data": []
}