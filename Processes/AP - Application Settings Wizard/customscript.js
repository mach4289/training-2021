///#GLOBALS Lib Sys
/**
 * Manage the General step
 */
var generalStep = {
	panels: [Controls.General_pane, Controls.General_Advanced_SAP_Informations_pane, Controls.General_Advanced_ConnectionType_Informations_pane, Controls.Available_modules_pane, Controls.ERP_pane, Controls.Portal],
	icon: "fa-cog",
	title: "General_pane",
	description: "Global settings",
	helpId: 2501,
	onStart: function ()
	{
		generalStep.initLayout();
		generalStep.handleErrorMessage();
		generalStep.handleModules();
		generalStep.handleERP();
		generalStep.handleConnectionType();
		Controls.EnableAccountPayableGlobalSetting__.OnChange = generalStep.handleModules;
		Controls.EnablePurchasingGlobalSetting__.OnChange = generalStep.updateWizard;
		Controls.EnableVendorManagementGlobalSetting__.OnChange = generalStep.updateWizard;
		Controls.ERP__.OnChange = generalStep.handleERP;
		Controls.ERPExportMethod__.OnChange = generalStep.handleConnectionType;
		Controls.SAPConfiguration__.OnChange = generalStep.showHideDisplayRequirementsButton;
		Controls.CheckSimulationProto__.OnClick = generalStep.showRequirementsDialog;
		Controls.EnableConsignmentStock__.OnChange = generalStep.handleERP;
		if (!User.isInDemoAccount)
		{
			Controls.APCodingsPrediction__.OnChange = function ()
			{
				if (Controls.APCodingsPrediction__.IsChecked())
				{
					Popup.Confirm("_APCodingsPredictionActivationMessage", false, function ()
					{
						Controls.APCodingsPrediction__.SetValue("1");
					}, function ()
					{
						Controls.APCodingsPrediction__.SetValue("0");
					}, "_APCodingsPredictionActivation");
				}
			};
		}
	},
	onQuit: function ()
	{
		var existingConfNames = [];
		if (Variable.GetValueAsString("ExistingConfigurations") && JSON.parse(Variable.GetValueAsString("ExistingConfigurations")))
		{
			existingConfNames = JSON.parse(Variable.GetValueAsString("ExistingConfigurations"));
		}
		var confName = Data.GetValue("ConfigurationName__");
		var result = true;
		if (!confName)
		{
			Controls.ConfigurationName__.SetError(Language.Translate("_Expecting a configuration name"));
			result = false;
		}
		else if (Sys.Helpers.Array.IndexOf(existingConfNames, confName) !== -1)
		{
			Controls.ConfigurationName__.SetError(Language.Translate("_Existing configuration"));
			result = false;
		}
		else if (generalStep.checkModules() === false)
		{
			result = false;
		}
		if (!result)
		{
			return result;
		}
		Controls.ERP__.OnChange = null;
		// After the first step, do not allow to change configuration name
		Controls.ConfigurationName__.SetReadOnly(true);
		return true;
	},
	handleModules: function ()
	{
		Controls.EnableExpenseClaimsGlobalSetting__.SetReadOnly(!Controls.EnableAccountPayableGlobalSetting__.IsChecked());
		Controls.EnableFlipPOGlobalSetting__.SetReadOnly(!Controls.EnableAccountPayableGlobalSetting__.IsChecked());
	},
	updateWizard: function ()
	{
		Lib.Wizard.Wizard.DrawSteps();
	},
	handleERP: function ()
	{
		var isSAP = Data.GetValue("ERP__") === "SAP";
		var isSAPS4HANA = Data.GetValue("ERP__") === "SAPS4CLOUD";
		var isGeneric = Data.GetValue("ERP__") === "generic";
		var isNAV = Data.GetValue("ERP__") === "NAV";
		Controls.ERPExportMethod__.Hide(isSAP || isSAPS4HANA);
		Controls.SAPConfiguration__.Hide(!isSAP);
		Controls.GeneralMoreOptionsSAPInformations__.Hide(!isSAP);
		Controls.SAPDocumentTypeFIConsignmentStock__.Hide((isSAP || isSAPS4HANA) && !Data.GetValue("EnableConsignmentStock__"));
		Controls.GoodIssueNumberPatterns__.Hide((!isSAP && !isSAPS4HANA) || !Data.GetValue("EnableConsignmentStock__"));
		Controls.GeneralMoreOptionsConnectionTypeInformations__.Hide(isSAP || isSAPS4HANA);
		Controls.CheckSimulationProto__.SetDisabled((!isSAP && !isSAPS4HANA) || !Controls.SAPConfiguration__.GetValue());
		if (!isSAP && !isSAPS4HANA)
		{
			Controls.CodingEnableCompanyCode__.SetValue(false);
			Controls.CodingEnableWBSElement__.SetValue(false);
			Controls.CodingEnableBusinessArea__.SetValue(false);
			Controls.CodingEnableAssignments__.SetValue(false);
			Controls.CodingEnableInternalOrder__.SetValue(false);
			Controls.CodingEnableTradingPartner__.SetValue(false);
		}
		Controls.WebserviceURL__.Hide(!isSAPS4HANA);
		Controls.WebserviceUser__.Hide(!isSAPS4HANA);
		Controls.WebservicePassword__.Hide(!isSAPS4HANA);
		Controls.WebserviceURLDetails__.Hide(!isSAPS4HANA);
		if (!isGeneric && !isNAV)
		{
			Controls.CodingEnableProjectCode__.SetValue(false);
		}
		// Always call this function to hide the associated advanced pane
		Lib.Wizard.Tools.HandleHTMLToggle(Controls.GeneralMoreOptionsSAPInformations__, Controls.General_Advanced_SAP_Informations_pane, Language.Translate("More options"));
		// Always call this function to hide the associated advanced pane
		Lib.Wizard.Tools.HandleHTMLToggle(Controls.GeneralMoreOptionsConnectionTypeInformations__, Controls.General_Advanced_ConnectionType_Informations_pane, Language.Translate("More options"));
		generalStep.handleConnectionType();
	},
	handleConnectionType: function ()
	{
		if (Data.GetValue("ERPExportMethod__") === "SFTP")
		{
			Controls.ERPNotifierProcessName__.Hide(true);
		}
		else
		{
			Controls.ERPNotifierProcessName__.Hide(false);
		}
	},
	handleErrorMessage: function ()
	{
		var errorMessage = Variable.GetValueAsString("ErrorMessage");
		if (errorMessage)
		{
			Popup.Alert(Language.Translate(errorMessage));
			Controls.ConfigurationName__.SetReadOnly(false);
			Variable.SetValueAsString("ErrorMessage", "");
		}
		else if (Variable.GetValueAsString("currentStep"))
		{
			//Once the first step has been left with a correct configuration name, the configuration name cannot be changed
			//in order to avoid issues with extraction configurations
			Controls.ConfigurationName__.SetReadOnly(true);
		}
	},
	setLastModifiedInformationsVisible: function (isVisible)
	{
		Controls.LastModifiedDateTime__.Hide(!isVisible);
		Controls.LastModifiedBy__.Hide(!isVisible);
	},
	initLayout: function ()
	{
		Controls.LastModifiedDateTime__.SetReadOnly(true);
		Controls.LastModifiedBy__.SetReadOnly(true);
		generalStep.setLastModifiedInformationsVisible(Boolean(Variable.GetValueAsString("ConfigurationLoaded")));
	},
	showHideDisplayRequirementsButton: function ()
	{
		Controls.CheckSimulationProto__.SetDisabled(!this.GetValue());
	},
	showRequirementsDialog: function ()
	{
		this.Wait(true);
		Query.SAPCallBapi(rfcReadTableCallback, Controls.SAPConfiguration__.GetValue(), "SVRS_GET_REPS_FROM_OBJECT",
		{
			"EXPORTS":
			{
				"OBJECT_NAME": "BAPI_INCOMINGINVOICE_CREATE",
				"OBJECT_TYPE": "FUNC"
			}
		});
	},
	checkModules: function ()
	{
		// Clear the value of enable Expense Management in case AP is not activated
		if (!Controls.EnableAccountPayableGlobalSetting__.IsChecked())
		{
			Controls.EnableExpenseClaimsGlobalSetting__.Check(false);
			Controls.EnableFlipPOGlobalSetting__.Check(false);
		}
		if (!Sys.Helpers.Array.Find(Controls.Available_modules_pane.GetControls(), function (ctrl)
			{
				return ctrl.GetType() === "CheckBox" && ctrl.IsChecked();
			}))
		{
			Popup.Alert("_You must select at least one module", null, null, "_Modules alert title");
			return false;
		}
		return null;
	}
};
/**
 * Manage Vendor Management
 */
var vendorManagementStep = {
	panels: [Controls.VendorManagement_options_pane],
	icon: "fas fa-briefcase",
	title: "_Vendor_management_tab_title",
	description: "_Vendor_management_tab_description",
	helpId: 2543,
	isVisible: function ()
	{
		return Data.GetValue("EnableVendorManagementGlobalSetting__");
	},
	onStart: function ()
	{
		vendorManagementStep.handleSiSid();
		Controls.EnableSiSid__.OnChange = vendorManagementStep.handleSiSid;
	},
	onQuit: function ()
	{
		return true;
	},
	handleSiSid: function ()
	{
		var hideSiSidControls = !Controls.EnableSiSid__.IsChecked();
		Controls.SiSidLogin__.Hide(hideSiSidControls);
		Controls.SiSidPassword__.Hide(hideSiSidControls);
	}
};
/**
 * Manage Purchasing
 */
var purchasingStep = {
	panels: [Controls.Purcahsing_options_pane],
	icon: "fa-shopping-cart",
	title: "Purchasing_pane_V2",
	description: "Purchasing settings_V2",
	helpId: 2509,
	isVisible: function ()
	{
		return Data.GetValue("EnablePurchasingGlobalSetting__");
	},
	onStart: null,
	onQuit: function ()
	{
		return true;
	}
};
/**
 * Manage the Destination Recognition step
 */
var destinationRecognitionStep = {
	panels: [Controls.Routing_Information, Controls.Recognition_pane, Controls.OCRParameters_pane],
	icon: "fa-external-link",
	title: "Routing_Recognition",
	description: "Select routing informations",
	helpId: 2502,
	OCRSettings:
	{
		LoadOCRJsonOverrideSettings: function ()
		{
			// Fill the languages list
			var OCRLanguages = Process.GetOCRLanguages();
			OCRLanguages = Sys.Helpers.TryCallFunction("Lib.AP.Customization.HTMLScripts.APWizard.Common.CustomizeOCRLanguagesList", OCRLanguages) || OCRLanguages;
			Controls.OCRLanguagesList__.SetAvailableValues([]);
			Controls.OCRLanguagesList__.AddOptions(OCRLanguages);
			// Try to parse the settings from the record
			var OCRJsonOverrideSettings;
			try
			{
				OCRJsonOverrideSettings = JSON.parse(Data.GetValue("OCRJsonOverrideSettings__"));
			}
			catch (e)
			{
				OCRJsonOverrideSettings = null;
			}
			// Show or not the parameters
			function UpdateOcrFieldsVisibility()
			{
				var checked = Controls.EnableOverrideOCRParameters__.IsChecked();
				var isGoogleCloud = Controls.OCREngineList__.GetValue() === "GoogleCloudVision";
				Controls.OCREngineList__.Hide(!checked || !ProcessInstance.isDebugActive);
				var hideOptions = !checked || isGoogleCloud;
				Controls.OCRImageBinarization__.Hide(hideOptions);
				Controls.OCRImageDraftFax__.Hide(hideOptions);
				Controls.OCRImageDespeckle__.Hide(hideOptions);
				Controls.OCRPdfProcessing__.Hide(hideOptions);
				Controls.OCRTolerantWordSeparation__.Hide(hideOptions);
				Controls.OCRGlobalPerformances__.Hide(hideOptions);
				Controls.OCRLanguagesList__.Hide(hideOptions);
			}
			Controls.EnableOverrideOCRParameters__.OnChange = UpdateOcrFieldsVisibility;
			// Fill the form
			if (OCRJsonOverrideSettings)
			{
				Controls.EnableOverrideOCRParameters__.Check(true);
				Controls.OCREngineList__.SetValue(OCRJsonOverrideSettings.ocr_engine ? OCRJsonOverrideSettings.ocr_engine : "KeepOriginalEngine");
				Controls.OCRPdfProcessing__.SelectOption(OCRJsonOverrideSettings["pdf-processing"]);
				Controls.OCRLanguagesList__.DeselectAllOptions();
				if (Object.prototype.hasOwnProperty.call(OCRJsonOverrideSettings, "allowed-language"))
				{
					Controls.OCRLanguagesList__.SelectOptions(OCRJsonOverrideSettings["allowed-language"].split("\n"));
				}
				Controls.OCRImageDraftFax__.Check(OCRJsonOverrideSettings["image-draft-fax"] === "yes");
				Controls.OCRImageDespeckle__.Check(OCRJsonOverrideSettings["image-despeckle"] === "yes");
				if (Object.prototype.hasOwnProperty.call(OCRJsonOverrideSettings, "ocr-tolerant-word-separation"))
				{
					Controls.OCRTolerantWordSeparation__.Check(OCRJsonOverrideSettings["ocr-tolerant-word-separation"] === "33");
				}
				Controls.OCRImageBinarization__.SelectOption(OCRJsonOverrideSettings["image-binarization"]);
				Controls.OCRGlobalPerformances__.SelectOption(OCRJsonOverrideSettings["trade-off"]);
			}
			else
			{
				Controls.EnableOverrideOCRParameters__.Check(false);
			}

			function backupOcrEngine()
			{
				if (!OCRJsonOverrideSettings)
				{
					OCRJsonOverrideSettings = {};
				}
				OCRJsonOverrideSettings.ocr_engine = Controls.OCREngineList__.GetValue();
			}
			Controls.OCREngineList__.OnChange = function ()
			{
				if (Controls.OCREngineList__.GetValue() === "GoogleCloudVision")
				{
					Popup.Confirm("_OCREngineList_GoogleCloudVision_Confirm_Selection_Message", false, function ()
					{
						// Confirm OK
						backupOcrEngine();
						UpdateOcrFieldsVisibility();
					}, function ()
					{
						// Confirm Cancel
						// Restore previous selected value
						var previousValue = "KeepOriginalEngine";
						if (OCRJsonOverrideSettings && OCRJsonOverrideSettings.ocr_engine)
						{
							previousValue = OCRJsonOverrideSettings.ocr_engine;
						}
						Controls.OCREngineList__.SetValue(previousValue);
					}, "_OCREngineList_GoogleCloudVision_Confirm_Selection_Title");
				}
				else
				{
					// Backup selected value in case of cancelation of Cloud Vision after
					backupOcrEngine();
					UpdateOcrFieldsVisibility();
				}
			};
			UpdateOcrFieldsVisibility();
		},
		SaveOCRJsonOverrideSettings: function ()
		{
			if (Controls.EnableOverrideOCRParameters__.IsChecked())
			{
				var ocrEngine = Controls.OCREngineList__.GetValue() !== "KeepOriginalEngine" ? Controls.OCREngineList__.GetValue() : null;
				var draftFax = Controls.OCRImageDraftFax__.IsChecked() ? "yes" : "no";
				var despeckle = Controls.OCRImageDespeckle__.IsChecked() ? "yes" : "no";
				var tolerance = Controls.OCRTolerantWordSeparation__.IsChecked() ? "33" : "no";
				var sLanguages = "";
				var sLanguagesArray = Controls.OCRLanguagesList__.GetSelectedOptions();
				if (sLanguagesArray)
				{
					for (var i in sLanguagesArray)
					{
						if (sLanguagesArray[i] !== "")
						{
							sLanguages += sLanguagesArray[i];
							sLanguages += "\n";
						}
					}
					sLanguages = sLanguages.slice(0, sLanguages.length - 1);
				}
				var json = {
					"pdf-processing": Controls.OCRPdfProcessing__.GetSelectedOption(),
					"image-draft-fax": draftFax,
					"image-despeckle": despeckle,
					"ocr-tolerant-word-separation": tolerance,
					"image-binarization": Controls.OCRImageBinarization__.GetSelectedOption(),
					"trade-off": Controls.OCRGlobalPerformances__.GetSelectedOption()
				};
				if (sLanguages)
				{
					json["allowed-language"] = sLanguages;
				}
				if (ocrEngine)
				{
					json.ocr_engine = ocrEngine;
				}
				Data.SetValue("OCRJsonOverrideSettings__", JSON.stringify(json));
			}
			else
			{
				Data.SetValue("OCRJsonOverrideSettings__", "");
			}
		}
	},
	onStart: function ()
	{
		Lib.Wizard.Tools.HandleHTMLToggle(Controls.DestinationMoreOptionsInvoiceDestination__, Controls.DefaultAPClerkEnd__, Language.Translate("More options"));
		Controls.ReconciliationType__.OnChange = destinationRecognitionStep.ReconciliationType.onChange;
		Controls.HeaderFooterThreshold__.OnChange = destinationRecognitionStep.HeaderFooterThreshold.validate;
		destinationRecognitionStep.HeaderFooterThreshold.updateVisibility();
		destinationRecognitionStep.OCRSettings.LoadOCRJsonOverrideSettings();
		Controls.Autolearning_Folder__.OnChange = destinationRecognitionStep.onAutolearningFolderChanged;
		destinationRecognitionStep.onAutolearningFolderChanged();
	},
	onQuit: function ()
	{
		var isValid = true;
		if (Controls.CompanyCode__.GetError())
		{
			isValid = false;
		}
		destinationRecognitionStep.OCRSettings.SaveOCRJsonOverrideSettings();
		destinationRecognitionStep.HeaderFooterThreshold.validate();
		return isValid;
	},
	ReconciliationType:
	{
		onChange: function ()
		{
			if (Controls.ReconciliationType__.GetValue() === "LineItems")
			{
				Controls.HeaderFooterThreshold__.SetValue("");
			}
			destinationRecognitionStep.HeaderFooterThreshold.updateVisibility();
		}
	},
	HeaderFooterThreshold:
	{
		updateVisibility: function ()
		{
			Controls.HeaderFooterThreshold__.Hide(Controls.ReconciliationType__.GetSelectedOption() !== "HeaderFooter");
		},
		validate: function ()
		{
			var isHeaderFooter = Controls.ReconciliationType__.GetValue() === "HeaderFooter";
			Controls.HeaderFooterThreshold__.SetWarning("");
			if (isHeaderFooter)
			{
				// Validate the value
				var validationRegEx = new RegExp(/^[0-9.]+\s?%?$/);
				if (!validationRegEx.test(Controls.HeaderFooterThreshold__.GetValue()))
				{
					Controls.HeaderFooterThreshold__.SetWarning(Language.Translate("_HeaderFooterThreshold_InvalidValue"));
				}
			}
			return !Controls.HeaderFooterThreshold__.GetWarning();
		}
	},
	onAutolearningFolderChanged: function ()
	{
		// Only allow to select the fallback on common folder if a folder is specified
		var value = Controls.Autolearning_Folder__.GetValue();
		if (value)
		{
			Controls.Autolearning_FallbackOnCommonFolder__.SetReadOnly(false);
		}
		else
		{
			Controls.Autolearning_FallbackOnCommonFolder__.SetValue("");
			Controls.Autolearning_FallbackOnCommonFolder__.SetReadOnly(true);
		}
	}
};
/**
 * Manage the Verification Coding step of the wizard
 */
var verificationCodingStep = {
	panels: [Controls.Verification_pane, Controls.Coding_pane, Controls.Taxes_pane, Controls.PlannedDeliveryCosts_pane],
	icon: "fa-eye",
	title: "Verification_coding_pane",
	description: "Verification coding description",
	helpId: 2503,
	onStart: function ()
	{
		verificationCodingStep.initLayout();
	},
	onQuit: function ()
	{
		return true;
	},
	initLayout: function ()
	{
		var isSAP = Data.GetValue("ERP__") === "SAP";
		var isSAPS4HANA = Data.GetValue("ERP__") === "SAPS4CLOUD";
		var isGeneric = Data.GetValue("ERP__") === "generic";
		var isNAV = Data.GetValue("ERP__") === "NAV";
		var isEBS = Data.GetValue("ERP__") === "EBS";
		Controls.SAPDuplicateCheck__.Hide(!isSAP);
		Controls.CodingEnableCompanyCode__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableWBSElement__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableBusinessArea__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableAssignments__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableInternalOrder__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableTradingPartner__.Hide(!isSAP && !isSAPS4HANA);
		Controls.CodingEnableProjectCode__.Hide(!isGeneric && !isNAV);
		// The taxes pane is only visible in SAP connected mode
		Controls.Taxes_pane.Hide(!isEBS && !isGeneric && !isSAP);
		Controls.TaxesWithholdingTax__.Hide(!isSAP);
		Controls.MultiTaxesOnALineItem__.Hide(isSAP);
		if (!isEBS && !isGeneric)
		{
			Controls.MultiTaxesOnALineItem__.SetValue(false);
		}
		// The Planned delivery costs pane is only visible in SAP connected mode
		Controls.PlannedDeliveryCosts_pane.Hide(!isSAP);
		Controls.PlannedPricingConditions__.Hide(!isSAP);
		// PO matching mode is not available for SAP
		if (isSAP)
		{
			Controls.VerificationPOMatchingMode__.Hide(true);
			Controls.VerificationPOMatchingMode__.SetValue("PO");
		}
		else
		{
			Controls.VerificationPOMatchingMode__.Hide(false);
		}
	}
};
var paymentApprovalWorkflowStep = {
	panels: [Controls.Workflow_pane],
	icon: "fa-check-circle-o",
	title: "Payment_approval_workflow",
	description: "Payment approval workflow description",
	helpId: 2504,
	associatedControls:
	{
		"CodingEnableCostCenter__": Controls.WorkflowReviewersCanModifyCostCenter__,
		"CodingEnableGLAccount__": Controls.WorkflowReviewersCanModifyGLAccount__,
		"CodingEnableCompanyCode__": Controls.WorkflowReviewersCanModifyCompanyCode__,
		"CodingEnableWBSElement__": Controls.WorkflowReviewersCanModifyWBSElement__,
		"CodingEnableBusinessArea__": Controls.WorkflowReviewersCanModifyBusinessArea__,
		"CodingEnableAssignments__": Controls.WorkflowReviewersCanModifyAssignments__,
		"CodingEnableInternalOrder__": Controls.WorkflowReviewersCanModifyInternalOrder__,
		"CodingEnableTradingPartner__": Controls.WorkflowReviewersCanModifyTradingPartner__,
		"CodingEnableProjectCode__": Controls.WorkflowReviewersCanModifyProjectCode__
	},
	onStart: function ()
	{
		// Display/Hide specifics fields for SAP
		var isSAP = Data.GetValue("ERP__") === "SAP";
		var isGeneric = Data.GetValue("ERP__") === "generic";
		var isNAV = Data.GetValue("ERP__") === "NAV";
		Controls.WorkflowReviewersCanModifyCompanyCode__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyWBSElement__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyBusinessArea__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyAssignments__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyInternalOrder__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyTradingPartner__.Hide(!isSAP);
		Controls.WorkflowReviewersCanModifyProjectCode__.Hide(!isGeneric && !isNAV);
		paymentApprovalWorkflowStep.syncWorkflowAxisWithCoding();
		Controls.CodingEnableCostCenter__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableGLAccount__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableCompanyCode__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableWBSElement__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableBusinessArea__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableAssignments__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableInternalOrder__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableTradingPartner__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.CodingEnableProjectCode__.OnChange = paymentApprovalWorkflowStep.onCodingVisibilityChanged;
		Controls.WorkflowReviewersCanAddLineItems__.OnChange = paymentApprovalWorkflowStep.onWorkflowReviewersCanAddLineItemsChange;
	},
	onQuit: function ()
	{
		return true;
	},
	syncWorkflowAxisWithCoding: function ()
	{
		// Enable/Disable not selected in previous step
		for (var c in paymentApprovalWorkflowStep.associatedControls)
		{
			if (Object.prototype.hasOwnProperty.call(paymentApprovalWorkflowStep.associatedControls, c))
			{
				var workflowControl = paymentApprovalWorkflowStep.associatedControls[c];
				var codingControl = Controls[c];
				workflowControl.SetReadOnly(!codingControl.IsChecked() || Controls.WorkflowReviewersCanAddLineItems__.IsChecked());
				if (Controls.WorkflowReviewersCanAddLineItems__.IsChecked())
				{
					workflowControl.Check(codingControl.IsChecked());
				}
				if (!codingControl.IsChecked())
				{
					workflowControl.Check(false);
				}
			}
		}
		if (Controls.WorkflowReviewersCanAddLineItems__.GetValue())
		{
			Controls.WorkflowReviewersCanAddLineItems__.SetHelpData("_InfoWorkflowReviewersCanAddLineItems", "", "HTML Format");
		}
		else
		{
			Controls.WorkflowReviewersCanAddLineItems__.SetHelpData("", "", "HTML Format");
		}
	},
	onCodingVisibilityChanged: function ()
	{
		if (!this.IsChecked())
		{
			var workflowControl = paymentApprovalWorkflowStep.associatedControls[this.GetName()];
			if (workflowControl)
			{
				workflowControl.Check(false);
			}
		}
	},
	onWorkflowReviewersCanAddLineItemsChange: function ()
	{
		paymentApprovalWorkflowStep.syncWorkflowAxisWithCoding();
	}
};
var archivingVendorPortalStep = {
	panels: [Controls.Archiving_pane, Controls.Vendor_Portal_Archiving],
	icon: "fa-archive",
	title: "Archiving_Vendor_Portal",
	description: "Archiving and vendor portal description",
	helpId: 2505,
	onStart: null,
	onQuit: function ()
	{
		return true;
	}
};
var labsStep = {
	panels: [Controls.Labs_pane],
	icon: "fa-flask",
	title: "Labs",
	description: "Experimental features",
	onStart: function ()
	{
		if (Data.GetValue("ERP__") !== "SAP")
		{
			Controls.EnableConsignmentStock__.Hide(true);
		}
		else
		{
			Controls.EnableConsignmentStock__.Hide(false);
		}
		Controls.EnableConsignmentStock__.OnChange = generalStep.handleERP;
	},
	onQuit: function ()
	{
		if (Controls.CompanyCode__.GetError())
		{
			return false;
		}
		return true;
	},
	hideStep: function ()
	{
		for (var _i = 0, _a = labsStep.panels; _i < _a.length; _i++)
		{
			var panel = _a[_i];
			panel.Hide(true);
		}
	},
	GetLabel: function ()
	{
		return null;
	}
};
var FieldNameLastModifiedDateTime = "LASTMODIFIEDDATETIME__";
var FieldNameOrderNumberPatterns = "ORDERNUMBERPATTERNS__";
var FieldNameGoodIssueNumberPatterns = "GOODISSUENUMBERPATTERNS__";

function InitializeConfigurationValues(callback)
{
	function InitializeGeneralPane()
	{
		//The configuration has been loaded from an existing record : it must not be modified !!
		var configurationName = Data.GetValue("ConfigurationName__");
		if (configurationName)
		{
			Controls.ConfigurationName__.SetReadOnly(true);
			generalStep.setLastModifiedInformationsVisible(true);
			Variable.SetValueAsString("ConfigurationLoaded", configurationName);
		}
	}

	function StoreConfigurationName()
	{
		var err = this.GetQueryError();
		if (err)
		{
			EndInitialization();
			Popup.Alert(err);
			return;
		}
		var existingConfNames = [];
		for (var i = 0; i < this.GetRecordsCount(); i++)
		{
			existingConfNames.push(this.GetQueryValue("ConfigurationName__", i));
		}
		Variable.SetValueAsString("ExistingConfigurations", JSON.stringify(existingConfNames));
		EndInitialization();
	}

	function FillValueForField(query, field)
	{
		var value = query.GetQueryValue(field);
		if (field.toUpperCase() === FieldNameLastModifiedDateTime.toUpperCase())
		{
			var dateLastModification = Sys.Helpers.Date.ISOSTringToDate(value);
			value = Sys.Helpers.Date.GetDateInUserTimezone(dateLastModification, User.utcOffset);
		}
		else if (field.toUpperCase() === FieldNameOrderNumberPatterns.toUpperCase() ||
			field.toUpperCase() === FieldNameGoodIssueNumberPatterns.toUpperCase())
		{
			var table = value.split(";");
			value = "";
			for (var i = 0; i < table.length; i++)
			{
				value = value + table[i] + (i === table.length - 1 ? "" : "\n");
			}
		}
		Data.SetValue(field, value);
	}

	function FillValues(query)
	{
		var fields = query.GetQueryValue().RecordsDefinition;
		var count = 0;
		for (var field in fields)
		{
			if (/.*__$/.test(field))
			{
				FillValueForField(query, field);
				count++;
			}
		}
		Log.Info("Sucessfully initialized " + count + " values for the configuration");
	}
	// handle upgrade on newly-added fields
	function FillDefaultValuesForExistingConfiguration(query)
	{
		var defaultValuesMap = {
			EnablePortalAccountCreation__: true
		};
		var headers = query.GetQueryValue().RecordsDefinition;
		for (var defaultValueKey in defaultValuesMap)
		{
			if (!(defaultValueKey.toUpperCase() in headers))
			{
				// Set the default value
				Data.SetValue(defaultValueKey, defaultValuesMap[defaultValueKey]);
			}
		}
	}

	function FillValuesCallback()
	{
		var err = this.GetQueryError();
		if (err)
		{
			EndInitialization();
			Popup.Alert(err);
			return;
		}
		if (this.GetRecordsCount() === 1)
		{
			FillDefaultValuesForExistingConfiguration(this);
			FillValues(this);
			InitializeGeneralPane();
		}
		var currentConfigurationName = Data.GetValue("ConfigurationName__");
		// Only query if it's a new configuration => configuration name is empty
		if (!currentConfigurationName)
		{
			// Query all configuration names
			Query.DBQuery(StoreConfigurationName, "AP - Application Settings__", "ConfigurationName__");
		}
		callback();
		if (currentConfigurationName)
		{
			EndInitialization();
		}
	}

	function FillGlobalValuesCallback()
	{
		var err = this.GetQueryError();
		if (err)
		{
			EndInitialization();
			Popup.Alert(err);
			return;
		}
		var nbRecords = this.GetRecordsCount();
		if (nbRecords === 1)
		{
			FillValues(this);
		}
		//The record that must be loaded is set using the "AncestorsRuid" variable
		Query.DBQuery(FillValuesCallback, "AP - Application Settings__", "", "Ruid=" + Variable.GetValueAsString("AncestorsRuid"));
	}

	function EndInitialization()
	{
		ProcessInstance.SetSilentChange(false);
	}
	ProcessInstance.SetSilentChange(true);
	Query.DBQuery(FillGlobalValuesCallback, "P2P - Global Application Settings__", "");
}

function parseFunctionModule(sapResults)
{
	// Get list of TABLES,USING and CHANGING for every PERFORM
	var performs = [];
	var currentPerform = null;
	var currentGroup = null;
	for (var _i = 0, sapResults_1 = sapResults; _i < sapResults_1.length; _i++)
	{
		var sapResult = sapResults_1[_i];
		var line = sapResult.LINE;
		// Skip comments
		if (line.indexOf("*") === 0)
		{
			continue;
		}
		// Check new "perform" (function call)
		var performIndex = line.toUpperCase().indexOf("PERFORM");
		if (performIndex !== -1)
		{
			currentPerform = {
				name: line.substr(performIndex + 8).split(/ |\./)[0]
			};
			performs.push(currentPerform);
		}
		if (currentPerform)
		{
			// Check group (TABLES,USING or CHANGING)
			currentGroup = line.match(/TABLES|USING|CHANGING/i) || currentGroup;
			if (currentGroup)
			{
				currentGroup = currentGroup.toString().toUpperCase();
				currentPerform[currentGroup] = currentPerform[currentGroup] || [];
				// Add param to list
				var currentParam = line.substring(line.lastIndexOf(" ")).replace(/\./g, "");
				currentPerform[currentGroup].push(currentParam);
			}
		}
		// A dot is the end of the call
		if (line.indexOf(".") === line.length - 1)
		{
			currentPerform = null;
			currentGroup = null;
		}
	}
	return performs;
}

function formatABAPCode(codes)
{
	var text = "<div style=\"height: 700px;\"><code>";
	for (var _i = 0, codes_1 = codes; _i < codes_1.length; _i++)
	{
		var code = codes_1[_i];
		text += code.LINE.replace(/ /g, "&nbsp;") + "<br>";
	}
	text += "</code></div>";
	return text;
}

function rfcReadTableCallback(jsonResult)
{
	if (jsonResult.GetQueryError())
	{
		Popup.Alert(jsonResult.GetQueryError(), false, null, "_Requirements dialog title");
		return;
	}
	var result = parseFunctionModule(jsonResult.TABLES.REPOS_TAB);

	function fillPopup(dialog)
	{
		var description = dialog.AddDescription("Description__");
		description.SetText("_Requirements dialog explanations");
		var link = dialog.AddLink("LinkToDoc__");
		link.SetText("_Requirements dialog help link");
		link.SetOpenInCurrentWindow(true);
		link.SetURL("#");
		var table = dialog.AddTable("FunctionModuleCalls__");
		table.SetReadOnly(true);
		table.SetRowToolsHidden(true);
		table.HideBottomNavigation(true);
		table.HideTopNavigation(true);
		var groups = ["USING", "TABLES", "CHANGING"];
		table.AddDescriptionColumn("Name__", "_Function name", 200);
		for (var _i = 0, groups_1 = groups; _i < groups_1.length; _i++)
		{
			var g = groups_1[_i];
			table.AddDescriptionColumn(g, "_" + g, 50);
		}
		for (var _a = 0, result_1 = result; _a < result_1.length; _a++)
		{
			var perform = result_1[_a];
			var item = table.AddItem();
			for (var _b = 0, groups_2 = groups; _b < groups_2.length; _b++)
			{
				var group = groups_2[_b];
				item.SetValue("Name__", perform.name);
				item.SetValue(group, perform[group] ? perform[group].length : 0);
			}
		}
		var button = dialog.AddButton("ShowFullCode__");
		button.SetText(Language.Translate("_Advanced information"));
	}

	function handlePopup(dialog, tabId, event, control)
	{
		if (event === "OnClick")
		{
			if (control.GetName() === "ShowFullCode__")
			{
				Popup.Dialog("_Advanced information dialog title", control, function (innerdialog)
				{
					var fullCodeCtrl = innerdialog.AddHTML("FullCode__");
					fullCodeCtrl.SetHTML(formatABAPCode(jsonResult.TABLES.REPOS_TAB));
				});
			}
			else if (control.GetName() === "LinkToDoc__")
			{
				Process.ShowHelp(2530);
			}
		}
	}
	// Display result popup
	Popup.Dialog("_Requirements dialog title", null, fillPopup, null, null, handlePopup);
}
var steps = [generalStep, vendorManagementStep, purchasingStep, destinationRecognitionStep, verificationCodingStep, paymentApprovalWorkflowStep, archivingVendorPortalStep];

function OnSave()
{
	for (var _i = 0, steps_1 = steps; _i < steps_1.length; _i++)
	{
		var step = steps_1[_i];
		if (!step.onQuit())
		{
			return false;
		}
	}
	return true;
}

function Main()
{
	if (ProcessInstance.isDebugActive)
	{
		steps.push(labsStep);
	}
	else
	{
		labsStep.hideStep();
	}
	Lib.Wizard.Wizard.Init(
	{
		steps: steps,
		previousButton: Controls.Previous,
		nextButton: Controls.Next,
		wizardControl: Controls.Wizard_steps__,
		descriptionControl: Controls.Wizard_step_desc__,
		LoadFirstStep: function ()
		{
			return Variable.GetValueAsString("currentStep");
		},
		SaveCurrentStep: function (currentStep)
		{
			ProcessInstance.SetSilentChange(true);
			Variable.SetValueAsString("currentStep", currentStep);
			ProcessInstance.SetSilentChange(false);
		},
		RefreshDefaultValues: function (runWizardCb)
		{
			if (!Variable.GetValueAsString("currentStep"))
			{
				InitializeConfigurationValues(runWizardCb);
			}
			else
			{
				runWizardCb();
			}
		}
	});
	// Other layout initializations
	Controls.Save.OnClick = OnSave;
}
/// ==================================
/// Entry point !!!
/// ==================================
Main();