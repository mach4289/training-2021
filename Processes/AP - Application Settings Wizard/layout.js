{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents",
														"hidden": true,
														"sameHeightAsSiblings": false
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-center-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"Wizard_steps": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color1",
																"labelsAlignment": "right",
																"label": "Wizard steps",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 9,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Wizard_steps__": "LabelWizard_steps__",
																			"LabelWizard_steps__": "Wizard_steps__"
																		},
																		"version": 0
																	},
																	"stamp": 10,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line__": {
																						"line": 2,
																						"column": 1
																					},
																					"Wizard_steps__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelWizard_steps__": {
																						"line": 1,
																						"column": 1
																					},
																					"Wizard_step_desc__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 11,
																			"*": {
																				"Wizard_steps__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"width": "",
																						"css": "@ .wizard-container {\n\tleft: 50%;\n\tposition: absolute;\n}@ \n.wizard-timeline {\n\tcontent: \"\";\n\theight: 3px;\n\tmargin-top: 35px;\n\tposition: absolute;\n\tbackground-color: #00b4bd;\n\tz-index: 10;\n\tleft: 0px;\n}@ \n.wizard {\n\theight: 150px;\n\tpadding-bottom: 15px;\n\tz-index: 30;\n\tleft: 0px;\n\tposition: absolute;\n\tcolor: #263645;\n}@ \n.wizard td {\n\ttext-align: center;\n\tfont-size: 14px;\n}@ \n.wizard td i {\n\twidth: 50px;\n\tpadding: 10px;\n}@ \n.wizard td.text {\n\ttext-transform: uppercase;\n\twhite-space: pre-wrap;\n\tvertical-align: top;\n\tfont-size: 12px;\n}@ \n.wizard td.wizardSelected {\n\tcolor: White;\n}@ ",
																						"version": 0
																					},
																					"stamp": 12
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "130",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 13
																				},
																				"Wizard_step_desc__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "color8",
																						"backgroundcolor": "default",
																						"label": "Select the invoice verification and coding preferences.",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 14
																				},
																				"LabelWizard_steps__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 15
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 16
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 17,
													"*": {
														"TitleArrow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "TitleArrow",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 18,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 19,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLArrow__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 20,
																			"*": {
																				"HTMLArrow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLArrow",
																						"htmlContent": "<div class=\"wizard-arrow\"></div>",
																						"css": "@ .wizard-arrow {\nbackground-color: white;\nwidth: 0;\nheight: 0;\nborder-style: solid;\nborder-width: 20px 25px 0 25px;\nborder-color: #008998 transparent transparent transparent;\nposition: absolute;\ntop: -10px;\nheight: 50px;\nleft: 50%;\nmargin-left: -25px;\n}@ ",
																						"width": 962,
																						"version": 0
																					},
																					"stamp": 21
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-center-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 22,
													"*": {
														"General_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "General_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 23,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelConfigurationName__": "ConfigurationName__",
																			"ConfigurationName__": "LabelConfigurationName__",
																			"LastModifiedDateTime__": "LabelLastModifiedDateTime__",
																			"LabelLastModifiedDateTime__": "LastModifiedDateTime__",
																			"LastModifiedBy__": "LabelLastModifiedBy__",
																			"LabelLastModifiedBy__": "LastModifiedBy__"
																		},
																		"version": 0
																	},
																	"stamp": 24,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelConfigurationName__": {
																						"line": 1,
																						"column": 1
																					},
																					"ConfigurationName__": {
																						"line": 1,
																						"column": 2
																					},
																					"LastModifiedDateTime__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelLastModifiedDateTime__": {
																						"line": 2,
																						"column": 1
																					},
																					"LastModifiedBy__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelLastModifiedBy__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 25,
																			"*": {
																				"LabelConfigurationName__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"label": "_Configuration Name",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"ConfigurationName__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Configuration Name",
																						"activable": true,
																						"width": 250,
																						"browsable": false,
																						"version": 0,
																						"autocompletable": false
																					},
																					"stamp": 27
																				},
																				"LabelLastModifiedDateTime__": {
																					"type": "Label",
																					"data": [
																						"LastModifiedDateTime__"
																					],
																					"options": {
																						"label": "_Last Modified Date Time",
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"LastModifiedDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"LastModifiedDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_Last Modified Date Time",
																						"activable": true,
																						"width": 250,
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"LabelLastModifiedBy__": {
																					"type": "Label",
																					"data": [
																						"LastModifiedBy__"
																					],
																					"options": {
																						"label": "_Last Modified By",
																						"version": 0
																					},
																					"stamp": 30
																				},
																				"LastModifiedBy__": {
																					"type": "ShortText",
																					"data": [
																						"LastModifiedBy__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Last Modified By",
																						"activable": true,
																						"width": 250,
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 32
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 33,
															"data": []
														}
													},
													"stamp": 34,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 35,
															"data": []
														}
													},
													"stamp": 36,
													"data": []
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 37,
													"*": {
														"Routing_Information": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Routing Information",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 38,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"DefaultAPClerk__": "LabelDefaultAPClerk__",
																			"LabelDefaultAPClerk__": "DefaultAPClerk__",
																			"DefaultAPClerkEnd__": "LabelDefaultAPClerkEnd__",
																			"LabelDefaultAPClerkEnd__": "DefaultAPClerkEnd__",
																			"DestinationMoreOptionsInvoiceDestination__": "LabelDestinationMoreOptionsInvoiceDestination__",
																			"LabelDestinationMoreOptionsInvoiceDestination__": "DestinationMoreOptionsInvoiceDestination__",
																			"LabelAutomatedDeterminationCompanyCode__": "AutomatedDeterminationCompanyCode__",
																			"AutomatedDeterminationCompanyCode__": "LabelAutomatedDeterminationCompanyCode__"
																		},
																		"version": 0
																	},
																	"stamp": 39,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyCode__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 2,
																						"column": 1
																					},
																					"DefaultAPClerk__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDefaultAPClerk__": {
																						"line": 3,
																						"column": 1
																					},
																					"DefaultAPClerkEnd__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelDefaultAPClerkEnd__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line8__": {
																						"line": 6,
																						"column": 1
																					},
																					"DestinationMoreOptionsInvoiceDestination__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDestinationMoreOptionsInvoiceDestination__": {
																						"line": 4,
																						"column": 1
																					},
																					"LabelAutomatedDeterminationCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"AutomatedDeterminationCompanyCode__": {
																						"line": 1,
																						"column": 2
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 40,
																			"*": {
																				"LabelAutomatedDeterminationCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"AutomatedDeterminationCompanyCode__"
																					],
																					"options": {
																						"label": "_AutomatedDeterminationCompanyCode",
																						"version": 0
																					},
																					"stamp": 41
																				},
																				"AutomatedDeterminationCompanyCode__": {
																					"type": "CheckBox",
																					"data": [
																						"AutomatedDeterminationCompanyCode__"
																					],
																					"options": {
																						"label": "_AutomatedDeterminationCompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "_HelpAutomatedDeterminationCompanyCode",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Default Company Code",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Default Company Code",
																						"activable": true,
																						"width": 250,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 44
																				},
																				"LabelDefaultAPClerk__": {
																					"type": "Label",
																					"data": [
																						"DefaultAPClerk__"
																					],
																					"options": {
																						"label": "_DefaultAPClerk",
																						"version": 0
																					},
																					"stamp": 45
																				},
																				"DefaultAPClerk__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"DefaultAPClerk__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_DefaultAPClerk",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 46
																				},
																				"DestinationMoreOptionsInvoiceDestination__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0,
																						"width": ""
																					},
																					"stamp": 47
																				},
																				"LabelDefaultAPClerkEnd__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_DefaultAPClerkEnd",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"DefaultAPClerkEnd__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"DefaultAPClerkEnd__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_DefaultAPClerkEnd",
																						"activable": true,
																						"width": "250",
																						"helpText": "_DefaultAPClerkEnd_Help",
																						"helpURL": "2508",
																						"helpFormat": "HTML Format",
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 49
																				},
																				"Spacer_line8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line8",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"LabelDestinationMoreOptionsInvoiceDestination__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 51
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-20": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 52,
													"*": {
														"Available_modules_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Available_modules_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 53,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"EnableAccountPayableGlobalSetting__": "LabelEnableAccountPayableGlobalSetting__",
																			"LabelEnableAccountPayableGlobalSetting__": "EnableAccountPayableGlobalSetting__",
																			"EnablePurchasingGlobalSetting__": "LabelEnablePurchasingGlobalSetting__",
																			"LabelEnablePurchasingGlobalSetting__": "EnablePurchasingGlobalSetting__",
																			"EnableExpenseClaimsGlobalSetting__": "LabelEnableExpenseClaimsGlobalSetting__",
																			"LabelEnableExpenseClaimsGlobalSetting__": "EnableExpenseClaimsGlobalSetting__",
																			"EnableFlipPOGlobalSetting__": "LabelEnableFlipPOGlobalSetting__",
																			"LabelEnableFlipPOGlobalSetting__": "EnableFlipPOGlobalSetting__",
																			"EnableVendorManagementGlobalSetting__": "LabelEnableVendorManagementGlobalSetting__",
																			"LabelEnableVendorManagementGlobalSetting__": "EnableVendorManagementGlobalSetting__",
																			"EnableContractGlobalSetting__": "LabelEnableContractGlobalSetting__",
																			"LabelEnableContractGlobalSetting__": "EnableContractGlobalSetting__"
																		},
																		"version": 0
																	},
																	"stamp": 54,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"EnableAccountPayableGlobalSetting__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEnableAccountPayableGlobalSetting__": {
																						"line": 1,
																						"column": 1
																					},
																					"EnablePurchasingGlobalSetting__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEnablePurchasingGlobalSetting__": {
																						"line": 2,
																						"column": 1
																					},
																					"EnableExpenseClaimsGlobalSetting__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelEnableExpenseClaimsGlobalSetting__": {
																						"line": 3,
																						"column": 1
																					},
																					"EnableFlipPOGlobalSetting__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelEnableFlipPOGlobalSetting__": {
																						"line": 5,
																						"column": 1
																					},
																					"EnableVendorManagementGlobalSetting__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelEnableVendorManagementGlobalSetting__": {
																						"line": 4,
																						"column": 1
																					},
																					"EnableContractGlobalSetting__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelEnableContractGlobalSetting__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 6,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 55,
																			"*": {
																				"LabelEnableAccountPayableGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableAccountPayableGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Account Payable (global setting)",
																						"version": 0
																					},
																					"stamp": 56
																				},
																				"EnableAccountPayableGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableAccountPayableGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Account Payable (global setting)",
																						"activable": true,
																						"width": "250",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 57
																				},
																				"LabelEnablePurchasingGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnablePurchasingGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Purchasing (global setting)_V2",
																						"version": 0
																					},
																					"stamp": 58
																				},
																				"EnablePurchasingGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnablePurchasingGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Purchasing (global setting)_V2",
																						"activable": true,
																						"width": "250",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 59
																				},
																				"LabelEnableExpenseClaimsGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableExpenseClaimsGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Expense Claims (global setting)",
																						"version": 0
																					},
																					"stamp": 60
																				},
																				"EnableExpenseClaimsGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableExpenseClaimsGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Expense Claims (global setting)",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 61
																				},
																				"LabelEnableVendorManagementGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableVendorManagementGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Vendor Management (global setting)",
																						"version": 0
																					},
																					"stamp": 62
																				},
																				"EnableVendorManagementGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableVendorManagementGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable Vendor Management (global setting)",
																						"activable": true,
																						"width": "230",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 63
																				},
																				"LabelEnableFlipPOGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableFlipPOGlobalSetting__"
																					],
																					"options": {
																						"label": "Enable 'Flip PO' (global setting)",
																						"version": 0
																					},
																					"stamp": 64
																				},
																				"EnableFlipPOGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableFlipPOGlobalSetting__"
																					],
																					"options": {
																						"label": "Enable 'Flip PO' (global setting)",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 65
																				},
																				"LabelEnableContractGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableContractGlobalSetting__"
																					],
																					"options": {
																						"label": "_EnableContractGlobalSetting",
																						"version": 0
																					},
																					"stamp": 66
																				},
																				"EnableContractGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableContractGlobalSetting__"
																					],
																					"options": {
																						"label": "_EnableContractGlobalSetting",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 67
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-25": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 68,
													"*": {
														"Portal": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_VendorPortal_Pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 69,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"EnablePortalAccountCreation__": "LabelEnablePortalAccountCreation__",
																			"LabelEnablePortalAccountCreation__": "EnablePortalAccountCreation__"
																		},
																		"version": 0
																	},
																	"stamp": 70,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"EnablePortalAccountCreation__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEnablePortalAccountCreation__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 71,
																			"*": {
																				"LabelEnablePortalAccountCreation__": {
																					"type": "Label",
																					"data": [
																						"EnablePortalAccountCreation__"
																					],
																					"options": {
																						"label": "_EnablePortalAccountCreation",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"EnablePortalAccountCreation__": {
																					"type": "CheckBox",
																					"data": [
																						"EnablePortalAccountCreation__"
																					],
																					"options": {
																						"label": "_EnablePortalAccountCreation",
																						"activable": true,
																						"width": 230,
																						"helpText": "_HelpPortalAccountCreation",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 73
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 74,
													"*": {
														"ERP_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ERP_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 75,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__",
																			"SAPConfiguration__": "LabelSAPConfiguration__",
																			"LabelSAPConfiguration__": "SAPConfiguration__",
																			"GeneralMoreOptionsSAPInformations__": "LabelGeneralMoreOptionsSAPInformations__",
																			"LabelGeneralMoreOptionsSAPInformations__": "GeneralMoreOptionsSAPInformations__",
																			"ERPExportMethod__": "LabelERPExportMethod__",
																			"LabelERPExportMethod__": "ERPExportMethod__",
																			"GeneralMoreOptionsConnectionTypeInformations__": "LabelGeneralMoreOptionsConnectionTypeInformations__",
																			"LabelGeneralMoreOptionsConnectionTypeInformations__": "GeneralMoreOptionsConnectionTypeInformations__",
																			"WebserviceURL__": "LabelWebserviceURL__",
																			"LabelWebserviceURL__": "WebserviceURL__",
																			"WebserviceUser__": "LabelWebserviceUser__",
																			"LabelWebserviceUser__": "WebserviceUser__",
																			"WebservicePassword__": "LabelWebservicePassword__",
																			"LabelWebservicePassword__": "WebservicePassword__",
																			"WebserviceURLDetails__": "LabelWebserviceURLDetails__",
																			"LabelWebserviceURLDetails__": "WebserviceURLDetails__"
																		},
																		"version": 0
																	},
																	"stamp": 76,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ERP__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 1,
																						"column": 1
																					},
																					"SAPConfiguration__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSAPConfiguration__": {
																						"line": 2,
																						"column": 1
																					},
																					"GeneralMoreOptionsSAPInformations__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelGeneralMoreOptionsSAPInformations__": {
																						"line": 3,
																						"column": 1
																					},
																					"ERPExportMethod__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelERPExportMethod__": {
																						"line": 4,
																						"column": 1
																					},
																					"GeneralMoreOptionsConnectionTypeInformations__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelGeneralMoreOptionsConnectionTypeInformations__": {
																						"line": 5,
																						"column": 1
																					},
																					"WebserviceURL__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelWebserviceURL__": {
																						"line": 6,
																						"column": 1
																					},
																					"WebserviceUser__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelWebserviceUser__": {
																						"line": 7,
																						"column": 1
																					},
																					"WebservicePassword__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelWebservicePassword__": {
																						"line": 8,
																						"column": 1
																					},
																					"WebserviceURLDetails__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelWebserviceURLDetails__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 77,
																			"*": {
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0
																					},
																					"stamp": 78
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Generic",
																							"1": "_SAP",
																							"2": "_NAV",
																							"3": "_JDE",
																							"4": "_EBS",
																							"5": "_SAPS4CLOUD"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "generic",
																							"1": "SAP",
																							"2": "NAV",
																							"3": "JDE",
																							"4": "EBS",
																							"5": "SAPS4CLOUD"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": 250,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 79
																				},
																				"LabelSAPConfiguration__": {
																					"type": "Label",
																					"data": [
																						"SAPConfiguration__"
																					],
																					"options": {
																						"label": "_SAPConfiguration",
																						"version": 0
																					},
																					"stamp": 80
																				},
																				"SAPConfiguration__": {
																					"type": "ShortText",
																					"data": [
																						"SAPConfiguration__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPConfiguration",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 81
																				},
																				"LabelGeneralMoreOptionsSAPInformations__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"GeneralMoreOptionsSAPInformations__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0
																					},
																					"stamp": 83
																				},
																				"LabelERPExportMethod__": {
																					"type": "Label",
																					"data": [
																						"ERPExportMethod__"
																					],
																					"options": {
																						"label": "_ERPExportMethod",
																						"version": 0
																					},
																					"stamp": 84
																				},
																				"ERPExportMethod__": {
																					"type": "ComboBox",
																					"data": [
																						"ERPExportMethod__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SFTP",
																							"1": "_PROCESS"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SFTP",
																							"1": "PROCESS"
																						},
																						"label": "_ERPExportMethod",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 85
																				},
																				"LabelGeneralMoreOptionsConnectionTypeInformations__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 86
																				},
																				"GeneralMoreOptionsConnectionTypeInformations__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																						"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																						"version": 0
																					},
																					"stamp": 87
																				},
																				"LabelWebserviceURL__": {
																					"type": "Label",
																					"data": [
																						"WebserviceURL__"
																					],
																					"options": {
																						"label": "_WebserviceURL",
																						"version": 0
																					},
																					"stamp": 88
																				},
																				"WebserviceURL__": {
																					"type": "ShortText",
																					"data": [
																						"WebserviceURL__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_WebserviceURL",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 89
																				},
																				"LabelWebserviceUser__": {
																					"type": "Label",
																					"data": [
																						"WebserviceUser__"
																					],
																					"options": {
																						"label": "_WebserviceUser",
																						"version": 0
																					},
																					"stamp": 90
																				},
																				"WebserviceUser__": {
																					"type": "ShortText",
																					"data": [
																						"WebserviceUser__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_WebserviceUser",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 91
																				},
																				"LabelWebservicePassword__": {
																					"type": "Label",
																					"data": [
																						"WebservicePassword__"
																					],
																					"options": {
																						"label": "_WebservicePassword",
																						"version": 0
																					},
																					"stamp": 92
																				},
																				"WebservicePassword__": {
																					"type": "Password",
																					"data": [
																						"WebservicePassword__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_WebservicePassword",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 93
																				},
																				"LabelWebserviceURLDetails__": {
																					"type": "Label",
																					"data": [
																						"WebserviceURLDetails__"
																					],
																					"options": {
																						"label": "_WebserviceURLDetails",
																						"version": 0
																					},
																					"stamp": 94
																				},
																				"WebserviceURLDetails__": {
																					"type": "LongText",
																					"data": [
																						"WebserviceURLDetails__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_WebserviceURLDetails",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 6,
																						"browsable": false
																					},
																					"stamp": 95
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 96,
													"*": {
														"Recognition_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Recognition_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 97,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"AvailableDocumentCultures__": "LabelAvailableDocumentCultures__",
																			"LabelAvailableDocumentCultures__": "AvailableDocumentCultures__",
																			"OrderNumberPatterns__": "LabelOrderNumberPatterns__",
																			"LabelOrderNumberPatterns__": "OrderNumberPatterns__",
																			"GoodIssueNumberPatterns__": "LabelGoodIssueNumberPatterns__",
																			"LabelGoodIssueNumberPatterns__": "GoodIssueNumberPatterns__",
																			"AutolearningOnPOLines__": "LabelAutolearningOnPOLines__",
																			"LabelAutolearningOnPOLines__": "AutolearningOnPOLines__",
																			"ReconciliationType__": "LabelReconciliationType__",
																			"LabelReconciliationType__": "ReconciliationType__",
																			"HeaderFooterThreshold__": "LabelHeaderFooterThreshold__",
																			"LabelHeaderFooterThreshold__": "HeaderFooterThreshold__",
																			"Autolearning_Folder__": "LabelAutolearning_Folder__",
																			"LabelAutolearning_Folder__": "Autolearning_Folder__",
																			"Autolearning_FallbackOnCommonFolder__": "LabelAutolearning_FallbackOnCommonFolder__",
																			"LabelAutolearning_FallbackOnCommonFolder__": "Autolearning_FallbackOnCommonFolder__",
																			"ContractNumberPatterns__": "LabelContractNumberPatterns__",
																			"LabelContractNumberPatterns__": "ContractNumberPatterns__"
																		},
																		"version": 0
																	},
																	"stamp": 98,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AvailableDocumentCultures__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelAvailableDocumentCultures__": {
																						"line": 1,
																						"column": 1
																					},
																					"ContractNumberPatterns__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelContractNumberPatterns__": {
																						"line": 2,
																						"column": 1
																					},
																					"OrderNumberPatterns__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelOrderNumberPatterns__": {
																						"line": 3,
																						"column": 1
																					},
																					"ReconciliationType__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelReconciliationType__": {
																						"line": 4,
																						"column": 1
																					},
																					"HeaderFooterThreshold__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelHeaderFooterThreshold__": {
																						"line": 5,
																						"column": 1
																					},
																					"GoodIssueNumberPatterns__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelGoodIssueNumberPatterns__": {
																						"line": 6,
																						"column": 1
																					},
																					"AutolearningOnPOLines__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelAutolearningOnPOLines__": {
																						"line": 7,
																						"column": 1
																					},
																					"Autolearning_Folder__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelAutolearning_Folder__": {
																						"line": 8,
																						"column": 1
																					},
																					"Autolearning_FallbackOnCommonFolder__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelAutolearning_FallbackOnCommonFolder__": {
																						"line": 9,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 99,
																			"*": {
																				"LabelAvailableDocumentCultures__": {
																					"type": "Label",
																					"data": [
																						"AvailableDocumentCultures__"
																					],
																					"options": {
																						"label": "_Available document cultures",
																						"version": 0
																					},
																					"stamp": 100
																				},
																				"AvailableDocumentCultures__": {
																					"type": "ShortText",
																					"data": [
																						"AvailableDocumentCultures__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Available document cultures",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "2514",
																						"helpFormat": "HTML Format",
																						"length": 100,
																						"defaultValue": "en-US,en-GB,fr-FR",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					}
																				},
																				"LabelContractNumberPatterns__": {
																					"type": "Label",
																					"data": [
																						"ContractNumberPatterns__"
																					],
																					"options": {
																						"label": "_ContractNumberPatterns"
																					},
																					"stamp": 349
																				},
																				"ContractNumberPatterns__": {
																					"type": "LongText",
																					"data": [
																						"ContractNumberPatterns__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ContractNumberPatterns",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 350
																				},
																				"LabelOrderNumberPatterns__": {
																					"type": "Label",
																					"data": [
																						"OrderNumberPatterns__"
																					],
																					"options": {
																						"label": "_OrderNumberPatterns",
																						"version": 0
																					},
																					"stamp": 102
																				},
																				"OrderNumberPatterns__": {
																					"type": "LongText",
																					"data": [
																						"OrderNumberPatterns__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_OrderNumberPatterns",
																						"activable": true,
																						"width": 250,
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 3
																					},
																					"stamp": 103
																				},
																				"LabelReconciliationType__": {
																					"type": "Label",
																					"data": [
																						"ReconciliationType__"
																					],
																					"options": {
																						"label": "_ReconciliationType",
																						"version": 0
																					},
																					"stamp": 104
																				},
																				"ReconciliationType__": {
																					"type": "ComboBox",
																					"data": [
																						"ReconciliationType__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ReconcileByHeaderFooter",
																							"1": "_ReconcileAtLineItems"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "HeaderFooter",
																							"1": "LineItems"
																						},
																						"label": "_ReconciliationType",
																						"activable": true,
																						"width": "250",
																						"helpText": "_HelpReconciliationType",
																						"helpURL": "2542",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 105
																				},
																				"LabelHeaderFooterThreshold__": {
																					"type": "Label",
																					"data": [
																						"HeaderFooterThreshold__"
																					],
																					"options": {
																						"label": "_HeaderFooterThreshold",
																						"version": 0
																					},
																					"stamp": 106
																				},
																				"HeaderFooterThreshold__": {
																					"type": "ShortText",
																					"data": [
																						"HeaderFooterThreshold__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_HeaderFooterThreshold",
																						"activable": true,
																						"width": "250",
																						"helpText": "_HelpHeaderFooterThreshold",
																						"helpURL": "2512",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 107
																				},
																				"LabelGoodIssueNumberPatterns__": {
																					"type": "Label",
																					"data": [
																						"GoodIssueNumberPatterns__"
																					],
																					"options": {
																						"label": "_GoodIssueNumberPatterns",
																						"version": 0
																					},
																					"stamp": 108
																				},
																				"GoodIssueNumberPatterns__": {
																					"type": "LongText",
																					"data": [
																						"GoodIssueNumberPatterns__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_GoodIssueNumberPatterns",
																						"activable": true,
																						"width": 250,
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 3
																					},
																					"stamp": 109
																				},
																				"LabelAutolearningOnPOLines__": {
																					"type": "Label",
																					"data": [
																						"AutolearningOnPOLines__"
																					],
																					"options": {
																						"label": "_AutolearningOnPOLines",
																						"version": 0
																					},
																					"stamp": 110
																				},
																				"AutolearningOnPOLines__": {
																					"type": "CheckBox",
																					"data": [
																						"AutolearningOnPOLines__"
																					],
																					"options": {
																						"label": "_AutolearningOnPOLines",
																						"activable": true,
																						"width": 250,
																						"version": 0
																					},
																					"stamp": 111
																				},
																				"LabelAutolearning_Folder__": {
																					"type": "Label",
																					"data": [
																						"Autolearning_Folder__"
																					],
																					"options": {
																						"label": "_Autolearning_Folder",
																						"version": 0
																					},
																					"stamp": 112
																				},
																				"Autolearning_Folder__": {
																					"type": "ShortText",
																					"data": [
																						"Autolearning_Folder__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Autolearning_Folder",
																						"activable": true,
																						"width": "250",
																						"helpText": "_HelpAutolearning_Folder",
																						"helpURL": "2517",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"autocompletable": true
																					},
																					"stamp": 113
																				},
																				"LabelAutolearning_FallbackOnCommonFolder__": {
																					"type": "Label",
																					"data": [
																						"Autolearning_FallbackOnCommonFolder__"
																					],
																					"options": {
																						"label": "_Autolearning_FallbackOnCommonFolder",
																						"version": 0
																					},
																					"stamp": 114
																				},
																				"Autolearning_FallbackOnCommonFolder__": {
																					"type": "CheckBox",
																					"data": [
																						"Autolearning_FallbackOnCommonFolder__"
																					],
																					"options": {
																						"label": "_Autolearning_FallbackOnCommonFolder",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 115
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 116,
													"*": {
														"General_Advanced_SAP_Informations_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_General_Advanced_SAP_Informations_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 117,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"SAPDocumentTypeFIInvoice__": "LabelSAPDocumentTypeFIInvoice__",
																			"LabelSAPDocumentTypeFIInvoice__": "SAPDocumentTypeFIInvoice__",
																			"SAPDocumentTypeFICreditNote__": "LabelSAPDocumentTypeFICreditNote__",
																			"LabelSAPDocumentTypeFICreditNote__": "SAPDocumentTypeFICreditNote__",
																			"SAPDocumentTypeFIConsignmentStock__": "LabelSAPDocumentTypeFIConsignmentStock__",
																			"LabelSAPDocumentTypeFIConsignmentStock__": "SAPDocumentTypeFIConsignmentStock__",
																			"SAPDocumentTypeMMInvoice__": "LabelSAPDocumentTypeMMInvoice__",
																			"LabelSAPDocumentTypeMMInvoice__": "SAPDocumentTypeMMInvoice__",
																			"SAPDocumentTypeMMCreditNote__": "LabelSAPDocumentTypeMMCreditNote__",
																			"LabelSAPDocumentTypeMMCreditNote__": "SAPDocumentTypeMMCreditNote__",
																			"CheckSimulationProto__": "LabelCheckSimulationProto__",
																			"LabelCheckSimulationProto__": "CheckSimulationProto__"
																		},
																		"version": 0
																	},
																	"stamp": 118,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"SAPDocumentTypeFIInvoice__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSAPDocumentTypeFIInvoice__": {
																						"line": 1,
																						"column": 1
																					},
																					"SAPDocumentTypeFICreditNote__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSAPDocumentTypeFICreditNote__": {
																						"line": 2,
																						"column": 1
																					},
																					"SAPDocumentTypeFIConsignmentStock__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSAPDocumentTypeFIConsignmentStock__": {
																						"line": 3,
																						"column": 1
																					},
																					"SAPDocumentTypeMMInvoice__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSAPDocumentTypeMMInvoice__": {
																						"line": 4,
																						"column": 1
																					},
																					"SAPDocumentTypeMMCreditNote__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSAPDocumentTypeMMCreditNote__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 6,
																						"column": 1
																					},
																					"CheckSimulationProto__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelCheckSimulationProto__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 119,
																			"*": {
																				"LabelSAPDocumentTypeFIInvoice__": {
																					"type": "Label",
																					"data": [
																						"SAPDocumentTypeFIInvoice__"
																					],
																					"options": {
																						"label": "_SAPDocumentTypeFIInvoice",
																						"version": 0
																					},
																					"stamp": 120
																				},
																				"SAPDocumentTypeFIInvoice__": {
																					"type": "ShortText",
																					"data": [
																						"SAPDocumentTypeFIInvoice__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPDocumentTypeFIInvoice",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"browsable": false,
																						"autocompletable": true,
																						"defaultValue": "KR",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"LabelSAPDocumentTypeFICreditNote__": {
																					"type": "Label",
																					"data": [
																						"SAPDocumentTypeFICreditNote__"
																					],
																					"options": {
																						"label": "_SAPDocumentTypeFICreditNote",
																						"version": 0
																					},
																					"stamp": 122
																				},
																				"SAPDocumentTypeFICreditNote__": {
																					"type": "ShortText",
																					"data": [
																						"SAPDocumentTypeFICreditNote__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPDocumentTypeFICreditNote",
																						"activable": true,
																						"width": 250,
																						"version": 0,
																						"helpText": "",
																						"defaultValue": "KG",
																						"browsable": false
																					},
																					"stamp": 123
																				},
																				"LabelSAPDocumentTypeFIConsignmentStock__": {
																					"type": "Label",
																					"data": [
																						"SAPDocumentTypeFIConsignmentStock__"
																					],
																					"options": {
																						"label": "_SAPDocumentTypeFIConsignmentStock",
																						"version": 0
																					},
																					"stamp": 124
																				},
																				"SAPDocumentTypeFIConsignmentStock__": {
																					"type": "ShortText",
																					"data": [
																						"SAPDocumentTypeFIConsignmentStock__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPDocumentTypeFIConsignmentStock",
																						"activable": true,
																						"width": 250,
																						"version": 0,
																						"helpText": "",
																						"defaultValue": "RE",
																						"browsable": false
																					},
																					"stamp": 125
																				},
																				"LabelSAPDocumentTypeMMInvoice__": {
																					"type": "Label",
																					"data": [
																						"SAPDocumentTypeMMInvoice__"
																					],
																					"options": {
																						"label": "_SAPDocumentTypeMMInvoice",
																						"version": 0
																					},
																					"stamp": 126
																				},
																				"SAPDocumentTypeMMInvoice__": {
																					"type": "ShortText",
																					"data": [
																						"SAPDocumentTypeMMInvoice__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPDocumentTypeMMInvoice",
																						"activable": true,
																						"width": 250,
																						"version": 0,
																						"helpText": "",
																						"defaultValue": "RE",
																						"browsable": false
																					},
																					"stamp": 127
																				},
																				"LabelSAPDocumentTypeMMCreditNote__": {
																					"type": "Label",
																					"data": [
																						"SAPDocumentTypeMMCreditNote__"
																					],
																					"options": {
																						"label": "_SAPDocumentTypeMMCreditNote",
																						"version": 0
																					},
																					"stamp": 128
																				},
																				"SAPDocumentTypeMMCreditNote__": {
																					"type": "ShortText",
																					"data": [
																						"SAPDocumentTypeMMCreditNote__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_SAPDocumentTypeMMCreditNote",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"defaultValue": "RE",
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 129
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 130
																				},
																				"LabelCheckSimulationProto__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 131
																				},
																				"CheckSimulationProto__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Display transport requirements",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true
																						},
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"width": 250
																					},
																					"stamp": 132
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 133,
													"*": {
														"General_Advanced_ConnectionType_Informations_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "General_Advanced_ConnectionType_Informations_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 134,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ERPNotifierProcessName__": "LabelERPNotifierProcessName__",
																			"LabelERPNotifierProcessName__": "ERPNotifierProcessName__",
																			"ExportInvoiceImageFormat__": "LabelExportInvoiceImageFormat__",
																			"LabelExportInvoiceImageFormat__": "ExportInvoiceImageFormat__"
																		},
																		"version": 0
																	},
																	"stamp": 135,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ERPNotifierProcessName__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelERPNotifierProcessName__": {
																						"line": 1,
																						"column": 1
																					},
																					"ExportInvoiceImageFormat__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelExportInvoiceImageFormat__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 136,
																			"*": {
																				"LabelERPNotifierProcessName__": {
																					"type": "Label",
																					"data": [
																						"ERPNotifierProcessName__"
																					],
																					"options": {
																						"label": "_ERPNotifierProcessName",
																						"version": 0
																					},
																					"stamp": 137
																				},
																				"ERPNotifierProcessName__": {
																					"type": "ShortText",
																					"data": [
																						"ERPNotifierProcessName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ERPNotifierProcessName",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 138
																				},
																				"LabelExportInvoiceImageFormat__": {
																					"type": "Label",
																					"data": [
																						"ExportInvoiceImageFormat__"
																					],
																					"options": {
																						"label": "_ExportInvoiceImageFormat",
																						"version": 0
																					},
																					"stamp": 139
																				},
																				"ExportInvoiceImageFormat__": {
																					"type": "ComboBox",
																					"data": [
																						"ExportInvoiceImageFormat__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_No",
																							"1": "_Yes as TIFF",
																							"2": "_Yes as PDF"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "",
																							"1": "tif",
																							"2": "pdf"
																						},
																						"label": "_ExportInvoiceImageFormat",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 140
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 141,
													"*": {
														"Verification_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Verification_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 142,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"VerificationCheckAttachmentSignature__": "LabelVerificationCheckAttachmentSignature__",
																			"LabelVerificationCheckAttachmentSignature__": "VerificationCheckAttachmentSignature__",
																			"VerificationPOMatchingMode__": "LabelVerificationPOMatchingMode__",
																			"LabelVerificationPOMatchingMode__": "VerificationPOMatchingMode__",
																			"AutomaticallyForwardNonPoInvoiceToReviewer__": "LabelAutomaticallyForwardNonPoInvoiceToReviewer__",
																			"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": "AutomaticallyForwardNonPoInvoiceToReviewer__",
																			"SAPDuplicateCheck__": "LabelSAPDuplicateCheck__",
																			"LabelSAPDuplicateCheck__": "SAPDuplicateCheck__"
																		},
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VerificationCheckAttachmentSignature__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelVerificationCheckAttachmentSignature__": {
																						"line": 1,
																						"column": 1
																					},
																					"VerificationPOMatchingMode__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelVerificationPOMatchingMode__": {
																						"line": 2,
																						"column": 1
																					},
																					"AutomaticallyForwardNonPoInvoiceToReviewer__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": {
																						"line": 3,
																						"column": 1
																					},
																					"SAPDuplicateCheck__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSAPDuplicateCheck__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 144,
																			"*": {
																				"LabelVerificationCheckAttachmentSignature__": {
																					"type": "Label",
																					"data": [
																						"VerificationCheckAttachmentSignature__"
																					],
																					"options": {
																						"label": "_VerificationCheckAttachmentSignature",
																						"version": 0
																					},
																					"stamp": 145
																				},
																				"VerificationCheckAttachmentSignature__": {
																					"type": "CheckBox",
																					"data": [
																						"VerificationCheckAttachmentSignature__"
																					],
																					"options": {
																						"label": "_VerificationCheckAttachmentSignature",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 146
																				},
																				"LabelVerificationPOMatchingMode__": {
																					"type": "Label",
																					"data": [
																						"VerificationPOMatchingMode__"
																					],
																					"options": {
																						"label": "_VerificationPOMatchingMode",
																						"version": 0
																					},
																					"stamp": 147
																				},
																				"VerificationPOMatchingMode__": {
																					"type": "ComboBox",
																					"data": [
																						"VerificationPOMatchingMode__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_PO based",
																							"1": "_GR based",
																							"2": "_Line item based"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "PO",
																							"1": "GR",
																							"2": "LineItem"
																						},
																						"label": "_VerificationPOMatchingMode",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 148
																				},
																				"LabelAutomaticallyForwardNonPoInvoiceToReviewer__": {
																					"type": "Label",
																					"data": [
																						"AutomaticallyForwardNonPoInvoiceToReviewer__"
																					],
																					"options": {
																						"label": "_AutomaticallyForwardNonPoInvoiceToReviewer",
																						"version": 0
																					},
																					"stamp": 149
																				},
																				"AutomaticallyForwardNonPoInvoiceToReviewer__": {
																					"type": "CheckBox",
																					"data": [
																						"AutomaticallyForwardNonPoInvoiceToReviewer__"
																					],
																					"options": {
																						"label": "_AutomaticallyForwardNonPoInvoiceToReviewer",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 150
																				},
																				"LabelSAPDuplicateCheck__": {
																					"type": "Label",
																					"data": [
																						"SAPDuplicateCheck__"
																					],
																					"options": {
																						"label": "_SAPDuplicateCheck",
																						"version": 0
																					},
																					"stamp": 151
																				},
																				"SAPDuplicateCheck__": {
																					"type": "CheckBox",
																					"data": [
																						"SAPDuplicateCheck__"
																					],
																					"options": {
																						"label": "_SAPDuplicateCheck",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 152
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 153,
													"*": {
														"Coding_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Coding_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 154,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CodingEnableCostCenter__": "LabelCodingEnableCostCenter__",
																			"LabelCodingEnableCostCenter__": "CodingEnableCostCenter__",
																			"CodingEnableGLAccount__": "LabelCodingEnableGLAccount__",
																			"LabelCodingEnableGLAccount__": "CodingEnableGLAccount__",
																			"CodingEnableWBSElement__": "LabelCodingEnableWBSElement__",
																			"LabelCodingEnableWBSElement__": "CodingEnableWBSElement__",
																			"CodingEnableBusinessArea__": "LabelCodingEnableBusinessArea__",
																			"LabelCodingEnableBusinessArea__": "CodingEnableBusinessArea__",
																			"CodingEnableAssignments__": "LabelCodingEnableAssignments__",
																			"LabelCodingEnableAssignments__": "CodingEnableAssignments__",
																			"CodingEnableInternalOrder__": "LabelCodingEnableInternalOrder__",
																			"LabelCodingEnableInternalOrder__": "CodingEnableInternalOrder__",
																			"CodingEnableTradingPartner__": "LabelCodingEnableTradingPartner__",
																			"LabelCodingEnableTradingPartner__": "CodingEnableTradingPartner__",
																			"CodingEnableCompanyCode__": "LabelCodingEnableCompanyCode__",
																			"LabelCodingEnableCompanyCode__": "CodingEnableCompanyCode__",
																			"CodingEnableProjectCode__": "LabelCodingEnableProjectCode__",
																			"LabelCodingEnableProjectCode__": "CodingEnableProjectCode__"
																		},
																		"version": 0
																	},
																	"stamp": 155,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CodingEnableCostCenter__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelCodingEnableCostCenter__": {
																						"line": 2,
																						"column": 1
																					},
																					"CodingEnableGLAccount__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelCodingEnableGLAccount__": {
																						"line": 3,
																						"column": 1
																					},
																					"CodingEnableWBSElement__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelCodingEnableWBSElement__": {
																						"line": 5,
																						"column": 1
																					},
																					"CodingEnableBusinessArea__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCodingEnableBusinessArea__": {
																						"line": 6,
																						"column": 1
																					},
																					"CodingEnableAssignments__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelCodingEnableAssignments__": {
																						"line": 7,
																						"column": 1
																					},
																					"CodingEnableInternalOrder__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelCodingEnableInternalOrder__": {
																						"line": 8,
																						"column": 1
																					},
																					"Coding_Description__": {
																						"line": 1,
																						"column": 1
																					},
																					"CodingEnableTradingPartner__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelCodingEnableTradingPartner__": {
																						"line": 9,
																						"column": 1
																					},
																					"CodingEnableCompanyCode__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelCodingEnableCompanyCode__": {
																						"line": 4,
																						"column": 1
																					},
																					"CodingEnableProjectCode__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelCodingEnableProjectCode__": {
																						"line": 10,
																						"column": 1
																					}
																				},
																				"lines": 10,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 156,
																			"*": {
																				"Coding_Description__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_Coding_Description",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 157
																				},
																				"LabelCodingEnableCostCenter__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableCostCenter__"
																					],
																					"options": {
																						"label": "_CodingEnableCostCenter",
																						"version": 0
																					},
																					"stamp": 158
																				},
																				"CodingEnableCostCenter__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableCostCenter__"
																					],
																					"options": {
																						"label": "_CodingEnableCostCenter",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"defaultValue": true,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 159
																				},
																				"LabelCodingEnableGLAccount__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableGLAccount__"
																					],
																					"options": {
																						"label": "_CodingEnableGLAccount",
																						"version": 0
																					},
																					"stamp": 160
																				},
																				"CodingEnableGLAccount__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableGLAccount__"
																					],
																					"options": {
																						"label": "_CodingEnableGLAccount",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"defaultValue": true,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 161
																				},
																				"LabelCodingEnableCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableCompanyCode__"
																					],
																					"options": {
																						"label": "_CodingEnableCompanyCode",
																						"version": 0
																					},
																					"stamp": 162
																				},
																				"CodingEnableCompanyCode__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableCompanyCode__"
																					],
																					"options": {
																						"label": "_CodingEnableCompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 163
																				},
																				"LabelCodingEnableWBSElement__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableWBSElement__"
																					],
																					"options": {
																						"label": "_CodingEnableWBSElement",
																						"version": 0
																					},
																					"stamp": 164
																				},
																				"CodingEnableWBSElement__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableWBSElement__"
																					],
																					"options": {
																						"label": "_CodingEnableWBSElement",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 165
																				},
																				"LabelCodingEnableBusinessArea__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableBusinessArea__"
																					],
																					"options": {
																						"label": "_CodingEnableBusinessArea",
																						"version": 0
																					},
																					"stamp": 166
																				},
																				"CodingEnableBusinessArea__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableBusinessArea__"
																					],
																					"options": {
																						"label": "_CodingEnableBusinessArea",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 167
																				},
																				"LabelCodingEnableAssignments__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableAssignments__"
																					],
																					"options": {
																						"label": "_CodingEnableAssignments",
																						"version": 0
																					},
																					"stamp": 168
																				},
																				"CodingEnableAssignments__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableAssignments__"
																					],
																					"options": {
																						"label": "_CodingEnableAssignments",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"LabelCodingEnableInternalOrder__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableInternalOrder__"
																					],
																					"options": {
																						"label": "_CodingEnableInternalOrder",
																						"version": 0
																					},
																					"stamp": 170
																				},
																				"CodingEnableInternalOrder__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableInternalOrder__"
																					],
																					"options": {
																						"label": "_CodingEnableInternalOrder",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 171
																				},
																				"LabelCodingEnableTradingPartner__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableTradingPartner__"
																					],
																					"options": {
																						"label": "_CodingEnableTradingPartner",
																						"version": 0
																					},
																					"stamp": 172
																				},
																				"CodingEnableTradingPartner__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableTradingPartner__"
																					],
																					"options": {
																						"label": "_CodingEnableTradingPartner",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 173
																				},
																				"LabelCodingEnableProjectCode__": {
																					"type": "Label",
																					"data": [
																						"CodingEnableProjectCode__"
																					],
																					"options": {
																						"label": "_CodingEnableProjectCode",
																						"version": 0
																					},
																					"stamp": 174
																				},
																				"CodingEnableProjectCode__": {
																					"type": "CheckBox",
																					"data": [
																						"CodingEnableProjectCode__"
																					],
																					"options": {
																						"label": "_CodingEnableProjectCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 175
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 176,
													"*": {
														"Workflow_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payment approval workflow",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 177,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"WorkflowReviewersCanModifyCostCenter__": "LabelWorkflowReviewersCanModifyCostCenter__",
																			"LabelWorkflowReviewersCanModifyCostCenter__": "WorkflowReviewersCanModifyCostCenter__",
																			"WorkflowReviewersCanModifyGLAccount__": "LabelWorkflowReviewersCanModifyGLAccount__",
																			"LabelWorkflowReviewersCanModifyGLAccount__": "WorkflowReviewersCanModifyGLAccount__",
																			"WorkflowReviewersCanModifyBusinessArea__": "LabelWorkflowReviewersCanModifyBusinessArea__",
																			"LabelWorkflowReviewersCanModifyBusinessArea__": "WorkflowReviewersCanModifyBusinessArea__",
																			"WorkflowReviewersCanModifyAssignments__": "LabelWorkflowReviewersCanModifyAssignments__",
																			"LabelWorkflowReviewersCanModifyAssignments__": "WorkflowReviewersCanModifyAssignments__",
																			"WorkflowReviewersCanModifyInternalOrder__": "LabelWorkflowReviewersCanModifyInternalOrder__",
																			"LabelWorkflowReviewersCanModifyInternalOrder__": "WorkflowReviewersCanModifyInternalOrder__",
																			"WorkflowReviewersCanModifyWBSElement__": "LabelWorkflowReviewersCanModifyWBSElement__",
																			"LabelWorkflowReviewersCanModifyWBSElement__": "WorkflowReviewersCanModifyWBSElement__",
																			"LabelWorkflowAutoPostAfterReview__": "WorkflowAutoPostAfterReview__",
																			"WorkflowAutoPostAfterReview__": "LabelWorkflowAutoPostAfterReview__",
																			"WorkflowReviewersCanModifyTradingPartner__": "LabelWorkflowReviewersCanModifyTradingPartner__",
																			"LabelWorkflowReviewersCanModifyTradingPartner__": "WorkflowReviewersCanModifyTradingPartner__",
																			"WorkflowReviewersCanAddLineItems__": "LabelWorkflowReviewersCanAddLineItems__",
																			"LabelWorkflowReviewersCanAddLineItems__": "WorkflowReviewersCanAddLineItems__",
																			"WorkflowEnableNewInvoiceNotifications__": "LabelWorkflowEnableNewInvoiceNotifications__",
																			"LabelWorkflowEnableNewInvoiceNotifications__": "WorkflowEnableNewInvoiceNotifications__",
																			"WorkflowReviewersCanModifyCompanyCode__": "LabelWorkflowReviewersCanModifyCompanyCode__",
																			"LabelWorkflowReviewersCanModifyCompanyCode__": "WorkflowReviewersCanModifyCompanyCode__",
																			"SendNotificationsToEachGroupMembers__": "LabelSendNotificationsToEachGroupMembers__",
																			"LabelSendNotificationsToEachGroupMembers__": "SendNotificationsToEachGroupMembers__",
																			"WorkflowReviewersCanModifyProjectCode__": "LabelWorkflowReviewersCanModifyProjectCode__",
																			"LabelWorkflowReviewersCanModifyProjectCode__": "WorkflowReviewersCanModifyProjectCode__"
																		},
																		"version": 0
																	},
																	"stamp": 178,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"WorkflowReviewersCanModifyCostCenter__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyCostCenter__": {
																						"line": 3,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyGLAccount__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyGLAccount__": {
																						"line": 4,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyBusinessArea__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyBusinessArea__": {
																						"line": 7,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyAssignments__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyAssignments__": {
																						"line": 8,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyInternalOrder__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyInternalOrder__": {
																						"line": 9,
																						"column": 1
																					},
																					"WorkflowAllowReviewersModifyDescription__": {
																						"line": 2,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyWBSElement__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyWBSElement__": {
																						"line": 6,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelWorkflowAutoPostAfterReview__": {
																						"line": 15,
																						"column": 1
																					},
																					"WorkflowAutoPostAfterReview__": {
																						"line": 15,
																						"column": 2
																					},
																					"Spacer_line6__": {
																						"line": 13,
																						"column": 1
																					},
																					"WorkflowOptionsDescription__": {
																						"line": 14,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyTradingPartner__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyTradingPartner__": {
																						"line": 10,
																						"column": 1
																					},
																					"WorkflowReviewersCanAddLineItems__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanAddLineItems__": {
																						"line": 12,
																						"column": 1
																					},
																					"WorkflowEnableNewInvoiceNotifications__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelWorkflowEnableNewInvoiceNotifications__": {
																						"line": 16,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyCompanyCode__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyCompanyCode__": {
																						"line": 5,
																						"column": 1
																					},
																					"SendNotificationsToEachGroupMembers__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelSendNotificationsToEachGroupMembers__": {
																						"line": 17,
																						"column": 1
																					},
																					"WorkflowReviewersCanModifyProjectCode__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelWorkflowReviewersCanModifyProjectCode__": {
																						"line": 11,
																						"column": 1
																					}
																				},
																				"lines": 17,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 179,
																			"*": {
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 180
																				},
																				"WorkflowAllowReviewersModifyDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_WorkflowAllowReviewersModifyDescription",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 181
																				},
																				"LabelWorkflowReviewersCanModifyCostCenter__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyCostCenter__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyCostCenter",
																						"version": 0
																					},
																					"stamp": 182
																				},
																				"WorkflowReviewersCanModifyCostCenter__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyCostCenter__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyCostCenter",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 183
																				},
																				"LabelWorkflowReviewersCanModifyGLAccount__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyGLAccount__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyGLAccount",
																						"version": 0
																					},
																					"stamp": 184
																				},
																				"WorkflowReviewersCanModifyGLAccount__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyGLAccount__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyGLAccount",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 185
																				},
																				"LabelWorkflowReviewersCanModifyCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyCompanyCode__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyCompanyCode",
																						"version": 0
																					},
																					"stamp": 186
																				},
																				"WorkflowReviewersCanModifyCompanyCode__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyCompanyCode__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyCompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 187
																				},
																				"LabelWorkflowReviewersCanModifyWBSElement__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyWBSElement__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyWBSElement",
																						"version": 0
																					},
																					"stamp": 188
																				},
																				"WorkflowReviewersCanModifyWBSElement__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyWBSElement__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyWBSElement",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 189
																				},
																				"LabelWorkflowReviewersCanModifyBusinessArea__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyBusinessArea__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyBusinessArea",
																						"version": 0
																					},
																					"stamp": 190
																				},
																				"WorkflowReviewersCanModifyBusinessArea__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyBusinessArea__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyBusinessArea",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 191
																				},
																				"LabelWorkflowReviewersCanModifyAssignments__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyAssignments__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyAssignments",
																						"version": 0
																					},
																					"stamp": 192
																				},
																				"WorkflowReviewersCanModifyAssignments__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyAssignments__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyAssignments",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 193
																				},
																				"LabelWorkflowReviewersCanModifyInternalOrder__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyInternalOrder__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyInternalOrder",
																						"version": 0
																					},
																					"stamp": 194
																				},
																				"WorkflowReviewersCanModifyInternalOrder__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyInternalOrder__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyInternalOrder",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 195
																				},
																				"LabelWorkflowReviewersCanModifyTradingPartner__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyTradingPartner__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyTradingPartner",
																						"version": 0
																					},
																					"stamp": 196
																				},
																				"WorkflowReviewersCanModifyTradingPartner__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyTradingPartner__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyTradingPartner",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 197
																				},
																				"LabelWorkflowReviewersCanModifyProjectCode__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanModifyProjectCode__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyProjectCode",
																						"version": 0
																					},
																					"stamp": 198
																				},
																				"WorkflowReviewersCanModifyProjectCode__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanModifyProjectCode__"
																					],
																					"options": {
																						"label": "_WorkflowReviewersCanModifyProjectCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 199
																				},
																				"LabelWorkflowReviewersCanAddLineItems__": {
																					"type": "Label",
																					"data": [
																						"WorkflowReviewersCanAddLineItems__"
																					],
																					"options": {
																						"label": "_Reviewers can also add/remove line items",
																						"version": 0
																					},
																					"stamp": 200
																				},
																				"WorkflowReviewersCanAddLineItems__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowReviewersCanAddLineItems__"
																					],
																					"options": {
																						"label": "_Reviewers can also add/remove line items",
																						"activable": true,
																						"width": 20,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 201
																				},
																				"Spacer_line6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line6",
																						"version": 0
																					},
																					"stamp": 202
																				},
																				"WorkflowOptionsDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_WorkflowOptionsDescription",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 203
																				},
																				"LabelWorkflowAutoPostAfterReview__": {
																					"type": "Label",
																					"data": [
																						"WorkflowAutoPostAfterReview__"
																					],
																					"options": {
																						"label": "_Auto post after review",
																						"version": 0
																					},
																					"stamp": 204
																				},
																				"WorkflowAutoPostAfterReview__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowAutoPostAfterReview__"
																					],
																					"options": {
																						"label": "_Auto post after review",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"version": 0
																					},
																					"stamp": 205
																				},
																				"LabelWorkflowEnableNewInvoiceNotifications__": {
																					"type": "Label",
																					"data": [
																						"WorkflowEnableNewInvoiceNotifications__"
																					],
																					"options": {
																						"label": "_WorkflowEnableNewInvoiceNotifications",
																						"version": 0
																					},
																					"stamp": 206
																				},
																				"WorkflowEnableNewInvoiceNotifications__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowEnableNewInvoiceNotifications__"
																					],
																					"options": {
																						"label": "_WorkflowEnableNewInvoiceNotifications",
																						"activable": true,
																						"width": 20,
																						"helpText": "_InfoWorkflowEnableNewInvoiceNotifications",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 207
																				},
																				"LabelSendNotificationsToEachGroupMembers__": {
																					"type": "Label",
																					"data": [
																						"SendNotificationsToEachGroupMembers__"
																					],
																					"options": {
																						"label": "_SendNotificationsToEachGroupMembers",
																						"version": 0
																					},
																					"stamp": 208
																				},
																				"SendNotificationsToEachGroupMembers__": {
																					"type": "CheckBox",
																					"data": [
																						"SendNotificationsToEachGroupMembers__"
																					],
																					"options": {
																						"label": "_SendNotificationsToEachGroupMembers",
																						"activable": true,
																						"width": "20",
																						"helpText": "_infoSendNotificationsToEachGroupMembers",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 209
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 210,
													"*": {
														"AutoPost": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "AutoPost",
																"leftImageURL": "",
																"removeMargins": false,
																"hidden": true,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 211,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 212,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {},
																				"lines": 0,
																				"columns": 2,
																				"colspans": [],
																				"version": 0
																			},
																			"stamp": 213,
																			"*": {}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 214,
													"*": {
														"Labs_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Labs_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 215,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"EnableConsignmentStock__": "LabelEnableConsignmentStock__",
																			"LabelEnableConsignmentStock__": "EnableConsignmentStock__",
																			"EnableAIForExpenses__": "LabelEnableAIForExpenses__",
																			"LabelEnableAIForExpenses__": "EnableAIForExpenses__",
																			"EnableSynergyNeuralNetworkGlobalSetting__": "LabelEnableSynergyNeuralNetworkGlobalSetting__",
																			"LabelEnableSynergyNeuralNetworkGlobalSetting__": "EnableSynergyNeuralNetworkGlobalSetting__",
																			"EnableTouchlessForNonPoInvoice__": "LabelEnableTouchlessForNonPoInvoice__",
																			"LabelEnableTouchlessForNonPoInvoice__": "EnableTouchlessForNonPoInvoice__",
																			"DisableTeaching__": "LabelDisableTeaching__",
																			"LabelDisableTeaching__": "DisableTeaching__",
																			"FTRVendorOnIdentifiersOnly__": "LabelFTRVendorOnIdentifiersOnly__",
																			"LabelFTRVendorOnIdentifiersOnly__": "FTRVendorOnIdentifiersOnly__",
																			"WorkflowDisableRules__": "LabelWorkflowDisableRules__",
																			"LabelWorkflowDisableRules__": "WorkflowDisableRules__",
																			"ocrEngine__": "LabelocrEngine__",
																			"LabelocrEngine__": "ocrEngine__",
																			"GLAccountDeterminationFrench__": "LabelGLAccountDeterminationFrench__",
																			"LabelGLAccountDeterminationFrench__": "GLAccountDeterminationFrench__",
																			"EnablePunchoutV2__": "LabelEnablePunchoutV2__",
																			"LabelEnablePunchoutV2__": "EnablePunchoutV2__",
																			"EnableItemRating__": "LabelEnableItemRating__",
																			"LabelEnableItemRating__": "EnableItemRating__",
																			"AnomalyDetectionForAP__": "LabelAnomalyDetectionForAP__",
																			"LabelAnomalyDetectionForAP__": "AnomalyDetectionForAP__",
																			"EnableVendorModifyPO__": "LabelEnableVendorModifyPO__",
																			"LabelEnableVendorModifyPO__": "EnableVendorModifyPO__",
																			"EnableLowBudgetNotification__": "LabelEnableLowBudgetNotification__",
																			"LabelEnableLowBudgetNotification__": "EnableLowBudgetNotification__",
																			"EnableInventoryManagement__": "LabelEnableInventoryManagement__",
																			"LabelEnableInventoryManagement__": "EnableInventoryManagement__",
																			"EnableServiceBasedItem__": "LabelEnableServiceBasedItem__",
																			"LabelEnableServiceBasedItem__": "EnableServiceBasedItem__",
																			"APCodingsPrediction__": "LabelAPCodingsPrediction__",
																			"LabelAPCodingsPrediction__": "APCodingsPrediction__",
																			"EnableAdvancedShippingNotice__": "LabelEnableAdvancedShippingNotice__",
																			"LabelEnableAdvancedShippingNotice__": "EnableAdvancedShippingNotice__"
																		},
																		"version": 0
																	},
																	"stamp": 216,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 19,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"EnableConsignmentStock__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelEnableConsignmentStock__": {
																						"line": 1,
																						"column": 1
																					},
																					"EnableTouchlessForNonPoInvoice__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEnableTouchlessForNonPoInvoice__": {
																						"line": 2,
																						"column": 1
																					},
																					"DisableTeaching__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDisableTeaching__": {
																						"line": 3,
																						"column": 1
																					},
																					"FTRVendorOnIdentifiersOnly__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelFTRVendorOnIdentifiersOnly__": {
																						"line": 4,
																						"column": 1
																					},
																					"WorkflowDisableRules__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelWorkflowDisableRules__": {
																						"line": 5,
																						"column": 1
																					},
																					"EnableItemRating__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelEnableItemRating__": {
																						"line": 6,
																						"column": 1
																					},
																					"EnableVendorModifyPO__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelEnableVendorModifyPO__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 9,
																						"column": 1
																					},
																					"EnableAIForExpenses__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelEnableAIForExpenses__": {
																						"line": 10,
																						"column": 1
																					},
																					"ocrEngine__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelocrEngine__": {
																						"line": 11,
																						"column": 1
																					},
																					"EnableSynergyNeuralNetworkGlobalSetting__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelEnableSynergyNeuralNetworkGlobalSetting__": {
																						"line": 12,
																						"column": 1
																					},
																					"GLAccountDeterminationFrench__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelGLAccountDeterminationFrench__": {
																						"line": 13,
																						"column": 1
																					},
																					"AnomalyDetectionForAP__": {
																						"line": 14,
																						"column": 2
																					},
																					"LabelAnomalyDetectionForAP__": {
																						"line": 14,
																						"column": 1
																					},
																					"EnablePunchoutV2__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelEnablePunchoutV2__": {
																						"line": 15,
																						"column": 1
																					},
																					"APCodingsPrediction__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelAPCodingsPrediction__": {
																						"line": 16,
																						"column": 1
																					},
																					"EnableLowBudgetNotification__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelEnableLowBudgetNotification__": {
																						"line": 17,
																						"column": 1
																					},
																					"EnableServiceBasedItem__": {
																						"line": 18,
																						"column": 2
																					},
																					"LabelEnableServiceBasedItem__": {
																						"line": 18,
																						"column": 1
																					},
																					"EnableInventoryManagement__": {
																						"line": 19,
																						"column": 2
																					},
																					"LabelEnableInventoryManagement__": {
																						"line": 19,
																						"column": 1
																					},
																					"EnableAdvancedShippingNotice__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelEnableAdvancedShippingNotice__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"stamp": 217,
																			"*": {
																				"LabelEnableConsignmentStock__": {
																					"type": "Label",
																					"data": [
																						"EnableConsignmentStock__"
																					],
																					"options": {
																						"label": "_EnableConsignmentStock",
																						"version": 0
																					},
																					"stamp": 218
																				},
																				"EnableConsignmentStock__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableConsignmentStock__"
																					],
																					"options": {
																						"label": "_EnableConsignmentStock",
																						"activable": true,
																						"width": "250",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 219
																				},
																				"LabelEnableTouchlessForNonPoInvoice__": {
																					"type": "Label",
																					"data": [
																						"EnableTouchlessForNonPoInvoice__"
																					],
																					"options": {
																						"label": "_Enable touchless for Non PO Invoices",
																						"version": 0
																					},
																					"stamp": 220
																				},
																				"EnableTouchlessForNonPoInvoice__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableTouchlessForNonPoInvoice__"
																					],
																					"options": {
																						"label": "_Enable touchless for Non PO Invoices",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 221
																				},
																				"LabelDisableTeaching__": {
																					"type": "Label",
																					"data": [
																						"DisableTeaching__"
																					],
																					"options": {
																						"label": "_Disable Teaching",
																						"version": 0
																					},
																					"stamp": 222
																				},
																				"DisableTeaching__": {
																					"type": "CheckBox",
																					"data": [
																						"DisableTeaching__"
																					],
																					"options": {
																						"label": "_Disable Teaching",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 223
																				},
																				"LabelFTRVendorOnIdentifiersOnly__": {
																					"type": "Label",
																					"data": [
																						"FTRVendorOnIdentifiersOnly__"
																					],
																					"options": {
																						"label": "_FTR Vendor on identifiers only",
																						"version": 0
																					},
																					"stamp": 224
																				},
																				"FTRVendorOnIdentifiersOnly__": {
																					"type": "CheckBox",
																					"data": [
																						"FTRVendorOnIdentifiersOnly__"
																					],
																					"options": {
																						"label": "_FTR Vendor on identifiers only",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "_If enabled, only uses Uniquer identifiers for the vendore recognition algorithm (such as IBAN, VAT, Phone number), in other words the recognition by address bloc is deactivated.",
																						"helpURL": "2007",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 225
																				},
																				"LabelWorkflowDisableRules__": {
																					"type": "Label",
																					"data": [
																						"WorkflowDisableRules__"
																					],
																					"options": {
																						"label": "_WorkflowDisableRules",
																						"version": 0
																					},
																					"stamp": 226
																				},
																				"WorkflowDisableRules__": {
																					"type": "CheckBox",
																					"data": [
																						"WorkflowDisableRules__"
																					],
																					"options": {
																						"label": "_WorkflowDisableRules",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 227
																				},
																				"LabelEnableItemRating__": {
																					"type": "Label",
																					"data": [
																						"EnableItemRating__"
																					],
																					"options": {
																						"label": "_EnableItemRating"
																					},
																					"stamp": 228
																				},
																				"EnableItemRating__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableItemRating__"
																					],
																					"options": {
																						"label": "_EnableItemRating",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 229
																				},
																				"LabelEnableVendorModifyPO__": {
																					"type": "Label",
																					"data": [
																						"EnableVendorModifyPO__"
																					],
																					"options": {
																						"label": "_EnableVendorModifyPO",
																						"version": 0
																					},
																					"stamp": 230
																				},
																				"EnableVendorModifyPO__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableVendorModifyPO__"
																					],
																					"options": {
																						"label": "_EnableVendorModifyPO",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 231
																				},
																				"LabelEnableAdvancedShippingNotice__": {
																					"type": "Label",
																					"data": [
																						"EnableAdvancedShippingNotice__"
																					],
																					"options": {
																						"label": "_EnableAdvancedShippingNotice"
																					},
																					"stamp": 349
																				},
																				"EnableAdvancedShippingNotice__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableAdvancedShippingNotice__"
																					],
																					"options": {
																						"label": "_EnableAdvancedShippingNotice",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"possibleValues": {},
																						"possibleKeys": {}
																					},
																					"stamp": 350
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "1",
																						"width": "",
																						"color": "color4",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 232
																				},
																				"EnableAIForExpenses__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableAIForExpenses__"
																					],
																					"options": {
																						"label": "_Enable AI for Expenses (global setting)",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 233
																				},
																				"LabelEnableAIForExpenses__": {
																					"type": "Label",
																					"data": [
																						"EnableAIForExpenses__"
																					],
																					"options": {
																						"label": "_Enable AI for Expenses (global setting)",
																						"version": 0
																					},
																					"stamp": 234
																				},
																				"LabelocrEngine__": {
																					"type": "Label",
																					"data": [
																						"ocrEngine__"
																					],
																					"options": {
																						"label": "_ocrEngine",
																						"version": 0
																					},
																					"stamp": 235
																				},
																				"ocrEngine__": {
																					"type": "ComboBox",
																					"data": [
																						"ocrEngine__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ServerSideOCR",
																							"1": "_LocalDeviceOCR"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Server side OCR",
																							"1": "Local device OCR"
																						},
																						"label": "_ocrEngine",
																						"activable": true,
																						"width": 230,
																						"version": 1,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 236
																				},
																				"LabelEnableSynergyNeuralNetworkGlobalSetting__": {
																					"type": "Label",
																					"data": [
																						"EnableSynergyNeuralNetworkGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable 'Synergy Neural Network' (global setting)",
																						"version": 0
																					},
																					"stamp": 237
																				},
																				"EnableSynergyNeuralNetworkGlobalSetting__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableSynergyNeuralNetworkGlobalSetting__"
																					],
																					"options": {
																						"label": "_Enable 'Synergy Neural Network' (global setting)",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 238
																				},
																				"LabelGLAccountDeterminationFrench__": {
																					"type": "Label",
																					"data": [
																						"GLAccountDeterminationFrench__"
																					],
																					"options": {
																						"label": "_GLAccountDeterminationFrench",
																						"version": 0
																					},
																					"stamp": 239
																				},
																				"GLAccountDeterminationFrench__": {
																					"type": "CheckBox",
																					"data": [
																						"GLAccountDeterminationFrench__"
																					],
																					"options": {
																						"label": "_GLAccountDeterminationFrench",
																						"activable": true,
																						"helpText": "_GLAccountDeterminationFrench_Help",
																						"helpURL": "1019",
																						"helpFormat": "HTML Format",
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 240
																				},
																				"LabelEnablePunchoutV2__": {
																					"type": "Label",
																					"data": [
																						"EnablePunchoutV2__"
																					],
																					"options": {
																						"label": "_EnablePunchoutV2",
																						"version": 0
																					},
																					"stamp": 241
																				},
																				"EnablePunchoutV2__": {
																					"type": "CheckBox",
																					"data": [
																						"EnablePunchoutV2__"
																					],
																					"options": {
																						"label": "_EnablePunchoutV2",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 242
																				},
																				"AnomalyDetectionForAP__": {
																					"type": "CheckBox",
																					"data": [
																						"AnomalyDetectionForAP__"
																					],
																					"options": {
																						"label": "_AnomalyDetectionForAP",
																						"activable": true,
																						"width": 230,
																						"helpText": "_AnomalyDetectionForAP_Help",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 243
																				},
																				"LabelEnableLowBudgetNotification__": {
																					"type": "Label",
																					"data": [
																						"EnableLowBudgetNotification__"
																					],
																					"options": {
																						"label": "_EnableLowBudgetNotification",
																						"version": 0
																					},
																					"stamp": 244
																				},
																				"EnableLowBudgetNotification__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableLowBudgetNotification__"
																					],
																					"options": {
																						"label": "_EnableLowBudgetNotification",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "_EnableLowBudgetNotification_help",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 245
																				},
																				"LabelAnomalyDetectionForAP__": {
																					"type": "Label",
																					"data": [
																						"AnomalyDetectionForAP__"
																					],
																					"options": {
																						"label": "_AnomalyDetectionForAP",
																						"version": 0
																					},
																					"stamp": 246
																				},
																				"EnableServiceBasedItem__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableServiceBasedItem__"
																					],
																					"options": {
																						"label": "_EnableServiceBasedItem",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 247
																				},
																				"LabelEnableServiceBasedItem__": {
																					"type": "Label",
																					"data": [
																						"EnableServiceBasedItem__"
																					],
																					"options": {
																						"label": "_EnableServiceBasedItem",
																						"version": 0
																					},
																					"stamp": 248
																				},
																				"LabelAPCodingsPrediction__": {
																					"type": "Label",
																					"data": [
																						"APCodingsPrediction__"
																					],
																					"options": {
																						"label": "_APCodingsPrediction"
																					},
																					"stamp": 348
																				},
																				"APCodingsPrediction__": {
																					"type": "CheckBox",
																					"data": [
																						"APCodingsPrediction__"
																					],
																					"options": {
																						"label": "_APCodingsPrediction",
																						"activable": true,
																						"width": 230,
																						"helpText": "_APCodingsPrediction_Help",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 249
																				},
																				"LabelEnableInventoryManagement__": {
																					"type": "Label",
																					"data": [
																						"EnableInventoryManagement__"
																					],
																					"options": {
																						"label": "_EnableInventoryManagement",
																						"version": 0
																					},
																					"stamp": 250
																				},
																				"EnableInventoryManagement__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableInventoryManagement__"
																					],
																					"options": {
																						"label": "_EnableInventoryManagement",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 251
																				}
																			}
																		}
																	},
																	"data": []
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 252,
													"*": {
														"Archiving_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 500,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Archiving_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 253,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ArchiveDurationInMonths__": "LabelArchiveDurationInMonths__",
																			"LabelArchiveDurationInMonths__": "ArchiveDurationInMonths__",
																			"LegalArchiving__": "LabelLegalArchiving__",
																			"LabelLegalArchiving__": "LegalArchiving__",
																			"LabelSignInvoicesForArchiving__": "SignInvoicesForArchiving__",
																			"SignInvoicesForArchiving__": "LabelSignInvoicesForArchiving__",
																			"ArchiveDataSettings__": "LabelArchiveDataSettings__",
																			"LabelArchiveDataSettings__": "ArchiveDataSettings__"
																		},
																		"version": 0
																	},
																	"stamp": 254,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelArchiveDurationInMonths__": {
																						"line": 2,
																						"column": 1
																					},
																					"ArchiveDurationInMonths__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSignInvoicesForArchiving__": {
																						"line": 3,
																						"column": 1
																					},
																					"SignInvoicesForArchiving__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelLegalArchiving__": {
																						"line": 4,
																						"column": 1
																					},
																					"LegalArchiving__": {
																						"line": 4,
																						"column": 2
																					},
																					"ArchiveDataSettings__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelArchiveDataSettings__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 255,
																			"*": {
																				"LabelArchiveDataSettings__": {
																					"type": "Label",
																					"data": [
																						"ArchiveDataSettings__"
																					],
																					"options": {
																						"label": "_ArchiveDataSettings",
																						"version": 0
																					},
																					"stamp": 256
																				},
																				"ArchiveDataSettings__": {
																					"type": "RadioButton",
																					"data": [
																						"ArchiveDataSettings__"
																					],
																					"options": {
																						"keys": [
																							"_ArchiveAll",
																							"_ArchiveDataOnly"
																						],
																						"values": [
																							"_ArchiveAll",
																							"_ArchiveDataOnly"
																						],
																						"label": "_ArchiveDataSettings",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 257
																				},
																				"LabelArchiveDurationInMonths__": {
																					"type": "Label",
																					"data": [
																						"ArchiveDurationInMonths__"
																					],
																					"options": {
																						"label": "_ArchiveDurationInMonths",
																						"version": 0
																					},
																					"stamp": 258
																				},
																				"ArchiveDurationInMonths__": {
																					"type": "ComboBox",
																					"data": [
																						"ArchiveDurationInMonths__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_TwoMonths",
																							"1": "_OneYear",
																							"2": "_TwoYears",
																							"3": "_ThreeYears",
																							"4": "_FourYears",
																							"5": "_FiveYears",
																							"6": "_SixYears",
																							"7": "_SevenYears",
																							"8": "_EightYears",
																							"9": "_NineYears",
																							"10": "_TenYears",
																							"11": "_ElevenYears",
																							"12": "_TwelveYears",
																							"13": "_ThirteenYears",
																							"14": "_FourteenYears",
																							"15": "_FifteenYears"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132",
																							"12": "144",
																							"13": "156",
																							"14": "168",
																							"15": "180"
																						},
																						"label": "_ArchiveDurationInMonths",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 259
																				},
																				"LabelSignInvoicesForArchiving__": {
																					"type": "Label",
																					"data": [
																						"SignInvoicesForArchiving__"
																					],
																					"options": {
																						"label": "_ArchivingSignInvoicesForArchiving",
																						"version": 0
																					},
																					"stamp": 260
																				},
																				"SignInvoicesForArchiving__": {
																					"type": "CheckBox",
																					"data": [
																						"SignInvoicesForArchiving__"
																					],
																					"options": {
																						"label": "_ArchivingSignInvoicesForArchiving",
																						"activable": true,
																						"width": 250,
																						"version": 0
																					},
																					"stamp": 261
																				},
																				"LabelLegalArchiving__": {
																					"type": "Label",
																					"data": [
																						"LegalArchiving__"
																					],
																					"options": {
																						"label": "_LegalArchiving",
																						"version": 0
																					},
																					"stamp": 262
																				},
																				"LegalArchiving__": {
																					"type": "CheckBox",
																					"data": [
																						"LegalArchiving__"
																					],
																					"options": {
																						"label": "_LegalArchiving",
																						"activable": true,
																						"width": 250,
																						"defaultValue": false,
																						"helpText": "_InfoLegalArchiving",
																						"version": 0,
																						"helpURL": "2510",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 263
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 264,
													"*": {
														"Vendor_Portal_Archiving": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Vendor_Portal_Archiving",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 265,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ArchiveDurationVendorPortalInMonth__": "LabelArchiveDurationVendorPortalInMonth__",
																			"LabelArchiveDurationVendorPortalInMonth__": "ArchiveDurationVendorPortalInMonth__"
																		},
																		"version": 0
																	},
																	"stamp": 266,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ArchiveDurationVendorPortalInMonth__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelArchiveDurationVendorPortalInMonth__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 267,
																			"*": {
																				"LabelArchiveDurationVendorPortalInMonth__": {
																					"type": "Label",
																					"data": [
																						"ArchiveDurationVendorPortalInMonth__"
																					],
																					"options": {
																						"label": "_ArchiveDurationVendorPortalInMonth",
																						"version": 0
																					},
																					"stamp": 268
																				},
																				"ArchiveDurationVendorPortalInMonth__": {
																					"type": "ComboBox",
																					"data": [
																						"ArchiveDurationVendorPortalInMonth__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_TwoMonths",
																							"1": "_OneYear",
																							"2": "_TwoYears",
																							"3": "_ThreeYears",
																							"4": "_FourYears",
																							"5": "_FiveYears",
																							"6": "_SixYears",
																							"7": "_SevenYears",
																							"8": "_EightYears",
																							"9": "_NineYears",
																							"10": "_TenYears",
																							"11": "_ElevenYears",
																							"12": "_TwelveYears",
																							"13": "_ThirteenYears",
																							"14": "_FourteenYears",
																							"15": "_FifteenYears"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "0",
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132",
																							"12": "144",
																							"13": "156",
																							"14": "168",
																							"15": "180"
																						},
																						"label": "_ArchiveDurationVendorPortalInMonth",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 269
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 270,
													"*": {
														"Taxes_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Taxes_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 271,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TaxesWithholdingTax__": "LabelTaxesWithholdingTax__",
																			"LabelTaxesWithholdingTax__": "TaxesWithholdingTax__",
																			"MultiTaxesOnALineItem__": "LabelMultiTaxesOnALineItem__",
																			"LabelMultiTaxesOnALineItem__": "MultiTaxesOnALineItem__"
																		},
																		"version": 0
																	},
																	"stamp": 272,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TaxesWithholdingTax__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelTaxesWithholdingTax__": {
																						"line": 1,
																						"column": 1
																					},
																					"MultiTaxesOnALineItem__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelMultiTaxesOnALineItem__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 273,
																			"*": {
																				"LabelTaxesWithholdingTax__": {
																					"type": "Label",
																					"data": [
																						"TaxesWithholdingTax__"
																					],
																					"options": {
																						"label": "_TaxesWithholdingTax",
																						"version": 0
																					},
																					"stamp": 274
																				},
																				"TaxesWithholdingTax__": {
																					"type": "ComboBox",
																					"data": [
																						"TaxesWithholdingTax__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Not used withholding taxes",
																							"1": "_Basic withholding taxes",
																							"2": "_Extended withholding taxes"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "NotUsed",
																							"1": "Basic",
																							"2": "Extended"
																						},
																						"label": "_TaxesWithholdingTax",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 275
																				},
																				"LabelMultiTaxesOnALineItem__": {
																					"type": "Label",
																					"data": [
																						"MultiTaxesOnALineItem__"
																					],
																					"options": {
																						"label": "_MultiTaxesOnALineItem",
																						"version": 0
																					},
																					"stamp": 276
																				},
																				"MultiTaxesOnALineItem__": {
																					"type": "CheckBox",
																					"data": [
																						"MultiTaxesOnALineItem__"
																					],
																					"options": {
																						"label": "_MultiTaxesOnALineItem",
																						"helpText": "_HelpMultiTaxesOnALineItem",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 277
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-18": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 278,
													"*": {
														"Purcahsing_options_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Purchasing_options_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"elementsAlignment": "left",
																"sameHeightAsSiblings": false
															},
															"stamp": 279,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"DisplayTaxCode__": "LabelDisplayTaxCode__",
																			"LabelDisplayTaxCode__": "DisplayTaxCode__",
																			"DisplayUnitOfMeasure__": "LabelDisplayUnitOfMeasure__",
																			"LabelDisplayUnitOfMeasure__": "DisplayUnitOfMeasure__",
																			"DefaultUnitOfMeasure__": "LabelDefaultUnitOfMeasure__",
																			"LabelDefaultUnitOfMeasure__": "DefaultUnitOfMeasure__",
																			"DisplayCostType__": "LabelDisplayCostType__",
																			"LabelDisplayCostType__": "DisplayCostType__",
																			"ContractViewer__": "LabelContractViewer__",
																			"LabelContractViewer__": "ContractViewer__"
																		},
																		"version": 0
																	},
																	"stamp": 280,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DisplayTaxCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelDisplayTaxCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"DisplayUnitOfMeasure__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelDisplayUnitOfMeasure__": {
																						"line": 2,
																						"column": 1
																					},
																					"DefaultUnitOfMeasure__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDefaultUnitOfMeasure__": {
																						"line": 3,
																						"column": 1
																					},
																					"DisplayCostType__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDisplayCostType__": {
																						"line": 4,
																						"column": 1
																					},
																					"ContractViewer__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelContractViewer__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 281,
																			"*": {
																				"LabelDisplayTaxCode__": {
																					"type": "Label",
																					"data": [
																						"DisplayTaxCode__"
																					],
																					"options": {
																						"label": "_Display Tax Code",
																						"version": 0
																					},
																					"stamp": 282
																				},
																				"DisplayTaxCode__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplayTaxCode__"
																					],
																					"options": {
																						"label": "_Display Tax Code",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 283
																				},
																				"LabelDisplayUnitOfMeasure__": {
																					"type": "Label",
																					"data": [
																						"DisplayUnitOfMeasure__"
																					],
																					"options": {
																						"label": "_Display Unit Of Measure",
																						"version": 0
																					},
																					"stamp": 284
																				},
																				"DisplayUnitOfMeasure__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplayUnitOfMeasure__"
																					],
																					"options": {
																						"label": "_Display Unit Of Measure",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 285
																				},
																				"LabelDefaultUnitOfMeasure__": {
																					"type": "Label",
																					"data": [
																						"DefaultUnitOfMeasure__"
																					],
																					"options": {
																						"label": "_Default Unit Of Measure",
																						"version": 0
																					},
																					"stamp": 286
																				},
																				"DefaultUnitOfMeasure__": {
																					"type": "ShortText",
																					"data": [
																						"DefaultUnitOfMeasure__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Default Unit Of Measure",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 287
																				},
																				"LabelDisplayCostType__": {
																					"type": "Label",
																					"data": [
																						"DisplayCostType__"
																					],
																					"options": {
																						"label": "_Display Cost Type",
																						"version": 0
																					},
																					"stamp": 288
																				},
																				"DisplayCostType__": {
																					"type": "CheckBox",
																					"data": [
																						"DisplayCostType__"
																					],
																					"options": {
																						"label": "_Display Cost Type",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 289
																				},
																				"LabelContractViewer__": {
																					"type": "Label",
																					"data": [
																						"ContractViewer__"
																					],
																					"options": {
																						"label": "_ContractViewer",
																						"version": 0
																					},
																					"stamp": 290
																				},
																				"ContractViewer__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ContractViewer__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches",
																						"label": "_ContractViewer",
																						"activable": true,
																						"width": "250",
																						"helpText": "_ContractsVisibilityUserGroup_Help",
																						"helpURL": "2516",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 291
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 292,
													"*": {
														"PlannedDeliveryCosts_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PlannedDeliveryCosts_pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 293,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"PlannedPricingConditions__": "LabelPlannedPricingConditions__",
																			"LabelPlannedPricingConditions__": "PlannedPricingConditions__"
																		},
																		"version": 0
																	},
																	"stamp": 294,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"PlannedPricingConditions__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPlannedPricingConditions__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 295,
																			"*": {
																				"LabelPlannedPricingConditions__": {
																					"type": "Label",
																					"data": [
																						"PlannedPricingConditions__"
																					],
																					"options": {
																						"label": "_PlannedPricingConditions",
																						"version": 0
																					},
																					"stamp": 296
																				},
																				"PlannedPricingConditions__": {
																					"type": "LongText",
																					"data": [
																						"PlannedPricingConditions__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_PlannedPricingConditions",
																						"activable": true,
																						"width": 250,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 297
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 298,
													"*": {
														"OCRParameters_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_OCRParameters_pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 299,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"OCRImageBinarization__": "LabelOCRImageBinarization__",
																			"LabelOCRImageBinarization__": "OCRImageBinarization__",
																			"OCRImageDraftFax__": "LabelOCRImageDraftFax__",
																			"LabelOCRImageDraftFax__": "OCRImageDraftFax__",
																			"OCRImageDespeckle__": "LabelOCRImageDespeckle__",
																			"LabelOCRImageDespeckle__": "OCRImageDespeckle__",
																			"OCRPdfProcessing__": "LabelOCRPdfProcessing__",
																			"LabelOCRPdfProcessing__": "OCRPdfProcessing__",
																			"OCRTolerantWordSeparation__": "LabelOCRTolerantWordSeparation__",
																			"LabelOCRTolerantWordSeparation__": "OCRTolerantWordSeparation__",
																			"OCRGlobalPerformances__": "LabelOCRGlobalPerformances__",
																			"LabelOCRGlobalPerformances__": "OCRGlobalPerformances__",
																			"OCRLanguagesList__": "LabelOCRLanguagesList__",
																			"LabelOCRLanguagesList__": "OCRLanguagesList__",
																			"OCRJsonOverrideSettings__": "LabelOCRJsonOverrideSettings__",
																			"LabelOCRJsonOverrideSettings__": "OCRJsonOverrideSettings__",
																			"EnableOverrideOCRParameters__": "LabelEnableOverrideOCRParameters__",
																			"LabelEnableOverrideOCRParameters__": "EnableOverrideOCRParameters__",
																			"OCREngineList__": "LabelOCREngineList__",
																			"LabelOCREngineList__": "OCREngineList__"
																		},
																		"version": 0
																	},
																	"stamp": 300,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"OCRImageBinarization__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelOCRImageBinarization__": {
																						"line": 4,
																						"column": 1
																					},
																					"OCRImageDraftFax__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelOCRImageDraftFax__": {
																						"line": 5,
																						"column": 1
																					},
																					"OCRImageDespeckle__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelOCRImageDespeckle__": {
																						"line": 6,
																						"column": 1
																					},
																					"OCRPdfProcessing__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelOCRPdfProcessing__": {
																						"line": 9,
																						"column": 1
																					},
																					"OCRTolerantWordSeparation__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelOCRTolerantWordSeparation__": {
																						"line": 10,
																						"column": 1
																					},
																					"OCRGlobalPerformances__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelOCRGlobalPerformances__": {
																						"line": 8,
																						"column": 1
																					},
																					"OCRLanguagesList__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelOCRLanguagesList__": {
																						"line": 7,
																						"column": 1
																					},
																					"OCRJsonOverrideSettings__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelOCRJsonOverrideSettings__": {
																						"line": 1,
																						"column": 1
																					},
																					"EnableOverrideOCRParameters__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEnableOverrideOCRParameters__": {
																						"line": 2,
																						"column": 1
																					},
																					"OCREngineList__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelOCREngineList__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 10,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 301,
																			"*": {
																				"LabelOCRJsonOverrideSettings__": {
																					"type": "Label",
																					"data": [
																						"OCRJsonOverrideSettings__"
																					],
																					"options": {
																						"label": "_OCRJsonOverrideSettings",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 302
																				},
																				"OCRJsonOverrideSettings__": {
																					"type": "ShortText",
																					"data": [
																						"OCRJsonOverrideSettings__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_OCRJsonOverrideSettings",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 2048,
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 303
																				},
																				"LabelEnableOverrideOCRParameters__": {
																					"type": "Label",
																					"data": [
																						"EnableOverrideOCRParameters__"
																					],
																					"options": {
																						"label": "_EnableOverrideOCRParameters",
																						"version": 0
																					},
																					"stamp": 304
																				},
																				"EnableOverrideOCRParameters__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableOverrideOCRParameters__"
																					],
																					"options": {
																						"label": "_EnableOverrideOCRParameters",
																						"activable": true,
																						"width": "250",
																						"helpText": "_EnableOverrideOCRParameters_help",
																						"helpURL": "2513",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 305
																				},
																				"LabelOCREngineList__": {
																					"type": "Label",
																					"data": [
																						"OCREngineList__"
																					],
																					"options": {
																						"label": "_OCREngineList",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 306
																				},
																				"OCREngineList__": {
																					"type": "ComboBox",
																					"data": [
																						"OCREngineList__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Keep original engine",
																							"1": "_Nuance190",
																							"2": "_Nuance210",
																							"3": "_GoogleCloudVision"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "KeepOriginalEngine",
																							"1": "Nuance190",
																							"2": "Nuance210",
																							"3": "GoogleCloudVision"
																						},
																						"label": "_OCREngineList",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 307
																				},
																				"LabelOCRImageBinarization__": {
																					"type": "Label",
																					"data": [
																						"OCRImageBinarization__"
																					],
																					"options": {
																						"label": "_Color",
																						"version": 0
																					},
																					"stamp": 308
																				},
																				"OCRImageBinarization__": {
																					"type": "ComboBox",
																					"data": [
																						"OCRImageBinarization__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Keep image colors",
																							"1": "_Gray scale",
																							"2": "_Black and white"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "no",
																							"1": "gray-scale",
																							"2": "auto"
																						},
																						"label": "_Color",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"notInDB": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 309
																				},
																				"LabelOCRImageDraftFax__": {
																					"type": "Label",
																					"data": [
																						"OCRImageDraftFax__"
																					],
																					"options": {
																						"label": "_LowLevelFaxImage",
																						"version": 0
																					},
																					"stamp": 310
																				},
																				"OCRImageDraftFax__": {
																					"type": "CheckBox",
																					"data": [
																						"OCRImageDraftFax__"
																					],
																					"options": {
																						"label": "_LowLevelFaxImage",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"version": 0
																					},
																					"stamp": 311
																				},
																				"LabelOCRImageDespeckle__": {
																					"type": "Label",
																					"data": [
																						"OCRImageDespeckle__"
																					],
																					"options": {
																						"label": "_ImageDespeckle",
																						"version": 0
																					},
																					"stamp": 312
																				},
																				"OCRImageDespeckle__": {
																					"type": "CheckBox",
																					"data": [
																						"OCRImageDespeckle__"
																					],
																					"options": {
																						"label": "_ImageDespeckle",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"version": 0
																					},
																					"stamp": 313
																				},
																				"LabelOCRPdfProcessing__": {
																					"type": "Label",
																					"data": [
																						"OCRPdfProcessing__"
																					],
																					"options": {
																						"label": "_PdfProcessing",
																						"version": 0
																					},
																					"stamp": 314
																				},
																				"OCRPdfProcessing__": {
																					"type": "ComboBox",
																					"data": [
																						"OCRPdfProcessing__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_OCRPDFTextAndImageProcessing",
																							"1": "_OCRImageProcessing",
																							"2": "_NoOCRProcessing"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "ocr-pdf-text-and-image",
																							"1": "ocr-image",
																							"2": "ocr-pdf"
																						},
																						"label": "_PdfProcessing",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"notInDB": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 315
																				},
																				"LabelOCRTolerantWordSeparation__": {
																					"type": "Label",
																					"data": [
																						"OCRTolerantWordSeparation__"
																					],
																					"options": {
																						"label": "Word separation tolerance",
																						"version": 0
																					},
																					"stamp": 316
																				},
																				"OCRTolerantWordSeparation__": {
																					"type": "CheckBox",
																					"data": [
																						"OCRTolerantWordSeparation__"
																					],
																					"options": {
																						"label": "Word separation tolerance",
																						"activable": true,
																						"width": "250",
																						"helpText": "Recommended for PDF processed as images by the OCR",
																						"helpURL": "2515",
																						"helpFormat": "HTML Format",
																						"defaultValue": true,
																						"notInDB": true,
																						"version": 0
																					},
																					"stamp": 317
																				},
																				"LabelOCRGlobalPerformances__": {
																					"type": "Label",
																					"data": [
																						"OCRGlobalPerformances__"
																					],
																					"options": {
																						"label": "_TradeOff",
																						"version": 0
																					},
																					"stamp": 318
																				},
																				"OCRGlobalPerformances__": {
																					"type": "ComboBox",
																					"data": [
																						"OCRGlobalPerformances__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Accurate",
																							"1": "_Fast",
																							"2": "_Balanced"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "accurate",
																							"1": "fast",
																							"2": "balanced"
																						},
																						"label": "_TradeOff",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"notInDB": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 319
																				},
																				"LabelOCRLanguagesList__": {
																					"type": "Label",
																					"data": [
																						"OCRLanguagesList__"
																					],
																					"options": {
																						"label": "_AllowedLanguages",
																						"version": 0
																					},
																					"stamp": 320
																				},
																				"OCRLanguagesList__": {
																					"type": "ComboBox",
																					"data": [
																						"OCRLanguagesList__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_English",
																							"1": "_German",
																							"2": "_French",
																							"3": "_Dutch",
																							"10": "_Spanish",
																							"13": "_Italian",
																							"119": "_Japanese",
																							"120": "_Simplified Chinese",
																							"121": "_Traditional Chinese",
																							"122": "_Korean",
																							"125": "_Hebrew",
																							"-2": "_AutoDetectLanguage"
																						},
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_AllowedLanguages",
																						"activable": true,
																						"width": "250",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"multiple": true,
																						"notInDB": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 321
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-21": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 322,
													"*": {
														"VendorManagement_options_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "500px",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Vendor_management_pane_verification",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 323,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelEnableOFACVerification__": "EnableOFACVerification__",
																			"EnableOFACVerification__": "LabelEnableOFACVerification__",
																			"EnableSiSid__": "LabelEnableSiSid__",
																			"LabelEnableSiSid__": "EnableSiSid__",
																			"SiSidLogin__": "LabelSiSidLogin__",
																			"LabelSiSidLogin__": "SiSidLogin__",
																			"SiSidPassword__": "LabelSiSidPassword__",
																			"LabelSiSidPassword__": "SiSidPassword__"
																		},
																		"version": 0
																	},
																	"stamp": 324,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelEnableOFACVerification__": {
																						"line": 1,
																						"column": 1
																					},
																					"EnableOFACVerification__": {
																						"line": 1,
																						"column": 2
																					},
																					"EnableSiSid__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelEnableSiSid__": {
																						"line": 2,
																						"column": 1
																					},
																					"SiSidLogin__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSiSidLogin__": {
																						"line": 3,
																						"column": 1
																					},
																					"SiSidPassword__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSiSidPassword__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 325,
																			"*": {
																				"EnableOFACVerification__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableOFACVerification__"
																					],
																					"options": {
																						"label": "_EnableOFACVerification",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 326
																				},
																				"LabelEnableOFACVerification__": {
																					"type": "Label",
																					"data": [
																						"EnableOFACVerification__"
																					],
																					"options": {
																						"label": "_EnableOFACVerification",
																						"version": 0
																					},
																					"stamp": 327
																				},
																				"LabelEnableSiSid__": {
																					"type": "Label",
																					"data": [
																						"EnableSiSid__"
																					],
																					"options": {
																						"label": "_Enable IBAN verification using 3rd party service SiS id",
																						"version": 0
																					},
																					"stamp": 328
																				},
																				"EnableSiSid__": {
																					"type": "CheckBox",
																					"data": [
																						"EnableSiSid__"
																					],
																					"options": {
																						"label": "_Enable IBAN verification using 3rd party service SiS id",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "_If enabled, IBANs entered in Vendor Registration form will be check by the 3rd party service SiS id.",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 329
																				},
																				"LabelSiSidLogin__": {
																					"type": "Label",
																					"data": [
																						"SiSidLogin__"
																					],
																					"options": {
																						"label": "_SiS id login",
																						"version": 0
																					},
																					"stamp": 330
																				},
																				"SiSidLogin__": {
																					"type": "ShortText",
																					"data": [
																						"SiSidLogin__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SiS id login",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 331
																				},
																				"LabelSiSidPassword__": {
																					"type": "Label",
																					"data": [
																						"SiSidPassword__"
																					],
																					"options": {
																						"label": "_SiS id password",
																						"version": 0
																					},
																					"stamp": 332
																				},
																				"SiSidPassword__": {
																					"type": "Password",
																					"data": [
																						"SiSidPassword__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_SiS id password",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 333
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 334,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "100%",
													"width": "0%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 335
																}
															},
															"stamp": 336,
															"data": []
														}
													},
													"stamp": 337,
													"data": []
												}
											},
											"stamp": 338,
											"data": []
										}
									},
									"stamp": 339,
									"data": []
								}
							},
							"stamp": 340,
							"data": []
						}
					},
					"stamp": 341,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Previous": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Previous__",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 342
						},
						"Next": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Next__",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings__",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 343
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"automaticSave": true,
								"nextprocess": {
									"processName": "Accounts Receivable Batch Processing",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 1,
								"url": ""
							},
							"stamp": 344,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true
								}
							},
							"stamp": 345,
							"data": []
						}
					},
					"stamp": 346,
					"data": []
				}
			},
			"stamp": 347,
			"data": []
		}
	},
	"stamps": 350,
	"data": []
}