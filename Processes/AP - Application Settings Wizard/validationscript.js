///#GLOBALS Sys Lib
function SetError(step, message)
{
	if (step !== null)
	{
		Variable.SetValueAsString("currentStep", step);
	}
	Variable.SetValueAsString("ErrorMessage", message);
	Process.PreventApproval();
}

function CheckConfiguration()
{
	var loadedConfiguration = Variable.GetValueAsString("ConfigurationLoaded");
	var currentConfiguration = Data.GetValue("ConfigurationName__");
	var GlobalStep = 0;
	Variable.SetValueAsString("ErrorMessage", "");
	if (!currentConfiguration)
	{
		SetError(GlobalStep, "_Expecting a configuration name");
		return false;
	}
	if (loadedConfiguration !== currentConfiguration)
	{
		//Check the configuration does not already exist
		Query.Reset();
		Query.SetSpecificTable("AP - Application Settings__");
		Query.SetFilter("ConfigurationName__=" + currentConfiguration);
		if (Query.MoveFirst() && Query.MoveNextRecord())
		{
			SetError(GlobalStep, "_Existing configuration");
			return false;
		}
	}
	if (Data.IsFormInError())
	{
		return false;
	}
	return true;
}

function Save()
{
	Log.Info("Saving configuration global settings");
	var recordGlobalSettings = Sys.Helpers.Database.CD2CT("P2P - Global Application Settings__");
	if (recordGlobalSettings.GetLastError() !== 0)
	{
		this.SetError(null, "Could not save global settings : " + recordGlobalSettings.GetLastErrorMessage());
	}
	Log.Info("Saving configuration settings");
	var options = {
		specialFields:
		{
			LastModifiedDateTime__:
			{
				GetValue: function ()
				{
					return Sys.Helpers.Date.Date2DBDateTime(new Date());
				}
			},
			LastModifiedBy__:
			{
				GetValue: function ()
				{
					var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
					var userVars = currentUser.GetVars();
					return userVars.GetValue_String("Login", 0);
				}
			},
			OrderNumberPatterns__:
			{
				GetValue: function ()
				{
					return Data.GetValue("OrderNumberPatterns__").replace(/([^\n])\n+([^\n])/g, "$1;$2").replace(/\n/g, "");
				}
			},
			GoodIssueNumberPatterns__:
			{
				GetValue: function ()
				{
					return Data.GetValue("GoodIssueNumberPatterns__").replace(/([^\n])\n+([^\n])/g, "$1;$2").replace(/\n/g, "");
				}
			},
			ContractNumberPatterns__:
			{
				GetValue: function ()
				{
					return Data.GetValue("ContractNumberPatterns__").replace(/([^\n])\n+([^\n])/g, "$1;$2").replace(/\n/g, "");
				}
			}
		}
	};
	var record = Sys.Helpers.Database.CD2CT("AP - Application Settings__", "ConfigurationName__=" + Data.GetValue("ConfigurationName__"), options);
	if (record.GetLastError() !== 0)
	{
		SetError(null, "Could not save settings : " + record.GetLastErrorMessage());
	}
	else
	{
		Log.Info("The configuration has been successfully saved");
	}
}

function run()
{
	// Saving the configuration
	if (Data.GetActionType() === "approve" && CheckConfiguration())
	{
		Save();
	}
}
run();