///#GLOBALS Lib
/*globals Data, Log, Lib, Process, Query */

var RecoveryHelper =
{
	/** Recovery for PAC */
	Recover: function ()
	{
		var operationsId = [];
		var budgetToCompute = [];

		var when = new Date();
		when.setTime(when.getTime() - 86400 * 3 * 1000);  // Only check 3 days old records		
		var filter = "(&(state>100)(CompletionDateTime>" + Sys.Helpers.Date.Date2DBDateTime(when) + "))";

		this.PopulateBudgetToCompute(operationsId, "Purchase requisition V2", filter);
		this.PopulateBudgetToCompute(operationsId, "Purchase order V2", filter);
		this.PopulateBudgetToCompute(operationsId, "Goods receipt V2", filter);
		this.PopulateBudgetToCompute(operationsId, "Vendor invoice", this.FindInvoicesInError());

		//delete operation detail from failed purchase Requisition
		for (var i = 0; i < operationsId.length; i++)
		{
			Lib.Budget.CleanOperationDetails(operationsId[i], budgetToCompute);
		}
		
		//recompute all impacted budget
		for (var index in budgetToCompute)
		{			
			var budget = {};
			budget[budgetToCompute[index]] = null;
			var criticalSectionName = Lib.Budget.GetCriticalSection(budget);
			Log.Info("Try entering Budget critical section: " + criticalSectionName);
			Process.PreventConcurrentAccess(criticalSectionName, 
				function ()
				{
					Log.Info("Entered budget critical section");
					Lib.Budget.ComputeBudgetFromOperationDetails(budgetToCompute[index]);
				},
				null,				
				60);
			
		}

		//delete operation detail from failed purchase Requisition
		for (var i = 0; i < operationsId.length; i++)
		{
			Lib.Budget.DeleteOperationDetails(operationsId[i]);
		}
	},

	FindInvoicesInError: function ()
	{
		var when = new Date();
		when.setTime(when.getTime() - 86400 * 3 * 1000);  // Only check 3 days old records		
		return "&(State>100)(ERPInvoiceNumber__=)(InvoiceStatus__!=Rejected)(CompletionDateTime>" + Sys.Helpers.Date.Date2DBDateTime(when) + "))";
	},

	PopulateBudgetToCompute: function (operationsId, processName, filter)
	{
		var xTransport;
		var vars;
		// Query for old PAC version
		Log.Info("PopulateBudgetToCompute with PAC");
		Query.Reset();
		Query.SetOptionEx("Limit=-1");
		Query.SetSpecificTable("CDNAME#" + processName);
		Query.SetAttributesList("RUIDEX");
		var when = new Date();
		when.setTime(when.getTime() - 86400 * 3 * 1000);  // Only check 3 days old records		
		Query.SetFilter("(&(state>100)(CompletionDateTime>" + Sys.Helpers.Date.Date2DBDateTime(when) + "))");
		if (Query.MoveFirst())
		{
			xTransport = Query.MoveNext();
			while (xTransport)
			{
				vars = xTransport.GetUninheritedVars();
				var ruidEx = vars.GetValue_String("RUIDEX", 0);
				operationsId.push(ruidEx);
				Log.Info("Check RUIDEX " + ruidEx);
				xTransport = Query.MoveNext();
			}
		}
		else
		{
			Log.Error(Query.GetLastErrorMessage());
		}
	}
};

var InvoicesRecoveryHelper =
{
	/** Recovery for AP */
	RecoverInvoices: function ()
	{
		this.RecoverInvoicesPostedNotExported();
	},

	FindInvoices: function (filter)
	{
		// [[invoicesRUIDEX, invoiceCompanyCode, deletedfromoperation][...]]
		var invoices = [];
		Query.Reset();
		Query.SetSpecificTable("CDNAME#Vendor invoice");
		Query.SetAttributesList("RUIDEX");
		Query.SetOptionEx("Limit=-1");
		Query.SetFilter(filter);

		if (Query.MoveFirst())
		{
			var xTransport = Query.MoveNext();
			var vars;
			while (xTransport)
			{
				vars = xTransport.GetUninheritedVars();
				Log.Info("Found invoice :" + vars.GetValue_String("RUIDEX", 0) + " - " + vars.GetValue_String("CompanyCode__", 0));
				invoices.push(
					{
						ruidex: vars.GetValue_String("RUIDEX", 0),						
					});

				xTransport = Query.MoveNext();
			}
		}
		else
		{
			Log.Error(Query.GetLastErrorMessage());
		}
		return invoices;
	},

	/**
	* Find invoices posted but not exported by the VIP
	*/
	FindInvoicesPostedNotExported: function ()
	{
		var when = new Date();
		when.setTime(when.getTime() - 86400 * 3 * 1000);  // Only check 3 days old records		
		return this.FindInvoices("(&(BudgetExportStatus__!=success)(BudgetExportStatus__!=ignored)(ERPInvoiceNumber__!=)(CompletionDateTime>" + Sys.Helpers.Date.Date2DBDateTime(when) + "))");
	},

		
	/**
	* Recreate operations and compute budget for all invoices posted but not exported
	*/
	RecoverInvoicesPostedNotExported: function ()
	{
		Log.Info("start RecoverInvoicesPostedNotExported");
		// [[invoicesRUIDEX,invoiceCompanyCode][...]]
		var invoices = this.FindInvoicesPostedNotExported();

		/** Recover invoices */
		for (var i = 0; i < invoices.length; i++)
		{
			Lib.Budget.RecreateOperationDetails(invoices[i].ruidex);			
		}
	},

	/**
	* Get the concurrent access and call the callback with Invoices
	*/
	ProcessConcurrentAccess: function (invoice, callback)
	{
		var criticalSectionName = "PAC_Budget_" + Process.GetProcessID(Lib.P2P.GetPRProcessName()) + "_" + invoice.companyCode;
		return Process.PreventConcurrentAccess(criticalSectionName, function ()
		{
			Log.Info("Entered budget critical section");
			callback(invoice);
		},
			function (ruidex)
			{
				Log.Info("Entered budget recovery section");
				Lib.Budget.recoverFromConcurrentAccess(ruidex);
			},
			60);
	}
};



if (Data.GetActionName() === "Approve" || Data.GetActionName() === "" && Lib.Budget.IsEnabled())
{
	Process.SetTimeOut(3600);
	
	/** Budget recovery */
	RecoveryHelper.Recover();

	/** AP budget recovery */
	InvoicesRecoveryHelper.RecoverInvoices();
}