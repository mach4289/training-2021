{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"maxwidth": 800,
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"maxwidth": 800,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"maxwidth": 800,
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"Wizard_steps": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "color1",
														"labelsAlignment": "right",
														"label": "Wizard steps",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"stamp": 3,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Wizard_steps__": "LabelWizard_steps__",
																	"LabelWizard_steps__": "Wizard_steps__"
																},
																"version": 0
															},
															"stamp": 4,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Spacer_line__": {
																				"line": 2,
																				"column": 1
																			},
																			"Wizard_steps__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelWizard_steps__": {
																				"line": 1,
																				"column": 1
																			},
																			"Wizard_step_desc__": {
																				"line": 3,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 5,
																	"*": {
																		"Wizard_steps__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "",
																				"width": "",
																				"css": "@ .wizard-container {\n\tleft: 50%;\n\tposition: absolute;\n}@ \n.wizard-timeline {\n\tcontent: \"\";\n\theight: 3px;\n\tmargin-top: 35px;\n\tposition: absolute;\n\tbackground-color: #00b4bd;\n\tz-index: 10;\n\tleft: 0px;\n}@ \n.wizard {\n\theight: 150px;\n\tpadding-bottom: 15px;\n\tz-index: 30;\n\tleft: 0px;\n\tposition: absolute;\n\tcolor: #263645;\n}@ \n.wizard td {\n\ttext-align: center;\n\tfont-size: 14px;\n}@ \n.wizard td i {\n\twidth: 50px;\n\tpadding: 10px;\n}@ \n.wizard td.text {\n\ttext-transform: uppercase;\n\twhite-space: pre-wrap;\n\tvertical-align: top;\n\tfont-size: 12px;\n}@ \n.wizard td.wizardSelected {\n\tcolor: White;\n}@ ",
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "130",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Wizard_step_desc__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "color8",
																				"backgroundcolor": "default",
																				"label": "Description",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 8
																		},
																		"LabelWizard_steps__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line2",
																				"version": 0
																			},
																			"stamp": 10
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-4": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 11,
											"*": {
												"TitleArrow": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "TitleArrow",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 12,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelConfiguration_type__": "Configuration_type__",
																	"Configuration_type__": "LabelConfiguration_type__",
																	"LabelConfigurationName__": "ConfigurationName__",
																	"ConfigurationName__": "LabelConfigurationName__",
																	"LabelDocument_ID__": "Document_ID__",
																	"Document_ID__": "LabelDocument_ID__",
																	"LabelDocument_Type__": "Document_Type__",
																	"Document_Type__": "LabelDocument_Type__",
																	"Family__": "LabelFamily__",
																	"LabelFamily__": "Family__",
																	"Routing__": "LabelRouting__",
																	"LabelRouting__": "Routing__"
																},
																"version": 0
															},
															"stamp": 13,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"HTMLArrow__": {
																				"line": 1,
																				"column": 1
																			},
																			"LabelConfiguration_type__": {
																				"line": 4,
																				"column": 1
																			},
																			"Configuration_type__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelConfigurationName__": {
																				"line": 3,
																				"column": 1
																			},
																			"ConfigurationName__": {
																				"line": 3,
																				"column": 2
																			},
																			"Spacer_line22__": {
																				"line": 2,
																				"column": 1
																			},
																			"LabelDocument_ID__": {
																				"line": 8,
																				"column": 1
																			},
																			"Document_ID__": {
																				"line": 8,
																				"column": 2
																			},
																			"Spacer_line9__": {
																				"line": 7,
																				"column": 1
																			},
																			"LabelDocument_Type__": {
																				"line": 5,
																				"column": 1
																			},
																			"Document_Type__": {
																				"line": 5,
																				"column": 2
																			},
																			"Category__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCategory__": {
																				"line": 6,
																				"column": 1
																			},
																			"LabelFamily__": {
																				"line": 6,
																				"column": 1
																			},
																			"Family__": {
																				"line": 6,
																				"column": 2
																			},
																			"Routing__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelRouting__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"lines": 9,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 14,
																	"*": {
																		"Document_ID__": {
																			"type": "ShortText",
																			"data": [
																				"Document_ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Document_ID__",
																				"activable": true,
																				"width": "100%",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 444
																		},
																		"LabelDocument_ID__": {
																			"type": "Label",
																			"data": [
																				"Document_ID__"
																			],
																			"options": {
																				"label": "Document_ID__",
																				"version": 0
																			},
																			"stamp": 443
																		},
																		"LabelConfigurationName__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"label": "ConfigurationName__",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"Spacer_line22__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line22",
																				"version": 0
																			},
																			"stamp": 403
																		},
																		"Configuration_type__": {
																			"type": "ComboBox",
																			"data": [
																				"Configuration_type__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "Configuration_type_DEMO",
																					"1": "Configuration_type_TEST",
																					"2": "Configuration_type_PROD"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "DEMO",
																					"1": "TEST",
																					"2": "PROD"
																				},
																				"label": "Configuration_type__",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 41
																		},
																		"LabelDocument_Type__": {
																			"type": "Label",
																			"data": [
																				"Document_Type__"
																			],
																			"options": {
																				"label": "Document_Type__",
																				"version": 0
																			},
																			"stamp": 441
																		},
																		"ConfigurationName__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ConfigurationName__",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 21
																		},
																		"Document_Type__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_type__"
																			],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "Document_type__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 442
																		},
																		"LabelFamily__": {
																			"type": "Label",
																			"data": [
																				"Family__"
																			],
																			"options": {
																				"label": "Family",
																				"version": 0
																			},
																			"stamp": 538
																		},
																		"Family__": {
																			"type": "ShortText",
																			"data": [
																				"Family__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "Family",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 539
																		},
																		"Spacer_line9__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line9",
																				"version": 0
																			},
																			"stamp": 482
																		},
																		"LabelConfiguration_type__": {
																			"type": "Label",
																			"data": [
																				"Configuration_type__"
																			],
																			"options": {
																				"label": "Configuration_type__",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"HTMLArrow__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTMLArrow",
																				"htmlContent": "<div class=\"wizard-arrow\"></div>",
																				"css": "@ .wizard-arrow {\nbackground-color: white;\nwidth: 0;\nheight: 0;\nborder-style: solid;\nborder-width: 20px 25px 0 25px;\nborder-color: #008998 transparent transparent transparent;\nposition: absolute;\ntop: -10px;\nheight: 50px;\nleft: 50%;\nmargin-left: -25px;\n}@ ",
																				"width": 962,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelRouting__": {
																			"type": "Label",
																			"data": [
																				"ComboBox__"
																			],
																			"options": {
																				"label": "Routing",
																				"version": 0
																			},
																			"stamp": 542
																		},
																		"Routing__": {
																			"type": "ComboBox",
																			"data": [
																				"Routing__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "Deliver message",
																					"1": "Routing message"
																				},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "Routing",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 543
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-8": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 16,
											"*": {
												"General_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "General_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 17,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"ConfigurationSelection_Enable__": "LabelConfigurationSelection_Enable__",
																	"LabelConfigurationSelection_Enable__": "ConfigurationSelection_Enable__",
																	"SubmittedDocType__": "LabelSubmittedDocType__",
																	"LabelSubmittedDocType__": "SubmittedDocType__",
																	"DisableExtraction__": "LabelDisableExtraction__",
																	"LabelDisableExtraction__": "DisableExtraction__",
																	"LabelEnable_Configuration__": "Enable_Configuration__",
																	"Enable_Configuration__": "LabelEnable_Configuration__"
																},
																"version": 0
															},
															"stamp": 18,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ConfigurationSelection_Enable__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelConfigurationSelection_Enable__": {
																				"line": 4,
																				"column": 1
																			},
																			"SubmittedDocType__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelSubmittedDocType__": {
																				"line": 2,
																				"column": 1
																			},
																			"DisableExtraction__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDisableExtraction__": {
																				"line": 3,
																				"column": 1
																			},
																			"LabelEnable_Configuration__": {
																				"line": 1,
																				"column": 1
																			},
																			"Enable_Configuration__": {
																				"line": 1,
																				"column": 2
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 19,
																	"*": {
																		"LabelEnable_Configuration__": {
																			"type": "Label",
																			"data": [
																				"Enable_Configuration__"
																			],
																			"options": {
																				"label": "_EnableConfiguration",
																				"version": 0
																			},
																			"stamp": 528
																		},
																		"Enable_Configuration__": {
																			"type": "CheckBox",
																			"data": [
																				"Enable_Configuration__"
																			],
																			"options": {
																				"label": "_EnableConfiguration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 529
																		},
																		"LabelSubmittedDocType__": {
																			"type": "Label",
																			"data": [
																				"SubmittedDocType__"
																			],
																			"options": {
																				"label": "SubmittedDocType__",
																				"version": 0
																			},
																			"stamp": 397
																		},
																		"SubmittedDocType__": {
																			"type": "ComboBox",
																			"data": [
																				"SubmittedDocType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "SubmittedDocType__PDF",
																					"1": "SubmittedDocType__TIF",
																					"2": "SubmittedDocType__UBL",
																					"3": "SubmittedDocType__UBLPDF",
																					"4": "SubmittedDocType__XML",
																					"5": "SubmittedDocType__XMLPDF"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "pdf",
																					"1": "tif",
																					"2": "ubl",
																					"3": "ubl_pdf",
																					"4": "customxml",
																					"5": "customxml_pdf"
																				},
																				"label": "SubmittedDocType__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 398
																		},
																		"LabelDisableExtraction__": {
																			"type": "Label",
																			"data": [
																				"DisableExtraction__"
																			],
																			"options": {
																				"label": "_Disable_Extraction",
																				"version": 0
																			},
																			"stamp": 506
																		},
																		"DisableExtraction__": {
																			"type": "CheckBox",
																			"data": [
																				"DisableExtraction__"
																			],
																			"options": {
																				"label": "_Disable_Extraction",
																				"activable": true,
																				"width": 230,
																				"helpText": "_This option is used only with Esker loader and inbound channel. When extraction is disable, the acceptance criteria and the splitting are disable too. The extraction script is still launched.",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 507
																		},
																		"LabelConfigurationSelection_Enable__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_Enable__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_Enable__",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"ConfigurationSelection_Enable__": {
																			"type": "CheckBox",
																			"data": [
																				"ConfigurationSelection_Enable__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_Enable__",
																				"activable": true,
																				"version": 0,
																				"helpText": "_Select this option to apply this configuration according to specific criteria (file name or string in document)",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"width": "230"
																			},
																			"stamp": 23
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-27": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 24,
											"*": {
												"Configuration_Recognition_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Configuration_Recognition_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"stamp": 25,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"ConfigurationSelection_Area__": "LabelConfigurationSelection_Area__",
																	"LabelConfigurationSelection_Area__": "ConfigurationSelection_Area__",
																	"ConfigurationSelection_Criteria__": "LabelConfigurationSelection_Criteria__",
																	"LabelConfigurationSelection_Criteria__": "ConfigurationSelection_Criteria__",
																	"ConfigurationSelection_CriteriaIsRegex__": "LabelConfigurationSelection_CriteriaIsRegex__",
																	"LabelConfigurationSelection_CriteriaIsRegex__": "ConfigurationSelection_CriteriaIsRegex__",
																	"ConfigurationSelection_CriteriaMatchCase__": "LabelConfigurationSelection_CriteriaMatchCase__",
																	"LabelConfigurationSelection_CriteriaMatchCase__": "ConfigurationSelection_CriteriaMatchCase__",
																	"ConfigurationSelection_Path__": "LabelConfigurationSelection_Path__",
																	"LabelConfigurationSelection_Path__": "ConfigurationSelection_Path__",
																	"LabelUseFileNameRegEx__": "MatchFilenameRegex__",
																	"MatchFilenameRegex__": "LabelUseFileNameRegEx__",
																	"MatchStringWithinDocument__": "LabelUseTextInside__",
																	"LabelUseTextInside__": "MatchStringWithinDocument__",
																	"LabelFilename_regular_expression__": "Filename_regular_expression__",
																	"Filename_regular_expression__": "LabelFilename_regular_expression__",
																	"LabelFileNameRegEx_CaseSensitive__": "FileNameRegEx_CaseSensitive__",
																	"FileNameRegEx_CaseSensitive__": "LabelFileNameRegEx_CaseSensitive__",
																	"MatchDocumentLayout__": "LabelMatchDocumentLayout__",
																	"LabelMatchDocumentLayout__": "MatchDocumentLayout__"
																},
																"version": 0
															},
															"stamp": 26,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ConfigurationSelection_Area__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelConfigurationSelection_Area__": {
																				"line": 5,
																				"column": 1
																			},
																			"ConfigurationSelection_Criteria__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelConfigurationSelection_Criteria__": {
																				"line": 6,
																				"column": 1
																			},
																			"ConfigurationSelection_CriteriaIsRegex__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelConfigurationSelection_CriteriaIsRegex__": {
																				"line": 7,
																				"column": 1
																			},
																			"ConfigurationSelection_CriteriaMatchCase__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelConfigurationSelection_CriteriaMatchCase__": {
																				"line": 8,
																				"column": 1
																			},
																			"ConfigurationSelection_Path__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelConfigurationSelection_Path__": {
																				"line": 3,
																				"column": 1
																			},
																			"LabelUseFileNameRegEx__": {
																				"line": 2,
																				"column": 1
																			},
																			"MatchFilenameRegex__": {
																				"line": 2,
																				"column": 2
																			},
																			"MatchStringWithinDocument__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelUseTextInside__": {
																				"line": 4,
																				"column": 1
																			},
																			"LabelFilename_regular_expression__": {
																				"line": 9,
																				"column": 1
																			},
																			"Filename_regular_expression__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelFileNameRegEx_CaseSensitive__": {
																				"line": 10,
																				"column": 1
																			},
																			"FileNameRegEx_CaseSensitive__": {
																				"line": 10,
																				"column": 2
																			},
																			"MatchDocumentLayout__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelMatchDocumentLayout__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 10,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 27,
																	"*": {
																		"LabelMatchDocumentLayout__": {
																			"type": "Label",
																			"data": [
																				"MatchDocumentLayout__"
																			],
																			"options": {
																				"label": "MatchDocumentLayout__",
																				"version": 0
																			},
																			"stamp": 526
																		},
																		"MatchDocumentLayout__": {
																			"type": "CheckBox",
																			"data": [
																				"MatchDocumentLayout__"
																			],
																			"options": {
																				"label": "MatchDocumentLayout__",
																				"activable": true,
																				"width": 230,
																				"helpText": "_AutoLearningCriteriaTooltip",
																				"helpURL": "7011",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 527
																		},
																		"FileNameRegEx_CaseSensitive__": {
																			"type": "CheckBox",
																			"data": [
																				"FileNameRegEx_CaseSensitive__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 521
																		},
																		"LabelFileNameRegEx_CaseSensitive__": {
																			"type": "Label",
																			"data": [
																				"FileNameRegEx_CaseSensitive__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"version": 0
																			},
																			"stamp": 520
																		},
																		"Filename_regular_expression__": {
																			"type": "ShortText",
																			"data": [
																				"Filename_regular_expression__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Filename regular expression",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Follow the link above for acces help to create regular expression",
																				"helpURL": "2127",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 513
																		},
																		"LabelFilename_regular_expression__": {
																			"type": "Label",
																			"data": [
																				"Filename_regular_expression__"
																			],
																			"options": {
																				"label": "_Filename regular expression",
																				"version": 0
																			},
																			"stamp": 512
																		},
																		"LabelUseFileNameRegEx__": {
																			"type": "Label",
																			"data": [
																				"MatchFilenameRegex__"
																			],
																			"options": {
																				"label": "_Match filename with regular expression",
																				"version": 0
																			},
																			"stamp": 518
																		},
																		"MatchFilenameRegex__": {
																			"type": "CheckBox",
																			"data": [
																				"MatchFilenameRegex__"
																			],
																			"options": {
																				"label": "_Match filename with regular expression",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Select this option to apply this configuration according to a specific filename",
																				"helpURL": "7007",
																				"helpFormat": "HTML Format",
																				"version": 0,
																				"readonly": false
																			},
																			"stamp": 519
																		},
																		"LabelConfigurationSelection_Path__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_Path__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_Path__",
																				"version": 0
																			},
																			"stamp": 401
																		},
																		"ConfigurationSelection_Path__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationSelection_Path__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ConfigurationSelection_Path__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 100,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 402
																		},
																		"LabelUseTextInside__": {
																			"type": "Label",
																			"data": [
																				"MatchStringWithinDocument__"
																			],
																			"options": {
																				"label": "_Match string within document",
																				"version": 0
																			},
																			"stamp": 522
																		},
																		"MatchStringWithinDocument__": {
																			"type": "CheckBox",
																			"data": [
																				"MatchStringWithinDocument__"
																			],
																			"options": {
																				"label": "_Match string within document",
																				"activable": true,
																				"width": 230,
																				"helpText": "_Select this option to apply this configuration according to a string in the document",
																				"helpURL": "7008",
																				"helpFormat": "HTML Format",
																				"version": 0,
																				"defaultValue": true
																			},
																			"stamp": 523
																		},
																		"LabelConfigurationSelection_Area__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_Area__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_Area__",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"ConfigurationSelection_Area__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationSelection_Area__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ConfigurationSelection_Area__",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false
																			},
																			"stamp": 29
																		},
																		"LabelConfigurationSelection_CriteriaMatchCase__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_CriteriaMatchCase__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"LabelConfigurationSelection_Criteria__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_Criteria__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_Criteria__",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"ConfigurationSelection_CriteriaMatchCase__": {
																			"type": "CheckBox",
																			"data": [
																				"ConfigurationSelection_CriteriaMatchCase__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 33
																		},
																		"ConfigurationSelection_Criteria__": {
																			"type": "ShortText",
																			"data": [
																				"ConfigurationSelection_Criteria__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "ConfigurationSelection_Criteria__",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false
																			},
																			"stamp": 31
																		},
																		"LabelConfigurationSelection_CriteriaIsRegex__": {
																			"type": "Label",
																			"data": [
																				"ConfigurationSelection_CriteriaIsRegex__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_CriteriaIsRegex__",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"ConfigurationSelection_CriteriaIsRegex__": {
																			"type": "CheckBox",
																			"data": [
																				"ConfigurationSelection_CriteriaIsRegex__"
																			],
																			"options": {
																				"label": "ConfigurationSelection_CriteriaIsRegex__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 35
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-14": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 36,
											"*": {
												"Features_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Features_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"stamp": 37,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelMoreSubmissionOptions__": "MoreSubmissionOptions__",
																	"MoreSubmissionOptions__": "LabelMoreSubmissionOptions__"
																},
																"version": 0
															},
															"stamp": 38,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelMoreSubmissionOptions__": {
																				"line": 1,
																				"column": 1
																			},
																			"MoreSubmissionOptions__": {
																				"line": 1,
																				"column": 2
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 39,
																	"*": {
																		"MoreSubmissionOptions__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": " ",
																				"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																				"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																				"version": 0
																			},
																			"stamp": 42
																		},
																		"LabelMoreSubmissionOptions__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 43
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-16": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 54,
											"*": {
												"General_advanced_options_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "General_advanced_options_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 55,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"ForwardAdditionalAttach__": "LabelForwardAdditionalAttach__",
																	"LabelForwardAdditionalAttach__": "ForwardAdditionalAttach__",
																	"Billing_Info_Billing_Account__": "LabelBilling_Info_Billing_Account__",
																	"LabelBilling_Info_Billing_Account__": "Billing_Info_Billing_Account__",
																	"Billing_Info_Cost_Center__": "LabelBilling_Info_Cost_Center__",
																	"LabelBilling_Info_Cost_Center__": "Billing_Info_Cost_Center__",
																	"Redirect_sender_notifications__": "LabelRedirect_sender_notifications__",
																	"LabelRedirect_sender_notifications__": "Redirect_sender_notifications__",
																	"Redirect_recipient_notifications__": "LabelRedirect_recipient_notifications__",
																	"LabelRedirect_recipient_notifications__": "Redirect_recipient_notifications__",
																	"Simulate_delivery__": "LabelSimulate_delivery__",
																	"LabelSimulate_delivery__": "Simulate_delivery__",
																	"TemplateIdList__": "LabelTemplateIdList__",
																	"LabelTemplateIdList__": "TemplateIdList__",
																	"LabelConfiguration_template__": "Configuration_template__",
																	"Configuration_template__": "LabelConfiguration_template__",
																	"OutputFilenamePattern__": "LabelOutputFilenamePattern__",
																	"LabelOutputFilenamePattern__": "OutputFilenamePattern__",
																	"EndOfValidityNotification__": "LabelEndOfValidityNotification__",
																	"LabelEndOfValidityNotification__": "EndOfValidityNotification__",
																	"IsAutolearningConf__": "LabelIsAutolearningConf__",
																	"LabelIsAutolearningConf__": "IsAutolearningConf__"
																},
																"version": 0
															},
															"stamp": 56,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Redirect_email_to__": {
																				"line": 1,
																				"column": 1
																			},
																			"Advanced__": {
																				"line": 7,
																				"column": 1
																			},
																			"ForwardAdditionalAttach__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelForwardAdditionalAttach__": {
																				"line": 9,
																				"column": 1
																			},
																			"Billing_Info_Billing_Account__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelBilling_Info_Billing_Account__": {
																				"line": 5,
																				"column": 1
																			},
																			"Billing_Info_Cost_Center__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelBilling_Info_Cost_Center__": {
																				"line": 6,
																				"column": 1
																			},
																			"Redirect_sender_notifications__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelRedirect_sender_notifications__": {
																				"line": 2,
																				"column": 1
																			},
																			"Redirect_recipient_notifications__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelRedirect_recipient_notifications__": {
																				"line": 3,
																				"column": 1
																			},
																			"Simulate_delivery__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelSimulate_delivery__": {
																				"line": 8,
																				"column": 1
																			},
																			"Billing_Info_part__": {
																				"line": 4,
																				"column": 1
																			},
																			"TemplateIdList__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelTemplateIdList__": {
																				"line": 10,
																				"column": 1
																			},
																			"LabelConfiguration_template__": {
																				"line": 11,
																				"column": 1
																			},
																			"Configuration_template__": {
																				"line": 11,
																				"column": 2
																			},
																			"OutputFilenamePattern__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelOutputFilenamePattern__": {
																				"line": 12,
																				"column": 1
																			},
																			"EndOfValidityNotification__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelEndOfValidityNotification__": {
																				"line": 13,
																				"column": 1
																			},
																			"IsAutolearningConf__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelIsAutolearningConf__": {
																				"line": 14,
																				"column": 1
																			}
																		},
																		"lines": 14,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 57,
																	"*": {
																		"Configuration_template__": {
																			"type": "CheckBox",
																			"data": [
																				"Configuration_template__"
																			],
																			"options": {
																				"label": "Configuration_template__",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "_Select this option to specify that this configuration is used as a base to create a new auto learning configuration",
																				"helpURL": "7009",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 509
																		},
																		"LabelConfiguration_template__": {
																			"type": "Label",
																			"data": [
																				"Configuration_template__"
																			],
																			"options": {
																				"label": "Configuration_template__",
																				"version": 0
																			},
																			"stamp": 508
																		},
																		"Redirect_email_to__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "Redirect_email_to_part",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 62
																		},
																		"LabelRedirect_sender_notifications__": {
																			"type": "Label",
																			"data": [
																				"Redirect_sender_notifications__"
																			],
																			"options": {
																				"label": "Redirect_sender_notifications__",
																				"version": 0
																			},
																			"stamp": 63
																		},
																		"Redirect_sender_notifications__": {
																			"type": "ShortText",
																			"data": [
																				"Redirect_sender_notifications__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Redirect_sender_notifications__",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"length": 250,
																				"browsable": false,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 64
																		},
																		"Redirect_recipient_notifications__": {
																			"type": "ShortText",
																			"data": [
																				"Redirect_recipient_notifications__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Redirect_recipient_notifications__",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"length": 250,
																				"browsable": false,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 76
																		},
																		"LabelRedirect_recipient_notifications__": {
																			"type": "Label",
																			"data": [
																				"Redirect_recipient_notifications__"
																			],
																			"options": {
																				"label": "Redirect_recipient_notifications__",
																				"version": 0
																			},
																			"stamp": 77
																		},
																		"Billing_Info_part__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "Billing_Info_part",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 425
																		},
																		"LabelBilling_Info_Billing_Account__": {
																			"type": "Label",
																			"data": [
																				"Billing_Info_Billing_Account__"
																			],
																			"options": {
																				"label": "Billing_Info_Billing_Account__",
																				"version": 0
																			},
																			"stamp": 426
																		},
																		"Billing_Info_Billing_Account__": {
																			"type": "ShortText",
																			"data": [
																				"Billing_Info_Billing_Account__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Billing_Info_Billing_Account__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 427
																		},
																		"LabelBilling_Info_Cost_Center__": {
																			"type": "Label",
																			"data": [
																				"Billing_Info_Cost_Center__"
																			],
																			"options": {
																				"label": "Billing_Info_Cost_Center__",
																				"version": 0
																			},
																			"stamp": 428
																		},
																		"Billing_Info_Cost_Center__": {
																			"type": "ShortText",
																			"data": [
																				"Billing_Info_Cost_Center__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Billing_Info_Cost_Center__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0
																			},
																			"stamp": 429
																		},
																		"Advanced__": {
																			"type": "Title",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "Advanced_part",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 97
																		},
																		"LabelSimulate_delivery__": {
																			"type": "Label",
																			"data": [
																				"Simulate_delivery__"
																			],
																			"options": {
																				"label": "Simulate_delivery__",
																				"version": 0
																			},
																			"stamp": 98
																		},
																		"Simulate_delivery__": {
																			"type": "CheckBox",
																			"data": [
																				"Simulate_delivery__"
																			],
																			"options": {
																				"label": "Simulate_delivery__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 99
																		},
																		"LabelForwardAdditionalAttach__": {
																			"type": "Label",
																			"data": [
																				"ForwardAdditionalAttach__"
																			],
																			"options": {
																				"label": "ForwardAdditionalAttach__",
																				"version": 0
																			},
																			"stamp": 102
																		},
																		"ForwardAdditionalAttach__": {
																			"type": "CheckBox",
																			"data": [
																				"ForwardAdditionalAttach__"
																			],
																			"options": {
																				"label": "ForwardAdditionalAttach__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"version": 0
																			},
																			"stamp": 103
																		},
																		"LabelTemplateIdList__": {
																			"type": "Label",
																			"data": [
																				"TemplateIdList__"
																			],
																			"options": {
																				"label": "TemplateIdList",
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 510
																		},
																		"TemplateIdList__": {
																			"type": "ShortText",
																			"data": [
																				"TemplateIdList__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "TemplateIdList",
																				"activable": true,
																				"hidden": true,
																				"width": 230
																			},
																			"stamp": 511
																		},
																		"LabelIsAutolearningConf__": {
																			"type": "Label",
																			"data": [
																				"IsAutolearningConf__"
																			],
																			"options": {
																				"label": "IsAutolearningConf__",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 600
																		},
																		"IsAutolearningConf__": {
																			"type": "CheckBox",
																			"data": [
																				"IsAutolearningConf__"
																			],
																			"options": {
																				"label": "IsAutolearningConf__",
																				"activable": true,
																				"readonly": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"hidden": true
																			},
																			"stamp": 601
																		},
																		"LabelOutputFilenamePattern__": {
																			"type": "Label",
																			"data": [
																				"OutputFilenamePattern__"
																			],
																			"options": {
																				"label": "OutputFilenamePattern__",
																				"version": 0
																			},
																			"stamp": 524
																		},
																		"OutputFilenamePattern__": {
																			"type": "ShortText",
																			"data": [
																				"OutputFilenamePattern__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "OutputFilenamePattern__",
																				"activable": true,
																				"width": 230,
																				"helpText": "Output_FileName_Pattern_Tooltip",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 200,
																				"defaultValue": "%[Document_Type__] %[Document_ID__]",
																				"browsable": false
																			},
																			"stamp": 525
																		},
																		"LabelEndOfValidityNotification__": {
																			"type": "Label",
																			"data": [
																				"EndOfValidityNotification__"
																			],
																			"options": {
																				"label": "EndOfValidityNotification__",
																				"version": 0
																			},
																			"stamp": 536
																		},
																		"EndOfValidityNotification__": {
																			"type": "CheckBox",
																			"data": [
																				"EndOfValidityNotification__"
																			],
																			"options": {
																				"label": "EndOfValidityNotification__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 537
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-5": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 104,
											"*": {
												"Splitting_pane": {
													"type": "PanelData",
													"options": {
														"border": {
															"collapsed": false
														},
														"version": 0,
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Splitting_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Split_Location__": "LabelSplit_Location__",
																	"LabelSplit_Location__": "Split_Location__",
																	"Split_Offset__": "LabelSplit_Offset__",
																	"LabelSplit_Offset__": "Split_Offset__",
																	"Split_DivisionMethod__": "LabelSplit_DivisionMethod__",
																	"LabelSplit_DivisionMethod__": "Split_DivisionMethod__",
																	"Split_Area__": "LabelSplit_Area__",
																	"LabelSplit_Area__": "Split_Area__",
																	"Split_NumberPages__": "LabelSplit_NumberPages__",
																	"LabelSplit_NumberPages__": "Split_NumberPages__",
																	"Split_String__": "LabelSplit_String__",
																	"LabelSplit_String__": "Split_String__",
																	"Split_UseRegex__": "LabelSplit_UseRegex__",
																	"LabelSplit_UseRegex__": "Split_UseRegex__",
																	"Split_CaseSensitive__": "LabelSplit_CaseSensitive__",
																	"LabelSplit_CaseSensitive__": "Split_CaseSensitive__",
																	"Split_AreaMustBeFilled__": "LabelSplit_AreaMustBeFilled__",
																	"LabelSplit_AreaMustBeFilled__": "Split_AreaMustBeFilled__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 11,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Split_Location__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelSplit_Location__": {
																				"line": 7,
																				"column": 1
																			},
																			"Split_Offset__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelSplit_Offset__": {
																				"line": 3,
																				"column": 1
																			},
																			"Split_DivisionMethod__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelSplit_DivisionMethod__": {
																				"line": 4,
																				"column": 1
																			},
																			"Split_Area__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelSplit_Area__": {
																				"line": 10,
																				"column": 1
																			},
																			"Split_NumberPages__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelSplit_NumberPages__": {
																				"line": 5,
																				"column": 1
																			},
																			"Split_String__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelSplit_String__": {
																				"line": 6,
																				"column": 1
																			},
																			"Split_UseRegex__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelSplit_UseRegex__": {
																				"line": 8,
																				"column": 1
																			},
																			"Split_CaseSensitive__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelSplit_CaseSensitive__": {
																				"line": 9,
																				"column": 1
																			},
																			"HTMLSplittingPreview__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line3__": {
																				"line": 2,
																				"column": 1
																			},
																			"Split_AreaMustBeFilled__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelSplit_AreaMustBeFilled__": {
																				"line": 11,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"HTMLSplittingPreview__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "Splitting_Preview",
																				"css": "@ .preview {\nposition: absolute;\nleft: 220px;\ntop: 0px;\n}@ \n.preview td {\n\ttext-align: left;\n\tfont-size: 20pt;\nheight: 20px;\n}@ \n.preview td.empty {\nwidth: 42px;\n}@ \n.preview td.split {\n\tfont-size: 10pt;\n\tcolor: red;\n}@ \n.dots {\n\tposition: relative;\n\tleft: 7px;\n\ttop: 2px;\n\tborder-left: dashed 1px red;\n\theight: 25px;\n\twidth: 2px;\n}@ \n\n.key {\n\tposition: relative;\n\tfloat: left;\n\tleft: 7px;\n\ttop: 15px;\n\tborder-left: solid 1px black;\n\theight: 15px;\n\twidth: 2px;\n}@ \n.key.right {\n\tfloat: right;\n\tleft: inherit;\n\tright: 10px;\n}@ \n.preview td.keystring {\n\ttext-align: left;\n\tfont-size: 10pt;\n}@ \n.preview td.keystring.right {\n\ttext-align: right;\n}@ \n.elli {\n\tfont-size: 10pt;\n}@ \nspan {\n\tpadding: 3px;\n}@ \n.preview td.docbordernone {\n\theight: 7px;\n}@ \n.preview td.docborderend {\n\tborder-bottom: solid 2px LightGray;\n\tborder-left: solid 2px LightGray;\n\theight: 7px;\n}@ \n.preview td.docborder {\n\tborder-bottom: solid 2px LightGray;\n\tborder-left: solid 2px LightGray;\n\tborder-right: solid 2px LightGray;\n\theight: 7px;\n}@ \n\n.preview td.doc {\n\tfont-size: 10pt;\n\ttext-align: center;\n}@ ",
																				"width": 962,
																				"version": 0
																			},
																			"stamp": 105
																		},
																		"Spacer_line3__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "100",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line3",
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"LabelSplit_Offset__": {
																			"type": "Label",
																			"data": [
																				"Split_Offset__"
																			],
																			"options": {
																				"label": "Split_Offset__",
																				"version": 0
																			},
																			"stamp": 107
																		},
																		"Split_Offset__": {
																			"type": "Integer",
																			"data": [
																				"Split_Offset__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "Split_Offset__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "250",
																				"browsable": false,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 108
																		},
																		"LabelSplit_DivisionMethod__": {
																			"type": "Label",
																			"data": [
																				"Split_DivisionMethod__"
																			],
																			"options": {
																				"label": "Split_DivisionMethod__",
																				"version": 0
																			},
																			"stamp": 109
																		},
																		"Split_DivisionMethod__": {
																			"type": "ComboBox",
																			"data": [
																				"Split_DivisionMethod__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Split_DivisionMethod_SIMPLE",
																					"1": "_Split_DivisionMethod_NPAGES",
																					"2": "_Split_DivisionMethod_ONSTRING",
																					"3": "_Split_DivisionMethod_ONAREA"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "SIMPLE",
																					"1": "NPAGES",
																					"2": "ONSTRING",
																					"3": "ONAREA"
																				},
																				"label": "Split_DivisionMethod__",
																				"activable": true,
																				"width": "250",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 110
																		},
																		"LabelSplit_NumberPages__": {
																			"type": "Label",
																			"data": [
																				"Split_NumberPages__"
																			],
																			"options": {
																				"label": "Split_NumberPages__",
																				"version": 0
																			},
																			"stamp": 111
																		},
																		"Split_NumberPages__": {
																			"type": "Integer",
																			"data": [
																				"Split_NumberPages__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "Split_NumberPages__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "250",
																				"defaultValue": "",
																				"browsable": false
																			},
																			"stamp": 112
																		},
																		"LabelSplit_String__": {
																			"type": "Label",
																			"data": [
																				"Split_String__"
																			],
																			"options": {
																				"label": "Split_String__",
																				"version": 0
																			},
																			"stamp": 113
																		},
																		"Split_String__": {
																			"type": "ShortText",
																			"data": [
																				"Split_String__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Split_String__",
																				"activable": true,
																				"width": "250",
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 114
																		},
																		"LabelSplit_Location__": {
																			"type": "Label",
																			"data": [
																				"Split_Location__"
																			],
																			"options": {
																				"label": "Split_Location__",
																				"version": 0
																			},
																			"stamp": 115
																		},
																		"Split_Location__": {
																			"type": "ComboBox",
																			"data": [
																				"Split_Location__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Split_Location_Before",
																					"1": "_Split_Location_After"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "1"
																				},
																				"label": "Split_Location__",
																				"activable": true,
																				"width": "250",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 116
																		},
																		"LabelSplit_UseRegex__": {
																			"type": "Label",
																			"data": [
																				"Split_UseRegex__"
																			],
																			"options": {
																				"label": "Split_UseRegex__",
																				"version": 0
																			},
																			"stamp": 117
																		},
																		"Split_UseRegex__": {
																			"type": "CheckBox",
																			"data": [
																				"Split_UseRegex__"
																			],
																			"options": {
																				"label": "Split_UseRegex__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 118
																		},
																		"LabelSplit_CaseSensitive__": {
																			"type": "Label",
																			"data": [
																				"Split_CaseSensitive__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"version": 0
																			},
																			"stamp": 119
																		},
																		"Split_CaseSensitive__": {
																			"type": "CheckBox",
																			"data": [
																				"Split_CaseSensitive__"
																			],
																			"options": {
																				"label": "MatchCase__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 120
																		},
																		"LabelSplit_Area__": {
																			"type": "Label",
																			"data": [
																				"Split_Area__"
																			],
																			"options": {
																				"label": "Split_Area__",
																				"version": 0
																			},
																			"stamp": 121
																		},
																		"Split_Area__": {
																			"type": "ShortText",
																			"data": [
																				"Split_Area__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Split_Area__",
																				"activable": true,
																				"width": "250",
																				"browsable": false,
																				"version": 0,
																				"autocompletable": false,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 122
																		},
																		"LabelSplit_AreaMustBeFilled__": {
																			"type": "Label",
																			"data": [
																				"Split_AreaMustBeFilled__"
																			],
																			"options": {
																				"label": "Split_AreaMustBeFilled__",
																				"version": 0
																			},
																			"stamp": 534
																		},
																		"Split_AreaMustBeFilled__": {
																			"type": "CheckBox",
																			"data": [
																				"Split_AreaMustBeFilled__"
																			],
																			"options": {
																				"label": "Split_AreaMustBeFilled__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 535
																		}
																	},
																	"stamp": 123
																}
															},
															"stamp": 124,
															"data": []
														}
													},
													"stamp": 125,
													"data": []
												}
											}
										},
										"form-content-center-6": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 126,
											"*": {
												"Validation_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Validation_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"stamp": 127,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelEnableErrorNotification__": "EnableErrorNotification__",
																	"EnableErrorNotification__": "LabelEnableErrorNotification__",
																	"ForceValidation__": "LabelForceValidation__",
																	"LabelForceValidation__": "ForceValidation__",
																	"MoreValidationOptions__": "LabelMoreValidationOptions__",
																	"LabelMoreValidationOptions__": "MoreValidationOptions__",
																	"Enable_conversation__": "LabelEnable_conversation__",
																	"LabelEnable_conversation__": "Enable_conversation__"
																},
																"version": 0
															},
															"stamp": 128,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ForceValidation__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelForceValidation__": {
																				"line": 1,
																				"column": 1
																			},
																			"LabelEnableErrorNotification__": {
																				"line": 2,
																				"column": 1
																			},
																			"EnableErrorNotification__": {
																				"line": 2,
																				"column": 2
																			},
																			"MoreValidationOptions__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelMoreValidationOptions__": {
																				"line": 3,
																				"column": 1
																			},
																			"Enable_conversation__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelEnable_conversation__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 129,
																	"*": {
																		"EnableErrorNotification__": {
																			"type": "CheckBox",
																			"data": [
																				"EnableErrorNotification__"
																			],
																			"options": {
																				"label": "EnableErrorNotification__",
																				"activable": true,
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 130
																		},
																		"LabelEnableErrorNotification__": {
																			"type": "Label",
																			"data": [
																				"EnableErrorNotification__"
																			],
																			"options": {
																				"label": "EnableErrorNotification__",
																				"version": 0
																			},
																			"stamp": 131
																		},
																		"LabelForceValidation__": {
																			"type": "Label",
																			"data": [
																				"ForceValidation__"
																			],
																			"options": {
																				"label": "ForceValidation__",
																				"version": 0
																			},
																			"stamp": 132
																		},
																		"ForceValidation__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceValidation__"
																			],
																			"options": {
																				"label": "ForceValidation__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 133
																		},
																		"LabelMoreValidationOptions__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 478
																		},
																		"MoreValidationOptions__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0,
																				"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																				"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ "
																			},
																			"stamp": 479
																		},
																		"LabelEnable_conversation__": {
																			"type": "Label",
																			"data": [
																				"Enable_conversation__"
																			],
																			"options": {
																				"label": "Enable conversation",
																				"version": 0
																			},
																			"stamp": 602
																		},
																		"Enable_conversation__": {
																			"type": "CheckBox",
																			"data": [
																				"Enable_conversation__"
																			],
																			"options": {
																				"label": "Enable conversation",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 603
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-7": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 581,
											"*": {
												"ButtonBar_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "ButtonBar_pane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"labelLength": "300px",
														"sameHeightAsSiblings": false
													},
													"stamp": 582,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelDisplaySaveButton__": "DisplaySaveButton__",
																	"DisplaySaveButton__": "LabelDisplaySaveButton__",
																	"LabelDisplayResubmitButton__": "DisplayResubmitButton__",
																	"DisplayResubmitButton__": "LabelDisplayResubmitButton__",
																	"LabelDisplayRejectButton__": "DisplayRejectButton__",
																	"DisplayRejectButton__": "LabelDisplayRejectButton__",
																	"LabelDisplaySaveAndQuitButton__": "DisplaySaveAndQuitButton__",
																	"DisplaySaveAndQuitButton__": "LabelDisplaySaveAndQuitButton__",
																	"LabelDisplaySetAsideButton__": "DisplaySetAsideButton__",
																	"DisplaySetAsideButton__": "LabelDisplaySetAsideButton__"
																},
																"version": 0
															},
															"stamp": 586,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelDisplaySaveButton__": {
																				"line": 2,
																				"column": 1
																			},
																			"DisplaySaveButton__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDisplayResubmitButton__": {
																				"line": 4,
																				"column": 1
																			},
																			"DisplayResubmitButton__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDisplayRejectButton__": {
																				"line": 1,
																				"column": 1
																			},
																			"DisplayRejectButton__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDisplaySaveAndQuitButton__": {
																				"line": 3,
																				"column": 1
																			},
																			"DisplaySaveAndQuitButton__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDisplaySetAsideButton__": {
																				"line": 5,
																				"column": 1
																			},
																			"DisplaySetAsideButton__": {
																				"line": 5,
																				"column": 2
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 587,
																	"*": {
																		"LabelDisplayRejectButton__": {
																			"type": "Label",
																			"data": [
																				"DisplayRejectButton__"
																			],
																			"options": {
																				"label": "DisplayRejectButton__",
																				"version": 0
																			},
																			"stamp": 588
																		},
																		"DisplayRejectButton__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplayRejectButton__"
																			],
																			"options": {
																				"label": "DisplayRejectButton__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 589
																		},
																		"DisplaySaveAndQuitButton__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplaySaveAndQuitButton__"
																			],
																			"options": {
																				"label": "DisplaySaveAndQuitButton__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 590
																		},
																		"LabelDisplaySaveAndQuitButton__": {
																			"type": "Label",
																			"data": [
																				"DisplaySaveAndQuitButton__"
																			],
																			"options": {
																				"label": "DisplaySaveAndQuitButton__",
																				"version": 0
																			},
																			"stamp": 591
																		},
																		"DisplaySaveButton__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplaySaveButton__"
																			],
																			"options": {
																				"label": "DisplaySaveButton__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 592
																		},
																		"LabelDisplaySaveButton__": {
																			"type": "Label",
																			"data": [
																				"DisplaySaveButton__"
																			],
																			"options": {
																				"label": "DisplaySaveButton__",
																				"version": 0
																			},
																			"stamp": 593
																		},
																		"LabelDisplayResubmitButton__": {
																			"type": "Label",
																			"data": [
																				"DisplayResubmitButton__"
																			],
																			"options": {
																				"label": "DisplayResubmitButton__",
																				"version": 0
																			},
																			"stamp": 594
																		},
																		"DisplayResubmitButton__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplayResubmitButton__"
																			],
																			"options": {
																				"label": "DisplayResubmitButton__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 595
																		},
																		"LabelDisplaySetAsideButton__": {
																			"type": "Label",
																			"data": [
																				"DisplaySetAsideButton__"
																			],
																			"options": {
																				"label": "DisplaySetAsideButton__",
																				"version": 0
																			},
																			"stamp": 610
																		},
																		"DisplaySetAsideButton__": {
																			"type": "CheckBox",
																			"data": [
																				"DisplaySetAsideButton__"
																			],
																			"options": {
																				"label": "DisplaySetAsideButton__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 611
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-20": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 476,
											"*": {
												"Advanced_validation_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "Advanced_validation_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 470,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelForceMessageValidation_MOD__": "ForceMessageValidation_MOD__",
																	"ForceMessageValidation_MOD__": "LabelForceMessageValidation_MOD__",
																	"LabelForceMessageValidation_NOTIF_EMAIL__": "ForceMessageValidation_NOTIF_EMAIL__",
																	"ForceMessageValidation_NOTIF_EMAIL__": "LabelForceMessageValidation_NOTIF_EMAIL__",
																	"LabelForceMessageValidation_WELCOME_EMAIL__": "ForceMessageValidation_WELCOME_EMAIL__",
																	"ForceMessageValidation_WELCOME_EMAIL__": "LabelForceMessageValidation_WELCOME_EMAIL__",
																	"LabelForceMessageValidation_EMAIL__": "ForceMessageValidation_EMAIL__",
																	"ForceMessageValidation_EMAIL__": "LabelForceMessageValidation_EMAIL__",
																	"LabelForceMessageValidation_FAX__": "ForceMessageValidation_FAX__",
																	"ForceMessageValidation_FAX__": "LabelForceMessageValidation_FAX__"
																},
																"version": 0
															},
															"stamp": 471,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelForceMessageValidation_MOD__": {
																				"line": 1,
																				"column": 1
																			},
																			"ForceMessageValidation_MOD__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelForceMessageValidation_NOTIF_EMAIL__": {
																				"line": 2,
																				"column": 1
																			},
																			"ForceMessageValidation_NOTIF_EMAIL__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelForceMessageValidation_WELCOME_EMAIL__": {
																				"line": 3,
																				"column": 1
																			},
																			"ForceMessageValidation_WELCOME_EMAIL__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelForceMessageValidation_EMAIL__": {
																				"line": 4,
																				"column": 1
																			},
																			"ForceMessageValidation_EMAIL__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelForceMessageValidation_FAX__": {
																				"line": 5,
																				"column": 1
																			},
																			"ForceMessageValidation_FAX__": {
																				"line": 5,
																				"column": 2
																			}
																		},
																		"lines": 5,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 472,
																	"*": {
																		"ForceMessageValidation_FAX__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceMessageValidation_FAX__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_FAX__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 60
																		},
																		"LabelForceMessageValidation_FAX__": {
																			"type": "Label",
																			"data": [
																				"ForceMessageValidation_FAX__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_FAX__",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"ForceMessageValidation_EMAIL__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceMessageValidation_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_EMAIL__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"LabelForceMessageValidation_EMAIL__": {
																			"type": "Label",
																			"data": [
																				"ForceMessageValidation_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_EMAIL__",
																				"version": 0
																			},
																			"stamp": 66
																		},
																		"ForceMessageValidation_WELCOME_EMAIL__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceMessageValidation_WELCOME_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_WELCOME_EMAIL__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 67
																		},
																		"LabelForceMessageValidation_WELCOME_EMAIL__": {
																			"type": "Label",
																			"data": [
																				"ForceMessageValidation_WELCOME_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_WELCOME_EMAIL__",
																				"version": 0
																			},
																			"stamp": 69
																		},
																		"ForceMessageValidation_NOTIF_EMAIL__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceMessageValidation_NOTIF_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_NOTIF_EMAIL__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 72
																		},
																		"LabelForceMessageValidation_NOTIF_EMAIL__": {
																			"type": "Label",
																			"data": [
																				"ForceMessageValidation_NOTIF_EMAIL__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_NOTIF_EMAIL__",
																				"version": 0
																			},
																			"stamp": 73
																		},
																		"ForceMessageValidation_MOD__": {
																			"type": "CheckBox",
																			"data": [
																				"ForceMessageValidation_MOD__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_MOD__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 70
																		},
																		"LabelForceMessageValidation_MOD__": {
																			"type": "Label",
																			"data": [
																				"ForceMessageValidation_MOD__"
																			],
																			"options": {
																				"label": "ForceMessageValidation_MOD__",
																				"version": 0
																			},
																			"stamp": 68
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-26": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 140,
											"*": {
												"Formatting_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Formatting_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 141,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Add_backgrounds__": "LabelAdd_backgrounds__",
																	"LabelAdd_backgrounds__": "Add_backgrounds__",
																	"Use_a_specific_background_for_the_first_page__": "LabelUse_a_specific_background_for_the_first_page__",
																	"LabelUse_a_specific_background_for_the_first_page__": "Use_a_specific_background_for_the_first_page__",
																	"Alternate_background_on_front_and_back_pages__": "LabelAlternate_background_on_front_and_back_pages__",
																	"LabelAlternate_background_on_front_and_back_pages__": "Alternate_background_on_front_and_back_pages__",
																	"BackPage__": "LabelBackPage__",
																	"LabelBackPage__": "BackPage__",
																	"FrontPage__": "LabelFrontPage__",
																	"LabelFrontPage__": "FrontPage__",
																	"Add_T_C__": "LabelAdd_T_C__",
																	"LabelAdd_T_C__": "Add_T_C__",
																	"Terms___conditions_file__": "LabelTerms___conditions_file__",
																	"LabelTerms___conditions_file__": "Terms___conditions_file__",
																	"Terms___conditions_position__": "LabelTerms___conditions_position__",
																	"LabelTerms___conditions_position__": "Terms___conditions_position__",
																	"MoreFormattingOptions__": "LabelMoreFormattingOptions__",
																	"LabelMoreFormattingOptions__": "MoreFormattingOptions__",
																	"FirstPage__": "LabelFirstPage__",
																	"LabelFirstPage__": "FirstPage__",
																	"Document_template__": "LabelDocument_template__",
																	"LabelDocument_template__": "Document_template__",
																	"FieldsOrder__": "LabelFieldsOrder__",
																	"LabelFieldsOrder__": "FieldsOrder__"
																},
																"version": 0
															},
															"stamp": 142,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Add_backgrounds__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelAdd_backgrounds__": {
																				"line": 3,
																				"column": 1
																			},
																			"Use_a_specific_background_for_the_first_page__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelUse_a_specific_background_for_the_first_page__": {
																				"line": 4,
																				"column": 1
																			},
																			"HTML_FirstPage__": {
																				"line": 6,
																				"column": 1
																			},
																			"Alternate_background_on_front_and_back_pages__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelAlternate_background_on_front_and_back_pages__": {
																				"line": 5,
																				"column": 1
																			},
																			"BackPage__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelBackPage__": {
																				"line": 11,
																				"column": 1
																			},
																			"FrontPage__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelFrontPage__": {
																				"line": 9,
																				"column": 1
																			},
																			"HTML_FrontPage__": {
																				"line": 8,
																				"column": 1
																			},
																			"HTML_BackPage__": {
																				"line": 10,
																				"column": 1
																			},
																			"HTML_BackgroundPreview__": {
																				"line": 1,
																				"column": 1
																			},
																			"Add_T_C__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelAdd_T_C__": {
																				"line": 12,
																				"column": 1
																			},
																			"Terms___conditions_file__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelTerms___conditions_file__": {
																				"line": 15,
																				"column": 1
																			},
																			"HTML_TC__": {
																				"line": 14,
																				"column": 1
																			},
																			"Terms___conditions_position__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelTerms___conditions_position__": {
																				"line": 13,
																				"column": 1
																			},
																			"MoreFormattingOptions__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelMoreFormattingOptions__": {
																				"line": 16,
																				"column": 1
																			},
																			"FirstPage__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelFirstPage__": {
																				"line": 7,
																				"column": 1
																			},
																			"Document_template__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDocument_template__": {
																				"line": 2,
																				"column": 1
																			},
																			"FieldsOrder__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelFieldsOrder__": {
																				"line": 17,
																				"column": 1
																			}
																		},
																		"lines": 17,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"HTML_BackgroundPreview__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML_BackgroundPreview__",
																				"htmlContent": "<div class=\"preview\">\n<span class=\"first\"><i class=\"fa fa-file-picture-o\"></i></span>\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																				"css": "@  .preview {\nfont-size: 20pt;\nposition: absolute;\nright: 10px;\ntop: 5px;\nborder-radius: 3px;\n}@ \n.preview span { padding-right: 2px; }@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ \n.tc { color: #00CC99; }@ ",
																				"width": 762,
																				"version": 0
																			},
																			"stamp": 144
																		},
																		"LabelDocument_template__": {
																			"type": "Label",
																			"data": [
																				"Document_template__"
																			],
																			"options": {
																				"label": "Document_template__",
																				"version": 0
																			},
																			"stamp": 404
																		},
																		"Document_template__": {
																			"type": "ShortText",
																			"data": [
																				"Document_template__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Document_template__",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 405
																		},
																		"LabelAdd_backgrounds__": {
																			"type": "Label",
																			"data": [
																				"Add_backgrounds__"
																			],
																			"options": {
																				"label": "Add_backgrounds__",
																				"version": 0
																			},
																			"stamp": 145
																		},
																		"Add_backgrounds__": {
																			"type": "CheckBox",
																			"data": [
																				"Add_backgrounds__"
																			],
																			"options": {
																				"label": "Add_backgrounds__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 146
																		},
																		"LabelUse_a_specific_background_for_the_first_page__": {
																			"type": "Label",
																			"data": [
																				"Use_a_specific_background_for_the_first_page__"
																			],
																			"options": {
																				"label": "Use_a_specific_background_for_the_first_page__",
																				"version": 0
																			},
																			"stamp": 147
																		},
																		"Use_a_specific_background_for_the_first_page__": {
																			"type": "CheckBox",
																			"data": [
																				"Use_a_specific_background_for_the_first_page__"
																			],
																			"options": {
																				"label": "Use_a_specific_background_for_the_first_page__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 148
																		},
																		"LabelAlternate_background_on_front_and_back_pages__": {
																			"type": "Label",
																			"data": [
																				"Alternate_background_on_front_and_back_pages__"
																			],
																			"options": {
																				"label": "Alternate_background_on_front_and_back_pages__",
																				"version": 0
																			},
																			"stamp": 149
																		},
																		"Alternate_background_on_front_and_back_pages__": {
																			"type": "CheckBox",
																			"data": [
																				"Alternate_background_on_front_and_back_pages__"
																			],
																			"options": {
																				"label": "Alternate_background_on_front_and_back_pages__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 150
																		},
																		"HTML_FirstPage__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML FirstPage",
																				"version": 0,
																				"htmlContent": "<div class=\"img\">\n<span class=\"first\"><i class=\"fa fa-file-picture-o\"></i></span>\n</div>",
																				"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																				"width": 762
																			},
																			"stamp": 151
																		},
																		"LabelFirstPage__": {
																			"type": "Label",
																			"data": [
																				"FirstPage__"
																			],
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 152
																		},
																		"FirstPage__": {
																			"type": "ShortText",
																			"data": [
																				"FirstPage__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": " ",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 153
																		},
																		"HTML_FrontPage__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML_FrontPage__",
																				"htmlContent": "<div class=\"img\">\n<span class=\"front\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																				"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																				"width": 745,
																				"version": 0
																			},
																			"stamp": 154
																		},
																		"LabelFrontPage__": {
																			"type": "Label",
																			"data": [
																				"FrontPage__"
																			],
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 155
																		},
																		"FrontPage__": {
																			"type": "ShortText",
																			"data": [
																				"FrontPage__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": " ",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 156
																		},
																		"HTML_BackPage__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML_BackPage__",
																				"htmlContent": "<div class=\"img\">\n<span class=\"back\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																				"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ ",
																				"width": 745,
																				"version": 0
																			},
																			"stamp": 157
																		},
																		"LabelBackPage__": {
																			"type": "Label",
																			"data": [
																				"BackPage__"
																			],
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 158
																		},
																		"BackPage__": {
																			"type": "ShortText",
																			"data": [
																				"BackPage__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": " ",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 159
																		},
																		"LabelAdd_T_C__": {
																			"type": "Label",
																			"data": [
																				"Add_T_C__"
																			],
																			"options": {
																				"label": "Add_T_C__",
																				"version": 0
																			},
																			"stamp": 160
																		},
																		"Add_T_C__": {
																			"type": "CheckBox",
																			"data": [
																				"Add_T_C__"
																			],
																			"options": {
																				"label": "Add_T_C__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 161
																		},
																		"LabelTerms___conditions_position__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_combo_box2__"
																			],
																			"options": {
																				"label": "Terms & conditions position",
																				"version": 0
																			},
																			"stamp": 162
																		},
																		"Terms___conditions_position__": {
																			"type": "ComboBox",
																			"data": [
																				"Terms___conditions_position__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Terms___conditions_position_FIRST",
																					"1": "_Terms___conditions_position_EACH",
																					"2": "_Terms___conditions_position_LAST"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "FIRST",
																					"1": "EACH",
																					"2": "LAST"
																				},
																				"label": "Terms___conditions_position__",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 163
																		},
																		"HTML_TC__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "HTML_TC__",
																				"htmlContent": "<div class=\"img\">\n<span class=\"tc\"><i class=\"fa fa-file-text-o\"></i></span>\n</div>",
																				"css": "@ .img {\nfont-size: 16pt;\nposition: absolute;\nleft: 278px;\nmargin-top: 0px;\n}@ \n.front { color: #6699FF; }@ \n.back { color: #FFCC99; }@ \n.tc { color: #00CC99; }@ ",
																				"width": 745,
																				"version": 0
																			},
																			"stamp": 164
																		},
																		"LabelTerms___conditions_file__": {
																			"type": "Label",
																			"data": [
																				"Terms___conditions_file__"
																			],
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 165
																		},
																		"Terms___conditions_file__": {
																			"type": "ShortText",
																			"data": [
																				"Terms___conditions_file__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": " ",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 166
																		},
																		"LabelMoreFormattingOptions__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 167
																		},
																		"MoreFormattingOptions__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": " ",
																				"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																				"css": "@ .adv { cursor: pointer; }@ \n.adv:hover { text-decoration:underline; }@ ",
																				"version": 0
																			},
																			"stamp": 168
																		},
																		"LabelFieldsOrder__": {
																			"type": "Label",
																			"data": [
																				"FieldsOrder__"
																			],
																			"options": {
																				"label": "FieldsOrder",
																				"hidden": true,
																				"version": 0
																			},
																			"stamp": 540
																		},
																		"FieldsOrder__": {
																			"type": "ShortText",
																			"data": [
																				"FieldsOrder__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "FieldsOrder",
																				"activable": true,
																				"width": "",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 2000,
																				"hidden": true,
																				"browsable": false
																			},
																			"stamp": 541
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-28": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 169,
											"*": {
												"Advanced_formatting_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "Advanced_formatting_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 170,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Document_Option_PDFCommand__": "LabelDocument_Option_PDFCommand__",
																	"LabelDocument_Option_PDFCommand__": "Document_Option_PDFCommand__"
																},
																"version": 0
															},
															"stamp": 171,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DescPDFCommand__2__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line20__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line21__": {
																				"line": 4,
																				"column": 1
																			},
																			"Document_Option_PDFCommand__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDocument_Option_PDFCommand__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 172,
																	"*": {
																		"Spacer_line20__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line20",
																				"version": 0
																			},
																			"stamp": 173
																		},
																		"DescPDFCommand__2__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescPDFCommand__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 174
																		},
																		"LabelDocument_Option_PDFCommand__": {
																			"type": "Label",
																			"data": [
																				"Document_Option_PDFCommand__"
																			],
																			"options": {
																				"label": "MOD_Option_PDFCommand__",
																				"version": 0
																			},
																			"stamp": 175
																		},
																		"Document_Option_PDFCommand__": {
																			"type": "ShortText",
																			"data": [
																				"Document_Option_PDFCommand__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "MOD_Option_PDFCommand__",
																				"activable": true,
																				"width": "100%",
																				"browsable": false,
																				"version": 0,
																				"length": 255,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"autocompletable": false
																			},
																			"stamp": 176
																		},
																		"Spacer_line21__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line21",
																				"version": 0
																			},
																			"stamp": 177
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-10": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 198,
											"*": {
												"Routing_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Routing_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 199,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"AutoCreateRecipients__": "LabelAutoCreateRecipients__",
																	"LabelAutoCreateRecipients__": "AutoCreateRecipients__",
																	"LabelBounceBacks_Enable__": "BounceBacks_Enable__",
																	"BounceBacks_Enable__": "LabelBounceBacks_Enable__",
																	"DefaultDelivery__": "LabelDefaultDelivery__",
																	"LabelDefaultDelivery__": "DefaultDelivery__",
																	"DeliveryMethodToUse__": "LabelDeliveryMethodToUse__",
																	"LabelDeliveryMethodToUse__": "DeliveryMethodToUse__",
																	"ProcessRelatedConfiguration__": "LabelProcessRelatedConfiguration__",
																	"LabelProcessRelatedConfiguration__": "ProcessRelatedConfiguration__",
																	"IsCOPAllowed__": "LabelIsCOPAllowed__",
																	"LabelIsCOPAllowed__": "IsCOPAllowed__",
																	"IsEmailAllowed__": "LabelIsEmailAllowed__",
																	"LabelIsEmailAllowed__": "IsEmailAllowed__",
																	"IsArchiveAllowed__": "LabelIsArchiveAllowed__",
																	"LabelIsArchiveAllowed__": "IsArchiveAllowed__",
																	"IsMODAllowed__": "LabelIsMODAllowed__",
																	"LabelIsMODAllowed__": "IsMODAllowed__",
																	"IsFaxAllowed__": "LabelIsFaxAllowed__",
																	"LabelIsFaxAllowed__": "IsFaxAllowed__",
																	"IsOtherAllowed__": "LabelIsOtherAllowed__",
																	"LabelIsOtherAllowed__": "IsOtherAllowed__",
																	"IsPortalAllowed__": "LabelIsPortalAllowed__",
																	"LabelIsPortalAllowed__": "IsPortalAllowed__",
																	"DefaultDeliveryEmail__": "Label_DefaultDeliveryEmail",
																	"Label_DefaultDeliveryEmail": "DefaultDeliveryEmail__",
																	"IsConversationAllowed__": "LabelIsConversationAllowed__",
																	"LabelIsConversationAllowed__": "IsConversationAllowed__",
																	"IsApplicationProcessAllowed__": "LabelIsApplicationProcessAllowed__",
																	"LabelIsApplicationProcessAllowed__": "IsApplicationProcessAllowed__",
																	"ProcessSelected__": "LabelProcessSelected__",
																	"LabelProcessSelected__": "ProcessSelected__",
																	"Target_configuration__": "LabelTarget_configuration__",
																	"LabelTarget_configuration__": "Target_configuration__"
																},
																"version": 0
															},
															"stamp": 200,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"AutoCreateRecipients__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelAutoCreateRecipients__": {
																				"line": 6,
																				"column": 1
																			},
																			"LabelBounceBacks_Enable__": {
																				"line": 5,
																				"column": 1
																			},
																			"BounceBacks_Enable__": {
																				"line": 5,
																				"column": 2
																			},
																			"DefaultDelivery__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDefaultDelivery__": {
																				"line": 2,
																				"column": 1
																			},
																			"DeliveryMethodToUse__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDeliveryMethodToUse__": {
																				"line": 1,
																				"column": 1
																			},
																			"ProcessRelatedConfiguration__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelProcessRelatedConfiguration__": {
																				"line": 3,
																				"column": 1
																			},
																			"IsCOPAllowed__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelIsCOPAllowed__": {
																				"line": 8,
																				"column": 1
																			},
																			"IsEmailAllowed__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelIsEmailAllowed__": {
																				"line": 9,
																				"column": 1
																			},
																			"IsArchiveAllowed__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelIsArchiveAllowed__": {
																				"line": 14,
																				"column": 1
																			},
																			"AllowedRoutingMethodsTitle__": {
																				"line": 7,
																				"column": 1
																			},
																			"IsMODAllowed__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelIsMODAllowed__": {
																				"line": 10,
																				"column": 1
																			},
																			"IsFaxAllowed__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelIsFaxAllowed__": {
																				"line": 12,
																				"column": 1
																			},
																			"IsOtherAllowed__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelIsOtherAllowed__": {
																				"line": 13,
																				"column": 1
																			},
																			"IsPortalAllowed__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelIsPortalAllowed__": {
																				"line": 11,
																				"column": 1
																			},
																			"Label_ShortText": {
																				"line": 4,
																				"column": 1
																			},
																			"DefaultDeliveryEmail__": {
																				"line": 4,
																				"column": 2
																			},
																			"IsConversationAllowed__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelIsConversationAllowed__": {
																				"line": 15,
																				"column": 1
																			},
																			"IsApplicationProcessAllowed__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelIsApplicationProcessAllowed__": {
																				"line": 16,
																				"column": 1
																			},
																			"ProcessSelected__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelProcessSelected__": {
																				"line": 17,
																				"column": 1
																			},
																			"Target_configuration__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelTarget_configuration__": {
																				"line": 18,
																				"column": 1
																			}
																		},
																		"lines": 18,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 201,
																	"*": {
																		"LabelDeliveryMethodToUse__": {
																			"type": "Label",
																			"data": [
																				"DeliveryMethodToUse__"
																			],
																			"options": {
																				"label": "DeliveryMethodToUse__",
																				"version": 0
																			},
																			"stamp": 483
																		},
																		"DeliveryMethodToUse__": {
																			"type": "RadioButton",
																			"data": [
																				"DeliveryMethodToUse__"
																			],
																			"options": {
																				"keys": [
																					"DeliveryMethodToUse_CUSTOMER",
																					"DeliveryMethodToUse_DEFAULT"
																				],
																				"values": [
																					"DeliveryMethodToUse_CUSTOMER",
																					"DeliveryMethodToUse_DEFAULT"
																				],
																				"label": "DeliveryMethodToUse__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 484
																		},
																		"LabelBounceBacks_Enable__": {
																			"type": "Label",
																			"data": [
																				"BounceBacks_Enable__"
																			],
																			"options": {
																				"label": "BounceBacks_Enable__",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"LabelDefaultDelivery__": {
																			"type": "Label",
																			"data": [
																				"DefaultDelivery__"
																			],
																			"options": {
																				"label": "DefaultDelivery__",
																				"version": 0
																			},
																			"stamp": 467
																		},
																		"LabelProcessRelatedConfiguration__": {
																			"type": "Label",
																			"data": [
																				"ProcessRelatedConfiguration__"
																			],
																			"options": {
																				"label": "ProcessRelatedConfiguration__",
																				"version": 0
																			},
																			"stamp": 530
																		},
																		"ProcessRelatedConfiguration__": {
																			"type": "ComboBox",
																			"data": [
																				"ProcessRelatedConfiguration__"
																			],
																			"options": {
																				"possibleValues": {},
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "ProcessRelatedConfiguration__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 531
																		},
																		"Label_ShortText": {
																			"type": "Label",
																			"data": [
																				"DefaultDeliveryEmail__"
																			],
																			"options": {
																				"label": "DefaultDeliveryEmail__",
																				"version": 0
																			},
																			"stamp": 583
																		},
																		"DefaultDeliveryEmail__": {
																			"type": "ShortText",
																			"data": [
																				"DefaultDeliveryEmail__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "DefaultDeliveryEmail__",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 584
																		},
																		"DefaultDelivery__": {
																			"type": "ComboBox",
																			"data": [
																				"DefaultDelivery__"
																			],
																			"options": {
																				"possibleKeys": {
																					"0": "MOD",
																					"1": "PORTAL",
																					"2": "SM",
																					"3": "FGFAXOUT",
																					"4": "OTHER",
																					"5": "NONE",
																					"6": "InboundCOP",
																					"7": "CONVERSATION",
																					"8": "APPLICATION_PROCESS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleValues": {
																					"0": "Default_MOD",
																					"1": "Default_PORTAL",
																					"2": "Default_EMAIL",
																					"3": "Default_FAX",
																					"4": "Default_OTHER",
																					"5": "Default_NONE",
																					"6": "CustomerOrderProcessing",
																					"7": "Default_CONVERSATION",
																					"8": "Default_APPLICATION_PROCESS"
																				},
																				"label": "DefaultDelivery__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 468
																		},
																		"BounceBacks_Enable__": {
																			"type": "CheckBox",
																			"data": [
																				"BounceBacks_Enable__"
																			],
																			"options": {
																				"label": "BounceBacks_Enable__",
																				"activable": true,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 48
																		},
																		"LabelAutoCreateRecipients__": {
																			"type": "Label",
																			"data": [
																				"AutoCreateRecipients__"
																			],
																			"options": {
																				"label": "AutoCreateRecipients__",
																				"version": 0
																			},
																			"stamp": 206
																		},
																		"AutoCreateRecipients__": {
																			"type": "CheckBox",
																			"data": [
																				"AutoCreateRecipients__"
																			],
																			"options": {
																				"label": "AutoCreateRecipients__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 207
																		},
																		"AllowedRoutingMethodsTitle__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "bold",
																				"textColor": "color1",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "AllowedRoutingMethodsTitle",
																				"version": 0
																			},
																			"stamp": 562
																		},
																		"LabelIsCOPAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsCOPAllowed__"
																			],
																			"options": {
																				"label": "CustomerOrderProcessing",
																				"version": 0
																			},
																			"stamp": 556
																		},
																		"IsCOPAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsCOPAllowed__"
																			],
																			"options": {
																				"label": "CustomerOrderProcessing",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 557
																		},
																		"LabelIsEmailAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsEmailAllowed__"
																			],
																			"options": {
																				"label": "Default_EMAIL",
																				"version": 0
																			},
																			"stamp": 558
																		},
																		"IsEmailAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsEmailAllowed__"
																			],
																			"options": {
																				"label": "Default_EMAIL",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 559
																		},
																		"LabelIsMODAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsMODAllowed__"
																			],
																			"options": {
																				"label": "Default_MOD",
																				"version": 0
																			},
																			"stamp": 563
																		},
																		"IsMODAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsMODAllowed__"
																			],
																			"options": {
																				"label": "Default_MOD",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 564
																		},
																		"LabelIsPortalAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsPortalAllowed__"
																			],
																			"options": {
																				"label": "Default_PORTAL",
																				"version": 0
																			},
																			"stamp": 569
																		},
																		"IsPortalAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsPortalAllowed__"
																			],
																			"options": {
																				"label": "Default_PORTAL",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 570
																		},
																		"LabelIsFaxAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsFaxAllowed__"
																			],
																			"options": {
																				"label": "Default_FAX",
																				"version": 0
																			},
																			"stamp": 565
																		},
																		"IsFaxAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsFaxAllowed__"
																			],
																			"options": {
																				"label": "Default_FAX",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 566
																		},
																		"LabelIsOtherAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsOtherAllowed__"
																			],
																			"options": {
																				"label": "Default_OTHER",
																				"version": 0
																			},
																			"stamp": 567
																		},
																		"IsOtherAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsOtherAllowed__"
																			],
																			"options": {
																				"label": "Default_OTHER",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 568
																		},
																		"LabelIsArchiveAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsArchiveAllowed__"
																			],
																			"options": {
																				"label": "Default_NONE",
																				"version": 0
																			},
																			"stamp": 560
																		},
																		"IsArchiveAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsArchiveAllowed__"
																			],
																			"options": {
																				"label": "Default_NONE",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 561
																		},
																		"LabelIsConversationAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsConversationAllowed__"
																			],
																			"options": {
																				"label": "IsConversationAllowed",
																				"version": 0
																			},
																			"stamp": 596
																		},
																		"IsConversationAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsConversationAllowed__"
																			],
																			"options": {
																				"label": "IsConversationAllowed",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 597
																		},
																		"LabelIsApplicationProcessAllowed__": {
																			"type": "Label",
																			"data": [
																				"IsApplicationProcessAllowed__"
																			],
																			"options": {
																				"label": "Default_APPLICATION_PROCESS",
																				"version": 0
																			},
																			"stamp": 604
																		},
																		"IsApplicationProcessAllowed__": {
																			"type": "CheckBox",
																			"data": [
																				"IsApplicationProcessAllowed__"
																			],
																			"options": {
																				"label": "Default_APPLICATION_PROCESS",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 605
																		},
																		"LabelProcessSelected__": {
																			"type": "Label",
																			"data": [
																				"ProcessSelected__"
																			],
																			"options": {
																				"label": "_Process selected",
																				"version": 0
																			},
																			"stamp": 606
																		},
																		"ProcessSelected__": {
																			"type": "ComboBox",
																			"data": [
																				"ProcessSelected__"
																			],
																			"options": {
																				"possibleValues": {},
																				"maxNbLinesToDisplay": 10,
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_Process selected",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 607
																		},
																		"LabelTarget_configuration__": {
																			"type": "Label",
																			"data": [
																				"Target_configuration__"
																			],
																			"options": {
																				"label": "_Target_configuration"
																			},
																			"stamp": 608
																		},
																		"Target_configuration__": {
																			"type": "ComboBox",
																			"data": [
																				"Target_configuration__"
																			],
																			"options": {
																				"possibleValues": {},
																				"maxNbLinesToDisplay": 10,
																				"version": 1,
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "_Target_configuration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 609
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-22": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 208,
											"*": {
												"Routing_advanced_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "Routing_advanced_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 209,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"DefaultDeliveryMethod__": "LabelDefaultDeliveryMethod__",
																	"LabelDefaultDeliveryMethod__": "DefaultDeliveryMethod__",
																	"AllowMethodMOD__": "LabelAllowMethodMOD__",
																	"LabelAllowMethodMOD__": "AllowMethodMOD__",
																	"AllowMethodSM__": "LabelAllowMethodSM__",
																	"LabelAllowMethodSM__": "AllowMethodSM__",
																	"AllowMethodPORTAL__": "LabelAllowMethodPORTAL__",
																	"LabelAllowMethodPORTAL__": "AllowMethodPORTAL__",
																	"AllowMethodFAX__": "LabelAllowMethodFAX__",
																	"LabelAllowMethodFAX__": "AllowMethodFAX__",
																	"NewRecipientAllowNoPwd__": "LabelNewRecipientAllowNoPwd__",
																	"LabelNewRecipientAllowNoPwd__": "NewRecipientAllowNoPwd__",
																	"AdditionalSettings__": "LabelAdditionalSettings__",
																	"LabelAdditionalSettings__": "AdditionalSettings__"
																},
																"version": 0
															},
															"stamp": 210,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DefaultDeliveryMethod__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDefaultDeliveryMethod__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line16__": {
																				"line": 2,
																				"column": 1
																			},
																			"DescAllowedDelivery__": {
																				"line": 3,
																				"column": 1
																			},
																			"AllowMethodMOD__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelAllowMethodMOD__": {
																				"line": 4,
																				"column": 1
																			},
																			"AllowMethodSM__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelAllowMethodSM__": {
																				"line": 5,
																				"column": 1
																			},
																			"AllowMethodPORTAL__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelAllowMethodPORTAL__": {
																				"line": 6,
																				"column": 1
																			},
																			"AllowMethodFAX__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelAllowMethodFAX__": {
																				"line": 7,
																				"column": 1
																			},
																			"Spacer_line19__": {
																				"line": 8,
																				"column": 1
																			},
																			"SecurityOptions__": {
																				"line": 9,
																				"column": 1
																			},
																			"NewRecipientAllowNoPwd__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelNewRecipientAllowNoPwd__": {
																				"line": 10,
																				"column": 1
																			},
																			"AdditionalSettings__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelAdditionalSettings__": {
																				"line": 11,
																				"column": 1
																			}
																		},
																		"lines": 11,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 211,
																	"*": {
																		"LabelDefaultDeliveryMethod__": {
																			"type": "Label",
																			"data": [
																				"DefaultDeliveryMethod__"
																			],
																			"options": {
																				"label": "DefaultDeliveryMethod__",
																				"version": 0
																			},
																			"stamp": 212
																		},
																		"DefaultDeliveryMethod__": {
																			"type": "RadioButton",
																			"data": [
																				"DefaultDeliveryMethod__"
																			],
																			"options": {
																				"keys": [
																					"DefaultDelivery_MOD",
																					"DefaultDelivery_PORTAL",
																					"DefaultDelivery_EMAIL",
																					"DefaultDelivery_FAX"
																				],
																				"values": [
																					"DefaultDelivery_MOD",
																					"DefaultDelivery_PORTAL",
																					"DefaultDelivery_EMAIL",
																					"DefaultDelivery_FAX"
																				],
																				"label": "DefaultDeliveryMethod__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 213
																		},
																		"Spacer_line16__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line16",
																				"version": 0
																			},
																			"stamp": 214
																		},
																		"DescAllowedDelivery__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescAllowedDelivery__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 215
																		},
																		"LabelAllowMethodMOD__": {
																			"type": "Label",
																			"data": [
																				"AllowMethodMOD__"
																			],
																			"options": {
																				"label": "AllowMethodMOD__",
																				"version": 0
																			},
																			"stamp": 216
																		},
																		"AllowMethodMOD__": {
																			"type": "CheckBox",
																			"data": [
																				"AllowMethodMOD__"
																			],
																			"options": {
																				"label": "AllowMethodMOD__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 217
																		},
																		"LabelAllowMethodSM__": {
																			"type": "Label",
																			"data": [
																				"AllowMethodSM__"
																			],
																			"options": {
																				"label": "AllowMethodSM__",
																				"version": 0
																			},
																			"stamp": 218
																		},
																		"AllowMethodSM__": {
																			"type": "CheckBox",
																			"data": [
																				"AllowMethodSM__"
																			],
																			"options": {
																				"label": "AllowMethodSM__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 219
																		},
																		"LabelAllowMethodPORTAL__": {
																			"type": "Label",
																			"data": [
																				"AllowMethodPORTAL__"
																			],
																			"options": {
																				"label": "AllowMethodPORTAL__",
																				"version": 0
																			},
																			"stamp": 220
																		},
																		"AllowMethodPORTAL__": {
																			"type": "CheckBox",
																			"data": [
																				"AllowMethodPORTAL__"
																			],
																			"options": {
																				"label": "AllowMethodPORTAL__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 221
																		},
																		"LabelAllowMethodFAX__": {
																			"type": "Label",
																			"data": [
																				"AllowMethodFAX__"
																			],
																			"options": {
																				"label": "AllowMethodFAX__",
																				"version": 0
																			},
																			"stamp": 222
																		},
																		"AllowMethodFAX__": {
																			"type": "CheckBox",
																			"data": [
																				"AllowMethodFAX__"
																			],
																			"options": {
																				"label": "AllowMethodFAX__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 223
																		},
																		"Spacer_line19__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line19",
																				"version": 0
																			},
																			"stamp": 224
																		},
																		"SecurityOptions__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "SecurityOptions__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 225
																		},
																		"LabelNewRecipientAllowNoPwd__": {
																			"type": "Label",
																			"data": [
																				"NewRecipientAllowNoPwd__"
																			],
																			"options": {
																				"label": "NewRecipientAllowNoPwd__",
																				"version": 0
																			},
																			"stamp": 226
																		},
																		"NewRecipientAllowNoPwd__": {
																			"type": "CheckBox",
																			"data": [
																				"NewRecipientAllowNoPwd__"
																			],
																			"options": {
																				"label": "NewRecipientAllowNoPwd__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 227
																		},
																		"LabelAdditionalSettings__": {
																			"type": "Label",
																			"data": [
																				"AdditionalSettings__"
																			],
																			"options": {
																				"label": "AdditionalSettings",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 598
																		},
																		"AdditionalSettings__": {
																			"type": "LongText",
																			"data": [
																				"AdditionalSettings__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "AdditionalSettings",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"hidden": true
																			},
																			"stamp": 599
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-13": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 492,
											"*": {
												"Related_Document_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Related_Document_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 486,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LinkToAnotherDocument__": "LabelLinkToAnotherDocument__",
																	"LabelLinkToAnotherDocument__": "LinkToAnotherDocument__",
																	"BehaviorOnExpiration__": "LabelBehaviorOnExpiration__",
																	"LabelBehaviorOnExpiration__": "BehaviorOnExpiration__",
																	"ArchivedAsLongAsMasterDocument__": "LabelArchivedAsLongAsMasterDocument__",
																	"LabelArchivedAsLongAsMasterDocument__": "ArchivedAsLongAsMasterDocument__",
																	"MasterDocumentExpirationTime__": "LabelMasterDocumentExpirationTime__",
																	"LabelMasterDocumentExpirationTime__": "MasterDocumentExpirationTime__"
																},
																"version": 0
															},
															"stamp": 487,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LinkToAnotherDocument__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelLinkToAnotherDocument__": {
																				"line": 1,
																				"column": 1
																			},
																			"BehaviorOnExpiration__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelBehaviorOnExpiration__": {
																				"line": 2,
																				"column": 1
																			},
																			"ArchivedAsLongAsMasterDocument__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelArchivedAsLongAsMasterDocument__": {
																				"line": 4,
																				"column": 1
																			},
																			"MasterDocumentExpirationTime__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelMasterDocumentExpirationTime__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 488,
																	"*": {
																		"LabelLinkToAnotherDocument__": {
																			"type": "Label",
																			"data": [
																				"LinkToAnotherDocument__"
																			],
																			"options": {
																				"label": "_LinkToAnotherDocument",
																				"version": 0
																			},
																			"stamp": 496
																		},
																		"LinkToAnotherDocument__": {
																			"type": "CheckBox",
																			"data": [
																				"LinkToAnotherDocument__"
																			],
																			"options": {
																				"label": "_LinkToAnotherDocument",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 497
																		},
																		"LabelBehaviorOnExpiration__": {
																			"type": "Label",
																			"data": [
																				"BehaviorOnExpiration__"
																			],
																			"options": {
																				"label": "_BehaviorOnExpiration",
																				"version": 0
																			},
																			"stamp": 498
																		},
																		"BehaviorOnExpiration__": {
																			"type": "RadioButton",
																			"data": [
																				"BehaviorOnExpiration__"
																			],
																			"options": {
																				"keys": [
																					"ERROR",
																					"SEND"
																				],
																				"values": [
																					"_BehaviorOnExpiration_SetOnError",
																					"_BehaviorOnExpiration_Send"
																				],
																				"label": "_BehaviorOnExpiration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 499
																		},
																		"LabelMasterDocumentExpirationTime__": {
																			"type": "Label",
																			"data": [
																				"MasterDocumentExpirationTime__"
																			],
																			"options": {
																				"label": "_MasterDocumentExpirationTime",
																				"version": 0
																			},
																			"stamp": 504
																		},
																		"MasterDocumentExpirationTime__": {
																			"type": "Decimal",
																			"data": [
																				"MasterDocumentExpirationTime__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_MasterDocumentExpirationTime",
																				"precision_internal": 3,
																				"precision_current": 3,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"autocompletable": false
																			},
																			"stamp": 505
																		},
																		"LabelArchivedAsLongAsMasterDocument__": {
																			"type": "Label",
																			"data": [
																				"ArchivedAsLongAsMasterDocument__"
																			],
																			"options": {
																				"label": "_ArchivedAsLongAsMasterDocument",
																				"version": 0
																			},
																			"stamp": 502
																		},
																		"ArchivedAsLongAsMasterDocument__": {
																			"type": "CheckBox",
																			"data": [
																				"ArchivedAsLongAsMasterDocument__"
																			],
																			"options": {
																				"label": "_ArchivedAsLongAsMasterDocument",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 503
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-12": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 228,
											"*": {
												"MOD_delivery_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "MOD_delivery_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 229,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"MOD_Option_Color__": "LabelMOD_Option_Color__",
																	"LabelMOD_Option_Color__": "MOD_Option_Color__",
																	"MOD_Option_Bothsided__": "LabelMOD_Option_Bothsided__",
																	"LabelMOD_Option_Bothsided__": "MOD_Option_Bothsided__",
																	"MOD_Option_Cover__": "LabelMOD_Option_Cover__",
																	"LabelMOD_Option_Cover__": "MOD_Option_Cover__",
																	"MoreMODOptions__": "LabelMoreMODOptions__",
																	"LabelMoreMODOptions__": "MoreMODOptions__",
																	"MOD_Option_Grouping__": "LabelMOD_Option_Grouping__",
																	"LabelMOD_Option_Grouping__": "MOD_Option_Grouping__",
																	"DeferredTimeAfterValidation__": "LabelDeferredTimeAfterValidation__",
																	"LabelDeferredTimeAfterValidation__": "DeferredTimeAfterValidation__",
																	"MOD_Option_CoverOnC4__": "LabelMOD_Option_CoverOnC4__",
																	"LabelMOD_Option_CoverOnC4__": "MOD_Option_CoverOnC4__",
																	"MOD_Option_FirstPageOnOddPage__": "LabelMOD_Option_FirstPageOnOddPage__",
																	"LabelMOD_Option_FirstPageOnOddPage__": "MOD_Option_FirstPageOnOddPage__",
																	"MOD_Option_MailService__": "LabelMOD_Option_MailService__",
																	"LabelMOD_Option_MailService__": "MOD_Option_MailService__"
																},
																"version": 0
															},
															"stamp": 230,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"MOD_Option_Color__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelMOD_Option_Color__": {
																				"line": 3,
																				"column": 1
																			},
																			"MOD_Option_Bothsided__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelMOD_Option_Bothsided__": {
																				"line": 2,
																				"column": 1
																			},
																			"MOD_Option_Cover__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelMOD_Option_Cover__": {
																				"line": 1,
																				"column": 1
																			},
																			"MoreMODOptions__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelMoreMODOptions__": {
																				"line": 10,
																				"column": 1
																			},
																			"MOD_Option_Grouping__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelMOD_Option_Grouping__": {
																				"line": 4,
																				"column": 1
																			},
																			"DescMODGrouping__": {
																				"line": 5,
																				"column": 1
																			},
																			"DeferredTimeAfterValidation__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDeferredTimeAfterValidation__": {
																				"line": 6,
																				"column": 1
																			},
																			"MOD_Option_CoverOnC4__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelMOD_Option_CoverOnC4__": {
																				"line": 7,
																				"column": 1
																			},
																			"MOD_Option_FirstPageOnOddPage__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelMOD_Option_FirstPageOnOddPage__": {
																				"line": 8,
																				"column": 1
																			},
																			"MOD_Option_MailService__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelMOD_Option_MailService__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"lines": 10,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 231,
																	"*": {
																		"LabelMOD_Option_Cover__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Cover__"
																			],
																			"options": {
																				"label": "MOD_Option_Cover__",
																				"version": 0
																			},
																			"stamp": 232
																		},
																		"MOD_Option_Cover__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_Cover__"
																			],
																			"options": {
																				"label": "MOD_Option_Cover__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 233
																		},
																		"LabelMOD_Option_Bothsided__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Bothsided__"
																			],
																			"options": {
																				"label": "MOD_Option_Bothsided__",
																				"version": 0
																			},
																			"stamp": 234
																		},
																		"MOD_Option_Bothsided__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_Bothsided__"
																			],
																			"options": {
																				"label": "MOD_Option_Bothsided__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 235
																		},
																		"LabelMOD_Option_Color__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Color__"
																			],
																			"options": {
																				"label": "MOD_Option_Color__",
																				"version": 0
																			},
																			"stamp": 236
																		},
																		"MOD_Option_Color__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_Color__"
																			],
																			"options": {
																				"label": "MOD_Option_Color__",
																				"activable": true,
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 237
																		},
																		"LabelMOD_Option_Grouping__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Grouping__"
																			],
																			"options": {
																				"label": "MOD_Option_Grouping__",
																				"version": 0
																			},
																			"stamp": 238
																		},
																		"MOD_Option_Grouping__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_Grouping__"
																			],
																			"options": {
																				"label": "MOD_Option_Grouping__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 239
																		},
																		"DescMODGrouping__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 310,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "italic",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescMODGrouping__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 240
																		},
																		"LabelDeferredTimeAfterValidation__": {
																			"type": "Label",
																			"data": [
																				"DeferredTimeAfterValidation__"
																			],
																			"options": {
																				"label": "DeferredTimeAfterValidation__",
																				"version": 0
																			},
																			"stamp": 241
																		},
																		"DeferredTimeAfterValidation__": {
																			"type": "CheckBox",
																			"data": [
																				"DeferredTimeAfterValidation__"
																			],
																			"options": {
																				"label": "DeferredTimeAfterValidation__",
																				"activable": true,
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 242
																		},
																		"LabelMOD_Option_CoverOnC4__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_CoverOnC4__"
																			],
																			"options": {
																				"label": "MOD_Option_CoverOnC4__",
																				"version": 0
																			},
																			"stamp": 243
																		},
																		"MOD_Option_CoverOnC4__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_CoverOnC4__"
																			],
																			"options": {
																				"label": "MOD_Option_CoverOnC4__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 244
																		},
																		"LabelMOD_Option_FirstPageOnOddPage__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_FirstPageOnOddPage__"
																			],
																			"options": {
																				"label": "MOD_Option_FirstPageOnOddPage__",
																				"version": 0
																			},
																			"stamp": 245
																		},
																		"MOD_Option_FirstPageOnOddPage__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_FirstPageOnOddPage__"
																			],
																			"options": {
																				"label": "MOD_Option_FirstPageOnOddPage__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 246
																		},
																		"LabelMOD_Option_MailService__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_combo_box__"
																			],
																			"options": {
																				"label": "MOD_Option_MailService__",
																				"version": 0
																			},
																			"stamp": 532
																		},
																		"MOD_Option_MailService__": {
																			"type": "ComboBox",
																			"data": [
																				"MOD_Option_MailService__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_MOD_Option_StampType_Standard",
																					"1": "_MOD_Option_StampType_EconomyIfApplicable"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "STANDARD",
																					"1": "ECONOMY_IF_APPLICABLE"
																				},
																				"label": "MOD_Option_MailService__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 533
																		},
																		"LabelMoreMODOptions__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 247
																		},
																		"MoreMODOptions__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": " ",
																				"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																				"css": "@ .adv {\ncursor: pointer;\n}@ \n.adv:hover {\ntext-decoration:underline;\n}@ ",
																				"version": 0
																			},
																			"stamp": 248
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-17": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 249,
											"*": {
												"Advanced_mod_options": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "Advanced_mod_options",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 250,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelMOD_Option_PDFCommand__": "MOD_Option_PDFCommand__",
																	"MOD_Option_PDFCommand__": "LabelMOD_Option_PDFCommand__",
																	"LabelMOD_Option_Resize__": "MOD_Option_Resize__",
																	"MOD_Option_Resize__": "LabelMOD_Option_Resize__",
																	"MOD_Option_Scale__": "LabelMOD_Option_Scale__",
																	"LabelMOD_Option_Scale__": "MOD_Option_Scale__",
																	"MOD_Option_OffsetX__": "LabelMOD_Option_OffsetX__",
																	"LabelMOD_Option_OffsetX__": "MOD_Option_OffsetX__",
																	"MOD_Option_OffsetY__": "LabelMOD_Option_OffsetY__",
																	"LabelMOD_Option_OffsetY__": "MOD_Option_OffsetY__"
																},
																"version": 0
															},
															"stamp": 251,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelMOD_Option_PDFCommand__": {
																				"line": 9,
																				"column": 1
																			},
																			"MOD_Option_PDFCommand__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelMOD_Option_Resize__": {
																				"line": 3,
																				"column": 1
																			},
																			"MOD_Option_Resize__": {
																				"line": 3,
																				"column": 2
																			},
																			"Spacer_line11__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line12__": {
																				"line": 10,
																				"column": 1
																			},
																			"MOD_Option_Scale__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelMOD_Option_Scale__": {
																				"line": 4,
																				"column": 1
																			},
																			"MOD_Option_OffsetX__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelMOD_Option_OffsetX__": {
																				"line": 5,
																				"column": 1
																			},
																			"MOD_Option_OffsetY__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelMOD_Option_OffsetY__": {
																				"line": 6,
																				"column": 1
																			},
																			"DescPDFCommand__": {
																				"line": 8,
																				"column": 1
																			},
																			"Spacer_line13__": {
																				"line": 7,
																				"column": 1
																			},
																			"DescResize__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 10,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 252,
																	"*": {
																		"Spacer_line11__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line11",
																				"version": 0
																			},
																			"stamp": 253
																		},
																		"DescResize__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescResize__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 254
																		},
																		"MOD_Option_Resize__": {
																			"type": "CheckBox",
																			"data": [
																				"MOD_Option_Resize__"
																			],
																			"options": {
																				"label": "MOD_Option_Resize__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 255
																		},
																		"LabelMOD_Option_Resize__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Resize__"
																			],
																			"options": {
																				"label": "MOD_Option_Resize__",
																				"version": 0
																			},
																			"stamp": 256
																		},
																		"LabelMOD_Option_Scale__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_Scale__"
																			],
																			"options": {
																				"label": "MOD_Option_Scale__",
																				"version": 0
																			},
																			"stamp": 257
																		},
																		"MOD_Option_Scale__": {
																			"type": "Integer",
																			"data": [
																				"MOD_Option_Scale__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "MOD_Option_Scale__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "50",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 258
																		},
																		"LabelMOD_Option_OffsetX__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_OffsetX__"
																			],
																			"options": {
																				"label": "MOD_Option_OffsetX__",
																				"version": 0
																			},
																			"stamp": 259
																		},
																		"MOD_Option_OffsetX__": {
																			"type": "Integer",
																			"data": [
																				"MOD_Option_OffsetX__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "MOD_Option_OffsetX__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "50",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 260
																		},
																		"LabelMOD_Option_OffsetY__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_OffsetY__"
																			],
																			"options": {
																				"label": "MOD_Option_OffsetY__",
																				"version": 0
																			},
																			"stamp": 261
																		},
																		"MOD_Option_OffsetY__": {
																			"type": "Integer",
																			"data": [
																				"MOD_Option_OffsetY__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "MOD_Option_OffsetY__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "50",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 262
																		},
																		"Spacer_line13__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line13",
																				"version": 0
																			},
																			"stamp": 263
																		},
																		"DescPDFCommand__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescPDFCommand__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 264
																		},
																		"LabelMOD_Option_PDFCommand__": {
																			"type": "Label",
																			"data": [
																				"MOD_Option_PDFCommand__"
																			],
																			"options": {
																				"label": "MOD_Option_PDFCommand__",
																				"version": 0
																			},
																			"stamp": 265
																		},
																		"MOD_Option_PDFCommand__": {
																			"type": "ShortText",
																			"data": [
																				"MOD_Option_PDFCommand__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "MOD_Option_PDFCommand__",
																				"activable": true,
																				"width": "100%",
																				"length": 1024,
																				"browsable": false,
																				"autocompletable": false,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 266
																		},
																		"Spacer_line12__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line12",
																				"version": 0
																			},
																			"stamp": 267
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-15": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 268,
											"*": {
												"Portal_delivery_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Portal_delivery_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 269,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelWhenSendWelcomeEmail__": "WhenSendWelcomeEmail__",
																	"WhenSendWelcomeEmail__": "LabelWhenSendWelcomeEmail__",
																	"LabelConcatWelcomeEmail__": "ConcatWelcomeEmail__",
																	"ConcatWelcomeEmail__": "LabelConcatWelcomeEmail__",
																	"LabelMorePortalOptions__": "MorePortalOptions__",
																	"MorePortalOptions__": "LabelMorePortalOptions__",
																	"GroupEmailNotifications__": "LabelGroupEmailNotifications__",
																	"LabelGroupEmailNotifications__": "GroupEmailNotifications__"
																},
																"version": 0
															},
															"stamp": 270,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Spacer_line4__": {
																				"line": 1,
																				"column": 1
																			},
																			"DescWelcomeEmail__": {
																				"line": 2,
																				"column": 1
																			},
																			"LabelWhenSendWelcomeEmail__": {
																				"line": 3,
																				"column": 1
																			},
																			"WhenSendWelcomeEmail__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelConcatWelcomeEmail__": {
																				"line": 4,
																				"column": 1
																			},
																			"ConcatWelcomeEmail__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelMorePortalOptions__": {
																				"line": 6,
																				"column": 1
																			},
																			"MorePortalOptions__": {
																				"line": 6,
																				"column": 2
																			},
																			"GroupEmailNotifications__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelGroupEmailNotifications__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 271,
																	"*": {
																		"MorePortalOptions__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "",
																				"htmlContent": "<span class=\"adv\"><i class=\"fa fa-plus-square\"></i> $TITLE$</span>",
																				"version": 0,
																				"css": "@ .adv {\ncursor: pointer;\n}@ \n.adv:hover {\ntext-decoration:underline;\n}@ "
																			},
																			"stamp": 272
																		},
																		"LabelMorePortalOptions__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 273
																		},
																		"ConcatWelcomeEmail__": {
																			"type": "CheckBox",
																			"data": [
																				"ConcatWelcomeEmail__"
																			],
																			"options": {
																				"label": "ConcatWelcomeEmail__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 274
																		},
																		"LabelConcatWelcomeEmail__": {
																			"type": "Label",
																			"data": [
																				"ConcatWelcomeEmail__"
																			],
																			"options": {
																				"label": "ConcatWelcomeEmail__",
																				"version": 0
																			},
																			"stamp": 275
																		},
																		"WhenSendWelcomeEmail__": {
																			"type": "ComboBox",
																			"data": [
																				"WhenSendWelcomeEmail__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_NEVER",
																					"1": "_ONNEVERLOGGED",
																					"2": "_ONFIRSTDOCUMENT"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "NEVER",
																					"1": "ONNEVERLOGGED",
																					"2": "ONFIRSTDOCUMENT"
																				},
																				"label": "WhenSendWelcomeEmail__",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 276
																		},
																		"LabelWhenSendWelcomeEmail__": {
																			"type": "Label",
																			"data": [
																				"WhenSendWelcomeEmail__"
																			],
																			"options": {
																				"label": "WhenSendWelcomeEmail__",
																				"version": 0
																			},
																			"stamp": 277
																		},
																		"LabelGroupEmailNotifications__": {
																			"type": "Label",
																			"data": [
																				"GroupEmailNotifications__"
																			],
																			"options": {
																				"label": "GroupEmailNotifications__",
																				"version": 0
																			},
																			"stamp": 447
																		},
																		"GroupEmailNotifications__": {
																			"type": "CheckBox",
																			"data": [
																				"GroupEmailNotifications__"
																			],
																			"options": {
																				"label": "GroupEmailNotifications__",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 448
																		},
																		"DescWelcomeEmail__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescWelcomeEmail__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 280
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line4",
																				"version": 0
																			},
																			"stamp": 281
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-18": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 282,
											"*": {
												"Advanced_portal_options": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "color7",
														"labelsAlignment": "right",
														"label": "Advanced_portal_options",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 283,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelAliasPortalUrl__": "AliasPortalUrl__",
																	"AliasPortalUrl__": "LabelAliasPortalUrl__",
																	"LabelAlwaysSendNotification__": "AlwaysSendNotification__",
																	"AlwaysSendNotification__": "LabelAlwaysSendNotification__",
																	"LabelWhenSendWelcomeLetter__": "WhenSendWelcomeLetter__",
																	"WhenSendWelcomeLetter__": "LabelWhenSendWelcomeLetter__"
																},
																"version": 0
															},
															"stamp": 284,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Spacer_line6__": {
																				"line": 1,
																				"column": 1
																			},
																			"DescAliasURL__": {
																				"line": 5,
																				"column": 1
																			},
																			"LabelAliasPortalUrl__": {
																				"line": 6,
																				"column": 1
																			},
																			"AliasPortalUrl__": {
																				"line": 6,
																				"column": 2
																			},
																			"Spacer_line7__": {
																				"line": 7,
																				"column": 1
																			},
																			"DescAlwaysSendNotification__": {
																				"line": 8,
																				"column": 1
																			},
																			"LabelAlwaysSendNotification__": {
																				"line": 9,
																				"column": 1
																			},
																			"AlwaysSendNotification__": {
																				"line": 9,
																				"column": 2
																			},
																			"Spacer_line8__": {
																				"line": 4,
																				"column": 1
																			},
																			"LabelWhenSendWelcomeLetter__": {
																				"line": 3,
																				"column": 1
																			},
																			"WhenSendWelcomeLetter__": {
																				"line": 3,
																				"column": 2
																			},
																			"DescWelcomeLetter__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line5__": {
																				"line": 10,
																				"column": 1
																			}
																		},
																		"lines": 10,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 285,
																	"*": {
																		"Spacer_line5__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line5",
																				"version": 0
																			},
																			"stamp": 286
																		},
																		"DescWelcomeLetter__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescWelcomeLetter__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 287
																		},
																		"LabelWhenSendWelcomeLetter__": {
																			"type": "Label",
																			"data": [
																				"WhenSendWelcomeLetter__"
																			],
																			"options": {
																				"label": "WhenSendWelcomeLetter__",
																				"version": 0
																			},
																			"stamp": 288
																		},
																		"AlwaysSendNotification__": {
																			"type": "CheckBox",
																			"data": [
																				"AlwaysSendNotification__"
																			],
																			"options": {
																				"label": "AlwaysSendNotification__",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 289
																		},
																		"Spacer_line8__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line8",
																				"version": 0
																			},
																			"stamp": 290
																		},
																		"WhenSendWelcomeLetter__": {
																			"type": "ComboBox",
																			"data": [
																				"WhenSendWelcomeLetter__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_NEVER",
																					"1": "_ONNEVERLOGGED",
																					"2": "_ONFIRSTDOCUMENT"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "NEVER",
																					"1": "ONNEVERLOGGED",
																					"2": "ONFIRSTDOCUMENT"
																				},
																				"label": "WhenSendWelcomeLetter__",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 291
																		},
																		"LabelAlwaysSendNotification__": {
																			"type": "Label",
																			"data": [
																				"AlwaysSendNotification__"
																			],
																			"options": {
																				"label": "AlwaysSendNotification__",
																				"version": 0
																			},
																			"stamp": 292
																		},
																		"DescAlwaysSendNotification__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescAlwaysSendNotification__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 293
																		},
																		"Spacer_line7__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line7",
																				"version": 0
																			},
																			"stamp": 294
																		},
																		"AliasPortalUrl__": {
																			"type": "ShortText",
																			"data": [
																				"AliasPortalUrl__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "AliasPortalUrl__",
																				"activable": true,
																				"autocompletable": false,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 295
																		},
																		"LabelAliasPortalUrl__": {
																			"type": "Label",
																			"data": [
																				"AliasPortalUrl__"
																			],
																			"options": {
																				"label": "AliasPortalUrl__",
																				"version": 0
																			},
																			"stamp": 296
																		},
																		"DescAliasURL__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"backgroundcolor": "default",
																				"label": "DescAliasURL__",
																				"version": 0,
																				"image_url": "",
																				"iconClass": ""
																			},
																			"stamp": 297
																		},
																		"Spacer_line6__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line6",
																				"version": 0
																			},
																			"stamp": 298
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-19": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 299,
											"*": {
												"Email_delivery_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Email_delivery_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 300,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"AddAttachmentsWithEmail__": "LabelAddAttachmentsWithEmail__",
																	"LabelAddAttachmentsWithEmail__": "AddAttachmentsWithEmail__",
																	"GroupEmailMaxAttachSize__": "LabelGroupEmailMaxAttachSize__",
																	"LabelGroupEmailMaxAttachSize__": "GroupEmailMaxAttachSize__",
																	"GroupEmailWithAttach__": "LabelGroupEmailWithAttach__",
																	"LabelGroupEmailWithAttach__": "GroupEmailWithAttach__"
																},
																"version": 0
															},
															"stamp": 301,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"AddAttachmentsWithEmail__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelAddAttachmentsWithEmail__": {
																				"line": 3,
																				"column": 1
																			},
																			"GroupEmailMaxAttachSize__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelGroupEmailMaxAttachSize__": {
																				"line": 2,
																				"column": 1
																			},
																			"GroupEmailWithAttach__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelGroupEmailWithAttach__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 302,
																	"*": {
																		"LabelGroupEmailWithAttach__": {
																			"type": "Label",
																			"data": [
																				"GroupEmailWithAttach__"
																			],
																			"options": {
																				"label": "GroupEmailWithAttach__",
																				"version": 0
																			},
																			"stamp": 449
																		},
																		"GroupEmailWithAttach__": {
																			"type": "CheckBox",
																			"data": [
																				"GroupEmailWithAttach__"
																			],
																			"options": {
																				"label": "GroupEmailWithAttach__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 450
																		},
																		"LabelGroupEmailMaxAttachSize__": {
																			"type": "Label",
																			"data": [
																				"GroupEmailMaxAttachSize__"
																			],
																			"options": {
																				"label": "GroupEmailMaxAttachSize__",
																				"version": 0
																			},
																			"stamp": 445
																		},
																		"GroupEmailMaxAttachSize__": {
																			"type": "Integer",
																			"data": [
																				"GroupEmailMaxAttachSize__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "GroupEmailMaxAttachSize__",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "50",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "6",
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 446
																		},
																		"LabelAddAttachmentsWithEmail__": {
																			"type": "Label",
																			"data": [
																				"AddAttachmentsWithEmail__"
																			],
																			"options": {
																				"label": "AddAttachmentsWithEmail__",
																				"version": 0
																			},
																			"stamp": 307
																		},
																		"AddAttachmentsWithEmail__": {
																			"type": "CheckBox",
																			"data": [
																				"AddAttachmentsWithEmail__"
																			],
																			"options": {
																				"label": "AddAttachmentsWithEmail__",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 308
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-11": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 337,
											"*": {
												"ArchivingSender_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "ArchivingSender_pane",
														"leftImageURL": "",
														"removeMargins": false,
														"version": 0,
														"hidden": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"stamp": 338,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Document_ArchiveDuration_MOD__": "LabelDocument_ArchiveDuration_MOD__",
																	"LabelDocument_ArchiveDuration_MOD__": "Document_ArchiveDuration_MOD__",
																	"Document_ArchiveDuration_SM__": "LabelDocument_ArchiveDuration_SM__",
																	"LabelDocument_ArchiveDuration_SM__": "Document_ArchiveDuration_SM__",
																	"Document_ArchiveDuration_PORTAL__": "LabelDocument_ArchiveDuration_PORTAL__",
																	"LabelDocument_ArchiveDuration_PORTAL__": "Document_ArchiveDuration_PORTAL__",
																	"Document_ArchiveDuration_FGFAXOUT__": "LabelDocument_ArchiveDuration_FGFAXOUT__",
																	"LabelDocument_ArchiveDuration_FGFAXOUT__": "Document_ArchiveDuration_FGFAXOUT__",
																	"Document_ArchiveDuration_NONE__": "LabelDocument_ArchiveDuration_NONE__",
																	"LabelDocument_ArchiveDuration_NONE__": "Document_ArchiveDuration_NONE__",
																	"Document_ArchiveDuration_OTHER__": "LabelDocument_ArchiveDuration_OTHER__",
																	"LabelDocument_ArchiveDuration_OTHER__": "Document_ArchiveDuration_OTHER__"
																},
																"version": 0
															},
															"stamp": 339,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Document_ArchiveDuration_MOD__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_MOD__": {
																				"line": 1,
																				"column": 1
																			},
																			"Document_ArchiveDuration_SM__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_SM__": {
																				"line": 2,
																				"column": 1
																			},
																			"Document_ArchiveDuration_PORTAL__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_PORTAL__": {
																				"line": 3,
																				"column": 1
																			},
																			"Document_ArchiveDuration_FGFAXOUT__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_FGFAXOUT__": {
																				"line": 4,
																				"column": 1
																			},
																			"Document_ArchiveDuration_NONE__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_NONE__": {
																				"line": 5,
																				"column": 1
																			},
																			"Document_ArchiveDuration_OTHER__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDocument_ArchiveDuration_OTHER__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 340,
																	"*": {
																		"LabelDocument_ArchiveDuration_MOD__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_MOD__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_MOD__",
																				"version": 0
																			},
																			"stamp": 341
																		},
																		"Document_ArchiveDuration_MOD__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_MOD__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_MOD__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 342
																		},
																		"LabelDocument_ArchiveDuration_SM__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_SM__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_SM__",
																				"version": 0
																			},
																			"stamp": 343
																		},
																		"Document_ArchiveDuration_SM__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_SM__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_SM__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 344
																		},
																		"LabelDocument_ArchiveDuration_PORTAL__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_PORTAL__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_PORTAL__",
																				"version": 0
																			},
																			"stamp": 345
																		},
																		"Document_ArchiveDuration_PORTAL__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_PORTAL__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_PORTAL__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 346
																		},
																		"LabelDocument_ArchiveDuration_FGFAXOUT__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_FGFAXOUT__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_FGFAXOUT__",
																				"version": 0
																			},
																			"stamp": 349
																		},
																		"Document_ArchiveDuration_FGFAXOUT__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_FGFAXOUT__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_FGFAXOUT__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 350
																		},
																		"LabelDocument_ArchiveDuration_NONE__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_NONE__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_NONE__",
																				"version": 0
																			},
																			"stamp": 411
																		},
																		"Document_ArchiveDuration_NONE__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_NONE__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_NONE__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 412
																		},
																		"LabelDocument_ArchiveDuration_OTHER__": {
																			"type": "Label",
																			"data": [
																				"Document_ArchiveDuration_OTHER__"
																			],
																			"options": {
																				"label": "Document_ArchiveDuration_OTHER__",
																				"version": 0
																			},
																			"stamp": 413
																		},
																		"Document_ArchiveDuration_OTHER__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_ArchiveDuration_OTHER__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ARC_FREE",
																					"1": "_ARC_1YEAR",
																					"2": "_ARC_2YEAR",
																					"3": "_ARC_3YEARS",
																					"4": "_ARC_4YEARS",
																					"5": "_ARC_5YEARS",
																					"6": "_ARC_6YEARS",
																					"7": "_ARC_7YEARS",
																					"8": "_ARC_8YEARS",
																					"9": "_ARC_9YEARS",
																					"10": "_ARC_10YEARS",
																					"11": "_ARC_11YEARS"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "0",
																					"1": "12",
																					"2": "24",
																					"3": "36",
																					"4": "48",
																					"5": "60",
																					"6": "72",
																					"7": "84",
																					"8": "96",
																					"9": "108",
																					"10": "120",
																					"11": "132"
																				},
																				"label": "Document_ArchiveDuration_OTHER__",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 414
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 571,
											"*": {
												"Advanced_extraction_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "300px",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "Advanced_extraction_pane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 572,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Show_comment__": "LabelShow_comment__",
																	"LabelShow_comment__": "Show_comment__",
																	"Show_recipient_id__": "LabelShow_recipient_id__",
																	"LabelShow_recipient_id__": "Show_recipient_id__",
																	"Show_document_number__": "LabelShow_document_number__",
																	"LabelShow_document_number__": "Show_document_number__"
																},
																"version": 0
															},
															"stamp": 573,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Show_comment__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelShow_comment__": {
																				"line": 1,
																				"column": 1
																			},
																			"Show_recipient_id__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelShow_recipient_id__": {
																				"line": 2,
																				"column": 1
																			},
																			"Show_document_number__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelShow_document_number__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 3,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 574,
																	"*": {
																		"LabelShow_comment__": {
																			"type": "Label",
																			"data": [
																				"Show_comment__"
																			],
																			"options": {
																				"label": "Show_comment",
																				"version": 0
																			},
																			"stamp": 575
																		},
																		"Show_comment__": {
																			"type": "CheckBox",
																			"data": [
																				"Show_comment__"
																			],
																			"options": {
																				"label": "Show_comment",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 576
																		},
																		"LabelShow_recipient_id__": {
																			"type": "Label",
																			"data": [
																				"Show_recipient_id__"
																			],
																			"options": {
																				"label": "Show_recipient_id",
																				"version": 0
																			},
																			"stamp": 577
																		},
																		"Show_recipient_id__": {
																			"type": "CheckBox",
																			"data": [
																				"Show_recipient_id__"
																			],
																			"options": {
																				"label": "Show_recipient_id",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 578
																		},
																		"LabelShow_document_number__": {
																			"type": "Label",
																			"data": [
																				"Show_document_number__"
																			],
																			"options": {
																				"label": "Show_document_number",
																				"version": 0
																			},
																			"stamp": 579
																		},
																		"Show_document_number__": {
																			"type": "CheckBox",
																			"data": [
																				"Show_document_number__"
																			],
																			"options": {
																				"label": "Show_document_number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": true,
																				"version": 0
																			},
																			"stamp": 580
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 387,
									"data": []
								}
							},
							"stamp": 388,
							"data": []
						}
					},
					"stamp": 389,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"maxwidth": 800,
						"version": 0
					},
					"*": {
						"Previous": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "< Previous",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 390
						},
						"Next": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "Next >",
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"url": "",
								"action": "none",
								"version": 0
							},
							"stamp": 391
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "color1",
								"style": 1,
								"url": "",
								"shortCut": "S"
							},
							"stamp": 392,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 393,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 394,
							"data": []
						}
					},
					"stamp": 395,
					"data": []
				}
			},
			"stamp": 396,
			"data": []
		}
	},
	"stamps": 611,
	"data": []
}