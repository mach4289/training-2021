{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 2,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Manager__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelManager__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 4
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "Cost center",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"CostCenter__": {
																			"type": "ShortText",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "Cost center",
																				"activable": true,
																				"width": 500,
																				"length": 20,
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": 500,
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"LabelManager__": {
																			"type": "Label",
																			"data": [
																				"Manager__"
																			],
																			"options": {
																				"label": "Manager",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Manager__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Manager__"
																			],
																			"options": {
																				"label": "Manager",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"PreFillFTS": true
																			},
																			"stamp": 10
																		}
																	},
																	"stamp": 11
																}
															},
															"stamp": 12,
															"data": []
														}
													},
													"stamp": 13,
													"data": []
												}
											}
										}
									},
									"stamp": 14,
									"data": []
								}
							},
							"stamp": 15,
							"data": []
						}
					},
					"stamp": 16,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 17,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 18,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 19,
							"data": []
						}
					},
					"stamp": 20,
					"data": []
				}
			},
			"stamp": 21,
			"data": []
		}
	},
	"stamps": 21,
	"data": []
}