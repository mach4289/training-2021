Controls.LineItemsImporterMapping__.OnChange = function ()
{
	var mappingJSON = this.GetValue();
	if (mappingJSON)
	{
		try
		{
			JSON.parse(mappingJSON);
		}
		catch(err)
		{
			this.SetError("_InvalidJSON");
		}
	}
	else
	{
		this.SetInfo("_InfoMapping");
	}
};

Controls.LineItemsImporterMapping__.OnChange();