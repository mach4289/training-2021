{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"OrderNumber__": "LabelOrderNumber__",
																	"LabelOrderNumber__": "OrderNumber__",
																	"OrderDate__": "LabelOrderDate__",
																	"LabelOrderDate__": "OrderDate__",
																	"InvoicedAmount__": "LabelInvoicedAmount__",
																	"LabelInvoicedAmount__": "InvoicedAmount__",
																	"OrderedAmount__": "LabelOrderedAmount__",
																	"LabelOrderedAmount__": "OrderedAmount__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"DeliveredAmount__": "LabelDeliveredAmount__",
																	"LabelDeliveredAmount__": "DeliveredAmount__",
																	"Buyer__": "LabelBuyer__",
																	"LabelBuyer__": "Buyer__",
																	"Receiver__": "LabelReceiver__",
																	"LabelReceiver__": "Receiver__",
																	"IsLocalPO__": "LabelIsLocalPO__",
																	"LabelIsLocalPO__": "IsLocalPO__",
																	"IsCreatedInERP__": "LabelIsCreatedInERP__",
																	"LabelIsCreatedInERP__": "IsCreatedInERP__",
																	"DiffernetInvoicingParty__": "LabelDiffernetInvoicingParty__",
																	"LabelDiffernetInvoicingParty__": "DiffernetInvoicingParty__",
																	"NoMoreInvoiceExpected__": "LabelNoMoreInvoiceExpected__",
																	"LabelNoMoreInvoiceExpected__": "NoMoreInvoiceExpected__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 14,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"DifferentInvoicingParty__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDifferentInvoicingParty__": {
																				"line": 3,
																				"column": 1
																			},
																			"OrderNumber__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelOrderNumber__": {
																				"line": 4,
																				"column": 1
																			},
																			"OrderDate__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelOrderDate__": {
																				"line": 5,
																				"column": 1
																			},
																			"OrderedAmount__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelOrderedAmount__": {
																				"line": 6,
																				"column": 1
																			},
																			"DeliveredAmount__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelDeliveredAmount__": {
																				"line": 7,
																				"column": 1
																			},
																			"InvoicedAmount__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelInvoicedAmount__": {
																				"line": 8,
																				"column": 1
																			},
																			"Buyer__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelBuyer__": {
																				"line": 10,
																				"column": 1
																			},
																			"Receiver__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelReceiver__": {
																				"line": 11,
																				"column": 1
																			},
																			"IsLocalPO__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelIsLocalPO__": {
																				"line": 12,
																				"column": 1
																			},
																			"IsCreatedInERP__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelIsCreatedInERP__": {
																				"line": 13,
																				"column": 1
																			},
																			"NoMoreInvoiceExpected__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelNoMoreInvoiceExpected__": {
																				"line": 14,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 4
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "Vendor number",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "Vendor number",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"length": 50,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelDifferentInvoicingParty__": {
																			"type": "Label",
																			"data": [
																				"DifferentInvoicingParty__"
																			],
																			"options": {
																				"label": "Different invoicing party",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"DifferentInvoicingParty__": {
																			"type": "ShortText",
																			"data": [
																				"DifferentInvoicingParty__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Different invoicing party",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"LabelOrderNumber__": {
																			"type": "Label",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"label": "Order number",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"OrderNumber__": {
																			"type": "ShortText",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"label": "Order number",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"length": 50,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelOrderDate__": {
																			"type": "Label",
																			"data": [
																				"OrderDate__"
																			],
																			"options": {
																				"label": "Order date",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"OrderDate__": {
																			"type": "DateTime",
																			"data": [
																				"OrderDate__"
																			],
																			"options": {
																				"label": "Order date",
																				"activable": true,
																				"width": "500",
																				"displayLongFormat": false,
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelOrderedAmount__": {
																			"type": "Label",
																			"data": [
																				"OrderedAmount__"
																			],
																			"options": {
																				"label": "Ordered amount",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"OrderedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"OrderedAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Ordered amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 12
																		},
																		"LabelInvoicedAmount__": {
																			"type": "Label",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"label": "Invoiced amount",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"InvoicedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Invoiced amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 14
																		},
																		"LabelDeliveredAmount__": {
																			"type": "Label",
																			"data": [
																				"DeliveredAmount__"
																			],
																			"options": {
																				"label": "Delivered amount",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"DeliveredAmount__": {
																			"type": "Decimal",
																			"data": [
																				"DeliveredAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Delivered amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 16
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "Currency",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"Currency__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"version": 1,
																				"label": "Currency",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 41
																		},
																		"LabelBuyer__": {
																			"type": "Label",
																			"data": [
																				"Buyer__"
																			],
																			"options": {
																				"label": "Buyer",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"Buyer__": {
																			"type": "ShortText",
																			"data": [
																				"Buyer__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Buyer",
																				"activable": true,
																				"width": "500",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"LabelReceiver__": {
																			"type": "Label",
																			"data": [
																				"Receiver__"
																			],
																			"options": {
																				"label": "Receiver",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"Receiver__": {
																			"type": "ShortText",
																			"data": [
																				"Receiver__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Receiver",
																				"activable": true,
																				"width": "500",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"LabelIsLocalPO__": {
																			"type": "Label",
																			"data": [
																				"IsLocalPO__"
																			],
																			"options": {
																				"label": "Is local PO_V2",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"IsLocalPO__": {
																			"type": "CheckBox",
																			"data": [
																				"IsLocalPO__"
																			],
																			"options": {
																				"label": "Is local PO_V2",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"LabelIsCreatedInERP__": {
																			"type": "Label",
																			"data": [
																				"IsCreatedInERP__"
																			],
																			"options": {
																				"label": "Is created in ERP",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"IsCreatedInERP__": {
																			"type": "CheckBox",
																			"data": [
																				"IsCreatedInERP__"
																			],
																			"options": {
																				"label": "Is created in ERP",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"LabelNoMoreInvoiceExpected__": {
																			"type": "Label",
																			"data": [
																				"NoMoreInvoiceExpected__"
																			],
																			"options": {
																				"label": "_NoMoreInvoiceExpected",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"NoMoreInvoiceExpected__": {
																			"type": "CheckBox",
																			"data": [
																				"NoMoreInvoiceExpected__"
																			],
																			"options": {
																				"label": "_NoMoreInvoiceExpected",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 39
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										}
									},
									"stamp": 20,
									"data": []
								}
							},
							"stamp": 21,
							"data": []
						}
					},
					"stamp": 22,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 23,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 24,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 25,
							"data": []
						}
					},
					"stamp": 26,
					"data": []
				}
			},
			"stamp": 27,
			"data": []
		}
	},
	"stamps": 41,
	"data": []
}