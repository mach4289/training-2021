{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"Section__": "LabelSection__",
																	"LabelSection__": "Section__",
																	"Axe__": "LabelAxe__",
																	"LabelAxe__": "Axe__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 3,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Section__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelSection__": {
																				"line": 2,
																				"column": 1
																			},
																			"Axe__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelAxe__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": 500,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20"
																			},
																			"stamp": 4
																		},
																		"LabelSection__": {
																			"type": "Label",
																			"data": [
																				"Section__"
																			],
																			"options": {
																				"label": "Section",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"Section__": {
																			"type": "ShortText",
																			"data": [
																				"Section__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Section",
																				"activable": true,
																				"width": 500,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"LabelAxe__": {
																			"type": "Label",
																			"data": [
																				"Axe__"
																			],
																			"options": {
																				"label": "Axe",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Axe__": {
																			"type": "ComboBox",
																			"data": [
																				"Axe__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "A1",
																					"1": "A2",
																					"2": "A3",
																					"3": "A4",
																					"4": "A5"
																				},
																				"keyValueMode": false,
																				"possibleKeys": {},
																				"label": "Axe",
																				"activable": true,
																				"width": 500,
																				"version": 0
																			},
																			"stamp": 8
																		}
																	},
																	"stamp": 9
																}
															},
															"stamp": 10,
															"data": []
														}
													},
													"stamp": 11,
													"data": []
												}
											}
										}
									},
									"stamp": 12,
									"data": []
								}
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 14,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 15,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 16,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 17,
							"data": []
						}
					},
					"stamp": 18,
					"data": []
				}
			},
			"stamp": 19,
			"data": []
		}
	},
	"stamps": 19,
	"data": []
}