{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "16%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"Error_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "left",
																"label": "_Error pane",
																"leftImageURL": "public\\error.png",
																"version": 0,
																"hidden": true,
																"hideTitle": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Error_field__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "20",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 9
																				},
																				"Error_field__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color2",
																						"backgroundcolor": "default",
																						"label": "Error field",
																						"version": 0,
																						"image_url": ""
																					},
																					"stamp": 10
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 11,
													"*": {
														"Process_description_pane": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Process description pane",
																"leftImageURL": "",
																"hideTitle": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 2,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Notification_from_CSV__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"Notification_from_CSV__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_Email grouping process",
																						"version": 0,
																						"image_url": ""
																					},
																					"stamp": 12
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 65
																				}
																			},
																			"stamp": 13
																		}
																	},
																	"stamp": 14,
																	"data": []
																}
															},
															"stamp": 15,
															"data": []
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"Import_pane__": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"label": "_Import pane",
																"hideTitle": false,
																"hidden": true
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_System data",
																"hidden": true,
																"hideTitle": false
															},
															"stamp": 19,
															"data": []
														}
													},
													"stamp": 20,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Next Process",
																"hideTitle": false
															},
															"stamp": 21,
															"data": []
														}
													},
													"stamp": 22,
													"data": []
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 23,
													"*": {
														"Spacer_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Spacer pane",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"hidden": true
															},
															"stamp": 24,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 25,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line3__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 26,
																			"*": {
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 27
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 28,
													"*": {
														"Summary_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "300",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Summary pane",
																"leftImageURL": "",
																"version": 0,
																"hidden": true,
																"hideTitle": false
															},
															"stamp": 29,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"SubmitDateTime__": "LabelSubmitDateTime__",
																			"LabelSubmitDateTime__": "SubmitDateTime__",
																			"CompletedDateTime__": "LabelCompletedDateTime__",
																			"LabelCompletedDateTime__": "CompletedDateTime__",
																			"State__": "LabelState__",
																			"LabelState__": "State__",
																			"CounterSentEmail__": "LabelCounterSentEmail__",
																			"LabelCounterSentEmail__": "CounterSentEmail__"
																		},
																		"version": 0
																	},
																	"stamp": 30,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line4__": {
																						"line": 1,
																						"column": 1
																					},
																					"SubmitDateTime__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSubmitDateTime__": {
																						"line": 3,
																						"column": 1
																					},
																					"CompletedDateTime__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelCompletedDateTime__": {
																						"line": 4,
																						"column": 1
																					},
																					"State__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelState__": {
																						"line": 2,
																						"column": 1
																					},
																					"CounterSentEmail__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelCounterSentEmail__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 31,
																			"*": {
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 32
																				},
																				"LabelState__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_State",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"State__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_State",
																						"version": 0
																					},
																					"stamp": 34
																				},
																				"LabelSubmitDateTime__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_Submit date time",
																						"version": 0
																					},
																					"stamp": 35
																				},
																				"SubmitDateTime__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"label": "_Submit date time",
																						"width": "300"
																					},
																					"stamp": 36
																				},
																				"LabelCompletedDateTime__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_Completion date time",
																						"version": 0
																					},
																					"stamp": 37
																				},
																				"CompletedDateTime__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"label": "_Completion date time",
																						"width": "300"
																					},
																					"stamp": 38
																				},
																				"LabelCounterSentEmail__": {
																					"type": "Label",
																					"data": [
																						"CounterSentEmail__"
																					],
																					"options": {
																						"label": "_Sent email",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"CounterSentEmail__": {
																					"type": "Integer",
																					"data": [
																						"CounterSentEmail__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_Sent email",
																						"precision_internal": 0,
																						"activable": true,
																						"width": "80",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"precision_current": 0
																					},
																					"stamp": 40
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 48,
													"*": {
														"Error_log_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Error log pane",
																"leftImageURL": "",
																"version": 0,
																"hideTitle": false,
																"hidden": true
															},
															"stamp": 49,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 50,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Error_log__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 51,
																			"*": {
																				"Error_log__": {
																					"type": "LongText",
																					"data": [
																						"Error_log__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Error log",
																						"activable": true,
																						"width": "100%",
																						"browsable": false,
																						"numberOfLines": 10,
																						"version": 0,
																						"minNbLines": 10
																					},
																					"stamp": 52
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 53,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Document Preview",
																"hideTitle": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 54
																}
															},
															"stamp": 55,
															"data": []
														}
													},
													"stamp": 56,
													"data": []
												}
											},
											"stamp": 57,
											"data": []
										}
									},
									"stamp": 58,
									"data": []
								}
							},
							"stamp": 59,
							"data": []
						}
					},
					"stamp": 60,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 62,
							"data": []
						}
					},
					"stamp": 63,
					"data": []
				}
			},
			"stamp": 64,
			"data": []
		}
	},
	"stamps": 65,
	"data": []
}