///#GLOBALS Sys
var DD;
(function (DD) {
    var EmailGrouping;
    (function (EmailGrouping) {
        var Extraction;
        (function (Extraction) {
            function Main() {
                // This file will contain an document stack per line, each stored in a JSON.
                // It will NOT be a big array of JSON.
                var tempFile = TemporaryFile.CreateFile("txt");
                var timeoutHelper = new Sys.Helpers.TimeoutHelper(400, 100);
                AppendRecordsToBeGrouped(tempFile, timeoutHelper, "PORTAL");
                AppendRecordsToBeGrouped(tempFile, timeoutHelper, "SM");
                Attach.AttachTemporaryFile(tempFile, "DocumentStacks");
            }
            Extraction.Main = Main;
            function AppendRecordsToBeGrouped(tempFile, timeoutHelper, deliveryMethod) {
                var groupingKey = GetGroupingKey(deliveryMethod);
                Variable.SetValueAsString("GroupingKey_" + deliveryMethod, JSON.stringify(groupingKey));
                var query = GetGroupableRecords(deliveryMethod, groupingKey);
                var documentRecord = null;
                var lastReadDocument = null;
                var documentStack = [];
                var attachSize = 0;
                do {
                    documentRecord = query.MoveNext();
                    if (documentRecord) {
                        var documentVars = documentRecord.GetUninheritedVars();
                        var documentExtVars = documentRecord.GetExternalVars();
                        var files = documentExtVars.GetValue_String("AttachmentFiles__", 0);
                        // The following line load the current CUSI record configuration, it allow
                        // us to use Sys.Outbound.GetParameter properly for this loop
                        Sys.DD.InitConfigurationFromRecordCache(documentRecord, true);
                        var settings = {
                            archiveCondition: Sys.DD.GetParameter("ArchiveCondition__"),
                            forceEmailValidation: Sys.DD.GetParameter("ForceMessageValidation_NOTIF_EMAIL"),
                            bounceBackEnabled: Sys.DD.GetParameter("BounceBacks_Enable")
                        };
                        var groupedFields = [];
                        for (var _i = 0, groupingKey_1 = groupingKey; _i < groupingKey_1.length; _i++) {
                            var groupingField = groupingKey_1[_i];
                            groupedFields.push(documentVars.GetValue_String(groupingField, 0));
                        }
                        var currentDocument = {
                            senderFormRUIDEX: documentVars.GetValue_String("RUIDEx", 0),
                            senderFormArchiveDuration: Sys.DD.ComputeArchiveDuration("", Data.GetValue("Delivery_method__")),
                            recipientID: documentVars.GetValue_String("Recipient_ID__", 0),
                            type: documentVars.GetValue_String("Document_Type__", 0),
                            number: documentVars.GetValue_String("Document_ID__", 0),
                            recipientDeliveryMethod: documentVars.GetValue_String("Delivery_method__", 0),
                            sendWelcomeEMail: documentExtVars.GetValue_String("SendWelcomeEmail__", 0),
                            passwordUrl: documentExtVars.GetValue_String("Password_url__", 0),
                            recipientEmailAddress: documentVars.GetValue_String("Recipient_email__", 0),
                            senderEmailAddress: documentVars.GetValue_String("Sender_email__", 0),
                            senderOwnerID: documentExtVars.GetValue_String("Sender_ownerID__", 0),
                            groupedFields: groupedFields,
                            files: files ? JSON.parse(files) : [],
                            settings: settings
                        };
                        var currentDocumentAttachmentsSize = parseInt(documentExtVars.GetValue_String("AttachmentsSize__", 0), 10);
                        attachSize += isNaN(currentDocumentAttachmentsSize) ? 0 : currentDocumentAttachmentsSize;
                        if (lastReadDocument === null) {
                            lastReadDocument = currentDocument;
                        }
                        else if (ShouldSendNewEmail(currentDocument, lastReadDocument, attachSize, deliveryMethod)) {
                            lastReadDocument = currentDocument;
                            TemporaryFile.Append(tempFile, JSON.stringify(documentStack) + "\n");
                            documentStack = [];
                            attachSize = currentDocumentAttachmentsSize;
                        }
                        documentStack.push(currentDocument);
                    }
                    timeoutHelper.NotifyIteration();
                } while (documentRecord);
                // The last stack has to be sent too
                if (documentStack.length > 0) {
                    TemporaryFile.Append(tempFile, JSON.stringify(documentStack) + "\n");
                }
            }
            function GetGroupingKey(deliveryMethod) {
                var groupKeyFromUserExit = Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.CustomizeEmailGroupingKey", deliveryMethod);
                if (groupKeyFromUserExit === null) {
                    Log.Info("Using default " + deliveryMethod + " grouping key because the user exit Lib.DD.Customization.Deliveries.CustomizeEmailGroupingKey returned null or isn't defined");
                }
                if (!Sys.Helpers.IsArray(groupKeyFromUserExit)) {
                    Log.Error("Using default " + deliveryMethod + " grouping key because object returned wasn't an array in Lib.DD.Customization.Deliveries.CustomizeEmailGroupingKey");
                }
                else if (groupKeyFromUserExit.length === 0) {
                    Log.Error("Using default " + deliveryMethod + " grouping key because was returned an empty array in Lib.DD.Customization.Deliveries.CustomizeEmailGroupingKey");
                }
                else {
                    Log.Info("Custom " + deliveryMethod + " grouping key: " + JSON.stringify(groupKeyFromUserExit));
                    return groupKeyFromUserExit;
                }
                return ["Recipient_ID__", "Recipient_email__"];
            }
            function ShouldSendNewEmail(currentDocument, lastReadDocument, attachSize, deliveryMethod) {
                // WARNING: here we're supposing than the query order determinates a correct
                // set of documents with THE SAME configuration. Multi-configurations sending
                // to a same customer IS NOT SUPPORTED.
                var maxAttachSize = parseInt(Sys.DD.GetParameter("GroupEmailMaxAttachSize"), 10); // Max attachment size in a single email
                if (isNaN(maxAttachSize)) {
                    Log.Error("Error: wizard parameter 'GroupEmailMaxAttachSize' is not a number, value is: " + maxAttachSize);
                    Log.Warn("Setting default value to: 6");
                    maxAttachSize = 6;
                }
                var sameGroupingKey = lastReadDocument.groupedFields.every(function (v, i) { return v === currentDocument.groupedFields[i]; });
                if (deliveryMethod === "SM") {
                    var attachMaxSizeReached = (attachSize >= maxAttachSize * 1024 * 1024);
                    return (!sameGroupingKey || attachMaxSizeReached);
                }
                else {
                    return !sameGroupingKey;
                }
            }
            function GetGroupableRecords(deliveryMethod, groupingKey) {
                // It's important not to create "AsProcessAdmin" because subaccounts don't have to group for brothers subaccounts
                var processQuery = Process.CreateQuery();
                var processID = "CD#" + Process.GetProcessID("DD - SenderForm");
                var filterForDMPortal = "(&(Delivery_method__=PORTAL)(State=100))";
                var filterForDMEmail = "(&(Delivery_method__=SM)(State=90))";
                var filterToUse = (deliveryMethod === "SM") ? filterForDMEmail : filterForDMPortal;
                var filter = "(&(Email_grouping_state__=GROUPABLE_WAITING)(Deleted=0)" + filterToUse + ")";
                var order = "";
                for (var _i = 0, groupingKey_2 = groupingKey; _i < groupingKey_2.length; _i++) {
                    var groupField = groupingKey_2[_i];
                    order += groupField + " ASC, ";
                }
                Log.Info("Query for delivery method " + deliveryMethod + " is:");
                Log.Info("   > filter: " + filter);
                Log.Info("   > order:  " + order);
                processQuery.SetSpecificTable(processID);
                processQuery.SetFilter(filter);
                processQuery.SetSearchInArchive(false);
                processQuery.SetAttributesList("*");
                processQuery.SetSortOrder(order);
                processQuery.SetOptionEx("Limit=-1");
                processQuery.SetOptionEx("DONOTGETLOCALDBFILES=1");
                var eddRecords = processQuery.MoveFirst();
                if (!eddRecords) {
                    throw new Error("Unable to query groupables records, error: " + processQuery.GetLastErrorMessage());
                }
                return processQuery;
            }
            Main();
        })(Extraction = EmailGrouping.Extraction || (EmailGrouping.Extraction = {}));
    })(EmailGrouping = DD.EmailGrouping || (DD.EmailGrouping = {}));
})(DD || (DD = {}));
