///#GLOBALS Lib
var options = {
	maxElementsPerRecall: 50,
	maxRetriesPerRecord: 10
};
Process.SetTimeOut(3600);
var NOT_FINISHED = 0;
if (Lib.CallScheduledAction.callActionOnDocumentsFromCSV(options) === NOT_FINISHED)
{
	Process.Sleep(10);
	Process.RecallScript("call_scheduled_action_in_progress", true);
}