{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"label": "_Document data",
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"PaymentTermCode__": "LabelPaymentTermCode__",
																	"LabelPaymentTermCode__": "PaymentTermCode__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"DiscountPeriod__": "LabelDiscountPeriod__",
																	"LabelDiscountPeriod__": "DiscountPeriod__",
																	"DayLimit__": "LabelDayLimit__",
																	"LabelDayLimit__": "DayLimit__",
																	"DiscountRate__": "LabelDiscountRate__",
																	"LabelDiscountRate__": "DiscountRate__",
																	"LatePaymentFeeRate__": "LabelLatePaymentFeeRate__",
																	"LabelLatePaymentFeeRate__": "LatePaymentFeeRate__",
																	"ReferenceDate__": "LabelReferenceDate__",
																	"LabelReferenceDate__": "ReferenceDate__",
																	"EndOfMonth__": "LabelEndOfMonth__",
																	"LabelEndOfMonth__": "EndOfMonth__",
																	"PaymentDay__": "LabelPaymentDay__",
																	"LabelPaymentDay__": "PaymentDay__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 10,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"PaymentTermCode__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelPaymentTermCode__": {
																				"line": 2,
																				"column": 1
																			},
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"DiscountPeriod__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelDiscountPeriod__": {
																				"line": 4,
																				"column": 1
																			},
																			"DayLimit__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelDayLimit__": {
																				"line": 5,
																				"column": 1
																			},
																			"DiscountRate__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDiscountRate__": {
																				"line": 6,
																				"column": 1
																			},
																			"LatePaymentFeeRate__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelLatePaymentFeeRate__": {
																				"line": 7,
																				"column": 1
																			},
																			"ReferenceDate__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelReferenceDate__": {
																				"line": 8,
																				"column": 1
																			},
																			"EndOfMonth__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelEndOfMonth__": {
																				"line": 10,
																				"column": 1
																			},
																			"PaymentDay__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelPaymentDay__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Company code",
																				"activable": true,
																				"width": "400",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"version": 0
																			},
																			"stamp": 4
																		},
																		"LabelPaymentTermCode__": {
																			"type": "Label",
																			"data": [
																				"PaymentTermCode__"
																			],
																			"options": {
																				"label": "Payment term",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"PaymentTermCode__": {
																			"type": "ShortText",
																			"data": [
																				"PaymentTermCode__"
																			],
																			"options": {
																				"label": "Payment term",
																				"activable": true,
																				"width": "400",
																				"browsable": false,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": "400",
																				"length": 400,
																				"browsable": false,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelDiscountPeriod__": {
																			"type": "Label",
																			"data": [
																				"DiscountPeriod__"
																			],
																			"options": {
																				"label": "Discount Period",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"DiscountPeriod__": {
																			"type": "ShortText",
																			"data": [
																				"DiscountPeriod__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Discount Period",
																				"activable": true,
																				"width": "200",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelDayLimit__": {
																			"type": "Label",
																			"data": [
																				"DayLimit__"
																			],
																			"options": {
																				"label": "Day limit",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"DayLimit__": {
																			"type": "ShortText",
																			"data": [
																				"DayLimit__"
																			],
																			"options": {
																				"version": 1,
																				"label": "Day limit",
																				"activable": true,
																				"width": "200",
																				"browsable": false,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 12
																		},
																		"LabelDiscountRate__": {
																			"type": "Label",
																			"data": [
																				"DiscountRate__"
																			],
																			"options": {
																				"label": "Discount Rate",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"DiscountRate__": {
																			"type": "ShortText",
																			"data": [
																				"DiscountRate__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Discount Rate",
																				"activable": true,
																				"width": "200",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"LabelLatePaymentFeeRate__": {
																			"type": "Label",
																			"data": [
																				"LatePaymentFeeRate__"
																			],
																			"options": {
																				"label": "Late payment fee rate",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LatePaymentFeeRate__": {
																			"type": "ShortText",
																			"data": [
																				"LatePaymentFeeRate__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Late payment fee rate",
																				"activable": true,
																				"width": "200",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"LabelReferenceDate__": {
																			"type": "Label",
																			"data": [
																				"ReferenceDate__"
																			],
																			"options": {
																				"label": "Reference date",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"ReferenceDate__": {
																			"type": "ComboBox",
																			"data": [
																				"ReferenceDate__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "Invoice date",
																					"1": "Posting date"
																				},
																				"keyValueMode": false,
																				"possibleKeys": {
																					"0": "Invoice date",
																					"1": "Posting date"
																				},
																				"label": "Reference date",
																				"activable": true,
																				"width": "200",
																				"version": 1
																			},
																			"stamp": 18
																		},
																		"LabelPaymentDay__": {
																			"type": "Label",
																			"data": [
																				"PaymentDay__"
																			],
																			"options": {
																				"label": "_PaymentDay",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"PaymentDay__": {
																			"type": "Integer",
																			"data": [
																				"PaymentDay__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_PaymentDay",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "200",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 33
																		},
																		"LabelEndOfMonth__": {
																			"type": "Label",
																			"data": [
																				"EndOfMonth__"
																			],
																			"options": {
																				"label": "End of month",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"EndOfMonth__": {
																			"type": "CheckBox",
																			"data": [
																				"EndOfMonth__"
																			],
																			"options": {
																				"label": "End of month",
																				"activable": true,
																				"version": 0
																			},
																			"stamp": 20
																		}
																	},
																	"stamp": 21
																}
															},
															"stamp": 22,
															"data": []
														}
													},
													"stamp": 23,
													"data": []
												}
											}
										}
									},
									"stamp": 24,
									"data": []
								}
							},
							"stamp": 25,
							"data": []
						}
					},
					"stamp": 26,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 27,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 28,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 29,
							"data": []
						}
					},
					"stamp": 30,
					"data": []
				}
			},
			"stamp": 31,
			"data": []
		}
	},
	"stamps": 33,
	"data": []
}