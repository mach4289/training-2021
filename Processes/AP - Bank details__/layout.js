{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"BankCountry__": "LabelBankCountry__",
																	"LabelBankCountry__": "BankCountry__",
																	"BankKey__": "LabelBankKey__",
																	"LabelBankKey__": "BankKey__",
																	"BankAccount__": "LabelBankAccount__",
																	"LabelBankAccount__": "BankAccount__",
																	"AccountHolder__": "LabelAccountHolder__",
																	"LabelAccountHolder__": "AccountHolder__",
																	"IBAN__": "LabelIBAN__",
																	"LabelIBAN__": "IBAN__",
																	"BankName__": "LabelBankName__",
																	"LabelBankName__": "BankName__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"BankAccountID__": "LabelBankAccountID__",
																	"LabelBankAccountID__": "BankAccountID__",
																	"ControlKey__": "LabelControlKey__",
																	"LabelControlKey__": "ControlKey__",
																	"SWIFT_BICCode__": "LabelSWIFT_BICCode__",
																	"LabelSWIFT_BICCode__": "SWIFT_BICCode__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"RoutingCode__": "LabelRoutingCode__",
																	"LabelRoutingCode__": "RoutingCode__",
																	"Treeview__": "Label_Treeview",
																	"Label_Treeview": "Treeview__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 14,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"BankCountry__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelBankCountry__": {
																				"line": 3,
																				"column": 1
																			},
																			"BankKey__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelBankKey__": {
																				"line": 6,
																				"column": 1
																			},
																			"BankAccount__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelBankAccount__": {
																				"line": 7,
																				"column": 1
																			},
																			"AccountHolder__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelAccountHolder__": {
																				"line": 4,
																				"column": 1
																			},
																			"IBAN__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelIBAN__": {
																				"line": 5,
																				"column": 1
																			},
																			"BankName__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelBankName__": {
																				"line": 12,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"BankAccountID__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelBankAccountID__": {
																				"line": 13,
																				"column": 1
																			},
																			"ControlKey__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelControlKey__": {
																				"line": 8,
																				"column": 1
																			},
																			"SWIFT_BICCode__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelSWIFT_BICCode__": {
																				"line": 9,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 11,
																				"column": 1
																			},
																			"RoutingCode__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelRoutingCode__": {
																				"line": 10,
																				"column": 1
																			},
																			"Label_Treeview": {
																				"line": 14,
																				"column": 1
																			},
																			"Treeview__": {
																				"line": 14,
																				"column": 2
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 31
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 29
																		},
																		"LabelBankCountry__": {
																			"type": "Label",
																			"data": [
																				"BankCountry__"
																			],
																			"options": {
																				"label": "_BankCountry",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"BankCountry__": {
																			"type": "ShortText",
																			"data": [
																				"BankCountry__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BankCountry",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelAccountHolder__": {
																			"type": "Label",
																			"data": [
																				"AccountHolder__"
																			],
																			"options": {
																				"label": "_AccountHolder",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"AccountHolder__": {
																			"type": "ShortText",
																			"data": [
																				"AccountHolder__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_AccountHolder",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 21
																		},
																		"LabelIBAN__": {
																			"type": "Label",
																			"data": [
																				"IBAN__"
																			],
																			"options": {
																				"label": "_IBAN",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"IBAN__": {
																			"type": "ShortText",
																			"data": [
																				"IBAN__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_IBAN",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 23
																		},
																		"LabelBankKey__": {
																			"type": "Label",
																			"data": [
																				"BankKey__"
																			],
																			"options": {
																				"label": "_BankKey",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"BankKey__": {
																			"type": "ShortText",
																			"data": [
																				"BankKey__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BankKey",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelBankAccount__": {
																			"type": "Label",
																			"data": [
																				"BankAccount__"
																			],
																			"options": {
																				"label": "_BankAccount",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"BankAccount__": {
																			"type": "ShortText",
																			"data": [
																				"BankAccount__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BankAccount",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 19
																		},
																		"LabelControlKey__": {
																			"type": "Label",
																			"data": [
																				"ControlKey__"
																			],
																			"options": {
																				"label": "_ControlKey",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"ControlKey__": {
																			"type": "ShortText",
																			"data": [
																				"ControlKey__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ControlKey",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 35
																		},
																		"LabelSWIFT_BICCode__": {
																			"type": "Label",
																			"data": [
																				"SWIFT_BICCode__"
																			],
																			"options": {
																				"label": "_SWIFT/BICCode",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"SWIFT_BICCode__": {
																			"type": "ShortText",
																			"data": [
																				"SWIFT_BICCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 0,
																				"label": "_SWIFT/BICCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 37
																		},
																		"LabelRoutingCode__": {
																			"type": "Label",
																			"data": [
																				"RoutingCode__"
																			],
																			"options": {
																				"label": "_RoutingCode",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"RoutingCode__": {
																			"type": "ShortText",
																			"data": [
																				"RoutingCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_RoutingCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 41
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "_Currency",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"Currency__": {
																			"type": "ShortText",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 0,
																				"label": "_Currency",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"browsable": false
																			},
																			"stamp": 39
																		},
																		"LabelBankName__": {
																			"type": "Label",
																			"data": [
																				"BankName__"
																			],
																			"options": {
																				"label": "_BankName",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"BankName__": {
																			"type": "ShortText",
																			"data": [
																				"BankName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BankName",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"LabelBankAccountID__": {
																			"type": "Label",
																			"data": [
																				"BankAccountID__"
																			],
																			"options": {
																				"label": "_BankAccountID",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"BankAccountID__": {
																			"type": "ShortText",
																			"data": [
																				"BankAccountID__"
																			],
																			"options": {
																				"label": "_BankAccountID",
																				"activable": true,
																				"width": "500",
																				"length": 50,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 33
																		},
																		"Label_Treeview": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "_Treeview"
																			},
																			"stamp": 42
																		},
																		"Treeview__": {
																			"type": "Treeview",
																			"data": false,
																			"options": {
																				"version": 1,
																				"label": "_Treeview",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"NbLines": 2,
																				"MaxNbLines": 3,
																				"horizontal": true
																			},
																			"stamp": 43
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 43,
	"data": []
}