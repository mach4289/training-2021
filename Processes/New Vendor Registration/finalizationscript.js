var CultureHelper = {
	cultures:
	{
		en: "en-EN",
		fr: "fr-FR",
		de: "de-DE",
		it: "it-IT",
		es: "es-ES",
		"zh-CN": "zh-CN"
	},
	language: "en",
	selectedCulture: "",
	computeCulture: function ()
	{
		this.language = Data.GetValue("VendorLanguage__");
		this.selectedCulture = this.cultures[this.language];
	}
};
var RegistrationHelper = {
	CreateRegistration: function ()
	{
		if (UserHelper.clonedUser)
		{
			var userVars = UserHelper.clonedUser.GetVars();
			var vendorRegistrationProcess = Process.CreateProcessInstanceForUser("Vendor Registration", Data.GetValue("OwnerId"));
			var capExtVars = vendorRegistrationProcess.GetExternalVars();
			capExtVars.AddValue_String("creatorOwnerId", Data.GetValue("OwnerId"), true);
			capExtVars.AddValue_String("creatorOwnerEmail", Users.GetUser(Data.GetValue("OwnerId")).GetValue("login"), true);
			capExtVars.AddValue_String("guestUserLogin", userVars.GetValue_String("Login", 0), true);
			vendorRegistrationProcess.Process();
		}
	}
};
var UserHelper = {
	guestUser: null,
	clonedUser: null,
	GetUserTemplateGuest: function ()
	{
		if (this.guestUser === null)
		{
			var internalUser = Users.GetUserAsProcessAdmin(Data.GetValue("OwnerId"));
			var supplierVars = internalUser.GetVars();
			var accountID = supplierVars.GetValue_String("AccountID", 0);
			this.guestUser = Users.GetUserAsProcessAdmin(accountID + "$_vendor_guest_template_");
		}
	},
	CloneTemplateGuest: function ()
	{
		this.GetUserTemplateGuest();
		if (this.guestUser)
		{
			var email = Data.GetValue("VendorEmail__");
			this.clonedUser = this.guestUser.Clone(email,
			{
				emailaddress: email,
				culture: CultureHelper.selectedCulture,
				language: CultureHelper.language
			});
			return this;
		}
		throw "An error occcured during the creation of the vendor. Try again later, if the problem persist, contact the support.";
	}
};
if (Data.GetValue("VendorEmail__"))
{
	CultureHelper.computeCulture();
	UserHelper.CloneTemplateGuest();
	RegistrationHelper.CreateRegistration();
}