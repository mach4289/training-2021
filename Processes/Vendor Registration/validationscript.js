///#GLOBALS Sys Lib
var companyCode, vendorNumber;
var MasterDataStructure = /** @class */ (function ()
{
	function MasterDataStructure()
	{
		this.companyProfile = {
			Company__: "",
			MailSub__: "",
			Street__: "",
			POBox__: "",
			Zip_Code__: "",
			City__: "",
			Mail_State__: "",
			Country__: "",
			Phone_Number__: "",
			Fax_Number__: "",
			Website__: "",
			CompanyStructure__: "",
			YearsInBusiness__: "",
			NumberOfEmployees__: "",
			TaxID__: "",
			VendorRegistrationDUNSNumber__: "",
			TaxStatus__: ""
		};
		this.contacts = {
			CompanyOfficersTable__: []
		};
		this.payment = {
			CompanyBankAccountsTable__: [],
			paymentInfoPane:
			{
				PaymentMethod__: "",
				PaymentTerms__: ""
			}
		};
		this.documents = {
			DocumentsTable__: []
		};
	}
	return MasterDataStructure;
}());
//
// HELPER FUNCTIONS
//
function GiveReadRightToCurrentContributor()
{
	var idx = workflowController.GetContributorIndex();
	var currentContributor = workflowController.GetContributorAt(idx);
	Process.SetRight(currentContributor.login, "read");
}

function Forward()
{
	var idx = workflowController.GetContributorIndex();
	var step = workflowController.GetContributorAt(idx);
	Process.Forward(step.login);
	Process.LeaveForm();
}
// Forward to main account to have access to master data and workflow rules
// Do not forget to forward back to the correct user in specified action!
function ForwardToMainAccountAndCallAction(actionToCall, isAsync)
{
	if (isAsync === void 0)
	{
		isAsync = false;
	}
	var defaultMainAccountUser = Lib.AP.VendorPortal.GetDefaultApUserLogin();
	Log.Info("Forwarding to main account: " + defaultMainAccountUser);
	Process.Forward(defaultMainAccountUser);
	Log.Info("Calling action " + actionToCall);
	Process.RecallScript(actionToCall, isAsync);
}

function GetVendorLinksInfos()
{
	var vendorLogin = Variable.GetValueAsString("vendorShortLogin");
	var query = Process.CreateQueryAsProcessAdmin();
	query.Reset();
	query.SetSpecificTable("AP - Vendors links__");
	query.SetAttributesList("CompanyCode__,Number__");
	query.SetFilter("(|(ShortLogin__=" + vendorLogin + ")(ShortLoginPAC__=" + vendorLogin + "))");
	query.MoveFirst();
	var record = query.MoveNextRecord();
	if (record)
	{
		companyCode = record.GetVars().GetValue_String("CompanyCode__", 0);
		vendorNumber = record.GetVars().GetValue_String("Number__", 0);
	}
	else
	{
		Log.Error("Could not retrieve vendor links for vendor " + vendorLogin);
	}
}

function GetMasterData()
{
	var masterData = new MasterDataStructure();
	var query = Process.CreateQueryAsProcessAdmin();
	query.Reset();
	query.SetSpecificTable("AP - Vendors__");
	query.SetAttributesList("Name__,Sub__,Street__,PostOfficeBox__,PostalCode__,City__,Region__,Country__,PhoneNumber__,FaxNumber__,VATNumber__,DUNSNumber__");
	var queryFilter = "Number__=" + vendorNumber;
	queryFilter = queryFilter.AddCompanyCodeFilter(companyCode);
	query.SetFilter(queryFilter);
	if (query.MoveFirst())
	{
		var record = query.MoveNextRecord();
		if (record)
		{
			var vars = record.GetVars();
			masterData.companyProfile.Company__ = vars.GetValue_String("Name__", 0);
			masterData.companyProfile.MailSub__ = vars.GetValue_String("Sub__", 0);
			masterData.companyProfile.Street__ = vars.GetValue_String("Street__", 0);
			masterData.companyProfile.POBox__ = vars.GetValue_String("PostOfficeBox__", 0);
			masterData.companyProfile.Zip_Code__ = vars.GetValue_String("PostalCode__", 0);
			masterData.companyProfile.City__ = vars.GetValue_String("City__", 0);
			masterData.companyProfile.Mail_State__ = vars.GetValue_String("Region__", 0);
			masterData.companyProfile.Country__ = vars.GetValue_String("Country__", 0);
			masterData.companyProfile.Phone_Number__ = vars.GetValue_String("PhoneNumber__", 0);
			masterData.companyProfile.Fax_Number__ = vars.GetValue_String("FaxNumber__", 0);
			masterData.companyProfile.TaxID__ = vars.GetValue_String("VATNumber__", 0);
			masterData.companyProfile.VendorRegistrationDUNSNumber__ = vars.GetValue_String("DUNSNumber__", 0);
		}
	}
	query.Reset();
	query.SetSpecificTable("AP - Vendors officers__");
	query.SetAttributesList("*");
	queryFilter = "VendorNumber__=" + vendorNumber;
	queryFilter = queryFilter.AddCompanyCodeFilter(companyCode);
	query.SetFilter(queryFilter);
	query.SetSortOrder("TableIndex__ ASC");
	if (query.MoveFirst())
	{
		var record = query.MoveNextRecord();
		while (record)
		{
			var vars = record.GetVars();
			var officersItem = {
				Role__: vars.GetValue_String("Role__", 0),
				FirstName__: vars.GetValue_String("FirstName__", 0),
				LastName__: vars.GetValue_String("LastName__", 0),
				Email__: vars.GetValue_String("Email__", 0)
			};
			masterData.contacts.CompanyOfficersTable__.push(officersItem);
			record = query.MoveNextRecord();
		}
	}
	query.Reset();
	query.SetSpecificTable("AP - Bank details__");
	query.SetAttributesList("*");
	queryFilter = "VendorNumber__=" + vendorNumber;
	queryFilter = queryFilter.AddCompanyCodeFilter(companyCode);
	query.SetFilter(queryFilter);
	if (query.MoveFirst())
	{
		var record = query.MoveNextRecord();
		while (record)
		{
			var vars = record.GetVars();
			var bankItem = {
				BankCountry__: vars.GetValue_String("BankCountry__", 0),
				BankAccountHolder__: vars.GetValue_String("AccountHolder__", 0),
				BankAccountNumber__: vars.GetValue_String("BankAccount__", 0),
				BankIBAN__: vars.GetValue_String("IBAN__", 0),
				BankKey__: vars.GetValue_String("BankKey__", 0),
				ControlKey__: vars.GetValue_String("ControlKey__", 0),
				SWIFT_BICCode__: vars.GetValue_String("SWIFT_BICCode__", 0),
				Currency__: vars.GetValue_String("Currency__", 0),
				RoutingCode__: vars.GetValue_String("RoutingCode__", 0)
			};
			masterData.payment.CompanyBankAccountsTable__.push(bankItem);
			record = query.MoveNextRecord();
		}
	}
	query.Reset();
	query.SetSpecificTable("Vendor_company_extended_properties__");
	query.SetAttributesList("*");
	queryFilter = "VendorNumber__=" + vendorNumber;
	queryFilter = queryFilter.AddCompanyCodeFilter(companyCode);
	query.SetFilter(queryFilter);
	if (query.MoveFirst())
	{
		var record = query.MoveNextRecord();
		if (record)
		{
			var vars = record.GetVars();
			masterData.companyProfile.NumberOfEmployees__ = vars.GetValue_String("NumberOfEmployees__", 0);
			masterData.companyProfile.YearsInBusiness__ = vars.GetValue_String("YearsInBusiness__", 0);
			masterData.companyProfile.Website__ = vars.GetValue_String("Website__", 0);
			masterData.companyProfile.TaxStatus__ = vars.GetValue_String("TaxStatus__", 0);
			masterData.companyProfile.CompanyStructure__ = vars.GetValue_String("CompanyStructure__", 0);
			masterData.payment.paymentInfoPane.PaymentMethod__ = vars.GetValue_String("PaymentMethod__", 0);
			masterData.payment.paymentInfoPane.PaymentTerms__ = vars.GetValue_String("PaymentTerms__", 0);
		}
	}
	return masterData;
}

function FillInterfaceFromMasterData(masterData)
{
	function fillHeaderFields(masterDataPane)
	{
		for (var _i = 0, _a = Object.keys(masterDataPane); _i < _a.length; _i++)
		{
			var field = _a[_i];
			Data.SetValue(field, masterDataPane[field]);
		}
	}

	function fillTable(tableName, masterDataTable)
	{
		var table = Data.GetTable(tableName);
		table.SetItemCount(masterDataTable.length);
		for (var i = 0; i < masterDataTable.length; i++)
		{
			var item = table.GetItem(i);
			for (var _i = 0, _a = Object.keys(masterDataTable[i]); _i < _a.length; _i++)
			{
				var field = _a[_i];
				if (Object.prototype.hasOwnProperty.call(masterDataTable[i], field))
				{
					item.SetValue(field, masterDataTable[i][field]);
				}
			}
		}
	}
	fillHeaderFields(masterData.companyProfile);
	fillHeaderFields(masterData.payment.paymentInfoPane);
	fillTable("CompanyOfficersTable__", masterData.contacts.CompanyOfficersTable__);
	fillTable("CompanyBankAccountsTable__", masterData.payment.CompanyBankAccountsTable__);
}

function CheckIbanFormat()
{
	var bankDetailsTable = Data.GetTable("CompanyBankAccountsTable__");
	var isValid = true;
	for (var i = 0; i < bankDetailsTable.GetItemCount(); i++)
	{
		var item = bankDetailsTable.GetItem(i);
		var country = item.GetValue("BankCountry__");
		if (country && Sys.Helpers.Iban.IsIbanCountry(country))
		{
			var iban = item.GetValue("BankIBAN__");
			var isIbanValid = iban ? Sys.Helpers.Iban.IsValid(iban, country) : true;
			isValid = isValid && isIbanValid;
			if (!isIbanValid)
			{
				item.SetError("BankIBAN__", "_invalid iban format");
				Log.Error("Invalid iban format for country " + country + ": " + iban);
			}
		}
	}
	return isValid;
}

function CheckVATFormat()
{
	var VATNumber = Data.GetValue("TaxID__");
	var country = Data.GetValue("Country__");
	if (!Sys.Helpers.VAT.IsVATCountry(country))
	{
		return true;
	}
	var vatInfo = Sys.Helpers.VAT.CheckVAT(VATNumber, [country]);
	return vatInfo.isValid;
}

function CheckVendorOnSisId(userLanguage)
{
	var SiSidLogin = Sys.Parameters.GetInstance("AP").GetParameter("SiSidLogin");
	var SiSidPassword = Sys.Parameters.GetInstance("AP").GetParameter("SiSidPassword");
	var bankDetailsTable = Data.GetTable("CompanyBankAccountsTable__");
	var registrationId = Data.GetValue("TaxID__");
	var country = Data.GetValue("Country__");
	var isIban = false;
	var ibanVerificationResult = [];
	var tempResult = {};
	for (var i = 0; i < bankDetailsTable.GetItemCount(); i++)
	{
		var item = bankDetailsTable.GetItem(i);
		var iban = item.GetValue("BankIBAN__");
		if (iban)
		{
			isIban = true;
			var options = {
				clientId: SiSidLogin,
				clientSecret: SiSidPassword,
				iban: iban,
				registrationId: registrationId,
				country: country,
				responseLanguage: userLanguage
			};
			tempResult = Sys.Helpers.Sis_ID.CheckIBAN(options) ||
			{};
			if (isNaN(tempResult.score))
			{
				tempResult.score = 50;
				tempResult.reasons = ["Error"];
				tempResult.reasonLabels = {
					"reason.code.Error": Language.Translate("_SisId technical error")
				};
				Log.Error("Sis id error: " + (tempResult ? tempResult.moreInformation : "unknow error"));
			}
			ibanVerificationResult.push(
			{
				IBAN: iban,
				score: tempResult.score,
				messageCode: tempResult.reasons,
				message: tempResult.reasonLabels
			});
			item.SetValue("BankIBANScore__", tempResult.score);
			Variable.SetValueAsString("SisidErrorAsWarning", false);
		}
	}
	if (isIban)
	{
		Variable.SetValueAsString("SisidIBANResults", JSON.stringify(ibanVerificationResult));
	}
	else
	{
		Variable.SetValueAsString("SisidIBANResults", "");
	}
}

function GetSisIdResultMessage()
{
	var resultVar = Variable.GetValueAsString("SisidIBANResults");
	var sisIdResults;
	var warningCount = 0;
	var errorCount = 0;
	var successCount = 0;
	var message = "";
	if (resultVar)
	{
		try
		{
			sisIdResults = JSON.parse(resultVar);
			for (var _i = 0, sisIdResults_1 = sisIdResults; _i < sisIdResults_1.length; _i++)
			{
				var sisIdResult = sisIdResults_1[_i];
				if (sisIdResult.score <= Sys.Helpers.Sis_ID.defaultScoreLimits.error)
				{
					errorCount++;
				}
				else if (sisIdResult.score < Sys.Helpers.Sis_ID.defaultScoreLimits.warning)
				{
					warningCount++;
				}
				else if (sisIdResult.score >= Sys.Helpers.Sis_ID.defaultScoreLimits.warning)
				{
					successCount++;
				}
			}
			if (successCount > 0)
			{
				message += Language.Translate("_SisId returned {0} IBAN(s) in success", true, successCount) + "\n";
			}
			if (warningCount > 0)
			{
				message += Language.Translate("_SisId returned {0} IBAN(s) in warning", true, warningCount) + "\n";
			}
			if (errorCount > 0)
			{
				message += Language.Translate("_SisId returned {0} IBAN(s) in error", true, errorCount) + "\n";
			}
			return message.slice(0, -1);
		}
		catch (e)
		{
			// Bad JSON
		}
	}
	return Language.Translate("_Unable to read SisIdResult");
}

function DoNeedToCheckVendorOnSisId()
{
	var isDataValid = Data.GetValue("TaxID__");
	var sisIdEnabled = Sys.Parameters.GetInstance("AP").GetParameter("EnableSiSid", "0") === "1";
	var bankDetailsModified = ModifiedFieldsHandling.GetModifiedData().tables.indexOf("CompanyBankAccountsTable__") !== -1;
	return isDataValid && sisIdEnabled && (Data.GetValue("RegistrationType__") !== "update" || bankDetailsModified);
}
var ModifiedFieldsHandling = {
	masterDataProcessVariable: "FieldsFromMasterData",
	modifiedFieldsProcessVariable: "ModifiedFields",
	SerializeModifiedData: function ()
	{
		var modifiedData = this.GetModifiedData();
		Variable.SetValueAsString(this.modifiedFieldsProcessVariable, JSON.stringify(modifiedData));
	},
	GetModifiedData: function ()
	{
		var initFlexible = Variable.GetValueAsString(this.masterDataProcessVariable);
		var initData = this.ParseStringToJSON(initFlexible);
		var currFlexible = Lib.FlexibleFormToJSON.Serializer.GetJSON();
		var currData = this.ParseStringToJSON(currFlexible);
		var modifiedData = {
			fields: [],
			tables: []
		};
		if (!initData ||
			!initData.fields ||
			!initData.tables ||
			!currData ||
			!currData.fields ||
			!initData.tables)
		{
			return modifiedData;
		}
		var fieldsToIgnore = [
			"Workflow__",
			"ProcessStatus__"
		];
		for (var i = 0; i < Data.GetNbFields(); i++)
		{
			var fieldName = Data.GetFieldName(i);
			if (fieldsToIgnore.indexOf(fieldName) === -1 && currData.fields[fieldName] !== initData.fields[fieldName])
			{
				modifiedData.fields.push(fieldName);
			}
		}
		for (var _i = 0, _a = Object.keys(currData.tables); _i < _a.length; _i++)
		{
			var tableName = _a[_i];
			if (fieldsToIgnore.indexOf(tableName) === -1 && this.IsTableModified(initData.tables[tableName], currData.tables[tableName]))
			{
				modifiedData.tables.push(tableName);
			}
		}
		return modifiedData;
	},
	IsTableModified: function (refTable, modifiedTable)
	{
		return JSON.stringify(refTable) !== JSON.stringify(modifiedTable);
	},
	ParseStringToJSON: function (flexibleString)
	{
		try
		{
			var flexibleJSON = JSON.parse(flexibleString);
			return flexibleJSON.flexible;
		}
		catch (ex)
		{
			Log.Warn("Unable to fetch initial master data");
		}
		return null;
	},
	SerializeFormToProcessVariable: function ()
	{
		Variable.SetValueAsString(this.masterDataProcessVariable, Lib.FlexibleFormToJSON.Serializer.GetJSON());
	},
	GetFlattedModifiedFields: function ()
	{
		if (Data.GetValue("RegistrationType__") === "update")
		{
			var modifiedFieldsString = Variable.GetValueAsString("ModifiedFields");
			if (modifiedFieldsString.length)
			{
				try
				{
					var modifiedFieldsJSON = JSON.parse(modifiedFieldsString);
					return modifiedFieldsJSON.fields.concat(modifiedFieldsJSON.tables);
				}
				catch (ex)
				{
					Log.Warn("Error when parsing modifiedFields");
				}
			}
		}
		return [];
	}
};

function AnotherRegistrationUpdateIsPending()
{
	var query = Process.CreateQuery();
	query.SetSpecificTable("CDNAME#Vendor Registration");
	query.SetAttributesList("RuidEx,State,Deleted,VendorNumber__,CompanyCode__");
	var queryFilter = "(&(State<100)(Deleted=0)(!(MsnEx=" + Data.GetValue("MsnEx") + "))(VendorNumber__=" + vendorNumber + ")(CompanyCode__=" + companyCode + "))";
	query.SetFilter(queryFilter);
	if (query.MoveFirst())
	{
		var record = query.MoveNext();
		if (record)
		{
			var uninheritedVars = record.GetUninheritedVars();
			var ruidEx = "[ could not retrieve the associated RuidEx ]";
			if (uninheritedVars)
			{
				ruidEx = uninheritedVars.GetValue_String("RUIDEX", 0);
			}
			Log.Warn("Another registration update found : " + ruidEx);
			return true;
		}
	}
	Log.Info("No other registration update found");
	return false;
}
var VendorHelper = {
	CreateSalesAdminVendor: function ()
	{
		// Create first sales admin
		var officersTable = Data.GetTable("CompanyOfficersTable__");
		var item = officersTable.GetItem(0);
		if (item)
		{
			var parameters = {
				VendorNumber__: Data.GetValue("VendorNumber__"),
				VendorName__: Data.GetValue("Company__"),
				CompanyCode__: Data.GetValue("CompanyCode__"),
				VendorContactEmail__: item.GetValue("Email__"),
				FirstName__: item.GetValue("FirstName__"),
				LastName__: item.GetValue("LastName__"),
				Configuration__: "Default"
			};
			// Retrieve Language/Culture/Timezone from guest
			var guestLogin = Variable.GetValueAsString("guestUserLogin");
			if (guestLogin)
			{
				var guestUser = Users.GetUser(guestLogin);
				if (guestUser)
				{
					parameters.Language = guestUser.GetValue("Language");
					parameters.Culture = guestUser.GetValue("Culture");
					parameters.TimeZoneIdentifier = guestUser.GetValue("TimeZoneIdentifier");
				}
			}
			var vendorUser = Lib.AP.VendorPortal.CreateNewVendorFromTemplate(parameters);
			if (vendorUser)
			{
				Variable.SetValueAsString("vendorShortLogin", Lib.AP.VendorPortal.GetShortLoginFromUser(vendorUser));
				Variable.SetValueAsString("vendorLogin", vendorUser.GetVars().GetValue_String("login", 0));
				return vendorUser;
			}
		}
		return null;
	}
};
var NotificationHelper = {
	SendWelcomeEmail: function (user)
	{
		user.UpdateWelcomeInfoDate("EMAIL");
		var templateName = "AP-Vendor_WelcomeEmail_XX.htm";
		var customTags = {
			login: user.GetValue("login").split("$")[1],
			passwordUrl: user.GeneratePasswordURL(),
			UrlOptions: user.GetPortalURL()
		};
		NotificationHelper.SendEmail(templateName,
		{
			customTags: customTags
		});
	},
	SendEmail: function (template, options)
	{
		var customTags = {};
		if (options && options.customTags)
		{
			customTags = options.customTags;
		}
		var userID = Data.GetValue("RegistrationType__") === "update" ? Data.GetValue("OwnerId") : Variable.GetValueAsString("creatorOwnerId");
		var userAdmin = Users.GetUserAsProcessAdmin(userID);
		if (userAdmin)
		{
			customTags.AccountLogo__ = userAdmin.GetLogoPath();
		}
		else
		{
			Log.Error("Cannot retrieve userAdmin from external variable with value '" + userID + "'");
		}
		customTags.VendorRegistrationUrl__ = null;
		var vendorID = Variable.GetValueAsString("vendorLogin") || Variable.GetValueAsString("guestUserLogin");
		var userVendor = Users.GetUserAsProcessAdmin(vendorID);
		if (userVendor)
		{
			if (options && options.processLink)
			{
				customTags.VendorRegistrationUrl__ = userVendor.GetProcessURL(Data.GetValue("Ruidex"), true);
			}
		}
		else
		{
			Log.Error("Cannot retrieve userVendor from external variable with value '" + vendorID + "'");
		}
		var destEmail = userVendor ? userVendor.GetVars().GetValue_String("EmailAddress", 0) : Variable.GetValueAsString("vendorShortLogin");
		var email = Sys.EmailNotification.CreateEmailWithUser(userVendor, destEmail, null, template, customTags, true);
		if (email)
		{
			if (options && options.addRequesterAsBBC && Data.GetValue("RegistrationType__") === "registration")
			{
				var userRequester = Users.GetUser(userID);
				if (userRequester)
				{
					var requesterEmailAdress = userRequester.GetValue("EmailAddress");
					if (requesterEmailAdress)
					{
						Sys.EmailNotification.AddBCC(email, requesterEmailAdress);
					}
					else
					{
						Log.Error("Cannot retrieve requesterEmailAdress from userRequester");
					}
				}
				else
				{
					Log.Error("Cannot retrieve userRequester from external variable creatorOwnerId with value '" + userID + "'");
				}
			}
			Sys.EmailNotification.SendEmail(email);
		}
		else
		{
			Log.Error("Cannot send email to '" + destEmail + "'");
		}
	}
};
var buildRequester = function (callback)
{
	var ownerLogin = Variable.GetValueAsString("creatorOwnerEmail");
	Log.Info("Requester: ", ownerLogin);
	Sys.OnDemand.Users.GetUsersFromLogins([ownerLogin], ["displayname", "emailaddress", "company"], function (users)
	{
		var requesters = Sys.Helpers.Array.Map(users, function (user)
		{
			return {
				contributorId: workflowController.CreateUniqueContributorId("requester_" + user.login),
				role: WorkflowParameters.roles.requester.contributorKey,
				login: user.login,
				email: user.exists ? user.emailaddress : user.login,
				name: user.exists ? user.displayname : user.login,
				action: "toSubmit"
			};
		});
		callback(requesters);
	});
	return true;
};
var buildVendor = function (callback)
{
	var vendorLogin = Variable.GetValueAsString("guestUserLogin") || Variable.GetValueAsString("vendorLogin");
	Sys.OnDemand.Users.GetUsersFromLogins([vendorLogin], ["displayname", "emailaddress", "company"], function (users)
	{
		var vendors = Sys.Helpers.Array.Map(users, function (user)
		{
			return {
				contributorId: workflowController.CreateUniqueContributorId("vendor_" + user.login),
				role: WorkflowParameters.roles.vendor.contributorKey,
				login: user.login,
				email: user.exists ? user.emailaddress : user.login,
				name: user.company ? user.company : user.emailaddress,
				action: "toSubmit"
			};
		});
		callback(vendors);
	});
	return true;
};
var buildApprovers = function (callback)
{
	var modifiedFields = ModifiedFieldsHandling.GetFlattedModifiedFields();
	var options = {
		fields:
		{
			"values":
			{
				"WorkflowType__": "vendorRegistration",
				"modifiedFields": modifiedFields
			}
		},
		success: function (approversList)
		{
			var approversLogin = Sys.Helpers.Array.Map(approversList, function (approver)
			{
				return approver.login;
			});
			Sys.OnDemand.Users.GetUsersFromLogins(approversLogin, ["displayname", "emailaddress", "company"], function (users)
			{
				var approvers = Sys.Helpers.Array.Map(users, function (user)
				{
					return {
						contributorId: workflowController.CreateUniqueContributorId("approver_" + user.login),
						role: WorkflowParameters.roles.approver.contributorKey,
						login: user.login,
						email: user.exists ? user.emailaddress : user.login,
						name: user.exists ? user.displayname : user.login,
						action: "toApprove"
					};
				});
				callback(approvers);
			});
		},
		error: function (errorMessage)
		{
			Log.Error("Cannot find any workflow rule for vendor registration: " + errorMessage);
			buildRequester(callback);
		}
	};
	Lib.P2P.GetApprovalWorkflow(options);
	return true;
};
var buildOFACChecker = function (callback)
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("EnableOFACVerification", "0") === "1")
	{
		callback([
		{
			contributorId: workflowController.CreateUniqueContributorId("OFACChecker"),
			role: WorkflowParameters.roles.OFACChecker.contributorKey,
			login: "",
			email: "",
			name: Language.Translate("_OFACName"),
			action: "OFACChecked"
		}]);
		return true;
	}
	return false;
};
var buildSisIdChecker = function (callback)
{
	if (DoNeedToCheckVendorOnSisId())
	{
		callback([
		{
			contributorId: workflowController.CreateUniqueContributorId("SisIdChecker"),
			role: WorkflowParameters.roles.SisIdChecker.contributorKey,
			login: "",
			email: "",
			name: Language.Translate("_SisIdName"),
			action: "SisIdChecked"
		}]);
		return true;
	}
	return false;
};
var buildLegalReviewers = function (callback)
{
	if (Sys.Parameters.GetInstance("AP").GetParameter("EnableOFACVerification", "0") === "1")
	{
		// Trigger workflow rule
		var OFACResults = Lib.VendorRegistration.CheckOFAC.GetLastOFACResults();
		var OFACMatch = false;
		var OFACMaxScore = 0;
		var OFACEntityType = false;
		var OFACIndividualType = false;
		if (OFACResults.length > 0)
		{
			OFACMatch = true;
			OFACResults.sort(function (a, b)
			{
				return b.score - a.score;
			});
			OFACMaxScore = OFACResults[0].score;
			OFACEntityType = OFACResults[0].type === Sys.OFAC.Common.OFACType.Entity;
			OFACIndividualType = OFACResults[0].type === Sys.OFAC.Common.OFACType.Individual;
		}
		var options = {
			fields:
			{
				"values":
				{
					"WorkflowType__": "OFACVerification",
					"OFACMatchFound__": OFACMatch,
					"OFACMaxScoreFound__": OFACMaxScore,
					"OFACEntityType__": OFACEntityType,
					"OFACIndividualType__": OFACIndividualType
				}
			},
			success: function (reviewerList)
			{
				var reviewersLogin = Sys.Helpers.Array.Map(reviewerList, function (reviewer)
				{
					return reviewer.login;
				});
				Sys.OnDemand.Users.GetUsersFromLogins(reviewersLogin, ["displayname", "emailaddress", "company"], function (users)
				{
					var contributors = Sys.Helpers.Array.Map(users, function (user)
					{
						return {
							contributorId: workflowController.CreateUniqueContributorId("LegalReviewer"),
							role: WorkflowParameters.roles.legalReviewer.contributorKey,
							login: user.login,
							email: user.exists ? user.emailaddress : user.login,
							name: user.exists ? user.displayname : user.login,
							action: "toReview"
						};
					});
					callback(contributors);
				});
			},
			error: function (errorMessage)
			{
				Log.Warn("Ofac verification workflow: " + errorMessage);
				callback([]);
			}
		};
		Lib.P2P.GetApprovalWorkflow(options);
		return true;
	}
	return false;
};
var FormToMasterData = {
	SaveVendorUser: function (vendor, extraData)
	{
		if (!extraData)
		{
			extraData = {};
		}
		extraData.City = Data.GetValue("City__");
		extraData.Company = Data.GetValue("Company__");
		extraData.Country = Data.GetValue("Country__");
		extraData.Faxnumber = Data.GetValue("Fax_Number__");
		extraData.Mailstate = Data.GetValue("Mail_State__");
		extraData.Mailsub = Data.GetValue("MailSub__");
		extraData.Phonenumber = Data.GetValue("Phone_Number__");
		extraData.Street = Data.GetValue("Street__");
		extraData.Pobox = Data.GetValue("POBox__");
		extraData.Zipcode = Data.GetValue("Zip_Code__");
		vendor.SetValues(extraData);
	},
	SerializeTables: function (user)
	{
		var parameters = {
			Company__: Data.GetValue("Company__"),
			MailSub__: Data.GetValue("MailSub__"),
			Street__: Data.GetValue("Street__"),
			POBox__: Data.GetValue("POBox__"),
			Zip_Code__: Data.GetValue("Zip_Code__"),
			City__: Data.GetValue("City__"),
			Mail_State__: Data.GetValue("Mail_State__"),
			Country__: Data.GetValue("Country__"),
			Phone_Number__: Data.GetValue("Phone_Number__"),
			Fax_Number__: Data.GetValue("Fax_Number__"),
			TaxID__: Data.GetValue("TaxID__"),
			VendorRegistrationDUNSNumber__: Data.GetValue("VendorRegistrationDUNSNumber__"),
			CompanyCode__: Data.GetValue("CompanyCode__"),
			VendorNumber__: Data.GetValue("VendorNumber__"),
			Email__: Data.GetTable("CompanyOfficersTable__").GetItem(0).GetValue("Email__"),
			Configuration__: "Default"
		};
		// Synchronize vendor links table
		if (user)
		{
			Lib.AP.VendorPortal.CreateOrUpdateVendorLink(user, parameters, true);
		}
		Lib.AP.VendorPortal.CreateOrUpdateP2PVendorRecord(parameters);
		// Loop through Form table CompanyOfficersTable__ and serialize all content in CT 'AP - Vendors officers__'
		var vendorRegistrationOfficersTable = {
			groupKeys:
			{
				CompanyCode__: Data.GetValue("CompanyCode__"),
				VendorNumber__: Data.GetValue("VendorNumber__")
			},
			dataIndexColumn: "TableIndex__",
			data:
			{}
		};
		var officersTable = Data.GetTable("CompanyOfficersTable__");
		for (var i = 0; i < officersTable.GetItemCount(); i++)
		{
			var item = officersTable.GetItem(i);
			vendorRegistrationOfficersTable.data[i + 1] = {
				Role__: item.GetValue("Role__"),
				FirstName__: item.GetValue("FirstName__"),
				LastName__: item.GetValue("LastName__"),
				Email__: item.GetValue("Email__")
			};
		}
		Sys.Helpers.Database.SynchronizeCustomTable("AP - Vendors officers__", vendorRegistrationOfficersTable);
		// Loop through Form table CompanyBankAccountsTable__ and serialize all content in CT 'AP - Bank details__'
		var vendorRegistrationBankDetailsTable = {
			groupKeys:
			{
				CompanyCode__: Data.GetValue("CompanyCode__"),
				VendorNumber__: Data.GetValue("VendorNumber__")
			},
			dataIndexColumn: "IBAN__",
			data:
			{}
		};
		var bankAccountTable = Data.GetTable("CompanyBankAccountsTable__");
		for (var i = 0; i < bankAccountTable.GetItemCount(); i++)
		{
			var item = bankAccountTable.GetItem(i);
			var iban = item.GetValue("BankIBAN__");
			vendorRegistrationBankDetailsTable.data[iban] = {
				BankCountry__: item.GetValue("BankCountry__"),
				BankKey__: item.GetValue("BankKey__"),
				BankAccount__: item.GetValue("BankAccountNumber__"),
				AccountHolder__: item.GetValue("BankAccountHolder__"),
				ControlKey__: item.GetValue("ControlKey__"),
				SWIFT_BICCode__: item.GetValue("SWIFT_BICCode__"),
				Currency__: item.GetValue("Currency__"),
				RoutingCode__: item.GetValue("RoutingCode__")
			};
		}
		Sys.Helpers.Database.SynchronizeCustomTable("AP - Bank details__", vendorRegistrationBankDetailsTable);
		// serialize some data in CT 'AP - Vendor company extended properties__'
		var companyProperties = Lib.AP.VendorPortal.GetOrCreateCompanyExtendedProperties(Data.GetValue("VendorNumber__"), Data.GetValue("CompanyCode__"));
		if (companyProperties)
		{
			var companyVars = companyProperties.GetVars();
			companyVars.AddValue_String("Website__", Data.GetValue("Website__"), true);
			companyVars.AddValue_String("NumberOfEmployees__", Data.GetValue("NumberOfEmployees__"), true);
			companyVars.AddValue_String("YearsInBusiness__", Data.GetValue("YearsInBusiness__"), true);
			companyVars.AddValue_String("PaymentMethod__", Data.GetValue("PaymentMethod__"), true);
			companyVars.AddValue_String("PaymentTerms__", Data.GetValue("PaymentTerms__"), true);
			companyVars.AddValue_String("CompanyStructure__", Data.GetValue("CompanyStructure__"), true);
			companyVars.AddValue_String("TaxStatus__", Data.GetValue("TaxStatus__"), true);
			companyProperties.Commit();
		}
	}
};
var CreateRelatedDocuments = function ()
{
	var SenderFormProcessName = "DD - SenderForm";
	if (Process.GetProcessID(SenderFormProcessName))
	{
		var relatedDocumentsStr = Variable.GetValueAsString("DocumentsSummaryJSON");
		if (!relatedDocumentsStr)
		{
			return;
		}
		try
		{
			var relatedDocumentsJson = JSON.parse(relatedDocumentsStr);
			var documentsArray = relatedDocumentsJson.documents;
			if (!documentsArray)
			{
				return;
			}
			for (var _i = 0, documentsArray_1 = documentsArray; _i < documentsArray_1.length; _i++)
			{
				var currentItem = documentsArray_1[_i];
				var attach = Attach.GetAttach(currentItem.attachmentIndex);
				if (!attach)
				{
					Log.Error("An error occured during creation of related documents : failed to find attachment");
					break;
				}
				var senderFormProcess = Process.CreateProcessInstance(SenderFormProcessName, true);
				var transportAttach = senderFormProcess.AddAttachEx(attach);
				var transportAttachVars = transportAttach.GetVars();
				transportAttachVars.AddValue_String("AttachToProcess", "1", true);
				// To avoid wrapping, configuration name is the same that document type to work with SDA
				var transportExternalVars = senderFormProcess.GetExternalVars();
				transportExternalVars.AddValue_String("Configuration", currentItem.type, true);
				// Company code and vendor number are saved in additional field 1 and 2 in Sender From
				var transportVars = senderFormProcess.GetUninheritedVars();
				transportVars.AddValue_String("CF_1711725718e_Company_code__", Data.GetValue("CompanyCode__"), true);
				transportVars.AddValue_String("CF_17117258411_Vendor_number__", Data.GetValue("VendorNumber__"), true);
				transportVars.AddValue_String("Subject", Attach.GetName(currentItem.attachmentIndex), true);
				var vendor = Users.GetUserAsProcessAdmin(Variable.GetValueAsString("vendorLogin") || Variable.GetValueAsString("guestUserLogin"));
				transportVars.AddValue_String("NotificationRecipient__", vendor.GetValue("Login"), true);
				if (currentItem.endOfValidity != null)
				{
					transportVars.AddValue_Date("EndOfValidity__", Sys.Helpers.Date.ISO8601StringToDate(currentItem.endOfValidity), true);
				}
				// Set right to all user of workflow
				for (var rightIndex = 0; rightIndex < workflowController.GetNbContributors(); rightIndex++)
				{
					var contributor = workflowController.GetContributorAt(rightIndex);
					senderFormProcess.AddRight(contributor.login, "all");
				}
				senderFormProcess.Process();
			}
		}
		catch (e)
		{
			Log.Error("An exception occured during creation of related documents : " + e);
		}
	}
	else
	{
		Log.Info("SDA isn't activated - related documents will not be created");
	}
};
// Workflow controller parameters
var WorkflowParameters = {
	roles:
	{
		requester:
		{
			OnBuild: buildRequester,
			contributorKey: "_Requester"
		},
		vendor:
		{
			OnBuild: buildVendor,
			contributorKey: "_Vendor"
		},
		approver:
		{
			// Obtains a list of approvers from the workflow rule.
			OnBuild: buildApprovers,
			contributorKey: "_Approver"
		},
		OFACChecker:
		{
			OnBuild: buildOFACChecker,
			contributorKey: "_OFACChecker"
		},
		SisIdChecker:
		{
			OnBuild: buildSisIdChecker,
			contributorKey: "_SisIdChecker"
		},
		legalReviewer:
		{
			OnBuild: buildLegalReviewers,
			contributorKey: "_LegalReviewer"
		}
	},
	// In the validation script(server side), WorkflowParameters defines what must be done for each action.
	actions:
	{
		requestVendorInformations:
		{
			OnDone: function ()
			{
				GiveReadRightToCurrentContributor();
				workflowController.NextContributor(
				{
					action: "submitted",
					date: new Date()
				});
				NotificationHelper.SendEmail("AP-Vendor_RegistrationInvitation.htm",
				{
					processLink: true
				});
				Forward();
			}
		},
		toSubmit:
		{
			OnDone: function ()
			{
				// Serialize the modified fields
				if (Data.GetValue("RegistrationType__") === "update")
				{
					ModifiedFieldsHandling.SerializeModifiedData();
				}
				else
				{
					NotificationHelper.SendEmail("AP-Vendor_RegistrationSubmit.htm");
				}
				Data.SetValue("Comment__", "");
				Data.SetValue("ProcessStatus__", "ToValidate");
				GiveReadRightToCurrentContributor();
				// Main account rights required to compute workflow
				ForwardToMainAccountAndCallAction("getWarningsToDisplayToApprover", true);
			}
		},
		submitted:
		{},
		OFACChecked:
		{},
		SisIdChecked:
		{},
		toReview:
		{
			OnDone: function ()
			{
				var nextContributor = workflowController.GetContributorAt(workflowController.GetContributorIndex() + 1);
				if (nextContributor.role === "_Approver")
				{
					Lib.VendorRegistration.CheckOFAC.ResetOFACResults();
				}
				GiveReadRightToCurrentContributor();
				workflowController.NextContributor(
				{
					action: "approved",
					date: new Date(),
					comment: Data.GetValue("Comment__")
				});
				Forward();
				Data.SetValue("Comment__", "");
			}
		},
		toApprove:
		{
			OnDone: function (index)
			{
				var nbSteps = workflowController.GetNbContributors();
				GiveReadRightToCurrentContributor();
				if (index < nbSteps - 1)
				{
					// Updates the current contributor attributes (action, date) then sets the next contributor.
					workflowController.NextContributor(
					{
						action: "approved",
						date: new Date()
					});
					Forward();
				}
				else
				{
					// Only replace the guest vendor by newly created SalesAdmin vendor for new registrations
					var user = Users.GetUserAsProcessAdmin(Variable.GetValueAsString("guestUserLogin") || Variable.GetValueAsString("vendorLogin"));
					if (Data.GetValue("RegistrationType__") !== "update")
					{
						NotificationHelper.SendEmail("AP-Vendor_RegistrationApprove.htm");
						user.SetValues(
						{
							AccountLocked: "1",
							LoginType: 0x01000000 // DeniedInDocMgr
						});
					}
					else
					{
						FormToMasterData.SaveVendorUser(user);
					}
					Data.SetValue("ProcessStatus__", "Approved");
					// Serialize Form associated tables (Vendor links, Vendor, Officers and Bank details) in CustomTables
					FormToMasterData.SerializeTables(user);
					if (Data.GetValue("RegistrationType__") !== "update")
					{
						var salesAdminUser = VendorHelper.CreateSalesAdminVendor();
						if (!salesAdminUser)
						{
							Log.Error("Cannot create Sales Admin vendor account");
							return;
						}
						FormToMasterData.SerializeTables(salesAdminUser);
						Process.AddRight(salesAdminUser.GetValue("Login"), "read");
						NotificationHelper.SendWelcomeEmail(salesAdminUser);
					}
					// Create child for SDA
					CreateRelatedDocuments();
					// Does not forward to anyone, and lets the document be approved and go to the success state.
					workflowController.EndWorkflow(
					{
						action: "approved",
						date: new Date()
					});
				}
			}
		},
		approved:
		{},
		backToPrevious:
		{
			OnDone: function (index)
			{
				GiveReadRightToCurrentContributor();
				Data.SetValue("ProcessStatus__", "ToValidate");
				workflowController.BackTo(index - 1,
				{
					action: "backToPrevious",
					date: new Date()
				});
				Forward();
			}
		},
		backToVendor:
		{
			OnDone: function ()
			{
				GiveReadRightToCurrentContributor();
				NotificationHelper.SendEmail("AP-Vendor_RegistrationBackToVendor.htm",
				{
					processLink: true,
					customTags:
					{
						comment: Data.GetValue("Comment__")
					}
				});
				Data.SetValue("ProcessStatus__", "PendingVendorInfo");
				workflowController.BackTo(workflowController.GetRoleSequenceIndex(WorkflowParameters.roles.vendor.contributorKey),
				{
					action: "backToVendor",
					date: new Date(),
					comment: Data.GetValue("Comment__")
				});
				Forward();
			}
		},
		recheckIban:
		{
			OnDone: function (step)
			{
				// Action is not a real approval action
				Process.PreventApproval();
				//Get user language for sis id call
				var currentUser = Users.GetUserAsProcessAdmin(Data.GetValue("OwnerId"));
				var userLanguage = currentUser ? currentUser.GetValue("language") : "EN";
				CheckVendorOnSisId(userLanguage);
				var wkfComment = GetSisIdResultMessage();
				// Add Workflow line
				var currentContributor = workflowController.GetContributorAt(step);
				currentContributor.action = "SisIdChecked";
				currentContributor.role = WorkflowParameters.roles.SisIdChecker.contributorKey;
				currentContributor.date = new Date();
				currentContributor.comment = wkfComment;
				workflowController.BackTo(step, currentContributor);
			}
		},
		reject:
		{
			OnDone: function ()
			{
				// Terminates the workflow and sets the message in the rejected state.
				GiveReadRightToCurrentContributor();
				NotificationHelper.SendEmail("AP-Vendor_RegistrationRejected.htm",
				{
					processLink: true,
					addRequesterAsBBC: true,
					customTags:
					{
						comment: Data.GetValue("Comment__")
					}
				});
				workflowController.EndWorkflow(
				{
					action: "rejected",
					date: new Date(),
					comment: Data.GetValue("Comment__")
				});
				Data.SetValue("State", 400);
				Data.SetValue("ProcessStatus__", "Rejected");
				Process.LeaveForm();
			}
		}
	},
	mappingTable:
	{
		// Indicates how to fill the table control on the form.
		// Name of the table control used to display the workflow on the form.
		tableName: "Workflow__",
		columns:
		{
			// Indicates that the property 'name' must be displayed in the column User__.
			// A property can be either a field of the role callback (contributorId, role, login, name or action) or
			// a field that you set yourself later (comment).
			User__:
			{
				data: "name"
			},
			Role__:
			{
				data: "role"
			},
			Date__:
			{
				data: "date"
			},
			Action__:
			{
				data: "action"
			},
			ApproverComment__:
			{
				data: "comment"
			}
		}
	},
	callbacks:
	{
		OnError: function (msg)
		{
			Log.Error(msg);
		}
	}
};
// Creates and initializes the workflow.
var workflowController = Sys.WorkflowController.Create(Data, Variable, Language);
workflowController.Define(WorkflowParameters);

function submitAction()
{
	if (!Data.IsFormInError() && CheckIbanFormat() && CheckVATFormat())
	{
		var isProcessValid = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorRegistration_Server.IsProcessValid");
		if (isProcessValid === false)
		{
			Process.PreventApproval();
		}
		else
		{
			var idx = workflowController.GetContributorIndex();
			var currentContributor = workflowController.GetContributorAt(idx);
			workflowController.DoAction(currentContributor.action);
		}
	}
	else
	{
		Process.PreventApproval();
	}
}

function backToPreviousAction()
{
	workflowController.DoAction("backToPrevious");
}

function backToVendorAction()
{
	workflowController.DoAction("backToVendor");
}

function rejectAction()
{
	workflowController.DoAction("reject");
}

function rebuildWorkflow()
{
	Log.Info("Rebuilding workflow to take vendor infos and checks into account...");
	workflowController.Rebuild();
	Forward();
}

function getWarningsToDisplayToApproverAction()
{
	//get user language for sis id call
	var currentUser = Users.GetUserAsProcessAdmin(Data.GetValue("OwnerId"));
	var userLanguage = currentUser ? currentUser.GetValue("language") : "EN";
	// initialize workflow
	rebuildWorkflow();
	if (Lib.VendorRegistration.CheckDuplicateVendor.ShouldCheckDuplicates())
	{
		Lib.VendorRegistration.CheckDuplicateVendor.CheckAllDuplicates(Data.GetValue("MsnEx"));
	}
	// Vendor submitted the registration
	workflowController.NextContributor(
	{
		action: "submitted",
		date: new Date()
	});
	// SIS-ID Check - SisIdChecker sequence step
	if (DoNeedToCheckVendorOnSisId())
	{
		CheckVendorOnSisId(userLanguage);
		var wkfComment = GetSisIdResultMessage();
		// Automatic check, move to next contributor
		workflowController.NextContributor(
		{
			date: new Date(),
			comment: wkfComment
		});
	}
	// OFAC Check
	if (Sys.Parameters.GetInstance("AP").GetParameter("EnableOFACVerification", "0") === "1")
	{
		Lib.VendorRegistration.CheckOFAC.CheckAllOFACLists();
		var OFACResults = Lib.VendorRegistration.CheckOFAC.GetLastOFACResults();
		var wkfComment = Language.Translate("_OFACNoMatch");
		if (OFACResults.length > 0)
		{
			wkfComment = Lib.VendorRegistration.CheckOFAC.FormatOFACDisplay(OFACResults, false);
		}
		// Automatic check, move to next contributor
		workflowController.NextContributor(
		{
			date: new Date(),
			comment: wkfComment
		});
	}
	// Rebuild workflow for legalReviewer sequence (requires OFAC checks results)
	rebuildWorkflow();
}

function recheckIbanAction()
{
	workflowController.DoAction("recheckIban");
}

function initRegistrationUpdateAction()
{
	// Defines the role sequence.
	GetVendorLinksInfos();
	// Check if there's another registration update pending
	if (AnotherRegistrationUpdateIsPending())
	{
		Variable.SetValueAsString("AlreadyPendingRegistrationUpdate", "true");
		return;
	}
	if (companyCode && vendorNumber)
	{
		Data.SetValue("CompanyCode__", companyCode);
		Data.SetValue("VendorNumber__", vendorNumber);
		FillInterfaceFromMasterData(GetMasterData());
		// Store master data, for later use
		ModifiedFieldsHandling.SerializeFormToProcessVariable();
	}
	workflowController.SetRolesSequence(["vendor", "SisIdChecker", "OFACChecker", "legalReviewer", "approver"]);
	workflowController.AllowRebuild(true);
	// Forward back to vendor
	Process.Forward(Variable.GetValueAsString("vendorLogin"));
}

function defaultAction()
{
	if (!Variable.GetValueAsString("guestUserLogin"))
	{
		// Registration update
		Data.SetValue("RegistrationType__", "update");
		var currentVendor = Users.GetUser(Data.GetValue("OwnerId"));
		Variable.SetValueAsString("vendorShortLogin", Lib.AP.VendorPortal.GetShortLoginFromUser(currentVendor));
		Variable.SetValueAsString("vendorLogin", currentVendor.GetVars().GetValue_String("login", 0));
		ForwardToMainAccountAndCallAction("InitRegistrationUpdate");
	}
	else
	{
		// New registration
		Data.SetValue("RegistrationType__", "registration");
		// Defines the role sequence.
		workflowController.SetRolesSequence(["requester", "vendor", "SisIdChecker", "OFACChecker", "legalReviewer", "approver"]);
		workflowController.AllowRebuild(true);
		workflowController.DoAction("requestVendorInformations");
	}
}

function run()
{
	Lib.AP.InitArchiveDuration();
	var actionName = Data.GetActionName();
	Log.Info("actionName:", actionName);
	var actionMap = {
		"Submit": submitAction,
		"BackToPrevious": backToPreviousAction,
		"BackToVendor": backToVendorAction,
		"Reject": rejectAction,
		"getWarningsToDisplayToApprover": getWarningsToDisplayToApproverAction,
		"recheckIban": recheckIbanAction,
		"InitRegistrationUpdate": initRegistrationUpdateAction
	};
	// Retrieve data for do a duplicate check on pending vendor registrations
	Lib.VendorRegistration.CheckDuplicateVendor.SetPendingRegistrationToCheck();
	// Defines what must be done when depending on requested action
	if (typeof actionMap[actionName] === "function")
	{
		actionMap[actionName]();
	}
	else
	{
		// new submission
		defaultAction();
	}
}
run();