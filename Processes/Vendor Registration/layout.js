{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1200,
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"hideTitle": true,
						"maxwidth": 1200
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1200
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents"
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 8,
															"data": []
														}
													},
													"stamp": 9,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 10,
															"data": []
														}
													},
													"stamp": 11,
													"data": []
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 12,
													"*": {
														"VendorRegistrationPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_VendorRegistrationPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 13,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Title__": "LabelTitle__",
																			"LabelTitle__": "Title__"
																		},
																		"version": 0
																	},
																	"stamp": 14,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Title__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelTitle__": {
																						"line": 1,
																						"column": 1
																					},
																					"RegistrationType__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 15,
																			"*": {
																				"LabelTitle__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 16
																				},
																				"Title__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0,
																						"htmlContent": "<input id=\"displayTitle\" type=\"hidden\" onclick=\"onStatusLoad(event)\"></input>\n<div class=\"Text Title header\">\n<div id=\"title\" class=\"title text-normal text-size-XL text-color-default text-backgroundcolor-default text-disabled\"></div>\n</div>\n<div class=\"topright\">\n<div id=\"status\" class=\"image\"></div>\n</div>\n<script>\nwindow.postMessage({eventName: \"onTitleLoad\", control: \"Title__\"},\ndocument.location.origin); \nfunction onStatusLoad(args) \n{\ndocument.getElementById('title').innerHTML = args.title;\nvar statusDiv = document.getElementById('status');\nif(args && args.status) \n{\n statusDiv.innerHTML = args.status.label;\nstatusDiv.style.backgroundImage = args.status.imageUrl;\n}\n}\n</script>\n",
																						"css": "@ .header {\n}@ \n.Title{\n   border-bottom: none;\n}@ \n\n.title {\n    width: 75%;\n    text-overflow: ellipsis;\n    overflow: hidden;\n}@ \n\n.topright {\n                position: absolute;\n                top: 12px;\n                right: 0px;\n}@ \n\n.image {\n    height: 18px;\n    background-repeat: no-repeat;\n    background-position-x: left;\n    text-transform: uppercase;\n    font-weight: bold;\n    padding-left: 25px;\n    padding-right: 0px;\n    padding-bottom: 24px;\n    text-align: right;\n    font-size: 12px;\n}@ \n.small {\n    background-position-y: 1px;\n    text-align: left;\n    background-size: 14px;\n    font-weight: normal;\n    text-transform: none;\n    white-space: normal;\n    margin-top: 7px;\n    padding-bottom: 7px;\n}@ \n.notvisible {\n    display: none;\n}@ "
																					},
																					"stamp": 17
																				},
																				"RegistrationType__": {
																					"type": "ComboBox",
																					"data": [
																						"RegistrationType__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_registration",
																							"1": "_update"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "registration",
																							"1": "update"
																						},
																						"label": "_RegistrationType",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true
																					},
																					"stamp": 18
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 19,
													"*": {
														"Steps": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "none",
																"labelsAlignment": "right",
																"label": "_Steps",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "center",
																"version": 0
															},
															"stamp": 20,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 21,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Company_Step__": {
																						"line": 1,
																						"column": 1
																					},
																					"spacer__": {
																						"line": 2,
																						"column": 1
																					},
																					"Company_Officers__": {
																						"line": 3,
																						"column": 1
																					},
																					"spacer2__": {
																						"line": 4,
																						"column": 1
																					},
																					"Banks__": {
																						"line": 5,
																						"column": 1
																					},
																					"spacer3__": {
																						"line": 5,
																						"column": 1
																					},
																					"spacer4__": {
																						"line": 6,
																						"column": 1
																					},
																					"Documents_Step__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 22,
																			"*": {
																				"Company_Step__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Company_Step",
																						"label": "",
																						"textPosition": "text-above",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-circle fa-2",
																						"style": 5,
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"width": "100%"
																					},
																					"stamp": 23
																				},
																				"spacer__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "___",
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"Company_Officers__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Company_Officers",
																						"label": "",
																						"textPosition": "text-above",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-circle-o fa-2",
																						"style": 5,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"spacer2__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "___",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"Banks__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Banks",
																						"label": "",
																						"textPosition": "text-above",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-circle-o fa-2",
																						"style": 5,
																						"width": "100%",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"spacer4__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XL",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "___",
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"Documents_Step__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Documents_Step",
																						"label": "",
																						"version": 0,
																						"textPosition": "text-above",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "fa fa-circle-o fa-2",
																						"style": 5,
																						"action": "none",
																						"url": "",
																						"width": "100%"
																					},
																					"stamp": 29
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 279,
													"*": {
														"CommentPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "none",
																"labelsAlignment": "right",
																"label": "CommentPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"stamp": 241,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Label_HTML": "CommentContent__",
																			"CommentContent__": "Label_HTML"
																		}
																	},
																	"stamp": 242,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CommentContent__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"stamp": 243,
																			"*": {
																				"CommentContent__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "CommentContent__",
																						"version": 0,
																						"css": "@ {\n    border: solid #ff8700;\n    border-width: 0 0 0 5px;\n    border-radius: 5px;\n    padding-left: 15px;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    margin-left: -10px;\n    background-color: #fff3e5;\n}@ ",
																						"width": "100%"
																					},
																					"stamp": 289
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 30,
													"*": {
														"CompanyProfilePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Company Profile",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "MultipleColumnPanel",
																"version": 0
															},
															"stamp": 31,
															"*": {
																"FieldsManager1": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Company__": "LabelCompany__",
																			"LabelCompany__": "Company__",
																			"MailSub__": "LabelMailSub__",
																			"LabelMailSub__": "MailSub__",
																			"Street__": "LabelStreet__",
																			"LabelStreet__": "Street__",
																			"POBox__": "LabelPOBox__",
																			"LabelPOBox__": "POBox__",
																			"Zip_Code__": "LabelZip_Code__",
																			"LabelZip_Code__": "Zip_Code__",
																			"City__": "LabelCity__",
																			"LabelCity__": "City__",
																			"Mail_State__": "LabelMail_State__",
																			"LabelMail_State__": "Mail_State__",
																			"Country__": "LabelCountry__",
																			"LabelCountry__": "Country__",
																			"Phone_Number__": "LabelPhone_Number__",
																			"LabelPhone_Number__": "Phone_Number__",
																			"Fax_Number__": "LabelFax_Number__",
																			"LabelFax_Number__": "Fax_Number__",
																			"Website__": "LabelWebsite__",
																			"LabelWebsite__": "Website__",
																			"ComboState__": "LabelComboState__",
																			"LabelComboState__": "ComboState__"
																		},
																		"version": 0
																	},
																	"stamp": 32,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Company__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompany__": {
																						"line": 1,
																						"column": 1
																					},
																					"MailSub__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelMailSub__": {
																						"line": 2,
																						"column": 1
																					},
																					"Street__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelStreet__": {
																						"line": 3,
																						"column": 1
																					},
																					"POBox__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelPOBox__": {
																						"line": 4,
																						"column": 1
																					},
																					"Zip_Code__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelZip_Code__": {
																						"line": 5,
																						"column": 1
																					},
																					"City__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelCity__": {
																						"line": 6,
																						"column": 1
																					},
																					"Mail_State__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelMail_State__": {
																						"line": 7,
																						"column": 1
																					},
																					"Country__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelCountry__": {
																						"line": 9,
																						"column": 1
																					},
																					"Phone_Number__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelPhone_Number__": {
																						"line": 10,
																						"column": 1
																					},
																					"Fax_Number__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelFax_Number__": {
																						"line": 11,
																						"column": 1
																					},
																					"Website__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelWebsite__": {
																						"line": 12,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 13,
																						"column": 1
																					},
																					"ComboState__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelComboState__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 13,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 33,
																			"*": {
																				"LabelCompany__": {
																					"type": "Label",
																					"data": [
																						"Company__"
																					],
																					"options": {
																						"label": "_Company",
																						"version": 0
																					},
																					"stamp": 34
																				},
																				"Company__": {
																					"type": "ShortText",
																					"data": [
																						"Company__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Company",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 60
																					},
																					"stamp": 35
																				},
																				"LabelMailSub__": {
																					"type": "Label",
																					"data": [
																						"MailSub__"
																					],
																					"options": {
																						"label": "_MailSub",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"MailSub__": {
																					"type": "ShortText",
																					"data": [
																						"MailSub__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_MailSub",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 50
																					},
																					"stamp": 37
																				},
																				"LabelStreet__": {
																					"type": "Label",
																					"data": [
																						"Street__"
																					],
																					"options": {
																						"label": "_Street",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"Street__": {
																					"type": "ShortText",
																					"data": [
																						"Street__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Street",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 60
																					},
																					"stamp": 39
																				},
																				"LabelPOBox__": {
																					"type": "Label",
																					"data": [
																						"POBox__"
																					],
																					"options": {
																						"label": "_POBox",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"POBox__": {
																					"type": "ShortText",
																					"data": [
																						"POBox__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_POBox",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 20
																					},
																					"stamp": 41
																				},
																				"LabelZip_Code__": {
																					"type": "Label",
																					"data": [
																						"Zip_Code__"
																					],
																					"options": {
																						"label": "_Zip code",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"Zip_Code__": {
																					"type": "ShortText",
																					"data": [
																						"Zip_Code__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Zip code",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 10
																					},
																					"stamp": 43
																				},
																				"LabelCity__": {
																					"type": "Label",
																					"data": [
																						"City__"
																					],
																					"options": {
																						"label": "_City",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"City__": {
																					"type": "ShortText",
																					"data": [
																						"City__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_City",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 50
																					},
																					"stamp": 45
																				},
																				"LabelMail_State__": {
																					"type": "Label",
																					"data": [
																						"Mail_State__"
																					],
																					"options": {
																						"label": "_MailState",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"Mail_State__": {
																					"type": "ShortText",
																					"data": [
																						"Mail_State__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_MailState",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 50
																					},
																					"stamp": 47
																				},
																				"LabelComboState__": {
																					"type": "Label",
																					"data": [
																						"ComboState__"
																					],
																					"options": {
																						"label": "_MailState",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"ComboState__": {
																					"type": "ComboBox",
																					"data": [
																						"ComboState__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "_MailState",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"notInDB": true,
																						"DisplayedColumns": ""
																					},
																					"stamp": 49
																				},
																				"LabelCountry__": {
																					"type": "Label",
																					"data": [
																						"Country__"
																					],
																					"options": {
																						"label": "_Country",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"Country__": {
																					"type": "ComboBox",
																					"data": [
																						"Country__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "_Country",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "ExistingDataSource",
																						"DisplayedColumns": "code",
																						"SavedColumn": "code"
																					},
																					"stamp": 51
																				},
																				"LabelPhone_Number__": {
																					"type": "Label",
																					"data": [
																						"Phone_Number__"
																					],
																					"options": {
																						"label": "_PhoneNumber",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"Phone_Number__": {
																					"type": "ShortText",
																					"data": [
																						"Phone_Number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_PhoneNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 40
																					},
																					"stamp": 53
																				},
																				"LabelFax_Number__": {
																					"type": "Label",
																					"data": [
																						"Fax_Number__"
																					],
																					"options": {
																						"label": "_FaxNumber",
																						"version": 0
																					},
																					"stamp": 54
																				},
																				"Fax_Number__": {
																					"type": "ShortText",
																					"data": [
																						"Fax_Number__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_FaxNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 40
																					},
																					"stamp": 55
																				},
																				"LabelWebsite__": {
																					"type": "Label",
																					"data": [
																						"Website__"
																					],
																					"options": {
																						"label": "_Website",
																						"version": 0
																					},
																					"stamp": 56
																				},
																				"Website__": {
																					"type": "ShortText",
																					"data": [
																						"Website__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Website",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"length": 50
																					},
																					"stamp": 57
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 58
																				}
																			}
																		}
																	}
																},
																"FieldsManager2": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompanyStructure__": "LabelCompanyStructure__",
																			"LabelCompanyStructure__": "CompanyStructure__",
																			"YearsInBusiness__": "LabelYearsInBusiness__",
																			"LabelYearsInBusiness__": "YearsInBusiness__",
																			"NumberOfEmployees__": "LabelNumberOfEmployees__",
																			"LabelNumberOfEmployees__": "NumberOfEmployees__",
																			"TaxID__": "LabelTaxID__",
																			"LabelTaxID__": "TaxID__",
																			"TaxStatus__": "LabelTaxStatus__",
																			"LabelTaxStatus__": "TaxStatus__",
																			"VendorRegistrationDUNSNumber__": "LabelVendorRegistrationDUNSNumber__",
																			"LabelVendorRegistrationDUNSNumber__": "VendorRegistrationDUNSNumber__"
																		},
																		"version": 0
																	},
																	"stamp": 59,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyStructure__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyStructure__": {
																						"line": 1,
																						"column": 1
																					},
																					"YearsInBusiness__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelYearsInBusiness__": {
																						"line": 2,
																						"column": 1
																					},
																					"NumberOfEmployees__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelNumberOfEmployees__": {
																						"line": 3,
																						"column": 1
																					},
																					"TaxID__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelTaxID__": {
																						"line": 4,
																						"column": 1
																					},
																					"VendorRegistrationDUNSNumber__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelVendorRegistrationDUNSNumber__": {
																						"line": 5,
																						"column": 1
																					},
																					"TaxStatus__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelTaxStatus__": {
																						"line": 6,
																						"column": 1
																					},
																					"ProcessStatus__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelProcessStatus__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 60,
																			"*": {
																				"LabelCompanyStructure__": {
																					"type": "Label",
																					"data": [
																						"CompanyStructure__"
																					],
																					"options": {
																						"label": "_CompanyStructure",
																						"version": 0
																					},
																					"stamp": 61
																				},
																				"CompanyStructure__": {
																					"type": "ComboBox",
																					"data": [
																						"CompanyStructure__"
																					],
																					"options": {
																						"possibleValues": {},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {},
																						"label": "_CompanyStructure",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 62
																				},
																				"LabelYearsInBusiness__": {
																					"type": "Label",
																					"data": [
																						"YearsInBusiness__"
																					],
																					"options": {
																						"label": "_YearsInBusiness",
																						"version": 0
																					},
																					"stamp": 63
																				},
																				"YearsInBusiness__": {
																					"type": "Integer",
																					"data": [
																						"YearsInBusiness__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_YearsInBusiness",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 64
																				},
																				"LabelNumberOfEmployees__": {
																					"type": "Label",
																					"data": [
																						"NumberOfEmployees__"
																					],
																					"options": {
																						"label": "_NumberOfEmployees",
																						"version": 0
																					},
																					"stamp": 65
																				},
																				"NumberOfEmployees__": {
																					"type": "Integer",
																					"data": [
																						"NumberOfEmployees__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfEmployees",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 66
																				},
																				"LabelTaxID__": {
																					"type": "Label",
																					"data": [
																						"TaxID__"
																					],
																					"options": {
																						"label": "_TaxID",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"TaxID__": {
																					"type": "ShortText",
																					"data": [
																						"TaxID__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TaxID",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"length": 50
																					},
																					"stamp": 68
																				},
																				"LabelVendorRegistrationDUNSNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorRegistrationDUNSNumber__"
																					],
																					"options": {
																						"label": "_VendorRegistrationDUNSNumber"
																					},
																					"stamp": 332
																				},
																				"VendorRegistrationDUNSNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorRegistrationDUNSNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorRegistrationDUNSNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 333
																				},
																				"LabelTaxStatus__": {
																					"type": "Label",
																					"data": [
																						"TaxStatus__"
																					],
																					"options": {
																						"label": "_TaxStatus",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"TaxStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"TaxStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "_taxable",
																							"2": "_exempt",
																							"3": "_Other"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "",
																							"1": "_taxable",
																							"2": "_exempt",
																							"3": "_Other"
																						},
																						"label": "_TaxStatus",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 70
																				},
																				"LabelProcessStatus__": {
																					"type": "Label",
																					"data": [
																						"ProcessStatus__"
																					],
																					"options": {
																						"label": "_ProcessStatus",
																						"version": 0,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 71
																				},
																				"ProcessStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"ProcessStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_ToValidate",
																							"1": "_PendingVendorInfo",
																							"2": "_Approved",
																							"3": "_Denied"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "ToValidate",
																							"1": "PendingVendorInfo",
																							"2": "Approved",
																							"3": "Rejected"
																						},
																						"label": "_ProcessStatus",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true
																					},
																					"stamp": 72
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 73,
													"*": {
														"CompanyOfficers": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_CompanyOfficers",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 74,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompanyOfficersTableMessage__": "LabelCompanyOfficersTableMessage__",
																			"LabelCompanyOfficersTableMessage__": "CompanyOfficersTableMessage__"
																		},
																		"version": 0
																	},
																	"stamp": 75,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyOfficersTable__": {
																						"line": 2,
																						"column": 1
																					},
																					"CompanyOfficersTableMessage__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 76,
																			"*": {
																				"CompanyOfficersTableMessage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"hidden": true,
																						"label": "",
																						"version": 0,
																						"css": "@ {\n    border: solid #0082c0;\n    border-width: 0 0 0 5px;\n    border-radius: 5px;\n    padding-left: 15px;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    margin: 10px 0px;\n    background-color: #fcf8e3;\n}@ "
																					},
																					"stamp": 297
																				},
																				"CompanyOfficersTable__": {
																					"type": "Table",
																					"data": [
																						"CompanyOfficersTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": false,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_CompanyOfficersTable",
																						"subsection": null
																					},
																					"stamp": 77,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 78,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 79
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 80
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 81,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 82,
																									"data": [],
																									"*": {
																										"Role__": {
																											"type": "Label",
																											"data": [
																												"Role__"
																											],
																											"options": {
																												"label": "_Role",
																												"version": 0
																											},
																											"stamp": 83,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"FirstName__": {
																											"type": "Label",
																											"data": [
																												"FirstName__"
																											],
																											"options": {
																												"label": "_FirstName",
																												"version": 0
																											},
																											"stamp": 85,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"LastName__": {
																											"type": "Label",
																											"data": [
																												"LastName__"
																											],
																											"options": {
																												"label": "_LastName",
																												"version": 0
																											},
																											"stamp": 87,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"Email__": {
																											"type": "Label",
																											"data": [
																												"Email__"
																											],
																											"options": {
																												"label": "_Email",
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 90,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 91,
																									"data": [],
																									"*": {
																										"Role__": {
																											"type": "ComboBox",
																											"data": [
																												"Role__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_President",
																													"1": "_SalesRepresentative",
																													"2": "_AccountReceivable"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "president",
																													"1": "salesRepresentative",
																													"2": "accountReceivable"
																												},
																												"label": "_Role",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": ""
																											},
																											"stamp": 92,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 93,
																									"data": [],
																									"*": {
																										"FirstName__": {
																											"type": "ShortText",
																											"data": [
																												"FirstName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_FirstName",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 94,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 95,
																									"data": [],
																									"*": {
																										"LastName__": {
																											"type": "ShortText",
																											"data": [
																												"LastName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_LastName",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 96,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 97,
																									"data": [],
																									"*": {
																										"Email__": {
																											"type": "ShortText",
																											"data": [
																												"Email__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Email",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false,
																												"length": 80
																											},
																											"stamp": 98,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 99,
													"*": {
														"PaymentPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PaymentPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 100,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"PaymentTerms__": "LabelPaymentTerms__",
																			"LabelPaymentTerms__": "PaymentTerms__",
																			"PaymentMethod__": "LabelPaymentMethod__",
																			"LabelPaymentMethod__": "PaymentMethod__"
																		},
																		"version": 0
																	},
																	"stamp": 101,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"PaymentTerms__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPaymentTerms__": {
																						"line": 1,
																						"column": 1
																					},
																					"PaymentMethod__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPaymentMethod__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 102,
																			"*": {
																				"LabelPaymentTerms__": {
																					"type": "Label",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"label": "_Payment terms",
																						"version": 0
																					},
																					"stamp": 103
																				},
																				"PaymentTerms__": {
																					"type": "LongText",
																					"data": [
																						"PaymentTerms__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Payment terms",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 104
																				},
																				"LabelPaymentMethod__": {
																					"type": "Label",
																					"data": [
																						"PaymentMethod__"
																					],
																					"options": {
																						"label": "_Payment method",
																						"version": 0
																					},
																					"stamp": 105
																				},
																				"PaymentMethod__": {
																					"type": "LongText",
																					"data": [
																						"PaymentMethod__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Payment method",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 106
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 107,
													"*": {
														"Banks": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_BanksPanel",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "center",
																"version": 0
															},
															"stamp": 108,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompanyBankAccountsTableMessage__": "LabelCompanyBankAccountsTableMessage__",
																			"LabelCompanyBankAccountsTableMessage__": "CompanyBankAccountsTableMessage__",
																			"IBANVerification__": "LabelIBANVerification__",
																			"LabelIBANVerification__": "IBANVerification__"
																		},
																		"version": 0
																	},
																	"stamp": 109,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyBankAccountsTable__": {
																						"line": 4,
																						"column": 1
																					},
																					"CompanyBankAccountsTableMessage__": {
																						"line": 3,
																						"column": 1
																					},
																					"IBANVerification__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelIBANVerification__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 110,
																			"*": {
																				"CompanyBankAccountsTable__": {
																					"type": "Table",
																					"data": [
																						"CompanyBankAccountsTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 10,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": false,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_CompanyBankAccountsTable",
																						"subsection": null
																					},
																					"stamp": 111,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 112,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 113
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 114
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 115,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 116,
																									"data": [],
																									"*": {
																										"BankCountry__": {
																											"type": "Label",
																											"data": [
																												"BankCountry__"
																											],
																											"options": {
																												"label": "_BankCountry",
																												"version": 0
																											},
																											"stamp": 117,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 120,
																									"data": [],
																									"*": {
																										"BankAccountHolder__": {
																											"type": "Label",
																											"data": [
																												"BankAccountHolder__"
																											],
																											"options": {
																												"label": "_BankAccountHolder",
																												"version": 0
																											},
																											"stamp": 121,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 124,
																									"data": [],
																									"*": {
																										"BankIBAN__": {
																											"type": "Label",
																											"data": [
																												"BankIBAN__"
																											],
																											"options": {
																												"label": "_BankIBAN",
																												"version": 0
																											},
																											"stamp": 125,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 328,
																									"data": [],
																									"*": {
																										"BankIBANScore__": {
																											"type": "Label",
																											"data": [
																												"BankIBANScore__"
																											],
																											"options": {
																												"label": "_Score",
																												"minwidth": 40,
																												"hidden": true
																											},
																											"stamp": 329,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 126,
																									"data": [],
																									"*": {
																										"BankKey__": {
																											"type": "Label",
																											"data": [
																												"BankKey__"
																											],
																											"options": {
																												"label": "_BankKey",
																												"version": 0
																											},
																											"stamp": 127,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 122,
																									"data": [],
																									"*": {
																										"BankAccountNumber__": {
																											"type": "Label",
																											"data": [
																												"BankAccountNumber__"
																											],
																											"options": {
																												"label": "_BankAccountNumber",
																												"version": 0
																											},
																											"stamp": 123,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 313,
																									"data": [],
																									"*": {
																										"ControlKey__": {
																											"type": "Label",
																											"data": [
																												"ControlKey__"
																											],
																											"options": {
																												"label": "_ControlKey",
																												"version": 0
																											},
																											"stamp": 314,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 317,
																									"data": [],
																									"*": {
																										"SWIFT_BICCode__": {
																											"type": "Label",
																											"data": [
																												"SWIFT_BICCode__"
																											],
																											"options": {
																												"label": "_SWIFT/BICCode",
																												"version": 0
																											},
																											"stamp": 318,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 325,
																									"data": [],
																									"*": {
																										"RoutingCode__": {
																											"type": "Label",
																											"data": [
																												"RoutingCode__"
																											],
																											"options": {
																												"label": "_RoutingCode",
																												"version": 0
																											},
																											"stamp": 326,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 321,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 322,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 128,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 129,
																									"data": [],
																									"*": {
																										"BankCountry__": {
																											"type": "ComboBox",
																											"data": [
																												"BankCountry__"
																											],
																											"options": {
																												"possibleValues": {},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {},
																												"label": "_BankCountry",
																												"activable": true,
																												"width": 120,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "ExistingDataSource",
																												"SavedColumn": "name",
																												"DisplayedColumns": "name"
																											},
																											"stamp": 130,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 133,
																									"data": [],
																									"*": {
																										"BankAccountHolder__": {
																											"type": "ShortText",
																											"data": [
																												"BankAccountHolder__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_BankAccountHolder",
																												"activable": true,
																												"width": 150
																											},
																											"stamp": 134,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 137,
																									"data": [],
																									"*": {
																										"BankIBAN__": {
																											"type": "ShortText",
																											"data": [
																												"BankIBAN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_BankIBAN",
																												"activable": true,
																												"width": 230,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 138,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true
																									},
																									"stamp": 330,
																									"data": [],
																									"*": {
																										"BankIBANScore__": {
																											"type": "Integer",
																											"data": [
																												"BankIBANScore__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_Score",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 331,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 139,
																									"data": [],
																									"*": {
																										"BankKey__": {
																											"type": "ShortText",
																											"data": [
																												"BankKey__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_BankKey",
																												"activable": true,
																												"width": 100
																											},
																											"stamp": 140,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 135,
																									"data": [],
																									"*": {
																										"BankAccountNumber__": {
																											"type": "ShortText",
																											"data": [
																												"BankAccountNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_BankAccountNumber",
																												"activable": true,
																												"width": 120,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 136,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 312,
																									"data": [],
																									"*": {
																										"ControlKey__": {
																											"type": "ShortText",
																											"data": [
																												"ControlKey__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ControlKey",
																												"activable": true,
																												"width": 50,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 315,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 316,
																									"data": [],
																									"*": {
																										"SWIFT_BICCode__": {
																											"type": "ShortText",
																											"data": [
																												"SWIFT_BICCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_SWIFT/BICCode",
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 319,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 324,
																									"data": [],
																									"*": {
																										"RoutingCode__": {
																											"type": "ShortText",
																											"data": [
																												"RoutingCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_RoutingCode",
																												"activable": true,
																												"width": 100,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 327,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 320,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [
																												"Currency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": 60,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 323,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"CompanyBankAccountsTableMessage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"hidden": true,
																						"label": "",
																						"version": 0,
																						"css": "@ {\n    border: solid #0082c0;\n    border-width: 0 0 0 5px;\n    border-radius: 5px;\n    padding-left: 15px;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    margin: 10px 0px;\n    background-color: #fcf8e3;\n}@ "
																					},
																					"stamp": 291
																				},
																				"LabelIBANVerification__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": ""
																					},
																					"stamp": 334
																				},
																				"IBANVerification__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_IBANVerification",
																						"label": "",
																						"version": 0,
																						"textPosition": "text-right",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"action": "none",
																						"url": "",
																						"urlImage": "AP_Refresh_circle.png",
																						"style": 4
																					},
																					"stamp": 335
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 141,
													"*": {
														"DocumentsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_DocumentsPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 142,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"DocumentsTableMessage__": "LabelDocumentsTableMessage__",
																			"LabelDocumentsTableMessage__": "DocumentsTableMessage__"
																		},
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DocumentsTable__": {
																						"line": 2,
																						"column": 1
																					},
																					"DocumentsTableMessage__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 144,
																			"*": {
																				"DocumentsTableMessage__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "DocumentsTableMessage__",
																						"version": 0,
																						"hidden": true,
																						"css": "@ {\n    border: solid #0082c0;\n    border-width: 0 0 0 5px;\n    border-radius: 5px;\n    padding-left: 15px;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    margin: 10px 0px;\n    background-color: #fcf8e3;\n}@ "
																					},
																					"stamp": 303
																				},
																				"DocumentsTable__": {
																					"type": "Table",
																					"data": [
																						"DocumentsTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 4,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_DocumentsTable",
																						"subsection": null
																					},
																					"stamp": 145,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 146,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 147
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 148
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 149,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 150,
																									"data": [],
																									"*": {
																										"DocumentType__": {
																											"type": "Label",
																											"data": [
																												"DocumentType__"
																											],
																											"options": {
																												"label": "_DocumentType",
																												"version": 0
																											},
																											"stamp": 151,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 152,
																									"data": [],
																									"*": {
																										"DocumentFile__": {
																											"type": "Label",
																											"data": [
																												"DocumentFile__"
																											],
																											"options": {
																												"label": "_DocumentFile",
																												"version": 0
																											},
																											"stamp": 153,
																											"position": [
																												"File uploader"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 309,
																									"data": [],
																									"*": {
																										"EndOfValidity__": {
																											"type": "Label",
																											"data": [
																												"EndOfValidity__"
																											],
																											"options": {
																												"label": "_EndOfValidity"
																											},
																											"stamp": 310,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 154,
																									"data": [],
																									"*": {
																										"DocumentStatus__": {
																											"type": "Label",
																											"data": [
																												"DocumentStatus__"
																											],
																											"options": {
																												"label": "_DocumentStatus",
																												"version": 0
																											},
																											"stamp": 155,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 156,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"DocumentType__": {
																											"type": "ComboBox",
																											"data": [
																												"DocumentType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_KBIS",
																													"1": "_RIB",
																													"2": "_W9Form",
																													"3": "_CertificateOfFiscalRegularity",
																													"4": "_Other"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "KBIS",
																													"1": "RIB",
																													"2": "W-9 form",
																													"3": "Certificate of fiscal regularity",
																													"4": "Other"
																												},
																												"label": "_DocumentType",
																												"activable": true,
																												"width": 180,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": ""
																											},
																											"stamp": 158,
																											"position": [
																												"Select from a combo box"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 159,
																									"data": [],
																									"*": {
																										"DocumentFile__": {
																											"type": "FileUploader",
																											"data": [
																												"DocumentFile__"
																											],
																											"options": {
																												"label": "_DocumentFile",
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"allowUploadingOnlyOneFile": true,
																												"version": 0
																											},
																											"stamp": 160,
																											"position": [
																												"File uploader"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {},
																									"stamp": 308,
																									"data": [],
																									"*": {
																										"EndOfValidity__": {
																											"type": "DateTime",
																											"data": [
																												"EndOfValidity__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_EndOfValidity",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 311,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"DocumentStatus__": {
																											"type": "ShortText",
																											"data": [
																												"DocumentStatus__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_DocumentStatus",
																												"activable": true,
																												"width": "250",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 162,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 163,
													"*": {
														"Navigation_Pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "none",
																"labelsAlignment": "right",
																"label": "Navigation Pane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "right",
																"version": 0
															},
															"stamp": 164,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Previous__": "LabelPrevious__",
																			"LabelPrevious__": "Previous__",
																			"Next__": "LabelNext__",
																			"LabelNext__": "Next__",
																			"Save__": "LabelSave__",
																			"LabelSave__": "Save__"
																		},
																		"version": 0
																	},
																	"stamp": 165,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Previous__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPrevious__": {
																						"line": 2,
																						"column": 1
																					},
																					"Next__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelNext__": {
																						"line": 3,
																						"column": 1
																					},
																					"Save__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSave__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"colspans": [
																					[
																						[]
																					],
																					[
																						[]
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 166,
																			"*": {
																				"LabelSave__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 167
																				},
																				"Save__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Save",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"style": 2,
																						"action": "save",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 168
																				},
																				"LabelPrevious__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"Previous__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Previous",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"style": 6
																					},
																					"stamp": 170
																				},
																				"LabelNext__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 171
																				},
																				"Next__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Next   ",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "Vendor Registration",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"style": 1,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 172
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 173,
													"*": {
														"ERPPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ERPPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 174,
															"*": {
																"FieldsManager1": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__"
																		},
																		"version": 0
																	},
																	"stamp": 175,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"CompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 176,
																			"*": {
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0
																					},
																					"stamp": 177
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true
																					},
																					"stamp": 178
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_VendorNumber",
																						"version": 0
																					},
																					"stamp": 179
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 180
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 181,
													"*": {
														"WorkflowPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_WorkflowPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 182,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Comment__": "LabelComment__",
																			"LabelComment__": "Comment__"
																		},
																		"version": 0
																	},
																	"stamp": 183,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Workflow__": {
																						"line": 2,
																						"column": 1
																					},
																					"Comment__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 184,
																			"*": {
																				"Comment__": {
																					"type": "LongText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Comment",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 234
																				},
																				"Workflow__": {
																					"type": "Table",
																					"data": [
																						"Workflow__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 7,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "Workflow",
																						"readonly": false
																					},
																					"stamp": 185,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 186,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 187
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 188
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 189,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 190,
																									"data": [],
																									"*": {
																										"Marker__": {
																											"type": "Label",
																											"data": [
																												"Marker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 191,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 192,
																									"data": [],
																									"*": {
																										"User__": {
																											"type": "Label",
																											"data": [
																												"User__"
																											],
																											"options": {
																												"label": "_User",
																												"version": 0
																											},
																											"stamp": 193,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 194,
																									"data": [],
																									"*": {
																										"Role__": {
																											"type": "Label",
																											"data": [
																												"Role__"
																											],
																											"options": {
																												"label": "_RoleWorkflow",
																												"version": 0
																											},
																											"stamp": 195,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 196,
																									"data": [],
																									"*": {
																										"Date__": {
																											"type": "Label",
																											"data": [
																												"Date__"
																											],
																											"options": {
																												"label": "_Date/Time",
																												"version": 0
																											},
																											"stamp": 197,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": false
																									},
																									"stamp": 236,
																									"data": [],
																									"*": {
																										"ApproverComment__": {
																											"type": "Label",
																											"data": [
																												"ApproverComment__"
																											],
																											"options": {
																												"label": "_ApproverComment",
																												"version": 0,
																												"hidden": false,
																												"readonly": true
																											},
																											"stamp": 237,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 200,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_Workflow_index",
																												"version": 0
																											},
																											"stamp": 201,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 198,
																									"data": [],
																									"*": {
																										"Action__": {
																											"type": "Label",
																											"data": [
																												"Action__"
																											],
																											"options": {
																												"label": "_Action",
																												"version": 0
																											},
																											"stamp": 199,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 202,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 203,
																									"data": [],
																									"*": {
																										"Marker__": {
																											"type": "ShortText",
																											"data": [
																												"Marker__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 204,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 205,
																									"data": [],
																									"*": {
																										"User__": {
																											"type": "ShortText",
																											"data": [
																												"User__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_User",
																												"activable": true,
																												"width": "200",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 206,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 207,
																									"data": [],
																									"*": {
																										"Role__": {
																											"type": "ShortText",
																											"data": [
																												"Role__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Role",
																												"activable": true,
																												"width": 170,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 208,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 209,
																									"data": [],
																									"*": {
																										"Date__": {
																											"type": "RealDateTime",
																											"data": [
																												"Date__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_Date/Time",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 210,
																											"position": [
																												"Date/Time"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"ApproverComment__": {
																											"type": "LongText",
																											"data": [
																												"ApproverComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ApproverComment",
																												"activable": true,
																												"width": "330",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 5,
																												"browsable": false,
																												"hidden": false,
																												"readonly": true
																											},
																											"stamp": 238,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 213,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Workflow_index",
																												"activable": true,
																												"width": 140
																											},
																											"stamp": 214,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 211,
																									"data": [],
																									"*": {
																										"Action__": {
																											"type": "ShortText",
																											"data": [
																												"Action__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Action",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 212,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 215,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 99
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 216
																}
															},
															"stamp": 217,
															"data": []
														}
													},
													"stamp": 218,
													"data": []
												}
											},
											"stamp": 219,
											"data": []
										}
									},
									"stamp": 220,
									"data": []
								}
							},
							"stamp": 221,
							"data": []
						}
					},
					"stamp": 222,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1200
					},
					"*": {
						"BackToVendor": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_BackToVendor",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0,
								"hidden": true
							},
							"stamp": 239
						},
						"BackToPrevious": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_BackToPrevious",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 223
						},
						"SubmitToNext": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_SubmitToNext",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0,
								"style": 1
							},
							"stamp": 224
						},
						"Approve": {
							"type": "SubmitButton",
							"options": {
								"label": "_Approve",
								"action": "none",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null
							},
							"stamp": 225,
							"data": []
						},
						"Reject": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reject",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Accounts Receivable Batch Processing",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"style": 3,
								"version": 0
							},
							"stamp": 226
						},
						"Save": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"action": "save",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"style": 2
							},
							"stamp": 227
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "Vendor Registration",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 228,
							"data": []
						}
					},
					"stamp": 229,
					"data": []
				}
			},
			"stamp": 230,
			"data": []
		}
	},
	"stamps": 335,
	"data": []
}