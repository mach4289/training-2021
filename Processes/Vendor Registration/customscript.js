///#GLOBALS Lib Sys
/* global navigator setTimeout*/
var g_commentPlaceHolder = Language.Translate("_Enter your comment...");
var warningMessageTradKey = "_One or more vendors with the same informations are already registered";
var highlightStyles = {
	error: "highlight-danger",
	warning: "highlight-warning",
	success: "highlight-success"
};
var workflowController = null;
var ValuesFromMasterData;
var WorkflowParameters = {
	roles:
	{
		requester: "_Requester",
		vendor: "_Vendor",
		approver: "_Approver",
		OFACChecker: "_OFACChecker",
		SisIdChecker: "_SisIdChecker",
		LegalReviewer: "_LegalReviewer"
	},
	actions:
	{
		requestVendorInformations:
		{
			image: "VR_submit_grey.png"
		},
		toSubmit:
		{
			image: "VR_submit_grey.png"
		},
		submitted:
		{
			image: "VR_submit.png"
		},
		OFACChecked:
		{
			image: "VR_Workflow_verified.png"
		},
		SisIdChecked:
		{
			image: "VR_Workflow_verified.png"
		},
		toApprove:
		{
			image: "VR_approve_or_reject_grey.png"
		},
		approved:
		{
			image: "VR_approval.png"
		},
		toReview:
		{
			image: "VR_approve_or_reject_grey.png"
		},
		backToPrevious:
		{
			image: "VR_back.png"
		},
		backToVendor:
		{
			image: "VR_backToVendor.png"
		},
		reject:
		{},
		rejected:
		{
			image: "VR_reject.png"
		}
	},
	// Indicates how to fill the table control on the form.
	mappingTable:
	{
		tableName: "Workflow__",
		columns:
		{
			User__:
			{
				data: "name"
			},
			Role__:
			{
				data: "role",
				translate: true
			},
			Date__:
			{
				data: "date"
			},
			Action__:
			{
				data: "action"
			}
		},
		OnRefreshRow: function (index)
		{
			var table = Controls[WorkflowParameters.mappingTable.tableName];
			var row = table.GetRow(index);
			// Sets the image.
			row.Role__.SetImageURL(WorkflowParameters.actions[row.Action__.GetValue()].image, false);
			// Highlights the row and adds a cursor in the first column if the process is not in a final state.
			if (ProcessInstance.state < 100)
			{
				if (workflowController.IsCurrentContributorAt(row))
				{
					row.AddStyle("highlight");
					row.Marker__.SetImageURL("arrow.png", false);
				}
				else
				{
					row.RemoveStyle("highlight");
					row.Marker__.SetImageURL();
				}
			}
		}
	},
	callbacks:
	{
		OnError: function (msg)
		{
			Log.Info(msg);
		},
		OnBuilding: function ()
		{
			ProcessInstance.SetSilentChange(true);
		},
		OnBuilt: function ()
		{
			if (!User.isVendor && User.profileRole !== "simpleUser")
			{
				ProcessInstance.SetSilentChange(false);
			}
		}
	}
};
var HandleActions = {
	ApproveWithPopupCommentDialog: function ()
	{
		ValidateRequiredFields();
		if (!Process.ShowFirstError())
		{
			HandleActions.popUpCommentDialog("_ApprovePopupTitle", "Submit");
		}
	},
	Approve: function ()
	{
		ValidateRequiredFields();
		var submitFn = function ()
		{
			if (!Process.ShowFirstError())
			{
				Lib.VendorRegistration.DocumentsTable.AttachDocumentsToRecord();
				ProcessInstance.Approve("Submit");
			}
		};
		var cancelApproveActionFn = function ()
		{
			// do nothing.
		};
		var passIBANErrorToWarningAndSubmit = function ()
		{
			SisID.EnableSisidErrorAsWarning(true);
			SisID.SetAllSisidErrorsAsWarnings();
			submitFn();
		};
		var displayApproveConfirmationPopup = function (messagesToFormat, callback)
		{
			var popupMessage = "";
			var popupTile = "";
			if (messagesToFormat.length > 1)
			{
				var concatenatedMessages_1 = "";
				messagesToFormat.forEach(function (element)
				{
					concatenatedMessages_1 += "\u2022 " + element + "\n";
				});
				popupMessage = Language.Translate("_{0} ApproveDespiteMultipleIssues", false, concatenatedMessages_1);
				popupTile = "_ApproveDespiteIssuesPopupTile";
			}
			else
			{
				popupMessage = Language.Translate("_{0} ApproveDespiteOneIssue", false, messagesToFormat[0]);
				popupTile = "_ApproveDespiteIssuePopupTile";
			}
			Popup.Confirm(popupMessage, false, callback, cancelApproveActionFn, popupTile);
		};
		if (!User.isVendor && ProcessInstance.id)
		{
			var messageToDisplay_1 = [];
			if (Lib.VendorRegistration.CheckDuplicateVendor.ShouldCheckDuplicates())
			{
				var ruidEx = ProcessInstance.id;
				var msnEx = ruidEx.substring(ruidEx.indexOf(".") + 1);
				Lib.VendorRegistration.CheckDuplicateVendor.CheckAllDuplicates(msnEx).then(function (duplicationsFound)
				{
					var popupConfirmationCallback = submitFn;
					if (duplicationsFound)
					{
						messageToDisplay_1.push(Language.Translate("_DuplicatesFound"));
					}
					if (SisID.hasError)
					{
						popupConfirmationCallback = passIBANErrorToWarningAndSubmit;
						messageToDisplay_1.push(Language.Translate("_SisIdErrorOnIBAN"));
					}
					if (messageToDisplay_1.length > 0)
					{
						displayApproveConfirmationPopup(messageToDisplay_1, popupConfirmationCallback);
					}
					else
					{
						submitFn();
					}
				});
			}
			else if (SisID.hasError)
			{
				displayApproveConfirmationPopup([Language.Translate("_SisIdErrorOnIBAN")], passIBANErrorToWarningAndSubmit);
			}
			else
			{
				submitFn();
			}
		}
		else
		{
			submitFn();
		}
	},
	Reject: function ()
	{
		HandleActions.popUpCommentDialog("_WarningRejectTitle", "Reject");
	},
	BackToVendor: function ()
	{
		HandleActions.popUpCommentDialog("_BackToVendorPopupTitle", "BackToVendor");
	},
	/* HELPER */
	popUpCommentDialog: function (dialogTitle, actionName)
	{
		var dialogOptions = {
			allowComment: true,
			requiredComment: true,
			explanationLabel: null,
			title: dialogTitle,
			onClickOk: function (result)
			{
				// Store value on the form!
				Data.SetValue("Comment__", result.comment);
				ProcessInstance.Approve(actionName);
			}
		};
		Lib.CommonDialog.PopupOkExtended(dialogOptions);
	}
};
var Wizard = {
	_currentStep: 0,
	steps: [
	{
		panels: [Controls.CompanyProfilePane],
		button: Controls.Company_Step__,
		requiredFields: [],
		withErrors: null,
		onInit: function ()
		{
			CountryManagement.CountryChange(Controls.Country__);
		},
		onShow: function ()
		{
			Wizard.GetRequiredFields(this);
			if (!this.withErrors)
			{
				Wizard.ResetRequiredFields(this);
			}
			VAT.Validate();
		},
		onQuit: function ()
		{
			Wizard.GetRequiredFields(this); // TaxID__'s required property is now dynamically set when Coutry__ changes, so we need to refresh the list here
			this.withErrors = !Wizard.CheckRequiredFields(this) || !VAT.Validate();
			if (this.withErrors)
			{
				Process.ShowFirstError();
			}
			return !this.withErrors;
		}
	},
	{
		panels: [Controls.CompanyOfficers],
		button: Controls.Company_Officers__,
		requiredFieldsInTable: [],
		withErrors: null,
		onShow: function ()
		{
			this.requiredFieldsInTable = [];
			for (var k = 1; k <= Controls.CompanyOfficersTable__.GetColumnCount(); k++)
			{
				var column = Controls.CompanyOfficersTable__.GetColumnControl(k);
				if (column.IsRequired())
				{
					this.requiredFieldsInTable.push(column.GetName());
				}
			}
		},
		onQuit: function ()
		{
			this.withErrors = false;
			if (Controls.CompanyOfficersTable__.GetItemCount() < 1)
			{
				Controls.CompanyOfficersTable__.AddItem(false);
			}
			for (var i = 0; i < Controls.CompanyOfficersTable__.GetItemCount(); i++)
			{
				var row = Controls.CompanyOfficersTable__.GetRow(i);
				for (var _i = 0, _a = this.requiredFieldsInTable; _i < _a.length; _i++)
				{
					var requiredFld = _a[_i];
					if (!row[requiredFld].GetValue())
					{
						row[requiredFld].SetError("This field is required!");
						this.withErrors = true;
					}
				}
			}
			if (this.withErrors)
			{
				Process.ShowFirstError();
			}
			return !this.withErrors;
		}
	},
	{
		panels: [Controls.PaymentPane, Controls.Banks],
		button: Controls.Banks__,
		requiredFields: [],
		withErrors: null,
		onInit: function ()
		{
			Wizard.GetRequiredFields(this);
			Wizard.ResetRequiredFields(this);
		},
		onShow: function ()
		{
			var country = Controls.Country__.GetValue();
			var bankDetailsTable = Data.GetTable("CompanyBankAccountsTable__");
			for (var i = 0; i < bankDetailsTable.GetItemCount(); i++)
			{
				var item = bankDetailsTable.GetItem(i);
				if (BankAccountsManagement.IsItemEmpty(item))
				{
					item.SetValue("BankCountry__", country);
					BankAccountsManagement.SetCurrency(item, country);
				}
			}
			this.withErrors = BankAccountsManagement.SetAllReadOnlyAndRequiredFields();
			if (!this.withErrors)
			{
				Wizard.ResetRequiredFields(this);
			}
		},
		onQuit: function ()
		{
			this.withErrors = BankAccountsManagement.SetAllReadOnlyAndRequiredFields();
			if (this.withErrors)
			{
				Process.ShowFirstError();
			}
			return !this.withErrors;
		}
	},
	{
		panels: [Controls.DocumentsPane],
		button: Controls.Documents_Step__,
		requiredFields: [],
		withErrors: null,
		onInit: function ()
		{
			Wizard.GetRequiredFields(this);
			Wizard.ResetRequiredFields(this);
		},
		onShow: function ()
		{
			if (!this.withErrors)
			{
				Wizard.ResetRequiredFields(this);
			}
		},
		onQuit: function ()
		{
			this.withErrors = !Wizard.CheckRequiredFields(this);
			if (this.withErrors)
			{
				Process.ShowFirstError();
			}
			return !this.withErrors;
		}
	}],
	Init: function ()
	{
		Wizard.MapControls();
		Wizard.InitPanes();
		Wizard.ShowStep(0);
	},
	InitPanes: function ()
	{
		for (var _i = 0, _a = Wizard.steps; _i < _a.length; _i++)
		{
			var step = _a[_i];
			if (typeof step.onInit === "function")
			{
				step.onInit();
			}
		}
	},
	ChangeStep: function (stepId)
	{
		if (!Wizard.ValidCurrentStep())
		{
			return;
		}
		Wizard.ShowStep(stepId);
	},
	ShowStep: function (stepId)
	{
		for (var index = 0; index < this.steps.length; index++)
		{
			var step = Wizard.steps[index];
			var isCurrentStep = index === stepId;
			for (var _i = 0, _a = step.panels; _i < _a.length; _i++)
			{
				var panel = _a[_i];
				panel.Hide(!isCurrentStep);
			}
			step.button.SetDisabled(stepId < index);
			step.button.SetAwesomeClasses(isCurrentStep ? "fa fa-circle fa-2" : "fa fa-circle-o fa-2");
		}
		Wizard.steps[stepId].onShow();
		Wizard.HandlePreviousNextButtons(stepId);
		Wizard.HandleSaveButton();
		Wizard._currentStep = stepId;
	},
	ValidCurrentStep: function ()
	{
		return Wizard._currentStep !== null && Wizard.steps[Wizard._currentStep].onQuit();
	},
	HandleSaveButton: function ()
	{
		Controls.Save__.Hide(ProcessInstance.isReadOnly || Data.GetValue("RegistrationType__") === "update");
	},
	HandlePreviousNextButtons: function (stepId)
	{
		Controls.Previous__.SetDisabled(stepId === 0);
		var nextButton = Controls.Next__;
		if (stepId === Wizard.steps.length - 1)
		{
			if (ProcessInstance.isReadOnly)
			{
				nextButton.SetDisabled(true);
				nextButton.SetText("_Next");
			}
			else
			{
				nextButton.SetDisabled(false);
				nextButton.SetText("_Submit");
			}
		}
		else
		{
			nextButton.SetDisabled(false);
			nextButton.SetText("_Next");
		}
	},
	MapControls: function ()
	{
		Controls.Company_Step__.OnClick = function ()
		{
			Wizard.ChangeStep(0);
		};
		Controls.Company_Officers__.OnClick = function ()
		{
			Wizard.ChangeStep(1);
		};
		Controls.Banks__.OnClick = function ()
		{
			Wizard.ChangeStep(2);
		};
		Controls.Documents_Step__.OnClick = function ()
		{
			Wizard.ChangeStep(3);
		};
		Controls.Previous__.OnClick = function ()
		{
			Wizard.ChangeStep(Wizard._currentStep - 1);
		};
		Controls.Next__.OnClick = function ()
		{
			var isStepValid = true;
			if (Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorRegistration_HTMLScripts.ValidCurrentStep") === false)
			{
				isStepValid = false;
			}
			if (isStepValid)
			{
				if (Wizard._currentStep === Wizard.steps.length - 1)
				{
					return Wizard.Approve();
				}
				return Wizard.ChangeStep(Wizard._currentStep + 1);
			}
		};
	},
	GetRequiredFields: function (step)
	{
		step.requiredFields = [];
		step.panels.forEach(function (panel)
		{
			panel.GetControls().forEach(function (control)
			{
				if (typeof control.IsRequired === "function" && control.IsRequired())
				{
					step.requiredFields.push(control);
				}
			});
		});
	},
	ResetRequiredFields: function (step)
	{
		step.requiredFields.forEach(function (control)
		{
			control.SetError(null);
		});
	},
	CheckRequiredFields: function (step)
	{
		var stepValid = true;
		for (var _i = 0, _a = step.requiredFields; _i < _a.length; _i++)
		{
			var control = _a[_i];
			if (!control.GetText() || (control.GetType() === "ComboBox" && !control.GetSelectedOption()))
			{
				control.SetError("This field is required!");
				stepValid = false;
			}
			else if ((control.GetType() === "Decimal" || control.GetType() === "Integer") && isNaN(parseFloat(control.GetText())))
			{
				control.SetError("Value is not allowed!");
				stepValid = false;
			}
		}
		return stepValid;
	},
	Approve: function ()
	{
		for (var i = 0; i < Wizard.steps.length; i++)
		{
			var step = Wizard.steps[i];
			if (!step.onQuit())
			{
				Wizard.ShowStep(i);
				return;
			}
		}
		var queryOptions = {
			table: "CDNAME#Vendor Registration",
			filter: "(&(State<100)(Deleted=0)(!(MsnEx=" + Data.GetValue("MsnEx") + "))(VendorNumber__=" + Data.GetValue("VendorNumber__") + ")(CompanyCode__=" + Data.GetValue("CompanyCode__") + "))",
			attributes: ["RuidEx"],
			maxRecords: 1,
			additionalOptions:
			{
				asAdmin: true
			}
		};
		Sys.GenericAPI.PromisedQuery(queryOptions).Then(function (queryResult)
		{
			if (queryResult && queryResult.length > 0)
			{
				// Show an error popup and leave the form
				Popup.Alert("_ErrorMessageDuplicateRegistrationUpdate", false, function ()
				{
					ProcessInstance.Quit("Quit");
				}, "_ErrorTitleDuplicateRegistrationUpdate");
			}
			else
			{
				Lib.VendorRegistration.DocumentsTable.AttachDocumentsToRecord();
				if (isVendorGuest())
				{
					Process.SetReturnURL("logout.aspx?vendor=1&status=LogoutAndByeSuccess&appId=VendorRegistration&lang=" + User.language);
				}
				ProcessInstance.Approve("Submit");
			}
		});
	}
};
var CountryManagement = {
	_countriesNameCache: null,
	_statesCache:
	{},
	_businessStructuresCache: null,
	_nbRetries: 4,
	Init: function ()
	{
		var wait = function (ms)
		{
			return Sys.Helpers.Promise.Create(function (resolve)
			{
				return setTimeout(resolve, ms);
			});
		};
		var getCountriesList = function ()
		{
			return Sys.Helpers.Promise.Create(function (resolve)
			{
				var allCountries = Controls.Country__.GetAvailableValues();
				if (allCountries.length <= 0 && CountryManagement._nbRetries > 0)
				{
					CountryManagement._nbRetries--;
					wait(200).Then(getCountriesList).Then(resolve);
				}
				else
				{
					CountryManagement._countriesNameCache = {};
					for (var i = 0; i < allCountries.length; i++)
					{
						var keyVal = allCountries[i].split("=");
						CountryManagement._countriesNameCache[keyVal[0]] = keyVal[1];
					}
					resolve();
				}
			});
		};
		return Sys.Helpers.Promise.Create(function (resolve)
		{
			if (CountryManagement._countriesNameCache === null)
			{
				getCountriesList().Then(resolve);
			}
			else
			{
				resolve();
			}
		});
	},
	CountryChange: function (countryControl)
	{
		VAT.Validate();
		Lib.VendorRegistration.DocumentsTable.UpdateDocumentTypeAvailableValues();
		var country = countryControl.GetValue();
		if (!CountryManagement._statesCache[country])
		{
			CountryManagement._statesCache[country] = null;
			if (country === "")
			{
				CountryManagement.DisplayStates();
				return;
			}
			else
			{
				Data.GetStatesFromCountry(country, function (result)
				{
					if (!result)
					{
						CountryManagement.DisplayStates();
						return;
					}
					var statesNode = result.getElementsByTagName("states").item(0);
					if (!statesNode)
					{
						CountryManagement.DisplayStates();
						return;
					}
					CountryManagement.CachingStates(country, statesNode);
					CountryManagement.DisplayStates(country);
				});
			}
		}
		else
		{
			CountryManagement.DisplayStates(CountryManagement._statesCache[country].length > 0 ? country : null);
		}
		CountryManagement.DisplayCountryBusinessStructures(country);
	},
	DisplayCountryBusinessStructures: function (country)
	{
		function getStructureValues()
		{
			var comboBoxOptions = [];
			var queryError = this.GetQueryError();
			if (queryError)
			{
				Log.Error("Query has failed. Could not retrieve the business structures");
			}
			else
			{
				var count = this.GetRecordsCount();
				if (count > 0)
				{
					for (var i = 0; i < count; i++)
					{
						var businessStructure = this.GetQueryValue("BusinessStructure__", i);
						comboBoxOptions.push(businessStructure + "=" + businessStructure);
					}
					comboBoxOptions.unshift("=");
				}
			}
			var isInit = false;
			if (!CountryManagement._businessStructuresCache)
			{
				CountryManagement._businessStructuresCache = {};
				isInit = true;
			}
			CountryManagement.CachingBusinessStructures(country, comboBoxOptions);
			setStructureValues(isInit);
		}

		function setStructureValues(isInit)
		{
			if (isInit === void 0)
			{
				isInit = false;
			}
			var values = CountryManagement._businessStructuresCache[country];
			Controls.CompanyStructure__.SetAvailableValues(values);
			if (isInit)
			{
				var value = Controls.CompanyStructure__.GetValue();
				if (Controls.CompanyStructure__.GetAvailableValues(true).indexOf(value) == -1)
				{
					Controls.CompanyStructure__.SetValue(null);
				}
			}
			else
			{
				Controls.CompanyStructure__.SetValue(null);
			}
			Controls.CompanyStructure__.Hide(values.length == 0);
		}
		if (!CountryManagement._businessStructuresCache || !CountryManagement._businessStructuresCache[country])
		{
			Query.DBQuery(getStructureValues, "Country_Business_Structures__", "BusinessStructure__", "(Country__=" + country + ")", "BusinessStructure__ ASC", 100);
		}
		else
		{
			setStructureValues();
		}
	},
	CachingBusinessStructures: function (country, comboBoxOptions)
	{
		CountryManagement._businessStructuresCache[country] = comboBoxOptions;
	},
	CachingStates: function (country, statesNode)
	{
		var states = statesNode.getElementsByTagName("state");
		CountryManagement._statesCache[country] = [];
		for (var i = 0; i < states.length; i++)
		{
			var state = states.item(i);
			CountryManagement._statesCache[country].push(state.getAttribute("code") + "=" + state.getAttribute("name"));
		}
	},
	DisplayStates: function (country)
	{
		if (country)
		{
			Controls.ComboState__.SetAvailableValues(CountryManagement._statesCache[country]);
			Controls.ComboState__.SelectOption(Controls.Mail_State__.GetValue());
			Controls.Mail_State__.SetValue(Controls.ComboState__.GetSelectedOption());
		}
		else
		{
			Controls.Mail_State__.SetValue("");
		}
		Controls.Mail_State__.Hide(Boolean(country));
		Controls.ComboState__.Hide(!country);
	},
	GetCountryName: function (country)
	{
		return CountryManagement._countriesNameCache[country] || country;
	}
};
var BankAccountsManagement = {
	_countriesUsingEuro: [
		"AT",
		"BE",
		"CY",
		"EE",
		"FI",
		"FR",
		"DE",
		"GR",
		"IE",
		"IT",
		"LV",
		"LT",
		"LU",
		"MT",
		"NL",
		"PT",
		"SK",
		"SI",
		"ES",
		"GF",
		"GP",
		"MQ",
		"RE",
		"TF",
		"YT",
		"BL",
		"MF",
		"PM",
		"AX",
		"IC",
		"EA",
		"MC",
		"AD",
		"SM",
		"VA" // Vatican
	],
	EmptyFields: function (item)
	{
		item.SetValue("BankIBAN__", "");
		item.SetValue("BankIBANScore__", null);
		item.SetValue("BankKey__", "");
		item.SetValue("BankAccountNumber__", "");
		item.SetValue("ControlKey__", "");
		item.SetValue("SWIFT_BICCode__", "");
		item.SetValue("RoutingCode__", "");
	},
	IsCountryUsingEuro: function (countryCode)
	{
		return BankAccountsManagement._countriesUsingEuro.indexOf(countryCode) >= 0;
	},
	IsItemEmpty: function (item)
	{
		return item.IsNullOrEmpty("BankAccountHolder__") &&
			item.IsNullOrEmpty("BankIBAN__") &&
			item.IsNullOrEmpty("BankKey__") &&
			item.IsNullOrEmpty("BankAccountNumber__") &&
			item.IsNullOrEmpty("ControlKey__") &&
			item.IsNullOrEmpty("SWIFT_BICCode__") &&
			item.IsNullOrEmpty("RoutingCode__");
	},
	SetCurrency: function (item, country)
	{
		if (item)
		{
			if (country === "US")
			{
				item.SetValue("Currency__", "USD");
			}
			else if (BankAccountsManagement.IsCountryUsingEuro(country))
			{
				item.SetValue("Currency__", "EUR");
			}
			else
			{
				item.SetValue("Currency__", "");
			}
		}
	},
	SetErrorIfEmpty: function (item, field)
	{
		if (item.IsNullOrEmpty(field))
		{
			item.SetError(field, "This field is required!");
			return 1;
		}
		item.SetError(field, "");
		return 0;
	},
	SetRequiredFields: function (item)
	{
		var nbErrors = 0;
		if (item)
		{
			if (BankAccountsManagement.IsItemEmpty(item))
			{
				item.SetError("BankCountry__", "");
				item.SetError("BankAccountHolder__", "");
				item.SetError("SWIFT_BICCode__", "");
				item.SetError("BankKey__", "");
				item.SetError("BankAccountNumber__", "");
				item.SetError("BankIBAN__", "");
				item.SetError("ControlKey__", "");
				item.SetError("Currency__", "");
			}
			else
			{
				nbErrors += BankAccountsManagement.SetErrorIfEmpty(item, "BankCountry__");
				nbErrors += BankAccountsManagement.SetErrorIfEmpty(item, "BankAccountHolder__");
				nbErrors += BankAccountsManagement.SetErrorIfEmpty(item, "SWIFT_BICCode__");
				nbErrors += BankAccountsManagement.SetErrorIfEmpty(item, "Currency__");
				var bankCountry = item.GetValue("BankCountry__");
				if (Sys.Helpers.Iban.IsIbanCountry(bankCountry))
				{
					item.SetError("BankKey__", "");
					item.SetError("BankAccountNumber__", "");
					nbErrors += IBAN.Validate(item);
				}
				else
				{
					item.SetError("BankIBAN__", "");
					nbErrors += BankAccountsManagement.SetErrorIfEmpty(item, "BankAccountNumber__");
				}
			}
		}
		return nbErrors;
	},
	SetReadOnlyFields: function (row)
	{
		if (row)
		{
			var bankCountry = row.BankCountry__.GetValue();
			if (Sys.Helpers.Iban.IsIbanCountry(bankCountry))
			{
				row.BankKey__.SetReadOnly(true);
				row.BankAccountNumber__.SetReadOnly(true);
				row.ControlKey__.SetReadOnly(true);
				row.RoutingCode__.SetReadOnly(true);
				row.BankIBAN__.SetReadOnly(false);
			}
			else
			{
				row.BankKey__.SetReadOnly(false);
				row.BankAccountNumber__.SetReadOnly(false);
				row.ControlKey__.SetReadOnly(false);
				row.RoutingCode__.SetReadOnly(false);
				row.BankIBAN__.SetReadOnly(true);
			}
		}
	},
	SetAllReadOnlyAndRequiredFields: function ()
	{
		var nbErrors = 0;
		var bankDetailsTable = Data.GetTable("CompanyBankAccountsTable__");
		for (var i = 0; i < bankDetailsTable.GetItemCount(); i++)
		{
			nbErrors += BankAccountsManagement.SetRequiredFields(bankDetailsTable.GetItem(i));
			BankAccountsManagement.SetReadOnlyFields(Controls.CompanyBankAccountsTable__.GetRow(i));
		}
		return nbErrors > 0;
	},
	SetReadOnlyAndRequiredFields: function (item, row)
	{
		BankAccountsManagement.SetRequiredFields(item);
		BankAccountsManagement.SetReadOnlyFields(row);
	}
};
var Layout = {
	General: function ()
	{
		Controls.CompanyOfficersTable__.SetWidth("100%");
		Controls.CompanyOfficersTable__.SetExtendableColumn("Email__");
		Controls.CompanyBankAccountsTable__.SetWidth("100%");
		Controls.CompanyBankAccountsTable__.SetExtendableColumn("BankIBAN__");
		Controls.DocumentsTable__.SetWidth("100%");
		Controls.DocumentsTable__.SetExtendableColumn("DocumentFile__");
		Controls.Workflow__.SetWidth("100%");
		Controls.Workflow__.SetExtendableColumn("ApproverComment__");
		Controls.Workflow__.HideTableRowDelete(true);
		Controls.Workflow__.HideTableRowAdd(true);
		var parameters = Controls.form_header.GetActionControl("parameters");
		if (parameters && !ProcessInstance.isDesignActive)
		{
			parameters.Hide();
		}
		Controls.Save__.OnClick = function ()
		{
			Lib.VendorRegistration.DocumentsTable.AttachDocumentsToRecord();
			ProcessInstance.Save("save");
		};
		Controls.Save.OnClick = Controls.Save__.OnClick;
		//WORKLOW LAYOUT
		//Controls.Workflow__.SetRowToolsHidden(true);
		if (!ProcessInstance.isDebugActive)
		{
			Controls.Workflow__.Action__.Hide();
			Controls.Workflow__.Workflow_index__.Hide();
		}
		var nbSteps = workflowController.GetNbContributors() - 1;
		var currentRole = workflowController.GetRoleAt(workflowController.GetContributorIndex());
		var previousRole = workflowController.GetRoleAt(workflowController.GetContributorIndex() - 1);
		// Hides buttons under certain conditions
		Controls.Approve.Hide(ProcessInstance.isReadOnly || workflowController.GetContributorIndex() !== nbSteps);
		Controls.SubmitToNext.Hide(ProcessInstance.isReadOnly || workflowController.GetContributorIndex() === nbSteps);
		Controls.BackToPrevious.Hide(ProcessInstance.isReadOnly || (previousRole !== WorkflowParameters.roles.approver && previousRole !== WorkflowParameters.roles.LegalReviewer));
		Controls.Reject.Hide(ProcessInstance.isReadOnly || currentRole === WorkflowParameters.roles.vendor);
		Controls.Comment__.SetPlaceholder(g_commentPlaceHolder);
		Layout.AdaptCompanyOfficersTableAvailableActions();
		Layout.AdaptBankAccountsTableAvailableActions();
		Controls.IBANVerification__.Hide(true);
		// Depending on user (simple user or vendor disable modifications)
		if (User.isVendor || User.profileRole !== "simpleUser")
		{
			ProcessInstance.SetSilentChange(true);
		}
		VAT.AdaptLabelAndRequire();
		Lib.VendorRegistration.DocumentsTable.Init();
		HighlightHandling.ResetAllTooltips();
	},
	ForVendor: function ()
	{
		Process.SetHelpId(2571);
		var contactLinkBtn = Controls.form_header.GetActionControl("contactLink");
		var logoutBtn = Controls.form_header.GetActionControl("logout");
		var homeBtn = Controls.form_header.GetActionControl("home");
		if (contactLinkBtn)
		{
			contactLinkBtn.Hide();
		}
		if (logoutBtn)
		{
			logoutBtn.Hide();
		}
		if (homeBtn)
		{
			homeBtn.Hide();
		}
		Controls.CompanyCode__.SetRequired(false);
		Controls.VendorNumber__.SetRequired(false);
		Controls.ERPPane.Hide(true);
		Controls.WorkflowPane.Hide(true);
		Controls.ProcessStatus__.Hide(true);
		Controls.Save.Hide(true);
		Controls.Approve.Hide(true);
		Controls.Close.Hide(true);
		Controls.Reject.Hide(true);
		Controls.SubmitToNext.Hide(true);
		Controls.BackToPrevious.Hide(true);
		Controls.DocumentsTable__.EndOfValidity__.Hide(true);
		var backToComment = Data.GetValue("Comment__");
		if (backToComment)
		{
			if (ProcessInstance.state === 400)
			{
				// Reject - set banner color to red
				var css = "{ " +
					"border: solid #E42518; " +
					"border-width: 0 0 0 5px; " +
					"border-radius: 5px;" +
					"padding-left: 15px; " +
					"padding-top: 10px; " +
					"padding-bottom: 10px; " +
					"margin-left: -10px; " +
					"background-color:#fce9e7; }";
				Controls.CommentContent__.SetCSS(css);
			}
			Controls.CommentContent__.SetHTML(backToComment.replace(/\n/g, "<br>"));
			Controls.CommentPane.Hide(false);
		}
	},
	ForManagement: function ()
	{
		Process.SetHelpId(2562);
		Controls.Steps.Hide(true);
		Controls.Navigation_Pane.Hide(true);
		// ERP fields are mandatory for the last approver
		Controls.CompanyCode__.SetRequired(IsLastApprover());
		Controls.VendorNumber__.SetRequired(IsLastApprover());
		Controls.Workflow__.HideTableRowMenu(true);
		Controls.Workflow__.SetReadOnly(false);
		Controls.Workflow__.User__.SetBrowsable(false);
		Controls.Workflow__.User__.SetReadOnly(true);
		Controls.Workflow__.Date__.SetReadOnly(true);
		Controls.Workflow__.Role__.SetReadOnly(true);
		Controls.Workflow__.Workflow_index__.SetReadOnly(true);
		Controls.Workflow__.Action__.SetReadOnly(true);
		Controls.Workflow__.Marker__.SetReadOnly(true);
		CountryManagement.CountryChange(Controls.Country__);
		Controls.BackToVendor.Hide(ProcessInstance.isReadOnly);
		var vendorIndex = workflowController.GetRoleSequenceIndex(WorkflowParameters.roles.vendor);
		if (vendorIndex + 1 === workflowController.GetContributorIndex())
		{
			Controls.BackToPrevious.Hide(true);
		}
	},
	AdaptCompanyOfficersTableAvailableActions: function ()
	{
		Controls.CompanyOfficersTable__.GetRow(0).Role__.SetReadOnly(true);
		Controls.CompanyOfficersTable__.HideTableRowDeleteForItem(0, true);
		Controls.CompanyOfficersTable__.HideTableRowDelete(Controls.CompanyOfficersTable__.GetItemCount() === 1);
		// Prevent pagination
		if (Data.GetTable("CompanyOfficersTable__").GetItemCount() > (Controls.CompanyOfficersTable__.GetLineCount() - 1))
		{
			Data.GetTable("CompanyOfficersTable__").SetItemCount(Controls.CompanyOfficersTable__.GetLineCount() - 1);
		}
	},
	AdaptBankAccountsTableAvailableActions: function ()
	{
		Controls.CompanyBankAccountsTable__.HideTableRowDelete(Controls.CompanyBankAccountsTable__.GetItemCount() === 1);
	}
};
var SisID = {
	messages: [],
	hasError: false,
	snackbarMessage: "",
	snackbarStatus: "",
	snackbarCounter: 0,
	Init: function ()
	{
		SisID.snackbarMessage = "";
		SisID.snackbarStatus = "";
		SisID.snackbarCounter = 0;
		SisID.messages = Sys.Helpers.GetSerializedObjectFromVariable("SisidIBANResults", []);
		SisID.RefreshAll();
		if (Data.GetActionName() === "recheckIban" && SisID.snackbarMessage !== "")
		{
			Popup.Snackbar(
			{
				message: Language.Translate(SisID.snackbarMessage, true, SisID.snackbarCounter),
				status: SisID.snackbarStatus ? SisID.snackbarStatus : "info"
			});
			SisID.snackbarMessage = "";
			SisID.snackbarStatus = "";
			SisID.snackbarCounter = 0;
		}
	},
	IsEnabled: function ()
	{
		// Only available for approvers when option activated
		var currentRole = workflowController.GetRoleAt(workflowController.GetContributorIndex());
		return Sys.Parameters.GetInstance("AP").GetParameter("EnableSiSid", "0") === "1" && currentRole === WorkflowParameters.roles.approver;
	},
	GetMessage: function (iban, score)
	{
		var IBANMessages = [];
		for (var i = 0; i < SisID.messages.length; i++)
		{
			if (SisID.messages[i].IBAN === iban && SisID.messages[i].score === score)
			{
				var messageCode = SisID.messages[i].messageCode;
				if (messageCode)
				{
					var messageCodeValue = SisID.messages[i].message;
					for (var j = 0; j < messageCode.length; j++)
					{
						IBANMessages.push("SisID: " + messageCodeValue["reason.code." + messageCode[j]]);
					}
					return IBANMessages.join("\n");
				}
			}
		}
		return "";
	},
	Refresh: function (row)
	{
		if (row && !User.isVendor)
		{
			var ibanControl = row.BankIBAN__;
			var score = row.BankIBANScore__.GetValue();
			if (score || score === 0)
			{
				/**
				 * Display iban verification button only if form is not in final state and bank detail has a SisID result in warning or error
				 * and keep it if it was already displayed
				 */
				Controls.IBANVerification__.Hide((ProcessInstance.isReadOnly || score >= 90 || !SisID.IsEnabled()) && !Controls.IBANVerification__.IsVisible());
				var fieldName = ibanControl.GetName();
				var item = row.GetItem();
				var message = SisID.GetMessage(ibanControl.GetValue(), score);
				if (score <= Sys.Helpers.Sis_ID.defaultScoreLimits.error && !SisID.ShouldTreatSisidErrorAsWarning())
				{
					AddError(fieldName, message, item);
					SisID.hasError = true;
					if (SisID.snackbarStatus !== "error")
					{
						SisID.snackbarCounter = 0;
					}
					SisID.snackbarMessage = "_SisIDSnackbarError";
					SisID.snackbarStatus = "error";
					SisID.snackbarCounter++;
				}
				else if (score <= Sys.Helpers.Sis_ID.defaultScoreLimits.warning ||
					(score <= Sys.Helpers.Sis_ID.defaultScoreLimits.error && SisID.ShouldTreatSisidErrorAsWarning()))
				{
					AddWarning(fieldName, message, item);
					// Set snackbar in warning only if not already in error
					if (SisID.snackbarStatus !== "error")
					{
						if (SisID.snackbarStatus !== "warning")
						{
							SisID.snackbarCounter = 0;
						}
						SisID.snackbarMessage = "_SisIDSnackbarWarning";
						SisID.snackbarStatus = "warning";
						SisID.snackbarCounter++;
					}
				}
				else
				{
					AddInfo(fieldName, message, item);
					// Set snackbar in success only if not already in warning or error
					if (SisID.snackbarStatus !== "error" && SisID.snackbarStatus !== "warning")
					{
						if (SisID.snackbarStatus !== "success")
						{
							SisID.snackbarCounter = 0;
						}
						SisID.snackbarMessage = "_SisIDSnackbarSuccess";
						SisID.snackbarStatus = "success";
						SisID.snackbarCounter++;
					}
				}
			}
			else
			{
				// Display iban verification button only if form is not in final state and SisID option is enabled, and keep it if it was already displayed
				Controls.IBANVerification__.Hide((ProcessInstance.isReadOnly || !SisID.IsEnabled()) && !Controls.IBANVerification__.IsVisible());
			}
		}
	},
	RefreshAll: function ()
	{
		if (!User.isVendor)
		{
			var bankDetailsControl = Controls.CompanyBankAccountsTable__;
			SisID.hasError = false;
			for (var i = 0; i < bankDetailsControl.GetItemCount(); i++)
			{
				SisID.Refresh(bankDetailsControl.GetRow(i));
			}
		}
	},
	/**
	 * Threats all Sis id error set on IBAN Fields as warning, removing the sid is error and adding it as a warning.
	 * Keep all other errors.
	 */
	SetAllSisidErrorsAsWarnings: function ()
	{
		var bankDetailsControl = Controls.CompanyBankAccountsTable__;
		for (var i = 0; i < bankDetailsControl.GetLineCount(); i++)
		{
			var row = bankDetailsControl.GetRow(i);
			if (row)
			{
				var ibanControl = row.BankIBAN__;
				var score = row.BankIBANScore__.GetValue();
				if (score || score === 0)
				{
					var message = SisID.GetMessage(ibanControl.GetValue(), score);
					if (score <= Sys.Helpers.Sis_ID.defaultScoreLimits.error)
					{
						if (SisID.SetSisidErrorAsWarning(ibanControl.GetName(), message, row.GetItem()))
						{
							ibanControl.RemoveStyle(highlightStyles.error);
						}
						ibanControl.AddStyle(highlightStyles.warning);
					}
				}
			}
		}
		SisID.hasError = false;
	},
	/**
	 * remove a string from another
	 * @param message the string to clean
	 * @param sisIdMessage the string to remove
	 */
	RemoveSisIdMessage: function (message, sisIdMessage)
	{
		if (!message || message === sisIdMessage)
		{
			return "";
		}
		var regex = new RegExp("(" + sisIdMessage + "\\n)|(\\n" + sisIdMessage + ")", "gm");
		return message.replace(regex, "");
	},
	SetSisidErrorAsWarning: function (fieldName, sisIdMessage, item)
	{
		var errorMessage = SisID.RemoveSisIdMessage(item.GetError(fieldName), sisIdMessage);
		if (errorMessage === Language.Translate("_One or more vendors with the same informations are already registered"))
		{
			sisIdMessage = errorMessage + "\n" + sisIdMessage;
			errorMessage = "";
		}
		item.SetError(fieldName, errorMessage);
		AddWarning(fieldName, sisIdMessage, item);
		return !errorMessage;
	},
	ShouldTreatSisidErrorAsWarning: function ()
	{
		var SisidErrorAsWarningValue = Variable.GetValueAsString("SisidErrorAsWarning");
		return SisidErrorAsWarningValue.toLowerCase() === "true" || SisidErrorAsWarningValue === "1";
	},
	EnableSisidErrorAsWarning: function (enable)
	{
		Variable.SetValueAsString("SisidErrorAsWarning", enable);
	},
	HandleIbanToolTip: function ()
	{
		if (!User.isVendor)
		{
			var bankDetailsControl = Controls.CompanyBankAccountsTable__;
			for (var i = 0; i < bankDetailsControl.GetLineCount(); i++)
			{
				var row = Controls.CompanyBankAccountsTable__.GetRow(i);
				var ibanItem = row.GetItem();
				var ibanControl = row.BankIBAN__;
				if (ibanItem)
				{
					var info = ibanItem.GetInfo("BankIBAN__");
					var warning = ibanItem.GetWarning("BankIBAN__");
					var error = ibanItem.GetError("BankIBAN__");
					if (error)
					{
						if (error.indexOf(warning) === -1 && error.indexOf(info) === -1)
						{
							ibanItem.SetError("BankIBAN__", "" + (info ? info + "\n" : "") + (warning ? warning + "\n" : "") + error);
						}
						ibanControl.RemoveStyle(highlightStyles.success);
						ibanControl.RemoveStyle(highlightStyles.warning);
						ibanControl.AddStyle(highlightStyles.error);
					}
					else if (warning)
					{
						if (warning.indexOf(info) === -1)
						{
							ibanItem.SetWarning("BankIBAN__", "" + (info ? info + "\n" : "") + warning);
						}
						ibanControl.RemoveStyle(highlightStyles.success);
						ibanControl.AddStyle(highlightStyles.warning);
						ibanControl.RemoveStyle(highlightStyles.error);
					}
					else if (info)
					{
						ibanItem.SetInfo("BankIBAN__", info);
						ibanControl.AddStyle(highlightStyles.success);
						ibanControl.RemoveStyle(highlightStyles.warning);
						ibanControl.RemoveStyle(highlightStyles.error);
					}
				}
			}
		}
	},
	ResetIbanStatus: function (row, ibanItem)
	{
		if (row && ibanItem && !User.isVendor)
		{
			var ibanControl = row.BankIBAN__;
			if (ibanItem)
			{
				ibanControl.RemoveStyle(highlightStyles.success);
				ibanControl.RemoveStyle(highlightStyles.warning);
				ibanControl.RemoveStyle(highlightStyles.error);
			}
		}
	}
};
var IBAN = {
	Validate: function (item)
	{
		var errorCount = 0;
		if (item && !BankAccountsManagement.IsItemEmpty(item))
		{
			var country = item.GetValue("BankCountry__");
			var notVerifiedBySisID = item.IsNullOrEmpty("BankIBANScore__");
			var checkIban = Sys.Helpers.Iban.IsIbanCountry(country);
			if (checkIban && notVerifiedBySisID)
			{
				item.SetError("BankIBAN__", "");
				item.SetValue("RoutingCode__", "");
				var isValidIBAN = Sys.Helpers.Iban.IsValid(item.GetValue("BankIBAN__"), country);
				if (!isValidIBAN)
				{
					var errorMsg = item.IsNullOrEmpty("BankIBAN__") ? "This field is required!" : "_invalid iban format";
					AddError("BankIBAN__", errorMsg, item);
					// Reset fields depending on BankIBAN__
					item.SetValue("BankKey__", "");
					item.SetValue("BankAccountNumber__", "");
					item.SetValue("ControlKey__", "");
					errorCount++;
				}
				else
				{
					// IBAN is valid - fill associated fields
					var bankIdentifiers = void 0;
					bankIdentifiers = Sys.Helpers.Iban.GetBankIdentifiers(item.GetValue("BankIBAN__"), country);
					item.SetValue("BankKey__", bankIdentifiers[0] + bankIdentifiers[1]); // BankCode + BranchCode
					item.SetValue("BankAccountNumber__", bankIdentifiers[2]);
					item.SetValue("ControlKey__", bankIdentifiers[3]);
				}
			}
		}
		return errorCount;
	},
	ValidateAll: function ()
	{
		var bankDetailsTable = Data.GetTable("CompanyBankAccountsTable__");
		for (var i = 0; i < bankDetailsTable.GetItemCount(); i++)
		{
			IBAN.Validate(bankDetailsTable.GetItem(i));
		}
	}
};
var VAT = {
	Validate: function ()
	{
		var withoutErrors = true;
		var country = Data.GetValue("Country__");
		if (Controls.TaxID__.GetValue())
		{
			var valid = true;
			if (Sys.Helpers.VAT.IsVATCountry(country))
			{
				var vatInfo = Sys.Helpers.VAT.CheckVAT(Controls.TaxID__.GetValue());
				valid = vatInfo.isValid;
				if (!valid)
				{
					Controls.TaxID__.SetError("_invalid VAT format");
					withoutErrors = false;
				}
				else if (vatInfo.country.isoCode.short !== country)
				{
					Controls.TaxID__.SetError("_invalid {0} VAT format does not match country {1}", CountryManagement.GetCountryName(vatInfo.country.isoCode.short), CountryManagement.GetCountryName(country));
					withoutErrors = false;
				}
				else
				{
					Controls.TaxID__.SetError("");
				}
			}
			else
			{
				Controls.TaxID__.SetError("");
			}
		}
		return withoutErrors;
	},
	AdaptLabelAndRequire: function ()
	{
		var country = Data.GetValue("Country__");
		if (!country)
		{
			// eslint-disable-next-line no-undef
			var navigatorLanguageSplit = navigator.language.split("-");
			country = navigatorLanguageSplit[navigatorLanguageSplit.length - 1];
			Data.SetValue("Country__", country);
		}
		if (Sys.Helpers.VAT.IsVATCountry(country))
		{
			Controls.TaxID__.SetRequired(true);
			Controls.TaxID__.SetLabel("_TaxID");
		}
		else
		{
			Controls.TaxID__.SetRequired(false);
			Controls.TaxID__.SetLabel("_TaxID_NonVAT");
		}
	}
};
var HighlightHandling = {
	masterDataProcessVariable: "FieldsFromMasterData",
	modifiedFieldsProcessVariable: "ModifiedFields",
	HeaderFieldValueFromMasterData: function (field)
	{
		HighlightHandling.FetchInitialMasterData();
		if (ValuesFromMasterData && ValuesFromMasterData.fields)
		{
			return ValuesFromMasterData.fields[field];
		}
		return "";
	},
	HighlightUpdatedFields: function ()
	{
		var highlightStyle = highlightStyles.warning;
		var modifiedFields = HighlightHandling.FetchModifiedFields();
		if (modifiedFields)
		{
			HighlightHandling.FetchInitialMasterData();
			for (var _i = 0, _a = modifiedFields.fields; _i < _a.length; _i++)
			{
				var field = _a[_i];
				HighlightHandling.HandleHighlightForField(field, highlightStyle, ValuesFromMasterData ? HighlightHandling.HeaderFieldValueFromMasterData(field) : null);
			}
			for (var _b = 0, _c = modifiedFields.tables; _b < _c.length; _b++)
			{
				var table = _c[_b];
				if (ValuesFromMasterData && ValuesFromMasterData.tables[table])
				{
					HighlightHandling.HandleHighlightForTable(table, ValuesFromMasterData.tables[table]);
				}
			}
		}
	},
	HandleHighlightForField: function (fieldName, highlightStyle, oldValue)
	{
		Controls[fieldName].AddStyle(highlightStyle);
		// Prettify the tooltip for the combo controls.
		if (oldValue)
		{
			if (fieldName === "Country__")
			{
				oldValue = CountryManagement.GetCountryName(oldValue);
			}
			else if (Controls[fieldName].GetType() === "ComboBox")
			{
				oldValue = Language.Translate(oldValue, false);
			}
		}
		Controls[fieldName].SetInfo(Language.Translate("_PreviousValueIs") + " " + (oldValue || Language.Translate("_NoValue")));
	},
	HandleHighlightForTable: function (tableName, initTableValues)
	{
		function stringifyTableLine(line)
		{
			// Pretty-print the bank account country
			if (line.BankCountry__)
			{
				line.BankCountry__ = CountryManagement.GetCountryName(line.BankCountry__);
			}
			return Controls[tableName]
				.GetColumnsOrder()
				.map(function (col)
				{
					return Language.Translate(Controls[tableName][col].GetLabel()) + ": <b>" + (line[col] || Language.Translate("_NoValue")) + "</b>";
				})
				.join(", ");
		}
		// Hard-coded controls name map for tables
		var tooltipTableMapping = {
			"CompanyOfficersTable__": "CompanyOfficersTableMessage__",
			"CompanyBankAccountsTable__": "CompanyBankAccountsTableMessage__",
			"DocumentsTable__": "DocumentsTableMessage__"
		};
		var tooltipControl = Controls[tooltipTableMapping[tableName]];
		// Create tooltip HTML value
		var tooltip = "";
		if (tableName === "DocumentsTable__")
		{
			// Specific case for documents
			if (Attach.GetNbAttach() > 0)
			{
				tooltip = "<b>" + Language.Translate("_{0}NewDocuments", undefined, Attach.GetNbAttach()) + "</b>";
			}
		}
		else
		{
			var diff = HighlightHandling.GetTableDiff(tableName, initTableValues);
			if (diff.deletedLines.length)
			{
				tooltip += Language.Translate("_PreviousLines");
				tooltip += "<br/><del>" + diff.deletedLines.map(function (line)
				{
					return stringifyTableLine(line);
				}).join("<br/>");
				tooltip += "</del><br/>";
			}
			if (diff.newLines.length)
			{
				tooltip += Language.Translate("_NewLines");
				tooltip += "<br/>" + diff.newLines.map(function (line)
				{
					return stringifyTableLine(line);
				}).join("<br/>");
				tooltip += "<br/>";
			}
		}
		if (tooltipControl && tooltip)
		{
			tooltipControl.Hide(false);
			tooltipControl.SetHTML(tooltip);
		}
	},
	GetTableDiff: function (tableName, initTableValues)
	{
		function lineEquals(line1, line2)
		{
			return !Object.keys(line1).some(function (column)
			{
				return (line1[column] || "") !== (line2[column] || "");
			});
		}
		// Returns elements which exists in the first array but not in the second one
		function existsOnlyIntheFirstTable(table1, table2)
		{
			var lines = [];
			var _loop_1 = function (line1)
			{
				// Check if the line is present in the second table
				if (!table2.some(function (line2)
					{
						return lineEquals(line1, line2);
					}))
				{
					lines.push(line1);
				}
			};
			for (var _i = 0, table1_1 = table1; _i < table1_1.length; _i++)
			{
				var line1 = table1_1[_i];
				_loop_1(line1);
			}
			return lines;
		}
		var currentTableValues = FlexibleFormToJSON.GetTable(tableName);
		var diffStructure = {
			newLines: [],
			deletedLines: []
		};
		diffStructure.newLines = existsOnlyIntheFirstTable(currentTableValues, initTableValues);
		diffStructure.deletedLines = existsOnlyIntheFirstTable(initTableValues, currentTableValues);
		return diffStructure;
	},
	FetchInitialMasterData: function ()
	{
		if (!ValuesFromMasterData)
		{
			try
			{
				var struct = Variable.GetValueAsString(HighlightHandling.masterDataProcessVariable);
				ValuesFromMasterData = JSON.parse(struct).flexible;
			}
			catch (ex)
			{
				Log.Warn("Unable to fetch initial master data");
			}
		}
	},
	FetchModifiedFields: function ()
	{
		try
		{
			var struct = Variable.GetValueAsString(HighlightHandling.modifiedFieldsProcessVariable);
			return JSON.parse(struct);
		}
		catch (ex)
		{
			Log.Warn("Unable to fetch the list of modified fields");
		}
		return null;
	},
	ResetAllTooltips: function ()
	{
		Object.keys(Controls).forEach(function (control)
		{
			if (Controls[control] && typeof Controls[control].SetInfo === "function")
			{
				Controls[control].SetInfo(null);
			}
		});
	}
};
var FlexibleFormToJSON = {
	GetTable: function (tableName)
	{
		var table = Data.GetTable(tableName);
		var tableObj = [];
		// Adds each item
		for (var itemIdx = 0; itemIdx < table.GetItemCount(); itemIdx++)
		{
			var item = table.GetItem(itemIdx);
			var itemObj = {};
			var lineEmpty = true;
			var columns = Controls[tableName].GetColumnsOrder();
			for (var _i = 0, columns_1 = columns; _i < columns_1.length; _i++)
			{
				var column = columns_1[_i];
				itemObj[column] = item.GetValue(column);
				if (lineEmpty && itemObj[column])
				{
					lineEmpty = false;
				}
			}
			if (!lineEmpty)
			{
				tableObj.push(itemObj);
			}
		}
		return tableObj;
	}
};

function IsLastApprover()
{
	return !workflowController.GetContributorAt(workflowController.GetContributorIndex() + 1);
}

function InitWorkflow()
{
	// Creates and initializes the workflow.
	workflowController = Sys.WorkflowController.Create(Data, Variable, Language, Controls);
	workflowController.Define(WorkflowParameters);
	// Defines the role sequence.
	workflowController.SetRolesSequence(["requester", "vendor", "approver"]);
	// Allow workflow rebuild only if we're on the first contributor (the vendor)
	workflowController.AllowRebuild(true);
}

function InitEvents()
{
	// Defines what must be done when the user clicks the form buttons.
	// In this sample, the 'Submit' action name executes the default contributor action.
	Controls.SubmitToNext.OnClick = function ()
	{
		var currentRole = workflowController.GetRoleAt(workflowController.GetContributorIndex());
		if (currentRole === WorkflowParameters.roles.LegalReviewer)
		{
			HandleActions.ApproveWithPopupCommentDialog();
		}
		else
		{
			HandleActions.Approve();
		}
	};
	Controls.Approve.OnClick = function ()
	{
		var currentRole = workflowController.GetRoleAt(workflowController.GetContributorIndex());
		if (currentRole === WorkflowParameters.roles.LegalReviewer)
		{
			HandleActions.ApproveWithPopupCommentDialog();
		}
		else
		{
			HandleActions.Approve();
		}
	};
	Controls.BackToPrevious.OnClick = function ()
	{
		ProcessInstance.Approve("BackToPrevious");
	};
	Controls.Reject.OnClick = function ()
	{
		HandleActions.Reject();
	};
	Controls.BackToVendor.OnClick = function ()
	{
		HandleActions.BackToVendor();
	};
	// Header fields
	Controls.TaxID__.OnChange = function ()
	{
		VAT.Validate();
		// Display iban verification button only if form is not in final state and SisID option is enabled, and keep it if it was already displayed
		Controls.IBANVerification__.Hide((ProcessInstance.isReadOnly || !SisID.IsEnabled()) && !Controls.IBANVerification__.IsVisible());
	};
	Controls.ComboState__.OnChange = function ()
	{
		Controls.Mail_State__.SetValue(this.GetSelectedOption());
	};
	Controls.Country__.OnChange = function ()
	{
		VAT.AdaptLabelAndRequire();
		CountryManagement.CountryChange(this);
	};
	// Officers table
	Controls.CompanyOfficersTable__.OnAddItem = function ( /*item: Item, tableIndex: number*/ )
	{
		Layout.AdaptCompanyOfficersTableAvailableActions();
	};
	Controls.CompanyOfficersTable__.OnDeleteItem = function ( /*item: Item, tableIndex: number*/ )
	{
		Layout.AdaptCompanyOfficersTableAvailableActions();
	};
	Controls.CompanyOfficersTable__.OnRefreshRow = function ( /*item: Item, tableIndex: number*/ )
	{
		Layout.AdaptCompanyOfficersTableAvailableActions();
	};
	// Bank details table
	Controls.CompanyBankAccountsTable__.OnAddItem = function (item, tableIndex)
	{
		var country = Controls.Country__.GetValue();
		item.SetValue("BankCountry__", country);
		BankAccountsManagement.SetCurrency(item, country);
		Layout.AdaptBankAccountsTableAvailableActions();
		Controls.CompanyBankAccountsTable__.OnRefreshRow(tableIndex);
	};
	Controls.CompanyBankAccountsTable__.OnDeleteItem = function (item, tableIndex)
	{
		Layout.AdaptBankAccountsTableAvailableActions();
		Controls.CompanyBankAccountsTable__.OnRefreshRow(tableIndex);
	};
	Controls.CompanyBankAccountsTable__.OnRefreshRow = function ( /*index: number*/ )
	{
		BankAccountsManagement.SetAllReadOnlyAndRequiredFields();
		SisID.RefreshAll();
		SisID.HandleIbanToolTip();
	};
	Controls.CompanyBankAccountsTable__.BankCountry__.OnChange = function ()
	{
		var item = this.GetItem();
		var row = this.GetRow();
		SisID.ResetIbanStatus(row, item);
		BankAccountsManagement.SetCurrency(item, item.GetValue("BankCountry__"));
		BankAccountsManagement.EmptyFields(item);
		BankAccountsManagement.SetReadOnlyAndRequiredFields(item, row);
		SisID.Refresh(row);
		IBAN.Validate(item);
	};
	Controls.CompanyBankAccountsTable__.BankAccountHolder__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.BankIBAN__.OnChange = function ()
	{
		var item = this.GetItem();
		var row = this.GetRow();
		SisID.ResetIbanStatus(row, item);
		item.SetValue("BankIBANScore__", null);
		BankAccountsManagement.SetReadOnlyAndRequiredFields(item, row);
		SisID.Refresh(row);
		IBAN.Validate(item);
	};
	Controls.CompanyBankAccountsTable__.BankKey__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.BankAccountNumber__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.ControlKey__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.SWIFT_BICCode__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.RoutingCode__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.CompanyBankAccountsTable__.Currency__.OnChange = function ()
	{
		BankAccountsManagement.SetReadOnlyAndRequiredFields(this.GetItem(), this.GetRow());
	};
	Controls.IBANVerification__.OnClick = function ()
	{
		Controls.CompanyCode__.SetRequired(false);
		Controls.VendorNumber__.SetRequired(false);
		ProcessInstance.Approve("recheckIban");
	};
	// Form title HTML control
	var titleState = {
		"PendingInternalInfo":
		{
			label: Language.Translate("_PendingInternalInfo"),
			imageUrl: "url('" + Process.GetImageURL("being_process.png") + "')"
		},
		"PendingVendorInfo":
		{
			label: Language.Translate("_PendingVendorInfo"),
			imageUrl: "url('" + Process.GetImageURL("being_process.png") + "')"
		},
		"Approved":
		{
			label: Language.Translate("_Approved"),
			imageUrl: "url('" + Process.GetImageURL("approval_green.png") + "')"
		},
		"ToValidate":
		{
			label: Language.Translate("_ToValidate"),
			imageUrl: "url('" + Process.GetImageURL("warning_yellow.png") + "')"
		},
		"Rejected":
		{
			label: Language.Translate("_Denied"),
			imageUrl: "url('" + Process.GetImageURL("warning_red.png") + "')"
		}
	};
	Controls.Title__.BindEvent("onTitleLoad", function ()
	{
		var status = Data.GetValue("ProcessStatus__");
		var statusToDisplay = null;
		if (!User.isVendor)
		{
			switch (status)
			{
			case "PendingVendorInfo":
				{
					statusToDisplay = titleState.PendingVendorInfo;
					break;
				}
			case "Approved":
				{
					statusToDisplay = titleState.Approved;
					break;
				}
			case "ToValidate":
				{
					var isUserTheOwner = User.loginId === Data.GetValue("OwnerId") || User.IsMemberOf(Data.GetValue("OwnerId"));
					statusToDisplay = isUserTheOwner ? titleState.ToValidate : titleState.PendingInternalInfo;
					break;
				}
			case "Rejected":
				{
					statusToDisplay = titleState.Rejected;
					break;
				}
			default:
				{
					break;
				}
			}
		}
		var type = Data.GetValue("RegistrationType__");
		var title;
		if (type === "registration")
		{
			title = Language.Translate("_TitlePane");
		}
		else if (User.isVendor)
		{
			title = Language.Translate("_UpdateTitlePaneVendorSide");
		}
		else
		{
			title = Language.Translate("_UpdateTitlePane");
		}
		Controls.Title__.FireEvent("displayTitle",
		{
			title: title,
			status: statusToDisplay
		});
	});
}

function isVendorGuest()
{
	return User.profileRole.toLowerCase() === "guest";
}

function InitUIForVendor()
{
	ResetRegistrationWarnings();
	ResetIbanVerificationMessage();
	Process.ShowFirstErrorAfterBoot(false);
	Layout.ForVendor();
	if (isVendorGuest())
	{
		var quitBtn = Controls.form_header.GetActionControl("quit");
		if (quitBtn)
		{
			quitBtn.OnClick = function ()
			{
				User.Logout();
			};
		}
		Controls.Close.OnClick = function ()
		{
			User.Logout();
		};
	}
	else if (Variable.GetValueAsString("AlreadyPendingRegistrationUpdate") === "true")
	{
		// Show an error popup and leave the form
		Popup.Alert("_ErrorMessageDuplicateRegistrationUpdate", false, function ()
		{
			ProcessInstance.Quit("Quit");
		}, "_ErrorTitleDuplicateRegistrationUpdate");
	}
	Wizard.Init();
}

function InitUIForManagement()
{
	ResetRegistrationWarnings();
	if (Data.GetValue("State") < 100)
	{
		UpdateCommentPane();
	}
	Layout.ForManagement();
	Controls.Close.OnClick = function ()
	{
		ProcessInstance.Quit();
	};
	UpdateWorkflowLayout();
}

function ValidateRequiredFields()
{
	Controls.Company__.Focus();
	Controls.Street__.Focus();
	Controls.Zip_Code__.Focus();
	Controls.City__.Focus();
	Controls.Phone_Number__.Focus();
	Controls.YearsInBusiness__.Focus();
	Controls.NumberOfEmployees__.Focus();
	Controls.TaxID__.Focus();
	Controls.CompanyCode__.Focus();
	Controls.VendorNumber__.Focus();
}

function UpdateWorkflowLayout()
{
	var approversTableCtrl = Controls.Workflow__;
	// Make sure that at least the last 4 steps and the current step is displayed by default
	// and that following steps are displayed (upon to 5)
	var tIdx1 = workflowController.GetTableIndex() - 4;
	tIdx1 = tIdx1 < 0 ? 0 : tIdx1;
	var tIdx2 = approversTableCtrl.GetItemCount() - 10;
	if (approversTableCtrl.GetItemCount() >= 10)
	{
		approversTableCtrl.DisplayItem(approversTableCtrl.GetItemCount() - 1);
		approversTableCtrl.DisplayItem(tIdx1 < tIdx2 ? tIdx1 : tIdx2);
	}
}

function UpdateCommentPane()
{
	Controls.CommentContent__.SetHTML("");
	DisplayOFACResults();
	DisplayDuplicateWarning();
}

function AddHtml(control, html, prepend)
{
	if (prepend === void 0)
	{
		prepend = false;
	}
	var first = prepend ? html : control.GetHTML();
	var second = prepend ? control.GetHTML() : html;
	var separator = first ? "</br></br>" : "";
	control.SetHTML("" + first + separator + second);
}

function AddInfo(field, text, control)
{
	if (control === void 0)
	{
		control = Data;
	}
	var old = control.GetInfo(field);
	if (!old)
	{
		old = text;
	}
	else if (old.indexOf(text) === -1)
	{
		old = old + "\n" + text;
	}
	control.SetInfo(field, old);
}

function AddWarning(field, text, control)
{
	if (control === void 0)
	{
		control = Data;
	}
	var old = control.GetWarning(field);
	if (!old)
	{
		old = text;
	}
	else if (old.indexOf(text) === -1)
	{
		old = old + "\n" + text;
	}
	control.SetWarning(field, old);
}

function AddError(field, text, control)
{
	if (control === void 0)
	{
		control = Data;
	}
	var old = control.GetError(field);
	if (!old)
	{
		old = text;
	}
	else if (old.indexOf(text) === -1)
	{
		old = old + "\n" + text;
	}
	control.SetError(field, old);
}

function DisplayOFACResults()
{
	var OFACResults = Lib.VendorRegistration.CheckOFAC.GetLastOFACResults();
	if (OFACResults.length)
	{
		var bannerWarningMessageHTML = getOFACBannerMessageHTML(OFACResults);
		AddHtml(Controls.CommentContent__, bannerWarningMessageHTML, true);
		Controls.CommentPane.Hide(false);
		DisplayOFACWarnings();
	}
}

function getOFACBannerMessageHTML(OFACResults)
{
	var retStr = Language.Translate("_OFACSanctionsListsHits", false);
	retStr += "<br><div class=\"Link\">";
	retStr += Lib.VendorRegistration.CheckOFAC.FormatOFACDisplay(OFACResults, true);
	retStr += Language.Translate("_OFACWebsite", false);
	retStr += "<a id = \"OFACWebSite\" class=\"nav-link\" onclick=\"window.open('" + Sys.OFAC.Common.OFACGeneralInfoURL + "');\" >";
	retStr += " " + Sys.OFAC.Common.OFACGeneralInfoURL;
	retStr += "</a></div>";
	return retStr;
}

function DisplayOFACWarnings()
{
	var OFACWarnings = Lib.VendorRegistration.CheckOFAC.GetLastOFACWarnings();
	if (OFACWarnings && Object.keys(OFACWarnings).length > 0)
	{
		var trad = Sys.OFAC.Common.OFAC_RESULT_WARNING_MESSAGE;
		for (var _i = 0, _a = OFACWarnings.fields; _i < _a.length; _i++)
		{
			var prop = _a[_i];
			AddWarning(prop, trad);
		}
		for (var _b = 0, _c = OFACWarnings.tables; _b < _c.length; _b++)
		{
			var table = _c[_b];
			AddWarning(table.column, trad, Data.GetTable(table.name).GetItem(table.line));
		}
	}
}

function DisplayDuplicateWarning()
{
	var vendorsDuplicates = Sys.Helpers.GetSerializedObjectFromVariable(Lib.VendorRegistration.CheckDuplicateVendor.KEY_VENDORS_DUPLICATES, null);
	if (vendorsDuplicates)
	{
		var bannerWarningMessageHTML = getVendorBannerMessageHTML(vendorsDuplicates.vendorsInformations);
		AddHtml(Controls.CommentContent__, bannerWarningMessageHTML);
		Controls.CommentPane.Hide(false);
		var warningMessage = Language.Translate(warningMessageTradKey, false);
		DisplayRegistrationDuplicatesWarnings(vendorsDuplicates, warningMessage);
	}
	else
	{
		var pendingRegistrationDuplicates = Sys.Helpers.GetSerializedObjectFromVariable(Lib.VendorRegistration.CheckDuplicateVendor.KEY_PENDING_REGISTRATION_DUPLICATES, null);
		if (pendingRegistrationDuplicates)
		{
			var bannerWarningMessageHTML = Language.Translate("_A pending registration already exists with the same information", false);
			AddHtml(Controls.CommentContent__, bannerWarningMessageHTML);
			Controls.CommentPane.Hide(false);
			var warningMessage = Language.Translate(warningMessageTradKey, false);
			DisplayRegistrationDuplicatesWarnings(pendingRegistrationDuplicates, warningMessage);
		}
	}
}

function getVendorBannerMessageHTML(vendorsInformations)
{
	var bannerWarningMessageHTML = Language.Translate(warningMessageTradKey, false);
	bannerWarningMessageHTML += "<br/>";
	bannerWarningMessageHTML += Language.Translate("_You can consult duplicate vendors informations below", false);
	for (var _i = 0, vendorsInformations_1 = vendorsInformations; _i < vendorsInformations_1.length; _i++)
	{
		var informations = vendorsInformations_1[_i];
		bannerWarningMessageHTML += "<br/>&#32;&#45;&#32;";
		bannerWarningMessageHTML += Language.Translate("_Company code: {0}, Vendor number: {1}", false, informations.companyCode, informations.vendorNumber);
	}
	return bannerWarningMessageHTML;
}

function DisplayWarningsOnProperties(propertyNames, warningMessage)
{
	for (var _i = 0, propertyNames_1 = propertyNames; _i < propertyNames_1.length; _i++)
	{
		var propertyName = propertyNames_1[_i];
		AddWarning(propertyName, warningMessage);
	}
}

function DisplayWarningsOnTableColums(tables, warningMessage)
{
	for (var _i = 0, tables_1 = tables; _i < tables_1.length; _i++)
	{
		var duplicateTable = tables_1[_i];
		var table = Data.GetTable(duplicateTable.name);
		for (var itemCounter = 0; itemCounter < table.GetItemCount(); itemCounter++)
		{
			var rowItem = table.GetItem(itemCounter);
			for (var _a = 0, _b = duplicateTable.duplicates; _a < _b.length; _a++)
			{
				var duplicate = _b[_a];
				var columnValue = rowItem.GetValue(duplicate.column);
				if (columnValue && duplicate.value.toLowerCase() === columnValue.toLowerCase())
				{
					AddWarning(duplicate.column, warningMessage, rowItem);
					break;
				}
			}
		}
	}
}

function DisplayRegistrationDuplicatesWarnings(registrationDuplicates, warningMessage)
{
	DisplayWarningsOnProperties(registrationDuplicates.propertiesNames, warningMessage);
	DisplayWarningsOnTableColums(registrationDuplicates.tables, warningMessage);
}

function ResetRegistrationWarningsTable(tableName, dataToCheck)
{
	var table = Data.GetTable(tableName);
	for (var i = 0; i < table.GetItemCount(); i++)
	{
		var item = table.GetItem(i);
		if (item)
		{
			for (var column in dataToCheck[tableName])
			{
				if (
					{}.hasOwnProperty.call(dataToCheck[tableName], column))
				{
					item.SetWarning(column, null);
				}
			}
		}
	}
}

function ResetRegistrationWarnings()
{
	var dataToCheck = Sys.Helpers.GetSerializedObjectFromVariable(Lib.VendorRegistration.CheckDuplicateVendor.KEY_PENDING_REGISTRATION_TO_CHECK, null);
	dataToCheck = Lib.VendorRegistration.CheckOFAC.ExtendRegistrationWarningFields(dataToCheck);
	for (var prop in dataToCheck)
	{
		if (typeof dataToCheck[prop] === "string")
		{
			Data.SetWarning(prop, null);
		}
		else if (typeof dataToCheck[prop] === "object")
		{
			ResetRegistrationWarningsTable(prop, dataToCheck);
		}
	}
}

function ResetIbanVerificationMessage()
{
	var bankDetailsControl = Controls.CompanyBankAccountsTable__;
	for (var i = 0; i < bankDetailsControl.GetLineCount(); i++)
	{
		var row = bankDetailsControl.GetRow(i);
		var item = row.GetItem();
		if (item)
		{
			item.SetInfo("BankIBAN__", "");
			item.SetWarning("BankIBAN__", "");
			item.SetError("BankIBAN__", "");
		}
		var ibanControl = row.BankIBAN__;
		ibanControl.RemoveStyle(highlightStyles.error);
		ibanControl.RemoveStyle(highlightStyles.warning);
		ibanControl.RemoveStyle(highlightStyles.success);
	}
}

function ValidateForm()
{
	BankAccountsManagement.SetAllReadOnlyAndRequiredFields();
	IBAN.ValidateAll();
	VAT.Validate();
}

function initUI()
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		if (User.isVendor)
		{
			InitUIForVendor();
		}
		else
		{
			InitUIForManagement();
			var sisIdEnabled = SisID.IsEnabled();
			if (sisIdEnabled)
			{
				SisID.Init();
				SisID.HandleIbanToolTip();
			}
			if (Data.GetValue("RegistrationType__") === "update")
			{
				HighlightHandling.HighlightUpdatedFields();
			}
		}
		ValidateForm();
		resolve();
	});
}

function Main()
{
	InitWorkflow();
	InitEvents();
	Layout.General();
	return CountryManagement.Init()
		.Then(initUI);
}
Main();