{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"Global_Parameters_Pane": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": "300",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Global parameters pane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"hidden": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Activate_Document_type_guessing__": "LabelActivate_Document_type_guessing__",
																	"LabelActivate_Document_type_guessing__": "Activate_Document_type_guessing__",
																	"Document_type_guessing__": "LabelDocument_type_guessing__",
																	"LabelDocument_type_guessing__": "Document_type_guessing__",
																	"Document_type_guessing_classifier__": "LabelDocument_type_guessing_classifier__",
																	"LabelDocument_type_guessing_classifier__": "Document_type_guessing_classifier__",
																	"setbillinginfoondeliveries__": "Labelsetbillinginfoondeliveries__",
																	"Labelsetbillinginfoondeliveries__": "setbillinginfoondeliveries__",
																	"DeleteAIData__": "LabelDeleteAIData__",
																	"LabelDeleteAIData__": "DeleteAIData__",
																	"ActivateAutolearnForEmailBody__": "LabelActivateAutolearnForEmailBody__",
																	"LabelActivateAutolearnForEmailBody__": "ActivateAutolearnForEmailBody__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 6,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Activate_Document_type_guessing__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelActivate_Document_type_guessing__": {
																				"line": 1,
																				"column": 1
																			},
																			"Document_type_guessing__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDocument_type_guessing__": {
																				"line": 3,
																				"column": 1
																			},
																			"Document_type_guessing_classifier__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDocument_type_guessing_classifier__": {
																				"line": 2,
																				"column": 1
																			},
																			"setbillinginfoondeliveries__": {
																				"line": 4,
																				"column": 2
																			},
																			"Labelsetbillinginfoondeliveries__": {
																				"line": 4,
																				"column": 1
																			},
																			"DeleteAIData__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDeleteAIData__": {
																				"line": 6,
																				"column": 1
																			},
																			"ActivateAutolearnForEmailBody__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelActivateAutolearnForEmailBody__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelActivate_Document_type_guessing__": {
																			"type": "Label",
																			"data": [
																				"Activate_Document_type_guessing__"
																			],
																			"options": {
																				"label": "Activate Document type guessing",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"Activate_Document_type_guessing__": {
																			"type": "CheckBox",
																			"data": [
																				"Activate_Document_type_guessing__"
																			],
																			"options": {
																				"label": "Activate Document type guessing",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"helpText": "_Enable AI-based document type recognition help",
																				"helpURL": "7023",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right"
																			},
																			"stamp": 57
																		},
																		"LabelDocument_type_guessing_classifier__": {
																			"type": "Label",
																			"data": [
																				"Document_type_guessing_classifier__"
																			],
																			"options": {
																				"label": "Document type guessing classifier",
																				"version": 0
																			},
																			"stamp": 62
																		},
																		"Document_type_guessing_classifier__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_type_guessing_classifier__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "legacy classifier",
																					"1": "v1alpha1 classifier",
																					"2": "v2alpha1 classifier"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "legacy",
																					"1": "v1alpha1",
																					"2": "v2alpha1"
																				},
																				"label": "Document type guessing classifier",
																				"activable": true,
																				"width": 230,
																				"helpText": "Document type guessing classifier help",
																				"helpURL": "7015",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 63
																		},
																		"LabelDocument_type_guessing__": {
																			"type": "Label",
																			"data": [
																				"Document_type_guessing__"
																			],
																			"options": {
																				"label": "Document type guessing",
																				"version": 0
																			},
																			"stamp": 60
																		},
																		"Document_type_guessing__": {
																			"type": "ComboBox",
																			"data": [
																				"Document_type_guessing__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "From document",
																					"1": "From email"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "DOCUMENT",
																					"1": "EMAIL"
																				},
																				"label": "Document type guessing",
																				"activable": true,
																				"width": 230,
																				"helpText": "Document type guessing help",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 61
																		},
																		"Labelsetbillinginfoondeliveries__": {
																			"type": "Label",
																			"data": [
																				"setbillinginfoondeliveries__"
																			],
																			"options": {
																				"label": "Set billing info on deliveries",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 66
																		},
																		"setbillinginfoondeliveries__": {
																			"type": "CheckBox",
																			"data": [
																				"setbillinginfoondeliveries__"
																			],
																			"options": {
																				"label": "Set billing info on deliveries",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 67
																		},
																		"LabelActivateAutolearnForEmailBody__": {
																			"type": "Label",
																			"data": [
																				"ActivateAutolearnForEmailBody__"
																			],
																			"options": {
																				"label": "_ActivateAutolearnForEmailBody",
																				"hidden": true
																			},
																			"stamp": 70
																		},
																		"ActivateAutolearnForEmailBody__": {
																			"type": "CheckBox",
																			"data": [
																				"ActivateAutolearnForEmailBody__"
																			],
																			"options": {
																				"label": "_ActivateAutolearnForEmailBody",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"hidden": true
																			},
																			"stamp": 71
																		},
																		"LabelDeleteAIData__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": ""
																			},
																			"stamp": 68
																		},
																		"DeleteAIData__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_DeleteAIData",
																				"label": "",
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "default",
																				"nextprocess": {
																					"processName": "Customer_Extended_Delivery_Properties__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"style": 3,
																				"action": "none",
																				"url": ""
																			},
																			"stamp": 69
																		}
																	},
																	"stamp": 7
																}
															},
															"stamp": 8,
															"data": []
														}
													},
													"stamp": 9,
													"data": []
												}
											}
										}
									},
									"stamp": 10,
									"data": []
								}
							},
							"stamp": 11,
							"data": []
						}
					},
					"stamp": 12,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 13,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 14,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 15,
							"data": []
						}
					},
					"stamp": 16,
					"data": []
				}
			},
			"stamp": 17,
			"data": []
		}
	},
	"stamps": 71,
	"data": []
}