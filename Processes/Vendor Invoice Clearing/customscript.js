///#GLOBALS Lib
var BalancingLineItem = {
	isRemovable: function (item)
	{
		return !item.GetValue("Amount__") || item.GetValue("Amount__") === 0;
	}
};
var ButtonsBehavior = {
	init: function ()
	{
		Controls.ClearInvoices.OnClick = ButtonsBehavior.onclickClearInvoices;
	},
	// actions
	onclickClearInvoices: function ()
	{
		ButtonsBehavior.removeEmptyBalancingItems();
		// always return false to let the validation script fire the preventApproval or not
		return false;
	},
	// helpers
	removeEmptyBalancingItems: function ()
	{
		var BalancingItemsTable = Data.GetTable("BalancingLineItems__");
		var i = 0;
		while (i < BalancingItemsTable.GetItemCount())
		{
			var item = BalancingItemsTable.GetItem(i);
			if (BalancingLineItem.isRemovable(item))
			{
				item.Remove();
			}
			else
			{
				i++;
			}
		}
		if (Data.GetTable("LineItems__").GetItemCount() > 0)
		{
			ProcessInstance.Approve("ClearInvoices");
		}
	}
};
var EventHandlers = {
	init: function ()
	{
		Controls.BalancingLineItems__.GLAccount__.OnChange = EventHandlers.onGLAccountChange;
		Controls.BalancingLineItems__.CostCenter__.OnChange = EventHandlers.onCostCenterChange;
		Controls.LineItems__.OnRefreshRow = EventHandlers.onRefreshRow;
		Controls.BalancingLineItems__.OnAddItem = EventHandlers.onAddItem;
		Controls.BalancingLineItems__.OnDeleteItem = EventHandlers.onDeleteItem;
		Controls.BalancingLineItems__.Amount__.OnChange = EventHandlers.onChangeAmount;
		Controls.BalancingLineItems__.TaxCode__.OnChange = EventHandlers.onChangeTaxCode;
		EventHandlers.initGlRow(Controls.BalancingLineItems__.GetItem(0));
	},
	// actions
	onCostCenterChange: function ()
	{
		var item = this.GetItem();
		var param = EventHandlers.getFillCCSAPParameters(item);
		Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, param);
	},
	onGLAccountChange: function ()
	{
		var item = this.GetItem();
		var param = EventHandlers.getFillGLSAPParameters(item);
		Lib.AP.GetInvoiceDocumentLayout().GetAndFillDescriptionFromCode(item, param);
	},
	onRefreshRow: function (index)
	{
		var row = this.GetRow(index);
		row.DocumentNumber__.DisplayAs(
		{
			type: "Link"
		});
	},
	onAddItem: function (item)
	{
		EventHandlers.initGlRow(item);
	},
	onDeleteItem: function ()
	{
		EventHandlers.computeAmount();
	},
	onChangeAmount: function ()
	{
		var item = this.GetItem();
		if (item && item.GetValue("LineType__") === Lib.P2P.LineType.GL && item.GetValue("TaxCode__"))
		{
			Lib.AP.GetInvoiceDocument().GetTaxRate(item, updateItemWithTaxRate, onErrorWithTaxCode, null);
			return;
		}
		EventHandlers.computeAmount();
	},
	onChangeTaxCode: function ()
	{
		var item = this.GetItem();
		if (item.GetValue("TaxCode__"))
		{
			Lib.AP.GetInvoiceDocument().GetTaxRate(item, updateItemWithTaxRate, onErrorWithTaxCode, null);
			return;
		}
		updateItemWithTaxRate(item, 0);
	},
	//helpers
	initGlRow: function (item)
	{
		if (ProcessInstance.isReadOnly)
		{
			var BalancingItemsTable = Data.GetTable("BalancingLineItems__");
			if (BalancingItemsTable.GetItemCount() === 1)
			{
				Controls.GL_lines_adjustment_pane.Hide(BalancingLineItem.isRemovable(BalancingItemsTable.GetItem(0)));
			}
		}
		else
		{
			item.SetValue("LineType__", Lib.P2P.LineType.GL);
			if (!item.GetValue("TaxAmount__"))
			{
				item.SetValue("TaxAmount__", 0.00);
			}
			if (!item.GetValue("Amount__"))
			{
				item.SetValue("Amount__", 0.00);
			}
		}
	},
	computeAmount: function ()
	{
		var balance = 0.0,
			totalAmount = 0.0,
			totalNetAmount = 0.0;
		for (var i = 0; i < Controls.LineItems__.GetItemCount(); i++)
		{
			var item = Controls.LineItems__.GetItem(i);
			totalNetAmount += parseFloat(item.GetValue("NetAmount__"));
			totalAmount += parseFloat(item.GetValue("Amount__"));
		}
		for (var i = 0; i < Controls.BalancingLineItems__.GetItemCount(); i++)
		{
			var item = Controls.BalancingLineItems__.GetItem(i);
			if (item.GetValue("Amount__"))
			{
				totalNetAmount += parseFloat(item.GetValue("Amount__"));
				totalAmount += parseFloat(item.GetValue("Amount__"));
			}
			if (item.GetValue("TaxAmount__"))
			{
				totalAmount += parseFloat(item.GetValue("TaxAmount__"));
			}
		}
		Data.SetValue("InvoiceNetAmount__", totalNetAmount);
		Data.SetValue("InvoiceAmount__", totalAmount);
		var assignedAmount = Controls.AssignedAmount__.GetValue();
		balance = totalAmount - assignedAmount;
		balance = Sys.Helpers.Round(balance, 2);
		Controls.Balance__.SetValue(balance);
		if (balance !== 0.00)
		{
			Controls.Balance__.SetError("");
			Controls.Balance__.SetWarning("_Balance is not null");
		}
		else
		{
			Controls.Balance__.SetError("");
			Controls.Balance__.SetWarning("");
		}
	},
	getGLAccountDescription: function (GLAcctId, callback, companyCode)
	{
		Lib.P2P.Browse.GetGlAccountDescription(callback, companyCode, GLAcctId);
	},
	getFillGLSAPParameters: function (item)
	{
		return {
			value: item.GetValue("GLAccount__"),
			formCodeField: "GLAccount__",
			formDescriptionField: "GLDescription__",
			companyCode: Data.GetValue("CompanyCode__"),
			getFunction: EventHandlers.getGLAccountDescription
		};
	},
	getCostCenterDescriptionFromId: function (CCId, callback, companyCode)
	{
		Lib.P2P.Browse.GetCostCenterDescription(callback, companyCode, CCId);
	},
	getFillCCSAPParameters: function (item)
	{
		return {
			value: item.GetValue("CostCenter__"),
			formCodeField: "CostCenter__",
			formDescriptionField: "CCDescription__",
			companyCode: Data.GetValue("CompanyCode__"),
			getFunction: EventHandlers.getCostCenterDescriptionFromId
		};
	}
};

function updateLayout()
{
	// When table is already filled force to display Document Number as Links
	for (var i = 0; i < Controls.LineItems__.GetLineCount(); ++i)
	{
		setVendorLineLayout(i);
	}
	for (var i = 0; i < Controls.BalancingLineItems__.GetLineCount(); ++i)
	{
		setGLLineLayout(i);
	}
	Controls.LineItems__.DocumentNumber__.OnClick = function ()
	{
		Process.OpenLink(this.GetRow().ValidationURL__.GetValue());
	};
	if (ProcessInstance.isReadOnly)
	{
		Controls.ClearInvoicesComment__.Hide(true);
	}
	else
	{
		Controls.ClearInvoicesComment__.SetPlaceholder(Language.Translate("_Enter your comment..."));
	}
}
/**
 * Query callback
 * @this QueryResult
 */
function getConsignmentDocumentsCallback()
{
	var _this = this;
	var addingLines = false;
	if (!this)
	{
		Log.Error("The request does not return any record, reset AncestorsRuid Variable");
		Variable.SetValueAsString("AncestorsRuid", "");
	}
	else
	{
		var err = this.GetQueryError();
		if (err)
		{
			Log.Error("Query in error : " + err);
			Variable.SetValueAsString("AncestorsRuid", "");
		}
		else if (this.GetRecordsCount() === 0)
		{
			Log.Info("Query does not return any record...");
			Variable.SetValueAsString("AncestorsRuid", "");
		}
		else
		{
			var currentConfiguration = Variable.GetValueAsString("Configuration");
			Controls.LineItems__.SetItemCount(0);
			var firstVendorNumber_1 = this.GetQueryValue("VendorNumber__", 0);
			var firstCompanyCode_1 = this.GetQueryValue("CompanyCode__", 0);
			var firstInvoiceCurrency_1 = this.GetQueryValue("InvoiceCurrency__", 0);
			var firstConfiguration_1 = this.GetQueryValue("Configuration__", 0);
			Data.SetValue("VendorNumber__", firstVendorNumber_1);
			Data.SetValue("CompanyCode__", firstCompanyCode_1);
			Data.SetValue("InvoiceCurrency__", firstInvoiceCurrency_1);
			Variable.SetValueAsString("Configuration", firstConfiguration_1 || "Default");
			addingLines = true;
			if (currentConfiguration !== Variable.GetValueAsString("Configuration"))
			{
				Variable.SetValueAsString("SAPConfiguration", "");
				Sys.Parameters.GetInstance("AP").Reload(Variable.GetValueAsString("Configuration"));
				Sys.Parameters.GetInstance("AP").IsReady(function ()
				{
					initFromConfiguration();
					AddLineItems(_this, firstVendorNumber_1, firstCompanyCode_1, firstInvoiceCurrency_1, firstConfiguration_1);
				});
			}
			else
			{
				AddLineItems(this, firstVendorNumber_1, firstCompanyCode_1, firstInvoiceCurrency_1, firstConfiguration_1);
			}
		}
	}
	if (!addingLines)
	{
		Controls.ERPInvoiceNumber__.Wait(false);
		Controls.LineItems__.GetRow(0).DocumentNumber__.SetError("_No settlement document found");
	}
}

function AddLineItems(queryResult, firstVendorNumber, firstCompanyCode, firstInvoiceCurrency, firstConfiguration)
{
	var validRuidExs = [];
	var settlements = [];
	var fiscalYear = "";
	Controls.LineItems__.SetItemCount(0);
	for (var i = 0; i < queryResult.GetRecordsCount(); ++i)
	{
		var vendorNumber = queryResult.GetQueryValue("VendorNumber__", i);
		var companyCode = queryResult.GetQueryValue("CompanyCode__", i);
		var documentNumber = queryResult.GetQueryValue("ERPInvoiceNumber__", i);
		var invoiceCurrency = queryResult.GetQueryValue("InvoiceCurrency__", i);
		var configuration = queryResult.GetQueryValue("Configuration__", i);
		if (firstVendorNumber === vendorNumber &&
			firstCompanyCode === companyCode &&
			firstInvoiceCurrency === invoiceCurrency &&
			firstConfiguration === configuration)
		{
			var item = Controls.LineItems__.AddItem(false);
			item.SetValue("LineType__", "Vendor");
			item.SetValue("DocumentNumber__", documentNumber);
			item.SetValue("NetAmount__", queryResult.GetQueryValue("NetAmount__", i));
			item.SetValue("Amount__", queryResult.GetQueryValue("InvoiceAmount__", i));
			item.SetValue("Currency__", queryResult.GetQueryValue("InvoiceCurrency__", i));
			item.SetValue("TaxAmount__", queryResult.GetQueryValue("TaxAmount__", i));
			item.SetValue("ValidationURL__", queryResult.GetQueryValue("ValidationURL", i));
			item.SetValue("InvoiceDate__", queryResult.GetQueryValue("InvoiceDate__", i));
			item.SetValue("PostingDate__", queryResult.GetQueryValue("PostingDate__", i));
			item.SetValue("InvoiceDescription__", queryResult.GetQueryValue("InvoiceDescription__", i));
			validRuidExs.push("CD#" + queryResult.GetQueryValue("ProcessID", i) + "." + queryResult.GetQueryValue("MsnEx", i));
			if (documentNumber)
			{
				var invoiceDocument = Lib.AP.ParseInvoiceDocumentNumber(documentNumber, true);
				var settlement = invoiceDocument.documentNumber;
				settlements.push(settlement);
				fiscalYear = invoiceDocument.fiscalYear;
			}
		}
		else
		{
			Log.Warn("The document " + documentNumber + " has been ignored");
		}
	}
	setTimeout(function ()
	{
		for (var i = 0; i < Controls.LineItems__.GetLineCount(); ++i)
		{
			Controls.LineItems__.GetRow(i).DocumentNumber__.DisplayAs(
			{
				type: "Link"
			});
		}
	});
	if (validRuidExs.length < queryResult.GetRecordsCount())
	{
		var errorMessage = Language.Translate(queryResult.GetRecordsCount() - validRuidExs.length === 1 ? "_ignored consignment invoices_sing" : "_ignored consignment invoices", false, queryResult.GetRecordsCount() - validRuidExs.length, firstVendorNumber, firstCompanyCode, firstInvoiceCurrency, firstConfiguration);
		Popup.Alert(errorMessage, false, null, "_ignored consignment invoices popup title");
	}
	var fields = "WRBTR|BELNR|GJAHR|BUKRS|LIFNR|WAERS";
	var filters;
	if (settlements.length > 0)
	{
		if (settlements.length > 1)
		{
			filters = "( BELNR = '" + settlements.join("' OR BELNR = '") + "' )";
		}
		else
		{
			filters = "BELNR = '" + settlements[0] + "'";
		}
		filters += " AND GJAHR = '" + fiscalYear + "' AND BUKRS = '" + firstCompanyCode + "' AND LIFNR = '" + Sys.Helpers.String.SAP.NormalizeID(Sys.Helpers.String.SAP.Trim(firstVendorNumber), 10) + "'";
		Log.Info("Filters : " + filters);
		Query.SAPQuery(setAssignedAmount, Variable.GetValueAsString("SAPConfiguration"), "BSIK", fields, filters, 100, 0, false);
	}
	else
	{
		// no settlement, no clearing possible
		Controls.ERPInvoiceNumber__.Wait(false);
	}
	setInvoicestoClearCountText(validRuidExs.length);
	Variable.SetValueAsString("AncestorsRuid", validRuidExs.join("|"));
	flagAncestorsRuidAsLoaded();
}
/**
 * SAPQuery Callback
 * @this SAPQueryResult
 */
function setAssignedAmount()
{
	var results = this.GetQueryValue();
	if (results.ERRORS && results.ERRORS.length > 0)
	{
		for (var _i = 0, _a = results.ERRORS; _i < _a.length; _i++)
		{
			var error = _a[_i];
			Log.Info("Query in error : " + error);
		}
	}
	else if (results.Records && results.Records.length > 0)
	{
		Log.Info("Query returned " + results.Records.length + " records");
		var sum = 0;
		for (var i = 0; i < results.Records.length; i++)
		{
			sum += parseFloat(this.GetQueryValue("WRBTR", i));
		}
		Data.SetValue("AssignedAmount__", sum);
		Data.SetValue("SAPCurrency__", this.GetQueryValue("WAERS", 0));
		EventHandlers.computeAmount();
	}
	/*
	else
	{
	    // no valid settlement, no clearing possible
	}
	*/
	Controls.ERPInvoiceNumber__.Wait(false);
}

function getMsnExsFromRuidExs()
{
	var result = {
		processid: null,
		msnexs: []
	};
	var ancestorRuids = Variable.GetValueAsString("AncestorsRuid");
	if (!ancestorRuids)
	{
		return result;
	}
	var ancestors = ancestorRuids.split("|");
	for (var _i = 0, ancestors_1 = ancestors; _i < ancestors_1.length; _i++)
	{
		var ancestor = ancestors_1[_i];
		if (ancestor.length > 3 && "CD#" === ancestor.substr(0, 3))
		{
			var currentProcessId = ancestor.substr(3, ancestor.indexOf(".") - 3);
			var msnEx = ancestor.substr(ancestor.indexOf(".") + 1);
			if (!result.processid)
			{
				result.processid = currentProcessId;
			}
			if (currentProcessId === result.processid)
			{
				result.msnexs.push(msnEx);
			}
			else
			{
				Log.Warn("Ignore " + ancestor + " because the processid " + currentProcessId + " !=" + result.processid);
			}
		}
		else
		{
			Log.Warn("Ignore " + ancestor + " because not a CD#");
		}
	}
	return result;
}

function ancestorsRuidLoaded()
{
	return Variable.GetValueAsString("AncestorsRuidLoaded") === "1";
}

function flagAncestorsRuidAsLoaded()
{
	Variable.SetValueAsString("AncestorsRuidLoaded", "1");
}

function setInvoicestoClearCountText(itemCount)
{
	if (itemCount <= 0)
	{
		Controls.InvoicesToClearCount__.SetText("_NoInvoiceToClearCountLabel");
	}
	else
	{
		Controls.InvoicesToClearCount__.SetText("_InvoicesToClearCountLabel", itemCount);
	}
}

function isDocumentCleared()
{
	return Boolean(Controls.ERPInvoiceNumber__.GetValue());
}

function queryAncestorsAndfillForm(ancestors)
{
	var attributes = [
		"MsnEx", "ProcessId", "ValidationURL",
		"Configuration__", "VendorNumber__", "InvoiceStatus__",
		"CompanyCode__", "InvoiceCurrency__", "LineType__",
		"ERPInvoiceNumber__", "ItemNumber__", "NetAmount__",
		"InvoiceAmount__", "TaxAmount__", "InvoiceDate__",
		"PostingDate__", "InvoiceDescription__"
	].join("|");
	var filter;
	var MAX_INVOICES_PER_QUERY = 100;
	if (ancestors.processid && ancestors.msnexs.length > 0)
	{
		Controls.ERPInvoiceNumber__.Wait(true);
		if (ancestors.msnexs.length > 1)
		{
			filter = "|(MsnEx=" + ancestors.msnexs.join(")(MsnEx=") + ")";
		}
		else
		{
			filter = "MsnEx=" + ancestors.msnexs[0];
		}
		Query.DBQuery(getConsignmentDocumentsCallback, "CD#" + ancestors.processid, attributes, "&(|(State=70)(State=90))(InvoiceStatus__=Wait for clearing)(" + filter + ")", null, MAX_INVOICES_PER_QUERY);
	}
	else
	{
		setInvoicestoClearCountText(0);
	}
}

function updateItemWithTaxRate(item, taxRate)
{
	setItemTaxRate(item, taxRate);
	EventHandlers.computeAmount();
}

function onErrorWithTaxCode(item, error, field)
{
	item.SetError(field, error);
	updateItemWithTaxRate(item, 0);
}

function setItemTaxRate(item, taxrate)
{
	var taxamount = item.GetValue("Amount__") * taxrate / 100;
	item.SetValue("TaxRate__", taxrate);
	item.SetValue("TaxAmount__", taxamount);
}

function setVendorLineLayout(itemIndex)
{
	var row = Controls.LineItems__.GetRow(itemIndex);
	if (row)
	{
		row.DocumentNumber__.DisplayAs(
		{
			type: "Link"
		});
		row.Amount__.SetReadOnly(true);
		row.TaxAmount__.SetReadOnly(true);
	}
}

function setGLLineLayout(itemIndex)
{
	var row = Controls.BalancingLineItems__.GetRow(itemIndex);
	if (row)
	{
		row.Description__.SetReadOnly(false);
		row.Amount__.SetReadOnly(false);
		row.GLAccount__.SetReadOnly(false);
		row.GLDescription__.SetReadOnly(true);
		row.CostCenter__.SetReadOnly(false);
		row.CCDescription__.SetReadOnly(true);
		row.TaxCode__.SetReadOnly(false);
		row.TaxAmount__.SetReadOnly(true);
	}
}

function initFromConfiguration()
{
	Lib.ERP.InitERPName("SAP", false, "AP");
	Lib.P2P.InitSAPConfiguration("SAP", "AP");
	Lib.P2P.Browse.Init("AP", "BalancingLineItems__");
}

function runCustomScript()
{
	initFromConfiguration();
	EventHandlers.init();
	ButtonsBehavior.init();
	updateLayout();
	Controls.LineItems__.HideTableRowDeleteForItem(-1, true);
	Controls.LineItems__.GetRow(0).DocumentNumber__.SetError("");
	var ancestors = getMsnExsFromRuidExs();
	if (!ancestorsRuidLoaded() && !ProcessInstance.isReadOnly)
	{
		queryAncestorsAndfillForm(ancestors);
	}
	else if (!isDocumentCleared())
	{
		setInvoicestoClearCountText(ancestors.msnexs.length);
	}
	else
	{
		Controls.InvoicesToClearCount__.SetText("");
	}
}
runCustomScript();