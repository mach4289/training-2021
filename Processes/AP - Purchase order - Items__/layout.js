{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"OrderNumber__": "LabelOrderNumber__",
																	"LabelOrderNumber__": "OrderNumber__",
																	"ItemNumber__": "LabelItemNumber__",
																	"LabelItemNumber__": "ItemNumber__",
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"OrderedAmount__": "LabelOrderedAmount__",
																	"LabelOrderedAmount__": "OrderedAmount__",
																	"OrderedQuantity__": "LabelOrderedQuantity__",
																	"LabelOrderedQuantity__": "OrderedQuantity__",
																	"InvoicedAmount__": "LabelInvoicedAmount__",
																	"LabelInvoicedAmount__": "InvoicedAmount__",
																	"InvoicedQuantity__": "LabelInvoicedQuantity__",
																	"LabelInvoicedQuantity__": "InvoicedQuantity__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"DeliveredQuantity__": "LabelDeliveredQuantity__",
																	"LabelDeliveredQuantity__": "DeliveredQuantity__",
																	"DeliveredAmount__": "LabelDeliveredAmount__",
																	"LabelDeliveredAmount__": "DeliveredAmount__",
																	"UnitPrice__": "LabelUnitPrice__",
																	"LabelUnitPrice__": "UnitPrice__",
																	"PartNumber__": "LabelPartNumber__",
																	"LabelPartNumber__": "PartNumber__",
																	"GLAccount__": "LabelGLAccount__",
																	"LabelGLAccount__": "GLAccount__",
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"OrderDate__": "LabelOrderDate__",
																	"LabelOrderDate__": "OrderDate__",
																	"IsLocalPO__": "LabelIsLocalPO__",
																	"LabelIsLocalPO__": "IsLocalPO__",
																	"NoMoreInvoiceExpected__": "LabelNoMoreInvoiceExpected__",
																	"LabelNoMoreInvoiceExpected__": "NoMoreInvoiceExpected__",
																	"IsCreatedInERP__": "LabelIsCreatedInERP__",
																	"LabelIsCreatedInERP__": "IsCreatedInERP__",
																	"GRIV__": "LabelGRIV__",
																	"LabelGRIV__": "GRIV__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"Receiver__": "LabelReceiver__",
																	"LabelReceiver__": "Receiver__",
																	"TaxRate__": "LabelTaxRate__",
																	"LabelTaxRate__": "TaxRate__",
																	"NoGoodsReceipt__": "LabelNoGoodsReceipt__",
																	"LabelNoGoodsReceipt__": "NoGoodsReceipt__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"UnitOfMeasureCode__": "LabelUnitOfMeasureCode__",
																	"LabelUnitOfMeasureCode__": "UnitOfMeasureCode__",
																	"ProjectCode__": "LabelProjectCode__",
																	"LabelProjectCode__": "ProjectCode__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__",
																	"ItemType__": "LabelItemType__",
																	"LabelItemType__": "ItemType__",
																	"InternalOrder__": "LabelInternalOrder__",
																	"LabelInternalOrder__": "InternalOrder__",
																	"WBSElement__": "LabelWBSElement__",
																	"LabelWBSElement__": "WBSElement__",
																	"WBSElementID__": "LabelWBSElementID__",
																	"LabelWBSElementID__": "WBSElementID__",
																	"FreeDimension1__": "LabelFreeDimension1__",
																	"LabelFreeDimension1__": "FreeDimension1__",
																	"FreeDimension1ID__": "LabelFreeDimension1ID__",
																	"LabelFreeDimension1ID__": "FreeDimension1ID__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 36,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"OrderNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelOrderNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"OrderDate__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelOrderDate__": {
																				"line": 4,
																				"column": 1
																			},
																			"ItemNumber__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelItemNumber__": {
																				"line": 5,
																				"column": 1
																			},
																			"PartNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelPartNumber__": {
																				"line": 6,
																				"column": 1
																			},
																			"Description__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 8,
																				"column": 1
																			},
																			"GLAccount__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelGLAccount__": {
																				"line": 9,
																				"column": 1
																			},
																			"Group__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 10,
																				"column": 1
																			},
																			"CostCenter__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 11,
																				"column": 1
																			},
																			"ProjectCode__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelProjectCode__": {
																				"line": 12,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 18,
																				"column": 1
																			},
																			"UnitPrice__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelUnitPrice__": {
																				"line": 19,
																				"column": 1
																			},
																			"OrderedAmount__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelOrderedAmount__": {
																				"line": 20,
																				"column": 1
																			},
																			"UnitOfMeasureCode__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelUnitOfMeasureCode__": {
																				"line": 21,
																				"column": 1
																			},
																			"OrderedQuantity__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelOrderedQuantity__": {
																				"line": 22,
																				"column": 1
																			},
																			"InvoicedAmount__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelInvoicedAmount__": {
																				"line": 23,
																				"column": 1
																			},
																			"InvoicedQuantity__": {
																				"line": 24,
																				"column": 2
																			},
																			"LabelInvoicedQuantity__": {
																				"line": 24,
																				"column": 1
																			},
																			"DeliveredAmount__": {
																				"line": 25,
																				"column": 2
																			},
																			"LabelDeliveredAmount__": {
																				"line": 25,
																				"column": 1
																			},
																			"DeliveredQuantity__": {
																				"line": 26,
																				"column": 2
																			},
																			"LabelDeliveredQuantity__": {
																				"line": 26,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 27,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 27,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 28,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 28,
																				"column": 1
																			},
																			"TaxRate__": {
																				"line": 29,
																				"column": 2
																			},
																			"LabelTaxRate__": {
																				"line": 29,
																				"column": 1
																			},
																			"Receiver__": {
																				"line": 30,
																				"column": 2
																			},
																			"LabelReceiver__": {
																				"line": 30,
																				"column": 1
																			},
																			"IsLocalPO__": {
																				"line": 32,
																				"column": 2
																			},
																			"LabelIsLocalPO__": {
																				"line": 32,
																				"column": 1
																			},
																			"IsCreatedInERP__": {
																				"line": 33,
																				"column": 2
																			},
																			"LabelIsCreatedInERP__": {
																				"line": 33,
																				"column": 1
																			},
																			"GRIV__": {
																				"line": 34,
																				"column": 2
																			},
																			"LabelGRIV__": {
																				"line": 34,
																				"column": 1
																			},
																			"NoMoreInvoiceExpected__": {
																				"line": 35,
																				"column": 2
																			},
																			"LabelNoMoreInvoiceExpected__": {
																				"line": 35,
																				"column": 1
																			},
																			"NoGoodsReceipt__": {
																				"line": 36,
																				"column": 2
																			},
																			"LabelNoGoodsReceipt__": {
																				"line": 36,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 31,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 31,
																				"column": 1
																			},
																			"ItemType__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelItemType__": {
																				"line": 7,
																				"column": 1
																			},
																			"InternalOrder__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelInternalOrder__": {
																				"line": 13,
																				"column": 1
																			},
																			"WBSElement__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelWBSElement__": {
																				"line": 14,
																				"column": 1
																			},
																			"WBSElementID__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelWBSElementID__": {
																				"line": 15,
																				"column": 1
																			},
																			"FreeDimension1__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelFreeDimension1__": {
																				"line": 16,
																				"column": 1
																			},
																			"FreeDimension1ID__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelFreeDimension1ID__": {
																				"line": 17,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 4
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "Vendor number",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"VendorNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "Vendor number",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelOrderNumber__": {
																			"type": "Label",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"label": "Order number",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"OrderNumber__": {
																			"type": "ShortText",
																			"data": [
																				"OrderNumber__"
																			],
																			"options": {
																				"label": "Order number",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelOrderDate__": {
																			"type": "Label",
																			"data": [
																				"OrderDate__"
																			],
																			"options": {
																				"label": "Order date",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"OrderDate__": {
																			"type": "DateTime",
																			"data": [
																				"OrderDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "Order date",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelItemNumber__": {
																			"type": "Label",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "Item number",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"ItemNumber__": {
																			"type": "ShortText",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "Item number",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 12
																		},
																		"LabelPartNumber__": {
																			"type": "Label",
																			"data": [
																				"PartNumber__"
																			],
																			"options": {
																				"label": "Part number",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"PartNumber__": {
																			"type": "ShortText",
																			"data": [
																				"PartNumber__"
																			],
																			"options": {
																				"label": "Part number",
																				"activable": true,
																				"width": "500",
																				"autocompletable": true,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 14
																		},
																		"LabelItemType__": {
																			"type": "Label",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"label": "_ItemType",
																				"version": 0
																			},
																			"stamp": 74
																		},
																		"ItemType__": {
																			"type": "ComboBox",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_QuantityBased",
																					"1": "_AmountBased"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "QuantityBased",
																					"1": "AmountBased"
																				},
																				"label": "_ItemType",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 75
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"length": 100,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 16
																		},
																		"LabelGLAccount__": {
																			"type": "Label",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "G/L account",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"GLAccount__": {
																			"type": "ShortText",
																			"data": [
																				"GLAccount__"
																			],
																			"options": {
																				"label": "G/L account",
																				"activable": true,
																				"width": "500",
																				"autocompletable": false,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 18
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": "500",
																				"length": 64,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "Cost center",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"CostCenter__": {
																			"type": "ShortText",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "Cost center",
																				"activable": true,
																				"width": "500",
																				"autocompletable": true,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 22
																		},
																		"LabelProjectCode__": {
																			"type": "Label",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"label": "_ProjectCode",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"ProjectCode__": {
																			"type": "ShortText",
																			"data": [
																				"ProjectCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ProjectCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 24
																		},
																		"LabelInternalOrder__": {
																			"type": "Label",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"label": "_InternalOrder",
																				"version": 0
																			},
																			"stamp": 76
																		},
																		"InternalOrder__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"InternalOrder__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_InternalOrder",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 77
																		},
																		"LabelWBSElement__": {
																			"type": "Label",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"label": "_WBSElement",
																				"version": 0
																			},
																			"stamp": 78
																		},
																		"WBSElement__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElement__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElement",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 79
																		},
																		"LabelWBSElementID__": {
																			"type": "Label",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"label": "_WBSElementID",
																				"version": 0
																			},
																			"stamp": 80
																		},
																		"WBSElementID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WBSElementID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_WBSElementID",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 81
																		},
																		"LabelFreeDimension1__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"label": "_FreeDimension1",
																				"version": 0
																			},
																			"stamp": 82
																		},
																		"FreeDimension1__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"FreeDimension1__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_FreeDimension1",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 83
																		},
																		"LabelFreeDimension1ID__": {
																			"type": "Label",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"label": "_FreeDimension1ID"
																			},
																			"stamp": 86
																		},
																		"FreeDimension1ID__": {
																			"type": "ShortText",
																			"data": [
																				"FreeDimension1ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FreeDimension1ID",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"length": 500,
																				"browsable": false
																			},
																			"stamp": 87
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "BudgetID",
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "BudgetID",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"LabelUnitPrice__": {
																			"type": "Label",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"label": "Unit price",
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"UnitPrice__": {
																			"type": "Decimal",
																			"data": [
																				"UnitPrice__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Unit price",
																				"precision_internal": 4,
																				"activable": true,
																				"width": "500",
																				"autocompletable": true,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 4
																			},
																			"stamp": 28
																		},
																		"LabelOrderedAmount__": {
																			"type": "Label",
																			"data": [
																				"OrderedAmount__"
																			],
																			"options": {
																				"label": "Ordered amount",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"OrderedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"OrderedAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Ordered amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 30
																		},
																		"LabelUnitOfMeasureCode__": {
																			"type": "Label",
																			"data": [
																				"UnitOfMeasureCode__"
																			],
																			"options": {
																				"label": "_UnitOfMeasureCode",
																				"version": 0
																			},
																			"stamp": 31
																		},
																		"UnitOfMeasureCode__": {
																			"type": "ShortText",
																			"data": [
																				"UnitOfMeasureCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_UnitOfMeasureCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 500
																			},
																			"stamp": 32
																		},
																		"LabelOrderedQuantity__": {
																			"type": "Label",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"label": "Ordered quantity",
																				"version": 0
																			},
																			"stamp": 33
																		},
																		"OrderedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"OrderedQuantity__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Ordered quantity",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 34
																		},
																		"LabelInvoicedAmount__": {
																			"type": "Label",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"label": "Invoiced amount",
																				"version": 0
																			},
																			"stamp": 35
																		},
																		"InvoicedAmount__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Invoiced amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 36
																		},
																		"LabelInvoicedQuantity__": {
																			"type": "Label",
																			"data": [
																				"InvoicedQuantity__"
																			],
																			"options": {
																				"label": "Invoiced quantity",
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"InvoicedQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedQuantity__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Invoiced quantity",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 38
																		},
																		"LabelDeliveredAmount__": {
																			"type": "Label",
																			"data": [
																				"DeliveredAmount__"
																			],
																			"options": {
																				"label": "Delivered amount",
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"DeliveredAmount__": {
																			"type": "Decimal",
																			"data": [
																				"DeliveredAmount__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Delivered amount",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 40
																		},
																		"LabelDeliveredQuantity__": {
																			"type": "Label",
																			"data": [
																				"DeliveredQuantity__"
																			],
																			"options": {
																				"label": "Delivered quantity",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"DeliveredQuantity__": {
																			"type": "Decimal",
																			"data": [
																				"DeliveredQuantity__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Delivered quantity",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2
																			},
																			"stamp": 42
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"S_lection_dans_une_table__"
																			],
																			"options": {
																				"label": "Currency",
																				"version": 0
																			},
																			"stamp": 43
																		},
																		"Currency__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"version": 1,
																				"label": "Currency",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 44
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "Tax code",
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"TaxCode__": {
																			"type": "ShortText",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "Tax code",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"length": 600,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 46
																		},
																		"LabelTaxRate__": {
																			"type": "Label",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"label": "_Tax rate",
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"TaxRate__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Tax rate",
																				"precision_internal": 3,
																				"precision_current": 3,
																				"precision_min": 2,
																				"activable": true,
																				"width": "500",
																				"helpText": "Tax rate associated to Tax code.\nUsed specifically for EBS in multi jurisdiction. When one Po Line has multiple tax codes associated \n(so the tax code is not defined in the Tax Code table)",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"defaultValue": "0.00",
																				"autocompletable": false
																			},
																			"stamp": 48
																		},
																		"LabelReceiver__": {
																			"type": "Label",
																			"data": [
																				"Receiver__"
																			],
																			"options": {
																				"label": "Receiver",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"Receiver__": {
																			"type": "ShortText",
																			"data": [
																				"Receiver__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Receiver",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 50
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"ComboBox__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 51
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 52
																		},
																		"LabelIsLocalPO__": {
																			"type": "Label",
																			"data": [
																				"IsLocalPO__"
																			],
																			"options": {
																				"label": "Is local PO_V2",
																				"version": 0
																			},
																			"stamp": 53
																		},
																		"IsLocalPO__": {
																			"type": "CheckBox",
																			"data": [
																				"IsLocalPO__"
																			],
																			"options": {
																				"label": "Is local PO_V2",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"LabelIsCreatedInERP__": {
																			"type": "Label",
																			"data": [
																				"IsCreatedInERP__"
																			],
																			"options": {
																				"label": "Is created in ERP",
																				"version": 0
																			},
																			"stamp": 55
																		},
																		"IsCreatedInERP__": {
																			"type": "CheckBox",
																			"data": [
																				"IsCreatedInERP__"
																			],
																			"options": {
																				"label": "Is created in ERP",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"LabelGRIV__": {
																			"type": "Label",
																			"data": [
																				"GRIV__"
																			],
																			"options": {
																				"label": "GR/IV",
																				"version": 0
																			},
																			"stamp": 57
																		},
																		"GRIV__": {
																			"type": "CheckBox",
																			"data": [
																				"GRIV__"
																			],
																			"options": {
																				"label": "GR/IV",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 58
																		},
																		"LabelNoMoreInvoiceExpected__": {
																			"type": "Label",
																			"data": [
																				"NoMoreInvoiceExpected__"
																			],
																			"options": {
																				"label": "No more invoice expected",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"NoMoreInvoiceExpected__": {
																			"type": "CheckBox",
																			"data": [
																				"NoMoreInvoiceExpected__"
																			],
																			"options": {
																				"label": "No more invoice expected",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 60
																		},
																		"LabelNoGoodsReceipt__": {
																			"type": "Label",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"NoGoodsReceipt__": {
																			"type": "CheckBox",
																			"data": [
																				"NoGoodsReceipt__"
																			],
																			"options": {
																				"label": "_NoGoodsReceipt",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 62
																		}
																	},
																	"stamp": 63
																}
															},
															"stamp": 64,
															"data": []
														}
													},
													"stamp": 65,
													"data": []
												}
											}
										}
									},
									"stamp": 66,
									"data": []
								}
							},
							"stamp": 67,
							"data": []
						}
					},
					"stamp": 68,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 69,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 70,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 71,
							"data": []
						}
					},
					"stamp": 72,
					"data": []
				}
			},
			"stamp": 73,
			"data": []
		}
	},
	"stamps": 87,
	"data": []
}