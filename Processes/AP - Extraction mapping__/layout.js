{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"FormField__": "LabelFormField__",
																	"LabelFormField__": "FormField__",
																	"ExtractedValue__": "LabelExtractedValue__",
																	"LabelExtractedValue__": "ExtractedValue__",
																	"BusinessValue__": "LabelBusinessValue__",
																	"LabelBusinessValue__": "BusinessValue__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"FormField__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelFormField__": {
																				"line": 2,
																				"column": 1
																			},
																			"ExtractedValue__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelExtractedValue__": {
																				"line": 3,
																				"column": 1
																			},
																			"BusinessValue__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelBusinessValue__": {
																				"line": 4,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code"
																			},
																			"stamp": 22
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 23
																		},
																		"LabelFormField__": {
																			"type": "Label",
																			"data": [
																				"FormField__"
																			],
																			"options": {
																				"label": "_FormField",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"FormField__": {
																			"type": "ShortText",
																			"data": [
																				"FormField__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_FormField",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 15
																		},
																		"LabelExtractedValue__": {
																			"type": "Label",
																			"data": [
																				"ExtractedValue__"
																			],
																			"options": {
																				"label": "_ExtractedValue",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"ExtractedValue__": {
																			"type": "ShortText",
																			"data": [
																				"ExtractedValue__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ExtractedValue",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelBusinessValue__": {
																			"type": "Label",
																			"data": [
																				"BusinessValue__"
																			],
																			"options": {
																				"label": "_BusinessValue",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"BusinessValue__": {
																			"type": "ShortText",
																			"data": [
																				"BusinessValue__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_BusinessValue",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 19
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 23,
	"data": []
}