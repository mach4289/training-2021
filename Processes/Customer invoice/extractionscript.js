///#GLOBALS Lib Sys
/* Customer invoice Extraction script */

function setVendorSubmissionVariables()
{
	var currentVendor = Users.GetUser(Data.GetValue("OwnerId"));
	var vendorVars = currentVendor.GetVars();
	var vendorLogin = vendorVars.GetValue_String("Login", 0);

	if (vendorLogin && vendorLogin.indexOf('$') !== -1)
	{
		vendorLogin = vendorLogin.substring(1 + vendorLogin.indexOf('$'));
	}

	Variable.SetValueAsString("submission_vendorid", vendorLogin);
	return vendorLogin;
}

function initCompany(vendorShortLogin)
{
	var vendorLinksInfos = getVendorLinksInfos(vendorShortLogin);
	Variable.SetValueAsString("vendorLinksInfos", JSON.stringify(vendorLinksInfos));
	var companiesDescriptions = getCompaniesDescription(vendorLinksInfos);
	Variable.SetValueAsString("companies", JSON.stringify(companiesDescriptions));
}

function addCompanyDescription(companiesDescriptions, companyCode, companyDescription)
{
	if (companyCode)
	{
		if (companyDescription)
		{
			companiesDescriptions[companyCode] = companyDescription;
		}
		else
		{
			companiesDescriptions[companyCode] = companyCode;
		}
	}
}

//Link company codes and their description so that display only the description for the vendor
function getCompaniesDescription(vendorLinksInfos)
{
	var companiesDescriptions = {};
	var countCompanies = 0;
	var query = Process.CreateQueryAsProcessAdmin();

	var filter = "";
	var i = 0;
	for (var CompanyCode in vendorLinksInfos)
	{
		if (i > 0)
		{
			filter = "|(CompanyCode__=" + CompanyCode + ")(" + filter + ")";
		}
		else
		{
			filter = "CompanyCode__=" + CompanyCode;
		}
		i++;
	}

	query.Reset();
	query.SetSpecificTable("PurchasingCompanycodes__");
	query.SetAttributesList("CompanyCode__,CompanyName__");
	query.SetFilter(filter);
	query.SetSortOrder("CompanyName__ ASC");
	query.MoveFirst();
	var record = query.MoveNextRecord();
	while (record)
	{
		var companyVars = record.GetVars();
		var companyCode = companyVars.GetValue_String("CompanyCode__", 0);
		var companyDescription = companyVars.GetValue_String("CompanyName__", 0);
		addCompanyDescription(companiesDescriptions, companyCode, companyDescription);
		if (countCompanies === 0)
		{
			Data.SetValue("Company__", companyDescription);
		}
		countCompanies++;
		record = query.MoveNextRecord();
	}
	if (countCompanies > 1)
	{
		Data.SetValue("Company__", getLastCompanySelected());
	}
	return companiesDescriptions;
}

//Get all company code for the current vendor
function getVendorLinksInfos(vendorShortLogin)
{
	var vendorLinksInfos = {};
	var query = Process.CreateQueryAsProcessAdmin();

	query.Reset();
	query.SetSpecificTable("AP - Vendors links__");
	query.SetAttributesList("CompanyCode__,Number__,Configuration__");
	query.SetFilter("ShortLogin__=" + vendorShortLogin);
	query.MoveFirst();
	var record = query.MoveNextRecord();
	while (record)
	{
		var CC = record.GetVars().GetValue_String("CompanyCode__", 0);
		var VID = record.GetVars().GetValue_String("Number__", 0);
		var conf = record.GetVars().GetValue_String("Configuration__", 0);
		if (!CC)
		{
			vendorLinksInfos = {};
			vendorLinksInfos["*"] = { number: VID, configuration: conf };
			return vendorLinksInfos;
		}
		vendorLinksInfos[CC] = { number: VID, configuration: conf };
		record = query.MoveNextRecord();
	}
	return vendorLinksInfos;
}

//Get the last company description selected in order to set the same value for the current invoice
function getLastCompanySelected()
{
	var query = Process.CreateQuery();
	query.Reset();
	query.SetSpecificTable("CDNAME#Customer invoice");
	query.SetAttributesList("RUIDEX,SubmitDateTime,Company__,CustomerInvoiceStatus__");
	query.SetSortOrder("SubmitDateTime DESC");
	query.SetFilter("!(CustomerInvoiceStatus__=To send)");
	query.SetOptionEx("limit=1");
	query.MoveFirst();
	var record = query.MoveNextRecord();
	if (record)
	{
		return record.GetVars().GetValue_String("Company__", 0);
	}
	return null;
}

function run()
{
	if (Data.GetValue("VIRuidEx__"))
	{
		// If the VIRuidEx__ field is set, it means the process was instanciated from an invoice
		Log.Info("Created from an invoice");
	}
	else if (Lib.AP.CustomerInvoiceType.isFlipPO())
	{
		Log.Info("FlipPO creation mode");
		if (!Lib.AP.VendorPortal.CreateFromFlipPO.Fill(Data))
		{
			Log.Error("Unable to fill the customer invoice");
		}

		// Linking the source CustomerOrder record with the current RuidEx
		var source_ruidex = Data.GetValue("Source_RuidEx__");
		if (source_ruidex !== null)
		{
			Log.Info("Linking the source CustomerOrder (" + source_ruidex + ") record with the current RuidEx");
			Process.UpdateProcessInstanceDataAsync(source_ruidex, JSON.stringify({
				fields:
					{
						Invoice_RuidEx__: Data.GetValue("RuidEx")
					}
			}));
		}

		// When created from the portal, CI is the conversation master
		// VI will join conversation via JoinVIToCIChat action
		Conversation.Create(Lib.AP.VendorPortal.BuildConversationListOptions());
	}
	else
	{
		Log.Info("Created from the portal");
		var vendorShortLogin = setVendorSubmissionVariables();
		initCompany(vendorShortLogin);

		// When created from the portal, CI is the conversation master
		// VI will join conversation via JoinVIToCIChat action
		Conversation.Create(Lib.AP.VendorPortal.BuildConversationListOptions());
	}

	Data.SetValue("PortalRuidEx__", Data.GetValue("RUIDEX"));
}

run();
