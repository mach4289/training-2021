///#GLOBALS Lib

/** ***** **/
/** DEBUG **/
/** ***** **/
// Enable/disable debug mode

var g_debug = false;

var CompanyHelper =
{
	getCompanies: function ()
	{
		var companies = Variable.GetValueAsString("companies");
		if (companies)
		{
			try
			{
				companies = JSON.parse(companies);
				return companies;
			}
			catch (e)
			{
				Log.Error("getCompanies - exception");
			}
		}
		return {};
	},

	getvendorLinksInfos: function ()
	{
		var vendorLinksInfos = Variable.GetValueAsString("vendorLinksInfos");
		if (vendorLinksInfos)
		{
			try
			{
				vendorLinksInfos = JSON.parse(vendorLinksInfos);
				return vendorLinksInfos;
			}
			catch (e)
			{
				Log.Error("getvendorLinksInfos - exception");
			}
		}
		return {};
	},

	getCompanyValues: function ()
	{
		var comboValues = [];
		var companies = this.getCompanies();
		if (!companies)
		{
			return comboValues;
		}

		for (var companyCode in companies)
		{
			if (companies.hasOwnProperty(companyCode))
			{
				comboValues.push(companies[companyCode]);
			}
		}
		return comboValues;
	},

	readyCompanyBrowse: false,
	companyBrowseListeners: {},

	initCompanyBrowse: function ()
	{
		if (Variable.GetValueAsString("vendorLinksInfos") !== "{}")
		{
			var comboValues = this.getCompanyValues();
			var selectedCompanyDescription = Data.GetValue("Company__");

			if (comboValues.length)
			{
				Controls.Company__.SetText(comboValues.join("\n"));
				Controls.Company__.SetRequired(true);
			}

			if (selectedCompanyDescription && selectedCompanyDescription !== "null")
			{
				if (!comboValues.length)
				{
					Controls.Company__.SetText(selectedCompanyDescription);
				}
				Controls.Company__.SetValue(selectedCompanyDescription);
			}

			Controls.Company__.Hide(comboValues.length <= 1);
			Controls.Company__.OnChange = function ()
			{
				for (var id in CompanyHelper.companyBrowseListeners)
				{
					if (CompanyHelper.companyBrowseListeners.hasOwnProperty(id))
					{
						CompanyHelper.companyBrowseListeners[id]();
					}
				}
			};

			CompanyHelper.readyCompanyBrowse = true;
		}
		else
		{
			Controls.Company__.Hide(true);
			Popup.Alert(["_PleaseContactAPDepartment"], true, null, "_Submission failed");
		}
	},

	updateSelectedCompany: function ()
	{
		var companies = this.getCompanies();
		var vendorLinksInfos = this.getvendorLinksInfos();
		var selectedCompanyDescription = Controls.Company__.GetValue();
		var currentConfiguration = Variable.GetValueAsString("Configuration");
		var newConfiguration;

		for (var companyCode in companies)
		{
			if (companies[companyCode] === selectedCompanyDescription)
			{
				Variable.SetValueAsString("customerInvoiceCompanyCode", companyCode);
				if (vendorLinksInfos["*"])
				{
					Variable.SetValueAsString("submission_vendorid", vendorLinksInfos["*"].number);
					newConfiguration = vendorLinksInfos["*"].configuration;
				}
				else
				{
					Variable.SetValueAsString("submission_vendorid", vendorLinksInfos[companyCode].number);
					newConfiguration = vendorLinksInfos[companyCode].configuration;
				}
				break;
			}
		}
		Variable.SetValueAsString("Configuration", newConfiguration || "Default");
		if (currentConfiguration !== Variable.GetValueAsString("Configuration"))
		{
			//force reload of tableparameters
			Sys.Parameters.GetInstance("AP").Reload(Variable.GetValueAsString("Configuration"));
		}
	}
};

var EventHistoryHelper =
{
	init: function()
	{
		// Conversation is enabled when the join in the extraction script was called. So when the msn is allocated too.
		if (CompanyHelper.readyCompanyBrowse && Controls.PortalRuidEx__.GetValue() && Controls.CustomerInvoiceStatus__.GetValue() !== Lib.AP.CIStatus.Draft)
		{
			EventHistoryHelper.updateTitle();
			Controls.Event_history.Hide(false);
			CompanyHelper.companyBrowseListeners.EventHistoryHelper = function ()
			{
				EventHistoryHelper.updateTitle();
			};
		}
		else
		{
			Controls.Event_history.Hide(true);
		}
	},

	updateTitle: function()
	{
		// Add vendor name into Event history panel title
		var title = Language.Translate("_Event_history", false, Data.GetValue("Company__"));
		Controls.Event_history.SetText(title);
	}
};

var LayoutHelper =
{
	initForm: function ()
	{
		var invoiceStatus = Controls.CustomerInvoiceStatus__.GetValue();
		Controls.Title__.SetHTML(this.buildHTMLContent("_Submission title", invoiceStatus));
		Process.SetHelpId(2532);
		//always hide status, it is displayed in the title
		Controls.CustomerInvoiceStatus__.Hide(!g_debug);
		Controls.PortalRuidEx__.Hide(!g_debug);
		Controls.ReceptionMethod__.Hide(!g_debug);
		Controls.VIRuidEx__.Hide(!g_debug);
		Controls.Source_RuidEx__.Hide(!g_debug);
		Controls.Order_number__.Hide(!g_debug);
		Controls.Tax_amount__.Hide(!g_debug);
		Controls.ContactsInformation.Hide(!g_debug);
		Controls.Line_Items.Hide(!g_debug);


		CompanyHelper.initCompanyBrowse();
		EventHistoryHelper.init();

		if ((invoiceStatus !== Lib.AP.CIStatus.ToSend && invoiceStatus !== Lib.AP.CIStatus.Draft) || ProcessInstance.isReadOnly)
		{
			Controls.Submit__.Hide();
			Controls.Line_Items.SetReadOnly(true);
		}
		else
		{
			Controls.Invoice_number__.SetRequired(true);
		}

		if (Lib.AP.CustomerInvoiceType.isFlipPO())
		{
			LayoutHelper.setFlipPOLayout(invoiceStatus);
		}
		else
		{
			Controls.RefreshPreview__.Hide();
		}

		// Disable modification of invoice informations after invoice posted
		Controls.Invoice_details.SetReadOnly(Data.GetValue("VIRuidEx__"));

		Controls.RejectReason__.Hide(invoiceStatus !== Lib.AP.CIStatus.Rejected || !Data.GetValue("RejectReason__"));
		Controls.PaymentDetails.Hide(invoiceStatus !== Lib.AP.CIStatus.Paid);
		var options =
		{
			ignoreIfExists: false,
			notifyByEmail: true,
			notifyAllUsersInGroup: true
		};
		Controls.ConversationUI__.SetOptions(options);
	},

	buildHTMLContent: function (title, subtitle)
	{
		var html = "<div style='padding: 15px 9px 15px 9px;margin: 0px -10px 0px -10px;line-height: 2em;'><span style='font-size: 20pt; font-weight: bold; color: #51626f; font-family: Arial, Helvetica, sans-serif;'>";
		html += Language.Translate(title);
		html += "</span><span style='font-size: 16pt; color: #51626f; font-family: Arial, Helvetica, sans-serif;'> - </span><span style='font-size: 16pt; color: #51626f; font-family: Arial, Helvetica, sans-serif;'>";
		html += Language.Translate(subtitle);
		html += "</span></div>";

		return html;
	},

	setFlipPOLayout: function(invoiceStatus)
	{
		// Manage visibility
		Controls.Order_number__.Hide(false);
		Controls.Tax_amount__.Hide(false);
		Controls.Due_date__.Hide(true);
		Controls.ContactsInformation.Hide(false);
		Controls.LineItems__.MaxExpectedQuantity__.Hide(true);
		Controls.Line_Items.Hide(false);

		var hideRefreshButton = (invoiceStatus !== Lib.AP.CIStatus.ToSend && invoiceStatus !== Lib.AP.CIStatus.Draft) || ProcessInstance.isReadOnly;
		Controls.RefreshPreview__.Hide(hideRefreshButton);

		// Manage read-only
		Controls.Invoice_amount__.SetReadOnly(true);
		Controls.Invoice_date__.SetReadOnly(true);
		Controls.Net_amount__.SetReadOnly(true);
		Controls.Tax_amount__.SetReadOnly(true);
		Controls.Currency__.SetReadOnly(true);

		// Line item quantity is required
		Controls.LineItems__.ExpectedQuantity__.SetRequired(true);
		Controls.LineItems__.TaxRate__.SetRequired(true);

		// If the invoice number is generated, we set the field in readonly
		if (Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.GenerateInvoiceNumber"))
		{
			Controls.Invoice_number__.SetReadOnly(true);
			Controls.Invoice_number__.SetRequired(false);
			Controls.Invoice_number__.SetPlaceholder(Language.Translate("_Computed at post"));
		}
	}
};

//#region Gartner
var DynamicDiscountHelper = {
	defaultRate: 0.02,
	isPaneShowed: false,
	init: function()
	{
		Controls.ProposeEarlyPayment__.Hide(false);
		Controls.ProposeEarlyPayment__.OnClick = DynamicDiscountHelper.showDynamicDiscountingPanel;
	},
	showDynamicDiscountingPanel: function()
	{
		if (!DynamicDiscountHelper.isPaneShowed)
		{
			Controls.ProposeEarlyPayment__.SetDisabled(true);
			Controls.Early_payment_offer_pane.Hide(false);

			Controls.DiscountLimitDate__.OnChange = function ()
			{
				var discountLimitDate = Controls.DiscountLimitDate__.GetValue();
				if (discountLimitDate)
				{
					Controls.DiscountLimitDate__.SetError(null);
					Controls.EstimatedDiscountAmount__.SetError(null);
					DynamicDiscountHelper.computeDynamicDiscountAmount(discountLimitDate, DynamicDiscountHelper.defaultRate);
				}
				else
				{
					Controls.DiscountPercent__.SetValue(null);
					Controls.EstimatedDiscountAmount__.SetValue(null);
					Controls.InvoiceAmountWithDiscount__.SetValue(null);
					Controls.DiscountLimitDate__.SetError("_FieldRequired");
					Controls.EstimatedDiscountAmount__.SetError("_FieldRequired");
				}
			};

			Controls.SubmitEarlyPaymentDate__.OnClick = DynamicDiscountHelper.checkAndSubmitEarlyPaymentDate;
		}
	},
	checkAndSubmitEarlyPaymentDate: function()
	{
		var EstimatedDiscountAmount = Data.GetValue("EstimatedDiscountAmount__");
		var DiscountLimitDate = Data.GetValue("DiscountLimitDate__");
		var IsFormInvalid = false;
		if (!EstimatedDiscountAmount && EstimatedDiscountAmount !== 0)
		{
			Controls.EstimatedDiscountAmount__.SetError("_FieldRequired");
			Log.Error("UpdateInvoiceDynamicDiscount: no EstimatedDiscountAmount__ field set");
			IsFormInvalid = true;
		}
		if (!DiscountLimitDate)
		{
			Controls.DiscountLimitDate__.SetError("_FieldRequired");
			Log.Error("UpdateInvoiceDynamicDiscount: no DiscountLimitDate__ field set");
			IsFormInvalid = true;
		}
		if (IsFormInvalid === true)
		{
			return;
		}
		Variable.SetValueAsString("DiscountLimitDateFormated", Language.FormatDate(DiscountLimitDate));
		ProcessInstance.Approve("submitearlypaymentdate");
	},
	computeDynamicDiscountAmount: function (expirationDiscountDate, dicountRate)
	{
		var invoiceDate = Controls.Invoice_date__.GetValue();
		var dueDate = Controls.Due_date__.GetValue();
		var invoiceAmount = Controls.Invoice_amount__.GetValue();

		var deltaFromInvoiceDateToDueDate = Sys.Helpers.Date.ComputeDeltaDays(invoiceDate, dueDate);
		var deltaFromInvoiceDateToExpirationDiscountDate = Sys.Helpers.Date.ComputeDeltaDays(invoiceDate, expirationDiscountDate);
		var dynamicDiscountRate = deltaFromInvoiceDateToDueDate !== 0 ?
			dicountRate - ((dicountRate / deltaFromInvoiceDateToDueDate) * deltaFromInvoiceDateToExpirationDiscountDate) : 0;
		var dynamicDiscountAmount = invoiceAmount * dynamicDiscountRate;
		if (deltaFromInvoiceDateToExpirationDiscountDate < 0 || deltaFromInvoiceDateToExpirationDiscountDate > deltaFromInvoiceDateToDueDate)
		{
			dynamicDiscountRate = 0;
			dynamicDiscountAmount = 0;
		}
		var invoiceAmountWithDiscount = invoiceAmount - (invoiceAmount * dynamicDiscountRate);

		//Format number to % value
		var pourcentFormatValue = Language.FormatNumber(Sys.Helpers.Round(dynamicDiscountRate * 100, 2)) + "%";
		Controls.DiscountPercent__.SetValue(pourcentFormatValue);
		Controls.EstimatedDiscountAmount__.SetValue(dynamicDiscountAmount);
		Controls.InvoiceAmountWithDiscount__.SetValue(invoiceAmountWithDiscount);
	},
	isDynamicDiscountingEnabled: function()
	{
		return Sys.Parameters.GetInstance("AP").GetParameter("VendorPortalDynamicDiscounting", "0") === "1";
	}
};

//#endregion

function initFlipPOData()
{
	if (Controls.CustomerInvoiceStatus__.GetValue() === Lib.AP.CIStatus.Draft)
	{
		Data.SetValue("Invoice_date__", new Date());
	}

	Controls.LineItems__.HideTableRowMenu(true);

	var lineItemsTable = Data.GetTable("LineItems__");
	var nbItems = lineItemsTable.GetItemCount();
	for (var n = 0; n < nbItems; n++)
	{
		var lineItem = lineItemsTable.GetItem(n);
		if (lineItem)
		{
			/**
			 * Save the initial expected quantity
			 * It corresponds to the quantity received and not invoiced
			 * It is used later to check if the expected quantity is more than the quantity received and not invoiced when modified manually
			 **/
			if (!lineItem.GetValue("MaxExpectedQuantity__"))
			{
				lineItem.SetValue("MaxExpectedQuantity__", lineItem.GetValue("ExpectedQuantity__"));
			}
		}
	}


	Controls.LineItems__.OnDeleteItem = OnLineItemChange;
	Controls.LineItems__.ExpectedQuantity__.OnChange = OnLineItemChange;

	function OnLineItemChange()
	{
		updateAmounts();

		lineItemsTable = Data.GetTable("LineItems__");
		nbItems = lineItemsTable.GetItemCount();
		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (item)
			{
				var maxQuantity = item.GetValue("MaxExpectedQuantity__");
				var currentQuantity = item.GetValue("ExpectedQuantity__");
				if (currentQuantity > maxQuantity)
				{
					item.SetWarning("ExpectedQuantity__", Language.Translate("_warningMessageExpectedQuantity", false, maxQuantity));
				}
			}
		}
	}

	Controls.LineItems__.TaxRate__.OnChange = function ()
	{
		updateAmounts();
	};

	function updateAmounts()
	{
		lineItemsTable = Data.GetTable("LineItems__");
		nbItems = lineItemsTable.GetItemCount();
		var invoiceNetAmount = 0;
		var invoiceTaxAmount = 0;

		for (var i = 0; i < nbItems; i++)
		{
			var item = lineItemsTable.GetItem(i);
			if (item)
			{
				var itemNetAmount = item.GetValue("UnitPrice__") * item.GetValue("ExpectedQuantity__");
				item.SetValue("ExpectedAmount__", itemNetAmount);
				var itemTaxAmount = itemNetAmount * (item.GetValue("TaxRate__") / 100);

				invoiceNetAmount += itemNetAmount;
				invoiceTaxAmount += itemTaxAmount;
			}
		}

		Controls.Net_amount__.SetValue(invoiceNetAmount);
		Controls.Tax_amount__.SetValue(invoiceTaxAmount);
		Controls.Invoice_amount__.SetValue(invoiceNetAmount + invoiceTaxAmount);
	}
}

function submit()
{
	if (CompanyHelper.readyCompanyBrowse)
	{
		// Allow submit in FlipPO mode or if a document was added
		if (Lib.AP.CustomerInvoiceType.isFlipPO() || Attach.GetProcessedDocument() !== null)
		{
			var generateInvoiceNumber = Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.GenerateInvoiceNumber");
			if ((Lib.AP.CustomerInvoiceType.isFlipPO() && generateInvoiceNumber) || Controls.Invoice_number__.GetValue())
			{
				CompanyHelper.updateSelectedCompany();
				Sys.Parameters.GetInstance("AP").IsReady(function ()
				{
					ProcessInstance.ApproveAsynchronous("Submit");
				});
			}
			else
			{
				Controls.Invoice_number__.ShowErrorMessage();
			}
		}
		else
		{
			Popup.Alert(["_Upload an invoice"], true, null, "_No invoice found");
		}
	}
	else
	{
		Popup.Alert(["_PleaseContactAPDepartment"], true, null, "_Submission failed");
	}
}

function run()
{
	LayoutHelper.initForm();

	Controls.Submit__.OnClick = submit;

	Controls.Exit__.OnClick = function ()
	{
		if (Variable.GetValueAsString("lasterror"))
		{
			ProcessInstance.Delete("Delete");
		}
		else
		{
			ProcessInstance.Quit("Quit");
		}
	};

	if (Lib.AP.CustomerInvoiceType.isFlipPO())
	{
		initFlipPOData();
	}

	// POC - Dynamic discounting
	if (DynamicDiscountHelper.isDynamicDiscountingEnabled())
	{
		if (Data.GetValue("PaymentTerms__") && Data.GetValue("PaymentTerms__") !== "")
		{
			Controls.PaymentTerms__.Hide(false);
		}

		if (Data.GetValue("State") === "70" &&
		(Data.GetValue("CustomerInvoiceStatus__") === "Awaiting payment approval" ||
		Data.GetValue("CustomerInvoiceStatus__") === "Awaiting payment transaction"))
		{
			DynamicDiscountHelper.init();
		}
	}

	Sys.Helpers.TryCallFunction("Lib.AP.Customization.VendorPortal.HTMLScripts.OnHTMLScriptEnd");

	var lastError = Variable.GetValueAsString("lasterror");
	if (lastError)
	{
		Popup.Alert([lastError], false, null, "_Submission failed");
	}
}

run();
