{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"backColor": "text-backgroundcolor-color7",
				"splitterType": "vertical",
				"splitterOptions": {
					"sizeable": false,
					"sidebarWidth": 400,
					"unit": "px",
					"fixedSlider": true
				},
				"version": 0,
				"maxwidth": 1024,
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-left": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0px",
											"left": "0px",
											"width": "400px"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left-4": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"BackToCartPane__": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "none",
														"labelsAlignment": "right",
														"label": "_BackToCartPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 3,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 4,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"BackToCartLink__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line10__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 5,
																	"*": {
																		"Spacer_line10__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line10",
																				"version": 0
																			},
																			"stamp": 6
																		},
																		"BackToCartLink__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_BackToCartLink",
																				"activable": true,
																				"width": "100%",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 7
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-left-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 8,
											"*": {
												"ItemImg": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "120",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_ItemImg",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 9,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 10,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"ItemImg__": {
																				"line": 1,
																				"column": 1
																			},
																			"TagsDisplay__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 11,
																	"*": {
																		"ItemImg__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_ItemImg",
																				"width": "100%",
																				"version": 0,
																				"css": "@ img {\n    border: 0;\n    width: 300px;\n}@ \n div { \n    text-align: center;\n}"
																			},
																			"stamp": 12
																		},
																		"TagsDisplay__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_TagsDisplay",
																				"htmlContent": "<input id=\"onLoadTags\" type=\"hidden\" style=\"display:none;\" onclick=\"SetTags(event);\">\n<div id=\"tag-container\" class=\"tag-container\">\n</div>\n<script type=\"text/javascript\">\n\twindow.postMessage({ eventName: \"OnLoadTags\", control: \"TagsDisplay__\" }, document.location.origin);\n\tvar tagsContainer = document.getElementById(\"tag-container\");\n\n\tfunction SetTags(evt)\n\t{\n\t\tif(evt && evt.tags)\n\t\t{\n\t\t\tvar allTags = evt.tags.split(\"\\n\");\n\t\t\tallTags.forEach(function(tag) {\n\t\t\t\tvar tagDiv = document.createElement('div');\n\t\t\t\ttagDiv.classList.add('catalog-tag');\n\t\t\t\ttagDiv.innerText = tag;\n\t\t\t\ttagsContainer.appendChild(tagDiv);\n\t\t\t});\n\t\t}\n\t}\n</script>",
																				"css": ".tag-container {\n\ttext-align: center;\n}\n.catalog-tag {\n\tmargin-left: 3px;\n\tmargin-right: 3px;\n}",
																				"version": 0,
																				"width": "100%"
																			},
																			"stamp": 13
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-left-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 14,
											"*": {
												"AddToCartPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "114",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_AddToCartPane",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"hidden": false,
														"sameHeightAsSiblings": false
													},
													"stamp": 15,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"TotalQuantity__": "LabelTotalQuantity__",
																	"LabelTotalQuantity__": "TotalQuantity__"
																},
																"version": 0
															},
															"stamp": 16,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TotalPrice__": {
																				"line": 2,
																				"column": 1
																			},
																			"TotalQuantity__": {
																				"line": 4,
																				"column": 2
																			},
																			"AddToCard__": {
																				"line": 6,
																				"column": 1
																			},
																			"LabelTotalQuantity__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer_line__": {
																				"line": 3,
																				"column": 1
																			},
																			"Spacer_line6__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line7__": {
																				"line": 7,
																				"column": 1
																			},
																			"Spacer_line8__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 17,
																	"*": {
																		"Spacer_line6__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line6",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"TotalPrice__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color1",
																				"version": 1,
																				"label": "_TotalPrice",
																				"activable": true,
																				"width": "100%",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false,
																				"autocompletable": false,
																				"length": 500
																			},
																			"stamp": 19
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"TotalQuantity__": {
																			"type": "Decimal",
																			"data": [],
																			"options": {
																				"version": 2,
																				"textSize": "M",
																				"textAlignment": "center",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": " ",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": "70",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "Number",
																				"autocompletable": false,
																				"defaultValue": "1",
																				"precision_min": 0,
																				"enablePlusMinus": true
																			},
																			"stamp": 21
																		},
																		"AddToCard__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_AddToCart",
																				"label": "_AddToCard",
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "color1",
																				"nextprocess": {
																					"processName": "AP - Application Settings__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"LabelTotalQuantity__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": " ",
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"Spacer_line7__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line7",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"Spacer_line8__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line8",
																				"version": 0
																			},
																			"stamp": 25
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 26,
									"data": []
								},
								"form-content-right": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"hidden": false,
										"box": {
											"top": "0px",
											"left": "400px",
											"width": "calc(100 % - 400px)"
										}
									},
									"*": {
										"form-content-right-4": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 27,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": "180",
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"ItemNumber__": "LabelItem_number__",
																	"LabelItem_number__": "ItemNumber__",
																	"ItemDescription__": "LabelItem_description__",
																	"LabelItem_description__": "ItemDescription__",
																	"ItemUnitPrice__": "LabelItem_unit_price__",
																	"LabelItem_unit_price__": "ItemUnitPrice__",
																	"SupplyTypeID__": "LabelSupply_type_ID__",
																	"LabelSupply_type_ID__": "SupplyTypeID__",
																	"ItemCurrency__": "LabelItemCurrency__",
																	"LabelItemCurrency__": "ItemCurrency__",
																	"UnitOfMeasure__": "LabelUnitOfMeasure__",
																	"LabelUnitOfMeasure__": "UnitOfMeasure__",
																	"SupplyTypeName__": "LabelSupplyTypeName__",
																	"LabelSupplyTypeName__": "SupplyTypeName__",
																	"Locked__": "LabelLocked__",
																	"LabelLocked__": "Locked__",
																	"ItemLongDescription__": "LabelItemLongDescription__",
																	"LabelItemLongDescription__": "ItemLongDescription__",
																	"ItemSupplierPartAuxID__": "LabelItemSupplierPartAuxID__",
																	"LabelItemSupplierPartAuxID__": "ItemSupplierPartAuxID__",
																	"ManufacturerName__": "LabelManufacturerName__",
																	"LabelManufacturerName__": "ManufacturerName__",
																	"ManufacturerPartID__": "LabelManufacturerPartID__",
																	"LabelManufacturerPartID__": "ManufacturerPartID__",
																	"ValidityDate__": "LabelValidityDate__",
																	"LabelValidityDate__": "ValidityDate__",
																	"ExpirationDate__": "LabelExpirationDate__",
																	"LabelExpirationDate__": "ExpirationDate__",
																	"UNSPSC__": "LabelUNSPSC__",
																	"LabelUNSPSC__": "UNSPSC__",
																	"LeadTime__": "LabelLeadTime__",
																	"LabelLeadTime__": "LeadTime__",
																	"LabelItem_company_code__": "ItemCompanyCode__",
																	"ItemCompanyCode__": "LabelItem_company_code__",
																	"ItemType__": "LabelItemType__",
																	"LabelItemType__": "ItemType__",
																	"PunchoutSiteName__": "LabelPunchoutSiteName__",
																	"LabelPunchoutSiteName__": "PunchoutSiteName__",
																	"GradeDisplay__": "LabelGradeDisplay__",
																	"LabelGradeDisplay__": "GradeDisplay__",
																	"Grade__": "LabelGrade__",
																	"LabelGrade__": "Grade__",
																	"Grade_number__": "LabelGrade_number__",
																	"LabelGrade_number__": "Grade_number__",
																	"ItemTags__": "LabelItemTags__",
																	"LabelItemTags__": "ItemTags__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 24,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"ItemNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelItem_number__": {
																				"line": 6,
																				"column": 1
																			},
																			"ItemDescription__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelItem_description__": {
																				"line": 2,
																				"column": 1
																			},
																			"ItemUnitPrice__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelItem_unit_price__": {
																				"line": 11,
																				"column": 1
																			},
																			"SupplyTypeID__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelSupply_type_ID__": {
																				"line": 16,
																				"column": 1
																			},
																			"ItemCurrency__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelItemCurrency__": {
																				"line": 12,
																				"column": 1
																			},
																			"UnitOfMeasure__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelUnitOfMeasure__": {
																				"line": 14,
																				"column": 1
																			},
																			"SupplyTypeName__": {
																				"line": 17,
																				"column": 2
																			},
																			"LabelSupplyTypeName__": {
																				"line": 17,
																				"column": 1
																			},
																			"Spacer_line11__": {
																				"line": 24,
																				"column": 1
																			},
																			"Locked__": {
																				"line": 18,
																				"column": 2
																			},
																			"LabelLocked__": {
																				"line": 18,
																				"column": 1
																			},
																			"ItemLongDescription__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelItemLongDescription__": {
																				"line": 4,
																				"column": 1
																			},
																			"ItemSupplierPartAuxID__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelItemSupplierPartAuxID__": {
																				"line": 7,
																				"column": 1
																			},
																			"ManufacturerName__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelManufacturerName__": {
																				"line": 8,
																				"column": 1
																			},
																			"ManufacturerPartID__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelManufacturerPartID__": {
																				"line": 9,
																				"column": 1
																			},
																			"ValidityDate__": {
																				"line": 19,
																				"column": 2
																			},
																			"LabelValidityDate__": {
																				"line": 19,
																				"column": 1
																			},
																			"ExpirationDate__": {
																				"line": 20,
																				"column": 2
																			},
																			"LabelExpirationDate__": {
																				"line": 20,
																				"column": 1
																			},
																			"LeadTime__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelLeadTime__": {
																				"line": 13,
																				"column": 1
																			},
																			"UNSPSC__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelUNSPSC__": {
																				"line": 15,
																				"column": 1
																			},
																			"LabelItem_company_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"ItemCompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"ItemType__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelItemType__": {
																				"line": 10,
																				"column": 1
																			},
																			"PunchoutSiteName__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelPunchoutSiteName__": {
																				"line": 5,
																				"column": 1
																			},
																			"GradeDisplay__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelGradeDisplay__": {
																				"line": 3,
																				"column": 1
																			},
																			"Grade__": {
																				"line": 21,
																				"column": 2
																			},
																			"LabelGrade__": {
																				"line": 21,
																				"column": 1
																			},
																			"Grade_number__": {
																				"line": 22,
																				"column": 2
																			},
																			"LabelGrade_number__": {
																				"line": 22,
																				"column": 1
																			},
																			"ItemTags__": {
																				"line": 23,
																				"column": 2
																			},
																			"LabelItemTags__": {
																				"line": 23,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelItem_company_code__": {
																			"type": "Label",
																			"data": [
																				"ItemCompanyCode__"
																			],
																			"options": {
																				"label": "_Item company code",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"ItemCompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemCompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Item company code",
																				"activable": true,
																				"width": "300",
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains"
																			},
																			"stamp": 29
																		},
																		"LabelItem_description__": {
																			"type": "Label",
																			"data": [
																				"ItemDescription__"
																			],
																			"options": {
																				"label": "_ItemDescription",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"ItemDescription__": {
																			"type": "LongText",
																			"data": [
																				"ItemDescription__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ItemDescription",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"minNbLines": 1,
																				"numberOfLines": 999
																			},
																			"stamp": 31
																		},
																		"LabelItemLongDescription__": {
																			"type": "Label",
																			"data": [
																				"ItemLongDescription__"
																			],
																			"options": {
																				"label": "_ItemLongDescription",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"ItemLongDescription__": {
																			"type": "LongText",
																			"data": [
																				"ItemLongDescription__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_ItemLongDescription",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 999,
																				"browsable": false
																			},
																			"stamp": 33
																		},
																		"LabelPunchoutSiteName__": {
																			"type": "Label",
																			"data": [
																				"PunchoutSiteName__"
																			],
																			"options": {
																				"label": "_PunchoutSiteName",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"PunchoutSiteName__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"PunchoutSiteName__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_PunchoutSiteName",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"RestrictSearch": true
																			},
																			"stamp": 35
																		},
																		"LabelItem_number__": {
																			"type": "Label",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"ItemNumber__": {
																			"type": "ShortText",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"activable": true,
																				"width": "300",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 37
																		},
																		"LabelItemSupplierPartAuxID__": {
																			"type": "Label",
																			"data": [
																				"ItemSupplierPartAuxID__"
																			],
																			"options": {
																				"label": "_ItemSupplierPartAuxID",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"ItemSupplierPartAuxID__": {
																			"type": "ShortText",
																			"data": [
																				"ItemSupplierPartAuxID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ItemSupplierPartAuxID",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 255,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 39
																		},
																		"LabelManufacturerName__": {
																			"type": "Label",
																			"data": [
																				"ManufacturerName__"
																			],
																			"options": {
																				"label": "_ManufacturerName",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"ManufacturerName__": {
																			"type": "ShortText",
																			"data": [
																				"ManufacturerName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ManufacturerName",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 255,
																				"browsable": false,
																				"autocompletable": false
																			},
																			"stamp": 41
																		},
																		"LabelManufacturerPartID__": {
																			"type": "Label",
																			"data": [
																				"ManufacturerPartID__"
																			],
																			"options": {
																				"label": "_ManufacturerPartID",
																				"version": 0
																			},
																			"stamp": 42
																		},
																		"ManufacturerPartID__": {
																			"type": "ShortText",
																			"data": [
																				"ManufacturerPartID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ManufacturerPartID",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 255,
																				"browsable": false
																			},
																			"stamp": 43
																		},
																		"LabelItemType__": {
																			"type": "Label",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"label": "_ItemType",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"ItemType__": {
																			"type": "ComboBox",
																			"data": [
																				"ItemType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_QuantityBased",
																					"1": "_AmountBased"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "QuantityBased",
																					"1": "AmountBased"
																				},
																				"label": "_ItemType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 45
																		},
																		"LabelItem_unit_price__": {
																			"type": "Label",
																			"data": [
																				"ItemUnitPrice__"
																			],
																			"options": {
																				"label": "_Item unit price",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"ItemUnitPrice__": {
																			"type": "Decimal",
																			"data": [
																				"ItemUnitPrice__"
																			],
																			"options": {
																				"integer": false,
																				"label": "_Item unit price",
																				"precision_internal": 2,
																				"activable": true,
																				"width": "300",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"precision_current": 2,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 47
																		},
																		"LabelItemCurrency__": {
																			"type": "Label",
																			"data": [
																				"ItemCurrency__"
																			],
																			"options": {
																				"label": "_Item currency",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"ItemCurrency__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemCurrency__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Item currency",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"RestrictSearch": true
																			},
																			"stamp": 49
																		},
																		"LabelLeadTime__": {
																			"type": "Label",
																			"data": [
																				"LeadTime__"
																			],
																			"options": {
																				"label": "_LeadTime",
																				"version": 0
																			},
																			"stamp": 50
																		},
																		"LeadTime__": {
																			"type": "Integer",
																			"data": [
																				"LeadTime__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_LeadTime",
																				"precision_internal": 0,
																				"precision_current": 0,
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 51
																		},
																		"LabelUnitOfMeasure__": {
																			"type": "Label",
																			"data": [
																				"UnitOfMeasure__"
																			],
																			"options": {
																				"label": "_UnitOfMeasure",
																				"version": 0
																			},
																			"stamp": 52
																		},
																		"UnitOfMeasure__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"UnitOfMeasure__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_UnitOfMeasure",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains"
																			},
																			"stamp": 53
																		},
																		"LabelUNSPSC__": {
																			"type": "Label",
																			"data": [
																				"UNSPSC__"
																			],
																			"options": {
																				"label": "_UNSPSC",
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"UNSPSC__": {
																			"type": "ShortText",
																			"data": [
																				"UNSPSC__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_UNSPSC",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 55
																		},
																		"LabelSupply_type_ID__": {
																			"type": "Label",
																			"data": [
																				"SupplyTypeID__"
																			],
																			"options": {
																				"label": "_Supply type ID",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"SupplyTypeID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"SupplyTypeID__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Supply type ID",
																				"activable": true,
																				"width": "80",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains"
																			},
																			"stamp": 57
																		},
																		"LabelSupplyTypeName__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_SupplyTypeName",
																				"version": 0
																			},
																			"stamp": 58
																		},
																		"SupplyTypeName__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_SupplyTypeName",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": true,
																				"length": 100,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String"
																			},
																			"stamp": 59
																		},
																		"LabelLocked__": {
																			"type": "Label",
																			"data": [
																				"Locked__"
																			],
																			"options": {
																				"label": "_Locked",
																				"version": 0
																			},
																			"stamp": 60
																		},
																		"Locked__": {
																			"type": "CheckBox",
																			"data": [
																				"Locked__"
																			],
																			"options": {
																				"label": "_Locked",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"LabelValidityDate__": {
																			"type": "Label",
																			"data": [
																				"ValidityDate__"
																			],
																			"options": {
																				"label": "_ValidityDate",
																				"version": 0
																			},
																			"stamp": 62
																		},
																		"ValidityDate__": {
																			"type": "DateTime",
																			"data": [
																				"ValidityDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_ValidityDate",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 63
																		},
																		"LabelExpirationDate__": {
																			"type": "Label",
																			"data": [
																				"ExpirationDate__"
																			],
																			"options": {
																				"label": "_ExpirationDate",
																				"version": 0
																			},
																			"stamp": 64
																		},
																		"ExpirationDate__": {
																			"type": "DateTime",
																			"data": [
																				"ExpirationDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_ExpirationDate",
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 65
																		},
																		"Spacer_line11__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line11",
																				"version": 0
																			},
																			"stamp": 66
																		},
																		"GradeDisplay__": {
																			"type": "HTML",
																			"data": false,
																			"options": {
																				"label": "_GradeDisplay",
																				"htmlContent": "<input id=\"onLoad\" type=\"hidden\" style=\"display:none;\" onclick=\"SetStyle(event);\">\n<div id=\"grade\">\n</div>\n<script type=\"text/javascript\">\n\twindow.postMessage({ eventName: \"OnLoad\", control: \"GradeDisplay__\" }, document.location.origin);\n\tvar elementGrade = document.getElementById(\"grade\");\n\n\tfunction SetStyle(evt)\n\t{\n\t\tfor (var i = 0; i < 5; i++)\n\t\t{\n\t\t\tvar icon = document.createElement(\"i\");\n\t\t\tif (evt.grade >= (i + 0.75))\n\t\t\t{\n\t\t\t\ticon.className = \"fa fa-star\";\n\t\t\t}\n\t\t\telse if ((i + 0.25) <= evt.grade && evt.grade < (i + 0.75))\n\t\t\t{\n\t\t\t\ticon.className = \"fa fa-star-half-o\";\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\ticon.className = \"fa fa-star-o\";\n\t\t\t}\n\t\t\telementGrade.appendChild(icon);\n\t\t}\n\t\tvar gradeNumber = document.createElement(\"span\");\n\t\tgradeNumber.innerText = \" (\" + evt.gradeNumber + \")\";\n\t\telementGrade.appendChild(gradeNumber);\n\t}\n</script>",
																				"css": "",
																				"version": 0,
																				"width": "100%"
																			},
																			"stamp": 67
																		},
																		"LabelGradeDisplay__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "_GradeDisplay",
																				"version": 0
																			},
																			"stamp": 68
																		},
																		"Grade__": {
																			"type": "Decimal",
																			"data": [
																				"Grade__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Grade",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 69
																		},
																		"LabelGrade__": {
																			"type": "Label",
																			"data": [
																				"Grade__"
																			],
																			"options": {
																				"label": "_Grade"
																			},
																			"stamp": 70
																		},
																		"Grade_number__": {
																			"type": "Integer",
																			"data": [
																				"Grade_number__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_Grade_number",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": "80",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false,
																				"browsable": false
																			},
																			"stamp": 71
																		},
																		"LabelGrade_number__": {
																			"type": "Label",
																			"data": [
																				"Grade_number__"
																			],
																			"options": {
																				"label": "_Grade_number"
																			},
																			"stamp": 72
																		},
																		"ItemTags__": {
																			"type": "LongText",
																			"data": [
																				"ItemTags__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_ItemTags",
																				"activable": true,
																				"width": 230
																			},
																			"stamp": 73
																		},
																		"LabelItemTags__": {
																			"type": "Label",
																			"data": [
																				"ItemTags__"
																			],
																			"options": {
																				"label": "_ItemTags"
																			},
																			"stamp": 74
																		}
																	},
																	"stamp": 75
																}
															},
															"stamp": 76,
															"data": []
														}
													},
													"stamp": 77,
													"data": []
												}
											}
										},
										"form-content-right-5": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 78,
											"*": {
												"VendorInformationPanel": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "180",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Vendor details",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"panelStyle": "FlexibleFormPanelLight",
														"sameHeightAsSiblings": false
													},
													"stamp": 79,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelVendorNumber__": "VendorNumber__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"VendorName__": "LabelVendorName__",
																	"LabelVendorName__": "VendorName__",
																	"VendorAddress__": "LabelVendorAddress__",
																	"LabelVendorAddress__": "VendorAddress__",
																	"VendorBusinessStructure__": "LabelVendorBusinessStructure__",
																	"LabelVendorBusinessStructure__": "VendorBusinessStructure__"
																},
																"version": 0
															},
															"stamp": 80,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelVendorNumber__": {
																				"line": 1,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 1,
																				"column": 2
																			},
																			"VendorName__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelVendorName__": {
																				"line": 2,
																				"column": 1
																			},
																			"VendorAddress__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelVendorAddress__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer_line12__": {
																				"line": 5,
																				"column": 1
																			},
																			"VendorBusinessStructure__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelVendorBusinessStructure__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 81,
																	"*": {
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_Vendor number",
																				"version": 0
																			},
																			"stamp": 82
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Vendor number",
																				"activable": true,
																				"width": 300,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains"
																			},
																			"stamp": 83
																		},
																		"LabelVendorName__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_VendorName",
																				"version": 0
																			},
																			"stamp": 84
																		},
																		"VendorName__": {
																			"type": "ShortText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_VendorName",
																				"activable": true,
																				"width": 300,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 85
																		},
																		"LabelVendorBusinessStructure__": {
																			"type": "Label",
																			"data": [
																				"VendorBusinessStructure__"
																			],
																			"options": {
																				"label": "_VendorBusinessStructure"
																			},
																			"stamp": 86
																		},
																		"VendorBusinessStructure__": {
																			"type": "ShortText",
																			"data": [
																				"VendorBusinessStructure__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_VendorBusinessStructure",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"readonly": true,
																				"browsable": false
																			},
																			"stamp": 87
																		},
																		"LabelVendorAddress__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_Vendor address",
																				"version": 0
																			},
																			"stamp": 88
																		},
																		"VendorAddress__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_Vendor address",
																				"activable": true,
																				"width": 300,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 999,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 89
																		},
																		"Spacer_line12__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "10",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line12",
																				"version": 0
																			},
																			"stamp": 90
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-7": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 96,
											"*": {
												"AnalyticsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {
															"collapsed": false
														},
														"labelLength": "180",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_AnalyticsPane",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 97,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelItem_tax_code__": "ItemTaxCode__",
																	"ItemTaxCode__": "LabelItem_tax_code__",
																	"LabelItem_glaccount__": "ItemGLAccount__",
																	"ItemGLAccount__": "LabelItem_glaccount__",
																	"CompanyCode_dup__": "LabelCompanyCode_dup__",
																	"LabelCompanyCode_dup__": "CompanyCode_dup__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__"
																},
																"version": 0
															},
															"stamp": 98,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelItem_tax_code__": {
																				"line": 2,
																				"column": 1
																			},
																			"ItemTaxCode__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelItem_glaccount__": {
																				"line": 4,
																				"column": 1
																			},
																			"ItemGLAccount__": {
																				"line": 4,
																				"column": 2
																			},
																			"Spacer_line4__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line13__": {
																				"line": 6,
																				"column": 1
																			},
																			"CompanyCode_dup__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelCompanyCode_dup__": {
																				"line": 5,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 3,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 99,
																	"*": {
																		"ItemGLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemGLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Item glaccount",
																				"activable": true,
																				"width": "300",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains"
																			},
																			"stamp": 100
																		},
																		"LabelItem_glaccount__": {
																			"type": "Label",
																			"data": [
																				"ItemGLAccount__"
																			],
																			"options": {
																				"label": "_Item glaccount",
																				"version": 0
																			},
																			"stamp": 101
																		},
																		"ItemTaxCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemTaxCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Item tax code",
																				"activable": true,
																				"width": "300",
																				"PreFillFTS": true,
																				"length": 600,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"RestrictSearch": true,
																				"autoCompleteSearchMode": "startsWith",
																				"searchMode": "contains"
																			},
																			"stamp": 102
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType",
																				"version": 0
																			},
																			"stamp": 103
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": "",
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 104
																		},
																		"LabelItem_tax_code__": {
																			"type": "Label",
																			"data": [
																				"ItemTaxCode__"
																			],
																			"options": {
																				"label": "_Item tax code",
																				"version": 0
																			},
																			"stamp": 105
																		},
																		"Spacer_line4__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line4",
																				"version": 0
																			},
																			"stamp": 106
																		},
																		"LabelCompanyCode_dup__": {
																			"type": "Label",
																			"data": [
																				"ItemCompanyCode__"
																			],
																			"options": {
																				"label": "_Item company code",
																				"dataSource": 1,
																				"version": 0
																			},
																			"stamp": 107
																		},
																		"CompanyCode_dup__": {
																			"type": "ShortText",
																			"data": [
																				"ItemCompanyCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Item company code",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"dataSource": 1,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"autocompletable": false
																			},
																			"stamp": 108
																		},
																		"Spacer_line13__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line13",
																				"version": 0
																			},
																			"stamp": 109
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 110,
											"*": {
												"ImagesPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "180",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_ImagesPane",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"panelStyle": "FlexibleFormPanelLight",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 111,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelImage__": "Image__",
																	"Image__": "LabelImage__",
																	"ItemImageBrowse__": "LabelItemImageBrowse__",
																	"LabelItemImageBrowse__": "ItemImageBrowse__"
																},
																"version": 0
															},
															"stamp": 112,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelImage__": {
																				"line": 3,
																				"column": 1
																			},
																			"Image__": {
																				"line": 3,
																				"column": 2
																			},
																			"Spacer_line5__": {
																				"line": 1,
																				"column": 1
																			},
																			"ItemImageBrowse__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelItemImageBrowse__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer_line14__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"lines": 4,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 113,
																	"*": {
																		"Image__": {
																			"type": "ShortText",
																			"data": [
																				"Image__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ItemImage",
																				"activable": true,
																				"width": "300",
																				"helpText": "_Help Item Image",
																				"helpURL": "5015",
																				"helpFormat": "HTML Format",
																				"length": 5000,
																				"browsable": true
																			},
																			"stamp": 114
																		},
																		"LabelItemImageBrowse__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_ItemImageBrowse",
																				"version": 0
																			},
																			"stamp": 115
																		},
																		"ItemImageBrowse__": {
																			"type": "ResourceComboBox",
																			"data": [],
																			"options": {
																				"version": 1,
																				"storeValueAndType": false,
																				"defaultResourceType": "TYPE_IMAGE",
																				"showOnlyDefaultType": true,
																				"allowedExtensions": [
																					"jpg",
																					"jpeg",
																					"png"
																				],
																				"label": "_ItemImageBrowse",
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": true
																			},
																			"stamp": 116
																		},
																		"LabelImage__": {
																			"type": "Label",
																			"data": [
																				"Image__"
																			],
																			"options": {
																				"label": "Image",
																				"version": 0
																			},
																			"stamp": 117
																		},
																		"Spacer_line5__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line5",
																				"version": 0
																			},
																			"stamp": 118
																		},
																		"Spacer_line14__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line14",
																				"version": 0
																			},
																			"stamp": 119
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-right-10": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {},
											"stamp": 141,
											"*": {
												"Inventory_pane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Inventory pane",
														"hidden": true,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 92,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 93,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"InventoryMovementsChart__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 1,
																		"columns": 2,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 94,
																	"*": {
																		"InventoryMovementsChart__": {
																			"type": "Report",
																			"data": false,
																			"options": {
																				"label": "_InventoryMovementsChart",
																				"width": 230,
																				"reportWidth": "Medium",
																				"reportHeight": "Medium",
																				"reportName": "_ReportName_PA - InventoryHistoryMovementsByWarehouse",
																				"reportTitle": "_InventoryMovementsTitle",
																				"delayLoad": true,
																				"version": 0
																			},
																			"stamp": 95
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 120,
									"data": []
								}
							},
							"stamp": 121,
							"data": []
						}
					},
					"stamp": 122,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 123,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								}
							},
							"stamp": 124,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 125,
							"data": []
						}
					},
					"stamp": 126,
					"data": []
				}
			},
			"stamp": 127,
			"data": []
		}
	},
	"stamps": 141,
	"data": []
}