///#GLOBALS Lib
/* Purchase order HTML page script */
var topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
var ConfirmDelivery_RetryFinalizePO = false;
var banner = Sys.Helpers.Banner;
banner.SetStatusCombo(Controls.GRStatus__);
banner.SetHTMLBanner(Controls.HTMLBanner__);
banner.SetMainTitle("_Goods receipt");
/** ************** **/
/** Global Helpers **/
/** ************** **/
var scoringHelper = {
    Init: function () {
        var scoringFeatureEnabled = !ProcessInstance.isReadOnly && Lib.AP && typeof Lib.AP.Scoring !== "undefined";
        var scoringFeatureDisabled = !scoringFeatureEnabled;
        Controls.ScoringPane.Hide(scoringFeatureDisabled);
        Controls.ScoringValue__.Hide(true);
        Controls.Scoring__.Hide(scoringFeatureDisabled);
        Controls.ScoringComment__.Hide(scoringFeatureDisabled);
        if (scoringFeatureEnabled) {
            Controls.ScoringComment__.SetPlaceholder(Language.Translate("_Enter your evaluation"));
            Controls.Scoring__.BindEvent("OnLoad", scoringHelper.OnLoad);
            Controls.Scoring__.BindEvent("OnClick", scoringHelper.OnClick);
        }
    },
    OnLoad: function () {
        var score = Data.GetValue("ScoringValue__");
        Controls.Scoring__.FireEvent("onLoad", { score: score });
    },
    OnClick: function (evt) {
        Data.SetValue("ScoringValue__", parseInt(evt.args, 10));
    }
};
function SetBannerLabel(status) {
    switch (status) {
        case "To receive":
            banner.SetSubTitle("_Banner goods to receive");
            break;
        case "Canceled":
            banner.SetSubTitle("_Banner goods canceled");
            break;
        default:
            banner.SetSubTitle("_Banner goods received");
    }
}
function CheckErrorOnRow(row) {
    var item = row.GetItem();
    var isAmountBased = Lib.Purchasing.Items.IsAmountBasedItem(item);
    var receivedField = isAmountBased ? row.NetAmount__ : row.ReceivedQuantity__;
    var openField = isAmountBased ? row.ItemOpenAmount__ : row.OpenQuantity__;
    var orderedField = isAmountBased ? row.ItemOrderedAmount__ : row.OrderedQuantity__;
    var overReceivedWarningMsg = isAmountBased ? "_You are receiving {0} more amounts than remaining" : "_You are receiving {0} more item(s) than remaining";
    var overReceivedErrorMsg = isAmountBased ? "_Received amount outmatch ordered amount" : "_Received qty outmatch ordered qty";
    var cancelRemainingWarningMsg = isAmountBased ? "_You will cancel the reception of {0} amount" : "_You will cancel the reception of {0} item(s)";
    var isReceivedValueEmpty = receivedField.GetValue() === null || receivedField.GetValue() === undefined;
    var isOverReceiving = receivedField.GetValue() > openField.GetValue();
    var precision = receivedField.GetPrecision() || 2; // GetPrecision doesn't exist ?!
    if (receivedField.GetValue() >= openField.GetValue()) {
        row.DeliveryCompleted__.Check(true);
        row.DeliveryCompleted__.SetReadOnly(true);
    }
    else {
        row.DeliveryCompleted__.SetReadOnly(false);
    }
    if (isReceivedValueEmpty && row.DeliveryCompleted__.IsChecked()) {
        /**
         * Received quantity is mandatory when DeliveryCompleted__ is checked
         */
        receivedField.SetError("_Received qty is required when no more reception is marked");
        receivedField.ShowErrorMessage(true);
    }
    else if (openField.GetValue() - receivedField.GetValue() > orderedField.GetValue()
        || isOverReceiving) {
        /**
         * Received value can't outmatch ordered value if not allowed by the user exit Lib.GR.Customization.Common.AllowOverdelivery
         * Received value can be negative, so we need to check that we didn't un-received more than we have already received
         */
        if (isOverReceiving && Sys.Helpers.TryCallFunction("Lib.GR.Customization.Common.AllowOverdelivery", item)) {
            var extraValueReceived = Sys.Helpers.Round(new Sys.Decimal(receivedField.GetValue()).minus(openField.GetValue()).toNumber(), precision).toFixed(precision);
            receivedField.SetWarning(Language.Translate(overReceivedWarningMsg, true, Language.FormatNumber(extraValueReceived, false)));
        }
        else {
            receivedField.SetError(overReceivedErrorMsg);
        }
    }
    else if (row.DeliveryCompleted__.IsChecked() && openField.GetValue() - receivedField.GetValue() > 0 && Sys.Helpers.IsEmpty(receivedField.GetWarning())) {
        var remainingValueToCancel = Sys.Helpers.Round(new Sys.Decimal(openField.GetValue()).minus(receivedField.GetValue()).toNumber(), precision).toFixed(precision);
        receivedField.SetWarning(Language.Translate(cancelRemainingWarningMsg, true, Language.FormatNumber(remainingValueToCancel, false)));
        receivedField.ShowErrorMessage(true);
    }
    else {
        receivedField.SetWarning("");
        receivedField.SetError("");
        receivedField.ShowErrorMessage(false);
    }
    if (isReceivedValueEmpty) {
        // Item is not received anymore, the budget closed/missing error on the DeliveryDate__ field may be reset if date is not invalid
        Controls.DeliveryDate__.OnChange();
    }
}
function CheckErrorOnTable() {
    for (var i = Controls.LineItems__.GetLineCount(true) - 1; i >= 0; i--) {
        var row = Controls.LineItems__.GetRow(i);
        CheckErrorOnRow(row);
        SetHiddenValues(row);
    }
}
function CheckConfirmDelivery() {
    var table = Data.GetTable("LineItems__");
    var nItems = table.GetItemCount();
    var disableConfirm = true;
    for (var i = 0; i < nItems; i++) {
        var lineItem = table.GetItem(i);
        if (lineItem.GetValue("NetAmount__") || lineItem.GetValue("ReceivedQuantity__") || lineItem.GetValue("DeliveryCompleted__")) {
            disableConfirm = false;
            break;
        }
    }
    Controls.ConfirmDelivery__.SetDisabled(disableConfirm);
}
function SetHiddenValues(row) {
    if (Lib.Purchasing.Items.IsAmountBasedItem(row.GetItem())) {
        row.ReceivedQuantity__.SetValue(row.NetAmount__.GetValue());
    }
    else {
        var receivedQuantity = row.ReceivedQuantity__.GetValue();
        var unitPrice = row.UnitPrice__.GetValue();
        var decimalHelper = Sys.Decimal.set({ rounding: Sys.Decimal.ROUND_DOWN });
        var netAmount = receivedQuantity ? new decimalHelper(receivedQuantity).mul(unitPrice).toNumber() : null;
        row.NetAmount__.SetValue(netAmount);
    }
}
var CancelManager = (function () {
    var CancelConfirmationPopup = (function () {
        var $comment = null;
        var onCommitted = null;
        var commentRequired = false;
        function Fill(dialog /*, tabId, event, control*/) {
            var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
            ctrl.SetText(Language.Translate("_GR cancel confirmation message"));
            var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
            if (commentRequired) {
                dialog.RequireControl(commentCtrl);
            }
        }
        function Commit(dialog /*, tabId, event, control*/) {
            $comment = dialog.GetControl("ctrlComments").GetValue();
            if (Sys.Helpers.IsFunction(onCommitted)) {
                onCommitted();
            }
        }
        function Validate(dialog /*, tabId, event, control*/) {
            if (commentRequired && !dialog.GetControl("ctrlComments").GetValue()) {
                dialog.GetControl("ctrlComments").SetError("This field is required!");
                return false;
            }
            return true;
        }
        function Display(_onCommitted, _commentRequired) {
            $comment = null;
            onCommitted = _onCommitted;
            commentRequired = _commentRequired;
            Popup.Dialog("_GR cancel confirmation", null, Fill, Commit, Validate);
        }
        return {
            get comment() { return $comment; },
            Display: Display
        };
    })();
    function Init() {
        var canceled = Controls.GRStatus__.GetValue() === "Canceled";
        var cancelable = (ProcessInstance.state === 70 || ProcessInstance.state === 100) && !canceled;
        Controls.CancelComment__.Hide(!canceled);
        Controls.Cancel__.Hide(!cancelable);
        Controls.Cancel__.OnClick = function () {
            CancelConfirmationPopup.Display(Cancel, !Sys.Helpers.IsEmpty(Controls.GRNumber__.GetValue()));
        };
    }
    var Cancel = (function () {
        function HideWaitScreen(hide) {
            // async call just after boot
            setTimeout(function () {
                Controls.OrderNumber__.Wait(!hide);
            });
        }
        var grCanceler = {
            creationErrorMessage: null,
            resultNextAlertVariable: null,
            table: null,
            msnEx: null,
            refreshStatusInterval: null,
            queryPending: false,
            ended: false,
            syncToken: null
        };
        function CreateAndWaitForGRCanceler(token) {
            HideWaitScreen(false);
            // 1- Create GR canceler
            Process.CreateProcessInstance("Goods receipt canceler", {
                GRRuidEx__: Data.GetValue("RuidEx"),
                CancelComment__: CancelConfirmationPopup.comment
            }, {
                "ERP": Data.GetValue("ERP__")
            }, {
                callback: function (data) {
                    if (data.error) {
                        grCanceler.creationErrorMessage = data.errorMessage;
                        token.Use();
                    }
                    else {
                        Log.Info("Canceler created with ruidEx: " + data.ruid);
                        grCanceler.table = data.ruid.replace(/\.[^\.]+$/, "");
                        grCanceler.msnEx = data.ruid.replace(/^[^\.]+\./, "");
                        grCanceler.syncToken = token;
                        // 2- Wait for the end of GR canceler (polling...)
                        grCanceler.refreshStatusInterval = setInterval(RefreshGRCancelerStatus, 2000);
                    }
                }
            });
        }
        function RefreshGRCancelerStatus() {
            if (!grCanceler.queryPending) {
                grCanceler.queryPending = true;
                Query.DBQuery(OnRequestResult, grCanceler.table, "State|EXTERNAL_VARIABLE_CommonDialog_NextAlert%FORMATTED", "msnex=" + grCanceler.msnEx, "", 1);
            }
        }
        function OnRequestResult() {
            grCanceler.queryPending = false;
            if (grCanceler.ended) {
                return;
            }
            var err = this.GetQueryError();
            if (err) {
                Log.Error("Query Error: " + err);
                return;
            }
            var recordsCount = this.GetRecordsCount();
            if (recordsCount !== 1) {
                return;
            }
            var state = this.GetQueryValue("State", 0);
            if (state === 70 || state >= 100) {
                grCanceler.ended = true;
                clearInterval(grCanceler.refreshStatusInterval);
                grCanceler.refreshStatusInterval = null;
                grCanceler.resultNextAlertVariable = this.GetQueryValue("EXTERNAL_VARIABLE_CommonDialog_NextAlert%FORMATTED", 0);
                grCanceler.syncToken.Use();
            }
        }
        function DisplayGRCancelerResult() {
            HideWaitScreen(true);
            if (grCanceler.resultNextAlertVariable) {
                Variable.SetValueAsString("CommonDialog_NextAlert", grCanceler.resultNextAlertVariable);
            }
            else {
                var reason = "Unexpected error.";
                if (grCanceler.creationErrorMessage) {
                    Log.Error("GR canceler creation error. Details: " + grCanceler.creationErrorMessage);
                    reason = grCanceler.creationErrorMessage;
                }
                Lib.CommonDialog.NextAlert.Define("_GR cancel error", "_GR cancel error message", {
                    isError: true,
                    behaviorName: "GRCancelFailure"
                }, reason);
            }
            Lib.CommonDialog.NextAlert.Show({
                "GRCancelSuccess": {
                    OnOK: function () {
                        ProcessInstance.Quit("quit");
                    }
                },
                "GRCancelFailure": {
                    OnOK: function () {
                        ProcessInstance.Quit("quit");
                    }
                },
                "GRCancelAborted": {
                    OnOK: function () {
                        ProcessInstance.Quit("quit");
                    }
                }
            });
        }
        return function () {
            if (Sys.Helpers.IsEmpty(Controls.GRNumber__.GetValue())) {
                // GR is in Draft, the result of a GR creation error in ERP, in this case,
                // no GR created in ERP, no budget impacted, no GR items created...
                // --> just cancel the current message.
                Controls.CancelComment__.SetValue(CancelConfirmationPopup.comment);
                ProcessInstance.ApproveAsynchronous("Cancel");
            }
            else {
                var syncCancel = Sys.Helpers.Synchronizer.Create(function () {
                    // defer call to avoid stacking popup
                    setTimeout(DisplayGRCancelerResult);
                }, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
                syncCancel.userData = {
                    dialogTitle: "_Form awaiting GR cancel",
                    dialogMessage: "_Form awaiting GR cancel message"
                };
                syncCancel.Register(CreateAndWaitForGRCanceler);
                syncCancel.Start();
            }
        };
    })();
    return {
        Init: Init,
        Cancel: Cancel
    };
})();
/** ****** **/
/** LAYOUT **/
/** ****** **/
Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1ID__", "Code__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1ID__"));
Sys.Helpers.Controls.OnDatabaseComboboxEvents(Controls.LineItems__.FreeDimension1ID__, Lib.P2P.FreeDimension.DefineOnSelectItem("FreeDimension1__", "Description__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"), Lib.P2P.FreeDimension.DefineOnUnknownOrEmptyValue("FreeDimension1__"));
Controls.LineItems__.ReceivedQuantity__.OnChange = function () {
    var currentRow = this.GetRow();
    CheckErrorOnRow(currentRow);
    SetHiddenValues(currentRow);
    CheckConfirmDelivery();
};
Controls.LineItems__.DeliveryCompleted__.OnChange = function () {
    var currentRow = this.GetRow();
    CheckErrorOnRow(currentRow);
    SetHiddenValues(currentRow);
    CheckConfirmDelivery();
};
Controls.LineItems__.NetAmount__.OnChange = function () {
    var currentRow = this.GetRow();
    var decimalHelper = Sys.Decimal.set({ rounding: Sys.Decimal.ROUND_DOWN });
    var orderedAmount = currentRow.ItemOrderedAmount__.GetValue();
    var receivedAmount = currentRow.NetAmount__.GetValue();
    var percentageReceived = null;
    if (receivedAmount) {
        percentageReceived = new decimalHelper(receivedAmount).div(orderedAmount).mul(100).toNumber();
    }
    else if (receivedAmount === 0) {
        percentageReceived = 0;
    }
    currentRow.PercentageReceived__.SetValue(percentageReceived);
    CheckErrorOnRow(currentRow);
    SetHiddenValues(currentRow);
    CheckConfirmDelivery();
};
Controls.LineItems__.PercentageReceived__.OnChange = function () {
    var currentRow = this.GetRow();
    var decimalHelper = Sys.Decimal.set({ rounding: Sys.Decimal.ROUND_DOWN });
    var percentageReceived = currentRow.PercentageReceived__.GetValue();
    var orderedAmount = currentRow.ItemOrderedAmount__.GetValue();
    var receivedAmount = null;
    if (percentageReceived) {
        receivedAmount = new decimalHelper(orderedAmount).mul(percentageReceived).div(100).toNumber();
    }
    else if (percentageReceived === 0) {
        receivedAmount = 0;
    }
    currentRow.NetAmount__.SetValue(receivedAmount);
    CheckErrorOnRow(currentRow);
    SetHiddenValues(currentRow);
    CheckConfirmDelivery();
};
Controls.DeliveryDate__.OnChange = function () {
    var curDate = Data.GetValue("DeliveryDate__");
    var nowDate = new Date();
    var isNotToday = curDate === null || curDate.getDate() != nowDate.getDate() || curDate.getMonth() != nowDate.getMonth() || curDate.getFullYear() != nowDate.getFullYear();
    if (curDate > nowDate && isNotToday) {
        Controls.DeliveryDate__.SetError("_Invalid date");
    }
    else {
        Controls.DeliveryDate__.SetError("");
    }
};
Controls.ConfirmDelivery__.OnClick = function () {
    if (ConfirmDelivery_RetryFinalizePO) {
        ProcessInstance.Approve("Retry_FinalizePO_");
        return false;
    }
    return Process.ShowFirstError() === null;
};
var GlobalLayout = {
    panes: ["TopPaneWarning",
        "Spacer",
        "Banner",
        "GeneralInformation",
        "LineItems",
        "Actions",
        "ScoringPane",
        "DocumentsPanel"].map(function (name) { return Controls[name]; }),
    HideWaitScreen: function (hide) {
        // async call just after boot
        setTimeout(function () {
            Controls.CompanyCode__.Wait(!hide);
        });
    }
};
function FillGR(token) {
    Log.Time("FillGoodReceipt");
    Log.Info("Filling GR Form with items from PO");
    var options = { orderByClause: null, ancestorsids: null };
    options.orderByClause = "LineNumber__ ASC";
    options.ancestorsids = ProcessInstance.selectedRuidFromView;
    if (options.ancestorsids) {
        Lib.Purchasing.GRItems.FillForm(options)
            .Then(function () {
            Log.TimeEnd("FillGoodReceipt");
            token.Use();
        })
            .Catch(function (e) {
            Lib.CommonDialog.NextAlert.Define("_Goods receipt creation error", e, { isError: true, behaviorName: "GRInitError" });
            Log.TimeEnd("FillGoodReceipt");
            token.Use();
        });
    }
    else {
        token.Use();
    }
}
function FixLayoutBeforeStarting() {
    Log.Info("FixLayoutBeforeStarting");
    Process.ShowFirstErrorAfterBoot(false);
    GlobalLayout.panes.forEach(function (pane) {
        pane.Hide(true);
    });
    GlobalLayout.HideWaitScreen(false);
}
function FixLayout() {
    GlobalLayout.panes.forEach(function (pane) {
        pane.Hide(false);
    });
    Controls.GRNumber__.Hide(!Controls.GRNumber__.GetText());
    Controls.LineItems__.SetWidth("100%");
    Controls.LineItems__.SetExtendableColumn("Description__");
    Controls.LineItems__.SetRowToolsHidden(true);
    // Hides the recipient column when none is found
    var table = Data.GetTable("LineItems__");
    var nItems = table.GetItemCount();
    var GRItem;
    var hideRecipient = true;
    var hideRequester = true;
    var recipient = "";
    var requesterName = "";
    var nbreItemsWithQte = 0;
    var isUnitOfMeasureEnabled = Sys.Parameters.GetInstance("PAC").GetParameter("DisplayUnitOfMeasure");
    for (var i = 0; i < nItems; i++) {
        GRItem = table.GetItem(i);
        recipient = GRItem.GetValue("RecipientDN__");
        requesterName = GRItem.GetValue("RequesterDN__");
        if (recipient && hideRecipient) {
            hideRecipient = false;
        }
        if (requesterName && hideRequester) {
            hideRequester = false;
        }
        var row = Controls.LineItems__.GetRow(i);
        var isAmountBased = Lib.Purchasing.Items.IsAmountBasedItem(GRItem);
        row.OrderedQuantity__.Hide(isAmountBased);
        row.OpenQuantity__.Hide(isAmountBased);
        row.ReceivedQuantity__.Hide(isAmountBased);
        row.ReceivedQuantity__.SetReadOnly(isAmountBased);
        row.ItemUnit__.Hide(isAmountBased);
        row.ItemUnitDescription__.Hide(isAmountBased);
        row.PercentageReceived__.Hide(!isAmountBased);
        row.PercentageReceived__.AddSurroundingText(isAmountBased ? "%" : "");
        row.PercentageReceived__.SetReadOnly(!isAmountBased);
        row.NetAmount__.SetReadOnly(!isAmountBased);
        if (isAmountBased) {
            row.ItemUnit__.SetRequired(false);
        }
        else {
            row.ItemUnit__.SetRequired(isUnitOfMeasureEnabled);
            nbreItemsWithQte++;
        }
    }
    var recipientDN = table.GetItem(0).GetValue("RecipientDN__") || "";
    var isBuyerOrBackUp = User.loginId.toUpperCase() === recipientDN.toUpperCase()
        || User.IsMemberOf(recipientDN)
        || User.IsBackupUserOf(recipientDN);
    if (isBuyerOrBackUp) {
        Process.SetHelpId("5032");
    }
    Controls.LineItems__.RecipientName__.Hide(hideRecipient);
    Controls.LineItems__.RequesterName__.Hide(hideRequester);
    Controls.ConfirmDelivery__.SetDisabled(true);
    if (nbreItemsWithQte === 0) {
        Controls.LineItems__.OrderedQuantity__.Hide(true);
        Controls.LineItems__.OpenQuantity__.Hide(true);
        Controls.LineItems__.ReceivedQuantity__.Hide(true);
        Controls.LineItems__.ItemUnit__.Hide(true);
        Controls.LineItems__.ItemUnitDescription__.Hide(true);
    }
    else {
        Controls.LineItems__.ItemUnit__.Hide(!isUnitOfMeasureEnabled);
    }
    if (nbreItemsWithQte === nItems) {
        Controls.LineItems__.ItemOrderedAmount__.Hide(true);
        Controls.LineItems__.ItemOpenAmount__.Hide(true);
        Controls.LineItems__.PercentageReceived__.Hide(true);
        Controls.LineItems__.NetAmount__.Hide(true);
    }
    if ((ProcessInstance.state === 100) || ProcessInstance.isReadOnly) {
        Controls.LineItems__.OpenQuantity__.Hide(true);
        Controls.LineItems__.ItemOpenAmount__.Hide(true);
        Controls.LineItems__.PercentageReceived__.Hide(true);
    }
    // Make Order number field clickable
    var sourceRuid = Data.GetValue("SourceRUID");
    var sourcePONum = Data.GetValue("OrderNumber__") || Variable.GetValueAsString("OrderNumber__");
    if (sourceRuid || sourcePONum) {
        Controls.OrderNumber__.DisplayAs({ type: "Link" });
        Controls.OrderNumber__.OnClick = function () {
            if (sourceRuid) {
                Process.OpenMessage(sourceRuid, true, true);
            }
            else {
                Sys.GenericAPI.PromisedQuery({
                    table: "CDNAME#Purchase order V2",
                    filter: "OrderNumber__=" + sourcePONum,
                    attributes: ["ValidationURL"],
                    sortOrder: "",
                    maxRecords: 1,
                    additionalOptions: { searchInArchive: true }
                }).Then(function (results) {
                    if (results && results.length) {
                        Process.OpenLink({ url: results[0].ValidationURL, inCurrentTab: false });
                    }
                    else {
                        Popup.Alert("_Purchase order not found or access denied", false, null, "_Purchase order not found title");
                    }
                });
            }
        };
    }
    if (Sys.Parameters.GetInstance("PAC").GetParameter("MultiShipTo", false)) {
        Controls.LineItems__.ItemShipToCompany__.Hide(false);
        Controls.LineItems__.ItemShipToCompany__.SetReadOnly(true);
        Controls.LineItems__.ItemShipToAddress__.Hide(false);
        Controls.LineItems__.ItemShipToAddress__.SetReadOnly(true);
    }
    Lib.P2P.InitItemTypeControl(Controls.LineItems__.ItemType__);
    Log.TimeStamp("HideWaitScreen");
    GlobalLayout.HideWaitScreen(true);
}
/** ******************** **/
/** FORM INITIALIZATION  **/
/** ******************** **/
Sys.Helpers.EnableSmartSilentChange();
// START - ignore all changes on form during the initialization processing
ProcessInstance.SetSilentChange(true);
function Start() {
    Log.Time("Start");
    Lib.Purchasing.InitTechnicalFields();
    Lib.Purchasing.Browse.Init();
    FixLayout();
    SetBannerLabel(Controls.GRStatus__.GetValue());
    // This allows to enable confirm receiption if we're back after an error
    if (!ProcessInstance.isReadOnly) {
        CheckErrorOnTable();
        CheckConfirmDelivery();
    }
    scoringHelper.Init();
    CancelManager.Init();
    /* All the layout is fixed, try to call for PS customization */
    var func = Sys.Helpers.TryGetFunction("Lib.GR.Customization.Client.CustomizeLayout");
    if (func) {
        func();
    }
    else {
        Sys.Helpers.TryCallFunction("Lib.GR.Customization.Client.CustomiseLayout");
    }
    Lib.CommonDialog.NextAlert.Show({
        "GRCreationInfo": {
            IsShowable: function () {
                // show info when GR has been just terminated
                return Data.GetActionName();
            },
            OnOK: function () {
                ProcessInstance.Quit("quit");
            }
        },
        "GRCreationError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesNoCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("Continue_");
                            break;
                        case "No":
                            ProcessInstance.Approve("Retry_");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit("Quit__");
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_GR found in ERP", "_GR not found in ERP");
            }
        },
        "finalizePOError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("Retry_FinalizePO_");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit("Quit__");
                            Controls.ConfirmDelivery__.SetDisabled(false);
                            ConfirmDelivery_RetryFinalizePO = true;
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_Retry finalize PO", "_Retry finalize PO later");
            }
        },
        "GREditionError": {
            Popup: function (nextAlert) {
                Lib.CommonDialog.NextAlert.PopupYesCancel(function (action) {
                    switch (action) {
                        case "Yes":
                            ProcessInstance.Approve("RetryOnEditOrder");
                            break;
                        case "Cancel":
                            Lib.Purchasing.SetDocumentReadonlyAndHideAllButtonsExceptQuit("Quit__");
                            break;
                        default:
                            break;
                    }
                }, nextAlert, "_Retry now", "_Retry later");
            }
        }
    });
    Controls.TopPaneWarning.Hide(true);
    Lib.P2P.DisplayArchiveDurationWarning("PAC", function () {
        topMessageWarning.Add(Language.Translate("_Archive duration warning"));
    });
    var additionalDisplayCondition = Data.GetValue("GRStatus__") === "Received";
    Lib.P2P.DisplayAdminWarning("PAC", function (displayName) {
        topMessageWarning.Add(Language.Translate("_View as admin on bealf of {0}", false, displayName));
    }, additionalDisplayCondition, [100]);
    Lib.P2P.DisplayBackupUserWarning("PAC", function (displayName) {
        topMessageWarning.Add(Language.Translate("_View on bealf of {0}", false, displayName));
    });
    if (!ProcessInstance.state) {
        ProcessInstance.DisableExtractionScript();
    }
    Log.TimeEnd("Start");
}
FixLayoutBeforeStarting();
var syncBeforeStarting = Sys.Helpers.Synchronizer.Create(function () {
    Start();
    // END - ignore all changes on form during the initialization processing
    ProcessInstance.SetSilentChange(false);
}, null, { progressDelay: 15000, OnProgress: Sys.Helpers.Synchronizer.OnProgress });
if (!Data.GetValue("State")) {
    Lib.CommonDialog.NextAlert.Reset();
    syncBeforeStarting.Register(FillGR);
}
Log.Time("LoadParameters");
Lib.Purchasing.LoadParameters().Then(function () {
    Log.TimeEnd("LoadParameters");
    syncBeforeStarting.Synchronize(function () {
        Log.TimeEnd("Synchronizer");
    });
    Log.Time("Synchronizer");
    syncBeforeStarting.Start();
});
