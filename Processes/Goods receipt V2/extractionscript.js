///#GLOBALS Lib
if (Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
    Lib.CommonDialog.NextAlert.Reset();
    var options = { autoReceiveOrderData: null, orderByClause: null };
    options.autoReceiveOrderData = Lib.Purchasing.GetAutoReceiveOrderData();
    options.orderByClause = "LineNumber__ ASC";
    Log.Info("Filling GR Form with items from PO");
    Lib.Purchasing.GRItems.FillForm(options)
        .Then(function () {
        Lib.Purchasing.InitTechnicalFields();
    })
        .Catch(function (e) {
        Lib.CommonDialog.NextAlert.Define("_Goods receipt creation error", e, { isError: true, behaviorName: "GRInitError" });
    });
}
