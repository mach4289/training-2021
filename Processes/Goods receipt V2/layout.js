{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1700,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning",
												"hideSeparator": true
											},
											"*": {
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Spacer": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Spacer",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {},
																				"lines": 0,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 15,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 16,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 17,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner__",
																						"width": 3802,
																						"htmlContent": "Goods Receipt",
																						"version": 0
																					},
																					"stamp": 18
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"GeneralInformation": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": "170",
																"iconURL": "GR_information.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_GeneralInformation",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"OrderNumber__": "LabelOrderNumber__",
																			"LabelOrderNumber__": "OrderNumber__",
																			"DeliveryDate__": "LabelDeliveryDate__",
																			"LabelDeliveryDate__": "DeliveryDate__",
																			"DeliveryNote__": "LabelDeliveryNote__",
																			"LabelDeliveryNote__": "DeliveryNote__",
																			"Comment__": "LabelComment__",
																			"LabelComment__": "Comment__",
																			"GRNumber__": "LabelGRNumber__",
																			"LabelGRNumber__": "GRNumber__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"GRStatus__": "LabelGRStatus__",
																			"LabelGRStatus__": "GRStatus__",
																			"CancelComment__": "LabelCancelComment__",
																			"LabelCancelComment__": "CancelComment__",
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 9,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"OrderNumber__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelOrderNumber__": {
																						"line": 2,
																						"column": 1
																					},
																					"DeliveryDate__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelDeliveryDate__": {
																						"line": 4,
																						"column": 1
																					},
																					"DeliveryNote__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelDeliveryNote__": {
																						"line": 5,
																						"column": 1
																					},
																					"Comment__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelComment__": {
																						"line": 6,
																						"column": 1
																					},
																					"GRNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelGRNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 7,
																						"column": 1
																					},
																					"GRStatus__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelGRStatus__": {
																						"line": 8,
																						"column": 1
																					},
																					"CancelComment__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelCancelComment__": {
																						"line": 9,
																						"column": 1
																					},
																					"ERP__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelGRNumber__": {
																					"type": "Label",
																					"data": [
																						"GRNumber__"
																					],
																					"options": {
																						"label": "_GRNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 19
																				},
																				"GRNumber__": {
																					"type": "ShortText",
																					"data": [
																						"GRNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_GRNumber",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false
																					},
																					"stamp": 20
																				},
																				"LabelOrderNumber__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_OrderNumber",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_OrderNumber",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 23
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SAP",
																							"1": "_Generic",
																							"2": "_EBS",
																							"3": "_NAV",
																							"4": "_JDE"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SAP",
																							"1": "generic",
																							"2": "EBS",
																							"3": "NAV",
																							"4": "JDE"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 24
																				},
																				"LabelDeliveryDate__": {
																					"type": "Label",
																					"data": [
																						"DeliveryDate__"
																					],
																					"options": {
																						"label": "_DeliveryDate",
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"DeliveryDate__": {
																					"type": "DateTime",
																					"data": [
																						"DeliveryDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_DeliveryDate",
																						"activable": true,
																						"width": "180",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"LabelDeliveryNote__": {
																					"type": "Label",
																					"data": [
																						"DeliveryNote__"
																					],
																					"options": {
																						"label": "_DeliveryNote",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"DeliveryNote__": {
																					"type": "ShortText",
																					"data": [
																						"DeliveryNote__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_DeliveryNote",
																						"activable": true,
																						"width": "180",
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"LabelComment__": {
																					"type": "Label",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"label": "_Comment",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"Comment__": {
																					"type": "LongText",
																					"data": [
																						"Comment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Comment",
																						"activable": true,
																						"width": "500",
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 3
																					},
																					"stamp": 30
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 31
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 32
																				},
																				"LabelGRStatus__": {
																					"type": "Label",
																					"data": [
																						"GRStatus__"
																					],
																					"options": {
																						"label": "_GRStatus",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 33
																				},
																				"GRStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"GRStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Goods to receive",
																							"1": "_Goods received",
																							"2": "_Goods canceled"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "To receive",
																							"1": "Received",
																							"2": "Canceled"
																						},
																						"label": "_GRStatus",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 34
																				},
																				"LabelCancelComment__": {
																					"type": "Label",
																					"data": [
																						"CancelComment__"
																					],
																					"options": {
																						"label": "_CancelComment",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 35
																				},
																				"CancelComment__": {
																					"type": "LongText",
																					"data": [
																						"CancelComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_CancelComment",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0,
																						"minNbLines": 3,
																						"hidden": true
																					},
																					"stamp": 36
																				}
																			},
																			"stamp": 37
																		}
																	},
																	"stamp": 38,
																	"data": []
																}
															},
															"stamp": 39,
															"data": []
														},
														"ScoringPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "GR_Scoring.png",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ScoringPane",
																"hidden": true,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"isSvgIcon": false,
																"version": 0,
																"labelLength": 0
															},
															"stamp": 235,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Scoring__": "LabelScoring__",
																			"LabelScoring__": "Scoring__",
																			"ScoringValue__": "LabelScoringValue__",
																			"LabelScoringValue__": "ScoringValue__",
																			"ScoringComment__": "LabelScoringComment__",
																			"LabelScoringComment__": "ScoringComment__"
																		},
																		"version": 0
																	},
																	"stamp": 236,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Scoring__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelScoring__": {
																						"line": 1,
																						"column": 1
																					},
																					"ScoringValue__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelScoringValue__": {
																						"line": 2,
																						"column": 1
																					},
																					"ScoringComment__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelScoringComment__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 237,
																			"*": {
																				"LabelScoring__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_Scoring",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 256
																				},
																				"Scoring__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_Scoring",
																						"htmlContent": "<input id=\"onLoad\" style=\"display:none;\" onclick=\"Init(event);\" />\n<div id=\"score\" style=\"display:none;\"></div>\n<div id=\"scoring\" style=\"cursor: pointer\">\n\t<i id=\"scoringstar0\" class=\"fa fa-2x fa-star-o\"></i>\n\t<i id=\"scoringstar1\" class=\"fa fa-2x fa-star-o\"></i>\n\t<i id=\"scoringstar2\" class=\"fa fa-2x fa-star-o\"></i>\n\t<i id=\"scoringstar3\" class=\"fa fa-2x fa-star-o\"></i>\n\t<i id=\"scoringstar4\" class=\"fa fa-2x fa-star-o\"></i>\n</div>\n\n<script type=\"text/javascript\">\n\tvar originControl = \"Scoring__\";\n\tvar eltScore = document.getElementById(\"score\");\n\tvar eltScoring = document.getElementById(\"scoring\");\n\tvar eltStar = []\n\tfor (var i = 0; i < 5; i++)\n\t{\n\t\teltStar.push(document.getElementById(\"scoringstar\" + i));\n\t}\n\n    window.postMessage({ eventName: \"OnLoad\", control: originControl }, document.location.origin);\n\tAddEvents();\n\n\tfunction AddEvents()\n\t{\n\t\tfor (var i = 0; i < 5; i++)\n\t\t{\n\t\t\teltStar[i].addEventListener(\"mouseenter\", SelectScore);\n\t\t\teltStar[i].addEventListener(\"click\", SaveScore);\n\t\t}\n\t\teltScoring.addEventListener(\"mouseleave\", ResetDisplay);\n\t}\n\n\tfunction GetScoreFromId(id)\n\t{\n\t\tvar score = 0;\n\t\tswitch (id)\n\t\t{\n\t\t\tcase \"scoringstar4\": score = 5; break;\n\t\t\tcase \"scoringstar3\": score = 4; break;\n\t\t\tcase \"scoringstar2\": score = 3; break;\n\t\t\tcase \"scoringstar1\": score = 2; break;\n\t\t\tcase \"scoringstar0\": score = 1; break;\n\t\t\tdefault: score = 0; break;\n\t\t}\n\t\treturn score;\n\t}\n\n\t/**\n\t * Save the score corresponding to the current clicked star\n\t */\n\tfunction SaveScore(evt)\n\t{\n\t\tvar score = GetScoreFromId(evt.target.id);\n\t\teltScore.innerText = score;\n\t\twindow.postMessage({ eventName: \"OnClick\", control: originControl, args: score}, document.location.origin);\n\t}\n\n\t/**\n\t * Get the current star down the mouse and highligh this star and all the previous one\n\t */\n\tfunction SelectScore(evt)\n\t{\n\t\tvar score = GetScoreFromId(evt.target.id);\n\t\tDisplay(score);\n\t}\n\n\t/**\n\t * Reset plain stars disply to its initial score\n\t */\n\tfunction ResetDisplay()\n\t{\n\t\tvar score = parseFloat(eltScore.innerText);\n\t\tDisplay(score)\n\t}\n\n\t/**\n\t * Display the correct number of stars according to the score\n\t */\n\tfunction Display(score)\n\t{\n\t\tif (typeof score === \"undefined\")\n\t\t{\n\t\t\tscore = 0;\n\t\t}\n\t\tfor (var i = 0; i < 5; i++)\n\t\t{\n\t\t\tif (score >= (i + 0.75))\n\t\t\t{\n\t\t\t\t// plain star\n\t\t\t\teltStar[i].className = \"fa fa-2x fa-star\";\n\t\t\t}\n\t\t\telse if ((i + 0.25) <= score)\n\t\t\t{\n\t\t\t\t// half star\n\t\t\t\teltStar[i].className = \"fa fa-2x fa-star-half-o\";\n\t\t\t}\n\t\t\telse\n\t\t\t{\n\t\t\t\t// empty star\n\t\t\t\teltStar[i].className = \"fa fa-2x fa-star-o\";\n\t\t\t}\n\t\t}\n\t}\n\n\tfunction Init(evt)\n\t{\n\t\t// store initial score for reseting purpose\n\t\teltScore.innerText = evt.score;\n\t\tDisplay(evt.score);\n\t}\n</script>",
																						"css": "",
																						"hidden": true,
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 257
																				},
																				"LabelScoringValue__": {
																					"type": "Label",
																					"data": [
																						"ScoringValue__"
																					],
																					"options": {
																						"label": "_ScoringValue",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 258
																				},
																				"ScoringValue__": {
																					"type": "Integer",
																					"data": [
																						"ScoringValue__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_ScoringValue",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": false,
																						"hidden": true,
																						"browsable": false,
																						"defaultValue": "0"
																					},
																					"stamp": 241
																				},
																				"LabelScoringComment__": {
																					"type": "Label",
																					"data": [
																						"ScoringComment__"
																					],
																					"options": {
																						"label": "_ScoringComment",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 260
																				},
																				"ScoringComment__": {
																					"type": "LongText",
																					"data": [
																						"ScoringComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ScoringComment",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 261
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 271
																				}
																			}
																		}
																	}
																}
															}
														}
													},
													"stamp": 40,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 41,
															"data": []
														}
													},
													"stamp": 42,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 43,
															"data": []
														}
													},
													"stamp": 44,
													"data": []
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 45,
													"*": {
														"LineItems": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "GR_items.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_LineItems",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 46,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 47,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LineItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 48,
																			"*": {
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 42,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": false,
																							"disableAddLine": false,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": false,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_LineItems",
																						"subsection": null
																					},
																					"stamp": 49,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 50,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 51
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 52
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 53,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 54,
																									"data": [],
																									"*": {
																										"LineNumber__": {
																											"type": "Label",
																											"data": [
																												"LineNumber__"
																											],
																											"options": {
																												"label": "_LineNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 55,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [
																												"Number__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"version": 0
																											},
																											"stamp": 57,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": 90,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 59,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 60,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"version": 0
																											},
																											"stamp": 61,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 62,
																									"data": [],
																									"*": {
																										"OrderedQuantity__": {
																											"type": "Label",
																											"data": [
																												"OrderedQuantity__"
																											],
																											"options": {
																												"label": "_OrderedQuantity",
																												"version": 0
																											},
																											"stamp": 63,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 64,
																									"data": [],
																									"*": {
																										"OpenQuantity__": {
																											"type": "Label",
																											"data": [
																												"OpenQuantity__"
																											],
																											"options": {
																												"label": "_OpenQuantity",
																												"version": 0
																											},
																											"stamp": 65,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 66,
																									"data": [],
																									"*": {
																										"ReceivedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ReceivedQuantity__"
																											],
																											"options": {
																												"label": "_ReceivedQuantity",
																												"version": 0
																											},
																											"stamp": 67,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 68,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "Label",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"label": "_ItemUnit",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 69,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 70,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"label": "_ItemUnitDescription",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 71,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 72,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 73,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 90,
																										"version": 0,
																										"hidden": false
																									},
																									"stamp": 74,
																									"data": [],
																									"*": {
																										"ItemOrderedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemOrderedAmount__"
																											],
																											"options": {
																												"label": "_ItemOrderedAmount",
																												"minwidth": 90,
																												"version": 0,
																												"hidden": false
																											},
																											"stamp": 75,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 90,
																										"version": 0,
																										"hidden": false
																									},
																									"stamp": 76,
																									"data": [],
																									"*": {
																										"ItemOpenAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemOpenAmount__"
																											],
																											"options": {
																												"label": "_ItemOpenAmount",
																												"minwidth": 90,
																												"version": 0,
																												"hidden": false
																											},
																											"stamp": 77,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": false,
																										"minwidth": "70"
																									},
																									"stamp": 78,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Label",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"label": "_ReceivedAmount",
																												"version": 0,
																												"hidden": false,
																												"minwidth": "70"
																											},
																											"stamp": 79,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "50",
																										"version": 0
																									},
																									"stamp": 80,
																									"data": [],
																									"*": {
																										"PercentageReceived__": {
																											"type": "Label",
																											"data": [
																												"PercentageReceived__"
																											],
																											"options": {
																												"label": "_PercentageReceived",
																												"minwidth": "50",
																												"version": 0
																											},
																											"stamp": 81,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "80"
																									},
																									"stamp": 82,
																									"data": [],
																									"*": {
																										"DeliveryCompleted__": {
																											"type": "Label",
																											"data": [
																												"DeliveryCompleted__"
																											],
																											"options": {
																												"label": "_DeliveryCompleted",
																												"version": 0,
																												"minwidth": "80"
																											},
																											"stamp": 83,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 249,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "Label",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"label": "_WarehouseID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 250,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 253,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "Label",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"label": "_WarehouseName",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 254,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"CostCenterId__": {
																											"type": "Label",
																											"data": [
																												"CostCenterId__"
																											],
																											"options": {
																												"label": "_CostCenterId",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 85,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "Label",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"label": "_CostCenter",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 87,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "Label",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"label": "_ProjectCode",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 90,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "Label",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"label": "_ProjectCodeDescription",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 91,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 92,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "Label",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"label": "_InternalOrder",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 93,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 94,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "Label",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"label": "_WBSElement",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 95,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 96,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "Label",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"label": "_WBSElementID",
																												"minwidth": 140,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 97,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 225,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"label": "_FreeDimension1",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 226,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 229,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"label": "_FreeDimension1ID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 230,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 98,
																									"data": [],
																									"*": {
																										"Group__": {
																											"type": "Label",
																											"data": [
																												"Group__"
																											],
																											"options": {
																												"label": "_Group",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 99,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 100,
																									"data": [],
																									"*": {
																										"RequisitionNumber__": {
																											"type": "Label",
																											"data": [
																												"RequisitionNumber__"
																											],
																											"options": {
																												"label": "_RequisitionNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 101,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 102,
																									"data": [],
																									"*": {
																										"OrderNumber__": {
																											"type": "Label",
																											"data": [
																												"OrderNumber__"
																											],
																											"options": {
																												"label": "_OrderNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 103,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 104,
																									"data": [],
																									"*": {
																										"ExchangeRate__": {
																											"type": "Label",
																											"data": [
																												"ExchangeRate__"
																											],
																											"options": {
																												"label": "_ExchangeRate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 105,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 106,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"RequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDate",
																												"version": 0
																											},
																											"stamp": 107,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 108,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "Label",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"label": "_CostType",
																												"minwidth": 80,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 109,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 110,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "Label",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"label": "_BudgetID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 111,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 112,
																									"data": [],
																									"*": {
																										"POBudgetID__": {
																											"type": "Label",
																											"data": [
																												"POBudgetID__"
																											],
																											"options": {
																												"label": "_POBudgetID",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 113,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 114,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "Label",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"label": "_RequesterDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 115,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 116,
																									"data": [],
																									"*": {
																										"RequesterName__": {
																											"type": "Label",
																											"data": [
																												"RequesterName__"
																											],
																											"options": {
																												"label": "_RequesterName",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 117,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 213,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"label": "_ItemShipToCompany",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 214,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 217,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"label": "_ItemDeliveryAddressID",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 218,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 221,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"label": "_ItemShipToAddress",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 222,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 118,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "Label",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"label": "_RecipientDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 119,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 120,
																									"data": [],
																									"*": {
																										"RecipientName__": {
																											"type": "Label",
																											"data": [
																												"RecipientName__"
																											],
																											"options": {
																												"label": "_RecipientName",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 121,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 245,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "Label",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 246,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 122,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 123,
																									"data": [],
																									"*": {
																										"LineNumber__": {
																											"type": "Integer",
																											"data": [
																												"LineNumber__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_LineNumber",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": "25",
																												"readonly": true,
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 124,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 125,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [
																												"Number__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": "100",
																												"version": 0,
																												"browsable": false,
																												"readonly": true
																											},
																											"stamp": 126,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 127,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"readonly": true,
																												"hidden": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 128,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 129,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [
																												"Description__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": "200",
																												"length": 200,
																												"resizable": true,
																												"minNbLines": 1,
																												"numberOfLines": 999,
																												"readonly": true,
																												"browsable": false,
																												"version": 1
																											},
																											"stamp": 130,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 131,
																									"data": [],
																									"*": {
																										"OrderedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"OrderedQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_OrderedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"readonly": true
																											},
																											"stamp": 132,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 133,
																									"data": [],
																									"*": {
																										"OpenQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"OpenQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_OpenQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"readonly": true
																											},
																											"stamp": 134,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 135,
																									"data": [],
																									"*": {
																										"ReceivedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ReceivedQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ReceivedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60"
																											},
																											"stamp": 136,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 137,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnit",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": false,
																												"length": 3,
																												"readonly": true,
																												"hidden": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 138,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 139,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnitDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"readonly": true,
																												"hidden": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches"
																											},
																											"stamp": 140,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 141,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"UnitPrice__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 142,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 143,
																									"data": [],
																									"*": {
																										"ItemOrderedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemOrderedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemOrderedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"autocompletable": false,
																												"hidden": false
																											},
																											"stamp": 144,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 145,
																									"data": [],
																									"*": {
																										"ItemOpenAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemOpenAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemOpenAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"autocompletable": false,
																												"hidden": false
																											},
																											"stamp": 146,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 147,
																									"data": [],
																									"*": {
																										"NetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"NetAmount__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ReceivedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": false
																											},
																											"stamp": 148,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 149,
																									"data": [],
																									"*": {
																										"PercentageReceived__": {
																											"type": "Decimal",
																											"data": [
																												"PercentageReceived__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_PercentageReceived",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"autocompletable": false
																											},
																											"stamp": 150,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 151,
																									"data": [],
																									"*": {
																										"DeliveryCompleted__": {
																											"type": "CheckBox",
																											"data": [
																												"DeliveryCompleted__"
																											],
																											"options": {
																												"label": "_DeliveryCompleted",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 152,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 248,
																									"data": [],
																									"*": {
																										"WarehouseID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 251,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 252,
																									"data": [],
																									"*": {
																										"WarehouseName__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WarehouseName__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_WarehouseName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 255,
																											"position": [
																												"_DatabaseComboBox2"
																											]
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 153,
																									"data": [],
																									"*": {
																										"CostCenterId__": {
																											"type": "ShortText",
																											"data": [
																												"CostCenterId__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_CostCenterId",
																												"activable": true,
																												"width": "90",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 154,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 155,
																									"data": [],
																									"*": {
																										"CostCenter__": {
																											"type": "ShortText",
																											"data": [
																												"CostCenter__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_CostCenter",
																												"activable": true,
																												"width": "170",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 156,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true,
																												"RestrictSearch": true
																											},
																											"stamp": 158,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 159,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCodeDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 160,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_InternalOrder",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 162,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 163,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElement",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 164,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 165,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElementID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 166,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 224,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_FreeDimension1",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"length": 500
																											},
																											"stamp": 227,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 228,
																									"data": [],
																									"*": {
																										"FreeDimension1ID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1ID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_FreeDimension1ID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"length": 500,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 231,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"Group__": {
																											"type": "ShortText",
																											"data": [
																												"Group__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Group",
																												"activable": true,
																												"width": "200",
																												"length": 64,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 168,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"RequisitionNumber__": {
																											"type": "ShortText",
																											"data": [
																												"RequisitionNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequisitionNumber",
																												"activable": true,
																												"width": "90",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 170,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"OrderNumber__": {
																											"type": "ShortText",
																											"data": [
																												"OrderNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_OrderNumber",
																												"activable": true,
																												"width": "90",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 172,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"ExchangeRate__": {
																											"type": "Decimal",
																											"data": [
																												"ExchangeRate__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ExchangeRate",
																												"precision_internal": 5,
																												"precision_current": 5,
																												"activable": true,
																												"width": "90",
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 174,
																											"position": [
																												"Nombre décimal"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 175,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"RequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_RequestedDeliveryDate",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0
																											},
																											"stamp": 176,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 177,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "ComboBox",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "",
																													"1": "_OpEx",
																													"2": "_CapEx"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "",
																													"1": "OpEx",
																													"2": "CapEx"
																												},
																												"label": "_CostType",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"readonly": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 178,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 179,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 180,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 181,
																									"data": [],
																									"*": {
																										"POBudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"POBudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_POBudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 182,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 183,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 184,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 185,
																									"data": [],
																									"*": {
																										"RequesterName__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 186,
																											"position": [
																												"Text2"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 212,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ItemShipToCompany",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true
																											},
																											"stamp": 215,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 216,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "ShortText",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemDeliveryAddressID",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 219,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 220,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "LongText",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemShipToAddress",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 4,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 223,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 187,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "ShortText",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RecipientDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 188,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 189,
																									"data": [],
																									"*": {
																										"RecipientName__": {
																											"type": "ShortText",
																											"data": [
																												"RecipientName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RecipientName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 190,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 244,
																									"data": [],
																									"*": {
																										"IsReplenishmentItem__": {
																											"type": "CheckBox",
																											"data": [
																												"IsReplenishmentItem__"
																											],
																											"options": {
																												"label": "_IsReplenishmentItem",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"possibleValues": {},
																												"possibleKeys": {},
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 247,
																											"position": [
																												"_CheckBox"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 191,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "PR_attachments.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Attachments",
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 192
																}
															},
															"stamp": 193,
															"data": []
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 194,
													"*": {
														"Actions": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Actions",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 195,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 196,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ConfirmDelivery__": {
																						"line": 2,
																						"column": 1
																					},
																					"Quit__": {
																						"line": 4,
																						"column": 1
																					},
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"Cancel__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 197,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "60",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement",
																						"version": 0
																					},
																					"stamp": 198
																				},
																				"ConfirmDelivery__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_ConfirmDelivery",
																						"label": "",
																						"urlImage": "",
																						"style": 5,
																						"width": "",
																						"version": 0,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "BudgetIntegrityValidation__",
																							"attachmentsMode": "all",
																							"willBeChild": true
																						},
																						"iconColor": "color1",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-approve-check-circle fa-4x ",
																						"action": "approve",
																						"url": ""
																					},
																					"stamp": 199
																				},
																				"Cancel__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_GRCancel",
																						"label": "Cancel",
																						"urlImage": "",
																						"style": 5,
																						"width": "100%",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "none",
																							"willBeChild": false,
																							"returnToOriginalUrl": true
																						},
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-cancel-goods-circle fa-4x ",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"urlImageOverlay": "",
																						"hidden": true
																					},
																					"stamp": 200
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "_Quit",
																						"urlImage": "",
																						"style": 5,
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "BudgetIntegrityValidation__",
																							"attachmentsMode": "all",
																							"willBeChild": true
																						},
																						"iconColor": "color1",
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x",
																						"action": "cancel",
																						"url": "",
																						"width": "",
																						"version": 0
																					},
																					"stamp": 201
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 202,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 203
																}
															},
															"stamp": 204,
															"data": []
														}
													},
													"stamp": 205,
													"data": []
												}
											},
											"stamp": 206,
											"data": []
										}
									},
									"stamp": 207,
									"data": []
								}
							},
							"stamp": 208,
							"data": []
						}
					},
					"stamp": 209,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1700
					},
					"*": {},
					"stamp": 210,
					"data": []
				}
			},
			"stamp": 211,
			"data": []
		}
	},
	"stamps": 280,
	"data": []
}