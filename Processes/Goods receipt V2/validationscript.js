///#GLOBALS Lib Sys
/////////////////////////////////////////////////////////////
// Global variables
/////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
// GR creation part
function CheckErrorGR() {
    return Lib.Purchasing.GRValidation.CheckOverReceivedItems() && Lib.ERP.ExecuteERPFunc("CheckError", "GR").ret;
}
function RetryGR(continueMode) {
    return Lib.ERP.ExecuteERPFunc("Retry", "GR", continueMode).ret;
}
function CreateGRInERP(callback) {
    var ERPName = Lib.ERP.GetERPName();
    var createDocInERP = Sys.Parameters.GetInstance("P2P_" + ERPName).GetParameter("CreateDocInERP");
    Log.Info("Create goods receipt in ERP " + ERPName);
    var grData = { number: Data.GetValue("GRNumber__") };
    if (!grData.number) {
        var ret = Lib.ERP.ExecuteERPFunc("Create", "GR");
        if (ret.error) {
            Lib.CommonDialog.NextAlert.Define("_Goods receipt creation error", ret.error);
            return false;
        }
        grData = ret.ret;
        // allocate a number if no document created in ERP
        if (!createDocInERP) {
            // Fetch previous GR number if it already exists (crash protection)
            grData.number = Transaction.Read("SafePAC_GRNUMBER");
            if (!grData.number) {
                grData.number = Lib.Purchasing.NextNumber("GR", "GRNumber__");
                Transaction.Write("SafePAC_GRNUMBER", grData.number);
            }
        }
        Data.SetValue("GRNumber__", grData.number);
        Log.Info("Created with number " + grData.number);
    }
    else {
        Log.Info("Already created with number " + grData.number);
    }
    Lib.ERP.ExecuteERPFunc("AttachURL", "GR");
    // handle case of future "generic" case (asynchronous), where PO number is not known yet
    var msg = "_Advise buyer on GR creation";
    if (grData && grData.number) {
        msg += createDocInERP ? " with number " + ERPName : " with number";
    }
    Lib.CommonDialog.NextAlert.Define("_GR creation popup", msg, {
        isError: false,
        behaviorName: "GRCreationInfo"
    }, grData && grData.number);
    return callback(createDocInERP, grData);
}
function TakeOwnerShipOnPurchaseOrder(transport, token) {
    var ok = false;
    if (transport) {
        ok = transport.GetAsyncOwnership(token, 20000) === 0;
    }
    if (!ok) {
        Lib.CommonDialog.NextAlert.Define("_Goods receipt creation error", "_PreventConcurrentAccess PO items modified error message");
    }
    return ok;
}
function ReleaseOwnershipOnPurchaseOrder(transport, token) {
    if (transport) {
        transport.ReleaseAsyncOwnership(token);
    }
}
function PublishReviewScoring(transport) {
    var transportVars = transport.GetUninheritedVars();
    var scoring = Data.GetValue("ScoringValue__");
    if (scoring > 0) {
        Lib.AP.Scoring.AddScoreValue(transportVars.GetValue_String("CompanyCode__", 0), transportVars.GetValue_String("VendorNumber__", 0), Lib.AP.Scoring.ScoreTypes.Delivery, scoring, Data.GetValue("ScoringComment__"));
    }
}
function Receive() {
    var poRuidEx = Data.GetValue("SourceRUID");
    var transport = Lib.P2P.QueryPurchasingTransport("Purchase order", "(RUIDEX=" + poRuidEx + ")", "*", true);
    var ownershipToken = "RECEIV_" + Data.GetValue("MsnEx");
    var ok = true;
    // Take ownership only when we are not in auto receive
    if (!Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
        // Take ownership to avoid conflict in case the PO is being edited by someone else
        // PAC - FT-013911 - Ability for the buyer to update the delivery date of a purchase order
        ok = TakeOwnerShipOnPurchaseOrder(transport, ownershipToken);
    }
    try {
        ok = ok && CreateGRInERP(Lib.Purchasing.GRValidation.OnCreateGRInERP);
        ok = ok && Lib.Purchasing.GRBudget.AsReceived();
        ok = ok && Sys.Helpers.Data.RollbackableSection(function (rollbackFn) {
            Data.SetValue("GRStatus__", "Received");
            if (!Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
                ReleaseOwnershipOnPurchaseOrder(transport, ownershipToken);
            }
            return Lib.Purchasing.GRValidation.SynchronizeItems() || rollbackFn();
        });
    }
    catch (e) {
        if (!Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
            ReleaseOwnershipOnPurchaseOrder(transport, ownershipToken);
        }
        Lib.Purchasing.OnUnexpectedError(e.toString());
        ok = false;
    }
    if (ok) {
        Data.SetValue("KeepOpenAfterApproval", "WaitForApproval");
    }
    else {
        if (!Lib.Purchasing.IsAutoReceiveOrderEnabled()) {
            ReleaseOwnershipOnPurchaseOrder(transport, ownershipToken);
        }
        Process.PreventApproval();
    }
    if (ok && Lib.AP && typeof Lib.AP.Scoring !== "undefined") {
        PublishReviewScoring(transport);
    }
}
function ReplenishStock() {
    var bok = true;
    if (Lib.P2P.Inventory.IsEnabled()) {
        if (Data.GetValue("GRStatus__") !== "Canceled") {
            var ruidex_1 = Data.GetValue("RUIDEX");
            var companyCode_1 = Data.GetValue("CompanyCode__");
            var inventoryFilter_1 = [];
            Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
                if (line.GetValue("IsReplenishmentItem__")) {
                    var warehouseID = line.GetValue("WarehouseID__");
                    var itemNumber = line.GetValue("Number__");
                    inventoryFilter_1.push({
                        companyCode: companyCode_1,
                        itemNumber: itemNumber,
                        warehouseID: warehouseID
                    });
                    var inventoryMovement = new Lib.P2P.Inventory.InventoryMovement();
                    inventoryMovement.companyCode = companyCode_1;
                    inventoryMovement.itemNumber = itemNumber;
                    inventoryMovement.lineNumber = line.GetValue("LineNumber__");
                    inventoryMovement.movementOrigin = ruidex_1;
                    inventoryMovement.movementType = "Replenishment";
                    inventoryMovement.movementValue = line.GetValue("ReceivedQuantity__");
                    inventoryMovement.unitOfMeasure = line.GetValue("ItemUnit__");
                    inventoryMovement.warehouseID = warehouseID;
                    bok = Lib.P2P.Inventory.CreateOrUpdateInventoryMovement(inventoryMovement) && bok;
                }
            });
            if (inventoryFilter_1.length > 0) {
                Lib.P2P.Inventory.GetInventories(inventoryFilter_1, false, true)
                    .Then(Lib.P2P.Inventory.UpdateInventoryStock);
            }
        }
    }
    return bok;
}
function SetRightsOnGR() {
    // Add right for supervisor and buyer
    Lib.Purchasing.SetRightForP2PSupervisor();
    Lib.Purchasing.GetAPClerk().GiveReadRight();
    var recipientDnSet = {};
    Sys.Helpers.Data.ForEachTableItem("LineItems__", function (line) {
        // Gives reading right to the recipient
        var currentDn = line.GetValue("RecipientDN__");
        if (Object.keys(recipientDnSet).length === 0 || !recipientDnSet[currentDn]) {
            recipientDnSet[currentDn] = 1;
            Log.Info("Grant read right to recipient " + currentDn);
            Process.SetRight(currentDn, "read");
        }
    });
    var buyer = Variable.GetValueAsString("BuyerLogin__");
    if (buyer) {
        Log.Info("Grant read right to buyer: " + buyer);
        Process.SetRight(buyer, "read");
    }
}
/////////////////////////////////////////////////////////////
// Main
/////////////////////////////////////////////////////////////
Lib.Purchasing.InitTechnicalFields();
// Index LineItems table to ES for better reporting
Lib.P2P.SetTablesToIndex(["LineItems__"]);
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- GR Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
// Check ERP Error
if (currentName === "Retry_FinalizePO_") {
    Receive();
    ReplenishStock();
}
else if (currentName === "OnEditOrder" || currentName === "RetryOnEditOrder") {
    Lib.Purchasing.GRValidation.OnEditOrder();
}
else if (currentName === "FullBudgetRecovery") {
    Lib.Purchasing.GRBudget.DoFullRecovery();
}
else {
    //ExactCD Validation, set rights
    if (Data.GetValue("State") === 50) {
        SetRightsOnGR();
    }
    // Route to correct actions for retry or continue
    // GPF, GR not found in ERP, retry all
    if (currentName === "Retry_") {
        RetryGR();
        currentAction = "approve";
    }
    else if (currentName === "Continue_") // GPF, GR Number known
     {
        RetryGR(true);
        currentAction = "approve";
    }
    // first approve with auto-reception (only execute the touchless in the sendCD action)
    if (currentName === "" && currentAction == "" && Lib.Purchasing.IsAutoReceiveOrderEnabled() && Data.GetValue("State") === 50) {
        if (Data.IsFormInError() || CheckErrorGR()) {
            Log.Info("Form in error.");
            Process.PreventApproval();
        }
        else {
            Receive();
            ReplenishStock();
        }
    }
    else if (currentName !== "" && currentAction !== "") {
        if (currentName === "Cancel") {
            Data.SetValue("GRStatus__", "Canceled");
            Data.SetValue("State", 300);
            Process.LeaveForm();
        }
        else if (Data.IsFormInError() || CheckErrorGR()) {
            Log.Info("Form in error.");
            Process.PreventApproval();
        }
        else if (currentAction === "approve_asynchronous" || currentAction === "approve") {
            Receive();
            ReplenishStock();
        }
    }
    var isRecallScriptScheduled = Process.IsRecallScriptScheduled();
    Sys.Helpers.TryCallFunction("Lib.GR.Customization.Server.OnValidationScriptEnd", currentAction, currentName, isRecallScriptScheduled);
}
