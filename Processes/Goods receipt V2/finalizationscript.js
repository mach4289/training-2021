///#GLOBALS Lib Sys
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- GR Finalization Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
function GenerateDemoInvoice() {
    var bHasReceivedSomething = false;
    var table = Data.GetTable("LineItems__");
    var nItems = table.GetItemCount();
    for (var i = 0; i < nItems; i++) {
        var lineItem = table.GetItem(i);
        if (lineItem.GetValue("ReceivedQuantity__")) {
            bHasReceivedSomething = true;
            break;
        }
    }
    if (bHasReceivedSomething) {
        Lib.Purchasing.Demo.GenerateVendorInvoice(Data.GetValue("OrderNumber__"), true, true);
    }
}
// Generate demo invoice asynchronously (demo only AND it can take some time)
if (Sys.Parameters.GetInstance("PAC").GetParameter("DemoEnableInvoiceCreation") == "3") {
    GenerateDemoInvoice();
}
