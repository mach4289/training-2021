///#GLOBALS Lib Sys
Controls.CompanyCode__.OnChange = function () {
    var control = this;
    Controls.AllowedCompanyCodes__.Defer(null, function () {
        if (!control.GetError()) {
            var cc = control.GetValue();
            var allowed = Controls.AllowedCompanyCodes__.GetValue();
            if (allowed) {
                var exp = new RegExp("(^|;)" + cc + "(;|$)");
                var res = exp.exec(allowed);
                if (res === null) {
                    allowed += ";" + cc;
                }
            }
            else {
                allowed = cc;
            }
            Controls.AllowedCompanyCodes__.SetValue(allowed);
        }
    }, 100);
};
Sys.Parameters.GetInstance("P2P").IsReady(function () {
    Controls.DefaultWarehouse__.Hide(!Lib.P2P.Inventory.IsEnabled());
});
