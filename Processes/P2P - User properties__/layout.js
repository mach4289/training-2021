{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"hideTitle": false,
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"UserLogin__": "LabelUserLogin__",
																	"LabelUserLogin__": "UserLogin__",
																	"AllowedCompanyCodes__": "LabelAllowed_company_codes__",
																	"LabelAllowed_company_codes__": "AllowedCompanyCodes__",
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"ManagerLogin__": "LabelManagerLogin__",
																	"LabelManagerLogin__": "ManagerLogin__",
																	"LOAAmount__": "LabelLOAAmount__",
																	"LabelLOAAmount__": "LOAAmount__",
																	"UserNumber__": "LabelUserNumber__",
																	"LabelUserNumber__": "UserNumber__",
																	"HasCompanyCard__": "LabelHasCompanyCard__",
																	"LabelHasCompanyCard__": "HasCompanyCard__",
																	"AllowedBudgetKeys__": "LabelAllowedBudgetKeys__",
																	"LabelAllowedBudgetKeys__": "AllowedBudgetKeys__",
																	"DefaultWarehouse__": "LabelDefaultWarehouse__",
																	"LabelDefaultWarehouse__": "DefaultWarehouse__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 10,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CostCenter__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 4,
																				"column": 1
																			},
																			"UserLogin__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelUserLogin__": {
																				"line": 1,
																				"column": 1
																			},
																			"AllowedCompanyCodes__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelAllowed_company_codes__": {
																				"line": 5,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 3,
																				"column": 1
																			},
																			"ManagerLogin__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelManagerLogin__": {
																				"line": 7,
																				"column": 1
																			},
																			"LOAAmount__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelLOAAmount__": {
																				"line": 8,
																				"column": 1
																			},
																			"UserNumber__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelUserNumber__": {
																				"line": 2,
																				"column": 1
																			},
																			"HasCompanyCard__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelHasCompanyCard__": {
																				"line": 9,
																				"column": 1
																			},
																			"AllowedBudgetKeys__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelAllowedBudgetKeys__": {
																				"line": 10,
																				"column": 1
																			},
																			"DefaultWarehouse__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelDefaultWarehouse__": {
																				"line": 6,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelUserLogin__": {
																			"type": "Label",
																			"data": [
																				"UserLogin__"
																			],
																			"options": {
																				"label": "UserLogin",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"UserLogin__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"UserLogin__"
																			],
																			"options": {
																				"label": "UserLogin",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true,
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 25
																		},
																		"LabelUserNumber__": {
																			"type": "Label",
																			"data": [
																				"UserNumber__"
																			],
																			"options": {
																				"label": "_UserNumber",
																				"version": 0
																			},
																			"stamp": 52
																		},
																		"UserNumber__": {
																			"type": "ShortText",
																			"data": [
																				"UserNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_UserNumber",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 53
																		},
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"Select_from_a_table__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": "500",
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 47
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "CostCenter",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"CostCenter__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "CostCenter",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true,
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 23
																		},
																		"LabelAllowed_company_codes__": {
																			"type": "Label",
																			"data": [
																				"AllowedCompanyCodes__"
																			],
																			"options": {
																				"label": "Allowed company codes",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"AllowedCompanyCodes__": {
																			"type": "ShortText",
																			"data": [
																				"AllowedCompanyCodes__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Allowed company codes",
																				"activable": true,
																				"width": "500",
																				"browsable": false,
																				"version": 0,
																				"length": 500
																			},
																			"stamp": 39
																		},
																		"LabelDefaultWarehouse__": {
																			"type": "Label",
																			"data": [
																				"DefaultWarehouse__"
																			],
																			"options": {
																				"label": "_DefaultWarehouse",
																				"version": 0,
																				"hidden": true
																			},
																			"stamp": 60
																		},
																		"DefaultWarehouse__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"DefaultWarehouse__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_DefaultWarehouse",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"hidden": true
																			},
																			"stamp": 61
																		},
																		"LabelManagerLogin__": {
																			"type": "Label",
																			"data": [
																				"ManagerLogin__"
																			],
																			"options": {
																				"label": "_ManagerLogin",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"ManagerLogin__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ManagerLogin__"
																			],
																			"options": {
																				"label": "_ManagerLogin",
																				"activable": true,
																				"width": "500",
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true,
																				"version": 0,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 49
																		},
																		"LabelLOAAmount__": {
																			"type": "Label",
																			"data": [
																				"LOAAmount__"
																			],
																			"options": {
																				"label": "_LOAAmount",
																				"version": 0
																			},
																			"stamp": 50
																		},
																		"LOAAmount__": {
																			"type": "Decimal",
																			"data": [
																				"LOAAmount__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_LOAAmount",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": "500"
																			},
																			"stamp": 51
																		},
																		"LabelHasCompanyCard__": {
																			"type": "Label",
																			"data": [
																				"HasCompanyCard__"
																			],
																			"options": {
																				"label": "_Has company card",
																				"version": 0
																			},
																			"stamp": 54
																		},
																		"HasCompanyCard__": {
																			"type": "CheckBox",
																			"data": [
																				"HasCompanyCard__"
																			],
																			"options": {
																				"label": "_Has company card",
																				"activable": true,
																				"width": "500",
																				"helpText": "_HelperNonRefundableExpenses",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 55
																		},
																		"LabelAllowedBudgetKeys__": {
																			"type": "Label",
																			"data": [
																				"AllowedBudgetKeys__"
																			],
																			"options": {
																				"label": "_AllowedBudgetKeys",
																				"version": 0
																			},
																			"stamp": 56
																		},
																		"AllowedBudgetKeys__": {
																			"type": "LongText",
																			"data": [
																				"AllowedBudgetKeys__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_AllowedBudgetKeys",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false,
																				"numberOfLines": 10
																			},
																			"stamp": 57
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 61,
	"data": []
}