///#GLOBALS Lib
function reverseCI()
{
	var CIruidex = Variable.GetValueAsString("CustomerInvoiceRuidEx");
	if (CIruidex)
	{
		var CIUpdated = false;
		var CIQuery = Process.CreateQueryAsProcessAdmin();
		CIQuery.Reset();
		CIQuery.SetSpecificTable("CDNAME#Customer invoice");
		CIQuery.SetAttributesList("RUIDEX,State,CustomerInvoiceStatus__");
		CIQuery.SetFilter("RUIDEX=" + CIruidex);
		CIQuery.SetSearchInArchive(true);
		if (CIQuery.MoveFirst())
		{
			var transport = CIQuery.MoveNext();
			if (transport)
			{
				var vars = transport.GetUninheritedVars();
				Log.Info("The customer invoice process " + CIruidex + " has been found");
				vars.AddValue_String("CustomerInvoiceStatus__", Lib.AP.InvoiceStatus.Rejected, true);
				CIUpdated = true;
				transport.Process();
			}
		}
		if (!CIUpdated)
		{
			Log.Info("The customer invoice process " + CIruidex + " has not been updated");
		}
	}
}

function reverseBudget()
{
	var VIPRuidex = Variable.GetValueAsString("VendorInvoiceRuidEx");
	if (VIPRuidex)
	{
		var BudgetManager = {
			IsBudgetEnable: Lib.Budget.IsEnabled,
			Updater: Lib.AP.Budget
		};
		if (BudgetManager && BudgetManager.IsBudgetEnable())
		{
			var BudgetQuery = Process.CreateQueryAsProcessAdmin();
			BudgetQuery.Reset();
			BudgetQuery.SetSpecificTable("CDNAME#Vendor invoice");
			BudgetQuery.SetAttributesList("*");
			BudgetQuery.SetFilter("RUIDEX=" + VIPRuidex);
			BudgetQuery.SetSearchInArchive(true);
			if (BudgetQuery.MoveFirst())
			{
				var transport = BudgetQuery.MoveNext();
				if (transport)
				{
					var vars = transport.GetUninheritedVars();
					vars.AddValue_String("RUIDEX", Data.GetValue("RuidEx"), true);
					BudgetManager.Updater.AsInvoiceReversed(transport);
				}
			}
		}
		else
		{
			Log.Info("Budget is not enable - Budget not been updated after reversing invoice in Vendor Invoice Process " + VIPRuidex);
		}
	}
}
reverseCI();
reverseBudget();