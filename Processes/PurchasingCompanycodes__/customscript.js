///#GLOBALS Lib Sys
Lib.P2P.Address.SetFormattedAddressControl(Controls.CompanyAddress__);
Controls.Sub__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.Street__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.PostalCode__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.Country__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.Region__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.City__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Controls.PostOfficeBox__.OnChange = Lib.P2P.Address.ComputeFormattedAddress;
Lib.P2P.Address.ComputeFormattedAddress();
