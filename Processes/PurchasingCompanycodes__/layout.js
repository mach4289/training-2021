{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"CompanyInfo": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_CompanyInfo",
														"hideTitle": false,
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"sameHeightAsSiblings": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyName__": "LabelCompany_name__",
																	"LabelCompany_name__": "CompanyName__",
																	"CompanyCode__": "LabelCompany_code__",
																	"LabelCompany_code__": "CompanyCode__",
																	"Currency__": "LabelCurrency__",
																	"LabelCurrency__": "Currency__",
																	"PurchasingOrganization__": "LabelPurchasingOrganization__",
																	"LabelPurchasingOrganization__": "PurchasingOrganization__",
																	"PurchasingGroup__": "LabelPurchasingGroup__",
																	"LabelPurchasingGroup__": "PurchasingGroup__",
																	"DeliveryAddressID__": "LabelDeliveryAddressID__",
																	"LabelDeliveryAddressID__": "DeliveryAddressID__",
																	"ERP__": "LabelERP__",
																	"LabelERP__": "ERP__",
																	"PhoneNumber__": "LabelPhoneNumber__",
																	"LabelPhoneNumber__": "PhoneNumber__",
																	"FaxNumber__": "LabelFaxNumber__",
																	"LabelFaxNumber__": "FaxNumber__",
																	"VATNumber__": "LabelVATNumber__",
																	"LabelVATNumber__": "VATNumber__",
																	"ContactEmail__": "LabelContactEmail__",
																	"LabelContactEmail__": "ContactEmail__",
																	"SIRET__": "LabelSIRET__",
																	"LabelSIRET__": "SIRET__",
																	"DefaultConfiguration__": "LabelDefaultConfiguration__",
																	"LabelDefaultConfiguration__": "DefaultConfiguration__",
																	"DefaultExpenseReportType__": "LabelDefaultExpenseReportType__",
																	"LabelDefaultExpenseReportType__": "DefaultExpenseReportType__",
																	"DeterminationKeyword__": "LabelDeterminationKeyword__",
																	"LabelDeterminationKeyword__": "DeterminationKeyword__",
																	"WorkingDays__": "LabelWorkingDays__",
																	"LabelWorkingDays__": "WorkingDays__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 16,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyName__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCompany_name__": {
																				"line": 2,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompany_code__": {
																				"line": 1,
																				"column": 1
																			},
																			"Currency__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCurrency__": {
																				"line": 8,
																				"column": 1
																			},
																			"PurchasingOrganization__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelPurchasingOrganization__": {
																				"line": 9,
																				"column": 1
																			},
																			"PurchasingGroup__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelPurchasingGroup__": {
																				"line": 10,
																				"column": 1
																			},
																			"DeliveryAddressID__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelDeliveryAddressID__": {
																				"line": 11,
																				"column": 1
																			},
																			"ERP__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelERP__": {
																				"line": 12,
																				"column": 1
																			},
																			"PhoneNumber__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPhoneNumber__": {
																				"line": 4,
																				"column": 1
																			},
																			"FaxNumber__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelFaxNumber__": {
																				"line": 5,
																				"column": 1
																			},
																			"VATNumber__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelVATNumber__": {
																				"line": 6,
																				"column": 1
																			},
																			"ContactEmail__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelContactEmail__": {
																				"line": 7,
																				"column": 1
																			},
																			"SIRET__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelSIRET__": {
																				"line": 3,
																				"column": 1
																			},
																			"DefaultConfiguration__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelDefaultConfiguration__": {
																				"line": 13,
																				"column": 1
																			},
																			"DefaultExpenseReportType__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelDefaultExpenseReportType__": {
																				"line": 14,
																				"column": 1
																			},
																			"DeterminationKeyword__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelDeterminationKeyword__": {
																				"line": 15,
																				"column": 1
																			},
																			"WorkingDays__": {
																				"line": 16,
																				"column": 2
																			},
																			"LabelWorkingDays__": {
																				"line": 16,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompany_code__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Company code",
																				"activable": true,
																				"width": "50",
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 4
																		},
																		"LabelCompany_name__": {
																			"type": "Label",
																			"data": [
																				"CompanyName__"
																			],
																			"options": {
																				"label": "Company name",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"CompanyName__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyName__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Company name",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 6
																		},
																		"LabelSIRET__": {
																			"type": "Label",
																			"data": [
																				"SIRET__"
																			],
																			"options": {
																				"label": "SIRET",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"SIRET__": {
																			"type": "ShortText",
																			"data": [
																				"SIRET__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "SIRET",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"LabelCurrency__": {
																			"type": "Label",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"label": "Currency",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"Currency__": {
																			"type": "ShortText",
																			"data": [
																				"Currency__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Currency",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"LabelPurchasingOrganization__": {
																			"type": "Label",
																			"data": [
																				"PurchasingOrganization__"
																			],
																			"options": {
																				"label": "_PurchasingOrganization",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"PurchasingOrganization__": {
																			"type": "ShortText",
																			"data": [
																				"PurchasingOrganization__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PurchasingOrganization",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 12
																		},
																		"LabelPurchasingGroup__": {
																			"type": "Label",
																			"data": [
																				"PurchasingGroup__"
																			],
																			"options": {
																				"label": "_PurchasingGroup",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"PurchasingGroup__": {
																			"type": "ShortText",
																			"data": [
																				"PurchasingGroup__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_PurchasingGroup",
																				"activable": true,
																				"width": 230,
																				"version": 0,
																				"browsable": false
																			},
																			"stamp": 14
																		},
																		"LabelDeliveryAddressID__": {
																			"type": "Label",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"label": "_DeliveryAddressID",
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"DeliveryAddressID__": {
																			"type": "ShortText",
																			"data": [
																				"DeliveryAddressID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_DeliveryAddressID",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"LabelERP__": {
																			"type": "Label",
																			"data": [
																				"ERP__"
																			],
																			"options": {
																				"label": "_ERP",
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"ERP__": {
																			"type": "ComboBox",
																			"data": [
																				"ERP__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_SAP",
																					"2": "_Generic",
																					"3": "_EBS",
																					"4": "_NAV",
																					"5": "_JDE",
																					"6": "_SAPS4CLOUD"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "SAP",
																					"2": "generic",
																					"3": "EBS",
																					"4": "NAV",
																					"5": "JDE",
																					"6": "SAPS4CLOUD"
																				},
																				"label": "_ERP",
																				"activable": true,
																				"width": 230,
																				"maxNbLinesToDisplay": 10
																			},
																			"stamp": 18
																		},
																		"PhoneNumber__": {
																			"type": "ShortText",
																			"data": [
																				"PhoneNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Phone number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 32,
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"LabelPhoneNumber__": {
																			"type": "Label",
																			"data": [
																				"PhoneNumber__"
																			],
																			"options": {
																				"label": "Phone number",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"FaxNumber__": {
																			"type": "ShortText",
																			"data": [
																				"FaxNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Fax number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 32,
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"LabelFaxNumber__": {
																			"type": "Label",
																			"data": [
																				"FaxNumber__"
																			],
																			"options": {
																				"label": "Fax number",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"VATNumber__": {
																			"type": "ShortText",
																			"data": [
																				"VATNumber__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "VAT number",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"length": 20,
																				"version": 0
																			},
																			"stamp": 23
																		},
																		"LabelVATNumber__": {
																			"type": "Label",
																			"data": [
																				"VATNumber__"
																			],
																			"options": {
																				"label": "VAT number",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"ContactEmail__": {
																			"type": "ShortText",
																			"data": [
																				"ContactEmail__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Contact email",
																				"activable": true,
																				"width": 230,
																				"version": 0
																			},
																			"stamp": 25
																		},
																		"LabelContactEmail__": {
																			"type": "Label",
																			"data": [
																				"ContactEmail__"
																			],
																			"options": {
																				"label": "Contact email",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"LabelDefaultConfiguration__": {
																			"type": "Label",
																			"data": [
																				"DefaultConfiguration__"
																			],
																			"options": {
																				"label": "_DefaultConfiguration",
																				"version": 0
																			},
																			"stamp": 27
																		},
																		"DefaultConfiguration__": {
																			"type": "ShortText",
																			"data": [
																				"DefaultConfiguration__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_DefaultConfiguration",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"LabelDefaultExpenseReportType__": {
																			"type": "Label",
																			"data": [
																				"DefaultExpenseReportType__"
																			],
																			"options": {
																				"label": "_DefaultExpenseReportType",
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"DefaultExpenseReportType__": {
																			"type": "ShortText",
																			"data": [
																				"DefaultExpenseReportType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DefaultExpenseReportType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 30
																		},
																		"LabelDeterminationKeyword__": {
																			"type": "Label",
																			"data": [
																				"DeterminationKeyword__"
																			],
																			"options": {
																				"label": "_DeterminationKeyword",
																				"version": 0
																			},
																			"stamp": 61
																		},
																		"DeterminationKeyword__": {
																			"type": "ShortText",
																			"data": [
																				"DeterminationKeyword__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_DeterminationKeyword",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 50,
																				"browsable": false
																			},
																			"stamp": 62
																		},
																		"LabelWorkingDays__": {
																			"type": "Label",
																			"data": [
																				"WorkingDays__"
																			],
																			"options": {
																				"label": "_WorkingDays"
																			},
																			"stamp": 63
																		},
																		"WorkingDays__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WorkingDays__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WorkingDays",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"length": 100,
																				"PreFillFTS": true
																			},
																			"stamp": 64
																		}
																	},
																	"stamp": 31
																}
															},
															"stamp": 32,
															"data": []
														}
													},
													"stamp": 33,
													"data": []
												},
												"CompanyAddress": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_CompanyAddress",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"sameHeightAsSiblings": false
													},
													"stamp": 34,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelStreet__": "Street__",
																	"Street__": "LabelStreet__",
																	"LabelCity__": "City__",
																	"City__": "LabelCity__",
																	"LabelPostalCode__": "PostalCode__",
																	"PostalCode__": "LabelPostalCode__",
																	"LabelRegion__": "Region__",
																	"Region__": "LabelRegion__",
																	"LabelPostOfficeBox__": "PostOfficeBox__",
																	"PostOfficeBox__": "LabelPostOfficeBox__",
																	"LabelCountry__": "Country__",
																	"Country__": "LabelCountry__",
																	"CompanyAddress__": "LabelCompanyAddress__",
																	"LabelCompanyAddress__": "CompanyAddress__",
																	"Sub__": "LabelSub__",
																	"LabelSub__": "Sub__"
																},
																"version": 0
															},
															"stamp": 35,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"LabelStreet__": {
																				"line": 2,
																				"column": 1
																			},
																			"Street__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCity__": {
																				"line": 4,
																				"column": 1
																			},
																			"City__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelPostalCode__": {
																				"line": 5,
																				"column": 1
																			},
																			"PostalCode__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelRegion__": {
																				"line": 6,
																				"column": 1
																			},
																			"Region__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelPostOfficeBox__": {
																				"line": 3,
																				"column": 1
																			},
																			"PostOfficeBox__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelCountry__": {
																				"line": 7,
																				"column": 1
																			},
																			"Country__": {
																				"line": 7,
																				"column": 2
																			},
																			"CompanyAddress__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCompanyAddress__": {
																				"line": 8,
																				"column": 1
																			},
																			"Sub__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelSub__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 8,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 36,
																	"*": {
																		"LabelSub__": {
																			"type": "Label",
																			"data": [
																				"Sub__"
																			],
																			"options": {
																				"label": "_Sub",
																				"version": 0
																			},
																			"stamp": 59
																		},
																		"Sub__": {
																			"type": "ShortText",
																			"data": [
																				"Sub__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Sub",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 60
																		},
																		"Country__": {
																			"type": "ShortText",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Country",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 3,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"LabelCountry__": {
																			"type": "Label",
																			"data": [
																				"Country__"
																			],
																			"options": {
																				"label": "Country",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"Region__": {
																			"type": "ShortText",
																			"data": [
																				"Region__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Region",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 3,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 39
																		},
																		"LabelPostOfficeBox__": {
																			"type": "Label",
																			"data": [
																				"PostOfficeBox__"
																			],
																			"options": {
																				"label": "PostOfficeBox",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"LabelRegion__": {
																			"type": "Label",
																			"data": [
																				"Region__"
																			],
																			"options": {
																				"label": "Region",
																				"version": 0
																			},
																			"stamp": 41
																		},
																		"PostOfficeBox__": {
																			"type": "ShortText",
																			"data": [
																				"PostOfficeBox__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "PostOfficeBox",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 20,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 42
																		},
																		"PostalCode__": {
																			"type": "ShortText",
																			"data": [
																				"PostalCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Postal code",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 10,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 43
																		},
																		"LabelPostalCode__": {
																			"type": "Label",
																			"data": [
																				"PostalCode__"
																			],
																			"options": {
																				"label": "Postal code",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"City__": {
																			"type": "ShortText",
																			"data": [
																				"City__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "City",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 35,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"LabelCity__": {
																			"type": "Label",
																			"data": [
																				"City__"
																			],
																			"options": {
																				"label": "City",
																				"version": 0
																			},
																			"stamp": 46
																		},
																		"Street__": {
																			"type": "ShortText",
																			"data": [
																				"Street__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Street",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"length": 50,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 47
																		},
																		"LabelStreet__": {
																			"type": "Label",
																			"data": [
																				"Street__"
																			],
																			"options": {
																				"label": "Street",
																				"version": 0
																			},
																			"stamp": 48
																		},
																		"LabelCompanyAddress__": {
																			"type": "Label",
																			"data": [],
																			"options": {
																				"label": "_CompanyAddressFormated",
																				"version": 0
																			},
																			"stamp": 49
																		},
																		"CompanyAddress__": {
																			"type": "LongText",
																			"data": [],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 5,
																				"label": "_CompanyAddressFormated",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"numberOfLines": 5,
																				"readonly": true,
																				"notInDB": true,
																				"dataType": "String",
																				"browsable": false
																			},
																			"stamp": 50
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 51,
									"data": []
								}
							},
							"stamp": 52,
							"data": []
						}
					},
					"stamp": 53,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 54,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 55,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 56,
							"data": []
						}
					},
					"stamp": 57,
					"data": []
				}
			},
			"stamp": 58,
			"data": []
		}
	},
	"stamps": 64,
	"data": []
}