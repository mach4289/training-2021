///#GLOBALS Lib
function recallInvoice(trn)
{
	var vars = trn.GetVars(false);
	var msnEx = vars.GetValue("MSNEX", 0);
	var invoiceNumber = vars.GetValue("InvoiceNumber__", 0);
	var invoiceStatus = vars.GetValue("InvoiceStatus__", 0);
	if (invoiceStatus === "To approve" || invoiceStatus === "On hold")
	{
		// Call the validate action "recalltoap" for the current transport
		vars.AddValue_String("RequestedActions", "approve|recalltoap", true);
		vars.AddValue_String("NeedValidation", "1", true);
		vars.AddValue_String("LastValidatorUserId__", Data.GetValue("OwnerID"), true);
		var validator = Users.GetUser(Data.GetValue("OwnerID"));
		var validatorVars = validator.GetVars();
		vars.AddValue_String("LastValidatorName__", validatorVars.GetValue_String("DisplayName", 0), true);
		vars.AddValue_String("Comment__", Data.GetValue("RecallToAPComment__"), true);
		trn.Validate("recalltoap");
		if (trn.GetLastError() !== 0)
		{
			Log.Error(msnEx + " invoice " + invoiceNumber + " recall failed :" + trn.GetLastErrorMessage());
		}
		else
		{
			Log.Info(msnEx + " invoice " + invoiceNumber + " recall ok");
		}
	}
	else
	{
		Log.Warn(msnEx + " invoice " + invoiceNumber + " already in accounting (status = " + invoiceStatus + ")");
	}
}

function queryFilter(ruids)
{
	var filter = "|";
	var ruidexList = ruids.split("|");
	for (var i = 0; i < ruidexList.length; i++)
	{
		filter += "(Ruidex=" + ruidexList[i] + ")";
	}
	return filter;
}

function run()
{
	var ruidexList = Variable.GetValueAsString("AncestorsRuid");
	if (ruidexList)
	{
		var query = Process.CreateQueryAsProcessAdmin();
		query.SetSpecificTable("CDNAME#Vendor invoice");
		// Must select all attributes to be able to call custom action recalltoap
		query.SetAttributesList("*");
		query.SetFilter(queryFilter(ruidexList));
		if (query.MoveFirst())
		{
			var trn = query.MoveNext();
			while (trn)
			{
				recallInvoice(trn);
				trn = query.MoveNext();
			}
		}
	}
}
run();