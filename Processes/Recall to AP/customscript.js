// RUN PART
var tabName = "_My documents-AP_SAP";
var discountViewName = "_AP_SAP_View_Vendor invoice - embedded";
var paymentFeeViewName = "_AP_SAP_View_Vendor invoice payment fee - embedded";
var paymentFeeFromViewName = "_AP_SAP_View_Invoices overdue";
var processName = "Vendor Invoice";

function getInvoicesFilter(ruids)
{
	if (!ruids)
	{
		return {
			msnex: ["0"]
		};
	}
	var msnexlist = ruids.split("|");
	for (var i = 0; i < msnexlist.length; i++)
	{
		msnexlist[i] = msnexlist[i].split(".")[1];
	}
	return {
		"msnex": msnexlist
	};
}

function run()
{
	var ancestorRuids = Variable.GetValueAsString("AncestorsRuid");
	var fromView = Variable.GetValueAsString("FromView");
	var view = fromView === paymentFeeFromViewName ? paymentFeeViewName : discountViewName;
	if (ancestorRuids)
	{
		var ancestors = ancestorRuids.split("|");
		Controls.InvoicesToRecallCount__.SetText("_InvoicesToRecallCountLabel", ancestors.length);
		Controls.InvoicesView__.SetView(tabName, view, processName);
		Controls.InvoicesView__.SetFilterParameters(getInvoicesFilter(ancestorRuids));
		Controls.InvoicesView__.CheckProfileTab(false);
		Controls.InvoicesView__.Apply();
	}
	else
	{
		Controls.InvoicesToRecallCount__.SetText("_NoInvoicesToRecallCountLabel");
	}
	if (ProcessInstance.isReadOnly)
	{
		Controls.RecallToAPComment__.Hide(true);
	}
	else
	{
		Controls.RecallToAPComment__.SetPlaceholder(Language.Translate("_Enter your comment..."));
	}
}
run();