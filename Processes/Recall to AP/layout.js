{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"height": "25%"
										},
										"hidden": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"label": "_Documents"
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 4
														}
													},
													"stamp": 5,
													"data": []
												}
											},
											"stamp": 6,
											"data": []
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"DataPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Document data",
																"hidden": true
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 0,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {},
																				"colspans": []
																			},
																			"data": [
																				"fields"
																			],
																			"*": {},
																			"stamp": 8
																		}
																	},
																	"stamp": 9,
																	"data": []
																}
															},
															"stamp": 10,
															"data": []
														}
													},
													"stamp": 11,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 12,
															"data": []
														}
													},
													"stamp": 13,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 14,
															"data": []
														}
													},
													"stamp": 15,
													"data": []
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 16,
													"*": {
														"TitlePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": " ",
																"leftImageURL": "",
																"removeMargins": false
															},
															"stamp": 17,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {}
																	},
																	"stamp": 18,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TitleRecallToAP__": {
																						"line": 1,
																						"column": 1
																					},
																					"SubTitleRecallToAP__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"stamp": 19,
																			"*": {
																				"TitleRecallToAP__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_TitleRecallToAP"
																					},
																					"stamp": 20
																				},
																				"SubTitleRecallToAP__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_SubTitleRecallToAP"
																					},
																					"stamp": 21
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 22,
													"*": {
														"RecallToAPPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "400",
																"iconURL": "AP_WorkflowBacktoAP.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RecallToAPPane",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0
															},
															"stamp": 23,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 24,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"InvoicesToRecallCount__": {
																						"line": 1,
																						"column": 1
																					},
																					"InvoicesView__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 4,
																						"column": 1
																					},
																					"RecallToAPComment__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 25,
																			"*": {
																				"InvoicesToRecallCount__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_NoInvoicesToRecallCountLabel"
																					},
																					"stamp": 26
																				},
																				"InvoicesView__": {
																					"type": "AdminList",
																					"data": false,
																					"options": {
																						"width": "100%",
																						"restrictToCurrentJobId": false,
																						"showActions": false,
																						"label": "Invoices to pay",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line"
																					},
																					"stamp": 28
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2"
																					},
																					"stamp": 29
																				},
																				"RecallToAPComment__": {
																					"type": "LongText",
																					"data": [
																						"RecallToAPComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_recallToAPReason",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 30
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 31,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 32
																}
															},
															"stamp": 33,
															"data": []
														}
													},
													"stamp": 34,
													"data": []
												}
											},
											"stamp": 35,
											"data": []
										}
									},
									"stamp": 36,
									"data": []
								}
							},
							"stamp": 37,
							"data": []
						}
					},
					"stamp": 38,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"RecallInvoices": {
							"type": "SubmitButton",
							"options": {
								"label": "_RecallInvoices",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textColor": "default",
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": ""
							},
							"stamp": 39,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 40,
							"data": []
						}
					},
					"stamp": 41,
					"data": []
				}
			},
			"stamp": 42,
			"data": []
		}
	},
	"stamps": 42,
	"data": []
}