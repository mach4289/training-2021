{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "23%"
										},
										"hidden": true,
										"hideSeparator": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 4,
											"*": {
												"WizardStepsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "color1",
														"labelsAlignment": "center",
														"label": "_WizardStepsPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"version": 0,
														"labelLength": 0
													},
													"stamp": 5,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 6,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"displayAsFlex": true,
																		"ctrlsPos": {
																			"EditRequestStepButton__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer1__": {
																				"line": 2,
																				"column": 1
																			},
																			"ApprovalStepButton__": {
																				"line": 3,
																				"column": 1
																			},
																			"Spacer2__": {
																				"line": 4,
																				"column": 1
																			},
																			"ResultStepButton__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"lines": 5,
																		"colspans": [
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"EditRequestStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_EditRequestStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color8",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-edit fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"Spacer1__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"ApprovalStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_ApprovalStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-check-square-o fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"Spacer2__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"ResultStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_ResultStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-flag-checkered fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 12
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 19,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "23%",
											"left": "0%",
											"width": null,
											"height": "77%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 0,
													"width": 40,
													"height": 100
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning",
												"hideSeparator": true
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 20,
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {
																	"collapsed": false
																},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"hidden": false,
																"iconURL": "PO_preview.png",
																"hideActionButtons": true,
																"TitleFontSize": "S",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0,
																		"initialZoom": "width"
																	},
																	"stamp": 21
																}
															},
															"stamp": 22,
															"data": []
														}
													}
												}
											},
											"stamp": 23,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 40,
													"width": 60,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 24,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"leftImageURL": "",
																"removeMargins": false,
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 25,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"restored_PaymentPercent__": "restored_PaymentPercent___label__",
																			"restored_PaymentPercent___label__": "restored_PaymentPercent__"
																		},
																		"version": 0
																	},
																	"stamp": 26,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					},
																					"restored_PaymentPercent___label__": {
																						"line": 2,
																						"column": 1
																					},
																					"restored_PaymentPercent__": {
																						"line": 2,
																						"column": 2
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 27,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"label": "_TopMessageWarning",
																						"version": 0,
																						"iconClass": ""
																					},
																					"stamp": 28
																				},
																				"restored_PaymentPercent___label__": {
																					"type": "Label",
																					"data": [
																						"PaymentPercent__"
																					],
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"restored_PaymentPercent__": {
																					"type": "Decimal",
																					"data": [
																						"PaymentPercent__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "restored_PaymentPercent__",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 30
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 377,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_Banner",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 369,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 370,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 371,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_HTML",
																						"width": "100%",
																						"htmlContent": "PO Edit Request",
																						"version": 0,
																						"css": "@ span {line-height: 44px;}@ "
																					},
																					"stamp": 381
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 393,
													"*": {
														"Action": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "color8",
																"labelsAlignment": "right",
																"label": "_Action",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 383,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 384,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"displayAsFlex": true,
																				"ctrlsPos": {
																					"SubmitApprove__": {
																						"line": 2,
																						"column": 1
																					},
																					"Reject__": {
																						"line": 4,
																						"column": 1
																					},
																					"Quit__": {
																						"line": 7,
																						"column": 1
																					},
																					"Cancel__": {
																						"line": 6,
																						"column": 1
																					},
																					"Save__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 1,
																						"column": 1
																					},
																					"HTML__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"colspans": [
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 386,
																			"*": {
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "10px",
																						"width": "100%",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 408
																				},
																				"SubmitApprove__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SubmitApprove",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "90",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-approve-check-circle fa-4x"
																					},
																					"stamp": 397
																				},
																				"Save__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Save",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "90",
																						"action": "none",
																						"url": "",
																						"hidden": true,
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-save-circle fa-4x"
																					},
																					"stamp": 401
																				},
																				"Reject__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Reject",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color2",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "100",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"hidden": true,
																						"iconColor": "color2",
																						"awesomeClasses": "esk-ifont-reject-document-circle fa-4x"
																					},
																					"stamp": 399
																				},
																				"Cancel__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Cancel",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "100",
																						"action": "none",
																						"url": "",
																						"hidden": true,
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-times-circle fa-4x"
																					},
																					"stamp": 403
																				},
																				"HTML__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_HTML",
																						"width": "50",
																						"htmlContent": "<div style=\"width: 80px\"></div>",
																						"version": 0
																					},
																					"stamp": 426
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color9",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "",
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "100",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"iconColor": "color1",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x"
																					},
																					"stamp": 401
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "3px",
																						"width": "100%",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 429
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 418,
													"*": {
														"HelpPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"label": "_HelpPane",
																"version": 0,
																"backgroundcolor": "default",
																"labelsAlignment": "center",
																"hideTitle": true,
																"hidden": true,
																"sameHeightAsSiblings": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"labelLength": 0
															},
															"stamp": 14,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 15,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line5__": {
																						"line": 2,
																						"column": 1
																					},
																					"HelpDescription__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 16,
																			"*": {
																				"HelpDescription__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color1",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_HelpDescription",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 18
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 17
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 31,
													"*": {
														"Requisition_information": {
															"type": "PanelData",
															"options": {
																"border": {
																	"collapsed": false,
																	"maximized": false
																},
																"version": 0,
																"labelLength": "200px",
																"label": "_Requisition information",
																"iconURL": "",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"hideTitle": false,
																"removeMargins": false,
																"hidden": false,
																"sameHeightAsSiblings": false,
																"elementsAlignment": "left",
																"isSvgIcon": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"OrderNumber__": "LabelOrder_number__",
																			"LabelOrder_number__": "OrderNumber__",
																			"BuyerName__": "LabelBuyer_name__",
																			"LabelBuyer_name__": "BuyerName__",
																			"BuyerLogin__": "LabelBuyer_login__",
																			"LabelBuyer_login__": "BuyerLogin__",
																			"LabelCompany_code__": "CompanyCode__",
																			"CompanyCode__": "LabelCompany_code__",
																			"TotalNetLocalCurrency__": "LabelTotalNetLocalCurrency__",
																			"LabelTotalNetLocalCurrency__": "TotalNetLocalCurrency__",
																			"OpenOrderLocalCurrency__": "LabelOpenOrderLocalCurrency__",
																			"LabelOpenOrderLocalCurrency__": "OpenOrderLocalCurrency__",
																			"OrderDate__": "LabelOrderDate__",
																			"LabelOrderDate__": "OrderDate__",
																			"ERP__": "LabelERP__",
																			"LabelERP__": "ERP__",
																			"InfoTotalNetAmount__": "LabelInfoTotalNetAmount__",
																			"LabelInfoTotalNetAmount__": "InfoTotalNetAmount__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__",
																			"Currency__": "LabelCurrency__",
																			"LabelCurrency__": "Currency__",
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"Status__": "LabelStatus__",
																			"LabelStatus__": "Status__",
																			"ShipToCompany__": "LabelShipToCompany__",
																			"LabelShipToCompany__": "ShipToCompany__",
																			"NextTotalNetAmount__": "LabelNextTotalNetAmount__",
																			"LabelNextTotalNetAmount__": "NextTotalNetAmount__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 17,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"OrderNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelOrder_number__": {
																						"line": 1,
																						"column": 1
																					},
																					"BuyerName__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelBuyer_name__": {
																						"line": 5,
																						"column": 1
																					},
																					"BuyerLogin__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelBuyer_login__": {
																						"line": 6,
																						"column": 1
																					},
																					"LabelCompany_code__": {
																						"line": 3,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 3,
																						"column": 2
																					},
																					"TotalNetLocalCurrency__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelTotalNetLocalCurrency__": {
																						"line": 9,
																						"column": 1
																					},
																					"OpenOrderLocalCurrency__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelOpenOrderLocalCurrency__": {
																						"line": 10,
																						"column": 1
																					},
																					"OrderDate__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelOrderDate__": {
																						"line": 11,
																						"column": 1
																					},
																					"Spacer_line4__": {
																						"line": 14,
																						"column": 1
																					},
																					"ERP__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelERP__": {
																						"line": 4,
																						"column": 1
																					},
																					"InfoTotalNetAmount__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelInfoTotalNetAmount__": {
																						"line": 12,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 15,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 15,
																						"column": 1
																					},
																					"Currency__": {
																						"line": 16,
																						"column": 2
																					},
																					"LabelCurrency__": {
																						"line": 16,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 7,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 8,
																						"column": 1
																					},
																					"Status__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 2,
																						"column": 1
																					},
																					"ShipToCompany__": {
																						"line": 17,
																						"column": 2
																					},
																					"LabelShipToCompany__": {
																						"line": 17,
																						"column": 1
																					},
																					"NextTotalNetAmount__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelNextTotalNetAmount__": {
																						"line": 13,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelOrder_number__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"version": 0,
																						"hidden": false
																					},
																					"stamp": 33
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_Order number",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": false,
																						"browsable": false,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 34
																				},
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"label": "_Status",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 35
																				},
																				"Status__": {
																					"type": "ComboBox",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_ToApprove",
																							"2": "_Accepted",
																							"3": "_Rejected"
																						},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "ToApprove",
																							"2": "Accepted",
																							"3": "Rejected"
																						},
																						"label": "_Status",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 36
																				},
																				"LabelERP__": {
																					"type": "Label",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"label": "_ERP",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 37
																				},
																				"ERP__": {
																					"type": "ComboBox",
																					"data": [
																						"ERP__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_SAP",
																							"1": "_Generic",
																							"2": "_EBS",
																							"3": "_NAV",
																							"4": "_JDE"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "SAP",
																							"1": "generic",
																							"2": "EBS",
																							"3": "NAV",
																							"4": "JDE"
																						},
																						"label": "_ERP",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true,
																						"hidden": true,
																						"maxNbLinesToDisplay": 10
																					},
																					"stamp": 38
																				},
																				"LabelCompany_code__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 39
																				},
																				"CompanyCode__": {
																					"type": "ShortText",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"activable": true,
																						"width": "275",
																						"version": 0,
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 40
																				},
																				"LabelBuyer_name__": {
																					"type": "Label",
																					"data": [
																						"BuyerName__"
																					],
																					"options": {
																						"label": "_Buyer name",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"BuyerName__": {
																					"type": "ShortText",
																					"data": [
																						"BuyerName__"
																					],
																					"options": {
																						"label": "_Buyer name",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default"
																					},
																					"stamp": 44
																				},
																				"LabelBuyer_login__": {
																					"type": "Label",
																					"data": [
																						"BuyerLogin__"
																					],
																					"options": {
																						"label": "_Buyer login",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 45
																				},
																				"BuyerLogin__": {
																					"type": "ShortText",
																					"data": [
																						"BuyerLogin__"
																					],
																					"options": {
																						"label": "_Buyer login",
																						"activable": true,
																						"width": "180",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 46
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_VendorName",
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"VendorName__": {
																					"type": "ShortText",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 48
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_VendorNumber",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 49
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 50
																				},
																				"LabelTotalNetLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"label": "_TotalNetLocalCurrency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 51
																				},
																				"TotalNetLocalCurrency__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetLocalCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_TotalNetLocalCurrency",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 52
																				},
																				"LabelOpenOrderLocalCurrency__": {
																					"type": "Label",
																					"data": [
																						"OpenOrderLocalCurrency__"
																					],
																					"options": {
																						"label": "_OpenOrderLocalCurrency",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 53
																				},
																				"OpenOrderLocalCurrency__": {
																					"type": "Decimal",
																					"data": [
																						"OpenOrderLocalCurrency__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_OpenOrderLocalCurrency",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"hidden": true
																					},
																					"stamp": 54
																				},
																				"LabelOrderDate__": {
																					"type": "Label",
																					"data": [
																						"OrderDate__"
																					],
																					"options": {
																						"label": "_OrderDate",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 55
																				},
																				"OrderDate__": {
																					"type": "DateTime",
																					"data": [
																						"OrderDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_OrderDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"version": 0,
																						"helpIconPosition": "Right",
																						"hidden": true
																					},
																					"stamp": 56
																				},
																				"LabelInfoTotalNetAmount__": {
																					"type": "Label",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"label": "_Info total net amount",
																						"dataSource": 1,
																						"version": 0
																					},
																					"stamp": 57
																				},
																				"InfoTotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"TotalNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_Info total net amount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"enablePlusMinus": false,
																						"readonly": true,
																						"dataSource": 1,
																						"notInDB": true,
																						"dataType": "Number",
																						"autocompletable": false
																					},
																					"stamp": 58
																				},
																				"LabelNextTotalNetAmount__": {
																					"type": "Label",
																					"data": [
																						"NextTotalNetAmount__"
																					],
																					"options": {
																						"label": "_NextTotalNetAmount",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 406
																				},
																				"NextTotalNetAmount__": {
																					"type": "Decimal",
																					"data": [
																						"NextTotalNetAmount__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": false,
																						"label": "_NextTotalNetAmount",
																						"precision_internal": 2,
																						"precision_current": 2,
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"currencySource": "Currency__",
																						"enablePlusMinus": false,
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 407
																				},
																				"Spacer_line4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line4",
																						"version": 0
																					},
																					"stamp": 59
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 60
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"length": 5000,
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 61
																				},
																				"LabelCurrency__": {
																					"type": "Label",
																					"data": [
																						"ComboBox__"
																					],
																					"options": {
																						"label": "_Currency",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 62
																				},
																				"Currency__": {
																					"type": "ComboBox",
																					"data": [
																						"Currency__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Euro",
																							"1": "USD"
																						},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "EUR",
																							"1": "USD"
																						},
																						"label": "_Currency",
																						"activable": true,
																						"width": "180",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 63
																				},
																				"LabelShipToCompany__": {
																					"type": "Label",
																					"data": [
																						"DatabaseComboBox__"
																					],
																					"options": {
																						"label": "_Ship to company",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 366
																				},
																				"ShipToCompany__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"ShipToCompany__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches",
																						"label": "_Ship to company",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"fTSmaxRecords": "20",
																						"hidden": true,
																						"readonly": true,
																						"browsable": true
																					},
																					"stamp": 367
																				}
																			},
																			"stamp": 64
																		}
																	},
																	"stamp": 65,
																	"data": []
																}
															},
															"stamp": 66,
															"data": []
														}
													}
												},
												"form-content-right-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 67,
													"*": {
														"Line_items": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"label": "_Line items",
																"version": 0,
																"iconURL": "",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hidden": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"isSvgIcon": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"labelLength": 0
															},
															"stamp": 68,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"HideUnmodifiedRows__": "LabelHideUnmodifiedRows__",
																			"LabelHideUnmodifiedRows__": "HideUnmodifiedRows__"
																		},
																		"version": 0
																	},
																	"stamp": 69,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Ligne_d_espacement__": {
																						"line": 1,
																						"column": 1
																					},
																					"LineItems__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line__": {
																						"line": 5,
																						"column": 1
																					},
																					"HideUnmodifiedRows__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelHideUnmodifiedRows__": {
																						"line": 3,
																						"column": 1
																					},
																					"FilterChangedItems__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 70,
																			"*": {
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 71
																				},
																				"FilterChangedItems__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_FilterChangedItems",
																						"label": "_Button",
																						"textPosition": "text-left",
																						"textSize": "S",
																						"textStyle": "default",
																						"textColor": "default",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "AP - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"style": 5,
																						"width": "190",
																						"action": "none",
																						"url": "",
																						"awesomeClasses": "fa fa-toggle-off fa-2x",
																						"version": 0
																					},
																					"stamp": 447
																				},
																				"LabelHideUnmodifiedRows__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "_HideUnmodifiedRows",
																						"hidden": false,
																						"version": 0
																					},
																					"stamp": 427
																				},
																				"HideUnmodifiedRows__": {
																					"type": "CheckBox",
																					"data": [],
																					"options": {
																						"label": "_HideUnmodifiedRows",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"defaultValue": true,
																						"notInDB": true,
																						"dataType": "Boolean",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 428
																				},
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 58,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Line items",
																						"readonly": true,
																						"subsection": null
																					},
																					"stamp": 72,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 73,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 74,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 76,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 77,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Label",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"label": "_Line item number",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 78
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 79,
																									"data": [],
																									"*": {
																										"RequestedModification__": {
																											"type": "Label",
																											"data": [
																												"RequestedModification__"
																											],
																											"options": {
																												"label": "_RequestedModification",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 80,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 81,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "Label",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"label": "_ItemType",
																												"minwidth": 90,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 82,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "100"
																									},
																									"stamp": 83,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "Label",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"version": 0,
																												"minwidth": "100"
																											},
																											"stamp": 84
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "260"
																									},
																									"stamp": 85,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"version": 0,
																												"minwidth": "260"
																											},
																											"stamp": 86
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 87,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Label",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"label": "_PRLineNumber",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 88,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 89,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "Label",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"label": "_PRNumber",
																												"version": 0,
																												"minwidth": "100",
																												"hidden": true
																											},
																											"stamp": 90,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 91,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "Label",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"label": "_PRRUIDEX",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 92,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 93,
																									"data": [],
																									"*": {
																										"PRSubmissionDateTime__": {
																											"type": "Label",
																											"data": [
																												"PRSubmissionDateTime__"
																											],
																											"options": {
																												"label": "_PRSubmissionDateTime",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 94,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "105"
																									},
																									"stamp": 95,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemRequestedDeliveryDate",
																												"version": 0,
																												"minwidth": "80"
																											},
																											"stamp": 96,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "105",
																										"version": 0
																									},
																									"stamp": 97,
																									"data": [],
																									"*": {
																										"ProposedDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ProposedDeliveryDate__"
																											],
																											"options": {
																												"label": "_ProposedDeliveryDate",
																												"minwidth": "80",
																												"version": 0
																											},
																											"stamp": 98,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "75"
																									},
																									"stamp": 99,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"label": "_Ordered quantity",
																												"version": 0,
																												"minwidth": "60"
																											},
																											"stamp": 100
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "75",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 363,
																									"data": [],
																									"*": {
																										"ProposedQuantity__": {
																											"type": "Label",
																											"data": [
																												"ProposedQuantity__"
																											],
																											"options": {
																												"label": "_ProposedQuantity",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 364,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 101,
																									"data": [],
																									"*": {
																										"ItemTotalDeliveredQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemTotalDeliveredQuantity__"
																											],
																											"options": {
																												"label": "_ItemTotalDeliveredQuantity",
																												"version": 0,
																												"minwidth": "60",
																												"hidden": true
																											},
																											"stamp": 102,
																											"position": [
																												"Nombre d�cimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "70"
																									},
																									"stamp": 103,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"label": "_Item unit price",
																												"version": 0,
																												"minwidth": "70"
																											},
																											"stamp": 104
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "105",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 403,
																									"data": [],
																									"*": {
																										"ProposedUnitPrice__": {
																											"type": "Label",
																											"data": [
																												"ProposedUnitPrice__"
																											],
																											"options": {
																												"label": "_ProposedUnitPrice",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 404,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 105,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"label": "_ItemOrderedAmount",
																												"version": 0,
																												"minwidth": "90",
																												"hidden": true
																											},
																											"stamp": 106
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "90",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 107,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"label": "_ItemReceivedAmount",
																												"minwidth": "90",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 108,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 109,
																									"data": [],
																									"*": {
																										"ItemDeliveryDate__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryDate__"
																											],
																											"options": {
																												"label": "_ItemDeliveryDate",
																												"minwidth": "180",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 110,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 111,
																									"data": [],
																									"*": {
																										"ItemDeliveryComplete__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryComplete__"
																											],
																											"options": {
																												"label": "_ItemDeliveryComplete",
																												"version": 0,
																												"hidden": true,
																												"minwidth": 140
																											},
																											"stamp": 112,
																											"position": [
																												"Case � cocher"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 113,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "Label",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"label": "_ItemUnit",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 114,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 115,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"label": "_ItemUnitDescription",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 116,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 117,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "Label",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"label": "_ItemCurrency",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 118,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 119,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"label": "_ItemShipToCompany",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 120,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 121,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "Label",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"label": "_ItemDeliveryAddressID",
																												"minwidth": "80",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 122,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 123,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "Label",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"label": "_ItemShipToAddress",
																												"minwidth": "170",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 124,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 125,
																									"data": [],
																									"*": {
																										"ItemCompanyCode__": {
																											"type": "Label",
																											"data": [
																												"ItemCompanyCode__"
																											],
																											"options": {
																												"label": "_ItemCompanyCode",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 126,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 127,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"label": "_ItemCostCenterId",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 128,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 129,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "Label",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"label": "_ItemCostCenterName",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 130,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 131,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "Label",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"label": "_ProjectCode",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 132,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 133,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "Label",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"label": "_ProjectCodeDescription",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 134,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 135,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "Label",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"label": "_InternalOrder",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 136,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 137,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "Label",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"label": "_WBSElement",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 138,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 139,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "Label",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"label": "_WBSElementID",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 140,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 141,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "Label",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"label": "_FreeDimension1",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 142,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 143,
																									"data": [],
																									"*": {
																										"SupplyTypeId__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeId__"
																											],
																											"options": {
																												"label": "_Supply type ID",
																												"version": 0,
																												"hidden": true,
																												"minwidth": "90"
																											},
																											"stamp": 144,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 145,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"label": "_SupplyTypeFullpath",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 146,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 147,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "Label",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"label": "_Supply type name",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 148,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 149,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"version": 0,
																												"hidden": true,
																												"minwidth": "60"
																											},
																											"stamp": 150
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 151,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 152,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 153,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"label": "_ItemTaxAmount",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 154,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 155,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Label",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"label": "_LeadTime",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 156,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "Label",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"version": 0,
																												"hidden": true,
																												"minwidth": "70"
																											},
																											"stamp": 158,
																											"position": [
																												"Select from a table"
																											]
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 159,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "Label",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"label": "_CostType",
																												"minwidth": 80,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 160,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "Label",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"label": "_ItemGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 162
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 163,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "Label",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"label": "_BudgetID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 164,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 165,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "Label",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"label": "_RequesterDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 166,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"ItemRequester__": {
																											"type": "Label",
																											"data": [
																												"ItemRequester__"
																											],
																											"options": {
																												"label": "_ItemRequester",
																												"version": 0,
																												"minwidth": "120",
																												"hidden": true
																											},
																											"stamp": 168,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"BuyerDN__": {
																											"type": "Label",
																											"data": [
																												"BuyerDN__"
																											],
																											"options": {
																												"label": "_BuyerDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 170,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "Label",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"label": "_ItemInCxml",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 172,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"RequestedVendor__": {
																											"type": "Label",
																											"data": [
																												"RequestedVendor__"
																											],
																											"options": {
																												"label": "_RequestedVendor",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 174,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 175,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "Label",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"label": "_RecipientDN",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 176,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 177,
																									"data": [],
																									"*": {
																										"ItemRecipient__": {
																											"type": "Label",
																											"data": [
																												"ItemRecipient__"
																											],
																											"options": {
																												"label": "_ItemRecipient",
																												"version": 0,
																												"minwidth": "120",
																												"hidden": true
																											},
																											"stamp": 178,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 179,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Label",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"label": "_ItemExchangeRate",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 180,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 181,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "Label",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 182,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 183,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "Label",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 184,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 185,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "Label",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 186,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "260",
																										"version": 0
																									},
																									"stamp": 187,
																									"data": [],
																									"*": {
																										"ItemComment__": {
																											"type": "Label",
																											"data": [
																												"ItemComment__"
																											],
																											"options": {
																												"label": "_ItemComment",
																												"minwidth": "260",
																												"version": 0
																											},
																											"stamp": 188,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 189,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 190,
																									"data": [],
																									"*": {
																										"LineItemNumber__": {
																											"type": "Integer",
																											"data": [
																												"LineItemNumber__"
																											],
																											"options": {
																												"integer": true,
																												"precision_internal": 0,
																												"label": "_Line item number",
																												"activable": true,
																												"width": "25",
																												"readonly": true,
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 191
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 192,
																									"data": [],
																									"*": {
																										"RequestedModification__": {
																											"type": "ComboBox",
																											"data": [
																												"RequestedModification__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_NoChange",
																													"1": "_DeliveryDateChange",
																													"2": "_QuantityAmountChange",
																													"3": "_NoLongerAvailableChange",
																													"4": "_Other"
																												},
																												"maxNbLinesToDisplay": 10,
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "NoChange",
																													"1": "DeliveryDateChange",
																													"2": "QuantityAmountChange",
																													"3": "NoLongerAvailableChange",
																													"4": "Other"
																												},
																												"label": "_RequestedModification",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": ""
																											},
																											"stamp": 193,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 194,
																									"data": [],
																									"*": {
																										"ItemType__": {
																											"type": "ComboBox",
																											"data": [
																												"ItemType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "_QuantityBased",
																													"1": "_AmountBased"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "QuantityBased",
																													"1": "AmountBased"
																												},
																												"label": "_ItemType",
																												"activable": true,
																												"width": 90,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"readonly": true,
																												"hidden": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 195,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 196,
																									"data": [],
																									"*": {
																										"ItemNumber__": {
																											"type": "ShortText",
																											"data": [
																												"ItemNumber__"
																											],
																											"options": {
																												"label": "_ItemNumber",
																												"activable": true,
																												"width": "100",
																												"version": 0,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 197
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 198,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "LongText",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": "260",
																												"resizable": true,
																												"minNbLines": 1,
																												"numberOfLines": 999,
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 199
																										}
																									},
																									"position": [
																										"Multiline text"
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 200,
																									"data": [],
																									"*": {
																										"PRLineNumber__": {
																											"type": "Integer",
																											"data": [
																												"PRLineNumber__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_PRLineNumber",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 201,
																											"position": [
																												"Nombre entier"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 202,
																									"data": [],
																									"*": {
																										"PRNumber__": {
																											"type": "ShortText",
																											"data": [
																												"PRNumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 203,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 204,
																									"data": [],
																									"*": {
																										"PRRUIDEX__": {
																											"type": "ShortText",
																											"data": [
																												"PRRUIDEX__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_PRRUIDEX",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 100,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 205,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 206,
																									"data": [],
																									"*": {
																										"PRSubmissionDateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"PRSubmissionDateTime__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_PRSubmissionDateTime",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 207,
																											"position": [
																												"_DateTime2"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 208,
																									"data": [],
																									"*": {
																										"ItemRequestedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemRequestedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemRequestedDeliveryDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 209,
																											"position": [
																												"Date"
																											]
																										}
																									}
																								},
																								"Cell11": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 210,
																									"data": [],
																									"*": {
																										"ProposedDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ProposedDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ProposedDeliveryDate",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"version": 0
																											},
																											"stamp": 211,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell12": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 212,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Ordered quantity",
																												"activable": true,
																												"width": "60",
																												"version": 1,
																												"autocompletable": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 213
																										}
																									}
																								},
																								"Cell13": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 362,
																									"data": [],
																									"*": {
																										"ProposedQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ProposedQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ProposedQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 365,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell14": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 214,
																									"data": [],
																									"*": {
																										"ItemTotalDeliveredQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTotalDeliveredQuantity__"
																											],
																											"options": {
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTotalDeliveredQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 215,
																											"position": [
																												"Nombre d�cimal"
																											]
																										}
																									}
																								},
																								"Cell15": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 216,
																									"data": [],
																									"*": {
																										"ItemUnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"ItemUnitPrice__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_Item unit price",
																												"activable": true,
																												"width": "70",
																												"version": 1,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 217
																										}
																									}
																								},
																								"Cell16": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 402,
																									"data": [],
																									"*": {
																										"ProposedUnitPrice__": {
																											"type": "Decimal",
																											"data": [
																												"ProposedUnitPrice__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ProposedUnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"currencySource": "Currency__",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 405,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell17": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 218,
																									"data": [],
																									"*": {
																										"ItemNetAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemNetAmount__"
																											],
																											"options": {
																												"integer": false,
																												"precision_internal": 2,
																												"label": "_ItemOrderedAmount",
																												"activable": true,
																												"width": "90",
																												"version": 1,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"precision_current": 2,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 219
																										}
																									}
																								},
																								"Cell18": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 220,
																									"data": [],
																									"*": {
																										"ItemReceivedAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemReceivedAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemReceivedAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false
																											},
																											"stamp": 221,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell19": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 222,
																									"data": [],
																									"*": {
																										"ItemDeliveryDate__": {
																											"type": "DateTime",
																											"data": [
																												"ItemDeliveryDate__"
																											],
																											"options": {
																												"displayLongFormat": false,
																												"label": "_ItemDeliveryDate",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"autocompletable": false,
																												"version": 0
																											},
																											"stamp": 223,
																											"position": [
																												"_DateTime"
																											]
																										}
																									}
																								},
																								"Cell20": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 224,
																									"data": [],
																									"*": {
																										"ItemDeliveryComplete__": {
																											"type": "CheckBox",
																											"data": [
																												"ItemDeliveryComplete__"
																											],
																											"options": {
																												"label": "_ItemDeliveryComplete",
																												"activable": true,
																												"width": 140,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"helpIconPosition": "Right"
																											},
																											"stamp": 225,
																											"position": [
																												"Case � cocher"
																											]
																										}
																									}
																								},
																								"Cell21": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 226,
																									"data": [],
																									"*": {
																										"ItemUnit__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnit__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnit",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"length": 3,
																												"PreFillFTS": true,
																												"readonly": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 227,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell22": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 228,
																									"data": [],
																									"*": {
																										"ItemUnitDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemUnitDescription__"
																											],
																											"options": {
																												"version": 1,
																												"label": "_ItemUnitDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"readonly": true,
																												"hidden": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains"
																											},
																											"stamp": 229,
																											"position": [
																												"Sélection dans une table"
																											]
																										}
																									}
																								},
																								"Cell23": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 230,
																									"data": [],
																									"*": {
																										"ItemCurrency__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCurrency__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCurrency",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 231,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell24": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 232,
																									"data": [],
																									"*": {
																										"ItemShipToCompany__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemShipToCompany__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ItemShipToCompany",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true
																											},
																											"stamp": 233,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell25": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 234,
																									"data": [],
																									"*": {
																										"ItemDeliveryAddressID__": {
																											"type": "ShortText",
																											"data": [
																												"ItemDeliveryAddressID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemDeliveryAddressID",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 235,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell26": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 236,
																									"data": [],
																									"*": {
																										"ItemShipToAddress__": {
																											"type": "LongText",
																											"data": [
																												"ItemShipToAddress__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemShipToAddress",
																												"activable": true,
																												"width": "170",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"numberOfLines": 4,
																												"browsable": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 237,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell27": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 238,
																									"data": [],
																									"*": {
																										"ItemCompanyCode__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCompanyCode__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCompanyCode",
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 239,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell28": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 240,
																									"data": [],
																									"*": {
																										"ItemCostCenterId__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCostCenterId__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCostCenterId",
																												"activable": true,
																												"width": 140,
																												"browsable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 241,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell29": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 242,
																									"data": [],
																									"*": {
																										"ItemCostCenterName__": {
																											"type": "ShortText",
																											"data": [
																												"ItemCostCenterName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemCostCenterName",
																												"activable": true,
																												"width": 170,
																												"browsable": false,
																												"version": 0,
																												"readonly": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true
																											},
																											"stamp": 243,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell30": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 244,
																									"data": [],
																									"*": {
																										"ProjectCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCode",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true,
																												"RestrictSearch": true
																											},
																											"stamp": 245,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell31": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 246,
																									"data": [],
																									"*": {
																										"ProjectCodeDescription__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ProjectCodeDescription__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "matches",
																												"label": "_ProjectCodeDescription",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 247,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell32": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 248,
																									"data": [],
																									"*": {
																										"InternalOrder__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"InternalOrder__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_InternalOrder",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 249,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell33": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 250,
																									"data": [],
																									"*": {
																										"WBSElement__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElement__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElement",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true
																											},
																											"stamp": 251,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell34": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 252,
																									"data": [],
																									"*": {
																										"WBSElementID__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"WBSElementID__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_WBSElementID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 253,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell35": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 254,
																									"data": [],
																									"*": {
																										"FreeDimension1__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"FreeDimension1__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_FreeDimension1",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"browsable": true
																											},
																											"stamp": 255,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell36": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 256,
																									"data": [],
																									"*": {
																										"SupplyTypeId__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeId__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Supply type ID",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 257,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell37": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 258,
																									"data": [],
																									"*": {
																										"SupplyTypeFullpath__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeFullpath__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_SupplyTypeFullpath",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 500,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 259,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell38": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 260,
																									"data": [],
																									"*": {
																										"SupplyTypeName__": {
																											"type": "ShortText",
																											"data": [
																												"SupplyTypeName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_Supply type name",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 261,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell39": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 262,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"length": 600,
																												"fTSmaxRecords": "20",
																												"PreFillFTS": true,
																												"version": 1,
																												"browsable": true,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"helpIconPosition": "Right"
																											},
																											"stamp": 263
																										}
																									}
																								},
																								"Cell40": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 264,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 265,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell41": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 266,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTaxAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 267,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell42": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 268,
																									"data": [],
																									"*": {
																										"LeadTime__": {
																											"type": "Integer",
																											"data": [
																												"LeadTime__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_LeadTime",
																												"precision_internal": 0,
																												"precision_current": 0,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 269,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell43": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 270,
																									"data": [],
																									"*": {
																										"ItemGLAccount__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemGLAccount__"
																											],
																											"options": {
																												"label": "_Item glaccount",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"PreFillFTS": true,
																												"fTSmaxRecords": "20",
																												"browsable": true,
																												"version": 0,
																												"hidden": true,
																												"readonly": true,
																												"RestrictSearch": true,
																												"autoCompleteSearchMode": "startswith",
																												"searchMode": "contains",
																												"helpIconPosition": "Right"
																											},
																											"stamp": 271,
																											"position": [
																												"Select from a table"
																											]
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell44": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 272,
																									"data": [],
																									"*": {
																										"CostType__": {
																											"type": "ComboBox",
																											"data": [
																												"CostType__"
																											],
																											"options": {
																												"possibleValues": {
																													"0": "",
																													"1": "_OpEx",
																													"2": "_CapEx"
																												},
																												"version": 1,
																												"keyValueMode": true,
																												"possibleKeys": {
																													"0": "",
																													"1": "OpEx",
																													"2": "CapEx"
																												},
																												"label": "_CostType",
																												"activable": true,
																												"width": 80,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"DataSourceOrigin": "Standalone",
																												"DisplayedColumns": "",
																												"hidden": true,
																												"readonly": true,
																												"maxNbLinesToDisplay": 10
																											},
																											"stamp": 273,
																											"position": [
																												"_ComboBox"
																											]
																										}
																									}
																								},
																								"Cell45": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 274,
																									"data": [],
																									"*": {
																										"ItemGroup__": {
																											"type": "ShortText",
																											"data": [
																												"ItemGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemGroup",
																												"activable": true,
																												"width": "200",
																												"length": 64,
																												"browsable": false,
																												"version": 0,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 275
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell46": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 276,
																									"data": [],
																									"*": {
																										"BudgetID__": {
																											"type": "ShortText",
																											"data": [
																												"BudgetID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BudgetID",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 277,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell47": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 278,
																									"data": [],
																									"*": {
																										"RequesterDN__": {
																											"type": "ShortText",
																											"data": [
																												"RequesterDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequesterDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 279,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell48": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 280,
																									"data": [],
																									"*": {
																										"ItemRequester__": {
																											"type": "ShortText",
																											"data": [
																												"ItemRequester__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemRequester",
																												"activable": true,
																												"width": "120",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 281,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell49": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 282,
																									"data": [],
																									"*": {
																										"BuyerDN__": {
																											"type": "ShortText",
																											"data": [
																												"BuyerDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_BuyerDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 283,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell50": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 284,
																									"data": [],
																									"*": {
																										"ItemInCxml__": {
																											"type": "LongText",
																											"data": [
																												"ItemInCxml__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemInCxml",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"resizable": true,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"minNbLines": 3,
																												"hidden": true
																											},
																											"stamp": 285,
																											"position": [
																												"Multiline text"
																											]
																										}
																									}
																								},
																								"Cell51": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 286,
																									"data": [],
																									"*": {
																										"RequestedVendor__": {
																											"type": "ShortText",
																											"data": [
																												"RequestedVendor__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RequestedVendor",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 287,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell52": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 288,
																									"data": [],
																									"*": {
																										"RecipientDN__": {
																											"type": "ShortText",
																											"data": [
																												"RecipientDN__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_RecipientDN",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"length": 500,
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 289,
																											"position": [
																												"Texte"
																											]
																										}
																									}
																								},
																								"Cell53": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 290,
																									"data": [],
																									"*": {
																										"ItemRecipient__": {
																											"type": "ShortText",
																											"data": [
																												"ItemRecipient__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_ItemRecipient",
																												"activable": true,
																												"width": "120",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"version": 0,
																												"helpIconPosition": "Right",
																												"hidden": true
																											},
																											"stamp": 291,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell54": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 292,
																									"data": [],
																									"*": {
																										"ItemExchangeRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemExchangeRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemExchangeRate",
																												"precision_internal": 6,
																												"precision_current": 6,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"enablePlusMinus": false,
																												"hidden": true
																											},
																											"stamp": 293,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell55": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 294,
																									"data": [],
																									"*": {
																										"RequestedDeliveryDateInPast__": {
																											"type": "CheckBox",
																											"data": [
																												"RequestedDeliveryDateInPast__"
																											],
																											"options": {
																												"label": "_RequestedDeliveryDateInPast",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 295,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell56": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 296,
																									"data": [],
																									"*": {
																										"NoGoodsReceipt__": {
																											"type": "CheckBox",
																											"data": [
																												"NoGoodsReceipt__"
																											],
																											"options": {
																												"label": "_NoGoodsReceipt",
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 297,
																											"position": [
																												"Check box"
																											]
																										}
																									}
																								},
																								"Cell57": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 298,
																									"data": [],
																									"*": {
																										"Locked__": {
																											"type": "CheckBox",
																											"data": [
																												"Locked__"
																											],
																											"options": {
																												"label": "_Locked",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 299,
																											"position": [
																												"Case à cocher"
																											]
																										}
																									}
																								},
																								"Cell58": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 300,
																									"data": [],
																									"*": {
																										"ItemComment__": {
																											"type": "LongText",
																											"data": [
																												"ItemComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 3,
																												"label": "_ItemComment",
																												"activable": true,
																												"width": "260",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 301,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 302
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 304,
													"*": {
														"AdditionalFees_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "PO_AdditionalFees.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_AdditionalFees_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 305,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"BrowseAddFees__": "LabelBrowseAddFees__",
																			"LabelBrowseAddFees__": "BrowseAddFees__"
																		},
																		"version": 0
																	},
																	"stamp": 306,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AdditionalFees__": {
																						"line": 2,
																						"column": 1
																					},
																					"BrowseAddFees__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelBrowseAddFees__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line7__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line13__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 307,
																			"*": {
																				"Spacer_line13__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line13",
																						"version": 0
																					},
																					"stamp": 308
																				},
																				"AdditionalFees__": {
																					"type": "Table",
																					"data": [
																						"AdditionalFees__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 6,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_AdditionalFees",
																						"subsection": null
																					},
																					"stamp": 309,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 310,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 311
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 312
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 313,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 314,
																									"data": [],
																									"*": {
																										"AdditionalFeeID__": {
																											"type": "Label",
																											"data": [
																												"AdditionalFeeID__"
																											],
																											"options": {
																												"label": "_AdditionalFeeID",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 315,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 316,
																									"data": [],
																									"*": {
																										"AdditionalFeeDescription__": {
																											"type": "Label",
																											"data": [
																												"AdditionalFeeDescription__"
																											],
																											"options": {
																												"label": "_AdditionalFeeDescription",
																												"version": 0
																											},
																											"stamp": 317,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 318,
																									"data": [],
																									"*": {
																										"Price__": {
																											"type": "Label",
																											"data": [
																												"Price__"
																											],
																											"options": {
																												"label": "_Price",
																												"version": 0
																											},
																											"stamp": 319,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 320,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"label": "_Item tax code",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 321,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 322,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"label": "_Item tax rate",
																												"minwidth": "60",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 323,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 324,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Label",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"label": "_ItemTaxAmount",
																												"minwidth": "70",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 325,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 326,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 327,
																									"data": [],
																									"*": {
																										"AdditionalFeeID__": {
																											"type": "ShortText",
																											"data": [
																												"AdditionalFeeID__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_AdditionalFeeID",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false,
																												"hidden": true
																											},
																											"stamp": 328,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 329,
																									"data": [],
																									"*": {
																										"AdditionalFeeDescription__": {
																											"type": "ShortText",
																											"data": [
																												"AdditionalFeeDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_AdditionalFeeDescription",
																												"activable": true,
																												"width": "350",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 330,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 331,
																									"data": [],
																									"*": {
																										"Price__": {
																											"type": "Decimal",
																											"data": [
																												"Price__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Price",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format"
																											},
																											"stamp": 332,
																											"position": [
																												"Decimal number"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 333,
																									"data": [],
																									"*": {
																										"ItemTaxCode__": {
																											"type": "DatabaseComboBox",
																											"data": [
																												"ItemTaxCode__"
																											],
																											"options": {
																												"version": 1,
																												"autoCompleteSearchMode": "contains",
																												"searchMode": "contains",
																												"label": "_Item tax code",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"minNbLines": 1,
																												"maxNbLines": 1,
																												"fTSmaxRecords": "20",
																												"hidden": true,
																												"readonly": true,
																												"browsable": true,
																												"PreFillFTS": true,
																												"RestrictSearch": true,
																												"length": 600
																											},
																											"stamp": 334,
																											"position": [
																												"_DatabaseComboBox"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 335,
																									"data": [],
																									"*": {
																										"ItemTaxRate__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxRate__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_Item tax rate",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 336,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 337,
																									"data": [],
																									"*": {
																										"ItemTaxAmount__": {
																											"type": "Decimal",
																											"data": [
																												"ItemTaxAmount__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemTaxAmount",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 338,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"Spacer_line7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line7",
																						"version": 0
																					},
																					"stamp": 339
																				},
																				"LabelBrowseAddFees__": {
																					"type": "Label",
																					"data": [
																						"BrowseAddFees__"
																					],
																					"options": {
																						"label": "_BrowseAddFees",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 340
																				},
																				"BrowseAddFees__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"BrowseAddFees__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_BrowseAddFees",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"PreFillFTS": true,
																						"hidden": true,
																						"RestrictSearch": true,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains"
																					},
																					"stamp": 341
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 342,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Attachments",
																"iconURL": "PO_attachments.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 343
																}
															},
															"stamp": 344,
															"data": []
														}
													}
												},
												"form-content-right-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 345,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"iconURL": "",
																"hideActionButtons": true,
																"TitleFontSize": "S",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 346,
															"data": []
														}
													}
												},
												"form-content-right-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 347,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "PO_system_data.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 348,
															"data": []
														}
													}
												}
											},
											"stamp": 349,
											"data": []
										}
									},
									"stamp": 350,
									"data": []
								}
							},
							"stamp": 351,
							"data": []
						}
					},
					"stamp": 352,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {},
					"stamp": 360,
					"data": []
				}
			},
			"stamp": 361,
			"data": []
		}
	},
	"stamps": 460,
	"data": []
}