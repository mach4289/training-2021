///#GLOBALS Lib Sys
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- Edit PO Request Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
function SendToApproval() {
    var status = Data.GetValue("Status__");
    if (!Data.FormHasError() && status === "Draft") {
        Data.SetValue("Status__", "ToApprove");
        // Forward the form to the buyer
        var buyerLogin = Data.GetValue("BuyerLogin__");
        var buyerName = Data.GetValue("BuyerName__");
        var user = Users.GetUserAsProcessAdmin(buyerLogin);
        // Send an email to inform the buyer that he's got an edit request to approve
        Log.Info("Send email notification to " + buyerLogin);
        SendNotif(user, "Purchasing_Email_POMod_NotifBuyer.htm", true, { DestinationFullName: buyerName });
        Log.Info("Forward edit request to " + user.GetValue("login"));
        Process.Forward(buyerLogin);
        Process.LeaveForm();
    }
}
function ApproveRequest(reject) {
    if (reject === void 0) { reject = false; }
    if (Data.GetValue("Status__") === "ToApprove") {
        Data.SetValue("Status__", reject ? "Rejected" : "Approved");
        // Forward the form to the buyer
        var vendorLogin = Sys.Helpers.String.ExtractLoginFromDN(Data.GetValue("CreatorOwnerID"));
        var user = Users.GetUserAsProcessAdmin(vendorLogin);
        var customTags = {
            PortalUrl: user.GetProcessURL(Data.GetValue("RUIDEX"), true),
            IsApproved: !reject
        };
        // Send an email to inform the vendor the request has been approved
        Log.Info("Send " + (reject ? "rejection" : "approval") + " email notification to " + user.GetValue("login"));
        SendNotif(user, "Purchasing_Email_NotifPOModResult.htm", true, customTags);
    }
}
function SendNotif(user, template, backupUserAsCC, customTags) {
    var options = {
        backupUserAsCC: !!backupUserAsCC,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    };
    var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
    var fromName = currentUser.GetValue("DisplayName");
    Sys.EmailNotification.SendEmailNotificationWithUser(user, null, template, customTags, fromName, null, options);
}
if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
    switch (currentName) {
        case "Submit":
            SendToApproval();
            break;
        case "Approve":
            ApproveRequest();
            break;
        case "Reject":
            ApproveRequest(true);
            Data.SetValue("State", 400);
            break;
        default:
            Process.PreventApproval();
            break;
    }
}
