{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 1,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 2,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"label": "_Document data",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"Description__": "LabelDescription__",
																	"LabelDescription__": "Description__",
																	"TaxCode__": "LabelTaxCode__",
																	"LabelTaxCode__": "TaxCode__",
																	"TaxRate__": "LabelTaxRate__",
																	"LabelTaxRate__": "TaxRate__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"TaxAccount__": "LabelTaxAccount__",
																	"LabelTaxAccount__": "TaxAccount__",
																	"TaxAccountForCollection__": "LabelTaxAccountForCollection__",
																	"LabelTaxAccountForCollection__": "TaxAccountForCollection__",
																	"TaxType__": "LabelTaxType__",
																	"LabelTaxType__": "TaxType__",
																	"TaxClassification__": "LabelTaxClassification__",
																	"LabelTaxClassification__": "TaxClassification__",
																	"TaxRoundingPriority__": "LabelTaxRoundingPriority__",
																	"LabelTaxRoundingPriority__": "TaxRoundingPriority__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 9,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"TaxCode__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelTaxCode__": {
																				"line": 2,
																				"column": 1
																			},
																			"TaxRate__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelTaxRate__": {
																				"line": 4,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"TaxAccount__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelTaxAccount__": {
																				"line": 5,
																				"column": 1
																			},
																			"TaxAccountForCollection__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelTaxAccountForCollection__": {
																				"line": 6,
																				"column": 1
																			},
																			"TaxType__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelTaxType__": {
																				"line": 7,
																				"column": 1
																			},
																			"TaxClassification__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelTaxClassification__": {
																				"line": 8,
																				"column": 1
																			},
																			"TaxRoundingPriority__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelTaxRoundingPriority__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "Company code",
																				"version": 0
																			},
																			"stamp": 3
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "Company code",
																				"activable": true,
																				"width": 500,
																				"fTSmaxRecords": "20",
																				"version": 0,
																				"PreFillFTS": true
																			},
																			"stamp": 4
																		},
																		"LabelTaxCode__": {
																			"type": "Label",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "Tax code",
																				"version": 0
																			},
																			"stamp": 5
																		},
																		"TaxCode__": {
																			"type": "ShortText",
																			"data": [
																				"TaxCode__"
																			],
																			"options": {
																				"label": "Tax code",
																				"activable": true,
																				"width": "500",
																				"length": 600,
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 6
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"version": 0
																			},
																			"stamp": 7
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "Description",
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 8
																		},
																		"LabelTaxRate__": {
																			"type": "Label",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"label": "Tax rate",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"TaxRate__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate__"
																			],
																			"options": {
																				"integer": false,
																				"label": "Tax rate",
																				"precision_internal": 3,
																				"precision_current": 3,
																				"precision_min": 2,
																				"activable": true,
																				"width": "500",
																				"version": 0,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default"
																			},
																			"stamp": 10
																		},
																		"LabelTaxAccount__": {
																			"type": "Label",
																			"data": [
																				"TaxAccount__"
																			],
																			"options": {
																				"label": "Tax account"
																			},
																			"stamp": 11
																		},
																		"TaxAccount__": {
																			"type": "ShortText",
																			"data": [
																				"TaxAccount__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Tax account",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"browsable": false
																			},
																			"stamp": 12
																		},
																		"LabelTaxAccountForCollection__": {
																			"type": "Label",
																			"data": [
																				"TaxAccountForCollection__"
																			],
																			"options": {
																				"label": "Tax account for collection"
																			},
																			"stamp": 13
																		},
																		"TaxAccountForCollection__": {
																			"type": "ShortText",
																			"data": [
																				"TaxAccountForCollection__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Tax account for collection",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"browsable": false
																			},
																			"stamp": 14
																		},
																		"LabelTaxType__": {
																			"type": "Label",
																			"data": [
																				"TaxType__"
																			],
																			"options": {
																				"label": "Tax type"
																			},
																			"stamp": 15
																		},
																		"TaxType__": {
																			"type": "ShortText",
																			"data": [
																				"TaxType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Tax type",
																				"activable": true,
																				"width": "500",
																				"length": 10,
																				"browsable": false
																			},
																			"stamp": 16
																		},
																		"LabelTaxClassification__": {
																			"type": "Label",
																			"data": [
																				"TaxClassification__"
																			],
																			"options": {
																				"label": "Tax classification"
																			},
																			"stamp": 28
																		},
																		"TaxClassification__": {
																			"type": "ShortText",
																			"data": [
																				"TaxClassification__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "Tax classification",
																				"activable": true,
																				"width": "500",
																				"length": 20,
																				"browsable": false
																			},
																			"stamp": 29
																		},
																		"LabelTaxRoundingPriority__": {
																			"type": "Label",
																			"data": [
																				"ComboBox__"
																			],
																			"options": {
																				"label": "_Tax Rounding Priority"
																			},
																			"stamp": 30
																		},
																		"TaxRoundingPriority__": {
																			"type": "ComboBox",
																			"data": [
																				"TaxRoundingPriority__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_Round_Before_Sum",
																					"2": "_Round_After_Sum"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "Round_Before_Sum",
																					"2": "Round_After_Sum"
																				},
																				"label": "_Tax Rounding Priority",
																				"activable": true,
																				"width": 230,
																				"helpText": "_HelpTaxRoundingPriority",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 31
																		}
																	},
																	"stamp": 17
																}
															},
															"stamp": 18,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												}
											}
										}
									},
									"stamp": 20,
									"data": []
								}
							},
							"stamp": 21,
							"data": []
						}
					},
					"stamp": 22,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0
							},
							"stamp": 23,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 24,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 25,
							"data": []
						}
					},
					"stamp": 26,
					"data": []
				}
			},
			"stamp": 27,
			"data": []
		}
	},
	"stamps": 31,
	"data": []
}