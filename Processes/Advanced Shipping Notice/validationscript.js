///#GLOBALS Lib Sys
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- Advanced Shipping Notice Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
function SubmitASN() {
    if (Data.GetValue("Status__") === "Draft") {
        Data.SetValue("Status__", "Submitted");
        // Remove service lines without quantity
        var lineServicesTable = Data.GetTable("LineItems__");
        var curItem = void 0, quantity = void 0;
        for (var index = lineServicesTable.GetItemCount() - 1; index >= 0; index--) {
            curItem = lineServicesTable.GetItem(index);
            quantity = curItem.GetValue("ItemQuantity__");
            if (Sys.Helpers.IsEmpty(quantity) || quantity === 0) {
                curItem.RemoveItem();
            }
        }
        /** NOTIF */
        var userID = Variable.GetValueAsString("RecipientLogin__");
        Process.AddRight(userID, "read");
        SendNotification(userID);
    }
}
function GetFormattedDateFromField(fieldName, user) {
    var options = {
        dateFormat: "ShortDate",
        timeFormat: "None",
        timeZone: "User"
    };
    var date = Data.GetValue(fieldName);
    return user && date ? user.GetFormattedDate(date, options) : null;
}
function GetTrackingLink() {
    var trackingNumber = Data.GetValue("TrackingNumber__");
    var carrier = Data.GetValue("CarrierCombo__");
    if (trackingNumber && carrier && carrier !== "Other") {
        return Lib.Shipping.GetLink(carrier, trackingNumber);
    }
    return null;
}
function SendNotification(userID) {
    var user = Users.GetUserAsProcessAdmin(userID);
    var template = "Purchasing_Email_NotifReceiver_ASNSubmitted_XX.htm";
    var currentUser = Users.GetUser(Data.GetValue("OwnerId"));
    var fromName = currentUser.GetValue("DisplayName");
    var shippingDate = GetFormattedDateFromField("ShippingDate__", currentUser);
    var expectedDeliveryDate = GetFormattedDateFromField("ExpectedDeliveryDate__", currentUser);
    var carrier = Data.GetValue("Carrier__");
    var trackingNumber = Data.GetValue("TrackingNumber__");
    var trackingLink = GetTrackingLink();
    var customTags = {
        RecipientDisplayName: user.GetValue("DisplayName"),
        shippingDate: shippingDate,
        expectedDeliveryDate: expectedDeliveryDate,
        carrier: carrier,
        trackingNumber: trackingNumber,
        trackingLink: trackingLink,
        isShippingDateDefined: !Sys.Helpers.IsEmpty(shippingDate),
        isExpectedDeliveryDateDefined: !Sys.Helpers.IsEmpty(expectedDeliveryDate),
        isCarrierDefined: !Sys.Helpers.IsEmpty(carrier),
        isTrackingNumberDefined: !Sys.Helpers.IsEmpty(trackingNumber),
        isTrackingLinkDefined: !Sys.Helpers.IsEmpty(trackingLink)
    };
    var options = {
        backupUserAsCC: true,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    };
    Sys.EmailNotification.SendEmailNotificationWithUser(user, null, template, customTags, fromName, null, options);
}
if (currentAction === "approve_asynchronous" || currentAction === "approve" || currentAction === "ResumeWithAction") {
    switch (currentName) {
        case "Submit":
            SubmitASN();
            break;
        default:
            Process.PreventApproval();
            break;
    }
}
