///#GLOBALS Lib
Log.Info("Filling ASN Form with items from PO");
var orderNumber = Variable.GetValueAsString("OrderNumber__");
if (orderNumber) {
    Data.SetValue("OrderNumber__", orderNumber);
    var queryParams = {
        orderNumber: orderNumber,
        additionnalFilters: [Sys.Helpers.LdapUtil.FilterEqual("ItemType__", Lib.P2P.ItemType.QUANTITY_BASED)]
    };
    Lib.Purchasing.POItems.FillItems(queryParams, Lib.Purchasing.Items.POItemsToASN).then(function (dbItems) {
        var dbItem = dbItems[0];
        if (dbItem) {
            var firstLineAddress = dbItem.GetValue("ShipToCompany__") + "\n" + dbItem.GetValue("ShipToAddress__");
            Data.SetValue("ShipToAddress__", firstLineAddress);
            Variable.SetValueAsString("RecipientLogin__", dbItem.GetValue("RecipientLogin__"));
        }
    });
}
