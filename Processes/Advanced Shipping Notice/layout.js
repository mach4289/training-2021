{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1700,
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"stamp": 4,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1700
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 48
										},
										"hidden": true,
										"hideSeparator": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"options": {
												"version": 0
											},
											"*": {
												"DocumentsPanel": {
													"type": "PanelDocument",
													"options": {
														"version": 0,
														"border": {
															"collapsed": true
														},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"label": "_Documents",
														"hidden": true
													},
													"*": {
														"Document": {
															"data": [
																"documentscontroldata"
															],
															"type": "Documents",
															"options": {
																"version": 0
															},
															"stamp": 10
														}
													},
													"stamp": 9,
													"data": []
												}
											},
											"stamp": 8,
											"data": []
										},
										"form-content-top-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 31,
											"*": {
												"SystemData": {
													"type": "PanelSystemData",
													"options": {
														"multicolumn": false,
														"version": 0,
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"label": "_System data",
														"hidden": true
													},
													"stamp": 18,
													"data": []
												}
											}
										}
									},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "48%",
											"left": "0%",
											"width": null,
											"height": "52%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 0,
													"width": 43,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"label": "_Next Process"
															},
															"stamp": 20,
															"data": []
														}
													},
													"stamp": 19,
													"data": []
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 297,
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"label": "_Document Preview",
																"hidden": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 24
																}
															},
															"stamp": 23,
															"data": []
														}
													}
												}
											},
											"stamp": 12,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "43%",
													"width": "57%",
													"height": null
												},
												"hidden": false,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 262,
													"*": {
														"Title": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "_Title",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 127,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 128,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 129,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner",
																						"htmlContent": "Advanced Shipping Notice",
																						"css": "@ span {line-height: 44px;}@ ",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 152
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 268,
													"*": {
														"ActionsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Actions",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 114,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 115,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"SubmitApprove__": {
																						"line": 1,
																						"column": 1
																					},
																					"Save__": {
																						"line": 2,
																						"column": 1
																					},
																					"HTML__": {
																						"line": 3,
																						"column": 1
																					},
																					"Quit__": {
																						"line": 4,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 117,
																			"*": {
																				"SubmitApprove__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_SubmitApprove",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "Advance Ship Notice",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-approve-check-circle fa-4x",
																						"style": 5,
																						"width": "90",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 121
																				},
																				"Save__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Save",
																						"label": "_Button",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "Advance Ship Notice",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-save-circle fa-4x",
																						"style": 5,
																						"width": "90",
																						"action": "none",
																						"url": "",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 123
																				},
																				"HTML__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_HTML",
																						"htmlContent": "<div style=\"width: 80px\"></div>",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 125
																				},
																				"Quit__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "_Quit",
																						"label": "_Quit",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color1",
																						"iconColor": "color1",
																						"nextprocess": {
																							"processName": "Advanced Shipping Notice",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImageOverlay": "",
																						"awesomeClasses": "esk-ifont-quit-circle fa-4x",
																						"style": 5,
																						"width": "90",
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 249
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 280,
													"*": {
														"HeaderPanel": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Header",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": false,
																"labelLength": 0
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"ASNNumber__": "LabelASNNumber__",
																			"LabelASNNumber__": "ASNNumber__",
																			"ShippingDate__": "LabelShippingDate__",
																			"LabelShippingDate__": "ShippingDate__",
																			"ExpectedDeliveryDate__": "LabelExpectedDeliveryDate__",
																			"LabelExpectedDeliveryDate__": "ExpectedDeliveryDate__",
																			"Status__": "LabelStatus__",
																			"LabelStatus__": "Status__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__",
																			"OrderNumber__": "LabelOrderNumber__",
																			"LabelOrderNumber__": "OrderNumber__",
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 7,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"ASNNumber__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelASNNumber__": {
																						"line": 1,
																						"column": 1
																					},
																					"ShippingDate__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelShippingDate__": {
																						"line": 2,
																						"column": 1
																					},
																					"ExpectedDeliveryDate__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelExpectedDeliveryDate__": {
																						"line": 3,
																						"column": 1
																					},
																					"Status__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 4,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 5,
																						"column": 1
																					},
																					"OrderNumber__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelOrderNumber__": {
																						"line": 6,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 7,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelASNNumber__": {
																					"type": "Label",
																					"data": [
																						"ASNNumber__"
																					],
																					"options": {
																						"label": "_ASNNumber",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"ASNNumber__": {
																					"type": "ShortText",
																					"data": [
																						"ASNNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_ASNNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 37
																				},
																				"LabelShippingDate__": {
																					"type": "Label",
																					"data": [
																						"ShippingDate__"
																					],
																					"options": {
																						"label": "_ShippingDate",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"ShippingDate__": {
																					"type": "DateTime",
																					"data": [
																						"ShippingDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ShippingDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"LabelExpectedDeliveryDate__": {
																					"type": "Label",
																					"data": [
																						"ExpectedDeliveryDate__"
																					],
																					"options": {
																						"label": "_ExpectedDeliveryDate",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"ExpectedDeliveryDate__": {
																					"type": "DateTime",
																					"data": [
																						"ExpectedDeliveryDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ExpectedDeliveryDate",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 41
																				},
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"label": "_Status",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 250
																				},
																				"Status__": {
																					"type": "ComboBox",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_Submitted"
																						},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "Submitted"
																						},
																						"label": "_Status",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 251
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 252
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"length": 5000,
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 253
																				},
																				"LabelOrderNumber__": {
																					"type": "Label",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"label": "_OrderNumber",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 255
																				},
																				"OrderNumber__": {
																					"type": "ShortText",
																					"data": [
																						"OrderNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_OrderNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"hidden": true,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 256
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_VendorName"
																					},
																					"stamp": 309
																				},
																				"VendorName__": {
																					"type": "ShortText",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": true,
																						"browsable": false,
																						"hidden": true
																					},
																					"stamp": 310
																				}
																			},
																			"stamp": 16
																		}
																	},
																	"stamp": 15,
																	"data": []
																}
															},
															"stamp": 14,
															"data": []
														},
														"ShippingInfoPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ShippingInfo",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 48,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Carrier__": "LabelCarrier__",
																			"LabelCarrier__": "Carrier__",
																			"TrackingNumber__": "LabelTrackingNumber__",
																			"LabelTrackingNumber__": "TrackingNumber__",
																			"ShippingNote__": "LabelShippingNote__",
																			"LabelShippingNote__": "ShippingNote__",
																			"LabelShipToAddress__": "ShipToAddress__",
																			"ShipToAddress__": "LabelShipToAddress__",
																			"CarrierCombo__": "LabelCarrierCombo__",
																			"LabelCarrierCombo__": "CarrierCombo__",
																			"TrackingLink__": "LabelTrackingLink__",
																			"LabelTrackingLink__": "TrackingLink__"
																		},
																		"version": 0
																	},
																	"stamp": 49,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Carrier__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelCarrier__": {
																						"line": 4,
																						"column": 1
																					},
																					"TrackingNumber__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTrackingNumber__": {
																						"line": 5,
																						"column": 1
																					},
																					"ShippingNote__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelShippingNote__": {
																						"line": 7,
																						"column": 1
																					},
																					"LabelShipToAddress__": {
																						"line": 1,
																						"column": 1
																					},
																					"ShipToAddress__": {
																						"line": 1,
																						"column": 2
																					},
																					"Spacer2__": {
																						"line": 2,
																						"column": 1
																					},
																					"CarrierCombo__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelCarrierCombo__": {
																						"line": 3,
																						"column": 1
																					},
																					"TrackingLink__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelTrackingLink__": {
																						"line": 6,
																						"column": 1
																					}
																				},
																				"lines": 7,
																				"columns": 2,
																				"colspans": [
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 50,
																			"*": {
																				"LabelShipToAddress__": {
																					"type": "Label",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"label": "_ShipToAddress",
																						"version": 0
																					},
																					"stamp": 169
																				},
																				"ShipToAddress__": {
																					"type": "LongText",
																					"data": [
																						"ShipToAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ShipToAddress",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"readonly": true,
																						"numberOfLines": 5
																					},
																					"stamp": 170
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 219
																				},
																				"LabelCarrierCombo__": {
																					"type": "Label",
																					"data": [
																						"CarrierCombo__"
																					],
																					"options": {
																						"label": "_CarrierCombo"
																					},
																					"stamp": 303
																				},
																				"CarrierCombo__": {
																					"type": "ComboBox",
																					"data": [
																						"CarrierCombo__"
																					],
																					"options": {
																						"possibleValues": {},
																						"maxNbLinesToDisplay": 10,
																						"version": 1,
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_CarrierCombo",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"hidden": true
																					},
																					"stamp": 304
																				},
																				"LabelCarrier__": {
																					"type": "Label",
																					"data": [
																						"Carrier__"
																					],
																					"options": {
																						"label": "_Carrier",
																						"version": 0
																					},
																					"stamp": 82
																				},
																				"Carrier__": {
																					"type": "ShortText",
																					"data": [
																						"Carrier__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_Carrier",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 83
																				},
																				"LabelTrackingNumber__": {
																					"type": "Label",
																					"data": [
																						"TrackingNumber__"
																					],
																					"options": {
																						"label": "_TrackingNumber",
																						"version": 0
																					},
																					"stamp": 84
																				},
																				"TrackingNumber__": {
																					"type": "ShortText",
																					"data": [
																						"TrackingNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TrackingNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 85
																				},
																				"LabelTrackingLink__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_TrackingLink",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 305
																				},
																				"TrackingLink__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"openInCurrentWindow": false,
																						"label": "_TrackingLink",
																						"hidden": true
																					},
																					"stamp": 306
																				},
																				"LabelShippingNote__": {
																					"type": "Label",
																					"data": [
																						"ShippingNote__"
																					],
																					"options": {
																						"label": "_ShippingNote",
																						"version": 0
																					},
																					"stamp": 86
																				},
																				"ShippingNote__": {
																					"type": "LongText",
																					"data": [
																						"ShippingNote__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_ShippingNote",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 87
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-right-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 274,
													"*": {
														"ItemsPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"maximized": false
																},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Items",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 61,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 62,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LineItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 63,
																			"*": {
																				"LineItems__": {
																					"type": "Table",
																					"data": [
																						"LineItems__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 5,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_LineItems",
																						"subsection": null
																					},
																					"stamp": 72,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0,
																								"hidden": false
																							},
																							"stamp": 73,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 74
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 75
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 76,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 172,
																									"data": [],
																									"*": {
																										"ItemPONumber__": {
																											"type": "Label",
																											"data": [
																												"ItemPONumber__"
																											],
																											"options": {
																												"label": "_ItemPONumber",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 173,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "260",
																										"version": 0
																									},
																									"stamp": 166,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "Label",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"label": "_ItemDescription",
																												"minwidth": "260",
																												"version": 0
																											},
																											"stamp": 167,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 154,
																									"data": [],
																									"*": {
																										"ItemPOLineQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemPOLineQuantity__"
																											],
																											"options": {
																												"label": "_ItemPOLineQuantity",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 155,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 158,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Label",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"label": "_ItemQuantity",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 159,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": 140,
																										"version": 0
																									},
																									"stamp": 162,
																									"data": [],
																									"*": {
																										"ItemUOM__": {
																											"type": "Label",
																											"data": [
																												"ItemUOM__"
																											],
																											"options": {
																												"label": "_ItemUOM",
																												"minwidth": 140,
																												"version": 0
																											},
																											"stamp": 163,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 77,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"ItemPONumber__": {
																											"type": "ShortText",
																											"data": [
																												"ItemPONumber__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemPONumber",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 174,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 165,
																									"data": [],
																									"*": {
																										"ItemDescription__": {
																											"type": "LongText",
																											"data": [
																												"ItemDescription__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ItemDescription",
																												"activable": true,
																												"width": "260",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 168,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 153,
																									"data": [],
																									"*": {
																										"ItemPOLineQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemPOLineQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemPOLineQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false
																											},
																											"stamp": 156,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"ItemQuantity__": {
																											"type": "Decimal",
																											"data": [
																												"ItemQuantity__"
																											],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_ItemQuantity",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"enablePlusMinus": false
																											},
																											"stamp": 160,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"ItemUOM__": {
																											"type": "ShortText",
																											"data": [
																												"ItemUOM__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ItemUOM",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"browsable": false
																											},
																											"stamp": 164,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 21,
											"data": []
										}
									},
									"stamp": 11,
									"data": []
								}
							},
							"stamp": 6,
							"data": []
						}
					},
					"stamp": 5,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1700
					},
					"*": {},
					"stamp": 25,
					"data": []
				}
			},
			"stamp": 3,
			"data": []
		}
	},
	"stamps": 310,
	"data": []
}