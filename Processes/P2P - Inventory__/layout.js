{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"WarehouseID__": "LabelWarehouseID__",
																	"LabelWarehouseID__": "WarehouseID__",
																	"StocktakingDateTime__": "LabelStocktakingDateTime__",
																	"LabelStocktakingDateTime__": "StocktakingDateTime__",
																	"MinimumThreshold__": "LabelMinimumThreshold__",
																	"LabelMinimumThreshold__": "MinimumThreshold__",
																	"MinimumThresholdUOM__": "LabelMinimumThresholdUOM__",
																	"LabelMinimumThresholdUOM__": "MinimumThresholdUOM__",
																	"ItemNumber__": "LabelItemNumber__",
																	"LabelItemNumber__": "ItemNumber__",
																	"ExpectedStockLevel__": "LabelExpectedStockLevel__",
																	"LabelExpectedStockLevel__": "ExpectedStockLevel__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"CurrentStock__": "LabelCurrentStock__",
																	"LabelCurrentStock__": "CurrentStock__",
																	"AlertNotifRecipient__": "LabelAlertNotifRecipient__",
																	"LabelAlertNotifRecipient__": "AlertNotifRecipient__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 9,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"WarehouseID__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelWarehouseID__": {
																				"line": 3,
																				"column": 1
																			},
																			"StocktakingDateTime__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelStocktakingDateTime__": {
																				"line": 4,
																				"column": 1
																			},
																			"MinimumThreshold__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelMinimumThreshold__": {
																				"line": 5,
																				"column": 1
																			},
																			"MinimumThresholdUOM__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelMinimumThresholdUOM__": {
																				"line": 6,
																				"column": 1
																			},
																			"ItemNumber__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelItemNumber__": {
																				"line": 1,
																				"column": 1
																			},
																			"ExpectedStockLevel__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelExpectedStockLevel__": {
																				"line": 7,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 2,
																				"column": 1
																			},
																			"CurrentStock__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelCurrentStock__": {
																				"line": 8,
																				"column": 1
																			},
																			"AlertNotifRecipient__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelAlertNotifRecipient__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelItemNumber__": {
																			"type": "Label",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"label": "_ItemNumber",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"ItemNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"ItemNumber__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_ItemNumber",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 27
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "contains",
																				"searchMode": "contains",
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"RestrictSearch": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 31
																		},
																		"LabelWarehouseID__": {
																			"type": "Label",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"label": "_WarehouseID",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"WarehouseID__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"WarehouseID__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_WarehouseID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 19
																		},
																		"LabelStocktakingDateTime__": {
																			"type": "Label",
																			"data": [
																				"StocktakingDateTime__"
																			],
																			"options": {
																				"label": "_StocktakingDateTime",
																				"version": 0
																			},
																			"stamp": 20
																		},
																		"StocktakingDateTime__": {
																			"type": "RealDateTime",
																			"data": [
																				"StocktakingDateTime__"
																			],
																			"options": {
																				"readonly": true,
																				"displayLongFormat": false,
																				"label": "_StocktakingDateTime",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"version": 0
																			},
																			"stamp": 21
																		},
																		"LabelMinimumThreshold__": {
																			"type": "Label",
																			"data": [
																				"MinimumThreshold__"
																			],
																			"options": {
																				"label": "_MinimumThreshold",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"MinimumThreshold__": {
																			"type": "Decimal",
																			"data": [
																				"MinimumThreshold__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_MinimumThreshold",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 23
																		},
																		"LabelMinimumThresholdUOM__": {
																			"type": "Label",
																			"data": [
																				"DatabaseComboBox__"
																			],
																			"options": {
																				"label": "_MinimumThresholdUOM",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"MinimumThresholdUOM__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"MinimumThresholdUOM__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_MinimumThresholdUOM",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 25
																		},
																		"LabelExpectedStockLevel__": {
																			"type": "Label",
																			"data": [
																				"ExpectedStockLevel__"
																			],
																			"options": {
																				"label": "_ExpectedStockLevel",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"ExpectedStockLevel__": {
																			"type": "Decimal",
																			"data": [
																				"ExpectedStockLevel__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ExpectedStockLevel",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 29
																		},
																		"LabelCurrentStock__": {
																			"type": "Label",
																			"data": [
																				"CurrentStock__"
																			],
																			"options": {
																				"label": "_CurrentStock"
																			},
																			"stamp": 34
																		},
																		"CurrentStock__": {
																			"type": "Decimal",
																			"data": [
																				"CurrentStock__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_CurrentStock",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"enablePlusMinus": false
																			},
																			"stamp": 35
																		},
																		"LabelAlertNotifRecipient__": {
																			"type": "Label",
																			"data": [
																				"AlertNotifRecipient__"
																			],
																			"options": {
																				"label": "_AlertNotifRecipient"
																			},
																			"stamp": 40
																		},
																		"AlertNotifRecipient__": {
																			"type": "ShortText",
																			"data": [
																				"AlertNotifRecipient__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_AlertNotifRecipient",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 41
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 41,
	"data": []
}