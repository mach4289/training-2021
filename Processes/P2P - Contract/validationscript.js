///#GLOBALS Lib Sys
var Validation;
(function (Validation)
{
	Validation.workflow = Sys.WorkflowController.Create(Data, Variable, Language);
	var g_newComment = Data.GetValue("Comments__");
	var g_sequenceStep = Validation.workflow.GetContributorIndex();
	var g_currentContributor = Validation.workflow.GetContributorAt(g_sequenceStep);

	function Save()
	{
		// The standard Save action does not save the EDDMessageVars, in our case ValidityDateTime
		// --> use Approve action + PreventApproval
		Process.PreventApproval();
		Process.LeaveForm();
	}

	function Main()
	{
		var currentName = Data.GetActionName();
		var currentAction = Data.GetActionType();
		Log.Info("-- Contract Validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice() + "'");
		Validation.workflow.AllowRebuild(false);
		Validation.workflow.Define(parameters);
		if (currentName != "" && currentAction != "")
		{
			// Back to requester
			if (currentName == "Save")
			{
				Save();
			}
			else if (currentName == "BackToRequester" && currentAction == "approve")
			{
				Validation.workflow.DoAction(parameters.actions.sentBack.GetName());
			}
			else if (currentName === "DeleteContract" && currentAction === "approve_asynchronous")
			{
				Validation.workflow.DoAction(parameters.actions.deleted.GetName());
			}
			else if (currentName === "UpdateContract")
			{
				if (Sys.Helpers.Date.CompareDateToToday(Data.GetValue("EndDate__")) < 0)
				{
					if (Data.GetValue("TacitRenewal__") && !Data.GetValue("InitiallyExpectedEndDate__"))
					{
						RenewContract();
					}
					else
					{
						// Terminate the contract
						EndContract();
					}
				}
				else
				{
					Log.Info("Nothing to do : EndDate__ not reached");
				}
			}
			else if (currentName === "ModifyContract")
			{
				if (g_sequenceStep > 0)
				{
					Validation.workflow.DoAction(parameters.actions.modifyContract.GetName());
				}
				else
				{
					Data.SetError("ApproversList__", "_Failed to forward back to requester");
				}
			}
			else if (currentName === "Share" && currentAction === "approve_asynchronous")
			{
				var advisorLogin = Variable.GetValueAsString("AdvisorLogin");
				if (advisorLogin)
				{
					Validation.workflow.DoAction(parameters.actions.share.GetName());
				}
			}
			else if (currentName === "Comment_Answer" && currentAction === "approve_asynchronous")
			{
				Validation.workflow.DoAction(parameters.actions.comment.GetName());
			}
			// Add document
			else if (currentAction == "reprocess")
			{}
			else if (!Data.FormHasError())
			{
				var idx = Validation.workflow.GetContributorIndex();
				var contributor = Validation.workflow.GetContributorAt(idx);
				Validation.workflow.DoAction(contributor.action);
				var additionalContributorsCache = {};
				Variable.SetValueAsString("AdditionalContributors", JSON.stringify(additionalContributorsCache));
			}
			else
			{
				Process.PreventApproval();
			}
		}
	}
	Validation.Main = Main;
	var validationDate = null;

	function GiveRightToApprovers()
	{
		Log.Info("Reset rights");
		Process.ResetRights();
		var lastSaveOwnerID = Data.GetValue("LastSavedOwnerID");
		Log.Info("Grant read right to LastSavedOwnerID: " + lastSaveOwnerID);
		Process.AddRight(lastSaveOwnerID, "read");
		// always give the "read" right to the initiator
		if (Validation.workflow.GetNbContributors() > 0)
		{
			Log.Info("Grant read right to initiator: " + Validation.workflow.GetContributorAt(0).login);
			Process.AddRight(Validation.workflow.GetContributorAt(0).login, "write");
		}
		// Skips initiator : starts at 1
		for (var i = 1; i < Validation.workflow.GetNbContributors(); i++)
		{
			var step = Validation.workflow.GetContributorAt(i);
			Log.Info("Grant read right to approver: " + step.login);
			Process.AddRight(step.login, "read");
		}
	}

	function AddSharedWith(comment)
	{
		var prefix = Language.Translate("_Shared with", false, Variable.GetValueAsString("AdvisorName"));
		if (comment)
		{
			return prefix + ": " + comment;
		}
		return prefix;
	}

	function ResetAdvisorList()
	{
		var list = Variable.GetValueAsString("AdvisorLoginList");
		if (list)
		{
			var logins = list.split("\n");
			for (var i = 0; i < logins.length; i++)
			{
				Log.Info("Grant read right to advisor: " + logins[i]);
				Process.SetRight(logins[i], "read");
			}
		}
		Variable.SetValueAsString("AdvisorLoginList", "");
	}

	function GetContributionData(sequenceStep, newAction, commentOptions)
	{
		var currentContributor = Validation.workflow.GetContributorAt(sequenceStep);
		currentContributor.action = newAction;
		if (validationDate === null)
		{
			currentContributor.date = new Date();
			validationDate = currentContributor.date;
		}
		else
		{
			currentContributor.date = validationDate;
		}
		currentContributor.actualApprover = Sys.Helpers.String.ExtractLoginFromDN(Lib.P2P.GetValidatorOrOwnerLogin());
		var wkfComment = Data.GetValue("Comments__");
		if (commentOptions)
		{
			if (commentOptions.comment)
			{
				wkfComment = commentOptions.comment;
			}
			if (commentOptions.sharedWith)
			{
				wkfComment = AddSharedWith(wkfComment);
			}
			if (commentOptions.onBehalfOf)
			{
				wkfComment = Lib.P2P.AddOnBehalfOf(currentContributor, wkfComment);
			}
		}
		if (wkfComment)
		{
			var previousComment = currentContributor.comment;
			var newCommentFormatted = wkfComment;
			if (previousComment && previousComment.length > 0)
			{
				newCommentFormatted += "\n" + previousComment;
			}
			Data.SetValue("Comments__", "");
			currentContributor.comment = newCommentFormatted;
		}
		return currentContributor;
	}

	function ChangeOwner(idx)
	{
		if (idx == null)
		{
			idx = Validation.workflow.GetContributorIndex();
		}
		Log.Info("change owner idx :" + idx);
		var step = Validation.workflow.GetContributorAt(idx);
		Process.ChangeOwner(step.login);
		if (idx == 1)
		{
			GiveRightToApprovers();
		}
	}

	function NextContributorExist()
	{
		var nextContributor = Validation.workflow.GetContributorAt(Validation.workflow.GetContributorIndex() + 1);
		if (nextContributor && Users.GetUser(nextContributor.login) == null)
		{
			Lib.CommonDialog.NextAlert.Define("_Workflow error", "_Invalid user '{0}' in worflow, please contact your administrator", null, nextContributor.login);
			return false;
		}
		else if (!nextContributor)
		{
			return false;
		}
		return true;
	}

	function GoToNextStep(contributionData)
	{
		Validation.workflow.NextContributor(contributionData);
		var nextContributor = Validation.workflow.GetContributorAt(Validation.workflow.GetContributorIndex());
		var nextContributors = Validation.workflow.GetParallelContributorsOf(nextContributor, true);
		nextContributors.forEach(function (contributor)
		{
			Log.Info("Grant validate right to approver: " + contributor.login);
			Process.AddRight(contributor.login, "validate");
			if (!contributor.additionalProperties || !contributor.additionalProperties.startingDate)
			{
				Validation.workflow.UpdateAdditionalContributorData(contributor.contributorId,
				{
					startingDate: new Date(contributionData.date)
				});
			}
		});
		ChangeOwner();
	}

	function SendEmailNotification(step, subject, template, backupUserAsCC, tags)
	{
		//if you change this structure, plz update the sample in lib_PR_Customization_Server
		var options = {
			userId: step.login,
			subject: subject,
			template: template,
			customTags: tags,
			fromName: "Esker Contract",
			backupUserAsCC: !!backupUserAsCC,
			sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
		};
		Sys.EmailNotification.SendEmailNotification(options);
	}

	function GetCustomTags()
	{
		return {
			Name__: Data.GetValue("Name__"),
			ReferenceNumber__: Data.GetValue("ReferenceNumber__"),
			OwnerNiceName__: Validation.workflow.GetContributorAt(0).name,
			Last_Comments__: g_newComment
		};
	}

	function InitConfiguration(companyCode)
	{
		return Lib.P2P.CompanyCodesValue.QueryValues(companyCode).Then(function (CCValues)
		{
			return Sys.Helpers.Promise.Create(function (resolve, reject)
			{
				if (Object.keys(CCValues).length > 0)
				{
					Sys.Parameters.GetInstance("PAC").Reload(CCValues.DefaultConfiguration__);
				}
				else
				{
					Log.Error("The requested company code is not in the company code table.");
				}
				Sys.Parameters.GetInstance("PAC").IsReady(function ()
				{
					resolve();
				});
			});
		});
	}

	function Approved(sequenceStep, comment)
	{
		Data.SetValue("ContractStatus__", "Active");
		Log.Info(" Contract workflow -- Action: approved -- SeqenceStep: " + sequenceStep);
		var contributionData = GetContributionData(sequenceStep, parameters.actions.approved.GetName(),
		{
			onBehalfOf: true,
			comment: comment
		});
		Validation.workflow.EndWorkflow(contributionData);
		ChangeOwner(0);
		var currentUser = Lib.P2P.GetValidatorOrOwner();
		var currentUserLogin = currentUser.GetValue("Login");
		var owner = Validation.workflow.GetContributorAt(0);
		if (currentUserLogin !== owner.login)
		{
			var tags = GetCustomTags();
			tags.Description__ = Data.GetValue("Description__");
			tags.LastApprover__ = currentUser.GetValue("DisplayName");
			SendEmailNotification(owner, "_A contract has been activated", "Contract_Email_Validate.htm", false, tags);
		}
		// Set read right to
		InitConfiguration(Data.GetValue("CompanyCode__")).Then(function ()
		{
			var contractViewer = Sys.Parameters.GetInstance("PAC").GetParameter("ContractViewer");
			if (contractViewer)
			{
				Process.SetRight(contractViewer, "read");
			}
		});
		ResetAdvisorList();
		return true;
	}

	function RenewContract()
	{
		var endDate = Data.GetValue("EndDate__");
		var newStartDate = endDate;
		newStartDate.setDate(endDate.getDate() + 1);
		Data.SetValue("StartDate__", newStartDate);
		var renewalTerm = Data.GetValue("RenewalTerms__");
		Lib.Contract.ComputeEndDate(renewalTerm);
		var newEndDate = Data.GetValue("EndDate__");
		var priorNotice = Data.GetValue("PriorNotice__");
		var resiliationDate = newEndDate;
		resiliationDate.setDate(newEndDate.getDate() - priorNotice);
		Data.SetValue("ResiliationDate__", resiliationDate);
		var timeOfNotification = Data.GetValue("RenewalReminderDays__") || 0;
		resiliationDate.setDate(resiliationDate.getDate() - timeOfNotification);
		Data.SetValue("NotificationDate__", resiliationDate);
		Log.Info("Contract renewed with new StartDate__ = " + newStartDate + ", new EndDate__ = " + newEndDate);
	}

	function EndContract()
	{
		var isManuallyTerminated = !Sys.Helpers.IsEmpty(Data.GetValue("InitiallyExpectedEndDate__"));
		if (isManuallyTerminated)
		{
			Data.SetValue("ContractStatus__", "Revoked");
		}
		else
		{
			Data.SetValue("ContractStatus__", "Expired");
		}
		Log.Info("EndDate__ (" + Data.GetValue("EndDate__") + ") reached, contract ended with Status = " + Data.GetValue("ContractStatus__"));
	}

	function autoApproveIfNextIsSameUser(sequenceStep, nextContributor, contributionData)
	{
		var currentUser = Lib.P2P.GetValidatorOrOwner();
		var currentUserLogin = currentUser.GetValue("Login");
		var forwarded = false;
		while (nextContributor && currentUserLogin === nextContributor.login)
		{
			forwarded = true;
			Log.Info("Auto approve step " + sequenceStep + " for " + nextContributor.login);
			Validation.workflow.NextContributor(contributionData);
			ChangeOwner(); // Need to change owner in order to be coherent with the current contributor
			sequenceStep = Validation.workflow.GetContributorIndex();
			var actionName = null;
			var autoComment = null;
			switch (nextContributor.action)
			{
			case parameters.actions.approval.GetName():
			default:
				actionName = parameters.actions.approved.GetName();
				autoComment = Language.Translate("_Contract auto approved");
				break;
			}
			contributionData = GetContributionData(sequenceStep, actionName,
			{
				comment: autoComment,
				onBehalfOf: true
			});
			nextContributor = Validation.workflow.GetContributorAt(sequenceStep + 1);
		}
		return {
			forwarded: forwarded,
			sequenceStep: sequenceStep,
			nextContributor: nextContributor,
			contributionData: contributionData
		};
	}
	//#region workflow parameters
	var parameters = {
		actions:
		{
			submission:
			{
				OnDone: function (sequenceStep)
				{
					Data.SetValue("ContractStatus__", "ToValidate");
					Log.Info(" Contract workflow -- Action: submission -- SeqenceStep: " + sequenceStep);
					var nextContributor = null,
						contributionData = null;
					var lastStepComment = null;
					var isLastStep = false;
					var bok = NextContributorExist();
					if (bok)
					{
						contributionData = GetContributionData(sequenceStep, this.actions.submitted.GetName(),
						{
							onBehalfOf: true
						});
						nextContributor = Validation.workflow.GetContributorAt(sequenceStep + 1);
						ResetAdvisorList();
						var autoApproveReturn = autoApproveIfNextIsSameUser(sequenceStep, nextContributor, contributionData);
						sequenceStep = autoApproveReturn.sequenceStep;
						nextContributor = autoApproveReturn.nextContributor;
						contributionData = autoApproveReturn.contributionData;
						var currentContributor = Validation.workflow.GetContributorAt(sequenceStep);
						var tags = GetCustomTags();
						SendEmailNotification(currentContributor, "_A contract has been created", "Contract_Email_FirstNotif.htm", false, tags);
						if (!nextContributor)
						{
							// Trigger the end of the workflow
							isLastStep = true;
							lastStepComment = Language.Translate("_Contract auto approved");
						}
					}
					else
					{
						isLastStep = true;
					}
					if (isLastStep)
					{
						bok = Approved(sequenceStep, lastStepComment);
					}
					else
					{
						var tags = GetCustomTags();
						SendEmailNotification(nextContributor, "_A contract is waiting for your action", "Contract_Email_NotifNextApprover.htm", true, tags);
						GoToNextStep(contributionData);
						Process.PreventApproval();
					}
					Process.LeaveForm();
					return bok;
				}
			},
			submitted:
			{},
			approval:
			{
				OnDone: function (sequenceStep)
				{
					Log.Info(" Contract workflow -- Action: approval -- SeqenceStep: " + sequenceStep);
					var nextContributor = null,
						contributionData = null;
					var lastStepComment = null;
					var bok = NextContributorExist();
					var isLastStep = false;
					if (bok)
					{
						var currentContributor = Validation.workflow.GetContributorAt(sequenceStep);
						nextContributor = Validation.workflow.GetContributorAt(sequenceStep + 1);
						contributionData = GetContributionData(sequenceStep, this.actions.approved.GetName(),
						{
							onBehalfOf: true
						});
						var autoApproveReturn = autoApproveIfNextIsSameUser(sequenceStep, nextContributor, contributionData);
						sequenceStep = autoApproveReturn.sequenceStep;
						nextContributor = autoApproveReturn.nextContributor;
						contributionData = autoApproveReturn.contributionData;
						if (!nextContributor)
						{
							isLastStep = true;
							lastStepComment = Language.Translate("_Contract auto approved");
						}
						else if (currentContributor.role !== nextContributor.role)
						{
							ResetAdvisorList();
						}
					}
					else
					{
						isLastStep = true;
					}
					if (isLastStep)
					{
						// End the workflow
						bok = Approved(sequenceStep, lastStepComment);
					}
					else
					{
						var tags = GetCustomTags();
						SendEmailNotification(nextContributor, "_A contract is waiting for your action", "Contract_Email_NotifNextApprover.htm", true, tags);
						GoToNextStep(contributionData);
						Process.PreventApproval();
					}
					Process.LeaveForm();
					return bok;
				}
			},
			share:
			{
				OnDone: function (sequenceStep)
				{
					var advisor = {
						login: Variable.GetValueAsString("AdvisorLogin"),
						name: Variable.GetValueAsString("AdvisorName"),
						email: Variable.GetValueAsString("AdvisorEmail")
					};
					// add the current advisor in advisor list
					var list = Variable.GetValueAsString("AdvisorLoginList");
					list = list ? advisor.login + "\n" + list : advisor.login;
					Variable.SetValueAsString("AdvisorLoginList", list);
					var contributionData = GetContributionData(sequenceStep, this.actions.share.GetName(),
					{
						comment: g_newComment,
						sharedWith: true,
						onBehalfOf: true
					});
					var tags = GetCustomTags();
					tags.LastValidatorName__ = Validation.workflow.GetContributorAt(sequenceStep).name;
					SendEmailNotification(advisor, "_A contract has been shared", "Contract_Email_NotifShare.htm", true, tags);
					Log.Info("Grand all right to advisor: " + advisor.login);
					Process.SetRight(advisor.login, "all"); // Advisor can modify the comment
					Validation.workflow.AddContributorAt(sequenceStep + 1, g_currentContributor);
					Validation.workflow.NextContributor(contributionData);
					Process.DisableChecks();
					Process.PreventApproval();
					Process.LeaveForm();
					return true;
				}
			},
			comment:
			{
				OnDone: function (sequenceStep)
				{
					var requesterOfInformations = Validation.workflow.GetContributorAt(sequenceStep);
					// Add the step of advisor here. Previously done in the custom script.
					// We do it here now in order to reduce the crash window between the both calls to workfow (AddContributorAt and NextContributor).
					var advisor = {
						login: Variable.GetValueAsString("AdvisorLogin"),
						name: Variable.GetValueAsString("AdvisorName"),
						email: Variable.GetValueAsString("AdvisorEmail")
					};
					Validation.workflow.AddContributorAt(sequenceStep,
					{
						//mandatory fields
						contributorId: advisor.login + "_Role advisor",
						role: "_Role advisor",
						//not mandatory fields
						login: advisor.login,
						name: advisor.name,
						email: advisor.email,
						action: this.actions.comment.GetName()
					});
					var contributionData = GetContributionData(sequenceStep, this.actions.commentDone.GetName(), g_newComment);
					var tags = GetCustomTags();
					tags.LastValidatorName__ = advisor.name;
					SendEmailNotification(requesterOfInformations, "_Your shared contract has been answered", "Contract_Email_NotifShareAnswer.htm", true, tags);
					Validation.workflow.NextContributor(contributionData);
					Process.DisableChecks();
					Process.PreventApproval();
					Process.LeaveForm();
				}
			},
			commentDone:
			{},
			approved:
			{},
			modifyContract:
			{
				OnDone: function (sequenceStep)
				{
					Log.Info(" Contract workflow -- Action: modifyContract -- SeqenceStep: " + sequenceStep);
					var contributionData = GetContributionData(0, this.actions.modifyContract.GetName());
					var currentContributor = Validation.workflow.GetContributorAt(sequenceStep);
					contributionData.comment = Language.Translate("_Has modified Contract", false) + "\n" + g_newComment;
					Data.SetValue("Comments__", "");
					ResetAdvisorList();
					var tags = GetCustomTags();
					tags.LastValidatorName__ = Validation.workflow.GetContributorAt(sequenceStep).name;
					Validation.workflow.Restart(contributionData);
					Data.SetValue("ContractStatus__", "Draft");
					var nextContributor = Validation.workflow.GetContributorAt(0);
					// mail to send went recall from requester
					SendEmailNotification(currentContributor, "_A contract has been recalled", "Contract_Email_NotifRecall.htm", false, tags);
					Process.Forward(nextContributor.login);
				}
			},
			sentBack:
			{
				OnDone: function (sequenceStep)
				{
					Log.Info(" Contract workflow -- Action: sentBack -- SeqenceStep: " + sequenceStep);
					var lastValidatorName = Validation.workflow.GetContributorAt(sequenceStep).name;
					var contributionData = GetContributionData(sequenceStep, this.actions.sentBack.GetName(),
					{
						onBehalfOf: true
					});
					ResetAdvisorList();
					Validation.workflow.Restart(contributionData);
					Data.SetValue("ContractStatus__", "Draft");
					var nextContributor = Validation.workflow.GetContributorAt(0);
					var tags = GetCustomTags();
					tags.LastValidatorName__ = lastValidatorName;
					SendEmailNotification(nextContributor, "_A Contract must be verified", "Contract_Email_NotifBack.htm", false, tags);
					Process.Forward(nextContributor.login);
					Process.LeaveForm();
				}
			},
			deleted:
			{
				OnDone: function (sequenceStep)
				{
					Log.Info(" Contract workflow -- Action: deleted -- SeqenceStep: " + sequenceStep);
					var contributionData = GetContributionData(0, this.actions.deleted.GetName());
					var currentContributor = Validation.workflow.GetContributorAt(sequenceStep);
					// mail to send went recall from requester
					var tags = GetCustomTags();
					SendEmailNotification(currentContributor, "_A contract has been deleted", "Contract_Email_NotifDelete.htm", false, tags);
					ResetAdvisorList();
					Validation.workflow.EndWorkflow(contributionData);
					Data.SetValue("ContractStatus__", "Deleted");
					Data.SetValue("State", 300);
					Process.LeaveForm();
				}
			}
		}
	};
	Sys.Helpers.Extend(true, parameters, Lib.Contract.Workflow.Parameters);
	//#endregion
	Lib.P2P.SetTablesToIndex(["ApproversList__"]);
	Main();
})(Validation || (Validation = {}));