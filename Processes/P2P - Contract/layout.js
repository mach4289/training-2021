{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": "",
				"backColor": "text-backgroundcolor-color7",
				"style": "FlexibleFormLight"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"height": "25%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 0,
													"width": 55,
													"height": 100
												},
												"hidden": false,
												"stickypanel": "TopPaneWarning"
											},
											"*": {
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"TopPaneWarning": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "M",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color3",
																"labelsAlignment": "right",
																"label": "TopPaneWarning",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TopMessageWarning__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"TopMessageWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color8",
																						"backgroundcolor": "color3",
																						"iconClass": "",
																						"label": "_TopMessageWarning",
																						"version": 0
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Banner": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Banner",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HTMLBanner__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"HTMLBanner__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "HTMLBanner__",
																						"htmlContent": "Contrat",
																						"width": "100%",
																						"version": 0
																					},
																					"stamp": 14
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 15,
													"*": {
														"ContractDetails": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": "120px",
																"iconURL": "Contract_Details.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Contract Details",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "FlexibleFormPanelLight",
																"sameHeightAsSiblings": true
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"ReferenceNumber__": "LabelReferenceNumber__",
																			"LabelReferenceNumber__": "ReferenceNumber__",
																			"Name__": "LabelName__",
																			"LabelName__": "Name__",
																			"Description__": "LabelDescription__",
																			"LabelDescription__": "Description__",
																			"ContractStatus__": "LabelContractStatus__",
																			"LabelContractStatus__": "ContractStatus__",
																			"OwnerNiceName__": "LabelOwnerNiceName__",
																			"LabelOwnerNiceName__": "OwnerNiceName__",
																			"OwnerLogin__": "LabelOwnerLogin__",
																			"LabelOwnerLogin__": "OwnerLogin__",
																			"TechnicalData__": "LabelTechnicalData__",
																			"LabelTechnicalData__": "TechnicalData__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 9,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"CompanyCode__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 1,
																						"column": 1
																					},
																					"ReferenceNumber__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelReferenceNumber__": {
																						"line": 4,
																						"column": 1
																					},
																					"Name__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelName__": {
																						"line": 5,
																						"column": 1
																					},
																					"Description__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelDescription__": {
																						"line": 8,
																						"column": 1
																					},
																					"ContractStatus__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelContractStatus__": {
																						"line": 2,
																						"column": 1
																					},
																					"OwnerNiceName__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelOwnerNiceName__": {
																						"line": 7,
																						"column": 1
																					},
																					"OwnerLogin__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelOwnerLogin__": {
																						"line": 6,
																						"column": 1
																					},
																					"TechnicalData__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelTechnicalData__": {
																						"line": 9,
																						"column": 1
																					},
																					"Spacer5__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_Company code",
																						"version": 0
																					},
																					"stamp": 16
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Company code",
																						"activable": true,
																						"width": "230",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches"
																					},
																					"stamp": 17
																				},
																				"LabelContractStatus__": {
																					"type": "Label",
																					"data": [
																						"ContractStatus__"
																					],
																					"options": {
																						"label": "_Contract status",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 18
																				},
																				"ContractStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"ContractStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_ToValidate",
																							"2": "_Active",
																							"3": "_Revoked",
																							"4": "_Deleted",
																							"5": "_Terminated",
																							"6": "_Expired"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "ToValidate",
																							"2": "Active",
																							"3": "Revoked",
																							"4": "Deleted",
																							"5": "Terminated",
																							"6": "Expired"
																						},
																						"label": "_Contract status",
																						"activable": true,
																						"width": 230,
																						"hidden": true,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 19
																				},
																				"Spacer5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "5",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer5",
																						"version": 0
																					},
																					"stamp": 20
																				},
																				"LabelReferenceNumber__": {
																					"type": "Label",
																					"data": [
																						"ReferenceNumber__"
																					],
																					"options": {
																						"label": "_Reference number",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"ReferenceNumber__": {
																					"type": "ShortText",
																					"data": [
																						"ReferenceNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Reference number",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 22
																				},
																				"LabelName__": {
																					"type": "Label",
																					"data": [
																						"Name__"
																					],
																					"options": {
																						"label": "_ContractName",
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"Name__": {
																					"type": "ShortText",
																					"data": [
																						"Name__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ContractName",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 24
																				},
																				"LabelOwnerLogin__": {
																					"type": "Label",
																					"data": [
																						"OwnerLogin__"
																					],
																					"options": {
																						"label": "_OwnerLogin",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"OwnerLogin__": {
																					"type": "ShortText",
																					"data": [
																						"OwnerLogin__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_OwnerLogin",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 26
																				},
																				"LabelOwnerNiceName__": {
																					"type": "Label",
																					"data": [
																						"OwnerNiceName__"
																					],
																					"options": {
																						"label": "_OwnerNiceName",
																						"hidden": false,
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"OwnerNiceName__": {
																					"type": "ShortText",
																					"data": [
																						"OwnerNiceName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_OwnerNiceName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": false,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 28
																				},
																				"LabelDescription__": {
																					"type": "Label",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"label": "_Description",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"Description__": {
																					"type": "LongText",
																					"data": [
																						"Description__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Description",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"minNbLines": 3
																					},
																					"stamp": 30
																				},
																				"LabelTechnicalData__": {
																					"type": "Label",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"label": "_TechnicalData",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"TechnicalData__": {
																					"type": "ShortText",
																					"data": [
																						"TechnicalData__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TechnicalData",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"defaultValue": "{}",
																						"hidden": true,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 32
																				}
																			},
																			"stamp": 33
																		}
																	},
																	"stamp": 34,
																	"data": []
																}
															},
															"stamp": 35,
															"data": []
														},
														"VendorContact": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120px",
																"iconURL": "Contract_Vendor.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Vendor Contact",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "FlexibleFormPanelLight",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 36,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"VendorAddress__": "LabelVendorAddress__",
																			"LabelVendorAddress__": "VendorAddress__",
																			"VendorEmail__": "LabelVendorEmail__",
																			"LabelVendorEmail__": "VendorEmail__",
																			"VendorPhone__": "LabelVendorPhone__",
																			"LabelVendorPhone__": "VendorPhone__",
																			"VendorWebsite__": "LabelVendorWebsite__",
																			"LabelVendorWebsite__": "VendorWebsite__",
																			"VendorOpeningHours__": "LabelVendorOpeningHours__",
																			"LabelVendorOpeningHours__": "VendorOpeningHours__"
																		},
																		"version": 0
																	},
																	"stamp": 37,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"VendorName__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 2,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 3,
																						"column": 1
																					},
																					"VendorAddress__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelVendorAddress__": {
																						"line": 4,
																						"column": 1
																					},
																					"VendorEmail__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelVendorEmail__": {
																						"line": 5,
																						"column": 1
																					},
																					"VendorPhone__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelVendorPhone__": {
																						"line": 6,
																						"column": 1
																					},
																					"VendorWebsite__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelVendorWebsite__": {
																						"line": 7,
																						"column": 1
																					},
																					"VendorOpeningHours__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelVendorOpeningHours__": {
																						"line": 8,
																						"column": 1
																					},
																					"Spacer6__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 38,
																			"*": {
																				"Spacer6__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "5",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer6",
																						"version": 0
																					},
																					"stamp": 39
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_Vendor name",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"VendorName__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"version": 1,
																						"label": "_Vendor name",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"PreFillFTS": true,
																						"fTSmaxRecords": "20",
																						"browsable": true,
																						"autoCompleteSearchMode": "startswith",
																						"searchMode": "matches",
																						"helpIconPosition": "Right",
																						"minNbLines": 1,
																						"maxNbLines": 1
																					},
																					"stamp": 41
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_Vendor number",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"VendorNumber__": {
																					"type": "ShortText",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor number",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 43
																				},
																				"LabelVendorAddress__": {
																					"type": "Label",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"label": "_Vendor address",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"VendorAddress__": {
																					"type": "LongText",
																					"data": [
																						"VendorAddress__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor address",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"minNbLines": 3
																					},
																					"stamp": 45
																				},
																				"LabelVendorEmail__": {
																					"type": "Label",
																					"data": [
																						"VendorEmail__"
																					],
																					"options": {
																						"label": "_Vendor email",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"VendorEmail__": {
																					"type": "ShortText",
																					"data": [
																						"VendorEmail__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor email",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 47
																				},
																				"LabelVendorPhone__": {
																					"type": "Label",
																					"data": [
																						"VendorPhone__"
																					],
																					"options": {
																						"label": "_Vendor phone",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"VendorPhone__": {
																					"type": "ShortText",
																					"data": [
																						"VendorPhone__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor phone",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 49
																				},
																				"LabelVendorWebsite__": {
																					"type": "Label",
																					"data": [
																						"VendorWebsite__"
																					],
																					"options": {
																						"label": "_Vendor website",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"VendorWebsite__": {
																					"type": "ShortText",
																					"data": [
																						"VendorWebsite__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor website",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false
																					},
																					"stamp": 51
																				},
																				"LabelVendorOpeningHours__": {
																					"type": "Label",
																					"data": [
																						"VendorOpeningHours__"
																					],
																					"options": {
																						"label": "_Vendor opening hours",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"VendorOpeningHours__": {
																					"type": "LongText",
																					"data": [
																						"VendorOpeningHours__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Vendor opening hours",
																						"activable": true,
																						"width": 230,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"minNbLines": 3
																					},
																					"stamp": 53
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 54,
													"*": {
														"Validity": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120px",
																"iconURL": "Contract_Validity.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Validity",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "FlexibleFormPanelLight",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 55,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Duration__": "LabelDuration__",
																			"LabelDuration__": "Duration__",
																			"StartDate__": "LabelStartDate__",
																			"LabelStartDate__": "StartDate__",
																			"EndDate__": "LabelEndDate__",
																			"LabelEndDate__": "EndDate__",
																			"PriorNotice__": "LabelPriorNotice__",
																			"LabelPriorNotice__": "PriorNotice__",
																			"RenewalTerms__": "LabelRenewalTerms__",
																			"LabelRenewalTerms__": "RenewalTerms__",
																			"TacitRenewal__": "LabelTacitRenewal__",
																			"LabelTacitRenewal__": "TacitRenewal__",
																			"ResiliationDate__": "LabelResiliationDate__",
																			"LabelResiliationDate__": "ResiliationDate__",
																			"InitialStartDate__": "LabelInitialStartDate__",
																			"LabelInitialStartDate__": "InitialStartDate__",
																			"InitialEndDate__": "LabelInitialEndDate__",
																			"LabelInitialEndDate__": "InitialEndDate__"
																		},
																		"version": 0
																	},
																	"stamp": 56,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Duration__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelDuration__": {
																						"line": 8,
																						"column": 1
																					},
																					"StartDate__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelStartDate__": {
																						"line": 7,
																						"column": 1
																					},
																					"EndDate__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelEndDate__": {
																						"line": 9,
																						"column": 1
																					},
																					"PriorNotice__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelPriorNotice__": {
																						"line": 11,
																						"column": 1
																					},
																					"RenewalTerms__": {
																						"line": 13,
																						"column": 2
																					},
																					"LabelRenewalTerms__": {
																						"line": 13,
																						"column": 1
																					},
																					"TacitRenewal__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelTacitRenewal__": {
																						"line": 2,
																						"column": 1
																					},
																					"ResiliationDate__": {
																						"line": 12,
																						"column": 2
																					},
																					"LabelResiliationDate__": {
																						"line": 12,
																						"column": 1
																					},
																					"InitialStartDate__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInitialStartDate__": {
																						"line": 4,
																						"column": 1
																					},
																					"InitialEndDate__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelInitialEndDate__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer2__": {
																						"line": 6,
																						"column": 1
																					},
																					"Spacer3__": {
																						"line": 3,
																						"column": 1
																					},
																					"NextOccurrenceDesc__": {
																						"line": 15,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 14,
																						"column": 1
																					},
																					"Spacer4__": {
																						"line": 10,
																						"column": 1
																					},
																					"Spacer7__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 15,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 57,
																			"*": {
																				"Spacer7__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "5",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer7",
																						"version": 0
																					},
																					"stamp": 58
																				},
																				"LabelTacitRenewal__": {
																					"type": "Label",
																					"data": [
																						"TacitRenewal__"
																					],
																					"options": {
																						"label": "_Tacit renewal",
																						"version": 0
																					},
																					"stamp": 59
																				},
																				"TacitRenewal__": {
																					"type": "CheckBox",
																					"data": [
																						"TacitRenewal__"
																					],
																					"options": {
																						"label": "_Tacit renewal",
																						"activable": true,
																						"width": "20",
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right"
																					},
																					"stamp": 60
																				},
																				"Spacer3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer3",
																						"version": 0
																					},
																					"stamp": 61
																				},
																				"LabelInitialStartDate__": {
																					"type": "Label",
																					"data": [
																						"InitialStartDate__"
																					],
																					"options": {
																						"label": "_Initial start date",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 62
																				},
																				"InitialStartDate__": {
																					"type": "DateTime",
																					"data": [
																						"InitialStartDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Initial start date",
																						"activable": true,
																						"width": "85",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0,
																						"readonly": true,
																						"autocompletable": false,
																						"hidden": true
																					},
																					"stamp": 63
																				},
																				"LabelInitialEndDate__": {
																					"type": "Label",
																					"data": [
																						"InitialEndDate__"
																					],
																					"options": {
																						"label": "_Initial end date",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 64
																				},
																				"InitialEndDate__": {
																					"type": "DateTime",
																					"data": [
																						"InitialEndDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Initial end date",
																						"activable": true,
																						"width": "85",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"version": 0,
																						"hidden": true,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 65
																				},
																				"Spacer2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer2",
																						"version": 0
																					},
																					"stamp": 66
																				},
																				"LabelStartDate__": {
																					"type": "Label",
																					"data": [
																						"StartDate__"
																					],
																					"options": {
																						"label": "_Start date",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"StartDate__": {
																					"type": "DateTime",
																					"data": [
																						"StartDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Start date",
																						"activable": true,
																						"width": "85",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"helpIconPosition": "Right",
																						"autocompletable": false
																					},
																					"stamp": 68
																				},
																				"LabelDuration__": {
																					"type": "Label",
																					"data": [
																						"Duration__"
																					],
																					"options": {
																						"label": "_Duration",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"Duration__": {
																					"type": "Integer",
																					"data": [
																						"Duration__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_Duration",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "40",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"helpIconPosition": "Right",
																						"enablePlusMinus": true
																					},
																					"stamp": 70
																				},
																				"LabelEndDate__": {
																					"type": "Label",
																					"data": [
																						"EndDate__"
																					],
																					"options": {
																						"label": "_End date",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"EndDate__": {
																					"type": "DateTime",
																					"data": [
																						"EndDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_End date",
																						"activable": true,
																						"width": "85",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"helpIconPosition": "Right",
																						"autocompletable": false
																					},
																					"stamp": 72
																				},
																				"Spacer4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer4",
																						"version": 0
																					},
																					"stamp": 73
																				},
																				"LabelPriorNotice__": {
																					"type": "Label",
																					"data": [
																						"PriorNotice__"
																					],
																					"options": {
																						"label": "_Prior notice",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 74
																				},
																				"PriorNotice__": {
																					"type": "Integer",
																					"data": [
																						"PriorNotice__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_Prior notice",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "40",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"helpIconPosition": "Right",
																						"enablePlusMinus": true,
																						"hidden": true,
																						"autocompletable": false
																					},
																					"stamp": 75
																				},
																				"LabelResiliationDate__": {
																					"type": "Label",
																					"data": [
																						"ResiliationDate__"
																					],
																					"options": {
																						"label": "_ResiliationDate",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 76
																				},
																				"ResiliationDate__": {
																					"type": "DateTime",
																					"data": [
																						"ResiliationDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ResiliationDate",
																						"activable": true,
																						"width": "85",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true,
																						"version": 0,
																						"helpIconPosition": "Right"
																					},
																					"stamp": 77
																				},
																				"LabelRenewalTerms__": {
																					"type": "Label",
																					"data": [
																						"RenewalTerms__"
																					],
																					"options": {
																						"label": "_Renewal terms",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 78
																				},
																				"RenewalTerms__": {
																					"type": "Integer",
																					"data": [
																						"RenewalTerms__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_Renewal terms",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "40",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"helpIconPosition": "Right",
																						"enablePlusMinus": true,
																						"hidden": true,
																						"autocompletable": false
																					},
																					"stamp": 79
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 80
																				},
																				"NextOccurrenceDesc__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 81
																				}
																			}
																		}
																	}
																}
															}
														},
														"Notification": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "150px",
																"iconURL": "Contract_notifications.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Notification",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"panelStyle": "FlexibleFormPanelLight",
																"version": 0,
																"sameHeightAsSiblings": true
															},
															"stamp": 82,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"RenewalReminderDays__": "LabelRenewalReminderDays__",
																			"LabelRenewalReminderDays__": "RenewalReminderDays__",
																			"NotificationDate__": "LabelNotificationDate__",
																			"LabelNotificationDate__": "NotificationDate__"
																		},
																		"version": 0
																	},
																	"stamp": 83,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RenewalReminderDays__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelRenewalReminderDays__": {
																						"line": 2,
																						"column": 1
																					},
																					"NotificationDate__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelNotificationDate__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer8__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 84,
																			"*": {
																				"Spacer8__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "5",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer8",
																						"version": 0
																					},
																					"stamp": 85
																				},
																				"LabelRenewalReminderDays__": {
																					"type": "Label",
																					"data": [
																						"RenewalReminderDays__"
																					],
																					"options": {
																						"label": "_RenewalReminderDays",
																						"version": 0
																					},
																					"stamp": 86
																				},
																				"RenewalReminderDays__": {
																					"type": "Integer",
																					"data": [
																						"RenewalReminderDays__"
																					],
																					"options": {
																						"version": 2,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_RenewalReminderDays",
																						"precision_internal": 0,
																						"precision_current": 0,
																						"activable": true,
																						"width": "40",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"enablePlusMinus": true,
																						"browsable": false,
																						"defaultValue": "30"
																					},
																					"stamp": 87
																				},
																				"NotificationDate__": {
																					"type": "DateTime",
																					"data": [
																						"NotificationDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_NotificationDate",
																						"activable": true,
																						"width": "85",
																						"helpText": "_NotificationDateTooltip",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0,
																						"hidden": false,
																						"readonly": true,
																						"helpIconPosition": "Right",
																						"autocompletable": false
																					},
																					"stamp": 88
																				},
																				"LabelNotificationDate__": {
																					"type": "Label",
																					"data": [
																						"NotificationDate__"
																					],
																					"options": {
																						"label": "_NotificationDate",
																						"version": 0,
																						"hidden": false
																					},
																					"stamp": 89
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-14": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 90,
													"*": {
														"ContractTermination": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "120px",
																"iconURL": "Contract_Validity.png",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Contract termination",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"isSvgIcon": false,
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 91,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"TerminationComment__": "LabelTerminationComment__",
																			"LabelTerminationComment__": "TerminationComment__",
																			"InitiallyExpectedEndDate__": "LabelInitiallyExpectedEndDate__",
																			"LabelInitiallyExpectedEndDate__": "InitiallyExpectedEndDate__",
																			"TerminatedByName__": "LabelTerminatedByName__",
																			"LabelTerminatedByName__": "TerminatedByName__",
																			"TerminatedBylogin__": "LabelTerminatedBylogin__",
																			"LabelTerminatedBylogin__": "TerminatedBylogin__",
																			"TerminationRequestedOn__": "LabelTerminationRequestedOn__",
																			"LabelTerminationRequestedOn__": "TerminationRequestedOn__"
																		},
																		"version": 0
																	},
																	"stamp": 92,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TerminationComment__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTerminationComment__": {
																						"line": 5,
																						"column": 1
																					},
																					"InitiallyExpectedEndDate__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelInitiallyExpectedEndDate__": {
																						"line": 4,
																						"column": 1
																					},
																					"TerminatedByName__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelTerminatedByName__": {
																						"line": 2,
																						"column": 1
																					},
																					"TerminatedBylogin__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelTerminatedBylogin__": {
																						"line": 1,
																						"column": 1
																					},
																					"TerminationRequestedOn__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelTerminationRequestedOn__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 93,
																			"*": {
																				"LabelTerminatedBylogin__": {
																					"type": "Label",
																					"data": [
																						"TerminatedBylogin__"
																					],
																					"options": {
																						"label": "_TerminatedBylogin",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 94
																				},
																				"TerminatedBylogin__": {
																					"type": "ShortText",
																					"data": [
																						"TerminatedBylogin__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TerminatedBylogin",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false,
																						"hidden": true,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 95
																				},
																				"LabelTerminatedByName__": {
																					"type": "Label",
																					"data": [
																						"TerminatedByName__"
																					],
																					"options": {
																						"label": "_TerminatedByName",
																						"version": 0
																					},
																					"stamp": 96
																				},
																				"TerminatedByName__": {
																					"type": "ShortText",
																					"data": [
																						"TerminatedByName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_TerminatedByName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 97
																				},
																				"LabelTerminationRequestedOn__": {
																					"type": "Label",
																					"data": [
																						"TerminationRequestedOn__"
																					],
																					"options": {
																						"label": "_TerminationRequestedOn",
																						"version": 0
																					},
																					"stamp": 98
																				},
																				"TerminationRequestedOn__": {
																					"type": "RealDateTime",
																					"data": [
																						"TerminationRequestedOn__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_TerminationRequestedOn",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": true,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 99
																				},
																				"LabelInitiallyExpectedEndDate__": {
																					"type": "Label",
																					"data": [
																						"InitiallyExpectedEndDate__"
																					],
																					"options": {
																						"label": "_Initially expected end date",
																						"version": 0
																					},
																					"stamp": 100
																				},
																				"InitiallyExpectedEndDate__": {
																					"type": "DateTime",
																					"data": [
																						"InitiallyExpectedEndDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Initially expected end date",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": false,
																						"version": 0
																					},
																					"stamp": 101
																				},
																				"LabelTerminationComment__": {
																					"type": "Label",
																					"data": [
																						"TerminationComment__"
																					],
																					"options": {
																						"label": "_Termination comment",
																						"version": 0
																					},
																					"stamp": 102
																				},
																				"TerminationComment__": {
																					"type": "LongText",
																					"data": [
																						"TerminationComment__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Termination comment",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"browsable": false
																					},
																					"stamp": 103
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 104,
													"*": {
														"Archive_Details": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "120",
																"iconURL": "Contract_Archive.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "left",
																"label": "_Archive Details",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"sameHeightAsSiblings": false
															},
															"stamp": 105,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ArchiveDurationInMonths__": "LabelArchiveDurationInMonths__",
																			"LabelArchiveDurationInMonths__": "ArchiveDurationInMonths__",
																			"ExpirationDate__": "LabelExpirationDate__",
																			"LabelExpirationDate__": "ExpirationDate__"
																		},
																		"version": 0
																	},
																	"stamp": 106,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ArchiveDurationInMonths__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelArchiveDurationInMonths__": {
																						"line": 1,
																						"column": 1
																					},
																					"ExpirationDate__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelExpirationDate__": {
																						"line": 2,
																						"column": 1
																					},
																					"NextOccurrenceWarning__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 107,
																			"*": {
																				"LabelArchiveDurationInMonths__": {
																					"type": "Label",
																					"data": [
																						"ArchiveDurationInMonths__"
																					],
																					"options": {
																						"label": "_ArchiveDurationInMonths",
																						"version": 0
																					},
																					"stamp": 108
																				},
																				"ArchiveDurationInMonths__": {
																					"type": "ComboBox",
																					"data": [
																						"ArchiveDurationInMonths__"
																					],
																					"options": {
																						"possibleValues": {
																							"1": "_OneYear",
																							"2": "_TwoYears",
																							"3": "_ThreeYears",
																							"4": "_FourYears",
																							"5": "_FiveYears",
																							"6": "_SixYears",
																							"7": "_SevenYears",
																							"8": "_EightYears",
																							"9": "_NineYears",
																							"10": "_TenYears",
																							"11": "_ElevenYears",
																							"12": "_TwelveYears",
																							"13": "_ThirteenYears",
																							"14": "_FourteenYears",
																							"15": "_FifteenYears",
																							"20": "_TwentyYears",
																							"25": "_TwentyFiveYears",
																							"30": "_ThirtyYears",
																							"50": "_FiftyYears"
																						},
																						"possibleKeys": {
																							"1": "12",
																							"2": "24",
																							"3": "36",
																							"4": "48",
																							"5": "60",
																							"6": "72",
																							"7": "84",
																							"8": "96",
																							"9": "108",
																							"10": "120",
																							"11": "132",
																							"12": "144",
																							"13": "156",
																							"14": "168",
																							"15": "180",
																							"20": "240",
																							"25": "300",
																							"30": "360",
																							"50": "600"
																						},
																						"keyValueMode": true,
																						"version": 1,
																						"label": "_ArchiveDurationInMonths",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": ""
																					},
																					"stamp": 109
																				},
																				"LabelExpirationDate__": {
																					"type": "Label",
																					"data": null,
																					"options": {
																						"label": "_ExpirationDate"
																					},
																					"stamp": 110
																				},
																				"ExpirationDate__": {
																					"type": "DateTime",
																					"data": null,
																					"options": {
																						"displayLongFormat": false,
																						"label": "_ExpirationDate",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"helpIconPosition": "Right",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Date"
																					},
																					"stamp": 111
																				},
																				"NextOccurrenceWarning__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_This date will be updated at next occurrence",
																						"hidden": true,
																						"version": 0
																					},
																					"stamp": 112
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-13": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 113,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "Contract_Attachment.png",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Documents",
																"sameHeightAsSiblings": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true
																	},
																	"stamp": 114
																}
															},
															"stamp": 115,
															"data": []
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 116,
													"*": {
														"ApprovalWorkflow": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"label": "_ApprovalWorkflow",
																"version": 0,
																"iconURL": "Contract_workflow.png",
																"hideActionButtons": true,
																"TitleFontSize": "L",
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"hideTitle": false,
																"hidden": false,
																"sameHeightAsSiblings": false
															},
															"stamp": 117,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ApprovedDate__": "LabelApproved_date__",
																			"LabelApproved_date__": "ApprovedDate__",
																			"LabelLast_validator_user_ID__": "LastValidatorUserID__",
																			"LastValidatorUserID__": "LabelLast_validator_user_ID__",
																			"LabelLast_validator_name__": "LastValidatorName__",
																			"LastValidatorName__": "LabelLast_validator_name__",
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__"
																		},
																		"version": 0
																	},
																	"stamp": 118,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ApproversList__": {
																						"line": 4,
																						"column": 1
																					},
																					"ApprovedDate__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelApproved_date__": {
																						"line": 6,
																						"column": 1
																					},
																					"Ligne_d_espacement5__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelLast_validator_user_ID__": {
																						"line": 7,
																						"column": 1
																					},
																					"LastValidatorUserID__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelLast_validator_name__": {
																						"line": 8,
																						"column": 1
																					},
																					"LastValidatorName__": {
																						"line": 8,
																						"column": 2
																					},
																					"Ligne_d_espacement4__": {
																						"line": 3,
																						"column": 1
																					},
																					"ComputingWorkflow__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelComputingWorkflow__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 9,
																						"column": 1
																					},
																					"Comments__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 9,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 119,
																			"*": {
																				"LastValidatorName__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_Last validator name",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 120
																				},
																				"Comments__": {
																					"type": "LongText",
																					"data": [
																						"Comments__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 3,
																						"label": "_Comments",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false
																					},
																					"stamp": 121
																				},
																				"Ligne_d_espacement4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement4",
																						"version": 0
																					},
																					"stamp": 122
																				},
																				"LabelLast_validator_name__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorName__"
																					],
																					"options": {
																						"label": "_Last validator name",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 123
																				},
																				"LastValidatorUserID__": {
																					"type": "ShortText",
																					"data": [
																						"LastValidatorUserID__"
																					],
																					"options": {
																						"label": "_Last validator user ID",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"browsable": false
																					},
																					"stamp": 124
																				},
																				"LabelLast_validator_user_ID__": {
																					"type": "Label",
																					"data": [
																						"LastValidatorUserID__"
																					],
																					"options": {
																						"label": "_Last validator user ID",
																						"version": 0
																					},
																					"stamp": 125
																				},
																				"LabelApproved_date__": {
																					"type": "Label",
																					"data": [
																						"ApprovedDate__"
																					],
																					"options": {
																						"label": "_Approved date",
																						"version": 0
																					},
																					"stamp": 126
																				},
																				"Ligne_d_espacement5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"label": "Ligne d'espacement5",
																						"version": 0,
																						"height": "",
																						"width": "",
																						"color": "default"
																					},
																					"stamp": 127
																				},
																				"ApprovedDate__": {
																					"type": "RealDateTime",
																					"data": [
																						"ApprovedDate__"
																					],
																					"options": {
																						"readonly": true,
																						"label": "_Approved date",
																						"activable": true,
																						"width": 230,
																						"displayLongFormat": false,
																						"autocompletable": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true
																					},
																					"stamp": 128
																				},
																				"ApproversList__": {
																					"type": "Table",
																					"data": [
																						"ApproversList__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 10,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"addButtonShouldInsert": true,
																							"hideTableRowDuplicate": true,
																							"hideTableRowButtonDuplicate": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_Approvers list",
																						"readonly": true,
																						"width": "100%"
																					},
																					"stamp": 129,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 130,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 131,
																									"data": []
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 132,
																									"data": []
																								}
																							},
																							"data": []
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 133,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 134,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 135
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 136,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "Label",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"version": 0
																											},
																											"stamp": 137
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 138,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "Label",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"version": 0
																											},
																											"stamp": 139
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 140,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "Label",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"label": "_WRKFDate",
																												"version": 0
																											},
																											"stamp": 141
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 142,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "Label",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"version": 0
																											},
																											"stamp": 143
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 144,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "Label",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 145
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 146,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 147
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 148,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"version": 0,
																												"hidden": true
																											},
																											"stamp": 149,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true
																									},
																									"stamp": 150,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "Label",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"label": "_WRKFActualApprover",
																												"minwidth": 140,
																												"hidden": true
																											},
																											"stamp": 151,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"minwidth": "0",
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 152,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "Label",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"label": "_WRKFRequestDateTime",
																												"minwidth": 140,
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 153,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 154,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 155,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"autocompletable": false,
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 156
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"activable": true,
																												"width": "180",
																												"version": 0,
																												"readonly": true,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 158
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 159,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"activable": true,
																												"width": "140",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false
																											},
																											"stamp": 160
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 161,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFDate",
																												"activable": true,
																												"width": "150",
																												"version": 0
																											},
																											"stamp": 162
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 163,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "LongText",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"activable": true,
																												"width": "300",
																												"version": 0,
																												"resizable": true,
																												"numberOfLines": 5,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"minNbLines": 1
																											},
																											"stamp": 164
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 165,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"activable": true,
																												"width": "140",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 166
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 167,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"activable": true,
																												"width": "90",
																												"version": 0,
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"browsable": false,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 168
																										}
																									},
																									"position": [
																										{}
																									]
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0,
																										"hidden": true
																									},
																									"stamp": 169,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false,
																												"version": 0,
																												"hidden": true,
																												"readonly": true
																											},
																											"stamp": 170,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true
																									},
																									"stamp": 171,
																									"data": [],
																									"*": {
																										"WRKFActualApprover__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFActualApprover__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFActualApprover",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"length": 80,
																												"hidden": true,
																												"browsable": false
																											},
																											"stamp": 172,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell10": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 173,
																									"data": [],
																									"*": {
																										"WRKFRequestDateTime__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFRequestDateTime__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFRequestDateTime",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"helpIconPosition": "Right",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 174,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				},
																				"LabelComputingWorkflow__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 175
																				},
																				"ComputingWorkflow__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "_ComputingWorkflow",
																						"htmlContent": "<div class=\"WaitingIcon-Small\"><div>",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 176
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 177
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 178,
													"*": {
														"RelatedInvoicesPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "Contract_Invoices.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"sameHeightAsSiblings": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_RelatedInvoicesPanelDefault",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"stamp": 179,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {}
																	},
																	"stamp": 180,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"RelatedInvoicesView__": {
																						"line": 1,
																						"column": 1
																					},
																					"RelatedInvoicesNoItem__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"stamp": 181,
																			"*": {
																				"RelatedInvoicesView__": {
																					"type": "AdminList",
																					"data": false,
																					"options": {
																						"openInReadOnlyMode": false,
																						"width": "100%",
																						"label": "_RelatedInvoicesView",
																						"restrictToCurrentJobId": false,
																						"showActions": false,
																						"openInNewTab": true
																					},
																					"stamp": 182
																				},
																				"RelatedInvoicesNoItem__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "italic",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"iconClass": "",
																						"label": "_RelatedInvoicesNoItem",
																						"version": 0,
																						"hidden": true
																					},
																					"stamp": 183
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true,
																"sameHeightAsSiblings": false
															},
															"stamp": 184,
															"data": []
														}
													},
													"stamp": 185,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process",
																"sameHeightAsSiblings": false
															},
															"stamp": 186,
															"data": []
														}
													},
													"stamp": 187,
													"data": []
												}
											},
											"stamp": 188,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 55,
													"width": 45,
													"height": 100
												},
												"hidden": false
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview",
																"sameHeightAsSiblings": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 189
																}
															},
															"stamp": 190,
															"data": []
														}
													},
													"stamp": 191,
													"data": []
												}
											},
											"stamp": 192,
											"data": []
										}
									},
									"stamp": 193,
									"data": []
								}
							},
							"stamp": 194,
							"data": []
						}
					},
					"stamp": 195,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": ""
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 196,
							"data": []
						},
						"ModifyContract": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "ModifyContract",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"iconColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "none"
							},
							"stamp": 197
						},
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Saveandquit",
								"action": "approve",
								"submit": true,
								"version": 0,
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null
							},
							"stamp": 198,
							"data": []
						},
						"DeleteContract": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_DeleteContract",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 3,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 199
						},
						"Terminate": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Terminate",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 3,
								"url": "",
								"position": null,
								"action": "none",
								"version": 0
							},
							"stamp": 200
						},
						"BackToRequester": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_BackToRequester",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "approve",
								"version": 0
							},
							"stamp": 201
						},
						"Share": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Share",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "approve",
								"style": 2,
								"version": 0
							},
							"stamp": 202
						},
						"Approve": {
							"type": "SubmitButton",
							"options": {
								"label": "_Activate",
								"action": "approve",
								"submit": true,
								"style": 1,
								"version": 0
							},
							"stamp": 203,
							"data": []
						},
						"Comment_Answer": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Comment & Answer",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "approve",
								"hidden": true,
								"version": 0
							},
							"stamp": 204
						},
						"EditAsAdmin": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "EditAsAdmin_",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"iconColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "unseal",
								"disabled": true
							},
							"stamp": 205
						},
						"SaveEditing": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "SaveEditing_",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"iconColor": "default",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 2,
								"url": "",
								"position": null,
								"action": "reseal",
								"disabled": true,
								"hidden": true
							},
							"stamp": 206
						}
					},
					"stamp": 207,
					"data": []
				}
			},
			"stamp": 208,
			"data": []
		}
	},
	"stamps": 208,
	"data": []
}