var Extraction;
(function (Extraction)
{
	function SetArchiveDuration()
	{
		// The default archive duration is set in the process properties
		Data.SetValue("ArchiveDuration", Process.GetDefaultArchiveDuration());
	}

	function Main()
	{
		SetArchiveDuration();
		Data.SetValue("ProcessingLabel", "CTR01");
		Data.SetValue("ContractStatus__", "Draft");
	}
	Extraction.Main = Main;
	Main();
})(Extraction || (Extraction = {}));