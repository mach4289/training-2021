///#GLOBALS Lib Sys
/*global setTimeout*/
var workflow = Lib.Contract.Workflow.Controller;
var topMessageWarning = Lib.P2P.TopMessageWarning(Controls.TopPaneWarning, Controls.TopMessageWarning__);
var cachedValues = {
	relatedInvoices: false
};

function CheckRequiredFields()
{
	for (var key in Controls)
	{
		if (Object.prototype.hasOwnProperty.call(Controls, key) && key.endsWith("__") && Controls[key].IsRequired && Controls[key].IsRequired())
		{
			Controls[key].Focus();
		}
	}
	Controls.Comments__.Focus();
}

function SetApproveButtonLabel(status)
{
	var approveButtonLabel;
	if (status === "Draft")
	{
		approveButtonLabel = "_SubmitForApproval";
	}
	else if (Lib.Contract.Workflow.HasAdditionalContributors())
	{
		approveButtonLabel = "_ValidateAndForward";
	}
	else if (workflow.GetContributorAt(workflow.GetContributorIndex() + 1) == null)
	{
		approveButtonLabel = "_Activate";
	}
	else
	{
		approveButtonLabel = "_ValidateContract";
	}
	Controls.Approve.SetLabel(approveButtonLabel);
}

function LoadUserProperties()
{
	return Sys.Helpers.Promise.Create(function (resolve)
	{
		Controls.CompanyCode__.Wait(true);
		Log.Info("LoadUserProperties");
		Lib.P2P.UserProperties.QueryValues(User.loginId)
			.Then(function (UserPropertiesValues)
			{
				var companyCode = Data.GetValue("CompanyCode__") || UserPropertiesValues.CompanyCode__;
				var allowedCompanyCodes = UserPropertiesValues.GetAllowedCompanyCodes() || UserPropertiesValues.CompanyCode__;
				if (allowedCompanyCodes.indexOf(companyCode) < 0)
				{
					allowedCompanyCodes = allowedCompanyCodes + "\n" + companyCode;
				}
				var LdapFilter;
				var ldapUtil = Sys.Helpers.LdapUtil;
				Sys.Helpers.Array.ForEach(allowedCompanyCodes.split("\n"), function (cc)
				{
					if (!LdapFilter)
					{
						LdapFilter = ldapUtil.FilterEqual("CompanyCode__", cc);
					}
					else
					{
						LdapFilter = ldapUtil.FilterOr(LdapFilter, ldapUtil.FilterEqual("CompanyCode__", cc));
					}
				});
				Controls.CompanyCode__.SetFilter(LdapFilter.toString());
				Data.SetValue("CompanyCode__", companyCode);
				Lib.P2P.CompanyCodesValue.QueryValues(companyCode, false)
					.Then(function (CCValues)
					{
						if (Object.keys(CCValues).length <= 0)
						{
							Data.SetError("CompanyCode__", "_This CompanyCode does not exist in the table.");
						}
						Controls.CompanyCode__.Hide(!Controls.CompanyCode__.GetError() && (Lib.P2P.UserProperties.GetValues(User.loginId).GetAllowedCompanyCodes().indexOf("\n") < 0));
						Controls.CompanyCode__.Wait(false);
						resolve();
					});
			});
	});
}
// event handling
function ResetVendor()
{
	Data.SetValue("VendorAddress__", null);
	Data.SetValue("VendorName__", null);
	Data.SetValue("VendorNumber__", null);
	Data.SetValue("VendorEmail__", null);
	Data.SetValue("VendorPhone__", null);
	Data.SetValue("VendorWebsite__", null);
	Data.SetValue("VendorOpeningHours__", null);
}

function OnVendorSelectItem(item)
{
	var vendorAddress = {
		"ToName": item.GetValue("Name__"),
		"ToSub": item.GetValue("Sub__"),
		"ToMail": item.GetValue("Street__"),
		"ToPostal": item.GetValue("PostalCode__"),
		"ToCountry": item.GetValue("Country__"),
		"ToCountryCode": item.GetValue("Country__"),
		"ToState": item.GetValue("Region__"),
		"ToCity": item.GetValue("City__"),
		"ToPOBox": item.GetValue("PostOfficeBox__")
	};
	Data.CheckPostalAddress(true, vendorAddress, "", function (result)
	{
		Data.SetValue("VendorAddress__", result.FormattedBlockAddress.replace(/^[^\r\n]+(\r|\n)+/, ""));
	});
	Data.SetValue("VendorName__", item.GetValue("Name__"));
	Data.SetValue("VendorNumber__", item.GetValue("Number__"));
	Data.SetValue("VendorEmail__", item.GetValue("Email__"));
	Data.SetValue("VendorPhone__", item.GetValue("PhoneNumber__"));
	Data.SetValue("VendorWebsite__", item.GetValue("Website__"));
	Data.SetValue("VendorOpeningHours__", item.GetValue("OpeningHours__"));
	Controls.VendorNumber__.DisplayAs(
	{
		type: "Link"
	});
	Controls.VendorNumber__.OnClick = OpenCompanyDashboard;
}
Controls.VendorName__.OnSelectItem = OnVendorSelectItem;
var previousCompanyCode = Data.GetValue("CompanyCode__");

function OnCompanyCodeSelectItem( /*item: Item*/ )
{
	setTimeout(function ()
	{
		Popup.Confirm("_This action will delete company code related fields.", false, function ()
		{
			previousCompanyCode = Data.GetValue("CompanyCode__");
			ResetVendor();
			Lib.Contract.Workflow.DelayRebuildWorkflow();
		}, function ()
		{
			Data.SetValue("CompanyCode__", previousCompanyCode);
		}, "_Warning");
	});
}
Controls.CompanyCode__.OnSelectItem = OnCompanyCodeSelectItem;

function SetTacitRenewalLayout()
{
	var tacitRenewal = Controls.TacitRenewal__.IsChecked();
	Controls.PriorNotice__.Hide(!tacitRenewal);
	Controls.ResiliationDate__.Hide(!tacitRenewal);
	Controls.RenewalTerms__.Hide(!tacitRenewal);
	Controls.PriorNotice__.SetRequired(tacitRenewal);
	Controls.RenewalTerms__.SetRequired(tacitRenewal);
	if (!Sys.Helpers.IsEmpty(Controls.EndDate__.GetValue()))
	{
		Controls.NextOccurrenceWarning__.Hide(!tacitRenewal || !Sys.Helpers.IsEmpty(Controls.InitiallyExpectedEndDate__.GetValue()));
	}
	SetNextOccuranceDesc();
}

function OnTacitRenewalChange()
{
	SetTacitRenewalLayout();
	var tacitRenewal = Controls.TacitRenewal__.IsChecked();
	if (!tacitRenewal)
	{
		Controls.PriorNotice__.SetValue(null);
		Controls.RenewalTerms__.SetValue(null);
		Controls.ResiliationDate__.SetValue(null);
		Controls.NextOccurrenceWarning__.Hide(true);
		OnPeriodOfNotificationChange();
	}
	else
	{
		Controls.PriorNotice__.SetValue(0);
		Controls.RenewalTerms__.SetValue(Controls.Duration__.GetValue());
		if (!Sys.Helpers.IsEmpty(Controls.EndDate__.GetValue()))
		{
			Controls.NextOccurrenceWarning__.Hide(false);
		}
		OnRenewalTermChange();
		OnPriorNoticeChange();
	}
}
Controls.TacitRenewal__.OnChange = OnTacitRenewalChange;

function OnStartDateChange()
{
	var startDate = Controls.StartDate__.GetValue();
	Controls.InitialStartDate__.SetValue(startDate);
	if (!Sys.Helpers.IsEmpty(startDate))
	{
		Controls.StartDate__.SetError("");
		OnDurationChange();
	}
}
Controls.StartDate__.OnChange = OnStartDateChange;

function OnDurationChange()
{
	var duration = Controls.Duration__.GetValue();
	if (duration > 0)
	{
		Controls.Duration__.SetError("");
		var startDate = Controls.StartDate__.GetValue();
		AutoFillRenewalTerm();
		if (!Sys.Helpers.IsEmpty(startDate))
		{
			Lib.Contract.ComputeEndDate(duration);
			OnEndDateChange();
		}
	}
	else if (duration !== null)
	{
		Controls.Duration__.SetError("_The duration must be positive");
	}
}
Controls.Duration__.OnChange = OnDurationChange;

function OnEndDateChange()
{
	var endDate = Controls.EndDate__.GetValue();
	Controls.InitialEndDate__.SetValue(endDate);
	if (!Sys.Helpers.IsEmpty(endDate))
	{
		Controls.EndDate__.SetError("");
		var startDate = Controls.StartDate__.GetValue();
		if (!Sys.Helpers.IsEmpty(startDate))
		{
			if (Sys.Helpers.Date.CompareDate(startDate, endDate) >= 1)
			{
				Controls.EndDate__.SetError("_The end date date must be later than the start date");
			}
			else
			{
				var deltaInMonth = Sys.Helpers.Date.ComputeDeltaMonth(startDate, endDate);
				var endDateSameMonth = new Date(endDate.getTime());
				endDateSameMonth.setMonth(endDate.getMonth() - deltaInMonth);
				var deltaInDays = Sys.Helpers.Date.ComputeDeltaDays(startDate, endDateSameMonth);
				if (deltaInDays >= 0)
				{
					deltaInMonth += 1;
				}
				Controls.Duration__.SetValue(deltaInMonth);
				Controls.Duration__.SetError("");
				AutoFillRenewalTerm();
			}
		}
		if (Controls.TacitRenewal__.IsChecked())
		{
			OnPriorNoticeChange();
			OnRenewalTermChange();
		}
		else
		{
			OnPeriodOfNotificationChange();
		}
	}
	ArchiveExpirationChange();
}
Controls.EndDate__.OnChange = OnEndDateChange;

function OnPriorNoticeChange()
{
	var priorNotice = Controls.PriorNotice__.GetValue() || 0;
	if (priorNotice < 0)
	{
		Controls.PriorNotice__.SetError("_The period of notice must be positive");
	}
	else
	{
		Controls.PriorNotice__.SetError("");
		var endDate = Controls.EndDate__.GetValue();
		if (!Sys.Helpers.IsEmpty(endDate))
		{
			var resiliationDate = new Date(endDate.getTime());
			resiliationDate.setDate(resiliationDate.getDate() - priorNotice);
			Controls.ResiliationDate__.SetValue(resiliationDate);
			var startDate = Controls.StartDate__.GetValue();
			if (!Sys.Helpers.IsEmpty(startDate) && Sys.Helpers.Date.CompareDate(startDate, resiliationDate) >= 1)
			{
				Controls.PriorNotice__.SetError("_The resiliation date date must be later than the start date");
			}
			OnPeriodOfNotificationChange();
		}
	}
}
Controls.PriorNotice__.OnChange = OnPriorNoticeChange;

function OnPeriodOfNotificationChange()
{
	var timeOfNotification = Controls.RenewalReminderDays__.GetValue() || 0;
	if (timeOfNotification < 0)
	{
		Controls.RenewalReminderDays__.SetError("_The number of days in advance for renewal/expiration reminder must be positive");
	}
	else
	{
		Controls.RenewalReminderDays__.SetError("");
		var notificationDate = void 0;
		var tacitRenewal = Controls.TacitRenewal__.IsChecked();
		if (tacitRenewal)
		{
			var resiliationDate = Controls.ResiliationDate__.GetValue();
			if (!Sys.Helpers.IsEmpty(resiliationDate))
			{
				notificationDate = new Date(resiliationDate.getTime());
			}
		}
		else
		{
			var endDate = Controls.EndDate__.GetValue();
			if (!Sys.Helpers.IsEmpty(endDate))
			{
				notificationDate = new Date(endDate.getTime());
			}
		}
		if (!Sys.Helpers.IsEmpty(notificationDate))
		{
			notificationDate.setDate(notificationDate.getDate() - timeOfNotification);
			Controls.NotificationDate__.SetValue(notificationDate);
			var startDate = Controls.StartDate__.GetValue();
			if (!Sys.Helpers.IsEmpty(startDate) && Sys.Helpers.Date.CompareDate(startDate, notificationDate) >= 1)
			{
				Controls.RenewalReminderDays__.SetError("_The renewal/expiration reminder date must come after the start date");
			}
		}
	}
}
Controls.RenewalReminderDays__.OnChange = OnPeriodOfNotificationChange;

function AutoFillRenewalTerm()
{
	if (Controls.TacitRenewal__.IsChecked() && Sys.Helpers.IsEmpty(Controls.RenewalTerms__.GetValue()))
	{
		Controls.RenewalTerms__.SetValue(Controls.Duration__.GetValue());
		OnRenewalTermChange();
	}
}

function SetNextOccuranceDesc()
{
	var nextOccurenceText = null;
	var status = Controls.ContractStatus__.GetValue();
	if (Controls.TacitRenewal__.IsChecked() && (status !== "Revoked") && (status !== "Expired"))
	{
		if (Sys.Helpers.IsEmpty(Controls.InitiallyExpectedEndDate__.GetValue()))
		{
			var renewalTerm = Controls.RenewalTerms__.GetValue();
			var endDate = Controls.EndDate__.GetValue();
			if (!Sys.Helpers.IsEmpty(endDate) && !Sys.Helpers.IsEmpty(renewalTerm))
			{
				var nextStartDate = new Date(endDate.getTime());
				nextStartDate.setDate(nextStartDate.getDate() + 1);
				var nextEndDate = new Date(nextStartDate.getTime());
				nextEndDate.setMonth(nextEndDate.getMonth() + renewalTerm);
				nextEndDate.setDate(nextStartDate.getDate() - 1);
				nextOccurenceText = Language.Translate("_Next occurence will be from {0} to {1}", false, Language.FormatDate(nextStartDate), Language.FormatDate(nextEndDate));
			}
		}
		else
		{
			Controls.NextOccurrenceDesc__.SetWarningStyle(true);
			nextOccurenceText = Language.Translate("_This is the last occurrence of the tacit renewable contract");
		}
	}
	Controls.NextOccurrenceDesc__.SetText(nextOccurenceText);
	Controls.NextOccurrenceDesc__.Hide(nextOccurenceText === null);
}

function OnRenewalTermChange()
{
	var RenewalTerm = Controls.RenewalTerms__.GetValue();
	if (RenewalTerm > 0)
	{
		Controls.RenewalTerms__.SetError("");
		SetNextOccuranceDesc();
	}
	else if (RenewalTerm <= 0)
	{
		Controls.RenewalTerms__.SetError("_The renewal term must be positive");
	}
}
Controls.RenewalTerms__.OnChange = OnRenewalTermChange;
Controls.BackToRequester.OnClick = function ()
{
	var dialogTexts = {
		titleConfirmation: "_Approval comments confirmation",
		titleRequired: "_Approval comments required",
		descriptionConfirmation: "_Please confirm your comment:",
		descriptionRequired: "_Please write your comment:"
	};

	function fill_CB(dialog /*, tabId, event, control*/ )
	{
		var commentValue = Data.GetValue("Comments__");
		var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
		if (Sys.Helpers.IsEmpty(commentValue))
		{
			ctrl.SetText(Language.Translate(dialogTexts.descriptionRequired));
		}
		else
		{
			ctrl.SetText(Language.Translate(dialogTexts.descriptionConfirmation));
		}
		var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
		dialog.RequireControl(commentCtrl);
		commentCtrl.SetValue(commentValue);
	}

	function commit_CB(dialog)
	{
		var newValue = dialog.GetControl("ctrlComments").GetValue();
		Data.SetValue("Comments__", newValue);
		Data.SetError("Comments__", "");
		ProcessInstance.Approve("BackToRequester");
	}

	function validate_CB(dialog /*, tabId, event, control*/ )
	{
		if (!dialog.GetControl("ctrlComments").GetValue())
		{
			dialog.GetControl("ctrlComments").SetError("_This field is required");
			return false;
		}
		return true;
	}
	var title = Data.GetValue("Comments__") ? dialogTexts.titleConfirmation : dialogTexts.titleRequired;
	Popup.Dialog(title, null, fill_CB, commit_CB, validate_CB);
	return false;
};
Controls.Approve.OnClick = function ()
{
	OnEndDateChange();
	CheckRequiredFields();
	if (Process.ShowFirstError() === null)
	{
		return true;
	}
	return false;
};
Controls.SaveEditing.OnClick = function ()
{
	OnEndDateChange();
	CheckRequiredFields();
	if (Process.ShowFirstError() === null)
	{
		return true;
	}
	return false;
};
Controls.DeleteContract.OnClick = function ()
{
	function OnConfirmCancel()
	{
		ProcessInstance.ApproveAsynchronous("DeleteContract");
	}
	Popup.Confirm("_Confirm delete message", false, OnConfirmCancel, null, "_Confirm delete title");
	return false;
};
Controls.ModifyContract.OnClick = function ()
{
	var dialogTexts = {
		titleRequired: "_ModifyContract",
		descriptionRequired: "_Modify contract warning message"
	};
	ApprovalCommentCheck.OnClick("ModifyContract", dialogTexts);
	return false;
};
Controls.Share.OnClick = function ()
{
	var dialogTexts = {
		title: "_Add advisor",
		role: "_Advisor",
		description: "_Request for information description",
		description_size: 475,
		confirmation: "_Request for information confirm message",
		confirmation_size: 475,
		browseTitle: "_Browse advisor"
	};
	Lib.P2P.Browse.AddApprover(Controls.Comments__.GetValue(), function (contributor, comment)
	{
		if (contributor)
		{
			// Lib.P2P.GetValidatorOrOwner()
			Controls.Comments__.SetValue(comment);
			Variable.SetValueAsString("AdvisorLogin", contributor.login);
			Variable.SetValueAsString("AdvisorName", contributor.displayName);
			Variable.SetValueAsString("AdvisorEmail", contributor.emailAddress);
			ProcessInstance.ApproveAsynchronous("Share");
		}
	}, false, dialogTexts);
	return false;
};
var ApprovalCommentCheck = {
	GetCommentField: function ()
	{
		return Controls.Comments__;
	},
	// Fill dialog callback: Design and instantiation of the controls
	fill_CB: function (dialogTexts)
	{
		return function (dialog /*, tabId, event, control*/ )
		{
			var commentValue = ApprovalCommentCheck.GetCommentField().GetValue();
			var ctrl = dialog.AddDescription("ctrlDesc", null, 466);
			if (Sys.Helpers.IsEmpty(commentValue))
			{
				ctrl.SetText(Language.Translate(dialogTexts.descriptionRequired));
			}
			else
			{
				ctrl.SetText(Language.Translate(dialogTexts.descriptionConfirmation));
			}
			var commentCtrl = dialog.AddMultilineText("ctrlComments", "_Comment", 400);
			dialog.RequireControl(commentCtrl);
			commentCtrl.SetValue(commentValue);
		};
	},
	// Validate dialog callback: checks if required fields are set
	validate_CB: function (dialog /*, tabId, event, control*/ )
	{
		if (!dialog.GetControl("ctrlComments").GetValue())
		{
			dialog.GetControl("ctrlComments").SetError("_This field is required");
			return false;
		}
		return true;
	},
	// Commit dialog callback: updates the process form with dialog results
	commit_CB: function (anAction, onCommitted)
	{
		var action = anAction;
		return function (dialog /*, tabId, event, control*/ )
		{
			var newValue = dialog.GetControl("ctrlComments").GetValue();
			ApprovalCommentCheck.GetCommentField().SetValue(newValue);
			ApprovalCommentCheck.GetCommentField().SetError("");
			if (Data.GetValue("state") != 90)
			{
				if (action === "ModifyContract")
				{
					ProcessInstance.Approve(action);
				}
				else
				{
					ProcessInstance.ApproveAsynchronous(action);
				}
			}
			else
			{
				ProcessInstance.ResumeWithActionAsynchronous(action,
				{
					"Comments__": newValue,
					"LastValidatorName__": User.fullName,
					"LastValidatorUserID__": User.loginId
				});
			}
			if (Sys.Helpers.IsFunction(onCommitted))
			{
				onCommitted();
			}
		};
	},
	OnClick: function (action, dialogTexts, onCommitted)
	{
		var defaultDialogTexts = {
				titleConfirmation: "_Approval comments confirmation",
				titleRequired: "_Approval comments required",
				descriptionConfirmation: "_Please confirm your comment:",
				descriptionRequired: "_Please write your comment:"
			},
			att;
		if (!dialogTexts)
		{
			dialogTexts = defaultDialogTexts;
		}
		else
		{
			for (att in defaultDialogTexts)
			{
				if (!(att in dialogTexts))
				{
					dialogTexts[att] = defaultDialogTexts[att];
				}
			}
		}
		var title = ApprovalCommentCheck.GetCommentField().GetValue() ? dialogTexts.titleConfirmation : dialogTexts.titleRequired;
		Popup.Dialog(title, null, ApprovalCommentCheck.fill_CB(dialogTexts), ApprovalCommentCheck.commit_CB(action, onCommitted), ApprovalCommentCheck.validate_CB);
		return false;
	}
};
Controls.Comment_Answer.OnClick = function ()
{
	Variable.SetValueAsString("AdvisorLogin", User.loginId);
	Variable.SetValueAsString("AdvisorName", User.fullName);
	Variable.SetValueAsString("AdvisorEmail", User.emailAddress);
	ApprovalCommentCheck.OnClick("Comment_Answer", null);
	return false;
};
Controls.Terminate.OnClick = function ()
{
	function fill_CB(dialog /*, tabId, event, control*/ )
	{
		var terminationDialogDescription = dialog.AddDescription("TerminationDialogDescription__", null, 466);
		terminationDialogDescription.SetText(Language.Translate("_TerminationDialogDescription"));
		var terminationDateCtrl = dialog.AddDate("TerminationDateDialog__", "_TerminationDateDialog");
		dialog.RequireControl(terminationDateCtrl);
		var terminationCommentCtrl = dialog.AddMultilineText("TerminationCommentDialog__", "_TerminationCommentDialog", 400);
		dialog.RequireControl(terminationCommentCtrl);
	}

	function commit_CB(dialog)
	{
		var newEndDate = dialog.GetControl("TerminationDateDialog__").GetValue();
		var terminationRequestedOn = new Date();
		var newValues = {
			"EndDate__": newEndDate.toISOString(),
			"InitiallyExpectedEndDate__": Controls.EndDate__.GetValue().toISOString(),
			"TerminationComment__": dialog.GetControl("TerminationCommentDialog__").GetValue(),
			"NotificationDate__": null,
			"ResiliationDate__": null,
			"TerminatedByName__": User.fullName,
			"TerminatedBylogin__": User.loginId,
			"TerminationRequestedOn__": terminationRequestedOn.toISOString()
		};
		ProcessInstance.ResumeWithActionAsynchronous("UpdateContract", newValues);
	}

	function validate_CB(dialog /*, tabId, event, control*/ )
	{
		var terminationDateCtrl = dialog.GetControl("TerminationDateDialog__");
		var terminationDate = terminationDateCtrl.GetValue();
		if (!terminationDate)
		{
			terminationDateCtrl.SetError("This field is required!");
		}
		else if (Sys.Helpers.Date.CompareDate(terminationDate, Data.GetValue("EndDate__")) > 0)
		{
			terminationDateCtrl.SetError("_The termination date should not exceed end date");
		}
		else if (Sys.Helpers.Date.CompareDate(terminationDate, Data.GetValue("StartDate__")) < 0)
		{
			terminationDateCtrl.SetError("_The termination date should not be prior to start date");
		}
		else if (Sys.Helpers.Date.CompareDate(terminationDate, Data.GetValue("ResiliationDate__")) > 0)
		{
			terminationDateCtrl.SetError("_The termination date should not exceed notice date");
		}
		else if (!dialog.GetControl("TerminationCommentDialog__").GetValue())
		{
			dialog.GetControl("TerminationCommentDialog__").SetError("This field is required!");
		}
		else
		{
			return true;
		}
		return false;
	}
	Popup.Dialog("_Contract termination dialog title", null, fill_CB, commit_CB, validate_CB);
	return false;
};

function ArchiveExpirationChange()
{
	var endDate = Controls.EndDate__.GetValue();
	if (!Sys.Helpers.IsEmpty(endDate))
	{
		Controls.ExpirationDate__.Hide(false);
		var expirationDate = Data.GetValue("ArchiveExpiration");
		if (!expirationDate || typeof expirationDate.getMonth !== "function" || Data.GetValue("state") < 100)
		{
			expirationDate = new Date(endDate.getTime());
			expirationDate.setMonth(expirationDate.getMonth() + parseInt(Controls.ArchiveDurationInMonths__.GetValue(), 10));
			expirationDate.setMonth(11, 31);
		}
		Controls.ExpirationDate__.SetValue(expirationDate);
		if (Controls.TacitRenewal__.IsChecked())
		{
			Controls.NextOccurrenceWarning__.Hide(false);
		}
	}
	else
	{
		Controls.ExpirationDate__.Hide(true);
		Controls.NextOccurrenceWarning__.Hide(true);
	}
}
Controls.ArchiveDurationInMonths__.OnChange = ArchiveExpirationChange;
/**
 * Return true if the current user is in the approval workflow
 */
function IsApprover()
{
	var approverList = workflow.GetContributorsByRole("approver");
	var additionalApproversList = workflow.GetAdditionalContributors();
	additionalApproversList = Sys.Helpers.Array.Filter(additionalApproversList, function (contributor)
	{
		return contributor && contributor.role === Lib.Purchasing.roleApprover;
	});

	function ContributorIsCurrentUser(contributor)
	{
		return contributor && (User.loginId === contributor.login || User.IsMemberOf(contributor.login) || User.IsBackupUserOf(contributor.login));
	}
	return !!((approverList && Sys.Helpers.Array.Find(approverList, ContributorIsCurrentUser)) ||
		(additionalApproversList && Sys.Helpers.Array.Find(additionalApproversList, ContributorIsCurrentUser)));
}
/**
 * Return true if the user is the current Contributor or if no workflow is build yet (requester)
 */
function IsCurrentContributor()
{
	var currentContributor = workflow.GetNbContributors() > 0 && workflow.GetContributorAt(workflow.GetContributorIndex());
	if (currentContributor)
	{
		return currentContributor.login === User.loginId ||
			User.IsBackupUserOf(currentContributor.login) ||
			User.IsMemberOf(currentContributor.login)
			// If the current user is the admin and he's not the owner of the document,
			// he can do the next contributor's actions (no matter who's the next contributor)
			||
			Lib.P2P.IsAdminNotOwner();
	}
	//No Workflow yet, you should be the Requester, so you are the Current Contributor
	return true;
}
/**
 * Return true if the current user is in advisor list
 */
function IsInAdvisorList()
{
	var list = Variable.GetValueAsString("AdvisorLoginList");
	if (list)
	{
		var logins = list.split("\n");
		for (var i = 0; i < logins.length; i++)
		{
			if (logins && (User.loginId.toUpperCase() === logins[i].toUpperCase() || User.IsMemberOf(logins[i])))
			{
				return true;
			}
		}
	}
	return false;
}
var GlobalLayout = {
	panes: ["TopPaneWarning", "Banner", "ContractDetails", "VendorContact", "Validity", "Archive_Details", "ApprovalWorkflow", "DocumentsPanel", "PreviewPanel"].map(function (name)
	{
		return Controls[name];
	}),
	actionButtons: ["Close", "Save", "DeleteContract", "BackToRequester", "Share", "Approve", "Comment_Answer", "Terminate", "ModifyContract"].map(function (name)
	{
		return Controls[name];
	}),
	Hide: function (hide)
	{
		GlobalLayout.panes.forEach(function (pane)
		{
			pane.Hide(hide);
		});
		GlobalLayout.actionButtons.forEach(function (button)
		{
			button.Hide(hide);
		});
	},
	SetDocumentSize: function ()
	{
		Controls.form_content_right.SetSize(45);
	}
};

function IsAdvisor()
{
	return !IsCurrentContributor() && IsInAdvisorList();
}

function UpdateLayout()
{
	Log.Info("Update layout");
	var status = Data.GetValue("ContractStatus__");
	SetApproveButtonLabel(status);
	Sys.Helpers.TryCallFunction("Lib.Contract.Customization.Client.OnUpdateLayout");
}

function SetHelpID()
{
	if (Lib.Contract.IsInitialOwner())
	{
		Process.SetHelpId(2700);
	}
	else if (IsApprover())
	{
		Process.SetHelpId(2701);
	}
}
// main part
function Main()
{
	var ownerID = Data.GetValue("OwnerId");
	var isOwner = Lib.P2P.IsOwner();
	var isAdvisor = IsAdvisor();
	var isOOTO = !!ownerID && User.IsBackupUserOf(ownerID);
	var isAdminNotOwner = Lib.P2P.IsAdminNotOwner();
	var isAdmin = Lib.P2P.IsAdmin();
	var isOwnerOrOOTO = isOwner || isOOTO;
	var isInitialOwner = Lib.Contract.IsInitialOwner();
	var status = Data.GetValue("ContractStatus__");
	var state = parseInt(Data.GetValue("State"), 10);
	var showTerminateButton = (status === "Active") &&
		Sys.Helpers.IsEmpty(Controls.InitiallyExpectedEndDate__.GetValue()) &&
		Sys.Helpers.Date.CompareDateToToday(Controls.EndDate__.GetValue()) >= 0 &&
		(isOwnerOrOOTO || isAdminNotOwner) &&
		!ProcessInstance.isEditing;
	var showEditAsAdminContractButton = isAdmin && status === "Active" && !ProcessInstance.isEditing;
	var showSaveEditAsAdminContractButton = status === "Active" && isAdmin && ProcessInstance.isEditing;
	Process.SetHelpId(2506);
	var banner = Sys.Helpers.Banner;
	banner.SetMainTitle("_Contract");
	banner.SetStatusCombo(Controls.ContractStatus__);
	banner.SetHTMLBanner(Controls.HTMLBanner__);
	banner.SetSubTitle();
	// Pass the information wether the account is a demo account or not to the server
	Variable.SetValueAsString("InDemoAccount", User.isInDemoAccount ? "1" : "0");
	// Manage topMessageWarning
	Controls.TopPaneWarning.Hide(true);
	Lib.P2P.DisplayArchiveDurationWarning("PAC", function ()
	{
		topMessageWarning.Add(Language.Translate("_Archive duration warning"));
	});
	Lib.P2P.DisplayAdminWarning("PAC", function (displayName)
	{
		topMessageWarning.Add(Language.Translate("_View as admin on bealf of {0}", false, displayName));
	}, (Data.GetValue("State") != 100) || showTerminateButton, [70, 90, 100]);
	Lib.P2P.DisplayAdminAndOOTOWarning("PAC", function ()
	{
		topMessageWarning.Add(Language.Translate("_Edit as admin"));
	}, showSaveEditAsAdminContractButton, [100]);
	Lib.P2P.DisplayBackupUserWarning("PAC", function (displayName)
	{
		topMessageWarning.Add(Language.Translate("_View on bealf of {0}", false, displayName));
	}, function ()
	{
		var recipientLogin = null;
		// Display warning on state = 90 and 70
		if (state == 50 || state == 70 || (state == 100 && showTerminateButton))
		{
			// Display banner for the backup of the recipient
			var recipientDn = void 0;
			if (isOOTO)
			{
				recipientDn = ownerID;
			}
			recipientLogin = recipientDn ? Sys.Helpers.String.ExtractLoginFromDN(recipientDn) : null;
		}
		return recipientLogin;
	});
	var hideContractTerminationPane = Sys.Helpers.IsEmpty(Controls.InitiallyExpectedEndDate__.GetValue());
	Controls.ContractTermination.Hide(hideContractTerminationPane);
	Controls.InitiallyExpectedEndDate__.SetReadOnly(true);
	Controls.TerminationComment__.SetReadOnly(true);
	if (isAdvisor)
	{
		topMessageWarning.Add(Language.Translate("_You are viewing this contract as an advisor"));
	}
	// A contract has an OwnerId only once it has been saved or submitted
	var isNewContract = !Data.GetValue("OwnerId");
	// Init default archive duration if necessary
	if (!Controls.ArchiveDurationInMonths__.GetValue())
	{
		Controls.ArchiveDurationInMonths__.SetValue(Data.GetValue("ArchiveDuration"));
	}
	ArchiveExpirationChange();
	if (!Controls.OwnerLogin__.GetValue())
	{
		Controls.OwnerLogin__.SetValue(User.loginId);
		Controls.OwnerNiceName__.SetValue(User.fullName);
		Controls.OwnerNiceName__.Hide(true);
	}
	else if (status === "Draft" && Controls.OwnerLogin__.GetValue() === User.loginId)
	{
		Controls.OwnerNiceName__.Hide(true);
	}
	Lib.Contract.Workflow.Init();
	Lib.Contract.Workflow.InitWorkflowPanel(UpdateLayout);
	Lib.Contract.Workflow.UpdateRolesSequence();
	if (status !== "Draft" || isAdvisor)
	{
		Controls.ContractDetails.SetReadOnly(true);
		Controls.VendorContact.SetReadOnly(true);
		Controls.Validity.SetReadOnly(!ProcessInstance.isEditing);
		Controls.Archive_Details.SetReadOnly(true);
		Controls.Notification.SetReadOnly(true);
		Controls.DocumentsPanel.AllowChangeOrUploadReferenceDocument(false);
		Controls.Duration__.Hide(true);
		Controls.VendorName__.DisplayAs(
		{
			type: "Link"
		});
		Controls.VendorName__.OnClick = OpenCompanyDashboard;
		Controls.VendorNumber__.DisplayAs(
		{
			type: "Link"
		});
		Controls.VendorNumber__.OnClick = OpenCompanyDashboard;
	}
	var showInitialDate = !Sys.Helpers.IsEmpty(Controls.StartDate__.GetValue()) && Sys.Helpers.Date.CompareDate(Controls.StartDate__.GetValue(), Controls.InitialStartDate__.GetValue()) != 0;
	Controls.Spacer2__.Hide(!showInitialDate);
	Controls.InitialStartDate__.Hide(!showInitialDate);
	Controls.InitialEndDate__.Hide(!showInitialDate);
	SetTacitRenewalLayout();
	SetApproveButtonLabel(status);
	var showDeleteContractButton = !isNewContract &&
		(status === "Draft" || status === "ToValidate") &&
		isInitialOwner;
	var showSaveButton = (status === "Draft") && isOwnerOrOOTO;
	var showApproveButton = isOwnerOrOOTO && ((status === "Draft") || (status === "ToValidate"));
	var showBackToRequester = (status !== "Draft") && !ProcessInstance.isReadOnly && isOwnerOrOOTO && !ProcessInstance.isEditing;
	var showShareButton = isOwnerOrOOTO && (status === "Draft" || (status === "ToValidate"));
	var showCommentAnswer = isAdvisor && !ProcessInstance.isReadOnly;
	var showModifyContract = status === "ToValidate" && isInitialOwner;
	Controls.DeleteContract.Hide(!showDeleteContractButton);
	Controls.Approve.Hide(!showApproveButton);
	Controls.Save.Hide(!showSaveButton);
	Controls.BackToRequester.Hide(!showBackToRequester);
	Controls.Share.Hide(!showShareButton);
	Controls.Comment_Answer.Hide(!showCommentAnswer);
	Controls.Terminate.Hide(!showTerminateButton);
	Controls.ModifyContract.Hide(!showModifyContract);
	Controls.EditAsAdmin.Hide(!showEditAsAdminContractButton);
	Controls.SaveEditing.Hide(!showSaveEditAsAdminContractButton);
	if (isOwnerOrOOTO)
	{
		workflow.AllowRebuild(true);
		if (status === "Draft")
		{
			workflow.Rebuild();
		}
	}
	var func = Sys.Helpers.TryGetFunction("Lib.Contract.Customization.Client.CustomizeLayout");
	if (func)
	{
		func();
	}
	else
	{
		Sys.Helpers.TryCallFunction("Lib.Contract.Customization.Client.CustomiseLayout");
	}
	SetHelpID();
	fillRelatedInvoicesView();
}
GlobalLayout.Hide(true);
var promises = [];
var OnloadPromise = Sys.Helpers.TryCallFunction("Lib.Contract.Customization.Client.OnLoad");
if (OnloadPromise)
{
	promises.push(OnloadPromise);
}
promises.push(LoadUserProperties());
Sys.Helpers.Synchronizer.All(promises)
	.Then(function ()
	{
		GlobalLayout.SetDocumentSize();
		GlobalLayout.Hide(false);
		Main();
	});

function fillRelatedInvoicesView()
{
	if (Data.GetValue("ReferenceNumber__"))
	{
		if (cachedValues.relatedInvoices)
		{
			setRelatedInvoicesView();
		}
		Controls.RelatedInvoicesPanel.Hide(false);
		Query.DBQuery(fillRelatedInvoicesViewCallBack, "CDNAME#Vendor invoice", "MsnEx", getRelatedInvoiceFilter(), null, 1,
		{}, "FastSearch=1");
	}
	else
	{
		Controls.RelatedInvoicesPanel.Hide(true);
		Controls.RelatedInvoicesView__.Hide(true);
	}
}

function fillRelatedInvoicesViewCallBack(callbackResult)
{
	if (callbackResult && callbackResult.GetRecordsCount() > 0)
	{
		cachedValues.relatedInvoices = true;
		setRelatedInvoicesView();
	}
	else
	{
		cachedValues.relatedInvoices = false;
		showRelatedInvoiceView(false);
	}
}

function setRelatedInvoicesView()
{
	Controls.RelatedInvoicesView__.SetView(
	{
		tabName: "_My documents-AP-Embedded",
		filter: getRelatedInvoiceFilter(),
		viewName: "_P2P_Contract_View_Related Invoices - embedded",
		processOrTableName: "Vendor invoice",
		checkProfileTab: false
	});
	Controls.RelatedInvoicesView__.OpenInNewTab(true);
	Controls.RelatedInvoicesView__.OpenInReadOnlyMode(false);
	Controls.RelatedInvoicesView__.Apply();
	showRelatedInvoiceView(true);
}

function getRelatedInvoiceFilter()
{
	return Sys.Helpers.LdapUtil.FilterAnd(Sys.Helpers.LdapUtil.FilterEqual("ContractNumber__", Data.GetValue("ReferenceNumber__")), Sys.Helpers.LdapUtil.FilterEqual("VendorNumber__", Data.GetValue("VendorNumber__"))).toString();
}

function showRelatedInvoiceView(isShown)
{
	Controls.RelatedInvoicesView__.Hide(!isShown);
	Controls.RelatedInvoicesNoItem__.Hide(isShown);
}

function OpenCompanyDashboard()
{
	var vendorNumber = Controls.VendorNumber__.GetValue(),
		companyCode = Controls.CompanyCode__.GetValue();
	if (vendorNumber && companyCode)
	{
		Lib.P2P.SIM.OpenCompanyDashboard(vendorNumber, companyCode);
	}
}