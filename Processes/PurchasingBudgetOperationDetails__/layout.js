{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"hideTitle": false
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CostCenter__": "LabelCostCenter__",
																	"LabelCostCenter__": "CostCenter__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"OperationID__": "LabelOperation_ID__",
																	"LabelOperation_ID__": "OperationID__",
																	"OperationRuidex__": "LabelOperation_ruidex__",
																	"LabelOperation_ruidex__": "OperationRuidex__",
																	"ToApprove__": "LabelToApprove__",
																	"LabelToApprove__": "ToApprove__",
																	"Committed__": "LabelCommitted__",
																	"LabelCommitted__": "Committed__",
																	"Ordered__": "LabelOrdered__",
																	"LabelOrdered__": "Ordered__",
																	"Received__": "LabelReceived__",
																	"LabelReceived__": "Received__",
																	"PeriodCode__": "LabelPeriod_code__",
																	"LabelPeriod_code__": "PeriodCode__",
																	"SourceType__": "LabelSource_type__",
																	"LabelSource_type__": "SourceType__",
																	"InvoicedNonPO__": "LabelInvoicedNonPO__",
																	"LabelInvoicedNonPO__": "InvoicedNonPO__",
																	"InvoicedPO__": "LabelInvoicedPO__",
																	"LabelInvoicedPO__": "InvoicedPO__",
																	"Group__": "LabelGroup__",
																	"LabelGroup__": "Group__",
																	"BudgetID__": "LabelBudgetID__",
																	"LabelBudgetID__": "BudgetID__",
																	"CostType__": "LabelCostType__",
																	"LabelCostType__": "CostType__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 15,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CostCenter__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelCostCenter__": {
																				"line": 7,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 4,
																				"column": 1
																			},
																			"OperationID__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelOperation_ID__": {
																				"line": 1,
																				"column": 1
																			},
																			"OperationRuidex__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelOperation_ruidex__": {
																				"line": 3,
																				"column": 1
																			},
																			"ToApprove__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelToApprove__": {
																				"line": 10,
																				"column": 1
																			},
																			"Committed__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelCommitted__": {
																				"line": 11,
																				"column": 1
																			},
																			"Ordered__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelOrdered__": {
																				"line": 12,
																				"column": 1
																			},
																			"Received__": {
																				"line": 13,
																				"column": 2
																			},
																			"LabelReceived__": {
																				"line": 13,
																				"column": 1
																			},
																			"PeriodCode__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelPeriod_code__": {
																				"line": 6,
																				"column": 1
																			},
																			"SourceType__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelSource_type__": {
																				"line": 2,
																				"column": 1
																			},
																			"InvoicedNonPO__": {
																				"line": 15,
																				"column": 2
																			},
																			"LabelInvoicedNonPO__": {
																				"line": 15,
																				"column": 1
																			},
																			"InvoicedPO__": {
																				"line": 14,
																				"column": 2
																			},
																			"LabelInvoicedPO__": {
																				"line": 14,
																				"column": 1
																			},
																			"Group__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelGroup__": {
																				"line": 8,
																				"column": 1
																			},
																			"BudgetID__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelBudgetID__": {
																				"line": 5,
																				"column": 1
																			},
																			"CostType__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelCostType__": {
																				"line": 9,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelOperation_ID__": {
																			"type": "Label",
																			"data": [
																				"OperationID__"
																			],
																			"options": {
																				"label": "_Operation ID",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"OperationID__": {
																			"type": "ShortText",
																			"data": [
																				"OperationID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Operation ID",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 15
																		},
																		"LabelSource_type__": {
																			"type": "Label",
																			"data": [
																				"SourceType__"
																			],
																			"options": {
																				"label": "_Source type",
																				"version": 0
																			},
																			"stamp": 38
																		},
																		"SourceType__": {
																			"type": "Integer",
																			"data": [
																				"SourceType__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": true,
																				"label": "_Source type",
																				"precision_internal": 0,
																				"activable": true,
																				"width": 230,
																				"precision_current": 0
																			},
																			"stamp": 39
																		},
																		"LabelOperation_ruidex__": {
																			"type": "Label",
																			"data": [
																				"OperationRuidex__"
																			],
																			"options": {
																				"label": "_Operation ruidex",
																				"version": 0
																			},
																			"stamp": 28
																		},
																		"OperationRuidex__": {
																			"type": "ShortText",
																			"data": [
																				"OperationRuidex__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Operation ruidex",
																				"activable": true,
																				"width": 230,
																				"length": 200,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 29
																		},
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_Company code",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"CompanyCode__": {
																			"type": "ShortText",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Company code",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 17
																		},
																		"LabelBudgetID__": {
																			"type": "Label",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"label": "_BudgetID"
																			},
																			"stamp": 46
																		},
																		"BudgetID__": {
																			"type": "ShortText",
																			"data": [
																				"BudgetID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_BudgetID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 47
																		},
																		"LabelPeriod_code__": {
																			"type": "Label",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"label": "_Period code",
																				"version": 0
																			},
																			"stamp": 36
																		},
																		"PeriodCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"PeriodCode__"
																			],
																			"options": {
																				"browsable": true,
																				"label": "_Period code",
																				"activable": true,
																				"width": 230,
																				"fTSmaxRecords": "20",
																				"PreFillFTS": true,
																				"version": 0
																			},
																			"stamp": 37
																		},
																		"LabelCostCenter__": {
																			"type": "Label",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"label": "_Cost center",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"CostCenter__": {
																			"type": "ShortText",
																			"data": [
																				"CostCenter__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Cost center",
																				"activable": true,
																				"width": 230,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 19
																		},
																		"LabelGroup__": {
																			"type": "Label",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"label": "_Group",
																				"version": 0
																			},
																			"stamp": 44
																		},
																		"Group__": {
																			"type": "ShortText",
																			"data": [
																				"Group__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"label": "_Group",
																				"activable": true,
																				"width": 230,
																				"length": 64,
																				"browsable": false,
																				"version": 0
																			},
																			"stamp": 45
																		},
																		"LabelCostType__": {
																			"type": "Label",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"label": "_CostType"
																			},
																			"stamp": 48
																		},
																		"CostType__": {
																			"type": "ComboBox",
																			"data": [
																				"CostType__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "",
																					"1": "_OpEx",
																					"2": "_CapEx"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "",
																					"1": "OpEx",
																					"2": "CapEx"
																				},
																				"label": "_CostType",
																				"activable": true,
																				"width": 80,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 49
																		},
																		"LabelToApprove__": {
																			"type": "Label",
																			"data": [
																				"ToApprove__"
																			],
																			"options": {
																				"label": "_ToApprove",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"ToApprove__": {
																			"type": "Decimal",
																			"data": [
																				"ToApprove__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ToApprove",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2
																			},
																			"stamp": 25
																		},
																		"LabelCommitted__": {
																			"type": "Label",
																			"data": [
																				"Committed__"
																			],
																			"options": {
																				"label": "_Committed",
																				"version": 0
																			},
																			"stamp": 30
																		},
																		"Committed__": {
																			"type": "Decimal",
																			"data": [
																				"Committed__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Committed",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2
																			},
																			"stamp": 31
																		},
																		"LabelOrdered__": {
																			"type": "Label",
																			"data": [
																				"Ordered__"
																			],
																			"options": {
																				"label": "_Ordered",
																				"version": 0
																			},
																			"stamp": 32
																		},
																		"Ordered__": {
																			"type": "Decimal",
																			"data": [
																				"Ordered__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Ordered",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2
																			},
																			"stamp": 33
																		},
																		"LabelReceived__": {
																			"type": "Label",
																			"data": [
																				"Received__"
																			],
																			"options": {
																				"label": "_Received",
																				"version": 0
																			},
																			"stamp": 34
																		},
																		"Received__": {
																			"type": "Decimal",
																			"data": [
																				"Received__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Received",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"precision_current": 2
																			},
																			"stamp": 35
																		},
																		"LabelInvoicedPO__": {
																			"type": "Label",
																			"data": [
																				"InvoicedPO__"
																			],
																			"options": {
																				"label": "_Invoiced po",
																				"version": 0
																			},
																			"stamp": 42
																		},
																		"InvoicedPO__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedPO__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced po",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"autocompletable": false,
																				"precision_current": 2
																			},
																			"stamp": 43
																		},
																		"LabelInvoicedNonPO__": {
																			"type": "Label",
																			"data": [
																				"InvoicedNonPO__"
																			],
																			"options": {
																				"label": "_Invoiced non po",
																				"version": 0
																			},
																			"stamp": 40
																		},
																		"InvoicedNonPO__": {
																			"type": "Decimal",
																			"data": [
																				"InvoicedNonPO__"
																			],
																			"options": {
																				"version": 1,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_Invoiced non po",
																				"precision_internal": 2,
																				"activable": true,
																				"width": 230,
																				"autocompletable": false,
																				"precision_current": 2
																			},
																			"stamp": 41
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 49,
	"data": []
}