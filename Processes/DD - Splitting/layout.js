{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 640
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 640
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 640
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 25
										},
										"hidden": true
									},
									"*": {},
									"stamp": 7,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 100
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 8,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Next Process",
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false
															},
															"stamp": 9,
															"data": []
														}
													}
												},
												"form-content-left-8": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 147,
													"*": {
														"Actions_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "color7",
																"labelsAlignment": "right",
																"label": "Actions_pane",
																"leftImageURL": "",
																"removeMargins": true,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 142,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Action_Send__": "LabelAction_Send__",
																			"LabelAction_Send__": "Action_Send__",
																			"Action_Cancel__": "LabelAction_Cancel__",
																			"LabelAction_Cancel__": "Action_Cancel__"
																		},
																		"version": 0
																	},
																	"stamp": 143,
																	"*": {
																		"Grid": {
																			"type": "HorizontalLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Action_Send__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelAction_Send__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 1,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 4,
																						"column": 1
																					},
																					"Action_Cancel__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelAction_Cancel__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"colspans": [
																					[
																						[
																							{
																								"index": 0,
																								"length": 2
																							}
																						]
																					],
																					[
																						[
																							[]
																						]
																					],
																					[],
																					[
																						[]
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 145,
																			"*": {
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line2",
																						"version": 0
																					},
																					"stamp": 155
																				},
																				"LabelAction_Send__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 153
																				},
																				"Action_Send__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Action_Send__",
																						"label": "",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color5",
																						"nextprocess": {
																							"processName": "DD - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "Bouton_check-small.png",
																						"style": 4,
																						"action": "approve",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 154
																				},
																				"LabelAction_Cancel__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 157
																				},
																				"Action_Cancel__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Action_Cancel__",
																						"label": "",
																						"textPosition": "text-below",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color5",
																						"nextprocess": {
																							"processName": "DD - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "Bouton_quit-small.png",
																						"style": 4,
																						"action": "cancel",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 158
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line3",
																						"version": 0
																					},
																					"stamp": 156
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 124,
													"*": {
														"Header_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Header_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 120,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 121,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"HeaderTitle__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 122,
																			"*": {
																				"HeaderTitle__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "HeaderTitle__",
																						"version": 0
																					},
																					"stamp": 129
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 89,
													"*": {
														"Informations_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false
																},
																"labelLength": "200",
																"label": "Informations_pane",
																"version": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"hidden": false
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Page_count__": "LabelPage_count__",
																			"LabelPage_count__": "Page_count__",
																			"Documents_count__": "LabelDocuments_count__",
																			"LabelDocuments_count__": "Documents_count__",
																			"ConfigurationName__": "LabelConfigurationName__",
																			"LabelConfigurationName__": "ConfigurationName__"
																		},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Page_count__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPage_count__": {
																						"line": 2,
																						"column": 1
																					},
																					"Documents_count__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelDocuments_count__": {
																						"line": 3,
																						"column": 1
																					},
																					"ConfigurationName__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelConfigurationName__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"LabelConfigurationName__": {
																					"type": "Label",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"label": "ConfigurationName__",
																						"version": 0
																					},
																					"stamp": 14
																				},
																				"ConfigurationName__": {
																					"type": "ShortText",
																					"data": [
																						"ConfigurationName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "ConfigurationName__",
																						"activable": true,
																						"width": "230",
																						"browsable": false,
																						"autocompletable": false,
																						"readonly": true,
																						"version": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 15
																				},
																				"LabelPage_count__": {
																					"type": "Label",
																					"data": [
																						"Page_count__"
																					],
																					"options": {
																						"label": "Page_count__",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"Page_count__": {
																					"type": "Integer",
																					"data": [
																						"Page_count__"
																					],
																					"options": {
																						"integer": true,
																						"label": "Page_count__",
																						"precision_internal": 0,
																						"activable": true,
																						"width": "60",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"defaultValue": "1"
																					},
																					"stamp": 28
																				},
																				"LabelDocuments_count__": {
																					"type": "Label",
																					"data": [
																						"Documents_count__"
																					],
																					"options": {
																						"label": "Documents_count__",
																						"version": 0
																					},
																					"stamp": 29
																				},
																				"Documents_count__": {
																					"type": "Integer",
																					"data": [
																						"Documents_count__"
																					],
																					"options": {
																						"integer": true,
																						"label": "Documents_count__",
																						"precision_internal": 0,
																						"activable": true,
																						"width": "60",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "right",
																						"textStyle": "default",
																						"textColor": "default",
																						"precision_current": 0,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 30
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 60,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true,
																	"maximized": false
																},
																"labelLength": 0,
																"label": "_System data",
																"hidden": true,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false
															},
															"stamp": 61,
															"data": []
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 192,
													"*": {
														"Explanation_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Explanation_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 187,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 188,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DescSplitting__": {
																						"line": 1,
																						"column": 1
																					},
																					"Marketing__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line5__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 189,
																			"*": {
																				"Marketing__": {
																					"type": "HTML",
																					"data": false,
																					"options": {
																						"label": "Marketing",
																						"width": "100%",
																						"css": "@ img {\npadding-left:260px;\n}@ ",
																						"version": 0
																					},
																					"stamp": 180
																				},
																				"Spacer_line5__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line5",
																						"version": 0
																					},
																					"stamp": 185
																				},
																				"DescSplitting__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "DescSplitting__",
																						"version": 0
																					},
																					"stamp": 160
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 169,
													"*": {
														"DocumentsPanel": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"label": "_Documents",
																"hidden": false,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0
																	},
																	"stamp": 5
																}
															},
															"stamp": 6,
															"data": []
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 195,
													"*": {
														"Toggle_button_top_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "240",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Toggle_button_top_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false
															},
															"stamp": 196,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Button_Show_More_Less__": "LabelButton_Show_More_Less__",
																			"LabelButton_Show_More_Less__": "Button_Show_More_Less__"
																		},
																		"version": 0
																	},
																	"stamp": 197,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Button_Show_More_Less__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelButton_Show_More_Less__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 198,
																			"*": {
																				"LabelButton_Show_More_Less__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 199
																				},
																				"Button_Show_More_Less__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_Show_More_Less__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color5",
																						"nextprocess": {
																							"processName": "DD - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "show_more.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 200
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 219,
													"*": {
														"Process_data": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Process_data",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false
															},
															"stamp": 210,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"LabelTime_completion__": "Time_completion__",
																			"Time_completion__": "LabelTime_completion__",
																			"LabelTime_submit__": "Time_submit__",
																			"Time_submit__": "LabelTime_submit__",
																			"LabelFilename__": "Filename__",
																			"Filename__": "LabelFilename__",
																			"LabelStatus__": "Status__",
																			"Status__": "LabelStatus__",
																			"Short_Status__": "LabelShort_Status__",
																			"LabelShort_Status__": "Short_Status__"
																		},
																		"version": 0
																	},
																	"stamp": 211,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"LabelTime_completion__": {
																						"line": 5,
																						"column": 1
																					},
																					"Time_completion__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelTime_submit__": {
																						"line": 4,
																						"column": 1
																					},
																					"Time_submit__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelFilename__": {
																						"line": 3,
																						"column": 1
																					},
																					"Filename__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 1,
																						"column": 1
																					},
																					"Status__": {
																						"line": 1,
																						"column": 2
																					},
																					"Short_Status__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelShort_Status__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 212,
																			"*": {
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Status__",
																						"version": 0
																					},
																					"stamp": 111
																				},
																				"Status__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "Status__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0,
																						"readonly": true
																					},
																					"stamp": 112
																				},
																				"LabelShort_Status__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "Short_Status__",
																						"version": 0
																					},
																					"stamp": 228
																				},
																				"Short_Status__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "Short_Status__",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "String",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 229
																				},
																				"LabelFilename__": {
																					"type": "Label",
																					"data": [
																						"Filename__"
																					],
																					"options": {
																						"label": "Filename__",
																						"version": 0
																					},
																					"stamp": 16
																				},
																				"Filename__": {
																					"type": "ShortText",
																					"data": [
																						"Filename__"
																					],
																					"options": {
																						"label": "Filename__",
																						"activable": true,
																						"width": "230",
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 17
																				},
																				"LabelTime_submit__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "Time_submit__",
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"Time_submit__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"label": "Time_submit__",
																						"text": "",
																						"width": "300",
																						"openInCurrentWindow": false
																					},
																					"stamp": 23
																				},
																				"Time_completion__": {
																					"type": "Link",
																					"data": false,
																					"options": {
																						"version": 1,
																						"link": "",
																						"label": "Time_completion__",
																						"text": "",
																						"width": "300",
																						"openInCurrentWindow": false
																					},
																					"stamp": 25
																				},
																				"LabelTime_completion__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "Time_completion__",
																						"version": 0
																					},
																					"stamp": 24
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-15": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 207,
													"*": {
														"Splitting_result_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {
																	"collapsed": false,
																	"maximized": false
																},
																"labelLength": "200",
																"label": "Splitting_result_pane",
																"version": 0,
																"hidden": false,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left"
															},
															"stamp": 49,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Split_parameters__": "LabelSplit_parameters__",
																			"LabelSplit_parameters__": "Split_parameters__",
																			"Split_details__": "LabelSplit_details__",
																			"LabelSplit_details__": "Split_details__",
																			"Split_area__": "LabelSplit_area__",
																			"LabelSplit_area__": "Split_area__",
																			"SplittingStatus__": "LabelSplittingStatus__",
																			"LabelSplittingStatus__": "SplittingStatus__",
																			"Split_success__": "LabelSplit_success__",
																			"LabelSplit_success__": "Split_success__"
																		},
																		"version": 0
																	},
																	"stamp": 50,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Split_parameters__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSplit_parameters__": {
																						"line": 3,
																						"column": 1
																					},
																					"Split_details__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSplit_details__": {
																						"line": 2,
																						"column": 1
																					},
																					"Split_area__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSplit_area__": {
																						"line": 4,
																						"column": 1
																					},
																					"SplittingStatus__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelSplittingStatus__": {
																						"line": 1,
																						"column": 1
																					},
																					"Split_success__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSplit_success__": {
																						"line": 5,
																						"column": 1
																					}
																				},
																				"lines": 5,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 51,
																			"*": {
																				"LabelSplittingStatus__": {
																					"type": "Label",
																					"data": [],
																					"options": {
																						"label": "SplittingStatus__",
																						"version": 0
																					},
																					"stamp": 109
																				},
																				"SplittingStatus__": {
																					"type": "ShortText",
																					"data": [],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "SplittingStatus__",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false,
																						"notInDB": true,
																						"dataType": "String",
																						"version": 0,
																						"readonly": true
																					},
																					"stamp": 110
																				},
																				"LabelSplit_details__": {
																					"type": "Label",
																					"data": [
																						"Split_details__"
																					],
																					"options": {
																						"label": "Split_details__",
																						"version": 0
																					},
																					"stamp": 54
																				},
																				"Split_details__": {
																					"type": "LongText",
																					"data": [
																						"Split_details__"
																					],
																					"options": {
																						"label": "Split_details__",
																						"activable": true,
																						"width": "300",
																						"numberOfLines": 6,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"resizable": true,
																						"browsable": false,
																						"minNbLines": 1
																					},
																					"stamp": 55
																				},
																				"LabelSplit_parameters__": {
																					"type": "Label",
																					"data": [
																						"Split_parameters__"
																					],
																					"options": {
																						"label": "Split_parameters__",
																						"version": 0
																					},
																					"stamp": 56
																				},
																				"Split_parameters__": {
																					"type": "LongText",
																					"data": [
																						"Split_parameters__"
																					],
																					"options": {
																						"label": "Split_parameters__",
																						"activable": true,
																						"width": "300",
																						"numberOfLines": 6,
																						"readonly": true,
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"resizable": true,
																						"browsable": false,
																						"minNbLines": 1
																					},
																					"stamp": 57
																				},
																				"LabelSplit_area__": {
																					"type": "Label",
																					"data": [
																						"Split_area__"
																					],
																					"options": {
																						"label": "Split_area__",
																						"version": 0
																					},
																					"stamp": 58
																				},
																				"Split_area__": {
																					"type": "ShortText",
																					"data": [
																						"Split_area__"
																					],
																					"options": {
																						"label": "Split_area__",
																						"activable": true,
																						"width": "300",
																						"version": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 59
																				},
																				"LabelSplit_success__": {
																					"type": "Label",
																					"data": [
																						"Split_success__"
																					],
																					"options": {
																						"label": "_Success"
																					},
																					"stamp": 230
																				},
																				"Split_success__": {
																					"type": "CheckBox",
																					"data": [
																						"Split_success__"
																					],
																					"options": {
																						"label": "_Success",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format"
																					},
																					"stamp": 231
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 222,
													"*": {
														"Toggle_button_bottom_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "240",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Toggle_button_bottom_pane",
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"hidden": false
															},
															"stamp": 223,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Button_Show_More_Less_bottom__": "LabelButton_Show_More_Less_bottom__",
																			"LabelButton_Show_More_Less_bottom__": "Button_Show_More_Less_bottom__"
																		},
																		"version": 0
																	},
																	"stamp": 224,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Button_Show_More_Less_bottom__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelButton_Show_More_Less_bottom__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 225,
																			"*": {
																				"LabelButton_Show_More_Less_bottom__": {
																					"type": "Label",
																					"data": false,
																					"options": {
																						"label": "",
																						"version": 0
																					},
																					"stamp": 226
																				},
																				"Button_Show_More_Less_bottom__": {
																					"type": "FormButton",
																					"data": false,
																					"options": {
																						"buttonLabel": "Button_Show_More_Less_bottom__",
																						"label": "",
																						"textPosition": "text-right",
																						"textSize": "MS",
																						"textStyle": "default",
																						"textColor": "color5",
																						"nextprocess": {
																							"processName": "DD - Application Settings Wizard",
																							"attachmentsMode": "all",
																							"willBeChild": true,
																							"returnToOriginalUrl": false
																						},
																						"urlImage": "show_less.png",
																						"style": 4,
																						"action": "none",
																						"url": "",
																						"version": 0
																					},
																					"stamp": 227
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 67,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "49%",
													"width": "51%",
													"height": null
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 68,
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"label": "_Document Preview",
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": true,
																"hideTitle": false
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 69
																}
															},
															"stamp": 70,
															"data": []
														}
													}
												}
											},
											"stamp": 71,
											"data": []
										}
									},
									"stamp": 72,
									"data": []
								}
							},
							"stamp": 73,
							"data": []
						}
					},
					"stamp": 74,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 640
					},
					"*": {},
					"stamp": 78,
					"data": []
				}
			},
			"stamp": 79,
			"data": []
		}
	},
	"stamps": 231,
	"data": []
}