///#GLOBALS Sys

// Handle error during splitting analysis
var splitStatus = Variable.GetValueAsString("Split_status__");
if ((splitStatus !== null) && (splitStatus !== "SUCCESS"))
{
	var shortStatus = Data.GetValue("ShortStatus");
	if (!shortStatus) {
		Data.SetValue("ShortStatus", Language.Translate("_Splitting error"));
	}

	if (Data.GetActionType() === "")
	{
		// directly put job in error in touchless mode
		Data.SetValue("State", 200);
	}
	else
	{
		// otherwise prevent approval
		Process.PreventApproval();
	}
}

Sys.Helpers.TryCallFunction("Lib.DD.Customization.Validation.FinalizeSplittingValidation");
