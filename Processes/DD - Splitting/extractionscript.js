///#GLOBALS Lib Sys

function InitializeSplitting()
{
	var iPageCount = Document.GetPageCount();
	Data.SetValue("Filename__", Document.GetName());
	Data.SetValue("Page_count__", iPageCount.toString());
	Data.SetValue("Subject", Attach.GetName(0));

	var bAnalysisOk = false;
	var configurationName = Data.GetValue("ConfigurationName__");
	if (configurationName)
	{
		bAnalysisOk = GetSplittingResult(iPageCount);
	}
	else
	{
		bAnalysisOk = Lib.DD.Splitting.ApplyOffset(0, iPageCount);
	}

	Data.SetValue("Split_details__", Lib.DD.Splitting.details);

	if (!bAnalysisOk)
	{
		Data.SetValue("Split_parameters__", "");
		Data.SetValue("Documents_count__", "0");
		Variable.SetValueAsString("Split_status__", Lib.DD.Splitting.error);
	}
	else
	{
		var splitParams = Lib.DD.Splitting.SerializeSplitParametersByChunk();
		Variable.SetValueAsString("SplitParams_NbRange", splitParams.length);
		for (var i = 0; i < splitParams.length; i++)
		{
			Variable.SetValueAsString("SplitParams_Range_" + i, splitParams[i]);
		}

		Data.SetValue("Split_parameters__", JSON.stringify(splitParams));
		Data.SetValue("Documents_count__", Lib.DD.Splitting.nDocs.toString());
		Variable.SetValueAsString("Split_status__", "SUCCESS");
	}
	Data.SetValue("Split_success__", bAnalysisOk ? 1 : 0);
}

function GetSplittingResult(iPageCount)
{
	var sSplitType = Sys.DD.GetParameter("Split_DivisionMethod");
	var iSplitOffset = Sys.DD.GetParameter("Split_Offset");
	var iDocLength = Sys.DD.GetParameter("Split_NumberPages");
	var bCaseSensitive = Sys.DD.GetParameter("Split_CaseSensitive");
	var bRegExpUse = Sys.DD.GetParameter("Split_UseRegex");
	var bSplitBefore = Sys.DD.GetParameter("Split_Location__") === "0";
	var sSplitString = Sys.DD.GetParameter("Split_String__");
	var sArea = Sys.DD.GetParameter("Split_Area__");
	var bSplitAreaMustBeFilled = Sys.DD.GetParameter("Split_AreaMustBeFilled__") === "1";

	var bAnalysisOk = false;

	var computeSplittingRangesCallback = Sys.Helpers.TryGetFunction("Lib.DD.Customization.Splitting.ComputeSplittingRanges");
	if (computeSplittingRangesCallback !== false)
	{
		computeSplittingRangesCallback();
		bAnalysisOk = Lib.DD.Splitting.error.length === 0;
	}
	else if (sSplitType === "SIMPLE")
	{
		bAnalysisOk = Lib.DD.Splitting.ApplyOffset(iSplitOffset, iPageCount);
	}
	else if (sSplitType === "NPAGES")
	{
		bAnalysisOk = Lib.DD.Splitting.SplitEveryNPages(iSplitOffset, iPageCount, iDocLength);
	}
	else if (sSplitType === "ONSTRING")
	{
		bAnalysisOk = Lib.DD.Splitting.SplitStringPage(iSplitOffset, iPageCount, sSplitString, bCaseSensitive, bSplitBefore, sArea, bRegExpUse);
	}
	else if (sSplitType === "ONAREA")
	{
		bAnalysisOk = Lib.DD.Splitting.SplitAreaChange(iSplitOffset, iPageCount, sArea, bCaseSensitive, bRegExpUse, sSplitString, bSplitAreaMustBeFilled);
	}
	else
	{
		Lib.DD.Splitting.error = Language.Translate("_Invalid split method");
		Lib.DD.Splitting.details = "";
	}

	return bAnalysisOk;
}

function Main()
{
	try
	{
		Data.SetValue("ConfigurationName__", Sys.DD.GetParameter("ConfigurationName"));
	}
	catch (e)
	{
		// (-1 because MaxRetryCount is MaxTry in fact)
		if (Process.GetScriptRetryCount() < Process.GetScriptMaxRetryCount() - 1)
		{
			Variable.SetValueAsString("ConfigurationNotFound", "1");
			if (Data.GetValue("IsInteractive"))
			{
				Process.PreventApproval();
			}
			else
			{
				throw e;
			}
		}
		else
		{
			Data.SetValue("State", 200);
			Data.SetValue("StatusCode", -1);
			Data.SetValue("ShortStatus", Language.Translate("_Configuration not found"));
			return;
		}
	}

	switch ((Attach.GetExtension(0) || "").toLowerCase())
	{
		case ".zip":
			Variable.SetValueAsString("ZIP_Attached__", "1");
			//Force Documents_count__ to one since we only support one document per zip for now.
			SetSplitSuccess(1);
			ExtractAndAttachPDFForPreview();
			break;
		case ".xml":
			Variable.SetValueAsString("XML_Attached__", "1");
			//Force Documents_count__ to one since we only support one document per xml for now.
			SetSplitSuccess(1);
			break;
		default:
			InitializeSplitting();
			break;
	}

	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Extraction.FinalizeSplittingExtraction");
}

function ExtractAndAttachPDFForPreview()
{
	if (Attach.GetNbAttach() === 1)
	{
		var compressedFile = Attach.GetCompressedFile(0);
		if (compressedFile)
		{
			var entriesContainer = { entries: compressedFile.GetEntries(), xmlHandled: false, pdfHandled: false };
			for (var i = 0; i < entriesContainer.entries.length; i++)
			{
				HandleEntry(entriesContainer, i);
			}

			if (!entriesContainer.xmlHandled)
			{
				throw "No xml found in zip file";
			}
		}
	}
}

function HandleEntry(entriesContainer, i)
{
	var entry = entriesContainer.entries[i];
	var index = Attach.GetNbAttach();
	var fileName = entry.GetFileName();
	var extension = (entry.GetExtension() || "").toLowerCase();
	if (extension === ".pdf")
	{
		if (entriesContainer.pdfHandled)
		{
			throw "Multiple pdf found in zip file";
		}
		entriesContainer.pdfHandled = true;
		var isAttached = Attach.AttachTemporaryFile(entry.GetFile(), { name: fileName, attachAsConverted: true });
		if (!isAttached)
		{
			throw Language.Translate("_Couldn't attach the generated file");
		}
		Data.SetValue("AttachmentPreviewIndex", index);

	}
	else if (extension === ".xml")
	{
		if (entriesContainer.xmlHandled)
		{
			throw "Multiple xml found in zip file";
		}
		entriesContainer.xmlHandled = true;
	}
}

function SetSplitSuccess(nbDocument)
{
	Variable.SetValueAsString("Split_status__", "SUCCESS");
	Data.SetValue("Split_success__", 1);
	Data.SetValue("Documents_count__", nbDocument);
}

Main();

