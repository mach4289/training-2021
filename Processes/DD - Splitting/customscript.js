///#GLOBALS Lib Sys
var g_otherPanes =
	[
		Controls.Process_data,
		Controls.Splitting_result_pane,
		Controls.Toggle_button_bottom_pane
	];

// Panes toggleable by the show more/less button (see function SetPaneToggelable)
var g_toggeledPanes = [];
var g_toggeledPaneDisplayed = false;

function InitFormControls(splitInvalid)
{
	// First show all form controls
	Lib.DD_Client.ShowAllControls();

	// Set all g_otherPanes panes toggelable
	for (var i = 0; i < g_otherPanes.length; i++)
	{
		SetPaneToggelable(g_otherPanes[i], true);
	}
	// Init the two "Show more/less" buttons panes
	InitToggleButtonPanes();
	DisplayToggeledPanes(splitInvalid);

	if (!ProcessInstance.state)
	{
		InitFirstScreen();
	}
	else
	{
		InitFinalScreen();
	}
}

function InitFirstScreen()
{
	Controls.HeaderTitle__.SetText(Language.Translate("_HeaderTitleBeforeProcessing"));
	var html = "<img src=\"";
	html += Process.GetImageURL("arrow_splitting.png");
	html += "\">";
	Controls.Marketing__.SetHTML(html);
	Controls.Action_Send__.Hide();
	Controls.Informations_pane.Hide();
	Controls.Explanation_pane.Hide(false);
	Controls.Toggle_button_top_pane.Hide();
}

function InitFinalScreen()
{
	Controls.HeaderTitle__.SetText(Language.Translate("_HeaderTitleAfterProcessing"));
	Controls.Informations_pane.Hide(false);
	Controls.Explanation_pane.Hide();
	Controls.Toggle_button_top_pane.Hide(false);

	if (Variable.GetValueAsString("ConfigurationNotFound") === "1")
	{
		Controls.ConfigurationName__.SetError(Language.Translate("_ConfigurationNotFound"));
		Controls.Action_Send__.Hide();
	}
	else
	{
		Controls.Action_Send__.Hide(false);
	}
}
Controls.DocumentsPanel.OnDocumentSelected = function()
{
	Controls.HeaderTitle__.SetText(Language.Translate("_HeaderTitleAfterProcessing"));
	Controls.Explanation_pane.Hide();
	Controls.Action_Send__.Hide(false);
	Controls.Informations_pane.Hide(false);
};

// --------------------
// Toggeled panes management
// --------------------

// Init the two "Show more/less" buttons panes
function InitToggleButtonPanes()
{
	Controls.Button_Show_More_Less__.OnClick = ToggleButtonsOnClickCallbacks;
	Controls.Button_Show_More_Less_bottom__.OnClick = ToggleButtonsOnClickCallbacks;
}

// Set the pane togglable, that means it will be shown by the "Show more/less" buttons.
// If setToggelable is set to false, then it will never be shown up anymore.
function SetPaneToggelable(control, setToggelable)
{
	var controlIndex = g_toggeledPanes.indexOf(control);
	var isInArray = controlIndex !== -1;

	if (setToggelable && !isInArray)
	{
		// We want it toggelable but it's not in the array, let's add it
		g_toggeledPanes.push(control);
	}

	if (!setToggelable && isInArray)
	{
		// We don't want it toggelable but it's in the array, let's remove it
		g_toggeledPanes.splice(controlIndex, 1);
	}
}

// Toggles the panes
function ToggleButtonsOnClickCallbacks()
{
	g_toggeledPaneDisplayed = !g_toggeledPaneDisplayed;
	DisplayToggeledPanes(g_toggeledPaneDisplayed);
}

// Show or hide toggeled panes according to display argument
function DisplayToggeledPanes(display)
{
	// Show or hide every array's controls
	g_toggeledPanes.map(function (control)
	{
		control.Hide(!display);
	}
	);

	// Toggle buttons labels and images
	if (display)
	{
		// Expanded form
		var showLessTranslated = Language.Translate("_Show less details");
		Controls.Button_Show_More_Less__.SetIconURL("show_less.png");
		Controls.Button_Show_More_Less_bottom__.SetIconURL("show_less.png");
		Controls.Button_Show_More_Less__.SetButtonLabel(showLessTranslated);
		Controls.Button_Show_More_Less_bottom__.SetButtonLabel(showLessTranslated);

	}
	else
	{
		// Compacted form
		var showMoreTranslated = Language.Translate("_Show more details");
		Controls.Button_Show_More_Less__.SetIconURL("show_more.png");
		Controls.Button_Show_More_Less_bottom__.SetIconURL("show_more.png");
		Controls.Button_Show_More_Less__.SetButtonLabel(showMoreTranslated);
		Controls.Button_Show_More_Less_bottom__.SetButtonLabel(showMoreTranslated);
	}
}

function Main()
{
	var splitStatus = Variable.GetValueAsString("Split_status__");
	var splitValid = splitStatus === "SUCCESS";
	var splitInvalid = splitStatus !== null && !splitValid;

	InitFormControls(splitInvalid);

	var zipAttached = Variable.GetValueAsString("ZIP_Attached__");
	var xmlAttached = Variable.GetValueAsString("XML_Attached__");

	//If a zip is attached we hide the pages count
	if (zipAttached || xmlAttached)
	{
		Controls.Page_count__.Hide();
	}

	// Form status
	Controls.Status__.SetText(Data.GetValue("StateTranslated"));
	if (splitInvalid || ProcessInstance.state === 200)
	{
		Controls.Status__.AddStyle("text-highlight-warning");
		var shortStatus = Data.GetValue("ShortStatusTranslated");
		if (shortStatus && Data.GetValue("ForwardSuccess") === "0")
		{
			Controls.Short_Status__.SetText(shortStatus);
			Controls.Short_Status__.Hide(false);
		}
		else
		{
			Controls.Short_Status__.Hide(true);
		}
	}

	// Update splitting status
	if (splitValid)
	{
		Controls.SplittingStatus__.SetText(Language.Translate("_Success"));
	}
	else if (splitInvalid)
	{
		Controls.SplittingStatus__.SetText(splitStatus);
		Controls.SplittingStatus__.AddStyle("text-highlight-warning");
	}

	Controls.Time_submit__.SetText(Data.GetValue("SubmitDateTime"));
	Controls.Time_completion__.SetText(Data.GetValue("CompletionDateTime"));
	Controls.Split_success__.Hide();
}

Main();

Sys.Helpers.TryCallFunction("Lib.DD.Customization.HTMLScripts.CustomizeSplitting");
