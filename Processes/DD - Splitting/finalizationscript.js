///#GLOBALS Lib Sys

var childrenProcessName = Variable.GetValueAsString("a_NextProcessName");

var Tools = {
	_nbAttach: null,
	_processDocIndex: null,
	GetNbAttach: function ()
	{
		if (!this._nbAttach)
		{
			this._nbAttach = Attach.GetNbAttach();
		}

		return this._nbAttach;
	},
	GetProcessDocumentIndex: function ()
	{
		if (!this._processDocIndex)
		{
			this._processDocIndex = 0;
			while (this._processDocIndex < this.GetNbAttach() && !Attach.IsProcessedDocument(this._processDocIndex))
			{
				this._processDocIndex++;
			}
		}
		return this._processDocIndex;
	}
};

var NotificationManager = {
	enableErrorNotif: true,
	notifOption: null,
	Init: function ()
	{
		this.enableErrorNotif = Sys.DD.GetParameter("EnableErrorNotification");
		if (this.enableErrorNotif)
		{
			var sender = Users.GetUserAsProcessAdmin(Data.GetValue("OwnerId"));
			if (!sender)
			{
				Log.Error("Could not retrieve sender. Notification on error could not be initialized.");
				return null;
			}

			var senderVars = sender.GetVars();
			var ownerId = Data.GetValue("OwnerId");
			var destAddress = senderVars.GetValue_String("EmailAddress", 0);
			destAddress = Sys.DD.ComputeRedirectEmailAddress("Redirect_sender_notifications", destAddress);
			this.notifOptions = {
				NotifyTemplateFile: "DD_ErrorNotification.htm",
				NotifyTemplateSkin: senderVars.GetValue_String("Skin", 0),
				NotifyTemplateOwnerID: ownerId,
				NotifyFilter: "(state=200)",
				NotifyAddressType: "SM",
				NotifyAddress: destAddress,
				NotifyIncludeAttachments: "0",
				NotifyEachTime: "1",
				NotifyTriggerName: "OnStateChanged",
				OwnerID: ownerId,
				FromLanguage: senderVars.GetValue_String("Language", 0),
				FromCulture: senderVars.GetValue_String("Culture", 0),
				FromTimeZone: senderVars.GetValue_String("TimeZoneIndex", 0),
				FromAddress: "notification@eskerondemand.com",
				FromName: "Notification Service Esker on Demand",
				Identifier: "ErrorNotification"
			};
		}

		return this;
	},
	AddErrorNotifictaion: function (transport)
	{
		if (this.enableErrorNotif && this.notifOptions)
		{
			var notif = transport.AddNotif(9);
			if (notif)
			{
				var notifVars = notif.GetVars();
				Sys.Helpers.Object.ForEach(this.notifOptions, function (optValue, opt)
				{
					notifVars.AddValue_String(opt, optValue, true);
				}, this);
			}
			else
			{
				Log.Error("Could not create error notification.");
			}
		}
	}
};

function AddBillingAccountToFromAccount(billingAccount)
{
	var fromAccount = Data.GetValue("FromAccount");

	var lastIndex = fromAccount.lastIndexOf("/");
	var partBefore = fromAccount.substring(0, lastIndex);
	var partAfter = fromAccount.substring(lastIndex + 1);

	return partBefore + "/" + billingAccount + "/" + partAfter;
}

function SetBillingInfo(childrenTransport)
{
	var billingAccount = Sys.DD.GetParameter("Billing_Info_Billing_Account");
	var uninheritedVars = childrenTransport.GetUninheritedVars();
	var externalVars = childrenTransport.GetExternalVars();

	if (billingAccount)
	{
		var updatedFromAccount = AddBillingAccountToFromAccount(billingAccount);
		uninheritedVars.AddValue_String("FromAccount", updatedFromAccount, true);
		// Add it to external vars too in order to be used in case of resend
		externalVars.AddValue_String("FromAccount", updatedFromAccount, true);
	}

	var costCenter = Sys.DD.GetParameter("Billing_Info_Cost_Center");
	if (costCenter)
	{
		uninheritedVars.AddValue_String("CostCenter", costCenter, true);
		// Add it to external vars too in order to be used in case of resend
		externalVars.AddValue_String("CostCenter", costCenter, true);
	}
}

var SplitManager = {

	//Initialized document variable for performance
	timeOutOffSet: 600,
	//Recalculate the timeOut every n jobs produce
	timeOutInterval: 150,
	notifMgr: null,

	Init: function ()
	{
		this.notifMgr = NotificationManager.Init();
	},

	Start: function ()
	{
		var dateStart = new Date();
		var nbSplitRange = 1;
		try
		{
			nbSplitRange = parseInt(Variable.GetValueAsString("SplitParams_NbRange"), 10);
		}
		catch (e) { }

		for (var i = 0; i < nbSplitRange; i++)
		{
			Lib.DD.Splitting.DeserializeSplitParameters(Variable.GetValueAsString("SplitParams_Range_" + i));
		}

		var splits = Lib.DD.Splitting.GetSplitParameters();
		var nbSplit = splits.length;

		var documentsCount = Data.GetValue("Documents_count__");
		if (documentsCount !== nbSplit)
		{
			Data.SetValue("State", 200);
			Data.SetValue("StatusCode", -1);
			Data.SetValue("ShortStatus", Language.Translate("_Error Wrong number documents generated"));
			return;
		}

		for (var index = 0; index < nbSplit; index++)
		{
			//Put off the time out
			if (index !== 0 && !(index % this.timeOutInterval))
			{
				Process.SetTimeOut(this.timeOutOffSet + ((new Date().getTime() - dateStart.getTime()) * nbSplit) / (index * 1000));
			}

			var childrenSenderForm = Process.CreateProcessInstance(childrenProcessName, true);
			this.AddAttachments(childrenSenderForm, splits[index].startPage, splits[index].endPage);
			this.notifMgr.AddErrorNotifictaion(childrenSenderForm);
			this.AddValues(childrenSenderForm);

			var context = {
				splits: splits,
				currentSplitIndex: index
			};

			SetBillingInfo(childrenSenderForm);
			Sys.Helpers.TryCallFunction("Lib.DD.Customization.Splitting.FinalizeSenderFormProcess", childrenSenderForm, context);
			childrenSenderForm.Process();
		}
	},

	AddAttachments: function (transport, splitFrom, splitTo)
	{
		var transportAttach;
		var transportAttachVars;
		var processDocIndex = Tools.GetProcessDocumentIndex();
		var nbAttach = Tools.GetNbAttach();
		var forwardAdditionalAttach = Sys.DD.GetParameter("ForwardAdditionalAttach");
		var documentType = Sys.DD.GetParameter("Document_Type");
		for (var i = 0; i < nbAttach; i++)
		{
			if (processDocIndex === i)
			{
				var attachFile = Document.ExtractPages(splitFrom - 1, splitTo - 1);
				transportAttach = transport.AddAttachEx(attachFile);
				transportAttachVars = transportAttach.GetVars();

				var outputFilenamePattern = Sys.DD.GetParameter("OutputFilenamePattern");

				if (!outputFilenamePattern)
				{
					var attachmentName = Attach.GetName(i);
					transportAttachVars.AddValue_String("AttachOutputName", attachmentName, true);
				}

				transportAttachVars.AddValue_String("AttachToProcess", "1", true);
				transportAttachVars.AddValue_String("AttachToDisplay", "converted", true);
				transportAttachVars.AddValue_String("DocumentType", documentType, true);
			}
			else if (forwardAdditionalAttach)
			{
				transportAttach = transport.AddAttachEx(Attach.GetConvertedFile(i) || Attach.GetInputFile(i));
				transportAttachVars = transportAttach.GetVars();
				transportAttachVars.AddValue_String("AttachManagement", "REF_COPY", true);
			}
		}
	},

	AddValues: function (transport)
	{
		var extVars = transport.GetExternalVars();
		extVars.AddValue_String(Sys.DD.configurationCacheVarName, Variable.GetValueAsString(Sys.DD.configurationCacheVarName), true);
		var vars = transport.GetUninheritedVars();
		vars.AddValue_String("ExtractionConfiguration", Sys.DD.GetParameter("ConfigurationName"), true);
	}
};

var ZipManager = {
	notifMgr: null,

	Init: function ()
	{
		this.notifMgr = NotificationManager.Init();
	},

	Start: function ()
	{
		var compressedFile = Attach.GetCompressedFile(0);
		if (!compressedFile)
		{
			throw "First attachment is missing or it's not a compressed file";
		}

		var childrenSenderForm = Process.CreateProcessInstance(childrenProcessName, true);

		var entries = compressedFile.GetEntries();
		var xmlHandled = false;
		var pdfHandled = false;
		var forwardAdditionalAttach = Sys.DD.GetParameter("ForwardAdditionalAttach");
		var xmlFile;
		var pdfFile;
		var otherFiles = [];
		for (var i = 0; i < entries.length; i++)
		{
			var entry = entries[i];

			var extension = (entry.GetExtension() || "").toLowerCase();
			if (extension === ".pdf")
			{
				if (pdfHandled)
				{
					throw "Multiple pdf found in zip file";
				}
				pdfHandled = true;
				pdfFile = entry.GetFile();
			}
			else if (extension === ".xml")
			{
				if (xmlHandled)
				{
					throw "Multiple xml found in zip file";
				}
				xmlHandled = true;
				xmlFile = entry.GetFile();
			}
			else if (forwardAdditionalAttach)
			{
				otherFiles.push(entry.GetFile());
			}
		}

		var transportAttach;
		var transportAttachVars;

		if (pdfHandled)
		{
			transportAttach = childrenSenderForm.AddAttachEx(pdfFile);
			transportAttachVars = transportAttach.GetVars();
			transportAttachVars.AddValue_String("AttachToProcess", "1", true);
			transportAttachVars.AddValue_String("AttachToDisplay", "converted", true);
			transportAttachVars.AddValue_String("AttachOutputName", "MainAttach", true);
			transportAttachVars.AddValue_String("DocumentType", "MainAttach", true);
		}

		if (xmlHandled)
		{
			transportAttach = childrenSenderForm.AddAttachEx(xmlFile);
			transportAttachVars = transportAttach.GetVars();
			transportAttachVars.AddValue_String("XMLToProcess", "1", true);
		}

		for (var idx = 0; idx < otherFiles.length; idx++)
		{
			transportAttach = childrenSenderForm.AddAttachEx(otherFiles[idx]);
			transportAttachVars = transportAttach.GetVars();
			transportAttachVars.AddValue_String("AttachManagement", "REF_COPY", true);
		}

		this.notifMgr.AddErrorNotifictaion(childrenSenderForm);

		SplitManager.AddValues(childrenSenderForm);

		var context = {
			splits: null,
			currentSplitIndex: -1
		};

		SetBillingInfo(childrenSenderForm);
		Sys.Helpers.TryCallFunction("Lib.DD.Customization.Splitting.FinalizeSenderFormProcess", childrenSenderForm, context);
		childrenSenderForm.Process();
	}
};

var XmlManager = {
	notifMgr: null,

	Init: function ()
	{
		this.notifMgr = NotificationManager.Init();
	},

	Start: function ()
	{
		var xmlFile = Attach.GetInputFile(0);

		if (!xmlFile || xmlFile.GetExtension().toLowerCase() !== ".xml")
		{
			throw "First attachment is missing or it's not an XML file";
		}

		var childrenSenderForm = Process.CreateProcessInstance(childrenProcessName, true);
		var xmlHandled = false;
		var forwardAdditionalAttach = Sys.DD.GetParameter("ForwardAdditionalAttach");
		var otherFiles = [];
		for (var i = 0; i < Attach.GetNbAttach(); i++)
		{
			var extension = (Attach.GetInputFile(i).GetExtension() || "").toLowerCase();
			if (extension === ".xml")
			{
				if (xmlHandled)
				{
					throw "Multiple XML attachements found";
				}
				xmlHandled = true;
				xmlFile = Attach.GetAttach(i);
			}
			else if (forwardAdditionalAttach)
			{
				otherFiles.push(Attach.GetAttach(i));
			}
		}

		var transportAttach;
		var transportAttachVars;
		if (xmlHandled)
		{
			transportAttach = childrenSenderForm.AddAttachEx(xmlFile);
			transportAttachVars = transportAttach.GetVars();
			transportAttachVars.AddValue_String("XMLToProcess", "1", true);
		}

		for (var idx = 0; idx < otherFiles.length; idx++)
		{
			transportAttach = childrenSenderForm.AddAttachEx(otherFiles[idx]);
			transportAttachVars = transportAttach.GetVars();
			transportAttachVars.AddValue_String("AttachManagement", "REF_COPY", true);
		}

		this.notifMgr.AddErrorNotifictaion(childrenSenderForm);

		SplitManager.AddValues(childrenSenderForm);

		var context = {
			splits: null,
			currentSplitIndex: -1
		};

		SetBillingInfo(childrenSenderForm);
		Sys.Helpers.TryCallFunction("Lib.DD.Customization.Splitting.FinalizeSenderFormProcess", childrenSenderForm, context);
		childrenSenderForm.Process();
	}
};

function Main()
{
	if (Variable.GetValueAsString("ZIP_Attached__") === "1")
	{
		ZipManager.Init();
		ZipManager.Start();
	}
	else if (Variable.GetValueAsString("XML_Attached__") === "1")
	{
		XmlManager.Init();
		XmlManager.Start();
	}
	else
	{
		SplitManager.Init();
		SplitManager.Start();
	}

	Sys.Helpers.TryCallFunction("Lib.DD.Customization.Deliveries.FinalizeSplittingGeneration");
}

Main();
