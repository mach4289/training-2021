{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data",
														"hidden": true
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 0,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {},
																		"colspans": []
																	},
																	"data": [
																		"fields"
																	],
																	"*": {},
																	"stamp": 185
																}
															},
															"stamp": 184,
															"data": []
														}
													},
													"stamp": 183,
													"data": []
												},
												"GeneralPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": "200",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_GeneralPanel",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"LabelCompanyCode__": "CompanyCode__",
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelName__": "Name__",
																	"Name__": "LabelName__",
																	"LabelDescription__": "Description__",
																	"Description__": "LabelDescription__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelName__": {
																				"line": 2,
																				"column": 1
																			},
																			"Name__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelDescription__": {
																				"line": 3,
																				"column": 1
																			},
																			"Description__": {
																				"line": 3,
																				"column": 2
																			},
																			"Spacer_line__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 186
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 187
																		},
																		"LabelName__": {
																			"type": "Label",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"label": "_TypeName",
																				"version": 0
																			},
																			"stamp": 188
																		},
																		"Name__": {
																			"type": "ShortText",
																			"data": [
																				"Name__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TypeName",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 189
																		},
																		"LabelDescription__": {
																			"type": "Label",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"label": "_TypeDescription",
																				"version": 0
																			},
																			"stamp": 190
																		},
																		"Description__": {
																			"type": "ShortText",
																			"data": [
																				"Description__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_TypeDescription",
																				"activable": true,
																				"width": "300",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false,
																				"autocompletable": false,
																				"length": 120
																			},
																			"stamp": 191
																		},
																		"Spacer_line__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line",
																				"version": 0
																			},
																			"stamp": 250
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										},
										"form-content-center-2": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 22,
											"*": {
												"SpecificPropertiesPanel": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "200",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Specific properties",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 369,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"VendorFieldBehaviour__": "LabelVendorFieldBehaviour__",
																	"LabelVendorFieldBehaviour__": "VendorFieldBehaviour__",
																	"CommentTemplate__": "LabelCommentTemplate__",
																	"LabelCommentTemplate__": "CommentTemplate__",
																	"Template__": "LabelTemplate__",
																	"LabelTemplate__": "Template__",
																	"BillableFieldBehaviour__": "LabelBillableFieldBehaviour__",
																	"LabelBillableFieldBehaviour__": "BillableFieldBehaviour__",
																	"CostCenterFieldBehaviour__": "LabelCostCenterFieldBehaviour__",
																	"LabelCostCenterFieldBehaviour__": "CostCenterFieldBehaviour__",
																	"ReceiptBehaviour__": "LabelReceiptBehaviour__",
																	"LabelReceiptBehaviour__": "ReceiptBehaviour__"
																},
																"version": 0
															},
															"stamp": 370,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"VendorFieldBehaviour__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelVendorFieldBehaviour__": {
																				"line": 3,
																				"column": 1
																			},
																			"CommentTemplate__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelCommentTemplate__": {
																				"line": 6,
																				"column": 1
																			},
																			"Template__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelTemplate__": {
																				"line": 1,
																				"column": 1
																			},
																			"BillableFieldBehaviour__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelBillableFieldBehaviour__": {
																				"line": 5,
																				"column": 1
																			},
																			"CostCenterFieldBehaviour__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelCostCenterFieldBehaviour__": {
																				"line": 4,
																				"column": 1
																			},
																			"ReceiptBehaviour__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelReceiptBehaviour__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 6,
																		"columns": 2,
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 371,
																	"*": {
																		"LabelTemplate__": {
																			"type": "Label",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"label": "_Template",
																				"version": 0
																			},
																			"stamp": 453
																		},
																		"Template__": {
																			"type": "ComboBox",
																			"data": [
																				"Template__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Standard",
																					"1": "_Distance"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "Standard",
																					"1": "Distance"
																				},
																				"label": "_Template",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 454
																		},
																		"LabelReceiptBehaviour__": {
																			"type": "Label",
																			"data": [
																				"ReceiptBehaviour__"
																			],
																			"options": {
																				"label": "_Receipt",
																				"version": 0
																			},
																			"stamp": 467
																		},
																		"ReceiptBehaviour__": {
																			"type": "ComboBox",
																			"data": [
																				"ReceiptBehaviour__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_ReiceptOptional",
																					"1": "_ReiceptRequired"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "Optional",
																					"1": "Required"
																				},
																				"label": "_Receipt",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 468
																		},
																		"LabelVendorFieldBehaviour__": {
																			"type": "Label",
																			"data": [
																				"VendorFieldBehaviour__"
																			],
																			"options": {
																				"label": "_Vendor",
																				"version": 0
																			},
																			"stamp": 380
																		},
																		"VendorFieldBehaviour__": {
																			"type": "ComboBox",
																			"data": [
																				"VendorFieldBehaviour__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Hidden",
																					"1": "_Optional",
																					"2": "_Required"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "Hidden",
																					"1": "Optional",
																					"2": "Required"
																				},
																				"label": "_Vendor",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 381
																		},
																		"LabelCostCenterFieldBehaviour__": {
																			"type": "Label",
																			"data": [
																				"CostCenterFieldBehaviour__"
																			],
																			"options": {
																				"label": "_CostCenter",
																				"version": 0
																			},
																			"stamp": 459
																		},
																		"CostCenterFieldBehaviour__": {
																			"type": "ComboBox",
																			"data": [
																				"CostCenterFieldBehaviour__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Hidden",
																					"1": "_Visible"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "Hidden",
																					"1": "Required"
																				},
																				"label": "_CostCenter",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 460
																		},
																		"LabelBillableFieldBehaviour__": {
																			"type": "Label",
																			"data": [
																				"BillableFieldBehaviour__"
																			],
																			"options": {
																				"label": "_Billable",
																				"version": 0
																			},
																			"stamp": 457
																		},
																		"BillableFieldBehaviour__": {
																			"type": "ComboBox",
																			"data": [
																				"BillableFieldBehaviour__"
																			],
																			"options": {
																				"possibleValues": {
																					"0": "_Hidden",
																					"1": "_Visible"
																				},
																				"version": 1,
																				"keyValueMode": true,
																				"possibleKeys": {
																					"0": "Hidden",
																					"1": "Required"
																				},
																				"label": "_Billable",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"DataSourceOrigin": "Standalone",
																				"DisplayedColumns": ""
																			},
																			"stamp": 458
																		},
																		"LabelCommentTemplate__": {
																			"type": "Label",
																			"data": [
																				"CommentTemplate__"
																			],
																			"options": {
																				"label": "_CommentTemplate",
																				"version": 0
																			},
																			"stamp": 449
																		},
																		"CommentTemplate__": {
																			"type": "LongText",
																			"data": [
																				"CommentTemplate__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"minNbLines": 3,
																				"label": "_CommentTemplate",
																				"activable": true,
																				"width": "300",
																				"helpText": "_HelperInputCommentTemplate",
																				"helpURL": "4013",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 450
																		}
																	}
																}
															}
														}
													}
												},
												"Analytics": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "200",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Analytics",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 23,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"DefaultGLAccount__": "LabelDefaultGLAccount__",
																	"LabelDefaultGLAccount__": "DefaultGLAccount__"
																},
																"version": 0
															},
															"stamp": 24,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"DefaultGLAccount__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelDefaultGLAccount__": {
																				"line": 1,
																				"column": 1
																			},
																			"Spacer_line2__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 25,
																	"*": {
																		"LabelDefaultGLAccount__": {
																			"type": "Label",
																			"data": [
																				"DefaultGLAccount__"
																			],
																			"options": {
																				"label": "_Default G/L account",
																				"version": 0
																			},
																			"stamp": 26
																		},
																		"DefaultGLAccount__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"DefaultGLAccount__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_Default G/L account",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"length": 30,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 27
																		},
																		"Spacer_line2__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "",
																				"width": "",
																				"color": "default",
																				"label": "Spacer line2",
																				"version": 0
																			},
																			"stamp": 251
																		}
																	}
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-6": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 378,
											"*": {
												"VAT": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "200",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VAT",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 340,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"TaxCode2__": "LabelTaxCode2__",
																	"LabelTaxCode2__": "TaxCode2__",
																	"TaxCode1__": "LabelTaxCode1__",
																	"LabelTaxCode1__": "TaxCode1__",
																	"TaxCode3__": "LabelTaxCode3__",
																	"LabelTaxCode3__": "TaxCode3__",
																	"TaxCode4__": "LabelTaxCode4__",
																	"LabelTaxCode4__": "TaxCode4__",
																	"TaxCode5__": "LabelTaxCode5__",
																	"LabelTaxCode5__": "TaxCode5__",
																	"InputTaxRate__": "LabelInputTaxRate__",
																	"LabelInputTaxRate__": "InputTaxRate__",
																	"TaxRate1__": "LabelTaxRate1__",
																	"LabelTaxRate1__": "TaxRate1__",
																	"TaxRate2__": "LabelTaxRate2__",
																	"LabelTaxRate2__": "TaxRate2__",
																	"TaxRate3__": "LabelTaxRate3__",
																	"LabelTaxRate3__": "TaxRate3__",
																	"TaxRate4__": "LabelTaxRate4__",
																	"LabelTaxRate4__": "TaxRate4__",
																	"TaxRate5__": "LabelTaxRate5__",
																	"LabelTaxRate5__": "TaxRate5__"
																},
																"version": 0
															},
															"stamp": 341,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"TaxCode2__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelTaxCode2__": {
																				"line": 5,
																				"column": 1
																			},
																			"TaxCode1__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelTaxCode1__": {
																				"line": 3,
																				"column": 1
																			},
																			"TaxCode3__": {
																				"line": 7,
																				"column": 2
																			},
																			"LabelTaxCode3__": {
																				"line": 7,
																				"column": 1
																			},
																			"TaxCode4__": {
																				"line": 9,
																				"column": 2
																			},
																			"LabelTaxCode4__": {
																				"line": 9,
																				"column": 1
																			},
																			"TaxCode5__": {
																				"line": 11,
																				"column": 2
																			},
																			"LabelTaxCode5__": {
																				"line": 11,
																				"column": 1
																			},
																			"InputTaxRate__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelInputTaxRate__": {
																				"line": 1,
																				"column": 1
																			},
																			"TaxRate1__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelTaxRate1__": {
																				"line": 4,
																				"column": 1
																			},
																			"TaxRate2__": {
																				"line": 6,
																				"column": 2
																			},
																			"LabelTaxRate2__": {
																				"line": 6,
																				"column": 1
																			},
																			"TaxRate3__": {
																				"line": 8,
																				"column": 2
																			},
																			"LabelTaxRate3__": {
																				"line": 8,
																				"column": 1
																			},
																			"TaxRate4__": {
																				"line": 10,
																				"column": 2
																			},
																			"LabelTaxRate4__": {
																				"line": 10,
																				"column": 1
																			},
																			"TaxRate5__": {
																				"line": 12,
																				"column": 2
																			},
																			"LabelTaxRate5__": {
																				"line": 12,
																				"column": 1
																			},
																			"HelperInputTaxRate__": {
																				"line": 2,
																				"column": 1
																			}
																		},
																		"lines": 12,
																		"columns": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[],
																			[]
																		],
																		"version": 0
																	},
																	"stamp": 342,
																	"*": {
																		"LabelInputTaxRate__": {
																			"type": "Label",
																			"data": [
																				"InputTaxRate__"
																			],
																			"options": {
																				"label": "_InputTaxRate",
																				"version": 0
																			},
																			"stamp": 343
																		},
																		"InputTaxRate__": {
																			"type": "CheckBox",
																			"data": [
																				"InputTaxRate__"
																			],
																			"options": {
																				"label": "_InputTaxRate",
																				"activable": true,
																				"width": 230,
																				"helpText": "_HelperInputTaxRate",
																				"helpURL": "4012",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 344
																		},
																		"HelperInputTaxRate__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "color1",
																				"backgroundcolor": "default",
																				"image_url": "",
																				"label": "",
																				"version": 0,
																				"iconClass": ""
																			},
																			"stamp": 345
																		},
																		"LabelTaxCode1__": {
																			"type": "Label",
																			"data": [
																				"TaxCode1__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 346
																		},
																		"TaxCode1__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode1__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 347
																		},
																		"LabelTaxRate1__": {
																			"type": "Label",
																			"data": [
																				"TaxRate1__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 348
																		},
																		"TaxRate1__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate1__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 349
																		},
																		"LabelTaxCode2__": {
																			"type": "Label",
																			"data": [
																				"TaxCode2__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 350
																		},
																		"TaxCode2__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode2__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 351
																		},
																		"LabelTaxRate2__": {
																			"type": "Label",
																			"data": [
																				"TaxRate2__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 352
																		},
																		"TaxRate2__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate2__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 353
																		},
																		"LabelTaxCode3__": {
																			"type": "Label",
																			"data": [
																				"TaxCode3__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 354
																		},
																		"TaxCode3__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode3__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 355
																		},
																		"LabelTaxRate3__": {
																			"type": "Label",
																			"data": [
																				"TaxRate3__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 356
																		},
																		"TaxRate3__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate3__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 357
																		},
																		"LabelTaxCode4__": {
																			"type": "Label",
																			"data": [
																				"TaxCode4__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 358
																		},
																		"TaxCode4__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode4__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 359
																		},
																		"LabelTaxRate4__": {
																			"type": "Label",
																			"data": [
																				"TaxRate4__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 360
																		},
																		"TaxRate4__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate4__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 361
																		},
																		"LabelTaxCode5__": {
																			"type": "Label",
																			"data": [
																				"TaxCode5__"
																			],
																			"options": {
																				"label": "_TaxCode",
																				"version": 0
																			},
																			"stamp": 362
																		},
																		"TaxCode5__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"TaxCode5__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_TaxCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches"
																			},
																			"stamp": 363
																		},
																		"LabelTaxRate5__": {
																			"type": "Label",
																			"data": [
																				"TaxRate5__"
																			],
																			"options": {
																				"label": "_TaxRate",
																				"version": 0
																			},
																			"stamp": 364
																		},
																		"TaxRate5__": {
																			"type": "Decimal",
																			"data": [
																				"TaxRate5__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_TaxRate",
																				"precision_internal": 2,
																				"precision_current": 2,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"readonly": true
																			},
																			"stamp": 365
																		}
																	}
																}
															}
														}
													}
												},
												"Blank": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Blank",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"hidden": false
													},
													"stamp": 261,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 262,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {},
																		"lines": 0,
																		"columns": 2,
																		"colspans": [],
																		"version": 0
																	},
																	"stamp": 263
																}
															}
														}
													}
												}
											}
										},
										"form-content-center-3": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 280,
											"*": {
												"VATAddRemoveButtons": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": "200",
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_VATAddRemoveButtons",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0
													},
													"stamp": 268,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"Add__": "LabelAdd__",
																	"LabelAdd__": "Add__"
																},
																"version": 0
															},
															"stamp": 269,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"Remove__": {
																				"line": 2,
																				"column": 1
																			},
																			"Add__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelAdd__": {
																				"line": 1,
																				"column": 1
																			}
																		},
																		"lines": 2,
																		"colspans": [
																			[],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 271,
																	"*": {
																		"Add__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_AddTaxField",
																				"label": "",
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "color1",
																				"nextprocess": {
																					"processName": "AP - Application Settings__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"width": "100",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 313
																		},
																		"Remove__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_RemoveTaxField",
																				"label": "Remove",
																				"textPosition": "text-right",
																				"textSize": "MS",
																				"textStyle": "default",
																				"textColor": "color1",
																				"nextprocess": {
																					"processName": "AP - Application Settings__",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"width": "100",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 311
																		},
																		"LabelAdd__": {
																			"type": "Label",
																			"data": false,
																			"options": {
																				"label": "",
																				"version": 0
																			},
																			"stamp": 314
																		}
																	}
																}
															}
														}
													}
												},
												"Blank2": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Blank",
														"leftImageURL": "",
														"removeMargins": false,
														"elementsAlignment": "left",
														"version": 0,
														"hidden": false
													},
													"stamp": 407,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {},
																"version": 0
															},
															"stamp": 408,
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {},
																		"lines": 0,
																		"columns": 2,
																		"colspans": [],
																		"version": 0
																	},
																	"stamp": 409
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"PreSave": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Save",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings__",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 245
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 468,
	"data": []
}