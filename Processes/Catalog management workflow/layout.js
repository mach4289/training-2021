{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"splitterOptions": {
					"sizeable": false,
					"sidebarTopHeight": 130,
					"sidebarLeftWidth": 1024,
					"unit": "px",
					"fixedSlider": true
				},
				"version": 0,
				"maxwidth": 1024,
				"style": "FlexibleFormLight",
				"backColor": "text-backgroundcolor-default"
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": true,
						"hideDesignButton": true,
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "17%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-top-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 4,
											"*": {
												"WizardStepsPane": {
													"type": "PanelData",
													"data": [],
													"options": {
														"border": {},
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "M",
														"hideActionButtons": true,
														"hideTitleBar": true,
														"hideTitle": true,
														"backgroundcolor": "color1",
														"labelsAlignment": "center",
														"label": "_WizardStepsPane",
														"hidden": false,
														"leftImageURL": "",
														"removeMargins": true,
														"elementsAlignment": "center",
														"version": 0
													},
													"stamp": 5,
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"data": [],
															"options": {
																"controlsAssociation": {
																	"LabelGeneralStepButton__": "ImportStepButton__",
																	"LabelPreviewStepButton__": "OperationStepButton__",
																	"WorkflowStepButton__": "LabelWorkflowStepButton__",
																	"LabelWorkflowStepButton__": "WorkflowStepButton__",
																	"SummaryStepButton__": "LabelSummaryStepButton__",
																	"LabelSummaryStepButton__": "SummaryStepButton__",
																	"OperationStepButton__": "LabelPreviewStepButton__",
																	"ImportStepButton__": "LabelGeneralStepButton__"
																},
																"version": 0
															},
															"stamp": 6,
															"*": {
																"Grid": {
																	"type": "HorizontalLayout",
																	"data": [
																		"fields"
																	],
																	"options": {
																		"ctrlsPos": {
																			"WizardTopSpacer__": {
																				"line": 1,
																				"column": 1
																			},
																			"ImportStepButton__": {
																				"line": 2,
																				"column": 1
																			},
																			"Spacer2__": {
																				"line": 3,
																				"column": 1
																			},
																			"WorkflowStepButton__": {
																				"line": 4,
																				"column": 1
																			},
																			"Spacer3__": {
																				"line": 5,
																				"column": 1
																			},
																			"SummaryStepButton__": {
																				"line": 6,
																				"column": 1
																			},
																			"WizardBottomSpacer__": {
																				"line": 7,
																				"column": 1
																			}
																		},
																		"lines": 7,
																		"colspans": [
																			[
																				[
																					{
																						"index": 0,
																						"length": 2
																					}
																				]
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				[
																					{
																						"index": 0,
																						"length": 2
																					}
																				]
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			],
																			[
																				{
																					"index": 0,
																					"length": 2
																				}
																			]
																		],
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"WizardTopSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "15",
																				"width": "100%",
																				"color": "default",
																				"label": "_WizardTopSpacer",
																				"version": 0
																			},
																			"stamp": 8
																		},
																		"ImportStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_ImportStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color8",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-cloud-upload fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 9
																		},
																		"WizardBottomSpacer__": {
																			"type": "Spacer",
																			"data": false,
																			"options": {
																				"height": "15",
																				"width": "100%",
																				"color": "default",
																				"label": "_WizardBottomSpacer",
																				"version": 0
																			},
																			"stamp": 10
																		},
																		"Spacer2__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 11
																		},
																		"WorkflowStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_WorkflowStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-check-square-o fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 12
																		},
																		"Spacer3__": {
																			"type": "Text",
																			"data": false,
																			"options": {
																				"marge": 0,
																				"textSize": "XL",
																				"textAlignment": "center",
																				"textStyle": "bold",
																				"textColor": "color6",
																				"backgroundcolor": "default",
																				"iconClass": "",
																				"label": "___",
																				"version": 0
																			},
																			"stamp": 13
																		},
																		"SummaryStepButton__": {
																			"type": "FormButton",
																			"data": false,
																			"options": {
																				"buttonLabel": "_SummaryStepButton",
																				"label": "_Button",
																				"textPosition": "text-below",
																				"textSize": "S",
																				"textStyle": "default",
																				"textColor": "color9",
																				"nextprocess": {
																					"processName": "AP - Application Settings Wizard",
																					"attachmentsMode": "all",
																					"willBeChild": true,
																					"returnToOriginalUrl": false
																				},
																				"urlImageOverlay": "",
																				"awesomeClasses": "fa fa-flag-checkered fa-3x",
																				"style": 5,
																				"width": "100%",
																				"action": "none",
																				"url": "",
																				"version": 0
																			},
																			"stamp": 14
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									},
									"stamp": 15,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": 17,
											"left": 0,
											"width": 100,
											"height": 83
										},
										"hidden": false
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"GeneralInformationsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "230",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"hideTitle": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_GeneralInformationsPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 17,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"Status__": "LabelStatus__",
																			"LabelStatus__": "Status__",
																			"SubmissionDateTime__": "LabelSubmissionDateTime__",
																			"LabelSubmissionDateTime__": "SubmissionDateTime__",
																			"RequesterLogin__": "LabelRequesterLogin__",
																			"LabelRequesterLogin__": "RequesterLogin__",
																			"RequesterName__": "LabelRequesterName__",
																			"LabelRequesterName__": "RequesterName__",
																			"Reason__": "LabelReason__",
																			"LabelReason__": "Reason__",
																			"ImportDescription__": "LabelImportDescription__",
																			"LabelImportDescription__": "ImportDescription__",
																			"CompanyCode__": "LabelCompanyCode__",
																			"LabelCompanyCode__": "CompanyCode__",
																			"VendorNumber__": "LabelVendorNumber__",
																			"LabelVendorNumber__": "VendorNumber__",
																			"VendorName__": "LabelVendorName__",
																			"LabelVendorName__": "VendorName__",
																			"ValidationDateTime__": "LabelValidationDateTime__",
																			"LabelValidationDateTime__": "ValidationDateTime__",
																			"IsInternalUpdateRequest__": "LabelIsInternalUpdateRequest__",
																			"LabelIsInternalUpdateRequest__": "IsInternalUpdateRequest__",
																			"Revert__": "LabelRevert__",
																			"LabelRevert__": "Revert__"
																		},
																		"version": 0
																	},
																	"stamp": 18,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Status__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelStatus__": {
																						"line": 2,
																						"column": 1
																					},
																					"SubmissionDateTime__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelSubmissionDateTime__": {
																						"line": 6,
																						"column": 1
																					},
																					"RequesterLogin__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelRequesterLogin__": {
																						"line": 4,
																						"column": 1
																					},
																					"RequesterName__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelRequesterName__": {
																						"line": 5,
																						"column": 1
																					},
																					"Reason__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelReason__": {
																						"line": 3,
																						"column": 1
																					},
																					"ImportDescription__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelImportDescription__": {
																						"line": 11,
																						"column": 1
																					},
																					"CompanyCode__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelCompanyCode__": {
																						"line": 8,
																						"column": 1
																					},
																					"VendorNumber__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelVendorNumber__": {
																						"line": 9,
																						"column": 1
																					},
																					"VendorName__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelVendorName__": {
																						"line": 10,
																						"column": 1
																					},
																					"ValidationDateTime__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelValidationDateTime__": {
																						"line": 7,
																						"column": 1
																					},
																					"IsInternalUpdateRequest__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelIsInternalUpdateRequest__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 11,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 19,
																			"*": {
																				"LabelIsInternalUpdateRequest__": {
																					"type": "Label",
																					"data": [
																						"IsInternalUpdateRequest__"
																					],
																					"options": {
																						"label": "_IsInternalUpdateRequest",
																						"hidden": true
																					},
																					"stamp": 20
																				},
																				"IsInternalUpdateRequest__": {
																					"type": "CheckBox",
																					"data": [
																						"IsInternalUpdateRequest__"
																					],
																					"options": {
																						"label": "_IsInternalUpdateRequest",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"hidden": true,
																						"readonly": true
																					},
																					"stamp": 21
																				},
																				"LabelStatus__": {
																					"type": "Label",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"label": "_Status",
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"Status__": {
																					"type": "ComboBox",
																					"data": [
																						"Status__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Draft",
																							"1": "_ToApprove",
																							"2": "_Approved",
																							"3": "_Rejected"
																						},
																						"version": 1,
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Draft",
																							"1": "ToApprove",
																							"2": "Approved",
																							"3": "Rejected"
																						},
																						"label": "_Status",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"DataSourceOrigin": "Standalone",
																						"DisplayedColumns": "",
																						"readonly": true
																					},
																					"stamp": 23
																				},
																				"LabelReason__": {
																					"type": "Label",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"label": "_Reason",
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"Reason__": {
																					"type": "LongText",
																					"data": [
																						"Reason__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 2,
																						"label": "_Reason",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 25
																				},
																				"LabelRequesterLogin__": {
																					"type": "Label",
																					"data": [
																						"RequesterLogin__"
																					],
																					"options": {
																						"label": "_RequesterLogin",
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"RequesterLogin__": {
																					"type": "ShortText",
																					"data": [
																						"RequesterLogin__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_RequesterLogin",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 27
																				},
																				"LabelRequesterName__": {
																					"type": "Label",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"label": "_RequesterName",
																						"version": 0
																					},
																					"stamp": 28
																				},
																				"RequesterName__": {
																					"type": "ShortText",
																					"data": [
																						"RequesterName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_RequesterName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 29
																				},
																				"LabelSubmissionDateTime__": {
																					"type": "Label",
																					"data": [
																						"SubmissionDateTime__"
																					],
																					"options": {
																						"label": "_SubmissionDateTime",
																						"version": 0
																					},
																					"stamp": 30
																				},
																				"SubmissionDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"SubmissionDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_SubmissionDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 31
																				},
																				"LabelValidationDateTime__": {
																					"type": "Label",
																					"data": [
																						"ValidationDateTime__"
																					],
																					"options": {
																						"label": "_ValidationDateTime",
																						"version": 0
																					},
																					"stamp": 32
																				},
																				"ValidationDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"ValidationDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_ValidationDateTime",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"version": 0
																					},
																					"stamp": 33
																				},
																				"LabelCompanyCode__": {
																					"type": "Label",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"label": "_CompanyCode",
																						"version": 0
																					},
																					"stamp": 34
																				},
																				"CompanyCode__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"CompanyCode__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"label": "_CompanyCode",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"fTSmaxRecords": "20",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 35
																				},
																				"LabelVendorNumber__": {
																					"type": "Label",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"label": "_VendorNumber",
																						"version": 0
																					},
																					"stamp": 36
																				},
																				"VendorNumber__": {
																					"type": "DatabaseComboBox",
																					"data": [
																						"VendorNumber__"
																					],
																					"options": {
																						"version": 1,
																						"autoCompleteSearchMode": "contains",
																						"searchMode": "contains",
																						"label": "_VendorNumber",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"minNbLines": 1,
																						"maxNbLines": 1,
																						"PreFillFTS": true,
																						"RestrictSearch": true,
																						"fTSmaxRecords": "20",
																						"browsable": false,
																						"readonly": true,
																						"autocompletable": false
																					},
																					"stamp": 37
																				},
																				"LabelVendorName__": {
																					"type": "Label",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"label": "_VendorName",
																						"version": 0
																					},
																					"stamp": 38
																				},
																				"VendorName__": {
																					"type": "ShortText",
																					"data": [
																						"VendorName__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"label": "_VendorName",
																						"activable": true,
																						"width": 230,
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 39
																				},
																				"LabelImportDescription__": {
																					"type": "Label",
																					"data": [
																						"ImportDescription__"
																					],
																					"options": {
																						"label": "_ImportDescription",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"ImportDescription__": {
																					"type": "LongText",
																					"data": [
																						"ImportDescription__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 2,
																						"label": "_ImportDescription",
																						"activable": true,
																						"width": "500",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 41
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-12": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 42,
													"*": {
														"AttachmentsPane": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Attachments",
																"hidden": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"downloadButton": true,
																		"previewButton": false
																	},
																	"stamp": 43
																}
															},
															"stamp": 44,
															"data": []
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {},
													"stamp": 45,
													"*": {
														"ParsingErrorsPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ParsingErrorsPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0,
																"labelLength": 0
															},
															"stamp": 46,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 47,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ParsingErrorItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 48,
																			"*": {
																				"ParsingErrorItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 3,
																						"displayEmptyLines": false,
																						"alwaysOneLine": true,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ErrorItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 49,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 50,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 51
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 52
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 53,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 54,
																									"data": [],
																									"*": {
																										"CSVLineNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CSVLineNumber",
																												"version": 0
																											},
																											"stamp": 55,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 56,
																									"data": [],
																									"*": {
																										"CSVLine__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CSVLine",
																												"version": 0
																											},
																											"stamp": 57,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 58,
																									"data": [],
																									"*": {
																										"ErrorStatus__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ErrorStatus",
																												"version": 0
																											},
																											"stamp": 59,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 60,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 61,
																									"data": [],
																									"*": {
																										"CSVLineNumber__": {
																											"type": "Integer",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": true,
																												"label": "_CSVLineNumber",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "70",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 62,
																											"position": [
																												"_Integer"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 63,
																									"data": [],
																									"*": {
																										"CSVLine__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_CSVLine",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 64,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 65,
																									"data": [],
																									"*": {
																										"ErrorStatus__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_ErrorStatus",
																												"activable": true,
																												"width": "350",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 66,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-11": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 67,
													"*": {
														"PreviewAddedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "885",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewAddedPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 68,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 69,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"AddedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 70,
																			"*": {
																				"AddedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 9,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_AddedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 71,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 72,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 73
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 74
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 75,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 76,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CompanyCode",
																												"version": 0
																											},
																											"stamp": 77,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 78,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_VendorNumber",
																												"version": 0
																											},
																											"stamp": 79,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 80,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 81,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 82,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 83,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 84,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 85,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 86,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 87,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 88,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 89,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 90,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 91,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 92,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 93,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 94,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 95,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CompanyCode",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 96,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 97,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_VendorNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 98,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 99,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 100,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 101,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 102,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 103,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 104,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 105,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 106,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 107,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 108,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 109,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "40",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 110,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 111,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 112,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 113,
													"*": {
														"PreviewModifiedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "885",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewModifiedPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 114,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 115,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ModifiedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 116,
																			"*": {
																				"ModifiedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 9,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_ModifiedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 117,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 118,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 119
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 120
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 121,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 122,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CompanyCode",
																												"version": 0
																											},
																											"stamp": 123,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 124,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_VendorNumber",
																												"version": 0
																											},
																											"stamp": 125,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 126,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 127,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 128,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 129,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 130,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 131,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 132,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 133,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 134,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 135,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 136,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 137,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 138,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 139,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 140,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 141,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CompanyCode",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 142,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 143,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_VendorNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 144,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 145,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 146,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 147,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 148,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 149,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 150,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 151,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 152,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 153,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 154,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 155,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 156,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 157,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 158,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 159,
													"*": {
														"PreviewDeletedPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "885",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_PreviewDeletedPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 160,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 161,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"DeletedItems__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 162,
																			"*": {
																				"DeletedItems__": {
																					"type": "Table",
																					"data": [],
																					"options": {
																						"version": 5,
																						"lines": 10,
																						"columns": 9,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": true,
																							"hideTableRowAdd": true,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": false
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_DeletedItems",
																						"readonly": true,
																						"notInDB": true,
																						"dataType": "Enumeration",
																						"subsection": null
																					},
																					"stamp": 163,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 164,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 165
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 166
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 167,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 168,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_CompanyCode",
																												"version": 0
																											},
																											"stamp": 169,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 170,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_VendorNumber",
																												"version": 0
																											},
																											"stamp": 171,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 172,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Number",
																												"version": 0
																											},
																											"stamp": 173,
																											"position": [
																												"Text"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 174,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Description",
																												"version": 0
																											},
																											"stamp": 175,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 176,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_ManufacturerName",
																												"version": 0
																											},
																											"stamp": 177,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 178,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitPrice",
																												"version": 0
																											},
																											"stamp": 179,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 180,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_Currency",
																												"version": 0
																											},
																											"stamp": 181,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 182,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UnitOfMeasure",
																												"version": 0
																											},
																											"stamp": 183,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 184,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "Label",
																											"data": [],
																											"options": {
																												"label": "_UNSPSC",
																												"version": 0
																											},
																											"stamp": 185,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 186,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 187,
																									"data": [],
																									"*": {
																										"CompanyCode__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_CompanyCode",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 188,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 189,
																									"data": [],
																									"*": {
																										"VendorNumber__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_VendorNumber",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 190,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 191,
																									"data": [],
																									"*": {
																										"Number__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Number",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 192,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 193,
																									"data": [],
																									"*": {
																										"Description__": {
																											"type": "LongText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_Description",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 999,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false
																											},
																											"stamp": 194,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 195,
																									"data": [],
																									"*": {
																										"ManufacturerName__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_ManufacturerName",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 196,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 197,
																									"data": [],
																									"*": {
																										"UnitPrice__": {
																											"type": "Decimal",
																											"data": [],
																											"options": {
																												"version": 2,
																												"textSize": "S",
																												"textAlignment": "right",
																												"textStyle": "default",
																												"textColor": "default",
																												"integer": false,
																												"label": "_UnitPrice",
																												"precision_internal": 2,
																												"precision_current": 2,
																												"activable": true,
																												"width": "80",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"enablePlusMinus": false,
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "Number",
																												"autocompletable": false
																											},
																											"stamp": 198,
																											"position": [
																												"_Decimal"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 199,
																									"data": [],
																									"*": {
																										"Currency__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_Currency",
																												"activable": true,
																												"width": "60",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 200,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 201,
																									"data": [],
																									"*": {
																										"UnitOfMeasure__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UnitOfMeasure",
																												"activable": true,
																												"width": "50",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 202,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell9": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 203,
																									"data": [],
																									"*": {
																										"UNSPSC__": {
																											"type": "ShortText",
																											"data": [],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_UNSPSC",
																												"activable": true,
																												"width": "100",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"notInDB": true,
																												"dataType": "String",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 204,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-17": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 205,
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Next Process"
															},
															"stamp": 206,
															"data": []
														}
													}
												},
												"form-content-left-16": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 207,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {
																	"collapsed": true
																},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 208,
															"data": []
														}
													}
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 209,
													"*": {
														"WorkflowPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": "200",
																"iconURL": "",
																"TitleFontSize": "L",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"hideTitle": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_WorkflowPane",
																"hidden": false,
																"leftImageURL": "",
																"removeMargins": false,
																"elementsAlignment": "left",
																"version": 0
															},
															"stamp": 210,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"DatabaseComboBox__": "Label_DatabaseComboBox",
																			"Label_DatabaseComboBox": "DatabaseComboBox__",
																			"LastValidatorUserID__": "LabelLastValidatorUserID__",
																			"LabelLastValidatorUserID__": "LastValidatorUserID__",
																			"ComputingWorkflow__": "LabelComputingWorkflow__",
																			"LabelComputingWorkflow__": "ComputingWorkflow__",
																			"ApprovedDate__": "LabelApprovedDate__",
																			"LabelApprovedDate__": "ApprovedDate__",
																			"LastValidatorName__": "LabelLastValidatorName__",
																			"LabelLastValidatorName__": "LastValidatorName__",
																			"Comments__": "LabelComments__",
																			"LabelComments__": "Comments__"
																		},
																		"version": 0
																	},
																	"stamp": 211,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"WorkflowTable__": {
																						"line": 4,
																						"column": 1
																					},
																					"Comments__": {
																						"line": 2,
																						"column": 1
																					},
																					"Spacer__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer4__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 212,
																			"*": {
																				"Spacer4__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer4",
																						"version": 0
																					},
																					"stamp": 213
																				},
																				"Comments__": {
																					"type": "LongText",
																					"data": [
																						"Comments__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"version": 1,
																						"minNbLines": 2,
																						"label": "_Comments",
																						"activable": true,
																						"width": "100%",
																						"helpText": "",
																						"helpURL": "",
																						"helpFormat": "HTML Format",
																						"numberOfLines": 999,
																						"browsable": false
																					},
																					"stamp": 214
																				},
																				"Spacer__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "_Spacer",
																						"version": 0
																					},
																					"stamp": 215
																				},
																				"WorkflowTable__": {
																					"type": "Table",
																					"data": [
																						"WorkflowTable__"
																					],
																					"options": {
																						"version": 5,
																						"lines": 100,
																						"columns": 8,
																						"displayEmptyLines": false,
																						"alwaysOneLine": false,
																						"menuOptions": {
																							"disableInsertLine": true,
																							"disableAddLine": true,
																							"hideTopNavigation": true,
																							"hideBottomNavigation": true,
																							"hideTableRowMenu": true,
																							"hideTableRowDelete": false,
																							"hideTableRowAdd": false,
																							"hideTableRowDuplicate": true,
																							"addButtonShouldInsert": true
																						},
																						"item": {
																							"display": null
																						},
																						"group": {
																							"name": "GDR\nPage"
																						},
																						"label": "_WorkflowTable",
																						"readonly": true,
																						"subsection": null
																					},
																					"stamp": 216,
																					"*": {
																						"Actions": {
																							"type": "$TableActions",
																							"subtype": "Actions",
																							"options": {
																								"version": 0
																							},
																							"stamp": 217,
																							"*": {
																								"Top": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 218
																								},
																								"Bottom": {
																									"type": "$TableActions",
																									"subtype": "Top",
																									"options": {
																										"version": 0
																									},
																									"stamp": 219
																								}
																							}
																						},
																						"Header": {
																							"type": "$TableRow",
																							"subtype": "Header",
																							"options": {
																								"version": 0
																							},
																							"stamp": 220,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 221,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "Label",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"label": "",
																												"version": 0
																											},
																											"stamp": 222,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 223,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "Label",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"label": "_WRKFUserName",
																												"version": 0
																											},
																											"stamp": 224,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 225,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "Label",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"label": "_WRKFRole",
																												"version": 0
																											},
																											"stamp": 226,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 227,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "Label",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"label": "_WRKFDate",
																												"version": 0
																											},
																											"stamp": 228,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 229,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "Label",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"label": "_WRKFComment",
																												"version": 0
																											},
																											"stamp": 230,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 231,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "Label",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"label": "_WRKFAction",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 232,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 233,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "Label",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"label": "_WRKFIndex",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 234,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 235,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "Label",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"label": "_WRKFIsGroup",
																												"hidden": true,
																												"version": 0
																											},
																											"stamp": 236,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						},
																						"Clipboard": {
																							"type": "$TableRow",
																							"subtype": "Clipboard",
																							"options": {
																								"version": 0
																							},
																							"stamp": 237,
																							"data": [
																								null
																							],
																							"*": {
																								"Cell1": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 238,
																									"data": [],
																									"*": {
																										"WRKFMarker__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFMarker__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "",
																												"activable": true,
																												"width": "0",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 239,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell2": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 240,
																									"data": [],
																									"*": {
																										"WRKFUserName__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFUserName__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFUserName",
																												"activable": true,
																												"width": "180",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 241,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell3": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 242,
																									"data": [],
																									"*": {
																										"WRKFRole__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFRole__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFRole",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"browsable": false
																											},
																											"stamp": 243,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell4": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 244,
																									"data": [],
																									"*": {
																										"WRKFDate__": {
																											"type": "RealDateTime",
																											"data": [
																												"WRKFDate__"
																											],
																											"options": {
																												"readonly": true,
																												"displayLongFormat": false,
																												"label": "_WRKFDate",
																												"activable": true,
																												"width": "150",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"version": 0
																											},
																											"stamp": 245,
																											"position": [
																												"_RealDateTime"
																											]
																										}
																									}
																								},
																								"Cell5": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"version": 0
																									},
																									"stamp": 246,
																									"data": [],
																									"*": {
																										"WRKFComment__": {
																											"type": "LongText",
																											"data": [
																												"WRKFComment__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"minNbLines": 1,
																												"label": "_WRKFComment",
																												"activable": true,
																												"width": "300",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"numberOfLines": 5,
																												"browsable": false
																											},
																											"stamp": 247,
																											"position": [
																												"_LongText"
																											]
																										}
																									}
																								},
																								"Cell6": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 248,
																									"data": [],
																									"*": {
																										"WRKFAction__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFAction__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFAction",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 249,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell7": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 250,
																									"data": [],
																									"*": {
																										"Workflow_index__": {
																											"type": "ShortText",
																											"data": [
																												"Workflow_index__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFIndex",
																												"activable": true,
																												"width": "90",
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false
																											},
																											"stamp": 251,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								},
																								"Cell8": {
																									"type": "$TableCell",
																									"subtype": "Cell",
																									"options": {
																										"hidden": true,
																										"version": 0
																									},
																									"stamp": 252,
																									"data": [],
																									"*": {
																										"WRKFIsGroup__": {
																											"type": "ShortText",
																											"data": [
																												"WRKFIsGroup__"
																											],
																											"options": {
																												"textSize": "S",
																												"textAlignment": "left",
																												"textStyle": "default",
																												"textColor": "default",
																												"version": 1,
																												"label": "_WRKFIsGroup",
																												"activable": true,
																												"width": 140,
																												"helpText": "",
																												"helpURL": "",
																												"helpFormat": "HTML Format",
																												"hidden": true,
																												"readonly": true,
																												"browsable": false,
																												"autocompletable": false
																											},
																											"stamp": 253,
																											"position": [
																												"_ShortText"
																											]
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 254,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": 0,
													"left": 45,
													"width": 55,
													"height": 100
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"hideTitle": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 255
																}
															},
															"stamp": 256,
															"data": []
														}
													},
													"stamp": 257,
													"data": []
												}
											},
											"stamp": 258,
											"data": []
										}
									},
									"stamp": 259,
									"data": []
								}
							},
							"stamp": 260,
							"data": []
						}
					},
					"stamp": 261,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 262,
							"data": []
						},
						"Submit": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Submit",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 1,
								"url": "",
								"position": null,
								"action": "approve",
								"version": 0
							},
							"stamp": 263
						},
						"Reject": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Reject",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"style": 3,
								"url": "",
								"position": null,
								"action": "reject",
								"version": 0
							},
							"stamp": 264
						},
						"Revert": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Revert",
								"textPosition": "text-right",
								"textSize": "MS",
								"textStyle": "default",
								"textColor": "color1",
								"nextprocess": {
									"processName": "AP - Application Settings Wizard",
									"attachmentsMode": "all",
									"willBeChild": true,
									"returnToOriginalUrl": false
								},
								"urlImageOverlay": "",
								"url": "",
								"position": null,
								"action": "none",
								"style": 2,
								"version": 0
							},
							"stamp": 265
						}
					},
					"stamp": 266,
					"data": []
				}
			},
			"stamp": 267,
			"data": []
		}
	},
	"stamps": 268,
	"data": []
}