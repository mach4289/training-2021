///#GLOBALS Lib Sys
var validationscript;
(function (validationscript) {
    var actionName = Data.GetActionName();
    var actionType = Data.GetActionType();
    Log.Info("-- Catalog Management Workflow Validation Script -- Name: '" + (actionName ? actionName : "<empty>") + "', Action: '" + (actionType ? actionType : "<empty>") + "' , Device: '" + Data.GetActionDevice());
    var wkf = Lib.Purchasing.CM.Workflow;
    function InitWorkflow() {
        Log.Info("Initializing workflow");
        wkf.controller.Define(wkf.parameters);
        wkf.controller.AllowRebuild(false);
        Log.Info("Initialization of workflow done");
    }
    InitWorkflow();
    // Defines what must be done depending on requested action.
    if (!actionName && !actionType) {
    }
    else if (actionType === "approve_asynchronous" || actionType === "approve" || actionType === "ResumeWithAction") {
        if (actionName == "save") {
            // first call of the validation script when a user clicks an action button.
            // nothing todo here because the XGF isn't saved after execution.
        }
        else if (actionName == "saveBeforeSubmit") {
            Process.RecallScript("Submit", true);
        }
        else if (actionName == "Submit") {
            var idx = wkf.controller.GetContributorIndex();
            var currentContributor = wkf.controller.GetContributorAt(idx);
            Log.Info("Do action " + currentContributor.action + " for " + currentContributor.name);
            wkf.controller.DoAction(currentContributor.action);
        }
        else if (actionName == "Reject") {
            wkf.controller.DoAction("reject");
        }
        else {
            // The exception is caught by the flexible framework.
            // The exception prevents the document from being approved.
            throw "Unknown action '" + actionName + "'";
        }
    }
})(validationscript || (validationscript = {}));
