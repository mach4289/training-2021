///#GLOBALS Lib
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
var wkf = Lib.Purchasing.CM.Workflow;
var vendorLogin = Lib.P2P.GetValidatorOrOwner().GetValue("AccountId") + "$" + Data.GetValue("RequesterLogin__");
Log.Info("-- Catalog Management Workflow Extraction Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "' , Device: '" + Data.GetActionDevice());
function UpdateParentProcess(action, data) {
    var transport = Process.GetUpdatableTransportAsProcessAdmin(Variable.GetValueAsString("CMRUIDEX"));
    var externalVars = transport.GetExternalVars();
    if (data) {
        var resumeWithActionData = JSON.stringify(data);
        externalVars.AddValue_String("resumeWithActionData", resumeWithActionData, true);
    }
    transport.ResumeWithAction(action);
}
function SendEmailNotification(login, template, backupUserAsCC) {
    var options = {
        userId: login,
        template: template,
        fromName: "Esker Catalog Management",
        backupUserAsCC: !!backupUserAsCC,
        sendToAllMembersIfGroup: Sys.Parameters.GetInstance("P2P").GetParameter("SendNotificationsToEachGroupMembers") === "1"
    };
    //let doSendNotif = Sys.Helpers.TryCallFunction("Lib.PR.Customization.Server.OnSendEmailNotification", options);
    //if (doSendNotif !== false)
    {
        Sys.EmailNotification.SendEmailNotification(options);
    }
}
function InitWorkflow() {
    Log.Info("Initializing workflow");
    wkf.controller.Define(wkf.parameters);
    wkf.controller.SetRolesSequence(["requester", "approver"]);
    wkf.controller.AllowRebuild(wkf.controller.GetContributorIndex() == 0);
    Log.Info("Initialization of workflow done");
    // Verify there are no workflow errors
    if (!Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest() && Variable.GetValueAsString("MissingWorkflowRuleError") == "true") {
        // The following sends a notification to the admin, who is the same as the process owner
        SendEmailNotification(Data.GetValue("OwnerID"), "CM-CatalogManager_MissingWorkflowRules.htm", true);
        SendEmailNotification(vendorLogin, "CM-Vendor_ImportFail_MissingWorkflowRules.htm", true);
        UpdateParentProcess("MissingWorkflowRules", { errorMsg: Variable.GetValueAsString("WorkflowErrorMessage") });
        // Put the process in error state 200
        Data.SetValue("Status__", "Failed");
        Data.SetValue("State", 200);
    }
}
function PerformFirstWorkflowStep() {
    var currentContributor = wkf.controller.GetContributorAt(0);
    Log.Info("Do action " + currentContributor.action + " for " + currentContributor.name);
    wkf.controller.DoAction(currentContributor.action);
}
InitWorkflow();
if (Lib.Purchasing.CM.Workflow.IsInternalUpdateRequest()) {
    Lib.Purchasing.CM.PrepareImportJson(true)
        .Then(function (extractedData) {
        Lib.Purchasing.CM.SetExternalData(extractedData);
    });
    if (Lib.Purchasing.CM.IsCMSubmitable()) {
        Variable.SetValueAsString("IsCMSubmitable", "true");
    }
    else {
        Variable.SetValueAsString("IsCMSubmitable", "false");
    }
}
else {
    // Set submission date time from timestamp
    // AddValue_Date sends the date with the server timezone not UTC, but still puts utc=1 in the process CD
    // So the CM process adds an external variable to the CMW instance to pass through the submission date's timestamp
    var submissionDate = new Date(parseInt(Variable.GetValueAsString("SubmissionTime"), 10));
    Data.SetValue("SubmissionDateTime__", submissionDate);
    PerformFirstWorkflowStep();
}
