{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"ID__": "LabelID__",
																	"LabelID__": "ID__",
																	"VehicleType__": "LabelVehicleType__",
																	"LabelVehicleType__": "VehicleType__",
																	"ReimbursmentRate1__": "LabelReimbursmentRate1__",
																	"LabelReimbursmentRate1__": "ReimbursmentRate1__",
																	"EffectiveDate__": "LabelEffectiveDate__",
																	"LabelEffectiveDate__": "EffectiveDate__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 5,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"ID__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelID__": {
																				"line": 2,
																				"column": 1
																			},
																			"VehicleType__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelVehicleType__": {
																				"line": 3,
																				"column": 1
																			},
																			"ReimbursmentRate1__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelReimbursmentRate1__": {
																				"line": 4,
																				"column": 1
																			},
																			"EffectiveDate__": {
																				"line": 5,
																				"column": 2
																			},
																			"LabelEffectiveDate__": {
																				"line": 5,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode",
																				"version": 0
																			},
																			"stamp": 14
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"fTSmaxRecords": "20",
																				"browsable": true,
																				"PreFillFTS": true
																			},
																			"stamp": 15
																		},
																		"LabelID__": {
																			"type": "Label",
																			"data": [
																				"ID__"
																			],
																			"options": {
																				"label": "_ExpenseMileageID",
																				"version": 0
																			},
																			"stamp": 16
																		},
																		"ID__": {
																			"type": "ShortText",
																			"data": [
																				"ID__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_ExpenseMileageID",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelVehicleType__": {
																			"type": "Label",
																			"data": [
																				"VehicleType__"
																			],
																			"options": {
																				"label": "_VehicleType",
																				"version": 0
																			},
																			"stamp": 18
																		},
																		"VehicleType__": {
																			"type": "ShortText",
																			"data": [
																				"VehicleType__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_VehicleType",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"browsable": false
																			},
																			"stamp": 19
																		},
																		"LabelReimbursmentRate1__": {
																			"type": "Label",
																			"data": [
																				"ReimbursmentRate1__"
																			],
																			"options": {
																				"label": "_ReimbursmentRate1",
																				"version": 0
																			},
																			"stamp": 22
																		},
																		"ReimbursmentRate1__": {
																			"type": "Decimal",
																			"data": [
																				"ReimbursmentRate1__"
																			],
																			"options": {
																				"version": 2,
																				"textSize": "S",
																				"textAlignment": "right",
																				"textStyle": "default",
																				"textColor": "default",
																				"integer": false,
																				"label": "_ReimbursmentRate1",
																				"precision_internal": 3,
																				"precision_current": 3,
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format"
																			},
																			"stamp": 23
																		},
																		"LabelEffectiveDate__": {
																			"type": "Label",
																			"data": [
																				"EffectiveDate__"
																			],
																			"options": {
																				"label": "_EffectiveDate",
																				"version": 0
																			},
																			"stamp": 24
																		},
																		"EffectiveDate__": {
																			"type": "DateTime",
																			"data": [
																				"EffectiveDate__"
																			],
																			"options": {
																				"displayLongFormat": false,
																				"label": "_EffectiveDate",
																				"activable": true,
																				"width": 230,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"version": 0
																			},
																			"stamp": 25
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 25,
	"data": []
}