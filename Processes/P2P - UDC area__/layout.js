{
	"*": {
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "simple-v",
				"version": 0,
				"backColor": "text-backgroundcolor-default",
				"style": null
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"hidePanelsMenu": false,
						"version": 0
					},
					"stamp": 2,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-center": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": "100%"
										},
										"hidden": false
									},
									"*": {
										"form-content-center-1": {
											"type": "FlexibleFormLine",
											"data": [],
											"options": {
												"version": 0
											},
											"stamp": 9,
											"*": {
												"DataPanel": {
													"type": "PanelData",
													"options": {
														"border": {},
														"version": 0,
														"labelLength": 0,
														"iconURL": "",
														"TitleFontSize": "S",
														"hideActionButtons": false,
														"hideTitleBar": false,
														"hideTitle": false,
														"sameHeightAsSiblings": false,
														"backgroundcolor": "default",
														"labelsAlignment": "right",
														"label": "_Document data"
													},
													"*": {
														"FieldsManager": {
															"type": "FieldsManager",
															"options": {
																"controlsAssociation": {
																	"CompanyCode__": "LabelCompanyCode__",
																	"LabelCompanyCode__": "CompanyCode__",
																	"Keyword__": "LabelKeyword__",
																	"LabelKeyword__": "Keyword__",
																	"VendorNumber__": "LabelVendorNumber__",
																	"LabelVendorNumber__": "VendorNumber__",
																	"Area__": "LabelArea__",
																	"LabelArea__": "Area__"
																},
																"version": 0
															},
															"*": {
																"Grid": {
																	"type": "GridLayout",
																	"options": {
																		"lines": 4,
																		"columns": 2,
																		"version": 0,
																		"ctrlsPos": {
																			"CompanyCode__": {
																				"line": 1,
																				"column": 2
																			},
																			"LabelCompanyCode__": {
																				"line": 1,
																				"column": 1
																			},
																			"Keyword__": {
																				"line": 2,
																				"column": 2
																			},
																			"LabelKeyword__": {
																				"line": 2,
																				"column": 1
																			},
																			"VendorNumber__": {
																				"line": 3,
																				"column": 2
																			},
																			"LabelVendorNumber__": {
																				"line": 3,
																				"column": 1
																			},
																			"Area__": {
																				"line": 4,
																				"column": 2
																			},
																			"LabelArea__": {
																				"line": 4,
																				"column": 1
																			}
																		},
																		"colspans": [
																			[],
																			[],
																			[],
																			[]
																		]
																	},
																	"data": [
																		"fields"
																	],
																	"*": {
																		"LabelCompanyCode__": {
																			"type": "Label",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"label": "_CompanyCode"
																			},
																			"stamp": 14
																		},
																		"CompanyCode__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"CompanyCode__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_CompanyCode",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 15
																		},
																		"LabelKeyword__": {
																			"type": "Label",
																			"data": [
																				"Keyword__"
																			],
																			"options": {
																				"label": "_Keyword"
																			},
																			"stamp": 16
																		},
																		"Keyword__": {
																			"type": "ShortText",
																			"data": [
																				"Keyword__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Keyword",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 17
																		},
																		"LabelVendorNumber__": {
																			"type": "Label",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"label": "_VendorNumber"
																			},
																			"stamp": 18
																		},
																		"VendorNumber__": {
																			"type": "DatabaseComboBox",
																			"data": [
																				"VendorNumber__"
																			],
																			"options": {
																				"version": 1,
																				"autoCompleteSearchMode": "startswith",
																				"searchMode": "matches",
																				"label": "_VendorNumber",
																				"activable": true,
																				"width": "500",
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"minNbLines": 1,
																				"maxNbLines": 1,
																				"PreFillFTS": true,
																				"fTSmaxRecords": "20",
																				"browsable": true
																			},
																			"stamp": 19
																		},
																		"LabelArea__": {
																			"type": "Label",
																			"data": [
																				"Area__"
																			],
																			"options": {
																				"label": "_Area"
																			},
																			"stamp": 20
																		},
																		"Area__": {
																			"type": "ShortText",
																			"data": [
																				"Area__"
																			],
																			"options": {
																				"textSize": "S",
																				"textAlignment": "left",
																				"textStyle": "default",
																				"textColor": "default",
																				"version": 1,
																				"label": "_Area",
																				"activable": true,
																				"width": 500,
																				"helpText": "",
																				"helpURL": "",
																				"helpFormat": "HTML Format",
																				"helpIconPosition": "Right",
																				"browsable": false
																			},
																			"stamp": 21
																		}
																	},
																	"stamp": 8
																}
															},
															"stamp": 7,
															"data": []
														}
													},
													"stamp": 6,
													"data": []
												}
											}
										}
									},
									"stamp": 5,
									"data": []
								}
							},
							"stamp": 4,
							"data": []
						}
					},
					"stamp": 3,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0
					},
					"*": {
						"Save": {
							"type": "SubmitButton",
							"options": {
								"label": "_Save",
								"action": "savequit",
								"submit": true,
								"version": 0,
								"shortCut": "S"
							},
							"stamp": 11,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"shortCut": "Q"
							},
							"stamp": 12,
							"data": []
						},
						"Delete": {
							"type": "SubmitButton",
							"options": {
								"label": "_Delete",
								"action": "delete",
								"submit": true,
								"version": 0,
								"disabled": true
							},
							"stamp": 13,
							"data": []
						}
					},
					"stamp": 10,
					"data": []
				}
			},
			"stamp": 1,
			"data": []
		}
	},
	"stamps": 21,
	"data": []
}