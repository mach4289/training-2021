{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": 0,
											"left": 0,
											"width": 100,
											"height": 15
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 6,
															"data": []
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 7,
													"*": {
														"TitlePane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": " ",
																"leftImageURL": "",
																"version": 0
															},
															"stamp": 8,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 9,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"TitleExportedInvoices__": {
																						"line": 3,
																						"column": 1
																					},
																					"TitleExportInvoices__": {
																						"line": 1,
																						"column": 1
																					},
																					"SubTitleExportInvoices__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 10,
																			"*": {
																				"TitleExportInvoices__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_TitleExportInvoices",
																						"version": 0
																					},
																					"stamp": 11
																				},
																				"SubTitleExportInvoices__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_SubTitleExportInvoices",
																						"version": 0
																					},
																					"stamp": 12
																				},
																				"TitleExportedInvoices__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_TitleExportedInvoices",
																						"version": 0
																					},
																					"stamp": 13
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-9": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 14,
													"*": {
														"InvoicesToExport": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 400,
																"iconURL": "AP_InvoicesToExport.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_InvoicesToExport",
																"leftImageURL": "",
																"version": 0,
																"hidden": false
															},
															"stamp": 15,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"SkipInvoicesAlreadyExported__": "LabelSkipInvoicesAlreadyExported__",
																			"LabelSkipInvoicesAlreadyExported__": "SkipInvoicesAlreadyExported__"
																		},
																		"version": 0
																	},
																	"stamp": 16,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"InvoicesToExportExplanation__": {
																						"line": 1,
																						"column": 1
																					},
																					"SkipInvoicesAlreadyExported__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelSkipInvoicesAlreadyExported__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 17,
																			"*": {
																				"InvoicesToExportExplanation__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_DefaultExportModeExplanation",
																						"version": 0
																					},
																					"stamp": 18
																				},
																				"LabelSkipInvoicesAlreadyExported__": {
																					"type": "Label",
																					"data": [
																						"SkipInvoicesAlreadyExported__"
																					],
																					"options": {
																						"label": "_Skip_Invoices_Already_Exported",
																						"version": 0
																					},
																					"stamp": 19
																				},
																				"SkipInvoicesAlreadyExported__": {
																					"type": "CheckBox",
																					"data": [
																						"SkipInvoicesAlreadyExported__"
																					],
																					"options": {
																						"label": "_Skip_Invoices_Already_Exported",
																						"activable": true,
																						"defaultValue": true,
																						"version": 0
																					},
																					"stamp": 20
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"ERP": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 400,
																"iconURL": "AP_ERP_Details.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ERP",
																"leftImageURL": "",
																"hidden": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"Converter__": "LabelConverter__",
																			"LabelConverter__": "Converter__",
																			"Payment_release_output_format__": "LabelPayment_release_output_format__",
																			"LabelPayment_release_output_format__": "Payment_release_output_format__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 4,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"Spacer_line10__": {
																						"line": 1,
																						"column": 1
																					},
																					"Converter__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelConverter__": {
																						"line": 2,
																						"column": 1
																					},
																					"Payment_release_output_format__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPayment_release_output_format__": {
																						"line": 3,
																						"column": 1
																					},
																					"Spacer_line20__": {
																						"line": 4,
																						"column": 1
																					},
																					"Spacer_line1__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"Spacer_line1__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"LabelConverter__": {
																					"type": "Label",
																					"data": [
																						"Converter__"
																					],
																					"options": {
																						"label": "_Select the file format to use with your ERP",
																						"version": 0
																					},
																					"stamp": 22
																				},
																				"Converter__": {
																					"type": "ComboBox",
																					"data": [
																						"Converter__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_TRA_BUSINESS",
																							"1": "_TRA_EXPERT",
																							"2": "_Generic CSV",
																							"3": "_Generic XML",
																							"4": "_CSV_MSDynamicsAX",
																							"5": "_CSV_MSDynamicsNAV",
																							"6": "_CSV_OracleEBS",
																							"7": "_CSV_OracleJDE",
																							"8": "_CSV_OraclePeopleSoft",
																							"9": "_CSV_SAGE50",
																							"10": "_CSV_SAGE1000",
																							"11": "_CSV_SAP"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "TRA_BUSINESS",
																							"1": "TRA_EXPERT",
																							"2": "Generic CSV",
																							"3": "Generic XML",
																							"4": "CSV_MSDynamicsAX",
																							"5": "CSV_MSDynamicsNAV",
																							"6": "CSV_OracleEBS",
																							"7": "CSV_OracleJDE",
																							"8": "CSV_OraclePeopleSoft",
																							"9": "CSV_SAGE50",
																							"10": "CSV_SAGE1000",
																							"11": "CSV_SAP"
																						},
																						"label": "_Select the file format to use with your ERP",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"LabelPayment_release_output_format__": {
																					"type": "Label",
																					"data": [
																						"Payment_release_output_format__"
																					],
																					"options": {
																						"label": "_Payment release output format",
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"Payment_release_output_format__": {
																					"type": "ComboBox",
																					"data": [
																						"Payment_release_output_format__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "_Generic CSV"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Generic CSV"
																						},
																						"label": "_Payment release output format",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"Spacer_line20__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 26
																				}
																			},
																			"stamp": 27
																		}
																	},
																	"stamp": 28,
																	"data": []
																}
															},
															"stamp": 29,
															"data": []
														}
													},
													"stamp": 30,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Next Process"
															},
															"stamp": 31,
															"data": []
														}
													},
													"stamp": 32,
													"data": []
												},
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 33,
													"*": {
														"ExportedInvoices": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_ExportedInvoices.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"label": "_ExportedInvoices",
																"hidden": false
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 0,
																		"previewButton": false,
																		"downloadButton": true
																	},
																	"stamp": 34
																}
															},
															"stamp": 35,
															"data": []
														}
													}
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 36,
													"*": {
														"ExportInformation": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "AP_HeaderDataPanel.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ExportedInformations",
																"leftImageURL": "",
																"version": 0,
																"hidden": false
															},
															"stamp": 37,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"ExportDateTime__": "LabelExportDateTime__",
																			"LabelExportDateTime__": "ExportDateTime__",
																			"ExportStatus__": "LabelExportStatus__",
																			"LabelExportStatus__": "ExportStatus__",
																			"CompletedDateTime__": "LabelCompletedDateTime__",
																			"LabelCompletedDateTime__": "CompletedDateTime__",
																			"NumberOfExportedInvoices__": "LabelNumberOfExportedInvoices__",
																			"LabelNumberOfExportedInvoices__": "NumberOfExportedInvoices__",
																			"ExportedBy__": "LabelExportedBy__",
																			"LabelExportedBy__": "ExportedBy__",
																			"NumberOfInvoicesInError__": "LabelNumberOfInvoicesInError__",
																			"LabelNumberOfInvoicesInError__": "NumberOfInvoicesInError__",
																			"NumberOfExportedApprovedInvoices__": "LabelNumberOfExportedApprovedInvoices__",
																			"LabelNumberOfExportedApprovedInvoices__": "NumberOfExportedApprovedInvoices__",
																			"InvoicesToExportTempFile__": "LabelInvoicesToExportTempFile__",
																			"LabelInvoicesToExportTempFile__": "InvoicesToExportTempFile__",
																			"InvoicesApprovedTempFile__": "LabelInvoicesApprovedTempFile__",
																			"LabelInvoicesApprovedTempFile__": "InvoicesApprovedTempFile__",
																			"LastMsnEx__": "LabelLastMsnEx__",
																			"LabelLastMsnEx__": "LastMsnEx__",
																			"DateOfStartExport__": "LabelDateOfStartExport__",
																			"LabelDateOfStartExport__": "DateOfStartExport__"
																		},
																		"version": 0
																	},
																	"stamp": 38,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ExportDateTime__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelExportDateTime__": {
																						"line": 2,
																						"column": 1
																					},
																					"ExportStatus__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelExportStatus__": {
																						"line": 4,
																						"column": 1
																					},
																					"CompletedDateTime__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelCompletedDateTime__": {
																						"line": 3,
																						"column": 1
																					},
																					"NumberOfExportedInvoices__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelNumberOfExportedInvoices__": {
																						"line": 5,
																						"column": 1
																					},
																					"ExportedBy__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelExportedBy__": {
																						"line": 1,
																						"column": 1
																					},
																					"NumberOfInvoicesInError__": {
																						"line": 6,
																						"column": 2
																					},
																					"LabelNumberOfInvoicesInError__": {
																						"line": 6,
																						"column": 1
																					},
																					"NumberOfExportedApprovedInvoices__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelNumberOfExportedApprovedInvoices__": {
																						"line": 7,
																						"column": 1
																					},
																					"InvoicesToExportTempFile__": {
																						"line": 8,
																						"column": 2
																					},
																					"LabelInvoicesToExportTempFile__": {
																						"line": 8,
																						"column": 1
																					},
																					"InvoicesApprovedTempFile__": {
																						"line": 9,
																						"column": 2
																					},
																					"LabelInvoicesApprovedTempFile__": {
																						"line": 9,
																						"column": 1
																					},
																					"LastMsnEx__": {
																						"line": 10,
																						"column": 2
																					},
																					"LabelLastMsnEx__": {
																						"line": 10,
																						"column": 1
																					},
																					"DateOfStartExport__": {
																						"line": 11,
																						"column": 2
																					},
																					"LabelDateOfStartExport__": {
																						"line": 11,
																						"column": 1
																					}
																				},
																				"lines": 11,
																				"columns": 2,
																				"colspans": [
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[],
																					[]
																				],
																				"version": 0
																			},
																			"stamp": 39,
																			"*": {
																				"LabelExportedBy__": {
																					"type": "Label",
																					"data": [
																						"ExportedBy__"
																					],
																					"options": {
																						"label": "_ExportedBy",
																						"version": 0
																					},
																					"stamp": 40
																				},
																				"ExportedBy__": {
																					"type": "ShortText",
																					"data": [
																						"ExportedBy__"
																					],
																					"options": {
																						"readonly": true,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_ExportedBy",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"version": 0
																					},
																					"stamp": 41
																				},
																				"LabelExportDateTime__": {
																					"type": "Label",
																					"data": [
																						"ExportDateTime__"
																					],
																					"options": {
																						"label": "_ExportDateTime",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"ExportDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"ExportDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_ExportDateTime",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"LabelCompletedDateTime__": {
																					"type": "Label",
																					"data": [
																						"CompletedDateTime__"
																					],
																					"options": {
																						"label": "_CompletedDateTime",
																						"version": 0
																					},
																					"stamp": 44
																				},
																				"CompletedDateTime__": {
																					"type": "RealDateTime",
																					"data": [
																						"CompletedDateTime__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "_CompletedDateTime",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 45
																				},
																				"LabelExportStatus__": {
																					"type": "Label",
																					"data": [
																						"ExportStatus__"
																					],
																					"options": {
																						"label": "_ExportStatus",
																						"version": 0
																					},
																					"stamp": 46
																				},
																				"ExportStatus__": {
																					"type": "ComboBox",
																					"data": [
																						"ExportStatus__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "",
																							"1": "Pending",
																							"2": "In progress",
																							"3": "Completed",
																							"4": "Error",
																							"5": "Cancelled"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "",
																							"1": "Pending",
																							"2": "InProgress",
																							"3": "Completed",
																							"4": "Error",
																							"5": "Cancelled"
																						},
																						"label": "_ExportStatus",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"version": 0
																					},
																					"stamp": 47
																				},
																				"LabelNumberOfExportedInvoices__": {
																					"type": "Label",
																					"data": [
																						"NumberOfExportedInvoices__"
																					],
																					"options": {
																						"label": "_NumberOfExportedInvoices",
																						"version": 0
																					},
																					"stamp": 48
																				},
																				"NumberOfExportedInvoices__": {
																					"type": "Integer",
																					"data": [
																						"NumberOfExportedInvoices__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfExportedInvoices",
																						"precision_internal": 0,
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 49
																				},
																				"LabelNumberOfInvoicesInError__": {
																					"type": "Label",
																					"data": [
																						"NumberOfInvoicesInError__"
																					],
																					"options": {
																						"label": "_NumberOfInvoicesInError",
																						"version": 0
																					},
																					"stamp": 50
																				},
																				"NumberOfInvoicesInError__": {
																					"type": "Integer",
																					"data": [
																						"NumberOfInvoicesInError__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfInvoicesInError",
																						"precision_internal": 0,
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false
																					},
																					"stamp": 51
																				},
																				"LabelNumberOfExportedApprovedInvoices__": {
																					"type": "Label",
																					"data": [
																						"NumberOfExportedApprovedInvoices__"
																					],
																					"options": {
																						"label": "_NumberOfExportedApprovedInvoices",
																						"version": 0
																					},
																					"stamp": 52
																				},
																				"NumberOfExportedApprovedInvoices__": {
																					"type": "Integer",
																					"data": [
																						"NumberOfExportedApprovedInvoices__"
																					],
																					"options": {
																						"version": 1,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"integer": true,
																						"label": "_NumberOfExportedApprovedInvoices",
																						"precision_internal": 0,
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"readonly": true
																					},
																					"stamp": 53
																				},
																				"LabelInvoicesToExportTempFile__": {
																					"type": "Label",
																					"data": [
																						"InvoicesToExportTempFile__"
																					],
																					"options": {
																						"label": "InvoicesToExportTempFile",
																						"version": 0
																					},
																					"stamp": 54
																				},
																				"InvoicesToExportTempFile__": {
																					"type": "ShortText",
																					"data": [
																						"InvoicesToExportTempFile__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "InvoicesToExportTempFile",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 55
																				},
																				"LabelInvoicesApprovedTempFile__": {
																					"type": "Label",
																					"data": [
																						"InvoicesApprovedTempFile__"
																					],
																					"options": {
																						"label": "InvoicesApprovedTempFile",
																						"version": 0
																					},
																					"stamp": 56
																				},
																				"InvoicesApprovedTempFile__": {
																					"type": "ShortText",
																					"data": [
																						"InvoicesApprovedTempFile__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "InvoicesApprovedTempFile",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 57
																				},
																				"LabelLastMsnEx__": {
																					"type": "Label",
																					"data": [
																						"LastMsnEx__"
																					],
																					"options": {
																						"label": "LastMsnEx",
																						"version": 0
																					},
																					"stamp": 58
																				},
																				"LastMsnEx__": {
																					"type": "ShortText",
																					"data": [
																						"LastMsnEx__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "LastMsnEx",
																						"activable": true,
																						"width": 230,
																						"readonly": true,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 59
																				},
																				"LabelDateOfStartExport__": {
																					"type": "Label",
																					"data": [
																						"DateOfStartExport__"
																					],
																					"options": {
																						"label": "DateOfStartExport"
																					},
																					"stamp": 60
																				},
																				"DateOfStartExport__": {
																					"type": "RealDateTime",
																					"data": [
																						"DateOfStartExport__"
																					],
																					"options": {
																						"readonly": true,
																						"displayLongFormat": false,
																						"label": "DateOfStartExport",
																						"activable": true,
																						"width": 230
																					},
																					"stamp": 61
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 62,
													"*": {
														"Scheduling": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 400,
																"iconURL": "AP_Scheduling.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Scheduling",
																"leftImageURL": "",
																"version": 0
															},
															"stamp": 63,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {
																			"SchedulingDate__": "LabelSchedulingDate__",
																			"LabelSchedulingDate__": "SchedulingDate__",
																			"Then_repeat__": "LabelThen_repeat__",
																			"LabelThen_repeat__": "Then_repeat__",
																			"SchedulingHours__": "LabelSchedulingHours__",
																			"LabelSchedulingHours__": "SchedulingHours__",
																			"SchedulingMinutes__": "LabelSchedulingMinutes__",
																			"LabelSchedulingMinutes__": "SchedulingMinutes__",
																			"SchedulingOptions__": "LabelSchedulingOptions__",
																			"LabelSchedulingOptions__": "SchedulingOptions__"
																		},
																		"version": 0
																	},
																	"stamp": 64,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Spacer_line1__": {
																						"line": 1,
																						"column": 1
																					},
																					"LabelSchedulingOptions__": {
																						"line": 2,
																						"column": 1
																					},
																					"SchedulingOptions__": {
																						"line": 2,
																						"column": 2
																					},
																					"SchedulingDate__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelSchedulingDate__": {
																						"line": 3,
																						"column": 1
																					},
																					"SchedulingHours__": {
																						"line": 4,
																						"column": 2
																					},
																					"LabelSchedulingHours__": {
																						"line": 4,
																						"column": 1
																					},
																					"SchedulingMinutes__": {
																						"line": 5,
																						"column": 2
																					},
																					"LabelSchedulingMinutes__": {
																						"line": 5,
																						"column": 1
																					},
																					"Spacer_line2__": {
																						"line": 6,
																						"column": 1
																					},
																					"Then_repeat__": {
																						"line": 7,
																						"column": 2
																					},
																					"LabelThen_repeat__": {
																						"line": 7,
																						"column": 1
																					},
																					"Spacer_line3__": {
																						"line": 8,
																						"column": 1
																					}
																				},
																				"lines": 8,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[],
																					[],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 65,
																			"*": {
																				"Spacer_line1__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 66
																				},
																				"LabelSchedulingOptions__": {
																					"type": "Label",
																					"data": [
																						"SchedulingOptions__"
																					],
																					"options": {
																						"label": "_SchedulingOptions",
																						"version": 0
																					},
																					"stamp": 67
																				},
																				"SchedulingOptions__": {
																					"type": "RadioButton",
																					"data": [
																						"SchedulingOptions__"
																					],
																					"options": {
																						"keys": [
																							"Execute now",
																							"Defer execution until"
																						],
																						"values": [
																							"Execute now",
																							"Defer execution until"
																						],
																						"label": "_SchedulingOptions",
																						"activable": true,
																						"version": 0
																					},
																					"stamp": 68
																				},
																				"LabelSchedulingDate__": {
																					"type": "Label",
																					"data": [
																						"SchedulingDate__"
																					],
																					"options": {
																						"label": "_SchedulingDate",
																						"version": 0
																					},
																					"stamp": 69
																				},
																				"SchedulingDate__": {
																					"type": "DateTime",
																					"data": [
																						"SchedulingDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_SchedulingDate",
																						"activable": true,
																						"width": "120",
																						"version": 0
																					},
																					"stamp": 70
																				},
																				"LabelSchedulingHours__": {
																					"type": "Label",
																					"data": [
																						"SchedulingHours__"
																					],
																					"options": {
																						"label": "_SchedulingHours",
																						"version": 0
																					},
																					"stamp": 71
																				},
																				"SchedulingHours__": {
																					"type": "ComboBox",
																					"data": [
																						"SchedulingHours__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "00OClock",
																							"1": "01OClock",
																							"2": "02OClock",
																							"3": "03OClock",
																							"4": "04OClock",
																							"5": "05OClock",
																							"6": "06OClock",
																							"7": "07OClock",
																							"8": "08OClock",
																							"9": "09OClock",
																							"10": "10OClock",
																							"11": "11OClock",
																							"12": "12OClock",
																							"13": "13OClock",
																							"14": "14OClock",
																							"15": "15OClock",
																							"16": "16OClock",
																							"17": "17OClock",
																							"18": "18OClock",
																							"19": "19OClock",
																							"20": "20OClock",
																							"21": "21OClock",
																							"22": "22OClock",
																							"23": "23OClock"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "00OClock",
																							"1": "01OClock",
																							"2": "02OClock",
																							"3": "03OClock",
																							"4": "04OClock",
																							"5": "05OClock",
																							"6": "06OClock",
																							"7": "07OClock",
																							"8": "08OClock",
																							"9": "09OClock",
																							"10": "10OClock",
																							"11": "11OClock",
																							"12": "12OClock",
																							"13": "13OClock",
																							"14": "14OClock",
																							"15": "15OClock",
																							"16": "16OClock",
																							"17": "17OClock",
																							"18": "18OClock",
																							"19": "19OClock",
																							"20": "20OClock",
																							"21": "21OClock",
																							"22": "22OClock",
																							"23": "23OClock"
																						},
																						"label": "_SchedulingHours",
																						"activable": true,
																						"width": "70",
																						"version": 0
																					},
																					"stamp": 72
																				},
																				"LabelSchedulingMinutes__": {
																					"type": "Label",
																					"data": [
																						"SchedulingMinutes__"
																					],
																					"options": {
																						"label": "_SchedulingMinutes",
																						"version": 0
																					},
																					"stamp": 73
																				},
																				"SchedulingMinutes__": {
																					"type": "ComboBox",
																					"data": [
																						"SchedulingMinutes__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "00",
																							"1": "10",
																							"2": "20",
																							"3": "30",
																							"4": "40",
																							"5": "50"
																						},
																						"keyValueMode": false,
																						"possibleKeys": {},
																						"label": "_SchedulingMinutes",
																						"activable": true,
																						"width": "70",
																						"version": 0
																					},
																					"stamp": 74
																				},
																				"Spacer_line2__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 75
																				},
																				"LabelThen_repeat__": {
																					"type": "Label",
																					"data": [
																						"Then_repeat__"
																					],
																					"options": {
																						"label": "_Then_repeat",
																						"version": 0
																					},
																					"stamp": 76
																				},
																				"Then_repeat__": {
																					"type": "ComboBox",
																					"data": [
																						"Then_repeat__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Never",
																							"1": "Everyday",
																							"2": "Every_week",
																							"3": "Every_month"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Never",
																							"1": "Everyday",
																							"2": "Every_week",
																							"3": "Every_month"
																						},
																						"label": "_Then_repeat",
																						"activable": true,
																						"width": "120",
																						"version": 0
																					},
																					"stamp": 77
																				},
																				"Spacer_line3__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Spacer line",
																						"version": 0
																					},
																					"stamp": 78
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											},
											"stamp": 79,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "45%",
													"width": "55%",
													"height": null
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Document Preview",
																"hidden": true
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 80
																}
															},
															"stamp": 81,
															"data": []
														}
													},
													"stamp": 82,
													"data": []
												}
											},
											"stamp": 83,
											"data": []
										}
									},
									"stamp": 84,
									"data": []
								}
							},
							"stamp": 85,
							"data": []
						}
					},
					"stamp": 86,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Export": {
							"type": "SubmitButton",
							"options": {
								"label": "Export now",
								"action": "approve",
								"submit": true,
								"version": 0,
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "View.link?tabName=_Exports&viewName=_AP_View_ExportedInvoices",
								"style": 1
							},
							"stamp": 87,
							"data": []
						},
						"Cancel": {
							"type": "SubmitButton",
							"data": false,
							"options": {
								"label": "_Cancel scheduled export",
								"nextprocess": {
									"processName": "Export Invoices",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"url": "",
								"action": "approve",
								"version": 0,
								"style": 3
							},
							"stamp": 88
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0,
								"nextprocess": {
									"processName": "",
									"attachmentsMode": "all",
									"willBeChild": true
								}
							},
							"stamp": 89,
							"data": []
						}
					},
					"stamp": 90,
					"data": []
				}
			},
			"stamp": 91,
			"data": []
		}
	},
	"stamps": 91,
	"data": []
}