{
	"*": {
		"GDRData": {
			"type": "GDRData",
			"options": {
				"version": 0
			},
			"stamp": 1,
			"data": []
		},
		"OCRParams": {
			"type": "OCRParams",
			"options": {
				"version": 0
			},
			"stamp": 2,
			"data": []
		},
		"Screen": {
			"type": "FlexibleFormLayout",
			"options": {
				"splitterType": "complex",
				"version": 0,
				"maxwidth": 1024
			},
			"*": {
				"form-header": {
					"type": "FlexibleFormHeader",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"stamp": 3,
					"data": []
				},
				"form-center": {
					"type": "FlexibleFormCenter",
					"options": {
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"form-content": {
							"type": "FlexibleFormContent",
							"options": {
								"version": 0
							},
							"*": {
								"form-content-top": {
									"type": "FlexibleFormSplitter",
									"options": {
										"autosize": true,
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "25%"
										},
										"hidden": true
									},
									"*": {},
									"stamp": 4,
									"data": []
								},
								"form-content-bottom": {
									"type": "FlexibleFormSplitter",
									"options": {
										"version": 1,
										"box": {
											"top": "0%",
											"left": "0%",
											"width": null,
											"height": "100%"
										},
										"hidden": false,
										"hideSeparator": true
									},
									"*": {
										"form-content-left": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "0%",
													"width": "100%",
													"height": null
												},
												"hidden": false
											},
											"*": {
												"form-content-left-6": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 5,
													"*": {
														"ErrorPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "color7",
																"labelsAlignment": "left",
																"label": "ErrorPane",
																"leftImageURL": "public\\error.png",
																"version": 0,
																"hidden": false
															},
															"stamp": 6,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 7,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"Error__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 1,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 8,
																			"*": {
																				"Error__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "color2",
																						"backgroundcolor": "default",
																						"label": "",
																						"version": 0
																					},
																					"stamp": 9
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-5": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 10,
													"*": {
														"Update_payment_title_pane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Update_payment_title_pane",
																"leftImageURL": "",
																"version": 0,
																"hidden": true
															},
															"stamp": 11,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 12,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"UpdatePaymentDetailsTitle__": {
																						"line": 1,
																						"column": 1
																					},
																					"UpdatePaymentDetailsSubTitle__": {
																						"line": 2,
																						"column": 1
																					}
																				},
																				"lines": 2,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 13,
																			"*": {
																				"UpdatePaymentDetailsTitle__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_UpdatePaymentDetailsTitle",
																						"version": 0
																					},
																					"stamp": 14
																				},
																				"UpdatePaymentDetailsSubTitle__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "M",
																						"textAlignment": "center",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_UpdatePaymentDetailsSubTitle",
																						"version": 0
																					},
																					"stamp": 15
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-7": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 16,
													"*": {
														"InvoicesViewPanel": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "Invoices view",
																"leftImageURL": "",
																"version": 0
															},
															"stamp": 17,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 18,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"InvoicesView__": {
																						"line": 1,
																						"column": 1
																					},
																					"Ligne_d_espacement__": {
																						"line": 2,
																						"column": 1
																					},
																					"InvoicesToPayExplanation__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"lines": 3,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 19,
																			"*": {
																				"InvoicesView__": {
																					"type": "AdminList",
																					"data": false,
																					"options": {
																						"width": "100%",
																						"restrictToCurrentJobId": false,
																						"showActions": false,
																						"label": "Invoices to pay",
																						"version": 0
																					},
																					"stamp": 20
																				},
																				"Ligne_d_espacement__": {
																					"type": "Spacer",
																					"data": false,
																					"options": {
																						"height": "",
																						"width": "",
																						"color": "default",
																						"label": "Ligne d'espacement",
																						"version": 0
																					},
																					"stamp": 21
																				},
																				"InvoicesToPayExplanation__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "center",
																						"textStyle": "bold",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_DefaultInvoicesToPayExplanation"
																					},
																					"stamp": 22
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"Payment_pane": {
															"type": "PanelData",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": "400",
																"iconURL": "AP_PaymentDetailsPanel.png",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_Payment_pane",
																"leftImageURL": "",
																"hidden": false
															},
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"options": {
																		"controlsAssociation": {
																			"PaymentDate__": "LabelPayment_date__",
																			"LabelPayment_date__": "PaymentDate__",
																			"Payment_method__": "LabelPayment_method__",
																			"LabelPayment_method__": "Payment_method__",
																			"PaymentReference__": "LabelPayment_reference__",
																			"LabelPayment_reference__": "PaymentReference__"
																		},
																		"version": 0
																	},
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"options": {
																				"lines": 3,
																				"columns": 2,
																				"version": 0,
																				"ctrlsPos": {
																					"PaymentDate__": {
																						"line": 1,
																						"column": 2
																					},
																					"LabelPayment_date__": {
																						"line": 1,
																						"column": 1
																					},
																					"Payment_method__": {
																						"line": 2,
																						"column": 2
																					},
																					"LabelPayment_method__": {
																						"line": 2,
																						"column": 1
																					},
																					"PaymentReference__": {
																						"line": 3,
																						"column": 2
																					},
																					"LabelPayment_reference__": {
																						"line": 3,
																						"column": 1
																					}
																				},
																				"colspans": [
																					[],
																					[],
																					[]
																				]
																			},
																			"data": [
																				"fields"
																			],
																			"*": {
																				"LabelPayment_date__": {
																					"type": "Label",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"label": "_Payment_date",
																						"version": 0
																					},
																					"stamp": 23
																				},
																				"PaymentDate__": {
																					"type": "DateTime",
																					"data": [
																						"PaymentDate__"
																					],
																					"options": {
																						"displayLongFormat": false,
																						"label": "_Payment_date",
																						"activable": true,
																						"width": 230,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 24
																				},
																				"LabelPayment_method__": {
																					"type": "Label",
																					"data": [
																						"Payment_method__"
																					],
																					"options": {
																						"label": "_Payment_method",
																						"version": 0
																					},
																					"stamp": 25
																				},
																				"Payment_method__": {
																					"type": "ComboBox",
																					"data": [
																						"Payment_method__"
																					],
																					"options": {
																						"possibleValues": {
																							"0": "Cash",
																							"1": "Check",
																							"2": "Credit card",
																							"3": "EFT",
																							"4": "Other"
																						},
																						"keyValueMode": true,
																						"possibleKeys": {
																							"0": "Cash",
																							"1": "Check",
																							"2": "Credit card",
																							"3": "EFT",
																							"4": "Other"
																						},
																						"label": "_Payment_method",
																						"activable": true,
																						"width": 230,
																						"version": 0
																					},
																					"stamp": 26
																				},
																				"LabelPayment_reference__": {
																					"type": "Label",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"label": "_Payment_reference",
																						"version": 0
																					},
																					"stamp": 27
																				},
																				"PaymentReference__": {
																					"type": "ShortText",
																					"data": [
																						"PaymentReference__"
																					],
																					"options": {
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"label": "_Payment_reference",
																						"activable": true,
																						"width": 230,
																						"browsable": false,
																						"autocompletable": false,
																						"version": 0
																					},
																					"stamp": 28
																				}
																			},
																			"stamp": 29
																		}
																	},
																	"stamp": 30,
																	"data": []
																}
															},
															"stamp": 31,
															"data": []
														}
													},
													"stamp": 32,
													"data": []
												},
												"form-content-left-2": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"SystemData": {
															"type": "PanelSystemData",
															"options": {
																"multicolumn": false,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_System data",
																"hidden": true
															},
															"stamp": 33,
															"data": []
														}
													},
													"stamp": 34,
													"data": []
												},
												"form-content-left-3": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"NextProcess": {
															"type": "PanelNextProcess",
															"options": {
																"hidden": true,
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Next Process"
															},
															"stamp": 35,
															"data": []
														}
													},
													"stamp": 36,
													"data": []
												},
												"form-content-left-4": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 37,
													"*": {
														"ProcessDescriptionPane": {
															"type": "PanelData",
															"data": [],
															"options": {
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": true,
																"backgroundcolor": "default",
																"labelsAlignment": "right",
																"label": "_ProcessDescriptionPane",
																"leftImageURL": "",
																"version": 0
															},
															"stamp": 38,
															"*": {
																"FieldsManager": {
																	"type": "FieldsManager",
																	"data": [],
																	"options": {
																		"controlsAssociation": {},
																		"version": 0
																	},
																	"stamp": 39,
																	"*": {
																		"Grid": {
																			"type": "GridLayout",
																			"data": [
																				"fields"
																			],
																			"options": {
																				"ctrlsPos": {
																					"ProcessDescriptionPart1__": {
																						"line": 2,
																						"column": 1
																					},
																					"ProcessDescriptionPart2__": {
																						"line": 3,
																						"column": 1
																					},
																					"ProcessDescriptionPart3__": {
																						"line": 4,
																						"column": 1
																					},
																					"PaymentStatusImport__": {
																						"line": 1,
																						"column": 1
																					}
																				},
																				"lines": 4,
																				"columns": 2,
																				"colspans": [
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					],
																					[
																						{
																							"index": 0,
																							"length": 2
																						}
																					]
																				],
																				"version": 0
																			},
																			"stamp": 40,
																			"*": {
																				"PaymentStatusImport__": {
																					"type": "Title",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "XXL",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "_Payment status import",
																						"version": 0
																					},
																					"stamp": 41
																				},
																				"ProcessDescriptionPart1__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "\nSelect the CSV import file that contains the list of invoices to update.\nMake sure the CSV import file is formatted as follows:\n\n",
																						"version": 0
																					},
																					"stamp": 42
																				},
																				"ProcessDescriptionPart2__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "bold",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "Company code;Vendor number;Invoice number;Payment date;Payment method;Payment reference",
																						"version": 0
																					},
																					"stamp": 43
																				},
																				"ProcessDescriptionPart3__": {
																					"type": "Text",
																					"data": false,
																					"options": {
																						"marge": 0,
																						"textSize": "S",
																						"textAlignment": "left",
																						"textStyle": "default",
																						"textColor": "default",
																						"backgroundcolor": "default",
																						"label": "US01;10000;4850010;2014-05-31;Credit card;\nUS01;10000;4850012;2014-04-01;Check;132684",
																						"version": 0
																					},
																					"stamp": 44
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												},
												"form-content-left-10": {
													"type": "FlexibleFormLine",
													"data": [],
													"options": {
														"version": 0
													},
													"stamp": 45,
													"*": {
														"CSVImportFilePane": {
															"type": "PanelDocument",
															"options": {
																"version": 0,
																"border": {},
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": true,
																"hideTitleBar": false,
																"label": "_CSVImportFilePane"
															},
															"*": {
																"Document": {
																	"data": [
																		"documentscontroldata"
																	],
																	"type": "Documents",
																	"options": {
																		"version": 1,
																		"previewButton": false,
																		"downloadButton": true
																	},
																	"stamp": 46
																}
															},
															"stamp": 47,
															"data": []
														}
													}
												}
											},
											"stamp": 48,
											"data": []
										},
										"form-content-right": {
											"type": "FlexibleFormSplitter",
											"options": {
												"version": 1,
												"box": {
													"top": "0%",
													"left": "45%",
													"width": "55%",
													"height": null
												},
												"hidden": true,
												"hideSeparator": true
											},
											"*": {
												"form-content-right-1": {
													"type": "FlexibleFormLine",
													"options": {
														"version": 0
													},
													"*": {
														"PreviewPanel": {
															"type": "PanelPreview",
															"options": {
																"border": {},
																"version": 0,
																"labelLength": 0,
																"iconURL": "",
																"TitleFontSize": "S",
																"hideActionButtons": false,
																"hideTitleBar": false,
																"label": "_Document Preview"
															},
															"*": {
																"Preview": {
																	"data": [
																		"previewdata"
																	],
																	"type": "Preview",
																	"options": {
																		"version": 0
																	},
																	"stamp": 49
																}
															},
															"stamp": 50,
															"data": []
														}
													},
													"stamp": 51,
													"data": []
												}
											},
											"stamp": 52,
											"data": []
										}
									},
									"stamp": 53,
									"data": []
								}
							},
							"stamp": 54,
							"data": []
						}
					},
					"stamp": 55,
					"data": []
				},
				"form-footer": {
					"type": "FlexibleFormFooter",
					"options": {
						"splitterType": "simple-h",
						"version": 0,
						"maxwidth": 1024
					},
					"*": {
						"Update": {
							"type": "SubmitButton",
							"options": {
								"label": "_UpdatePayment",
								"action": "approve",
								"submit": true,
								"version": 0,
								"nextprocess": {
									"processName": "Accounts Receivable Batch Processing",
									"attachmentsMode": "all",
									"willBeChild": true
								},
								"style": 1,
								"url": ""
							},
							"stamp": 56,
							"data": []
						},
						"Close": {
							"type": "SubmitButton",
							"options": {
								"label": "_Close",
								"action": "cancel",
								"submit": true,
								"version": 0
							},
							"stamp": 57,
							"data": []
						}
					},
					"stamp": 58,
					"data": []
				}
			},
			"stamp": 59,
			"data": []
		}
	},
	"stamps": 59,
	"data": []
}