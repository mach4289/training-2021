///#GLOBALS Lib Sys
////////////////
/// Entry point
////////////////
var currentName = Data.GetActionName();
var currentAction = Data.GetActionType();
Log.Info("-- GR canceler validation Script -- Name: '" + (currentName ? currentName : "<empty>") + "', Action: '" + (currentAction ? currentAction : "<empty>") + "'");
// First validation
if (currentName === "" && currentAction === "") {
    if (Data.GetValue("State") == 50) {
        Log.Info("Validation in touchless");
        currentName = "Approve";
        currentAction = "approve";
    }
}
if (currentName === "Approve" && currentAction === "approve") {
    var ruidEx = Data.GetValue("GRRuidEx__");
    var comment = Data.GetValue("CancelComment__");
    Lib.Purchasing.GRCanceler.Cancel(ruidEx, comment)
        .Then(function () {
        Lib.CommonDialog.NextAlert.Define("_GR cancel success", "_GR cancel success message", {
            isError: false,
            behaviorName: "GRCancelSuccess"
        });
    })
        .Catch(function (reason) {
        // any error
        if (Sys.Helpers.IsString(reason) || reason instanceof Error) {
            Log.Error(reason.toString());
            Lib.CommonDialog.NextAlert.Define("_GR cancel error", "_GR cancel error message", {
                isError: true,
                behaviorName: "GRCancelFailure"
            }, reason.toString());
        }
        // noError reject
        else if (reason instanceof Lib.Purchasing.GRCanceler.NoLongerCancelable) {
            Log.Info(reason.toString());
            Lib.CommonDialog.NextAlert.Define("_GR cancel aborted", "_GR cancel aborted message", {
                isError: false,
                behaviorName: "GRCancelAborted"
            }, reason.toString());
        }
    });
}
